﻿/** 
 *
 * @summary   UDH Select2 4.0.0 Library for remote data
 *
 * @remoteDataUrl - url that provides JSON data for select2. 
 * The function that is denoted by @remoteDataUrl should have 'searchTerm' 
 * and 'page' as Parameters
 *
 * @data - JSON data with format [{'Id': '4', 'Text': 'Name'}]
 *
 *
 *Usage: $('#ddlId').select2(remoteDataConfig(testUrl, 3, 250, '--Choose'));
 *
 */



var remoteDataConfig = function (remoteDataUrl, minimumInputLength, delay, placeholder) {
    
    minimumInputLength = typeof minimumInputLength !== 'undefined' ? minimumInputLength : 1;
    delay = typeof delay !== 'undefined' ? delay : 250;
    placeholder = typeof placeholder !== 'undefined' ? placeholder : '--Choose';

    var config = {

        placeholder: placeholder,
        allowClear: true,
        delay: delay,
        minimumInputLength: minimumInputLength,
        ajax: {

            url: remoteDataUrl,
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    searchTerm: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                if (data) {
                    params.page = params.page || 1;

                    data = JSON.parse(data);
                    var select2Data = $.map(data, function(obj) {
                        obj.id = obj.Id;
                        obj.text = obj.Text;

                        return obj;
                    });

                    return {
                        results: select2Data,
                        pagination: {
                            more: data.more
                        }
                    };
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; },
        //templateResult: formatRepo,
        //templateSelection: formatRepoSelection
        templateResult: function (option) {
            if (option.loading) return option.text;
            return "<option>" + option.Text + "</option>";
        },
        templateSelection: function(option) {
            return option.full_name || option.text;
        }
    };

    return config;
}

function formatRepo(repo) {
    if (repo.loading) return repo.text;

    var markup = '<div class="clearfix">' +
    
    '<div clas="col-sm-10">' +
    '<div class="clearfix">' +
    '<div class="col-sm-6">' + repo.Text + '</div>' +
    
    '</div>';

    if (repo.description) {
        markup += '<div>' + repo.description + '</div>';
    }

    markup += '</div></div>';

    return markup;
}

function formatRepoSelection(repo) {
    return repo.full_name || repo.text;
}
