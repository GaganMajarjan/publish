$(function () {
    $('.errorPara').each(function () {
        if ($(this).text() === '*') {
            var next = $(this).parents('.input-group').find('.form-control');
            var found = $(this).parents('.input-group').next('.field-validation-valid');

            if ((next.val() && found)) {
                $(this).parents('.input-group').removeClass('has-error');
            }
            else {
                $(this).parents('.input-group').addClass('has-error');
            }
        }
    });

    var editFormErrorDisplay = function (ob) {
        $(ob).valid();
        var field = $(ob).parent().next().attr('class');
        if (field == 'field-validation-valid') {
            $(ob).parent().removeClass('has-error');
        }
        else if (field == 'field-validation-error') {
            $(ob).parent().addClass('has-error');
        }
    };

    $('.form-control').change(function () {
        var self = this;
        //jquery validate works only for page having form tag
        if ($(self).parents("form").length == 0)
            return;
        $(self).validate();
        setTimeout(function (e) {
            editFormErrorDisplay(self);

        }, 1);


    });

    $('.btn-validate').click(function () {
        setTimeout(function () {
            $('input.input-validation-error').each(function () {
                $('span.field-validation-error').show();
                $(this).parents('.input-group').addclass('has-error');
            });
        }, 500);
       
    });
});

$('a[data-toggle="tab"]').on('shown.bs.tab', function () {
    $('.errorPara').each(function () {
        if ($(this).text() === '*') {
            var next = $(this).parents('.input-group').find('.form-control');
            var found = $(this).parents('.input-group').next('.field-validation-valid');
            if ((next.val() && found)) {
                $(this).parents('.input-group').removeClass('has-error');
            }
            else {
                $(this).parents('.input-group').addClass('has-error');
            }
        }
    });

    var editFormErrorDisplay = function (ob) {
        $(ob).valid();
        var field = $(ob).parent().next().attr('class');
        if (field == 'field-validation-valid') {
            $(ob).parent().removeClass('has-error');
        }
        else if (field == 'field-validation-error') {
            $(ob).parent().addClass('has-error');
        }
    };

    $('.form-control').change(function () {
        var self = this;
        //jquery validate works only for page having form tag
        if ($(self).parents("form").length == 0)
            return;
        $(self).validate();
        setTimeout(function (e) {
            editFormErrorDisplay(self);
        }, 1);
    });
   
});