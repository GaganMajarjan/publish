﻿/*
    UDH implementation of handsontable
    http://handsontable.com/
*/

var UdhHandsonTable = {

    /**
  * @method  getSettings
  * @param   {Object} options
  * @returns {Object} handsontable settings
  */
    getSettings: function (options) { 
        
        options = options || {};

        var defaultOptions = {
            //data: {},
            colHeaders: true,
            rowHeaders: true,
            stretchH: 'all',
            columnSorting: false,
            //search: true,
            fixedRowsTop: 0,
            fixedColumnsLeft: 1,
            contextMenu: true,
            //columns: {},
            minSpareRows: 0,
            manualColumnResize: true,
            //manualColumnMove: true,
            destroy: true, 
            formulas: true
        };

        $.extend(true, defaultOptions, options);

        return defaultOptions;
    },

    //----------------------testing----------------------
    test: function() {
        
    }
};

