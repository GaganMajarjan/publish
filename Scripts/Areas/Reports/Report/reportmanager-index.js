﻿if (saveStatus) {
    alertmsg("green", saveStatus);
}

if (deleteStatus) {
    alertmsg("green", deleteStatus);
}

var counter = 0;
var id = 0;
var customfieldData;
var customfieldGroup;

function getSingleReportSelected() {
    counter = 0;
    id = 0;
    $('input[type=checkbox][class=selectOne]').each(function () {
        if (this.checked) {
            id = this.value;
            counter++;
        }
    });
    return id;
}


$('#selectAllCheckBox').click(function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.selectOne').prop("checked", true);
    } else {
        $('.selectOne').prop("checked", false);
    }
});

$('#udhReportList').dataTable({
    "deferRender": true,
    "bServerSide": true,
    "aLengthMenu": dataTableMenuLength,
    "iDisplayLength": dataTableDisplayLength,
    "destroy": true,
    "sAjaxSource": urlGetReportDataTable,
    "scrollX": true,
    "aoColumns": [
                    {

                        "bSearchable": false,
                        "bSortable": false,
                        "mRender": function (data, type, full) {
                            return '<input type="checkbox" id="selectOneCheckBox" class="selectOne" value="' + data + '">'

                        }
                    },
                    {

                        "bSortable": false,
                        "sName": "Name"
                    },
                    {

                        "bSortable": false,
                        "sName": "CreateTs"
                    },
                    {

                        "bSortable": false,
                        "sName": "CreateUser"
                    },
                    {

                        "bSearchable": false,
                        "bSortable": false,
                        "mRender": function (data, type, full) {
                            return '<a href ="' + urlViewReport + '?id=' + data + '" target = "_blank">View </a>'

                        }
                    }


    ],
    "fnPreDrawCallback": function () {
        // gather info to compose a message
        var ob = this;
        SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
        //SGtableloading('fa-plane', '', this);


    },
    "fnDrawCallback": function () {
        // in case your overlay needs to be put away automatically you can put it here
        var divid = this.closest('.dataTables_wrapper').attr('id');
        SGtableloadingRemove(divid);
    }
});

$('#createReportButton').click(function () {

    window.location = urlCreateReport;
});

$('#editReportButton').click(function () {

    var selectedReportId = getSingleReportSelected();
    if (counter !== 1) {
        alertmsg("red", "Please, select a single report to edit");
        return;
    }

    window.location = urlUpdateReport + "?id=" + selectedReportId;
});

$("#deleteReportButton").click(function () {

    var selectedReportId = getSingleReportSelected();
    if (counter !== 1) {
        alertmsg("red", "Please, select a single report to delete");
        return;
    }
    $("#reportIdsToDelete").val(selectedReportId);
    $("#divConfirmDelete").modal("show");


});

$("#divConfirmDeleteYes").click(function () {

    var reportIdsCommaSeparated = $("#reportIdsToDelete").val();

    // Ajax call
    $.ajax({
        type: 'POST',
        url: urlForDelete,
        cache: false,
        data: { 'reportIdsCommaSeparated': reportIdsCommaSeparated },
        dataType: 'json',
        success: function (response) {
            location.reload();
            if (response != null && response.Status === true) {
                alertmsg("green", response.Message);
            } else if (response != null && response.Status === false) {
                alertmsg("red", response.Message);
            }
            else {
                alertmsg("red", errorOcurredMsg);
            }
        },
        error: function (err) {
            location.reload();
            alertmsg("red", errorOcurredMsg);
        }
    });
});

$("#addReportParameterButton").click(function () {
    var selectedReportId = getSingleReportSelected();
    if (counter !== 1) {
        alertmsg("red", "Please, select a single report to update parameter.");
        return;
    }

    window.location = urlUpdateReportParameter + "?reportId=" + selectedReportId;

});