﻿

//var jsonData = JSON.parse(reportParamList);
var jsonData = reportParamList;

if (jsonData.filters != null) {
    $('#builder').queryBuilder(jsonData);
    $('button[data-add="group"]').hide();
    if (filterJson) {
        try {
            filterJson = fixJsonQuote(filterJson);
            console.log(filterJson);

            $("#builder").queryBuilder('setRules', filterJson);
        } catch (e) {

        }
    }

}

$("#saveFilterCriteria").click(function () {
    var paramHolder = [];
    var whereJson = "";
    var whereFilter = "";
    try {
        var filter = $("#builder").queryBuilder('getRules', false);
        if (filter) {
            var rules = filter.rules;
            if (rules) {
                $.each(rules, function (key, val) {
                    var data = ruleParser(val);
                    paramHolder.push(data);
                });
            }

            whereJson = JSON.stringify(filter);
            whereFilter = $("#builder").queryBuilder('getSQL', false).sql.replace('\n', '');
        }
    } catch (e) {

    }

    var vm = new ReportManagerParameterViewModel();
    vm.RFXReportManagerId = reportId;
    vm.ParameterViewModel = paramHolder;

    var jsonData = JSON.stringify(
    {
        vm: vm,
        whereJson: whereJson,
        whereFilter: whereFilter
    });
    console.log(jsonData);

    // Ajax call
    $.ajax({
        type: 'POST',
        url: urlUpdateReportParameter,
        cache: false,
        contentType: 'application/json; charset=utf-8',
        //traditional: true,
        data: jsonData,
        beforeSend: function () {
            SGloading('Loading');
        },
        dataType: 'json',
        success: function (response) {
            if (response != null && response.Status === true) {
                alertmsg("green", response.Message);
            } else if (response != null && response.Status === false) {
                alertmsg("red", response.Message);
            } else {
                alertmsg("red", errorOcurredMsg);
            }
        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (err) {
            alertmsg("red", errorOcurredMsg);
        }
    });
});


var ParameterViewModel = function () {

    this.ParamName = "";
    this.DataType = "";
    this.Operator = "";
    this.DefaultValue = "";

}

var ReportManagerParameterViewModel = function () {
    this.RFXReportManagerId = 0;
    this.ParameterViewModel = [];
}

var ruleParser = function (input) {

    var column = input.id;
    var dataType = input.type;
    var value = input.value;
    var operator = input.operator;

    var param = new ParameterViewModel();
    param.ParamName = column;
    param.DataType = dataType;
    param.DefaultValue = value;
    param.Operator = operator;

    return param;

}