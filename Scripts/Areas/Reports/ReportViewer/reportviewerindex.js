﻿
//var ReportVM = function (value) {
//    var me = this;

//    me.Id = (value.Id == null) ? "" : value.Id;
//    me.Name = (value.Name == null) ? "" : value.Name;
//    me.Sql = (value.Sql == null) ? "" : value.Sql;
//    me.DefaultParamValue = (value.DefaultParamValue == null) ? "" : value.DefaultParamValue;
//    me.CreateTs = (value.CreateTs == null) ? "" : value.CreateTs;
//    me.CreateUser = (value.CreateUser == null) ? "" : value.CreateUser;

//}

//var ReportViewerVM = function () {
//    var self = this;

//    self.myData = ko.observableArray([]);

//    //self.gridOptions = {
//    //    data: self.myData,
//    //    showGroupPanel: true,
//    //    jqueryUIDraggable: true
//    //};

//    self.NameClick = function (value) {
//        //alertmsg('green', 'Id: ' + value.Id + '\n Sql: ' + value.Sql);
//        initializeTableHeaders(value.Id);


//    }
//}

//var vm = new ReportViewerVM();
//ko.applyBindings(vm);



//$.each(jsonReportList, function (key, value) {
//    vm.myData.push(new ReportVM(value));
//});

////refresh
//vm.myData(vm.myData());

$('#selectAllCheckBox').click(function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.selectOne').prop("checked", true);
    } else {
        $('.selectOne').prop("checked", false);
    }
});

$('#udhReportList').dataTable({
    "deferRender": true,
    "bServerSide": true,
    "aLengthMenu": dataTableMenuLength,
    "iDisplayLength": dataTableDisplayLength,
    "destroy": true,
    "sAjaxSource": urlGetReportDataTable,
    "scrollX": true,
    "bSort": false,
    "aoColumns": [
                    {

                        "bSearchable": false,
                        "bSortable": false,
                        "mRender": function (data, type, full) {
                            return '<input type="checkbox" id="selectOneCheckBox" class="selectOne" value="' + data + '">'

                        }
                    },
                    {

                        "bSortable": false,
                        "sName": "Name"
                    },
                    {

                        "bSortable": false,
                        "sName": "CreateTs"
                    },
                    {

                        "bSortable": false,
                        "sName": "CreateUser"
                    },
                    {

                        "bSearchable": false,
                        "bSortable": false,
                        "mRender": function (data, type, full) {
                            return '<a href ="' + urlViewReport + '?id=' + data + '" target = "_blank">View </a>'

                        }
                    }


    ],
    "fnPreDrawCallback": function () {
        // gather info to compose a message
        var ob = this;
        SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
        //SGtableloading('fa-plane', '', this);


    },
    "fnDrawCallback": function () {
        // in case your overlay needs to be put away automatically you can put it here
        var divid = this.closest('.dataTables_wrapper').attr('id');
        SGtableloadingRemove(divid)
    }
});


$('#udhExportList').dataTable({
    "deferRender": true,
    "bServerSide": true,
    "iDisplayLength": 25,
    "destroy": true,
    "sAjaxSource": urlGetReportDataTable,
    "bProcessing": true,
    "scrollX": true,
    "bSort": false,
    "dom": 'T<"clear">lfrtip',
    "tableTools": {
        "sSwfPath": tableToolsPath
    },
    "aoColumns": [
                    {

                        "bSearchable": false,
                        "bSortable": false,
                        "mRender": function (data, type, full) {
                            return '<input type="checkbox" id="selectOneCheckBox" class="selectOne" value="' + data + '">'

                        }
                    },
                    {

                        "bSortable": false,
                        "sName": "Name"
                    },
                    {

                        "bSortable": false,
                        "sName": "CreateTs"
                    },
                    {

                        "bSortable": false,
                        "sName": "CreateUser"
                    },
                    {

                        "bSearchable": false,
                        "bSortable": false,
                        "mRender": function (data, type, full) {
                            return '<a href ="' + urlViewReport + '?id=' + data + '" target = "_blank">View </a>'

                        }
                    }


    ],
    "fnPreDrawCallback": function () {
        // gather info to compose a message
        var ob = this;
        SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
        //SGtableloading('fa-plane', '', this);


    },
    "fnDrawCallback": function () {
        // in case your overlay needs to be put away automatically you can put it here
        var divid = this.closest('.dataTables_wrapper').attr('id');
        SGtableloadingRemove(divid)
    }
});
