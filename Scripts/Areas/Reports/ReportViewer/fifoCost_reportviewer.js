﻿$(document)
    .ready(function () {
        debugger;
        $("#tbl-report").removeClass("hidden");
        var urlSplitBySlash = window.location.href.split("/");
        var id = urlSplitBySlash[urlSplitBySlash.length - 1];

        $('#tbl-report-server')
            .dataTable({
                "deferRender": true,
                "bServerSide": true,
                "bSort": true,
                "stateSave": false,
                "bFilter": true,
                "aLengthMenu": dataTableMenuLength, //Global Variable in common.js
                "iDisplayLength": dataTableDisplayLength, //Global Variable in common.js
                "sAjaxSource": urlBulkFifoCostReport, //Get server side data from this Url
                "fnServerParams": function (aoData) {
                    aoData.push({ "name": "id", "value": id });
                },
                "bProcessing": true,
                "scrollX": true,
                
                "fnPreDrawCallback": function () {
                    // gather info to compose a message
                    var ob = this;
                    SGtableloading({ class: faIcon, message: 'Processing...', objct: ob });
                },
                "fnDrawCallback": function (settings) {
                    SGtableloadingRemove(divid);
                    TblpgntBtm();
                    rebuilttooltip();
                }
            });

        

        $("#in-progress").addClass("hidden");

        

    });
