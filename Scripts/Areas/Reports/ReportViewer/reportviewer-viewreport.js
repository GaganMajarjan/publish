﻿var filterParams = "";
 $(document).ready(function () {
   
    $(".sg-more-option").hide();
    //$(".search-slide").click(function () {

    //    $("i", this).toggleClass("fa-rotate-180");

    //    $(".sg-more-option").slideToggle("slow");

    //});

    initializeTableHeaders(reportViewerId);
    $(".reportFilterDateTime").datepicker();

    if (!$("#btnViewReport").length) {

        //initializeTableHeaders(reportViewerId);

        
    }

    function LoadDataTable(id) {
        var _exportUrl = urlExportReportData + "?id=" + id;
    $('#reportViewerDataTable').dataTable({
        "deferRender": true,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "iDisplayLength": 100,
        "destroy": true,
        "sAjaxSource": urlLoadDataTable + "?id=" + id + "&filterParams=" + filterParams,
        "bProcessing": true,
        "scrollX": true,
        "bSort": true,
        "dom": 'Blfrtip',
        "buttons": [{
            text: 'Export',
            action: function (e, dt, node, config) {

                //construct download all url with querystring
                var urlToDownload = _exportUrl;

                //redirect to the url to download all csv
                window.location = urlToDownload;

            }
        }],
        "fnPreDrawCallback": function () {
            // gather info to compose a message
            var ob = this;
            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
            //SGtableloading('fa-plane', '', this);


        },
        "fnDrawCallback": function () {
            // in case your overlay needs to be put away automatically you can put it here
            var divid = this.closest('.dataTables_wrapper').attr('id');
            SGtableloadingRemove(divid)
        }
    });
   
   
}

function initializeTableHeaders(id) {
   
    $.ajax({
        "dataType": 'json',
        "type": "GET",
        "url": urlGetTableSchema,
        "data": { id: id },
        
        "beforeSend": function () {
            // gather info to compose a message
            var ob = this;
            SGtableloading(faIcon, '', this);
            //SGtableloading('fa-plane', '', this);
        },
       

        "success": function (response) {
            SGtableloadingRemove();
            //data = "[" + data + "]";
            //data = JSON.parse(data);
            //console.log(data);
            //var jsonResultData = ConvertJsonToTable(data, "reportViewerDataTable", "", null);
            $('#reportViewerDataTableDiv').append('<table id="reportViewerDataTable"><thead><tr></tr></thead></table>');

            if (response && response.sColumns) {
                $.each(response.sColumns, function (key, value) {
                    $('#reportViewerDataTable thead tr').append('<th>' + value + '</th>');
                });
            }

            //$('#reportViewerDataTableDiv').html(jsonResultData);
            //$('#reportViewerDataTableDiv tbody tr').remove();

            LoadDataTable(id);
            var title = document.title;
            if (title.toLowerCase().indexOf('search result') > -1) {
                $('#reportViewerDataTable').dataTable().api().search(reportUserName).draw()
               
            }
        },

        "error": function (err) {
            alertmsg("red", "Report Viewer Columns not found.");
            SGtableloadingRemove();
        }
    });
    

}

if ($("#btnViewReport").length) {

    $('#btnViewReport').click(function () {

        var filterCount = $("#lblControlParamCount").val();
        filterParams = "";
        if (filterCount != null && filterCount !== "") {
            for (var i = 0; i < filterCount; i++) {

                filterParams += $("#textbox_" + i).val() + ",";
            }
           
            
        }
             
        TblpgntBtm();

        initializeTableHeaders(reportViewerId);

        setTimeout(customJqueryUI, 200);
        
    });
   
}
 });

