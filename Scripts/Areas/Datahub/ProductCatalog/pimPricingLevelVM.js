﻿//pricing viewmodel
var PIMPricingLevelViewModel = function (vm) {

    var self = this;
    self.PIMPricingId = ko.observable((vm != null && vm.PIMPricingId != null) ? vm.PIMPricingId : '');

    self.Id = ko.observable((vm != null && vm.Id != null) ? vm.Id : '');

    self.QuantityFrom = ko.observable(vm == null ? 1 : vm.QuantityFrom != null ? vm.QuantityFrom : '');
    //self.QuantityTo = ko.observable(vm == null ? 1 : vm.QuantityTo != null?vm.QuantityTo:'');
    self.GrossMarginPercent = ko.observable(vm == null ? /*(viewModel.MarginPercent())*/ 0 : vm.GrossMarginPercent != null ? (vm.GrossMarginPercent) : '')
                                     .extend({ numeric: decimal_places });

    self.GrossMarginAmount = ko.observable(vm == null ? /*(viewModel.MarginAmount())*/ 0 : vm.GrossMarginAmount != null ? (vm.GrossMarginAmount) : '')
                                         .extend({ numeric: decimal_places });

    self.PriceForPricingLevel = ko.observable(vm == null ? 0 : vm.Price != null ? (vm.Price) : '')
                                         .extend({ numeric: decimal_places });

    self.Price = ko.computed(function () {

        if (self.PriceForPricingLevel() == viewModel.SelectedCost()) {
            alertmsg("yellow", "Warning: Selling price for pricing level '{0}' and cost '{0}' cannot be same".format(self.PriceForPricingLevel(), viewModel.SelectedCost()));
        }

        return self.PriceForPricingLevel();
    });

    self.GrossMarginPercent.subscribe(function (val) {


        if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("GMP") > -1) {
            changeLevel = "";

            return;
        }
        if ((changeLevel3.match(/,/g) || []).length === 1 || changeLevel3.indexOf("GMP") > -1) {
            changeLevel3 = "";
            changeLevel = "";

            return;
        }

        val = parseFloat(val);

        if ($.isNumeric(val) === false) {
            return;
        }
        if (val === 0)
            return;
        if (parseFloat(val) >= 100) {
            alertmsg('red', 'Margin percent cannot be greater than or equal to 100');
            self.GrossMarginPercent(0);
            self.GrossMarginAmount(0);
            return;
        }
        var myCost = parseFloat(viewModel.SelectedCost());
        var myGrossMarginAmount = 0;
        var myGrossMarginPercent = parseFloat(val);
        var mySellingPrice = 0;

        // Validation
        if (myCost === 0 || myGrossMarginPercent === 0) //amount check not required
            return;

        // Calculation

        mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);
        myGrossMarginAmount = mySellingPrice - myCost;

        changeLevel += "GMP,";
        changeLevel3 += "GMP,";
        // Set values
        self.GrossMarginAmount(myGrossMarginAmount);

        self.PriceForPricingLevel(mySellingPrice);
        //////////////console.log('outside GrossMarginPercent');
    });

    self.PriceForPricingLevel.subscribe(function (val) {
        //////////////console.log('inside PriceForPricingLevel');


        if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("SEP") > -1) {
            changeLevel = "";

            return;
        }

        if ((changeLevel3.match(/,/g) || []).length === 1 || changeLevel3.indexOf("SEP") > -1) {
            changeLevel3 = "";
            changeLevel = "";

            return;
        }

        val = parseFloat(val);
        if ($.isNumeric(val) === false) {
            return;
        }
        if (val === 0)
            return;

        var myCost = parseFloat(viewModel.SelectedCost());
        var myGrossMarginPercent = parseFloat(self.GrossMarginPercent());
        var mySellingPrice = parseFloat(val);



        // Validation
        if (myCost === mySellingPrice) {
            return;
        }
        if (myCost === 0 || mySellingPrice === 0)
            return;

        // Calculation

        if (myCost !== 0) {
            myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
            //if (myGrossMarginPercent < 0 && costChanged === true) {
            //    marginNegativeDueToCostChange = true;
            //}
            //else {
            //    marginNegativeDueToCostChange = false;
            //}

        }

        changeLevel += "SEP,";
        changeLevel3 += "SEP,";
        // Set values
        //

        //if (!viewModel.IsMarginLocked()) {

            self.GrossMarginPercent(myGrossMarginPercent);

       // }
        self.GrossMarginAmount(mySellingPrice - myCost);

    });

    self.QuantityFrom.subscribe(function (val) {
        var quantityFrom = parseFloat(val);

        var currentIndex = 0;
        for (var i = 0; i < viewModel.PricingLevels().length; i++) {
            if (viewModel.PricingLevels()[i].QuantityFrom() === val) {
                currentIndex = i;
                break;
            }
        }

        if (currentIndex > 0) {

            var previousPricingLevel = viewModel.PricingLevels()[currentIndex - 1];
            var addedPricingLevel = viewModel.PricingLevels()[currentIndex];

            if (previousPricingLevel != null && previousPricingLevel.QuantityFrom() != null && previousPricingLevel.QuantityFrom() !== "") {
                if (parseFloat(previousPricingLevel.Quantity) < parseFloat(addedPricingLevel.QuantityFrom)) {
                    alertmsg('red', 'Previous Quantity (' + previousPricingLevel.QuantityFrom + ') should be less than current quantity (' + val.quantityFrom + '.');

                    self.QuantityFrom('');
                    return;
                }
            }

        }

    });

}
