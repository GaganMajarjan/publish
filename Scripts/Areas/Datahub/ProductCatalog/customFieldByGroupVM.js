﻿
var validateCustomField = function () {
    var isValid = true;

    //check if required custom field contains value
    $('.value-required-cf').each(function(i, val) {
        if (val.value === null || val.value === '') {
            //Note: In case resource is not available, use the following line for display
            alertmsg("red", valuRequiredCustomFieldMessage.format($(this).attr('data-name')));

            $("#{0}".format($(this).attr('id'))).focus();
            isValid = false;
        }
    });

    //check if ship-weight starts with .
    var shipWeight = $('[data-name="ship_weight"]');
    var shipWeightValue = shipWeight.val();
    if (shipWeightValue && shipWeightValue.startsWith(".")) {
        alertmsg("red", 'Yahoo requires {0} value to be started with 0'.format(shipWeight.parent().find('label').text()));
        isValid = false;
    }

    return isValid;
}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        var val = this.value;
        var selector = $('#' + this.name);
        if (selector.prop("type") === "checkbox") {
            val = selector.prop('checked');
        }

        //if (val === 'on') {
        //    val = '1';s
        //}
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(val || '');
        } else {
            o[this.name] = val || '';
        }
    });

    $("form input:checkbox").each(function () {
        var selector = $('#' + this.name);
        if (selector.hasClass("bit-cf-datatype")) {
            o[this.name] = +this.checked;
        }
        
    });
    return o;
};

//example of call: var x = getRegularFieldJson('.rf');
//it writes to knockoutJs observable from form object
var getReferencedCustomFieldToRegularField = function(selector, viewModel) {

    //var regularFieldsFromUi = $('.regular-field').serializeObject();

    var regularFieldsFromUi = $(selector).serializeObject();

    $.each(regularFieldsFromUi, function (key, val) {
        if (viewModel[key] != null) {
            viewModel[key](val);
        }
    });

    return regularFieldsFromUi;
}

//example of call: getCustomFieldJson(productCatalogVm.CustomTemplateGroup, '.cf-by-group .cf', true)
//example of call: getCustomFieldJson(productCatalogVm.CustomTemplateGroup, '.value-required-cf', false)
//returns: customTemplates list of changed custom fields only
var getCustomFieldJson = function (customTemplateGroup, selector, returnChangedOnly) {
    customTemplateGroup = customTemplateGroup || {};
    var customFieldsFromUi = $(selector).serializeObject();
    var customTemplates = [];

    $.each(customFieldsFromUi, function (key, val) {
        var selector = $('#' + key);
        var oldVal = selector.attr('data-oldvalue');
        var isDirty = false;
        var FieldReferenceSysInformationSchemaId = null;
        var FieldReferenceSysInformationSchemaIdKit = null;
        

        if (val !== oldVal) {
            isDirty = true;
        }
        if (viewModel.Id() <= 0 || viewModel.Id() === "") {
            if (selector.attr('data-name') === 'FulfillmentCenter') {
                isDirty = true;
            }
        }
        if (isDirty) {

            if (selector.attr('data-name') === 'FulfillmentCenter') {
                //if (val === " " || val === null) {DaniUpdate1212020
                    if (val === null) {
                    //val = " ";
                    val = "NY";
                }
            }
            if (selector.attr('data-name') === 'TaxCode') {
                if (val === "" || val === null) {
                    val = "";
                }
                else {
                    val = val.trim();
                }
            }

        if (selector.attr('data-name') === 'image') {
                if (val === "") {
                    val = "https://sep.yimg.com/ty/cdn/stylespilotshop/image_coming_soon.jpg";
                }
            }
          
       
        if (selector.attr('data-name') === 'Barcode') {

                if (val.trim() === "" || val === null) {
                    /* changes 6242020*/
                    // val = "out of stock";
                    val = viewModel.SkyGeekPN();
                }
            } 
        if (selector.attr('data-FieldReferenceSysInformationSchemaId') !== null && selector.attr('data-FieldReferenceSysInformationSchemaId') !== "" && selector.attr('data-FieldReferenceSysInformationSchemaId') !== undefined) {
            FieldReferenceSysInformationSchemaId = selector.attr('data-FieldReferenceSysInformationSchemaId');
            
        }
        if (selector.attr('data-FieldReferenceSysInformationSchemaIdKit') !== null && selector.attr('data-FieldReferenceSysInformationSchemaIdKit') !== "" && selector.attr('data-FieldReferenceSysInformationSchemaIdKit') !== undefined) {
            FieldReferenceSysInformationSchemaIdKit = selector.attr('data-FieldReferenceSysInformationSchemaIdKit');
           
        }
            var customFieldVm = new CustomFieldManagerViewModel();
            var id = selector.attr('id');
            
            //customFieldVm.Id = id;
            customFieldVm.CustomField = id; //this is the customFieldId
            customFieldVm.FieldValue = (val !== null || val !== "")? val : "";
            customFieldVm.FieldValueIsDirty = true; //this has changed, so set IsDirty
            
            //add values to bypass modelstate validation
            var customFields = {
                name: 'not null',
                fieldDataTypeSdvId: '0',
                customFieldTemplateId: '0',
                fieldDescription: '',
                fieldAlias: selector.attr('data-name'),
                description: '',
                fieldReferenceSysInformationSchemaId: FieldReferenceSysInformationSchemaId !== null ? FieldReferenceSysInformationSchemaId:"",
                fieldReferenceSysInformationSchemaIdKit: FieldReferenceSysInformationSchemaIdKit !== null ? FieldReferenceSysInformationSchemaIdKit:""
            };

            customFieldVm.CustomFields = customFields;
            customTemplates.push(customFieldVm);
           
        }
    });

    //set changed properties to viewModel
    if (returnChangedOnly == true)
        return customTemplates;
    else
        return customTemplateGroup;
}

var CustomFieldManagerGroupViewModel = function() {
    var self = this;

    self.Id = ko.observable();
    self.Name = ko.observable();
    self.Description = ko.observable();
    self.CustomTemplate = ko.observableArray([]); // new CustomFieldManagerViewModel
    self.ColumnPerRow = ko.observable();

}

var CustomFieldManagerViewModel = function()
    //{(id,customField,fieldValue,fieldAlias,fieldValueBoolean,allowedValuesSdtId,isRequired,isReadonly,isNullable,fieldValueIsDirty,customFields,allowedValuesList,fieldInputTypeSdvId,fieldInputType,fieldDataTypeSdvId,fieldDataType,customFieldClass) 
{
    var self = this;

    self.Id = ko.observable();
    self.CustomField = ko.observable();
    self.FieldValue = ko.observable();
    self.FieldAlias = ko.observable();
    self.FieldValueBoolean = ko.observable();
    self.AllowedValuesSdtId = ko.observable();
    self.IsRequired = ko.observable();
    self.IsReadOnly = ko.observable();
    self.IsNullable = ko.observable();
    self.FieldValueIsDirty = ko.observable();
    self.CustomFields = ko.observable(); //CustomFieldViewModel
    self.AllowedValuesList = ko.observableArray();
    self.FieldInputTypeSdvId = ko.observable();
    self.FieldInputType = ko.observable();
    self.FieldDataTypeSdvId = ko.observable();
    self.FieldDataType = ko.observable();
    self.CustomFieldClass = ko.observable();
}
