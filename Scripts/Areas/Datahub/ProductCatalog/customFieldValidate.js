﻿function validateYahooId() {
    var yahooId = $('input[data-name="YahooId"],textarea[data-name="YahooId"]').val();
    var platformQualifier = $('#PlatformQualifier').val();
    if (typeof platformQualifier !== 'undefined' && platformQualifier !== null && platformQualifier.indexOf('Yahoo') >= 0
        && typeof yahooId !== 'undefined' && yahooId !== null && yahooId === "") {
        var fieldAlias = $('input[data-name="YahooId"],textarea[data-name="YahooId"]').parent().find('label').text();
        alertmsg("red", "{0} cannot be blank when {1} is a PlatformQualifier".format(fieldAlias, "Yahoo"));
        return false;
    }

    var regexp = /^[a-zA-Z0-9-]+$/;
    var check = yahooId;
    if (check != null && check != '' && check.search(regexp) == -1) {
        var fieldAliasRegex = $('input[data-name="YahooId"],textarea[data-name="YahooId"]').parent().find('label').text();
        alertmsg("red", "{0} contains invalid characters".format(fieldAliasRegex, "Yahoo"));
        return false;
    }
   
    return true;
}

function validateItemPoints() {
    //var itemPoints = $('[data-name="ItemPoints"]').val();
    //var dimensions = $('[data-name="dimensions"]').val();
    //var splitDimensions = [];
    //var calculatedItemPoints;
    //if (dimensions) {
    //    splitDimensions = dimensions.split(' ');
    //    calculatedItemPoints = parseFloat(splitDimensions[0]) *
    //        parseFloat(splitDimensions[1]) *
    //        parseFloat(splitDimensions[2]);
    //}
    //if (itemPoints && dimensions && splitDimensions.length == 3 && itemPoints != calculatedItemPoints) {
    //    alertmsg("red", "ItemPoints '{0}' is invalid. Expected value is '{1}'.".format(itemPoints, calculatedItemPoints));
    //    return false;
    //}

    return true;
}

function validateCommodityCode() {
    var scheduleB = $('[data-name="schedule_b"]').val();
    var scheduleBalias = $('[data-name="schedule_b"]').parent().find('label')[0].innerHTML;
    var commodityCode = $('[data-name="CommodityCode"]').val();
    var commodityCodeAlias = $('[data-name="CommodityCode"]').parent().find('label')[0].innerHTML;

    if (scheduleB && commodityCode && scheduleB.substring(0, 4) != commodityCode) {
        alertmsg("red", "{0} '{1}' is invalid. Expected value is '{2}' (calculated from {3})".format(commodityCodeAlias, commodityCode, scheduleB.substring(0, 4), scheduleBalias));
        return false;
    }

    return true;
}


// SG-771 validation for product_url and YahooId
function validateYahooIdWithProductUrl()
{
    var productUrl = $('[data-name="product_url"]');
    var yahooId = $('[data-name="YahooId"]');
   // var mobileLink = $('[data-name="MobileLink"]');
    var imageUrl = $('[data-name="image"]');

    if (productUrl && yahooId)
    {

        if (yahooId.val() !== "") {
            var ExpectedProductValue = "https://www.skygeek.com/{0}.html".format(yahooId.val());

            if (productUrl.val() !== ExpectedProductValue) {
               alertmsg('red', ' Product-url \'' + productUrl.val() + '\' is invalid. Expected value is \'' + ExpectedProductValue + '\' (calculated from YahooId)');
                return false;
            }
        }
    }

    //if (mobileLink && yahooId)
    //{
    //    if (yahooId.val() !== "") {
    //        var ExpectedValue = "http://m.skygeek.com/{0}.html".format(yahooId.val()).toLowerCase();

    //        if (mobileLink.val().toLowerCase() !== ExpectedValue) {
    //            alertmsg('red', ' Mobile Link \'' + mobileLink.val() + '\' is invalid. Expected value is \'' + ExpectedValue + '\' (calculated from YahooId)');
    //            return false;
    //        }
    //    }

    //}
    //if (imageUrl && yahooId) {
    //    if (yahooId.val() !== "") {
    //        var ExpectedValue = "http://feeds3.yourstorewizards.com/4481/images/full/{0}.jpg".format(yahooId.val());

    //        if (imageUrl.val() !== ExpectedValue) {
    //            alertmsg('red', ' Image Url \'' + imageUrl.val() + '\' is invalid. Expected value is \'' + ExpectedValue + '\' (calculated from YahooId)');
    //            return false;
    //        }
    //    }

    //}

    return true;

}