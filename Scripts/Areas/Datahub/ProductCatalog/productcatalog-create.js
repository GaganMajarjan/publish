﻿/***
changeLevels List:
 COST = cost
 MAP= Margin Percent
 MAA = Margin Amount
 SAP = Sale Price
 GMP = Gross Margin Percent
 GMA = Gross Margin Amount
 SEP = Selling Price
***/

$(document).ready(function () {
    $("#ProductCategoryId").select2();
    if (vpName !== "" && vpName !== null) {
        document.getElementById('Name').value = vpName;

    }
    if (vpPartNumber !== "" && vpPartNumber !== null) {
        document.getElementById('LocalSku').value = vpPartNumber;
    }
    if (vpDescription !== "" && vpDescription !== null) {
        vpDescription = decodeHtml(vpDescription);
        document.getElementById('ShortDescription').value = vpDescription;
    }
});
var changeLevel = "";
var changeLevel2 = "";
var changeLevel3 = "";
//Json parse viewbag elements
//decode viewModel
productCatalogVm = productCatalogVm.replace(/\\"/g, '"').replace(/"/g, '\\"');
productCatalogVm = decodeHtml(productCatalogVm);

//Parse viewModel
if (productCatalogVm != null && productCatalogVm != "") {
    //console.log(productCatalogVm);
    productCatalogVm = fixJsonSingleQuote(productCatalogVm);
    ReplaceSingle2Quote(productCatalogVm);
} else
    productCatalogVm = null;

//parse value required custom fields 
if (valueRequiredCustomFieldsViewBag)
    valueRequiredCustomFieldsViewBag = fixJsonQuote(valueRequiredCustomFieldsViewBag);
else
    valueRequiredCustomFieldsViewBag = null;

//parse read only fields
if (customFieldsReadOnlyViewBag)
    customFieldsReadOnlyViewBag = fixJsonQuote(customFieldsReadOnlyViewBag);
else
    customFieldsReadOnlyViewBag = null;

unitOfMeasureListViewBag = JSON.parse(unitOfMeasureListViewBag);

var unitOfMeasureList = [];
$.each(unitOfMeasureListViewBag, function (index, val) {
    unitOfMeasureList.push(val.TargetUOM);
});

//parse additinal customfields viewbag
customFieldsAdditionalViewBag = fixJsonQuote(customFieldsAdditionalViewBag);

//push additional customfields to list
var customFieldsAdditionalViewBagList = [];
$.each(customFieldsAdditionalViewBag, function (index, val) {
    customFieldsAdditionalViewBagList.push({
        Name: val.Name,
        Id: val.Id,
        DataType: val.DataType,
        AllowedValuesSdtId: val.AllowedValuesSdtId,
        DefaultValue: val.DefaultValue
    });
});

//push readOnly Fields
var customFieldsReadOnlyViewBagList = [];
$.each(customFieldsReadOnlyViewBag, function (index, val) {
    customFieldsReadOnlyViewBagList.push({
        Name: val.Name,
        Id: val.Id,
        DataType: val.DataType,
        AllowedValuesSdtId: val.AllowedValuesSdtId
    });
});

//parse required customfields viewbag
customFieldsRequiredViewBag = fixJsonQuote(customFieldsRequiredViewBag);

//push required customfields to list
var customFieldsRequiredViewBagList = [];
$.each(customFieldsRequiredViewBag, function (index, val) {
    customFieldsRequiredViewBagList.push({
        Name: val.Name,
        Id: val.Id,
        DataType: val.DataType,
        AllowedValuesSdtId: val.AllowedValuesSdtId,
        DefaultValue: val.DefaultValue
    });
});

//Concat required and additional list to
var customFieldsGenerateDomList = customFieldsRequiredViewBagList.concat(customFieldsAdditionalViewBagList);

// Suppliers list
var supplierList = [];
var primarySupplierIdIndex;

if (supplierListViewBag !== "[]") {


    supplierListViewBag = decodeHtml(supplierListViewBag);
    supplierListViewBag = fixJsonSingleQuote(supplierListViewBag);
    ReplaceSingle2Quote(supplierListViewBag);

    $.each(supplierListViewBag, function (index, val) {

        supplierList.push({
            vpcId: val.vpcId,
            value: val.vendorId, //vendorId: val.vendorId,
            name: val.Name, //vendorName: val.Name,
            nameCost: val.VendorName + '(Cost: ' + val.Cost + ')', //vendorName: val.Name,
            VPPRCost: val.Cost,
            OrderBy: val.OrderBy,
            OrderByText: val.OrderBy != null ? (val.OrderBy.toUpperCase() === 'P' ? 'by the piece' : val.OrderBy.toUpperCase() === 'L' ? 'by the lot' : '') : '',//: val.OrderBy.toUpperCase() === 'Q' ? 'by the piece in fixed lots'
            QuantityPerLot: val.QuantityPerLot,
            CostPerLot: val.CostPerLot,
            MinimumOrderUnit: val.MinimumOrderUnit,
            MinimumOrderUnitText: val.MinimumOrderUnit != null ? (val.MinimumOrderUnit.toUpperCase() === 'P' ? 'by the piece' : val.MinimumOrderUnit.toUpperCase() === 'L' ? 'by the lot' : '') : '',//: val.MinimumOrderUnit.toUpperCase() === 'Q' ? 'by the piece in fixed lots'
            MinimumOrderQuantity: val.MinimumOrderQuantity,
            VendorPartNumber: val.VendorPartNumber,
            VendorName: val.VendorName
        });

        if (productCatalogVm != null && productCatalogVm.SupplierId != null && val.vendorId === productCatalogVm.SupplierId) {
            primarySupplierIdIndex = index;
        }

    });

}

//Cost
var originalCost = (productCatalogVm != null && productCatalogVm.Cost != null) ? productCatalogVm.Cost : null;
var originalSalePrice = (productCatalogVm != null && productCatalogVm.Price != null) ? productCatalogVm.Price : null;

//fuctions defined

//construct control elements as per data type
function ConstructControlForDataType(currentIndex, dataType, cssClass, hasEmptyValue, allowedValueSdtId, defaultValue) {
    //console.log('top');

    if (dataType === "DATETIME" || dataType === "DATE") {

        $("#itemTextbox_" + currentIndex)
            .show()
            .attr("class", "form-control " + cssClass)
            .datepicker();

        $("#itemCheckbox_" + currentIndex).hide();
        if ($("#drop_" + currentIndex).data("select2")) {
            $("#drop_" + currentIndex).select2("destroy");
            $("#drop_" + currentIndex).hide();

        } else {
            $("#drop_" + currentIndex).hide();

        }

        if (hasEmptyValue === "true") {

            $("#itemTextbox_" + currentIndex)
                .val("").change();
            if (defaultValue) {
                $("#itemTextbox_" + currentIndex).val(defaultValue).change();
            }
        }

    }
    else if (dataType === "BIT") {
        $("#itemCheckbox_" + currentIndex).show();
        $("#itemTextbox_" + currentIndex).hide();
        if ($("#drop_" + currentIndex).data("select2")) {
            $("#drop_" + currentIndex).select2("destroy");
            $("#drop_" + currentIndex).hide();

        } else {
            $("#drop_" + currentIndex).hide();

        }

    } else {

        if (allowedValueSdtId != null && allowedValueSdtId !== 0 && $.isNumeric(allowedValueSdtId)) {

            $("#itemCheckbox_" + currentIndex).hide();
            $("#itemTextbox_" + currentIndex).hide();
            $("#drop_" + currentIndex)
                .show();

            $.ajax({
                url: urlGetStaticDataValues,
                type: 'POST',
                data: { "staticDataTypeId": allowedValueSdtId },
                dataType: "json",
                beforeSend: function () {
                    SGloading('Loading');
                },
                success: function (result) {

                    if (result === "NO_DATA") {
                        alertmsg("red", "No options exists for given static data type, please add statice values first.", 8000);
                        return;
                    }
                    var jsonResult = $.parseJSON(result);

                    //Create a new select element and then append all options. Once loop is finished, append it to actual DOM object.
                    //This way we'll modify DOM for only one time!

                    var selectOptions = $('<select>');
                    selectOptions.append(
                        $('<option></option>').val("").html(pleaseSelectText));

                    $.each(jsonResult, function (index, val) {
                        selectOptions.append(
                            $('<option></option>').val(val.Id).html(val.Name)
                        );
                    });

                    //clear first before append
                    $("#drop_" + currentIndex).html("");

                    $("#drop_" + currentIndex).append(selectOptions.html());

                    if (defaultValue) {
                        viewModel.CustomTemplatesAll()[currentIndex].AllowedValuesSdtId(defaultValue);
                    }

                    //get the selected value for dropdown list
                    var selectedData = viewModel.CustomTemplatesAll()[currentIndex];
                    var returnedData = $.grep(productCatalogVm.CustomTemplates, function (element, index) {
                        return element.CustomField === selectedData.CustomField();
                    });

                    if (productCatalogVm.CustomTemplates[currentIndex] != null) {

                        //var dropSelectedValue = productCatalogVm.CustomTemplates[currentIndex].FieldValue;
                        if (typeof returnedData !== 'undefined') {
                            if (returnedData !== null && returnedData.length > 0) {
                                var dropSelectedValue = returnedData[0].FieldValue;

                                if (dropSelectedValue != null && dropSelectedValue !== "" && dropSelectedValue !== 0) {

                                    viewModel.CustomTemplatesAll()[currentIndex].AllowedValuesSdtId(dropSelectedValue);
                                }
                            }
                        }
                    }

                    $("#drop_" + currentIndex).select2({ width: '100%' });

                },
                complete: function () {
                    SGloadingRemove();
                },
                error: function (err) {
                    ////////console.log(err);
                }
            });
        } else {


            $("#itemTextbox_" + currentIndex)
                .datepicker("destroy")
                .show()
                .attr("class", "form-control " + cssClass);
            $("#itemCheckbox_" + currentIndex).hide();
            if ($("#drop_" + currentIndex).data("select2")) {
                $("#drop_" + currentIndex).select2("destroy");
                $("#drop_" + currentIndex).hide();

            } else {
                $("#drop_" + currentIndex).hide();

            }


            if (hasEmptyValue === "true") {
                $("#itemTextbox_" + currentIndex)
                    .val("").change();

                if (defaultValue) {
                    $("#itemTextbox_" + currentIndex)
                        .val(defaultValue).change();
                }
            }


        }
    }


}

//get data type of elements
function RenderElementWithDataType(customFieldId, isForAll) {

    if (isForAll === "true") {
        for (var i = 0; i < viewModel.CustomTemplatesAll().length; i++) {

            customFieldId = viewModel.CustomTemplatesAll()[i].CustomField();
            var returnedData = $.grep(customFieldsGenerateDomList, function (element, index) {
                return element.Id === customFieldId;
            });
            //var returnedData = $.grep(customFieldsAdditionalViewBag, function (element, index) {
            //    return element.Id === customFieldId;
            //});

            if (returnedData.length > 0) {
                var selectedDataType = returnedData[0].DataType;
                var selectedAllowedValueSdtId = returnedData[0].AllowedValuesSdtId;
                var defaultValue = returnedData[0].DefaultValue;

                if (selectedDataType == null)
                    selectedDataType = "VARCHAR(MAX)";
                ConstructControlForDataType(i, selectedDataType, selectedDataType.replace(/ *\([^)]*\) */g, "").toLowerCase() + "DataType", "false", selectedAllowedValueSdtId, defaultValue);
            }

        }

    } else {

        var currentIndex = 0;
        for (var i = 0; i < viewModel.CustomTemplatesAll().length; i++) {
            if (viewModel.CustomTemplatesAll()[i].CustomField() === customFieldId) {
                currentIndex = i;
                break;
            }
        }

        //Search for DataType 
        var returnedData = $.grep(customFieldsGenerateDomList, function (element, index) {
            return element.Id === customFieldId;
        });

        //////////////console.log(returnedData.length);

        if (returnedData.length > 0) {
            var selectedDataType = returnedData[0].DataType;
            var selectedAllowedValueSdtId = returnedData[0].AllowedValuesSdtId;
            var defaultValue = returnedData[0].DefaultValue;
            ConstructControlForDataType(currentIndex, selectedDataType, selectedDataType.replace(/ *\([^)]*\) */g, "").toLowerCase() + "DataType", "true", selectedAllowedValueSdtId, defaultValue);
            viewModel.CustomTemplatesAll()[currentIndex].FieldAlias(returnedData[0].Name);
        }
    }

}

//viewmodels
//product catalog viewmodel
var ProductCatalogViewModel = function (productCatalogVm) {
    var self = this;
    self.StoreId = ko.observable((productCatalogVm != null && productCatalogVm.StoreId != null) ? productCatalogVm.StoreId : '');
    // Properties - General
    self.Id = ko.observable((productCatalogVm != null && productCatalogVm.Id != null) ? productCatalogVm.Id : '');
    self.Name = ko.observable((productCatalogVm != null && productCatalogVm.Name != null) ? productCatalogVm.Name : '');
    self.LocalSku = ko.observable((productCatalogVm != null && productCatalogVm.LocalSku != null) ? productCatalogVm.LocalSku : '');
    self.Location = ko.observable((productCatalogVm != null && productCatalogVm.Location != null) ? productCatalogVm.Location : '');
    self.ShortDescription = ko.observable((productCatalogVm != null && productCatalogVm.ShortDescription != null) ? productCatalogVm.ShortDescription : '');
    self.IsDisabled = ko.observable((productCatalogVm != null && productCatalogVm.IsDisabled != null) ? productCatalogVm.IsDisabled : false);

    // Properties - Pricing
    self.Cost = ko.observable((productCatalogVm != null && productCatalogVm.Cost != null) ? productCatalogVm.Cost : 0)
        .extend({ numeric: decimal_places });
    self.IsCostLocked = ko.observable((productCatalogVm != null && productCatalogVm.IsCostLocked != null) ? productCatalogVm.IsCostLocked : false);
    self.MarginPercent = ko.observable((productCatalogVm != null && productCatalogVm.MarginPercent != null) ? productCatalogVm.MarginPercent : '')
        .extend({ numeric: decimal_places });

    self.IsMarginLocked = ko.observable((productCatalogVm != null && productCatalogVm.IsMarginLocked != null) ? productCatalogVm.IsMarginLocked : false);
    self.MarginAmount = ko.observable((productCatalogVm != null && productCatalogVm.MarginAmount != null) ? productCatalogVm.MarginAmount : '')
        .extend({ numeric: decimal_places });

    self.Price = ko.observable((productCatalogVm != null && productCatalogVm.Price != null) ? productCatalogVm.Price : '')
        .extend({ numeric: decimal_places });

    self.IsSalePriceLocked = ko.observable((productCatalogVm != null && productCatalogVm.IsSalePriceLocked != null) ? productCatalogVm.IsSalePriceLocked : false);
    self.IsStoreEnabled = ko.observable(isStoreEnabled);
    self.PricingLevels = ko.observableArray([]);

    self.CustomFieldsRequired = ko.observableArray([]);
    self.CustomFieldsAdditional = ko.observableArray([]);
    self.CustomFieldsReadOnly = ko.observableArray([]);
    self.CustomTemplates = ko.computed(function () {
        return self.CustomFieldsRequired().concat(self.CustomFieldsAdditional());
    });
    self.CustomTemplatesAll = ko.computed(function () {
        return self.CustomFieldsRequired().concat(self.CustomFieldsReadOnly()).concat(self.CustomFieldsAdditional());
    });
    self.CountReqAndReadonly = ko.computed(function () {
        return (parseInt(self.CustomFieldsRequired().length) + parseInt(self.CustomFieldsReadOnly().length));
    });
    self.CountReq = ko.computed(function () {
        return (parseInt(self.CustomFieldsRequired().length));
    });
    //self.Count = ko.computed(function () {
    //    return self.CustomFieldsRequired().length + self.CustomFieldsReadOnly().length + self.CustomFieldsAdditional().length;
    //});
    self.IsCostPriceEqual = ko.observable(false);
    self.IsByPassCheckCostPrice = ko.observable(false);
    self.PlatformQualifier = ko.observable((productCatalogVm != null && productCatalogVm.PlatformQualifier != null) ? productCatalogVm.PlatformQualifier : '');
    self.PlatformQualifiers = ko.observableArray((productCatalogVm != null && productCatalogVm.PlatformQualifiers != null) ? productCatalogVm.PlatformQualifiers : []);

    // Properties - Identifiers
    self.ISBN = ko.observable((productCatalogVm != null && productCatalogVm.ISBN != null) ? productCatalogVm.ISBN : '');
    self.NSN = ko.observable((productCatalogVm != null && productCatalogVm.NSN != null) ? productCatalogVm.NSN : '');
    self.UPC = ko.observable((productCatalogVm != null && productCatalogVm.UPC != null) ? productCatalogVm.UPC : '');
    self.EAN = ko.observable((productCatalogVm != null && productCatalogVm.EAN != null) ? productCatalogVm.EAN : '');
    self.MPN = ko.observable((productCatalogVm != null && productCatalogVm.MPN != null) ? productCatalogVm.MPN : '');
    self.ASIN = ko.observable((productCatalogVm != null && productCatalogVm.ASIN != null) ? productCatalogVm.ASIN : '');

    // Properties - Dimensions
    self.Weight = ko.observable((productCatalogVm != null && productCatalogVm.Weight != null) ? productCatalogVm.Weight : '');
    self.Height = ko.observable((productCatalogVm != null && productCatalogVm.Height != null) ? productCatalogVm.Height : '');
    self.Width = ko.observable((productCatalogVm != null && productCatalogVm.Width != null) ? productCatalogVm.Width : '');
    self.Length = ko.observable((productCatalogVm != null && productCatalogVm.Length != null) ? productCatalogVm.Length : '');
    self.ProductCategoryId = ko.observable((productCatalogVm != null && productCatalogVm.ProductCategoryId != null) ? productCatalogVm.ProductCategoryId : '');
    self.ProductCategoryIds = ko.observableArray((productCatalogVm != null && productCatalogVm.ProductCategoryIds != null) ? productCatalogVm.ProductCategoryIds : []);

    self.isSetPrimary = ko.observable(false);
    self.UOM = ko.observable((productCatalogVm != null && productCatalogVm.UOM != null) ? productCatalogVm.UOM : '');
    self.supplierId = ko.observable();
    self.supplierPartNumber = ko.observable();
    self.supplierName = ko.observable();
    self.url = ko.observable('');
    self.urlTitle = ko.observable('');

    self.urlClick = function () {
        var fullUrl = urlEditVendorProductCatalog + '&id=' + self.Id() + '&returnUrl=' + window.location.href;
        window.location = fullUrl;
    }
    self.supplierId.subscribe(function (val) {
        if (val == null) {
            self.supplierPartNumber('');
            self.url('');
            self.urlTitle('');
            $('#view-vendor-product-catalog').hide();
            return;
        }
        $('#view-vendor-product-catalog').show();

        self.supplierPartNumber(val.VendorPartNumber);

        self.url(urlEditVendorProductCatalog + '/' + val.vpcId + '?returnUrl=' + window.location.href);
        self.urlTitle('Vendor product catalog details for ' + val.nameCost);
        self.supplierName(val.name);

        rebuilttooltip();
    });

    // Properties - Clearance (sale)
    self.hasSalePriceForClearance = ko.observable((productCatalogVm != null && productCatalogVm.HasSalePriceForClearance != null) ? productCatalogVm.HasSalePriceForClearance : false);
    self.SalePrice = ko.observable((productCatalogVm != null && productCatalogVm.SalePrice != null) ? productCatalogVm.SalePrice : '')
        .extend({ numeric: decimal_places });
    self.salePriceForClearanceMarginPercent = ko.observable((productCatalogVm != null && productCatalogVm.SalePrice != null) ? productCatalogVm.SalePrice : '')
        .extend({ numeric: decimal_places });

    self.hasSalePriceForClearance.subscribe(function (val) {
        if (val === false) {
            self.SalePrice('0');
            self.salePriceForClearanceMarginPercent('0');
        }
        else {
            if (self.salePriceForClearanceMarginPercent() === 0 && self.SalePrice() === 0) {
                self.SalePrice(self.Cost());
            }
        }
        changeLevel2 = "";
    });

    // Create inventory supplier
    self.createInventorySupplierId = ko.observable();

    // Computed
    self.ShowPricing = ko.computed(function () {
        return (self.Name() && self.LocalSku()) ? true : false;
    });

    // Functions/Events
    self.addPricingLevel = function () {

        for (var i = 0; i < self.PricingLevels().length; i++) {
            var quantityFrom = self.PricingLevels()[i].QuantityFrom();
            //var quantityTo = self.PricingLevels()[i].QuantityTo();
            if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                alertmsg('red', 'Quantity should be greater than 1');
                return;
            }

            var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
            var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

            if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                alertmsg('red', 'Margin %  should be greater than 0');
                return;
            }

            if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                alertmsg('red', 'Margin $  should be greater than 0');
                return;
            }


        }
        if (self.PricingLevels().length > 1) {
            var quantityFromCurrent = self.PricingLevels()[self.PricingLevels().length - 1].QuantityFrom();
            var quantityFromPrevious = self.PricingLevels()[self.PricingLevels().length - 2].QuantityFrom();


            if (quantityFromPrevious != null && quantityFromCurrent != null && parseFloat(quantityFromPrevious) >= parseFloat(quantityFromCurrent)) {
                alertmsg('red', 'Current "Quantity" should be greater than the previous "Quantity"');
                return;
            }
        }



        self.PricingLevels.push(new PIMPricingLevelViewModel());
        rebuilttooltip();
    };

    self.addCustomField = function () {

        self.CustomFieldsAdditional.push(new CustomFieldManagerViewModel());
        $(".customFieldSelect2").select2({ width: '100%' });

    };

    self.removeAdditionalCustomField = function (customTemplate) {
        self.CustomFieldsAdditional.remove(customTemplate);

    };

    self.removePricingLevel = function (pricingLevel) {

        self.PricingLevels.remove(pricingLevel);
    };

    self.hasCustomFieldTemplateValue = ko.observable(true);
    self.hasCustomFieldValue = ko.observable(false);

    self.save = function (form) {

        if (showAllProductCatalogTabs !== null && showAllProductCatalogTabs === "True") {

            for (var i = 0; i < self.PricingLevels().length; i++) {

                var quantityFrom = self.PricingLevels()[i].QuantityFrom();
                //var quantityTo = self.PricingLevels()[i].QuantityTo();
                if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                    alertmsg('red', 'Please enter Quantity greater than one');
                    return;
                }

                var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
                var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

                if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                    alertmsg('red', 'Margin %  should be greater than 0');
                    return;
                }

                if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                    alertmsg('red', 'Margin $  should be greater than 0');
                    return;
                }

                //if (i < self.PricingLevels().length - 1) {

                //    var prevQuantityFrom = self.PricingLevels()[i].QuantityFrom();
                //    var currQuantityFrom = self.PricingLevels()[i + 1].QuantityFrom();

                //    if (prevQuantityFrom != null && currQuantityFrom != null && parseFloat(prevQuantityFrom) >= parseFloat(currQuantityFrom)) {
                //        alertmsg('red', 'Please enter the values for Quantity in ascending order. Save Cancelled.');
                //        return;
                //    }
                //}
            }
        }

        //validation check for custom fields
        var customFieldList = [];

        //validation for the value required custom fields
        for (var i = 0; i < self.CustomFieldsRequired().length; i++) {
            var customFieldSelected = self.CustomFieldsRequired()[i];
            customFieldList.push(customFieldSelected.CustomField());

            var customFieldFromViewBag = $.grep(customFieldsRequiredViewBag, function (element, index) {
                return element.Id === customFieldSelected.CustomField();
            });

            if (!customFieldSelected.FieldValue()) {

                var customFieldDataType = "VARCHAR(MAX)";
                if (customFieldFromViewBag.length > 0) {
                    var selectedDataType = customFieldFromViewBag[0].DataType;
                    if (selectedDataType)
                        customFieldDataType = selectedDataType;
                }
                if (customFieldDataType === "BIT" || customFieldDataType === "bit") {
                    self.CustomFieldsRequired()[i].FieldValue(self.CustomFieldsRequired()[i].FieldValueBoolean());
                }
                else if (customFieldSelected.AllowedValuesSdtId()) {
                    self.CustomFieldsRequired()[i].FieldValue(self.CustomFieldsRequired()[i].AllowedValuesSdtId());
                }

            }

            if (customFieldSelected != null && (customFieldSelected.CustomField() == null || $.isNumeric(customFieldSelected.CustomField()) === false || customFieldSelected.CustomField() === 0)) {

                // found existing custom field with NullOrEmpty 
                alertmsg('red', customFieldNameRequiredMessage);
                return;
            }

            if (valueRequiredCustomFieldsViewBag.indexOf(customFieldSelected.CustomField()) > -1 && $.trim(self.CustomFieldsRequired()[i].FieldValue()) === "") {

                if (customFieldFromViewBag.length > 0) {
                    alertmsg("red", valuRequiredCustomFieldMessage.format(customFieldFromViewBag[0].Name));
                }
                return;
            }

        }

        //validation for the additional custom fields
        for (var i = 0; i < self.CustomFieldsAdditional().length; i++) {
            var customTemplate = self.CustomFieldsAdditional()[i];
            customFieldFromViewBag = $.grep(customFieldsAdditionalViewBag, function (element, index) {
                return element.Id === customTemplate.CustomField();
            });

            if (customTemplate != null && (customTemplate.CustomField() == null || $.isNumeric(customTemplate.CustomField()) === false || customTemplate.CustomField() === 0)) {

                //found existing custom field with NullOrEmpty 
                alertmsg('red', customFieldNameRequiredMessage);
                return;
            }

            if (!customTemplate.FieldValue()) {

                customFieldDataType = "VARCHAR(MAX)";
                if (customFieldFromViewBag.length > 0) {
                    selectedDataType = customFieldFromViewBag[0].DataType;
                    if (selectedDataType)
                        customFieldDataType = selectedDataType;
                }
                if (customFieldDataType === "BIT" || customFieldDataType === "bit") {
                    self.CustomFieldsAdditional()[i].FieldValue(self.CustomFieldsAdditional()[i].FieldValueBoolean());
                }
                else if (customTemplate.AllowedValuesSdtId()) {
                    self.CustomFieldsAdditional()[i].FieldValue(self.CustomFieldsAdditional()[i].AllowedValuesSdtId());
                }

            }


            if (!self.CustomFieldsAdditional()[i].FieldValue()) {

                if (customFieldFromViewBag.length > 0) {
                    alertmsg("red", valueRequiredOtherCustomFieldMessage.format(customFieldFromViewBag[0].Name));
                }
                return;
            }
        }

        //validate for the required custom fields
        if (customFieldsRequiredViewBag) {
            //////console.log(customFieldList);

            if (!customFieldList) {
                alertmsg("red", fillUpAllRequiredCustomFields);
                return;
            }

            var isRequiredFieldMapped = true;
            $.each(customFieldsRequiredViewBag, function (index, val) {
                if (customFieldList.indexOf(val.Id) === -1) {
                    isRequiredFieldMapped = false;

                }

            });

            if (isRequiredFieldMapped == false) {
                alertmsg("red", fillUpAllRequiredCustomFields);
                return;
            }

        }

        //CustomFieldValue updated so
        //combine  CustomFields = CustomFieldRequired + Additional CustomFields
        self.CustomTemplates = self.CustomFieldsRequired().concat(self.CustomFieldsAdditional());

        var workflowId = getQueryStringByName("workflowId");
        var stepId = getQueryStringByName("stepId");
        var vendorProductId = getQueryStringByName("vendorProductId");
        var guid = getQueryStringByName("guid");
        var stagingId = getQueryStringByName("stagingId");
        var logUserUploadId = getQueryStringByName("logUserUploadId");
        var errorType = getQueryStringByName("errorType");

        // Ajax call
        $.ajax({
            type: 'POST',
            url: createUrl,
            cache: false,
            contentType: 'application/json; charset=utf-8',
            data: ko.toJSON({
                viewModel: self,
                workflowId: workflowId,
                stepId: stepId,
                vendorProductId: vendorProductId,
                logUserUploadId: logUserUploadId,
                stagingId: stagingId,
                errorType: errorType,
                guid: guid
            }),
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {

                if (response != null && response.Success === true) {
                    alertmsg('green', response.Message);

                    var returnUrl = response.Data;
                    if (returnUrl)
                        window.location.href = returnUrl;

                    var suggestionUrl = getQueryStringByName("returnUrl");
                    if (suggestionUrl) {
                        var productSuggestionParams = "";
                        if (suggestionUrl.indexOf("ValidateProductSuggestion") > -1) {
                            var vendorId = getQueryStringByName("vendorId");
                            var skuType = getQueryStringByName("skuType");
                            var errorType = getQueryStringByName("errorType");
                            var logUserUploadId = getQueryStringByName("logUserUploadId");
                            var guid = getQueryStringByName("guid");
                            var stagingId = getQueryStringByName("stagingId");

                            productSuggestionParams = "?vendorId=" + vendorId + "&skuType=" + skuType + "&errorType=" + errorType + "&logUserUploadId=" + logUserUploadId
                                + "&stagingId=" + stagingId + "&productId=" + response.Data + "&guid=" + guid;
                        }
                        if (productSuggestionParams !== "")
                            window.location = suggestionUrl + productSuggestionParams;
                        else
                            window.location = suggestionUrl;
                    }
                }
                else if (response != null && response.Success === false) {
                    alertmsg('red', response.Message);
                } else {
                    alertmsg('red', errorOccurredMessage);
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });

        // To actually transmit to server as a regular form post, write this: ko.utils.postJson($("form")[0], self.PricingLevels);
    };

    self.edit = function (form) {

        if (showAllProductCatalogTabs !== null && showAllProductCatalogTabs === "True") {

            if (self.Cost() == null || self.Cost() <= 0) {
                alertmsg('red', 'Cost should be greater than 0');
                return;
            }


            for (var i = 0; i < self.PricingLevels().length; i++) {
                var quantityFrom = self.PricingLevels()[i].QuantityFrom();
                //var quantityTo = self.PricingLevels()[i].QuantityTo();
                if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                    alertmsg('red', 'Please enter Quantity greater than one. Save Cancelled.');
                    return;
                }

                var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
                var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

                if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                    alertmsg('red', 'Margin %  should be greater than 0');
                    return;
                }

                if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                    alertmsg('red', 'Margin $  should be greater than 0');
                    return;
                }

                //if (i < self.PricingLevels().length - 1) {

                //    var prevQuantityFrom = parseFloat(self.PricingLevels()[i].QuantityFrom());
                //    var currQuantityFrom = parseFloat(self.PricingLevels()[i + 1].QuantityFrom());

                //    //if (prevQuantityFrom != null && currQuantityFrom != null && parseFloat(prevQuantityFrom) >= parseFloat(currQuantityFrom)) {
                //    //    alertmsg('red', 'Please enter the values for Quantity in ascending order. Save Cancelled.');
                //    //    return;
                //    //}
                //}
            }
        }

        //validation check for custom fields
        var customFieldList = [];

        //validation for the value required custom fields
        for (var i = 0; i < self.CustomFieldsRequired().length; i++) {
            var customFieldSelected = self.CustomFieldsRequired()[i];
            customFieldList.push(customFieldSelected.CustomField());

            var customFieldFromViewBag = $.grep(customFieldsRequiredViewBag, function (element, index) {
                return element.Id === customFieldSelected.CustomField();
            });

            //if (!customFieldSelected.FieldValue()) {

            var customFieldDataType = "VARCHAR(MAX)";
            if (customFieldFromViewBag.length > 0) {
                var selectedDataType = customFieldFromViewBag[0].DataType;
                if (selectedDataType)
                    customFieldDataType = selectedDataType;
            }
            if (customFieldDataType === "BIT" || customFieldDataType === "bit") {
                self.CustomFieldsRequired()[i].FieldValue(self.CustomFieldsRequired()[i].FieldValueBoolean());
            }
            else if (customFieldFromViewBag[0].AllowedValuesSdtId) {
                self.CustomFieldsRequired()[i].FieldValue(self.CustomFieldsRequired()[i].AllowedValuesSdtId());
            }

            // }

            if (customFieldSelected != null && (customFieldSelected.CustomField() == null || $.isNumeric(customFieldSelected.CustomField()) === false || customFieldSelected.CustomField() === 0)) {

                // found existing custom field with NullOrEmpty 
                alertmsg('red', customFieldNameRequiredMessage);
                return;
            }

            if (valueRequiredCustomFieldsViewBag.indexOf(customFieldSelected.CustomField()) > -1 && $.trim(self.CustomFieldsRequired()[i].FieldValue()) === "") {

                if (customFieldFromViewBag.length > 0) {
                    alertmsg("red", valuRequiredCustomFieldMessage.format(customFieldFromViewBag[0].Name));
                }
                return;
            }

        }

        //validation for the additional custom fields
        for (var i = 0; i < self.CustomFieldsAdditional().length; i++) {

            var customTemplate = self.CustomFieldsAdditional()[i];
            customFieldFromViewBag = $.grep(customFieldsAdditionalViewBag, function (element, index) {
                return element.Id === customTemplate.CustomField();
            });

            if (customTemplate != null && (customTemplate.CustomField() == null || $.isNumeric(customTemplate.CustomField()) === false || customTemplate.CustomField() === 0)) {

                //found existing custom field with NullOrEmpty 
                alertmsg('red', customFieldNameRequiredMessage);
                return;
            }

            // if (!customTemplate.FieldValue()) {

            customFieldDataType = "VARCHAR(MAX)";
            if (customFieldFromViewBag.length > 0) {
                selectedDataType = customFieldFromViewBag[0].DataType;
                if (selectedDataType)
                    customFieldDataType = selectedDataType;
            }

            //////console.log(self.CustomFieldsAdditional()[i].FieldValue());
            //////console.log(customFieldDataType);
            if (customFieldDataType === "BIT" || customFieldDataType === "bit") {
                //////console.log('1');
                //////console.log(self.CustomFieldsAdditional()[i].FieldValueBoolean());
                self.CustomFieldsAdditional()[i].FieldValue(self.CustomFieldsAdditional()[i].FieldValueBoolean());
            }
            else if (customFieldFromViewBag[0].AllowedValuesSdtId) {
                self.CustomFieldsAdditional()[i].FieldValue(self.CustomFieldsAdditional()[i].AllowedValuesSdtId());
            }
            //else {
            //    self.CustomFieldsAdditional()[i].FieldValue("");
            //}

            //}
            //////console.log(self.CustomFieldsAdditional()[i].FieldValue());

            if (customFieldDataType.toLowerCase() !== "bit" && !self.CustomFieldsAdditional()[i].FieldValue()) {

                if (customFieldFromViewBag.length > 0) {
                    alertmsg("red", customFieldValueRequiredMessage.format(customFieldFromViewBag[0].Name));
                }
                return;
            }
        }

        //validate for the required custom fields
        if (customFieldsRequiredViewBag) {

            //////console.log(customFieldList);
            if (!customFieldList) {
                alertmsg("red", fillUpAllRequiredCustomFields);
                return;
            }

            var isRequiredFieldMapped = true;
            $.each(customFieldsRequiredViewBag, function (index, val) {
                if (customFieldList.indexOf(val.Id) === -1) {
                    isRequiredFieldMapped = false;

                }

            });

            if (isRequiredFieldMapped === false) {
                alertmsg("red", fillUpAllRequiredCustomFields);
                return;
            }

        }

        //CustomFieldValue updated so
        //combine  CustomFields = CustomFieldRequired + Additional CustomFields
        self.CustomTemplates = self.CustomFieldsRequired().concat(self.CustomFieldsAdditional());

        var workflowId = getQueryStringByName("workflowId");
        var stepId = getQueryStringByName("stepId");
        var guid = getQueryStringByName("guid");
        var stagingId = getQueryStringByName("stagingId");
        var logUserUploadId = getQueryStringByName("logUserUploadId");
        var errorType = getQueryStringByName("errorType");
        // Ajax call
        $.ajax({
            type: 'POST',
            url: editUrl,
            cache: false,
            contentType: 'application/json; charset=utf-8',
            data: ko.toJSON({
                viewModel: self,
                workflowId: workflowId,
                stepId: stepId,
                logUserUploadId: logUserUploadId,
                stagingId: stagingId,
                errorType: errorType,
                guid: guid
            }),
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {
                if (response != null && response.Success === true) {
                    alertmsg('green', response.Message);

                    var returnUrl = response.Data;
                    if (returnUrl)
                        window.location.href = returnUrl;

                    var suggestionUrl = getQueryStringByName("returnUrl");
                    if (suggestionUrl)
                        window.location = suggestionUrl;
                }
                else if (response != null && response.Success === false) {
                    alertmsg('red', response.Message);
                }
                else {
                    alertmsg('red', errorOccuredMessage);
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });

        // To actually transmit to server as a regular form post, write this: ko.utils.postJson($("form")[0], self.PricingLevels);
    };

    self.supplierInfo = function () {
        $('#supplier-modal').modal('show');
    };

    self.viewStagingData = function () {
        $('#staging-data-dialog').modal('show');
    };

    self.setPrimary = function () {
        //$('#supplier-modal').modal('show');

        if (self.supplierId() == null) {
            alertmsg('red', 'Please select primary supplier first');
            return;
        }

        var vendorProductId = self.supplierId().vpcId;
        var productId = self.Id();
        var previousCost = self.Cost();
        // Ajax call
        $.ajax({
            type: 'POST',
            url: vendorProductSetPrimaryUrl,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { vendorProductId: vendorProductId, productId: productId },
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {
                if (response === "success") {
                    changeLevel = "";
                    self.isSetPrimary(true);
                    // Change cost to primary vendor's cost
                    self.Cost(self.supplierId().VPPRCost);
                    if (self.IsCostPriceEqual() === false) {
                        // Animation
                        $('#Cost').addClass('btn-warning');//.css('background-color', 'green');
                        setTimeout(function () {
                            $('#Cost').removeClass('btn-warning'); //.css('background-color', '');
                        }, 2000);

                        // Lock cost
                        self.IsCostLocked(true);

                        alertmsg('green', Alert_VendorProductSetPrimarySuccess);
                    }

                    else {
                        self.IsByPassCheckCostPrice(true);
                        self.Cost(FormatAmount(previousCost));
                    }
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });

    };

    self.setPrimaryOnLoad = function () {
        if (self.supplierId() == null) {
            return;
        }

        var vendorProductId = self.supplierId().vpcId;
        var productId = self.Id();
        var previousCost = self.Cost();
        // Ajax call
        $.ajax({
            type: 'POST',
            url: vendorProductSetPrimaryUrl,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { vendorProductId: vendorProductId, productId: productId },
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Processing');
            },
            success: function (response) {

                if (response === "success") {
                    //changeLevel = 1;
                    self.isSetPrimary(true);
                    setTimeout(function () {
                        //show the primary vendor by default 
                        //(index=0 is --Choose, index=1 is the vendor that needs to be set primary)
                        $("#supplier").prop('selectedIndex', 1).change();
                    }, 500);

                    // Change cost to primary vendor's cost
                    if (self.Cost() == null || self.Cost() === '' || self.Cost() === 0)
                        self.Cost(supplierList[0].VPPRCost);
                    if (self.IsCostPriceEqual() === false) {
                        // Lock cost
                        self.IsCostLocked(true);
                    }
                    else {
                        self.IsByPassCheckCostPrice(true);
                        self.Cost(FormatAmount(previousCost));
                    }
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                //alert(status + ' ' + error);
            }
        });

    };

    self.createInventorySupplier = function () {
        $('#create-inventory-supplier-modal').modal('show');
    };

    // Subscribe to changes
    self.Cost.subscribe(function (val) {

        ////////console.log('Cost');
        ////////console.log(changeLevel);

        if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("COST") > -1) {
            changeLevel = "";
            return;
        }

        val = parseFloat(val);

        if ($.isNumeric(val) === false) {
            return;
        }

        if (val === 0)
            return;
        var myCost = val;
        var myGrossMarginAmount = parseFloat(self.MarginAmount());
        var myGrossMarginPercent = parseFloat(self.MarginPercent());
        var mySellingPrice = parseFloat(self.Price());

        // Validation
        if (myGrossMarginAmount === 0 && myGrossMarginPercent === 0) {
            self.Price(val);
            return;
        }
        // Calculation
        if (myGrossMarginPercent !== 0) {
            if (changeLevel.indexOf('SAP') === -1) {
                mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);
            }
        } else if (mySellingPrice !== 0) {
            if (changeLevel.indexOf('MAP') === -1) {
                myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
            }
        }

        changeLevel += "COST,";

        //if quantity discount is there, update their value
        if (self.PricingLevels().length > 0) {
            for (var i = 0; i < self.PricingLevels().length; i++) {
                var pricingLevel = self.PricingLevels()[i];
                if (pricingLevel != null) {
                    var margin = pricingLevel.GrossMarginPercent();
                    pricingLevel.GrossMarginPercent(0);
                    pricingLevel.GrossMarginPercent(margin);
                }
            }
        }

        // Set values
        if (!self.IsMarginLocked()) {
            self.MarginPercent((myGrossMarginPercent));
        }
        self.MarginAmount((mySellingPrice - myCost));
        if (self.IsMarginLocked())
            self.Price((mySellingPrice));

    });

    self.MarginPercent.subscribe(function (val) {

        ////////console.log('MarginPercent');
        ////////console.log(changeLevel);

        if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("MAP") > -1) {
            changeLevel = "";
            return;
        }

        val = parseFloat(val);
        if ($.isNumeric(val) === false) {
            return;
        }
        if (val === 0)
            return;
        if (parseFloat(val) >= 100) {
            alertmsg('red', 'Margin percent cannot be greater than or equal to 100');
            self.MarginAmount(0);
            self.MarginPercent(0);
            self.Price(self.Cost());
            return;
        }

        var myCost = parseFloat(self.Cost());
        var myGrossMarginAmount = parseFloat(self.MarginAmount());
        var myGrossMarginPercent = parseFloat(val);
        var mySellingPrice = parseFloat(self.Price());

        // Validation
        if (myCost === 0 && mySellingPrice === 0) //amount check not required
            return;

        // Calculation
        if (myCost !== 0) {
            if (changeLevel.indexOf('SAP') === -1) {
                mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);
            }
        } else if (mySellingPrice !== 0) {
            if (changeLevel.indexOf('COST') === -1) {
                myCost = calculateCost(mySellingPrice, myGrossMarginPercent);
            }
        }

        if (changeLevel.indexOf('MAA') === -1) {
            myGrossMarginAmount = mySellingPrice - myCost;
        }

        changeLevel += "MAP,";

        // Set values
        if (!self.IsCostLocked() && !self.IsMarginLocked())
            self.Cost((myCost));
        if (!self.IsMarginLocked())
            self.MarginAmount((myGrossMarginAmount));
        //if (!self.IsSalePriceLocked()) {
        //    self.Price((mySellingPrice));

        //}
    });

    self.MarginAmount.subscribe(function (val) {
        ////////console.log('MarginAmount');
        ////////console.log(changeLevel);

        if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("MAA") > -1) {
            changeLevel = "";
            return;
        }

        val = parseFloat(val);
        if ($.isNumeric(val) === false) {
            return;
        }
        if (val === 0)
            return;

        var myCost = parseFloat(self.Cost());
        var myGrossMarginAmount = parseFloat(val);
        var myGrossMarginPercent = parseFloat(self.MarginPercent());
        var mySellingPrice = parseFloat(self.Price());

        // Validation
        if (myCost === 0 && mySellingPrice === 0) //percent check not required
            return;

        // Calculation
        if (myCost !== 0) {
            if (changeLevel.indexOf('SAP') === -1) {
                mySellingPrice = myCost + myGrossMarginAmount;
            }
        } else if (mySellingPrice !== 0) {
            if (changeLevel.indexOf('COST') === -1) {
                myCost = mySellingPrice - myGrossMarginAmount;
            }
        }

        changeLevel += "MAA,";

        if (!self.IsSalePriceLocked()) {
            self.Price((mySellingPrice));
        }
        if (!self.IsMarginLocked()) {
            myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
            self.MarginPercent((myGrossMarginPercent));
        }
        if (!self.IsCostLocked())
            self.Cost((myCost));

    });

    self.Price.subscribe(function (val) {
        ////////console.log('Price');
        ////////console.log(changeLevel);
        if (viewModel.isSetPrimary() === true && viewModel.IsByPassCheckCostPrice() === false) {
            var result = costpricevalidationFromProduct(viewModel);
            if (!result) {
                self.IsCostPriceEqual(true);
                return;
            }
            else {
                self.IsCostPriceEqual(false);
            }
        }
        if (self.IsCostPriceEqual() === false)
        {
            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("SAP") > -1) {
                changeLevel = "";
                return;
            }

            val = parseFloat(val);
            if ($.isNumeric(val) === false) {
                return;
            }
            if (val === 0)
                return;

            var myCost = parseFloat(self.Cost());
            var myGrossMarginAmount = parseFloat(self.MarginAmount());
            var myGrossMarginPercent = parseFloat(self.MarginPercent());
            var mySellingPrice = parseFloat(val);

            // Validation
            if (myCost === mySellingPrice) {
                return;
            }
            if (myCost === 0 && myGrossMarginAmount === 0 && myGrossMarginPercent === 0)
                return;

            // Calculation
            if (self.IsMarginLocked()) {
                if (changeLevel.indexOf('COST') === -1) {
                    myCost = mySellingPrice - myGrossMarginAmount;
                }
            }
            if (myCost !== 0) {
                if (changeLevel.indexOf('MAP') === -1) {
                    myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
                }
            } else if (myGrossMarginPercent !== 0) {
                if (changeLevel.indexOf('COST') === -1) {
                    myCost = calculateCost(mySellingPrice, myGrossMarginPercent);
                }
            }

            changeLevel += "SAP,";

            // Set values
            //

            if (!self.IsMarginLocked()) {
                //self.MarginAmount((self.Price() - self.Cost()));
                self.MarginPercent((myGrossMarginPercent));

            }
        }
        //self.MarginAmount();   <-- commented
    });

    // Check for lock (only 2 lock at most can be locked)
    self.IsCostLocked.subscribe(function (val) {
        if (self.Cost() === 0) {

            $('#LockCost').change(function () {
                alertmsg('red', 'Cannot lock when cost is zero');
            });
            self.IsCostLocked(false);
        }
        if (self.IsMarginLocked() === true && val === true) {
            alertmsg('red', 'Cannot lock cost when margin is locked');
            self.IsCostLocked(false);
        }
        // self.IsMarginLocked(!val);
    });
    self.IsMarginLocked.subscribe(function (val) {
        if (self.MarginAmount() == 0 || self.MarginPercent() == 0) {
            alertmsg('red', 'Cannot lock when margin is zero');
            self.IsMarginLocked(false);
        }
        self.IsSalePriceLocked(val);
        if (val === true && self.IsCostLocked() === true) {
            alertmsg('red', 'Cannot lock margin when cost is locked');
            self.IsMarginLocked(false);
        }
        //self.IsCostLocked(!val);
    });
    //self.IsSalePriceLocked.subscribe(function (val) {
    //    if (self.Price() == 0) {
    //        alertmsg('red', 'Cannot lock when price is zero');
    //        self.IsSalePriceLocked(false);
    //    }

    //    if (self.IsMarginLocked() === true && val === true && self.IsCostLocked() === true) {
    //        alertmsg('red', 'Cannot lock when both cost and margin are locked');
    //        self.IsSalePriceLocked(false);
    //    }
    //});

    // Clearance computation
    self.SalePrice.subscribe(function (val) {

        if ((changeLevel2.match(/,/g) || []).length === 1 || changeLevel2.indexOf("SPS") > -1) {
            changeLevel2 = "";
            return;
        }
        var grossMarginpercent = calculateGrossMarginPercent(self.Cost(), val);


        changeLevel2 += "SPS,";

        self.salePriceForClearanceMarginPercent(grossMarginpercent);
    });
    

    self.salePriceForClearanceMarginPercent.subscribe(function (val) {

        if ((changeLevel2.match(/,/g) || []).length === 1 || changeLevel2.indexOf("SPMP") > -1) {
            changeLevel2 = "";
            return;
        }
        if (!self.hasSalePriceForClearance()) {
            return;
        }
        }
        var sellingPrice = calculateSellingPrice(self.Cost(), val);


        changeLevel2 += "SPMP,";


        self.SalePrice(sellingPrice);

    });

};

//pricing viewmodel
var PIMPricingLevelViewModel = function (vm) {

    var self = this;
    self.PIMPricingId = ko.observable((vm != null && vm.PIMPricingId != null) ? vm.PIMPricingId : '');

    self.Id = ko.observable((vm != null && vm.Id != null) ? vm.Id : '');

    self.QuantityFrom = ko.observable(vm == null ? 1 : vm.QuantityFrom != null ? vm.QuantityFrom : '');
    //self.QuantityTo = ko.observable(vm == null ? 1 : vm.QuantityTo != null?vm.QuantityTo:'');
    self.GrossMarginPercent = ko.observable(vm == null ? /*(viewModel.MarginPercent())*/ 0 : vm.GrossMarginPercent != null ? (vm.GrossMarginPercent) : '')
        .extend({ numeric: decimal_places });

    self.GrossMarginAmount = ko.observable(vm == null ? /*(viewModel.MarginAmount())*/ 0 : vm.GrossMarginAmount != null ? (vm.GrossMarginAmount) : '')
        .extend({ numeric: decimal_places });

    self.PriceForPricingLevel = ko.observable(vm == null ? 0 : vm.Price != null ? (vm.Price) : '')
        .extend({ numeric: decimal_places });
    self.GrossMarginPercent.subscribe(function (val) {


        if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("GMP") > -1) {
            changeLevel = "";

            return;
        }
        if ((changeLevel3.match(/,/g) || []).length === 1 || changeLevel3.indexOf("GMP") > -1) {
            changeLevel3 = "";
            changeLevel = "";

            return;
        }

        val = parseFloat(val);

        if ($.isNumeric(val) === false) {
            return;
        }
        if (val === 0)
            return;
        if (parseFloat(val) >= 100) {
            alertmsg('red', 'Margin percent cannot be greater than or equal to 100');
            self.GrossMarginPercent(0);
            self.GrossMarginAmount(0);
            return;
        }
        var myCost = parseFloat(viewModel.Cost());
        var myGrossMarginAmount = 0;
        var myGrossMarginPercent = parseFloat(val);
        var mySellingPrice = 0;

        // Validation
        if (myCost === 0 || myGrossMarginPercent === 0) //amount check not required
            return;

        // Calculation

        mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);
        myGrossMarginAmount = mySellingPrice - myCost;

        changeLevel += "GMP,";
        changeLevel3 += "GMP,";
        // Set values
        self.GrossMarginAmount(myGrossMarginAmount);

        self.PriceForPricingLevel(mySellingPrice);
        //////////////console.log('outside GrossMarginPercent');
    });

    self.PriceForPricingLevel.subscribe(function (val) {
        //////////////console.log('inside PriceForPricingLevel');


        if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("SEP") > -1) {
            changeLevel = "";

            return;
        }

        if ((changeLevel3.match(/,/g) || []).length === 1 || changeLevel3.indexOf("SEP") > -1) {
            changeLevel3 = "";
            changeLevel = "";

            return;
        }

        val = parseFloat(val);
        if ($.isNumeric(val) === false) {
            return;
        }
        if (val === 0)
            return;

        var myCost = parseFloat(viewModel.Cost());
        var myGrossMarginPercent = parseFloat(self.GrossMarginPercent());
        var mySellingPrice = parseFloat(val);



        // Validation
        if (myCost === mySellingPrice) {
            return;
        }
        if (myCost === 0 || mySellingPrice === 0)
            return;

        // Calculation

        if (myCost !== 0) {
            myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);

        }

        changeLevel += "SEP,";
        changeLevel3 += "SEP,";
        // Set values
        //

        if (!viewModel.IsMarginLocked()) {

            self.GrossMarginPercent(myGrossMarginPercent);

        }
        self.GrossMarginAmount(mySellingPrice - myCost);

    });

    self.QuantityFrom.subscribe(function (val) {
        var quantityFrom = parseFloat(val);

        var currentIndex = 0;
        for (var i = 0; i < viewModel.PricingLevels().length; i++) {
            if (viewModel.PricingLevels()[i].QuantityFrom() === val) {
                currentIndex = i;
                break;
            }
        }

        if (currentIndex > 0) {

            var previousPricingLevel = viewModel.PricingLevels()[currentIndex - 1];
            var addedPricingLevel = viewModel.PricingLevels()[currentIndex];

            if (previousPricingLevel != null && previousPricingLevel.QuantityFrom() != null && previousPricingLevel.QuantityFrom() !== "") {
                if (parseFloat(previousPricingLevel.Quantity) < parseFloat(addedPricingLevel.QuantityFrom)) {
                    alertmsg('red', 'Previous Quantity (' + previousPricingLevel.QuantityFrom + ') should be less than current quantity (' + val.quantityFrom + '.');

                    self.QuantityFrom('');
                    return;
                }
            }

        }


    });


}

//custom field manager viewmodel
var CustomFieldManagerViewModel = function (vm) {

    var self = this;
    self.CustomField = ko.observable(vm != null && vm.CustomField != null ? vm.CustomField : '');
    self.FieldValue = ko.observable(vm != null && vm.FieldValue != null ? vm.FieldValue : '');
    self.FieldValueBoolean = ko.observable(vm != null && vm.FieldValueBoolean != null ? vm.FieldValueBoolean : '');
    self.AllowedValuesSdtId = ko.observable();
    self.FieldAlias = ko.observable(vm != null && vm.FieldAlias != null ? vm.FieldAlias : '');

    self.FieldValueIsDirty = ko.observable(false);

    self.CustomField.subscribe(function (val) {

        if (val == null)
            return;

        var customFieldId = val;
        if (customFieldId !== "") {

            viewModel.hasCustomFieldValue(true);

            var countDuplication = 0;

            //Check for duplicate
            for (var i = 0; i < viewModel.CustomTemplates().length; i++) {

                var customTemplate = viewModel.CustomTemplates()[i];

                if (customTemplate != null && customTemplate.CustomField() != null) {
                    if (parseFloat(customFieldId) === parseFloat(customTemplate.CustomField()) && $.isNumeric(customFieldId) === true) {

                        countDuplication++;

                    }
                }
            }

            if (parseFloat(countDuplication) > 1) {

                // found existing custom field
                alertmsg('red', 'Custom Field cannot be duplicate');
                self.CustomField('');

                return;
            }

            RenderElementWithDataType(customFieldId, "false");

            return;
        }
        viewModel.hasCustomFieldValue(false);

    });

    self.FieldValue.subscribe(function (val) {

        var customFieldId = self.CustomField();
        var oldValue = $.grep(productCatalogVm.CustomTemplates, function (element, index) {
            return element.CustomField === customFieldId;
        });

        var newValue = val;
        // //console.log(oldValue[0].FieldValueBoolean);
        ////console.log(newValue);
        if (oldValue.length > 0) {
            if (oldValue[0].FieldValue !== newValue)
                self.FieldValueIsDirty(true);
            else
                self.FieldValueIsDirty(false);
        }
        else
            self.FieldValueIsDirty(true);
    });

    self.FieldValueBoolean.subscribe(function () {
        var customFieldId = self.CustomField();
        var oldValue = $.grep(productCatalogVm.CustomTemplates, function (element, index) {
            return element.CustomField === customFieldId;
        });

        var newValue = self.FieldValueBoolean();
        // //console.log(oldValue[0].FieldValueBoolean);
        // //console.log(newValue);

        if (oldValue.length > 0) {
            if (oldValue[0].FieldValueBoolean !== newValue)
                self.FieldValueIsDirty(true);
            else
                self.FieldValueIsDirty(false);
        }
        else
            self.FieldValueIsDirty(true);
    });

    self.AllowedValuesSdtId.subscribe(function () {
        var customFieldId = self.CustomField();
        var oldValue = $.grep(productCatalogVm.CustomTemplates, function (element, index) {
            return element.CustomField === customFieldId;
        });

        var newValue = self.AllowedValuesSdtId();
        //console.log(oldValue[0].FieldValueBoolean);
        //console.log(newValue);
        if (oldValue.length > 0) {
            if (oldValue[0].AllowedValuesSdtId !== newValue)
                self.FieldValueIsDirty(true);
            else
                self.FieldValueIsDirty(false);
        }
        else
            self.FieldValueIsDirty(true);
    });
}

//apply ko bindings to viewmodel
var viewModel = new ProductCatalogViewModel(productCatalogVm);
ko.applyBindings(viewModel);


if (vpName && vpName !== "") {
    viewModel.Name(vpName);
}
if (vpPartNumber !== "" && vpPartNumber) {
    viewModel.LocalSku(vpPartNumber);
}

if (vpDescription !== "" && vpDescription) {
    viewModel.ShortDescription(vpDescription);
}

//loop through existing 
//viewModel.MarginPercent(0);
//viewModel.MarginPercent((productCatalogVm != null && productCatalogVm.MarginPercent != null) ? productCatalogVm.MarginPercent : '0');

viewModel.SalePrice(0);
viewModel.SalePrice((productCatalogVm != null && productCatalogVm.SalePrice != null) ? productCatalogVm.SalePrice : '0');

//pricing levels for edit mode
if (productCatalogVm != null && productCatalogVm.PricingLevels != null) {
    $.each(productCatalogVm.PricingLevels, function (indexPricingLevels, valuePricingLevels) {
        //////////////console.log(valuePricingLevels);
        viewModel.PricingLevels.push(new PIMPricingLevelViewModel(valuePricingLevels));
    });

}

//toggle cost
//viewModel.Cost(0);
//viewModel.Cost(productCatalogVm != null && productCatalogVm.Cost != null ? productCatalogVm.Cost : 0);

//viewModel.Price(0);
//viewModel.Price(productCatalogVm != null && productCatalogVm.Price != null ? productCatalogVm.Price : 0);

//custom fields for edit mode
if (productCatalogVm != null && productCatalogVm.CustomTemplates != null) {
    $.each(productCatalogVm.CustomTemplates, function (index, value) {
        if (value.IsReadOnly === 1) {
            viewModel.CustomFieldsReadOnly.push(new CustomFieldManagerViewModel(value));

        }
        else if (value.IsRequired === 1) {
            viewModel.CustomFieldsRequired.push(new CustomFieldManagerViewModel(value));

        }
        else {
            viewModel.CustomFieldsAdditional.push(new CustomFieldManagerViewModel(value));

        }
    });
}

//construct elements as per data type
RenderElementWithDataType(-1, "true");

try {
    //The list contains the first supplier as primary supplier, hence select it to get the primary supplier
    viewModel.supplierId(supplierList[0]);

    //select primary supplier by default
    var isSetPrimaryValid = $.isNumeric(viewModel.Cost());
    if (viewModel.Cost() > 0) {
        isSetPrimaryValid = true;
    }
    if ($('#supplier')[0].length > 1 && isSetPrimaryValid) { //check if there is valid suppliers list
        //check if it is a workflow. And auto-associate PC and VPC only if the request has come from workflow
        var workflowId = getQueryStringByName("workflowId");
        if (workflowId) {
            viewModel.setPrimaryOnLoad();
        }
    }

    if ($('#supplier')[0].length === 2) { //if there is only one primary supplier (1. please select and 2. primary supplier), disable set primary button
        $('#btn-set-primary').attr('disabled', 'disabled');
        $('#supplier').attr('disabled', 'disabled');
    }

    window.setTimeout(function (e) {
        //If the product does not have primary vendor product, show --Select on dropdown and enable set primary button
        if (productCatalogVm.PrimaryVendorProductId === null) {
            $('#btn-set-primary').attr('disabled', false);
            $('#supplier').attr('disabled', false);
            $('#supplier').val('');
            viewModel.supplierId('');
        }
    }, 100);

    isSaved = true;

} catch (e) {
    //alert(e);
}
changeLevel = "";
// Activate datatable
$('#VendorProductDataTable').DataTable();

// Activate jQuery Validation
$("form").validate({ submitHandler: viewModel.save });

//For Staging Data
$('#grid-div').append('<table id="gridDataTable"><thead><tr></tr></thead></table>');
////////////console.log("ok upto here16");
//add heading to table
var i = 0;
var includedColumns = [];

if (returnObject != null && returnObject !== "") {
    returnObject = JSON.parse(returnObject);

    $.each(returnObject.sColumns, function (key, value) {

        var headerRemove = value.replace(/_/gi, " ");

        if (i > 0) {
            $('#gridDataTable thead tr').append('<th>' + headerRemove + '</th>');
            includedColumns.push({ "sName": value });

        } else {
            $('#gridDataTable thead tr').append('<th>' + "" + '</th>');
            includedColumns.push({
                "sName": value,
                "bSearchable": false,
                "bSortable": false,
                "width": "10",

                "mRender": function (data, type, full) {
                    return '<input type="checkbox" class="listStagingDataGuid" value="' + data + '">'
                }
            });

        }
        i++;
    });

    //add data to table via datatable
    var table = $('#gridDataTable').DataTable({
        destroy: true,
        data: returnObject.aaData,
        "scrollX": true,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "aoColumns": includedColumns
    });
}

$("#btnCancel").click(function () {

    var returnUrl = getQueryStringByName("returnUrl");
    if (returnUrl != null && returnUrl !== "") {
        window.location = returnUrl;
    }
    //if no return url is specified, return to catalog manager page
    window.location = catalogManagerIndexUrl;

});

//$('#PlatformQualifier').select2({
//    maximumSelectionLength: 4
//});

//ko.bindingHandlers.select2 = {
//    init: function (element, valueAccessor) {
//        var options = ko.toJS(valueAccessor()) || {};
//        setTimeout(function () {
//            $(element).select2(options);
//        }, 0);
//    }
//};

$("#PlatformQualifier").select2({
    maximumSelectionSize: 3
});

$(".customFieldSelect2").select2({ width: '100%' });
