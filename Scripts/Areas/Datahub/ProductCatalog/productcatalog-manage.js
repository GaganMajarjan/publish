﻿
/***
changeLevels List:
 COST = cost
 MAP= Margin Percent
 MAA = Margin Amount
 SAP = Sale Price
 GMP = Gross Margin Percent
 GMA = Gross Margin Amount
 SEP = Selling Price
***/
//try {

$(document)
    .ready(function() {
        $("#ProductCategoryId")
            .select2({
                tags: true,
                placeholder: '--Please select a category'
            });
    });

var changeLevel = "";
var changeLevel2 = "";
var changeLevel3 = "";
var sessionOption = "";


//Json parse viewbag elements
//decode viewModel
var productCatalogVm = fixJsonQuote(productCatalogVm);
//console.log(vendorProductCatalogVmJson);
ReplaceSingle2Quote(productCatalogVm);


//productCatalogVm = productCatalogVm.replace(/\\"/g, '"').replace(/"/g, '\\"');
//productCatalogVm = decodeHtml(productCatalogVm);
//productCatalogVm = fixNewLineOnly(productCatalogVm);
////Parse viewModel
//parse value required custom fields 
if (valueRequiredCustomFieldsViewBag)
    valueRequiredCustomFieldsViewBag = fixJsonQuote(valueRequiredCustomFieldsViewBag);
else
    valueRequiredCustomFieldsViewBag = null;

//parse read only fields
if (customFieldsReadOnlyViewBag)
    customFieldsReadOnlyViewBag = fixJsonQuote(customFieldsReadOnlyViewBag);
else
    customFieldsReadOnlyViewBag = null;

unitOfMeasureListViewBag = JSON.parse(unitOfMeasureListViewBag);

var unitOfMeasureList = [];
$.each(unitOfMeasureListViewBag, function(index, val) {
    unitOfMeasureList.push(val.TargetUOM);
});

// Suppliers list
var supplierList = [];
var primarySupplierIdIndex;

if (supplierListViewBag !== "[]") {

    supplierListViewBag = decodeHtml(supplierListViewBag);
    supplierListViewBag = fixJsonSingleQuote(supplierListViewBag);
    ReplaceSingle2Quote(supplierListViewBag);

    $.each(supplierListViewBag, function(index, val) {

        supplierList.push({
            Id: val.Id,
            vpcId: val.vpcId,
            value: val.VendorId, //vendorId: val.vendorId,
            VendorId: val.VendorId,
            name: val.Name, //vendorName: val.Name,
            nameCost: val.VendorName + '(Cost: ' + val.Cost + ')', //vendorName: val.Name,
            VPPRCost: val.Cost,
            OrderBy: val.OrderBy,
            //OrderByText: val.OrderBy !== null
            //    ? (val.OrderBy.toUpperCase() === 'P'
            //        ? 'by the piece'
            //        : val.OrderBy.toUpperCase() === 'L'
            //        ? 'by the lot'
            //        : val.OrderBy.toUpperCase() === 'Q' ? 'by the piece in fixed lots' : '')
            //    : '',
            OrderByText: val.OrderBy !== null
                ? (val.OrderBy.toUpperCase() === 'P'
                    ? 'by the piece'
                    : val.OrderBy.toUpperCase() === 'L'
                        ? 'by the lot'
                        : '')//: val.OrderBy.toUpperCase() === 'Q' ? 'by the piece in fixed lots'  SKYG-820
                : '',
            QuantityPerLot: val.QuantityPerLot,
            CostPerLot: val.CostPerLot,
            MinimumOrderUnit: val.MinimumOrderUnit,
            MinimumOrderUnitText: val.MinimumOrderUnit !== null
                ? (val.MinimumOrderUnit.toUpperCase() === 'P'
                    ? 'by the piece'
                    : val.MinimumOrderUnit.toUpperCase() === 'L'
                    ? 'by the lot'
                        : '')// : val.MinimumOrderUnit.toUpperCase() === 'Q' ? 'by the piece in fixed lots'
                : '',
            MinimumOrderQuantity: val.MinimumOrderQuantity,
            VendorPartNumber: val.VendorPartNumber,
            VendorName: val.VendorName,
            MinLineOrderAmount: val.MinLineOrderAmount,
            VendorIsDisabled: val.VendorIsDisabled,
            VpcIsDisabled: val.VpcIsDisabled,
            IsNotAllowedToSetPrimary: val.IsNotAllowedToSetPrimary,
            Cost: val.Cost,
            DateLastCostUpdate: val.DateLastCostUpdate ? val.DateLastCostUpdate : val.CreateTs, //if update date is not there, get create date
            Qoh: val.Qoh,
            QohUpdatedOn: val.QohUpdatedOn,
            ShelfLife: val.ShelfLife,
            CreateTs: val.CreateTs,
            DateCreated: val.DateCreated,
            DateLastUpdated: val.DateLastUpdated,
            ProductId: val.ProductId
        });

        if (productCatalogVm !== null && productCatalogVm.SupplierId !== null && val.vendorId === productCatalogVm.SupplierId) {
            primarySupplierIdIndex = index;
        }
    });
}

//Cost
var originalCost = (productCatalogVm !== null && productCatalogVm.Cost !== null) ? productCatalogVm.Cost : null;
var originalSalePrice = (productCatalogVm !== null && productCatalogVm.Price !== null) ? productCatalogVm.Price : null;

//apply ko bindings to viewmodel
var viewModel = new ProductCatalogViewModel(productCatalogVm);
ko.applyBindings(viewModel);

try {

    var vpName = vendorProductCatalogVmJson.Name;
    var vpDescription = vendorProductCatalogVmJson.Description;
    var vpPartNumber = vendorProductCatalogVmJson.VendorPartNumber;

    if (vpName && vpName !== "" && productCatalogVm.Id === null) {
        viewModel.Name(vpName);
    }
    if (vpPartNumber && vpPartNumber !== "" && productCatalogVm.Id === null) {
        viewModel.LocalSku(vpPartNumber);
    }

    if (vpDescription && vpDescription !== "" && productCatalogVm.Id === null) {
        viewModel.ShortDescription(vpDescription);
    }

    viewModel.vendorProduct.CanDropship(!vendorProductCatalogVmJson.CanNotDropship);

} catch (e) {
    console.log(e);
}

//pricing levels for edit mode
if (productCatalogVm !== null && productCatalogVm.PricingLevels !== null) {
    $.each(productCatalogVm.PricingLevels, function(indexPricingLevels, valuePricingLevels) {
        //////////////console.log(valuePricingLevels);
        viewModel.PricingLevels.push(new PIMPricingLevelViewModel(valuePricingLevels));
    });

}

//custom fields for edit mode
if (productCatalogVm !== null && productCatalogVm.CustomTemplates !== null) {
    $.each(productCatalogVm.CustomTemplates, function(index, value) {
        if (value.IsReadOnly === 1) {
            viewModel.CustomFieldsReadOnly.push(new CustomFieldManagerViewModel(value));

        } else if (value.IsRequired === 1) {
            viewModel.CustomFieldsRequired.push(new CustomFieldManagerViewModel(value));

        } else {
            viewModel.CustomFieldsAdditional.push(new CustomFieldManagerViewModel(value));

        }
    });
}

try {
    //The list contains the first supplier as primary supplier, hence select it to get the primary supplier
    viewModel.supplierId(supplierList[0]);

    //select primary supplier by default
    var isSetPrimaryValid = $.isNumeric(viewModel.Cost());
    if (viewModel.Cost() > 0) {
        isSetPrimaryValid = true;
    }
    if ($('#supplier')[0].length > 1 && isSetPrimaryValid) { //check if there is valid suppliers list
        //check if it is a workflow. And auto-associate PC and VPC only if the request has come from workflow
        var workflowId = getQueryStringByName("workflowId");
        if (workflowId) {
            viewModel.setPrimaryOnLoad();
        }
    }

    if ($('#supplier')[0].length === 2) { //if there is only one primary supplier (1. please select and 2. primary supplier), disable set primary button
        $('#btn-set-primary').attr('disabled', 'disabled');
        $('#supplier').attr('disabled', 'disabled');
    }

    window.setTimeout(function(e) {
        //If the product does not have primary vendor product, show --Select on dropdown and enable set primary button
        if (productCatalogVm.PrimaryVendorProductId === null) {
            $('#btn-set-primary').attr('disabled', false);
            $('#supplier').attr('disabled', false);
            $('#supplier').val('');
            viewModel.supplierId('');
        }
    }, 100);

    isSaved = true;

} catch (e) {
    //alert(e);
}
changeLevel = "";
// Activate datatable
$('#VendorProductDataTable').DataTable();

// Activate jQuery Validation
$("form").validate({ submitHandler: viewModel.save });

//For Staging Data
$('#grid-div').append('<table id="gridDataTable"><thead><tr></tr></thead></table>');
//add heading to table
var i = 0;
var includedColumns = [];

if (returnObject != null && returnObject !== "") {
    returnObject = JSON.parse(returnObject);

    $.each(returnObject.sColumns, function(key, value) {

        var headerRemove = value.replace(/_/gi, " ");

        if (i > 0) {
            $('#gridDataTable thead tr').append('<th>' + headerRemove + '</th>');
            includedColumns.push({ "sName": value });

        } else {
            $('#gridDataTable thead tr').append('<th>' + "" + '</th>');
            includedColumns.push({
                "sName": value,
                "bSearchable": false,
                "bSortable": false,
                "width": "10",

                "mRender": function(data, type, full) {
                    return '<input type="checkbox" class="listStagingDataGuid" value="' + data + '">'
                }
            });

        }
        i++;
    });

    //add data to table via datatable
    var table = $('#gridDataTable').DataTable({
        destroy: true,
        data: returnObject.aaData,
        "scrollX": true,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "aoColumns": includedColumns
    });
}

$("#btnCancel, #btnCancelone").click(function() {

    var returnUrl = getQueryStringByName("returnUrl");
    if (returnUrl != null && returnUrl !== "") {
        window.location = returnUrl;
    }
    //if no return url is specified, return to catalog manager page
    window.location = catalogManagerIndexUrl;

});

$("#PlatformQualifier").select2({
    maximumSelectionSize: 3
});

$(".customFieldSelect2").select2({ width: '100%' });

$('.vendor-product-tab select').select2();

var animateCost = function(cssClass) {
    cssClass = cssClass || 'btn-warning';
    $('#Cost').addClass(cssClass);
    setTimeout(function() {
            $('#Cost').removeClass(cssClass);
        },
        2000);
}


var setPrimary = function (options) {
    var vendorProductId = options.vendorProductId;
    var productId = options.productId;
    var isSetPrimary = options.isSetPrimary;
    var currentCost = options.currentCost;
    var oldCost = options.oldCost;
    var isMarginLocked = options.isMarginLocked;
    var isMarginNegative = options.IsMarginNegative;
    viewModel.IsMarginLocked(isMarginLocked);
    viewModel.isSetPrimary(true);
    viewModel.IsByPassCheckCostPrice(false);
    viewModel.IsMarginNegative(isMarginNegative);
    //for set primary, don't change vendor product cost
    viewModel.LockVendorProductCost(true);
    //change only product's cost
    viewModel.Cost(FormatAmount(currentCost));
    //var result = validateCostPriceRangeSetPrimary(viewModel.Price(), viewModel.Cost());
       
    if (viewModel.IsCostPriceEqual() === false) {
        //if (result) {
        animateCost('btn-warning');

        if (bypassNegativeMarginDueToFifo === false && viewModel.IsMarginLocked() !== true) {

            viewModel.Module = $('ul[role=tablist]>li').first().text();
            viewModel.RecalcMarginPct = viewModel.MarginPercent();


            if (viewModel.MarginPercent() < 10) {
                viewModel.IsValidate = true;
                viewModel.IsPriceNegative = false;
                $('#MarginNegativeDueToFifoCostModal').modal('show');
                viewModel.Cost(FormatAmount(oldCost));
                return;

            }

            if (viewModel.PricingLevels().length > 0) {
                for (var i = 0; i < viewModel.PricingLevels().length; i++) {
                    var pricingLevel = viewModel.PricingLevels()[i];
                    if (pricingLevel != null) {
                        if (pricingLevel.GrossMarginPercent() < 0) {
                            viewModel.IsValidate = true;
                            viewModel.IsPriceNegative = true;
                            $('#MarginLeveNegativeDueToFifoCostModal').modal('show');
                            return;
                        }

                    }
                }
            }
        }


        var data = ko.toJSON(viewModel);

        //convert string to object
        data = JSON.parse(data);

        //add property to object
        data.CustomTemplates = getCustomFieldJson(viewModel.CustomTemplateGroup, '.cf-by-group .cf', true);

        //set isSetPrimary to true
        data.isSetPrimary = true;

        //check if yahooId validation has to be bypassed (occurs if user clicks buttons to override existing YahooId)
        if (options && options.triggerActionId) {
            data.viewModel.triggerActionId = options.triggerActionId;
        }

        //check if validation [for conflict of prices after round-up] has to be bypassed
        if (options && !options.bypassQuantityDiscountRoundUpCheck) {
            var opt = {
                vendorProductId: vendorProductId,
                productId: productId,
                isMarginLocked: isMarginLocked,
                isSetPrimary: isSetPrimary,
                currentCost: currentCost,
                oldCost: oldCost
            };
            opt.bypassQuantityDiscountRoundUpCheck = true;
            isValid = validateQuantityDiscountRoundUp(opt);

            if (!isValid)
                return false;
        }

        if (options && options.bypassKitQuantityDiscountRoundUpCheck)
            data.bypassKitQuantityDiscountRoundUpCheck = true;

        // Ajax call
        $.ajax({
            type: 'POST',
            url: vendorProductSetPrimaryUrl,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: {
                vendorProductId: vendorProductId,
                productId: productId,
                isMarginLocked: isMarginLocked,
                viewModel: data
            },
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Processing');
            },
            success: function (response) {
                if (response && response.Status == true && response.Message == "success") {

                    changeLevel = "";

                    // Change cost to primary vendor's cost
                    viewModel.Cost(FormatAmount(currentCost));
                    animateCost('btn-success');

                    // Lock cost
                    viewModel.IsCostLocked(true);

                    alertmsg('green', Alert_VendorProductSetPrimarySuccess);

                    viewModel.PrimaryVendorProductId = vendorProductId;

                    var activeHash = getActiveHash();

                    if (activeHash !== "") {
                        window.location = window.location.toString().split('#')[0] + activeHash;
                    }

                    window.location.reload();

                }
                else if (response && response.Status == false && response.Message == "INVALID" && response.Data == "popup") {
                    options.bypassKitQuantityDiscountRoundUpCheck = true;
                    options.bypassQuantityDiscountRoundUpCheck = true;
                    $('#hiddenQuantityDiscountRoundUpValue').val(JSON.stringify(options));
                    $('#QuantityDiscountRoundUpModal').modal('show');
                    $("#quantityDiscountRoundUpIsKit").text("KIT: ");
                    SGloadingRemove();
                }
                else {
                    //if error occurs, we reach here
                    alertmsg('red', response.Message + '. ' + 'Cannot set primary.');
                    viewModel.Cost(FormatAmount(oldCost));
                    animateCost('btn-danger');
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
                //remove lock
                viewModel.LockVendorProductCost(false);
            },
            error: function (xhr, status, error) {
                //reset variables here
                viewModel.Cost(FormatAmount(oldCost));
                $('#Cost').addClass('btn-danger');
                setTimeout(function () {
                    $('#Cost').removeClass('btn-danger');
                },
                    2000);

                alert(status + ' ' + error);
            }
        });
    }
    else {
        viewModel.IsByPassCheckCostPrice(true);
        viewModel.Cost(FormatAmount(oldCost));
       
    }
    
}

//set primary vendor
$('.btn-set-primary')
    .click(function(options) {
        var hidden = $(this).find('input[type=hidden]');
       
        options = options || {};

        var vendorProductId = options.vendorProductId;

        if (hidden) {
            vendorProductId = vendorProductId || hidden.val();
        }

        var productId = options.productId || viewModel.Id();

        if (vendorProductId === "") {
            alertmsg('red', 'No vendor product to set primary');
            return;
        }
        if (productId === "") {
            alertmsg('red', 'No product to set primary');
            return;
        }

        if (viewModel) {
            if (viewModel.supplierId() == null) {
                alertmsg('red', 'Please select primary supplier first');
                return;
            }

            //if cost is locked, cannot set primary
            if (viewModel.IsCostLocked() == true) {
                alertmsg('red', 'Cannot set primary when cost is locked');
                return;
            }
        }
       self.setFieldReferenceValue();
        viewModel = viewModel || {};
        //var index = supplierList.findIndex(function (ele) {
        //    return ele.Id.toString() === vendorProductId;
        //});
        //console.log(index);
        //viewModel.vendorProduct(new VendorProductCatalogViewModel(supplierList[index]));
        //viewModel.supplierId(supplierList[index]);

        //var currentCost = $(this).parent().parent().parent().find('.cost').html();
        var currentCost = $(this).parent().parent().parent().find('.cost span')[0].innerHTML;
        currentCost = parseFloat(currentCost) || 0;

        var oldCost = viewModel.Cost();

        viewModel.currentCost = currentCost;
        viewModel.oldCost = oldCost;

        //set options
        options.vendorProductId = options.vendorProductId || vendorProductId;
        options.productId = options.productId || productId;
        options.isMarginLocked = options.isMarginLocked || viewModel.IsMarginLocked();
        options.currentCost = options.currentCost || currentCost;
        options.oldCost = options.oldCost || oldCost;
        options.isSetPrimary = options.isSetPrimary || true;

        sessionOption = options;
        setPrimary(options);

    });
self.setFieldReferenceValue = function () {
    viewModel.IdentifierExists = $('[data-name = "IdentifierExists"]').val() !== "1" ? false : true;
    viewModel.HazmatBoxFee = $('[data-name="HazmatBoxFee"]').val();
}

$('.custom-fields select').select2();

$(".datetimeDataType").datepicker();

(function($) {
    $(window).load(function() {
        $(".list-v-scroll").mCustomScrollbar({
            theme: "dark"
        });

        if (productCatalogVm.VendorProduct == null || productCatalogVm.VendorProduct.Id == null || productCatalogVm.VendorProduct.Id === 0) {
            //no vendor product information --> default the Vendor Product section to the Pricing section
            activateBootStrapTab('general');
            activateBootStrapTab('vpc-general');
        }

        if (viewModel != null && viewModel.vendorProduct() != null && viewModel.vendorProduct().IsDisabledForEdit() === false) {
            $('#addNewVendorProduct').attr('disabled', true);
        } else {
            $('#addNewVendorProduct').attr('disabled', false);
        }

        var productHash = window.location.hash.split('#')[1];
        var vendorProductHash = window.location.hash.split('#')[2];

        if (productHash) {
            activateBootStrapTab(productHash);

            if (vendorProductHash) {
                activateBootStrapTab(vendorProductHash);
            }
        }

        //disable location in edit mode
        if (typeof viewModel !== 'undefined' && viewModel.Id() !== "" && viewModel.Id() > 0) {
            $('#Location').prop('disabled', true);
        }

        $.fn.matchHeight._update();

        $('.custom-fields select').select2();
       
    });

    if (viewModel.IsCostLocked() == true) {
        $(".vendor-product-tab #Cost").attr("disabled", true);
        $(".vendor-product-tab #CostPerLot").attr("disabled", true);
        $(".vendor-product-tab #QuantityPerLot").attr("disabled", true);
        $(".vendor-product-tab #OrderBy").attr("disabled", true);
    }

})(jQuery);
//} catch (e) {
//    alertmsg('red', e.toString());
//}

