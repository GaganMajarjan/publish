﻿
var autoCalculateItemPoints = function (changes) {
    //get necessary values
    var rowIndex = changes[0];
    var prop = changes[1];
    var newVal = changes[3] === (null || "") ? 1 : changes[3];

    //if (prop.toLowerCase() == "itempoints")
    //    return;

    //check if there are enough columns for itempoints auto-calculation
    if (hot) {
        var cols = hot.getSettings().columns;
        var itemPointsColumns = [];
        //$.each(cols,
        //    function (index, value) {
        //        if (value.data.toLowerCase() == "itempoints" ||
        //            value.data.toLowerCase() == "dimensions") {
        //            itemPointsColumns.push(value.data);
        //        }
        //    });

        $.each(cols,
          function (index, value) {
              if (value.data.toLowerCase() == "itempoints" ||
                  value.data.toLowerCase() == "shippingheight" ||
                  value.data.toLowerCase() == "shippingwidth" ||
                  value.data.toLowerCase() == "shippinglength") {
                  itemPointsColumns.push(value.data);
              }
          });

        if (itemPointsColumns.length == 4) {

            var heightcellvalue = hot.getDataAtRowProp(rowIndex, "ShippingHeight");
            var widthcellvalue = hot.getDataAtRowProp(rowIndex, "ShippingWidth");
            var lengthcellvalue = hot.getDataAtRowProp(rowIndex, "ShippingLength");

            if (heightcellvalue === null && widthcellvalue === null && lengthcellvalue === null)
            {
                hot.setDataAtRowProp(rowIndex, "ItemPoints", "");
            }

            else if (heightcellvalue === "" && widthcellvalue === "" && lengthcellvalue === "") {
                hot.setDataAtRowProp(rowIndex, "ItemPoints", "");
            }

            else {
                var height = heightcellvalue === (null || "") ? 1 : heightcellvalue;
                var width = widthcellvalue === (null || "") ? 1 : widthcellvalue;
                var length = lengthcellvalue === (null || "") ? 1 : lengthcellvalue;

                if (prop.toLowerCase() == "shippingheight")
                    height = newVal;
                else if (prop.toLowerCase() == "shippingwidth")
                    width = newVal;
                else if (prop.toLowerCase() == "shippinglength")
                    length = newVal;

                if (height && width && length) {
                    var itemPoints = height * width * length;

                    hot.setDataAtRowProp(rowIndex, "ItemPoints", itemPoints);
                } else {
                    hot.setDataAtRowProp(rowIndex, "ItemPoints", "");
                }
            }

        }


        //calculate only if all columns for itemPoints are there
        //if (itemPointsColumns.length == 2) {

        //    var dimensions = hot.getDataAtRowProp(rowIndex, "dimensions");

        //    if (prop == "dimensions")
        //        dimensions = newVal;

        //    var itemPoints = "0";
        //    var oldItemPoints = hot.getDataAtRowProp(rowIndex, "ItemPoints");
        //    if (dimensions) {
        //        var dimensionsSplit = dimensions.split(' ');
        //        if (dimensionsSplit.length == 3) {
        //            itemPoints = parseFloat(dimensionsSplit[0]) *
        //                parseFloat(dimensionsSplit[1]) *
        //                parseFloat(dimensionsSplit[2]);
        //            itemPoints = parseFloat(itemPoints) || "0";
        //        }
        //    }

        //    if (itemPoints != oldItemPoints)
        //        hot.setDataAtRowProp(rowIndex, "ItemPoints", itemPoints.toString());
        //}
    }

    //if there are columns, then auto-calculate itempoints
};

var autocalculateCommodityCode = function (changes) {
    //get necessary values
    var rowIndex = changes[0];
    var prop = changes[1];
    var newVal = changes[3];

    //if (prop.toLowerCase() == "itempoints")
    //    return;

    //check if there are enough columns for itempoints auto-calculation
    if (hot) {
        var cols = hot.getSettings().columns;
        var commodityCodeColumns = [];
        $.each(cols,
            function (index, value) {
                if (value.data.toLowerCase() == "schedule-b" ||
                    value.data.toLowerCase() == "commoditycode") {
                    commodityCodeColumns.push(value.data);
                }
            });

        //calculate only if all columns for commodityCode are there
        if (commodityCodeColumns.length == 2) {

            var scheduleB = hot.getDataAtRowProp(rowIndex, "schedule-b");
            var commodityCode = hot.getDataAtRowProp(rowIndex, "CommodityCode");;

            if (prop == "schedule-b")
                scheduleB = newVal;

            var oldCommodityCode = hot.getDataAtRowProp(rowIndex, "CommodityCode");

            if (scheduleB) {
                commodityCode = scheduleB.substring(0, 4);
                commodityCode = commodityCode || "";
                if (commodityCode != oldCommodityCode)
                    hot.setDataAtRowProp(rowIndex, "CommodityCode", commodityCode.toString());
            } else {
                if (commodityCode != oldCommodityCode)
                    hot.setDataAtRowProp(rowIndex, "CommodityCode", "");
            }

        }
    }

    //if there are columns, then auto-calculate itempoints
};

var autocalculateReorderPointUpdatedOn = function (changes) {
    //get necessary values
    var rowIndex = changes[0];
    var prop = changes[1];
    var newVal = changes[3];
    
    //check if there are enough columns for reorder point last updated date auto-calculation
    if (hot) {
        var cols = hot.getSettings().columns;
        var reorderPointUpdatedOnColumns = [];
        $.each(cols,
            function (index, value) {
                if (value.data.toLowerCase() == "reorder point" ||
                    value.data.toLowerCase() == "reorder point last updated date") {
                    reorderPointUpdatedOnColumns.push(value.data);
                }
            });

        //calculate only if all columns for reorderPointUpdatedOn are there
        if (reorderPointUpdatedOnColumns.length == 2) {

            var reorderPoint = hot.getDataAtRowProp(rowIndex, "Reorder Point");
            var oldReorderPoint = changes[2];
            var reorderPointUpdatedOn = hot.getDataAtRowProp(rowIndex, "Reorder Point Last Updated Date");;

            if (prop == "Reorder Point")
                reorderPoint = newVal;

            var oldReorderPointUpdatedOn = hot.getDataAtRowProp(rowIndex, "Reorder Point Last Updated Date");

            //update reorder point updated on when reorder point changes and reorder point has value
            //fix: consider 0 as well for changes
            if (reorderPoint != oldReorderPoint && (reorderPoint != "" || reorderPoint == "0")) {
                reorderPointUpdatedOn = FormattedDate(new Date());
                reorderPointUpdatedOn = reorderPointUpdatedOn || "";
                if (reorderPointUpdatedOn != oldReorderPointUpdatedOn) {
                    hot.setDataAtRowProp(rowIndex, "Reorder Point Last Updated Date", reorderPointUpdatedOn.toString());
                }
            } else {
                if (reorderPoint != oldReorderPoint) {
                    hot.setDataAtRowProp(rowIndex, "Reorder Point Last Updated Date", "");
                }
            }
        }
    }
};

var autocalculateTargetQuantityUpdatedOn = function (changes) {
    //get necessary values
    var rowIndex = changes[0];
    var prop = changes[1];
    var newVal = changes[3];

    //check if there are enough columns for target quantity last updated date auto-calculation
    if (hot) {
        var cols = hot.getSettings().columns;
        var targetQuantityUpdatedOnColumns = [];
        $.each(cols,
            function (index, value) {
                if (value.data.toLowerCase() == "target quantity" ||
                    value.data.toLowerCase() == "target quantity last updated date") {
                    targetQuantityUpdatedOnColumns.push(value.data);
                }
            });

        //calculate only if all columns for targetQuantityUpdatedOn are there
        if (targetQuantityUpdatedOnColumns.length == 2) {

            var targetQuantity = hot.getDataAtRowProp(rowIndex, "Target Quantity");
            var oldTargetQuantity = changes[2];
            var targetQuantityUpdatedOn = hot.getDataAtRowProp(rowIndex, "Target Quantity Last Updated Date");;

            if (prop == "Target Quantity")
                targetQuantity = newVal;

            var oldTargetQuantityUpdatedOn = hot.getDataAtRowProp(rowIndex, "Target Quantity Last Updated Date");

            //update target quantity updated on when target quantity changes and target quantity has value
            //fix: consider 0 as well
            if (targetQuantity != oldTargetQuantity && (targetQuantity != "" || targetQuantity == "0")) {
                targetQuantityUpdatedOn = FormattedDate(new Date());
                targetQuantityUpdatedOn = targetQuantityUpdatedOn || "";
                if (targetQuantityUpdatedOn != oldTargetQuantityUpdatedOn) {
                    hot.setDataAtRowProp(rowIndex,
                        "Target Quantity Last Updated Date",
                        targetQuantityUpdatedOn.toString());
                }
            } else {
                if (targetQuantity != oldTargetQuantity) {
                    hot.setDataAtRowProp(rowIndex, "Target Quantity Last Updated Date", "");
                }
            }
        }
    }
};

var autocalculateProductUrl = function (changes) {
    //get necessary values
    var rowIndex = changes[0];
    var prop = changes[1];
    var newVal = changes[3];

    //check if there are enough columns for product-url
    if (hot) {
        var cols = hot.getSettings().columns;
        var productUrlColumns = [];
        $.each(cols,
            function (index, value) {
                if (value.data.toLowerCase() == "product-url" ||
                    value.data.toLowerCase() == "yahooid") {
                    productUrlColumns.push(value.data);
                }
            });

        //calculate only if all columns for product-url are there
        if (productUrlColumns.length == 2) {

            var yahooId = hot.getDataAtRowProp(rowIndex, "YahooId");
            var productUrl = hot.getDataAtRowProp(rowIndex, "product-url");

            if (prop == "YahooId")
                yahooId = newVal;

            var oldProductUrl = hot.getDataAtRowProp(rowIndex, "product-url");
            if (yahooId === "") {
                hot.setDataAtRowProp(rowIndex, "product-url", "");
            }

            if (yahooId) {
                productUrl = "https://www.skygeek.com/{0}.html".format(yahooId);
                productUrl = productUrl || "";
                if (productUrl != oldProductUrl)
                    hot.setDataAtRowProp(rowIndex, "product-url", productUrl.toString());
            }
            else {
                if (productUrl != oldProductUrl) {
                    hot.setDataAtRowProp(rowIndex, "product-url", "");
                }
            }

        }
    }

    //if there are columns, then auto-calculate itempoints
};

var autoCalculateYahooId = function (changes) {
    //get necessary values
    var rowIndex = changes[0];
    var prop = changes[1];
    var newVal = changes[3];

    //check if there are enough columns for yahooid auto-calculation
    if (hot) {
        var cols = hot.getSettings().columns;
        var yahooIdColumns = [];
        $.each(cols,
            function (index, value) {
                if (value.data.toLowerCase() == "yahooid" ||
                    value.data.toLowerCase() == "localsku" ||
                    value.data.toLowerCase() == "code" ||
                    value.data.toLowerCase() == "manufacturer" ||
                    value.data.toLowerCase() == "name") {
                    yahooIdColumns.push(value.data);
                }
            });

        //calculate only if all columns for yahooid are there
        if (yahooIdColumns.length == 4 || yahooIdColumns.length == 5 ) {

            var code = hot.getDataAtRowProp(rowIndex, "code");
            var localSku = hot.getDataAtRowProp(rowIndex, "LocalSku");
            var mpn = hot.getDataAtRowProp(rowIndex, "manufacturer");
            var name = hot.getDataAtRowProp(rowIndex, "Name");

            if (prop == "code")
                code = newVal;
            if (prop == "LocalSku")
                localSku = newVal;
            if (prop == "manufacturer")
                mpn = newVal;
            if (prop == "Name")
                name = newVal;

            var yahooId = "";
            var oldYahooId = hot.getDataAtRowProp(rowIndex, "YahooId");
            if (code || localSku || mpn || name) {
                yahooId = udh.utilities.getYahooId(code || localSku, mpn, name);
            }
            oldYahooId = oldYahooId || "";

            if (yahooId != oldYahooId) {
               
                if (oldYahooId === "" || udh.settings.customfields.replaceExistingYahooIdWithAutoCalculatedValue === true) {
                    if (yahooIdColumns.indexOf('yahooId') > -1) {
                        hot.setDataAtRowProp(rowIndex, "YahooId", yahooId.toString());
                    }
                }
            }
        }
    }

};

//TODO: Complete this implementation and replace autoCalculateYahooId with autoCalculateYahooId2
var autoCalculateYahooId2 = function (changes) {
    //get necessary values
    var rowIndex = changes[0];
    var prop = changes[1];
    var newVal = changes[3];

    if (prop.toLowerCase() !== "yahooid" &&
        prop.toLowerCase() !== "localsku" ||
        prop.toLowerCase() !== "code" &&
        prop.toLowerCase() !== "manufacturer" &&
        prop.toLowerCase() !== "Name") {
        return;
    }

    //check if instance of hot exists for handsontable
    if (hot) {

        var hotOption = {
            mode: udh.constants.mode.HOT, //mode is FORM or HOT (default is FORM)

            //target property - yahooId - calculated based on mpn & partNumber & name
            yahooId: "YahooId",

            //source properties - local part number (LocalSku), name & manufacturer part number (MPN)
            partNumber: "LocalSku",
            name: "Name",
            mpn: "manufacturer",

            additionalData: hot.getSettings().data[rowIndex]
        }

        yahooIdCalculation.initialize(hotOption);

    }

};


var autoCalcualteHazmatType = function (changes) {
    var rowIndex = changes[0];
    var prop = changes[1];
    var newVal = changes[3];

    if (hot) {
        var cols = hot.getSettings().columns;
        var HazmatTypeColumns = [];
        $.each(cols,
            function (index, value) {
                if (value.data.toLowerCase() == "un number" ||
                    value.data.toLowerCase() == "un class" ||
                    value.data.toLowerCase() == "packing group" ||
                    value.data.toLowerCase() == "hazmat type")
                    HazmatTypeColumns.push(value.data)

            });

        
        if (HazmatTypeColumns.length == 4) {

            var hazmatNumber = hot.getDataAtRowProp(rowIndex, "UN Number");
            var hazmatClass = hot.getDataAtRowProp(rowIndex, "UN Class");
            var hazmatGroup = hot.getDataAtRowProp(rowIndex, "Packing Group");

            if (prop == "UN Number")
                hazmatNumber = newVal;
            if (prop == "UN Class")
                hazmatClass = newVal;
            if (prop == "Packing Group")
                hazmatGroup = newVal;

            var hazmatType = "";
            var hazmatTypeOld = hot.getDataAtRowProp(rowIndex, "Hazmat Type");
            if (hazmatNumber || hazmatClass || hazmatGroup) {
                if (hazmatNumber != "")
                { hazmatType = hazmatType + ' ' + hazmatNumber; }
                if (hazmatClass != "")
                { hazmatType = hazmatType + ' ' + hazmatClass; }
                if (hazmatGroup != "")
                { hazmatType = hazmatType + ' ' + hazmatGroup; }
                hazmatType.trim();

            }
            hot.setDataAtRowProp(rowIndex, "Hazmat Type", hazmatType.toString());

        }
    }
};

//var autocalculateMobileLinkUrl = function (changes) {
//    //get necessary values
//    var rowIndex = changes[0];
//    var prop = changes[1];
//    var newVal = changes[3];

//    //check if there are enough columns for product-url
//    if (hot) {
//        var cols = hot.getSettings().columns;
//        var mobileUrlColumns = [];
//        $.each(cols,
//            function (index, value) {
//                if (value.data.toLowerCase() == "mobilelink" ||
//                    value.data.toLowerCase() == "yahooid") {
//                    mobileUrlColumns.push(value.data);
//                }
//            });

//        //calculate only if all columns for product-url are there
//        if (mobileUrlColumns.length == 2) {

//            var yahooId = hot.getDataAtRowProp(rowIndex, "YahooId");
//            var mobileLinkUrl = hot.getDataAtRowProp(rowIndex, "MobileLink");;

//            if (prop == "YahooId")
//                yahooId = newVal;

//            var oldMobileLinkUrl = hot.getDataAtRowProp(rowIndex, "MobileLink");

//            if (yahooId) {
//                mobileLinkUrl = "http://m.skygeek.com/{0}.html".format(yahooId);
//                mobileLinkUrl = mobileLinkUrl || "";
//                if (mobileLinkUrl != oldMobileLinkUrl)
//                    hot.setDataAtRowProp(rowIndex, "MobileLink", mobileLinkUrl.toString());
//            } else {
//                if (mobileLinkUrl != oldMobileLinkUrl)
//                    hot.setDataAtRowProp(rowIndex, "MobileLink", "");
//            }

//        }
//    }

//    //if there are columns, then auto-calculate itempoints
//};

var autocalculateImageUrl = function (changes) {
    //get necessary values
    var rowIndex = changes[0];
    var prop = changes[1];
    var newVal = changes[3];

    //check if there are enough columns for product-url
    if (hot) {
        var cols = hot.getSettings().columns;
        var imageUrlColumns = [];
        $.each(cols,
            function (index, value) {
                if (value.data.toLowerCase() == "image url" ||
                    value.data.toLowerCase() == "yahooid") {
                    imageUrlColumns.push(value.data);
                }
            });

        //calculate only if all columns for product-url are there
        if (imageUrlColumns.length == 2) {

            var yahooId = hot.getDataAtRowProp(rowIndex, "YahooId");
            var ImageUrl = hot.getDataAtRowProp(rowIndex, "Image URL");

            if (prop == "YahooId")
                yahooId = newVal;

            var oldImageUrl = hot.getDataAtRowProp(rowIndex, "Image URL");

            if (yahooId && oldImageUrl === "")
            {

               //feature-SKYG-690 ImageUrl = "http://feeds3.yourstorewizards.com/4481/images/full/{0}.jpg".format(yahooId);
                
                ImageUrl = "https://sep.yimg.com/ty/cdn/stylespilotshop/image_coming_soon.jpg";
                ImageUrl = ImageUrl || "";
                if (ImageUrl != oldImageUrl)
                    hot.setDataAtRowProp(rowIndex, "Image URL", ImageUrl.toString());
            }
            else {
                if (ImageUrl != oldImageUrl)
                    hot.setDataAtRowProp(rowIndex, "Image URL", "");
            }

        }
    }

    //if there are columns, then auto-calculate itempoints
};