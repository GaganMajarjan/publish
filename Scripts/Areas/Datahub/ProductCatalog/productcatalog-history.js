﻿$(document)
    .ready(function () {
        var historyDataTable = null;
        var loadHistoryTable = function () {
            if (historyDataTable == null) {
                historyDataTable = $('#PIMProductHistoryDataTable')
                    .dataTable({
                        "deferRender": true,
                        "bServerSide": true,
                        "stateSave": false,
                        "bSort": true,
                        "searching": false,
                        "paging": true,
                        "aLengthMenu": dataTableMenuLength,
                        "iDisplayLength": dataTableDisplayLength,
                        "sAjaxSource": getProductHistoryTable,
                        "bProcessing": true,
                        //"scrollX": true,
                        "fnPreDrawCallback": function () {
                            // gather info to compose a message
                            SGloading("Loading");
                            //SGtableloading('fa-plane', '', this);
                        },
                        searchDelay: 500,
                        "fnDrawCallback": function () {
                            // in case your overlay needs to be put away automatically you can put it here
                            SGloadingRemove();
                            //rebuilttooltip();
                        },
                        "order": [[3, "desc"]]
                        //,
                        //"oLanguage": {
                        //    "sEmptyTable": "No history found for this product"
                        //}

                    });

                // Ajax call
                $.ajax({
                    type: 'GET',
                    url: getProductSeomStatus,
                    cache: false,
                    contentType: 'application/json; charset=utf-8',
                    //dataType: 'json',
                    //data: data,
                    beforeSend: function () {
                        //$("#btnsave").attr('disabled', true);
                        SGloading('Loading');
                    },
                    success: function (response) {
                        if (response != null && response.Status === true) {
                            if (response.Data) {
                                //$("#divPCHistoryExportedToSEOM").addClass("text-success");
                                $("#spanPCHistoryExportedToSEOM").text(response.Data);

                                //Waiting, In Progress, Success, Failed, Synchronized
                                if (response.Data == "Waiting")
                                    $("#divPCHistoryExportedToSEOM").addClass("text-warning");
                                else if (response.Data == "In Progress")
                                    $("#divPCHistoryExportedToSEOM").addClass("text-info");
                                else if (response.Data == "Success")
                                    $("#divPCHistoryExportedToSEOM").addClass("text-success");
                                else if (response.Data == "Failed")
                                    $("#divPCHistoryExportedToSEOM").addClass("text-danger");
                                else if (response.Data == "Synchronized")
                                    $("#divPCHistoryExportedToSEOM").addClass("text-success");
                            }
                            //alertmsg("green", response.Message);
                        } else if (response != null && response.Status === false) {
                            $("#divPCHistoryExportedToSEOM").addClass("text-danger");
                            alertmsg("red", response.Message);
                        } else {
                            $("#divPCHistoryExportedToSEOM").addClass("text-danger");
                            alertmsg("red", "An Error occurred");
                        }

                    },
                    complete: function () {
                        //$("#btnsave").attr('disabled', false);
                        SGloadingRemove();
                        $("#spanPCHistoryExportedToSEOM")
                            .removeClass("fa")
                            .removeClass("fa-plane")
                            .removeClass("fa-spin");
                    },
                    error: function (xhr, status, error) {
                        //reset variables here
                        console.log(xhr, status, error);
                        alert(status + ' ' + error);
                    }
                });

            }

        }

        //loadHistoryTable();

        $("#tab_pc_history")
            .on('click',
                function (e) {
                    e.preventDefault();
                    //historyDataTable
                    loadHistoryTable();
                    //historyDataTable=$('#PIMProductHistoryDataTable').dataTable();
                    $(this).tab('show');
                });

    });