﻿//This javasript file holds all the transformation rule required for product/kit

//SG-362: Product Edit - Product URL needs to be calculated.

var calculateItemPoints = function () {
    //var dimensions = $('[data-name="dimensions"]');
    //var itempoints = $('[data-name="ItemPoints"]');

    //if (itempoints && dimensions && dimensions.val()) {
    //    var splitDimensions = dimensions.val().split(' ');
    //    if (splitDimensions.length == 3 &&
    //        $.isNumeric(parseFloat(splitDimensions[0])) &&
    //        $.isNumeric(parseFloat(splitDimensions[1])) &&
    //        $.isNumeric(parseFloat(splitDimensions[2]))) {
    //        itempoints.val(parseFloat(splitDimensions[0]) * parseFloat(splitDimensions[1]) * parseFloat(splitDimensions[2]));
    //    } else {
    //        itempoints.val("0");
    //    }
    //}

    var height = $('[data-name="Shipping_Height"]');
    var width = $('[data-name="Shipping_Width"]');
    var length = $('[data-name="Shipping_Length"]');
    var itempoints = $('[data-name="ItemPoints"]');
    var heightValue = parseFloat(height.val());
    var widthValue = parseFloat(width.val());
    var lengthValue = parseFloat(length.val())

    if (itempoints && height && width && length) {
        if (!$.isNumeric(heightValue) &&
            !$.isNumeric(widthValue) &&
            !$.isNumeric(lengthValue)) {
            itempoints.val("");
           
        } else {
            if (!$.isNumeric(heightValue))
            { heightValue = 1; }
            if (!$.isNumeric(widthValue))
            { widthValue = 1; }
            if (!$.isNumeric(lengthValue))
            { lengthValue = 1; }

            itempoints.val(heightValue * widthValue * lengthValue);
        }
    }


}

//SG-605: Custom Field - CommodityCode is just the first 4 characters of Schedule-B

var calculateCommodityCode = function () {
    var scheduleB = $('[data-name="schedule_b"]');
    var commodityCode = $('[data-name="CommodityCode"]');

    if (commodityCode && scheduleB) {
        var valScheduleB = scheduleB.val();
        if (valScheduleB) {
            if (valScheduleB.length > 4)
                commodityCode.val(scheduleB.val().substring(0, 4));
            else {
                commodityCode.val(scheduleB.val());
            }
        }
    }
}

//SG-686- Product Edit - Reorder Point/Target Quantity Last Updated Date Fields
var calculateReorderPointUpdatedOn = function () {
    var reorderPointUpdatedOn = $("#PricingCustomField_reorderQuantityUpdatedOn");
    var reorderPoint = $("#PricingCustomField_reorderQuantity");

    if (reorderPointUpdatedOn && reorderPoint) {
        var valReorderPoint = reorderPoint.val();
        if (valReorderPoint) {
            reorderPointUpdatedOn.val(FormattedDate(new Date())).change();
        } else {
            reorderPointUpdatedOn.val("");
        }
    }
}

var calculateTargetQuantityUpdatedOn = function () {
    var targetQuantityUpdatedOn = $("#PricingCustomField_reorderPointUpdatedOn");
    var targetQuantity = $("#PricingCustomField_reorderPoint");

    if (targetQuantityUpdatedOn && targetQuantity) {
        var valTargetQuantity = targetQuantity.val();
        if (valTargetQuantity) {
            targetQuantityUpdatedOn.val(FormattedDate(new Date())).change();
        } else {
            targetQuantityUpdatedOn.val("");
        }
    }
}

//SG-362: Product Edit - Product URL needs to be calculated.
var calculateProductUrl = function (options) {
    options = options || {};
    var productUrl = $('[data-name="product_url"]');
    var yahooId = $('[data-name="YahooId"]');

    if (productUrl && yahooId) {

        if ((productUrl.val() === "" || options.override) && yahooId.val() !== "") {
            productUrl.val("https://www.skygeek.com/{0}.html".format(yahooId.val()));
        }

        yahooId.change(function () {

            if (yahooId.val() === "") {
                productUrl.val("");
                return;
            }

            productUrl.val("https://www.skygeek.com/{0}.html".format(yahooId.val()));

        });
    }
}
//SKYG-718
var calculateIdentifierExists = function () {

   
   // console.log($('[data-name="brand"]').val());
   // var brand = ($('[data-name="brand"]').val() !== null && $('[data-name="brand"]').val() !== undefined) ? $('[data-name="brand"]').val().trim() : "";
    var brand = ($('#BrandId option:selected').val() !== null && $('#BrandId option:selected').val() !== undefined) ? $('#BrandId option:selected').val() : "";


    var manufacturerPN = ($('[data-name="MPN"]').val() !== null && $('[data-name="MPN"]').val() !== undefined) ? $('[data-name="MPN"]').val().trim() : "";
    var gtin = ($('[data-name="GTIN"]').val() !== null && $('[data-name="GTIN"]').val() !== undefined) ? $('[data-name="GTIN"]').val().trim() : "";


    var identifierExists = $('[data-name="IdentifierExists"]');

    if ((brand !== "") && (manufacturerPN !== "")) {
        identifierExists.val(1);
    }
    else if ((brand !== "") && (gtin !== "")) {
        identifierExists.val(1);
    }
    else {
        identifierExists.val(0);
    }
    
}
//var calculate MobileLinkUrl = function (options) {
//    options = options || {};
//    var mobileUrl = $('[data-name="MobileLink"]');
//    var yahooId = $('[data-name="YahooId"]');

//    if (mobileUrl && yahooId) {

//        if ((mobileUrl.val() === "" || options.override) && yahooId.val() !== "") {
//            mobileUrl.val("http://m.skygeek.com/{0}.html".format(yahooId.val()));
//        }

//        yahooId.change(function () {

//            if (yahooId.val() === "") {
//                mobileUrl.val("");
//                return;
//            }

//            mobileUrl.val("http://m.skygeek.com/{0}.html".format(yahooId.val()));

//        });
//    }
//}


var calculateImageUrl = function (options) {
    options = options || {};
    var imageUrl = $('[data-name="image"]');
    var yahooId = $('[data-name="YahooId"]');
    if (imageUrl && yahooId) {
        if (imageUrl.val() === "") {
            imageUrl.val("https://sep.yimg.com/ty/cdn/stylespilotshop/image_coming_soon.jpg");
        }
    }
}




var calculateImageUrl1 = function (options) {
    options = options || {};
    var imageUrl = $('[data-name="image"]');
    var yahooId = $('[data-name="YahooId"]');
    if (imageUrl && yahooId) {
      
        //feature SKYG-690 && udh.settings.customfields.replaceExistingYahooIdWithAutoCalculatedValue === true
        if ((imageUrl.val() === "" || options.override) &&  yahooId.val() !== "" && udh.settings.customfields.replaceExistingYahooIdWithAutoCalculatedValue === true)
        {
            // imageUrl.val("http://feeds3.yourstorewizards.com/4481/images/full/{0}.jpg".format(yahooId.val()));
            imageUrl.val("https://sep.yimg.com/ty/cdn/stylespilotshop/image_coming_soon.jpg");
        }
    }
}


//SG-687, SKYG-27 - Product Create - Automatically create Yahoo ID

/**
 * {yahooIdCalculation} - module to facilitate yahooId calculation
 * @returns {nothing} 
 */
var yahooIdCalculation = yahooIdCalculation || {
   
    //properties
    yahooId: null,
    partNumber: null,
    productName: null,
    mpn: null,
    //options
    options: {},

    //defaultOption is in function() because we want it to be in private scope of yahooIdCalculation
    defaultOption: function () {
        return {
            //default mode is FORM
            mode: udh.constants.mode.FORM
        };
    },

    /**
     * {hasAllFields} - checks if all fields are present
     * @returns {true} if all fields are present, {false} otherwise 
     */
    hasAllFields: function () {
        return (yahooIdCalculation.yahooId && yahooIdCalculation.partNumber && yahooIdCalculation.productName && yahooIdCalculation.mpn );
    },

    /**
     * {atLeastOneSourceFieldHasValue} - checks if all fields (except yahooId) have values
     * @returns {true} if atLeastOneSourceFieldHasValue, {false} otherwise 
     */
    atLeastOneSourceFieldHasValue: function () {
        var partNumber = udh.utilities.getValue(yahooIdCalculation.options.mode,
            yahooIdCalculation.partNumber,
            yahooIdCalculation.options.additionalData);
        var mpn = udh.utilities.getValue(yahooIdCalculation.options.mode,
            yahooIdCalculation.mpn,
            yahooIdCalculation.options.additionalData);
        var productName = udh.utilities.getValue(yahooIdCalculation.options.mode,
            yahooIdCalculation.productName,
            yahooIdCalculation.options.additionalData);

        return (partNumber !== "" || mpn !== "" || productName !== "" );
    },

    /**
     * {clearYahooId} - clears yahoo Id
     * @returns {nothing} 
     */
    clearYahooId: function () {
        udh.utilities.setValue(yahooIdCalculation.options.mode,
            yahooIdCalculation.yahooId,
            "");
        //yahooIdCalculation.yahooId.val("");
    },

    /**
     * {isYahooIdBlank} - check if yahoo Id is blank
     * @returns {true} if yahooId is blank, {false} otherwie 
     */
    isYahooIdBlank: function () {
        var yahooId = udh.utilities.getValue(yahooIdCalculation.options.mode,
            yahooIdCalculation.yahooId,
            yahooIdCalculation.options.additionalData);
        return (yahooIdCalculation.yahooId && yahooId == "");
    },

    /**
     * {getNewYahooId} - get new yahooId from partNumber, mpn & name
     * @returns {yahooId} - calculated value of yahooId
     */
    getNewYahooId: function () {
        if (!yahooIdCalculation.hasAllFields()) {
            console.error("LocalSku, Manufacturer PN, Name and YahooId should be present");
            return null;
        }

        var partNumber = udh.utilities.getValue(yahooIdCalculation.options.mode,
            yahooIdCalculation.partNumber,
            yahooIdCalculation.options.additionalData);
        var mpn = udh.utilities.getValue(yahooIdCalculation.options.mode,
            yahooIdCalculation.mpn,
            yahooIdCalculation.options.additionalData);
        var productName = udh.utilities.getValue(yahooIdCalculation.options.mode,
            yahooIdCalculation.productName,
            yahooIdCalculation.options.additionalData);

      
        var newYahooId = udh.utilities.getYahooId(
            partNumber,
            mpn,
            productName);
        return newYahooId;

    },
    /** 
     * {setupEvents} setup event change from user changes any one of manufacturerpartnumber, partnumber or name
     * @param {none}
     * @returns {nothing}
    */
    setupEvents: function () {

        //proceed forward if all fields exist
        if (yahooIdCalculation.hasAllFields()) {
            //handler for partnumber change
            yahooIdCalculation.partNumber.change(function () {

                var partNumber = udh.utilities.getValue(yahooIdCalculation.options.mode,
                    yahooIdCalculation.partNumber,
                    yahooIdCalculation.options.additionalData);
                if (partNumber !== "") {
                    //on local sku change, set it to uppercase
                    udh.utilities.setValue(yahooIdCalculation.options.mode,
                        yahooIdCalculation.partNumber,
                        partNumber.toUpperCase(),
                        yahooIdCalculation.options.additionalData);
                }

                if (yahooIdCalculation.atLeastOneSourceFieldHasValue()) {
                    var newYahooId = yahooIdCalculation.getNewYahooId();
                    var oldYahooId = udh.utilities.getValue(yahooIdCalculation.options.mode,
                        yahooIdCalculation.yahooId,
                        yahooIdCalculation.options.additionalData);
                    if (oldYahooId === "" || udh.settings.customfields.replaceExistingYahooIdWithAutoCalculatedValue === true) {
                        udh.utilities.setValue(yahooIdCalculation.options.mode,
                            yahooIdCalculation.yahooId,
                            newYahooId,
                            yahooIdCalculation.options.additionalData);
                    }
                }

                if (yahooIdCalculation.options.mode == udh.constants.mode.FORM) {
                    calculateProductUrl({ override: true });
                    //calculateMobileLinkUrl({ override: true });
                    calculateImageUrl({ override: true});
                }
            });

            //handler for mpn change
            yahooIdCalculation.mpn.change(function () {
             
                if (yahooIdCalculation.atLeastOneSourceFieldHasValue()) {
                    var newYahooId = yahooIdCalculation.getNewYahooId();
                    var oldYahooId = udh.utilities.getValue(yahooIdCalculation.options.mode,
                       yahooIdCalculation.yahooId,
                       yahooIdCalculation.options.additionalData);
                    if (oldYahooId === "" ||
                        udh.settings.customfields.replaceExistingYahooIdWithAutoCalculatedValue === true) {
                        udh.utilities.setValue(yahooIdCalculation.options.mode,
                            yahooIdCalculation.yahooId,
                            newYahooId,
                            yahooIdCalculation.options.additionalData);
                    }
                }

                if (yahooIdCalculation.options.mode == udh.constants.mode.FORM) {
                    calculateProductUrl({ override: true });
                   // calculateMobileLinkUrl({ override: true });
                    calculateImageUrl({ override: true });

                }
            });
            
            //handler for name change
            yahooIdCalculation.productName.change(function () {
             
                if (yahooIdCalculation.atLeastOneSourceFieldHasValue()) {
                    var newYahooId = yahooIdCalculation.getNewYahooId();
                    var oldYahooId = udh.utilities.getValue(yahooIdCalculation.options.mode,
                       yahooIdCalculation.yahooId,
                       yahooIdCalculation.options.additionalData);
                    if (oldYahooId === "" ||
                        udh.settings.customfields.replaceExistingYahooIdWithAutoCalculatedValue === true) {
                        udh.utilities.setValue(yahooIdCalculation.options.mode,
                            yahooIdCalculation.yahooId,
                            newYahooId,
                            yahooIdCalculation.options.additionalData);
                    }
                }

                if (yahooIdCalculation.options.mode == udh.constants.mode.FORM) {
                    calculateProductUrl({ override: true });
                    //calculateMobileLinkUrl({ override: true });
                    calculateImageUrl({ override: true });
                }
            });

            yahooIdCalculation.brand.change(function () {
                calculateIdentifierExists(); //SKYG-718

            });
            yahooIdCalculation.manufacturerPN.change(function () {
                calculateIdentifierExists();  //SKYG-718

            });
            yahooIdCalculation.gtin.change(function () {
                calculateIdentifierExists();  //SKYG-718
            });
        }
    },

    /** 
    * {triggerChange} calculates yahoo id based on manufacturerpartnumber, partnumber or name and overrides the value to yahooId
    * @param {none}
    * @returns {nothing}
   */
    triggerChange: function () {
        if (yahooIdCalculation.hasAllFields() && yahooIdCalculation.atLeastOneSourceFieldHasValue()) {

            var partNumber = udh.utilities.getValue(yahooIdCalculation.options.mode,
                yahooIdCalculation.partNumber,
                yahooIdCalculation.options.additionalData);
            var mpn = udh.utilities.getValue(yahooIdCalculation.options.mode,
                yahooIdCalculation.mpn,
                yahooIdCalculation.options.additionalData);
            var productName = udh.utilities.getValue(yahooIdCalculation.options.mode,
                yahooIdCalculation.productName,
                yahooIdCalculation.options.additionalData);
            var newYahooId = udh.utilities.getYahooId(
                partNumber,
                mpn,
                productName);

            var oldYahooId = udh.utilities.getValue(yahooIdCalculation.options.mode,
                       yahooIdCalculation.yahooId,
                       yahooIdCalculation.options.additionalData);
            if (oldYahooId === "" || udh.settings.customfields.replaceExistingYahooIdWithAutoCalculatedValue === true) {
                udh.utilities.setValue(yahooIdCalculation.options.mode,
                    yahooIdCalculation.yahooId,
                    newYahooId,
                    yahooIdCalculation.options.additionalData);
            }
            //yahooIdCalculation.yahooId.val(newYahooId);
        }
    },

    /**
     * {initialize} - function to initialize the module
     * @returns {nothing} 
     */
    initialize: function (options) {

        //if options is null, set {}
        options = options || {};

        //extend/merge defaultOption & options
        yahooIdCalculation.options = $.extend(yahooIdCalculation.defaultOption, options);

        //properties initialization in initializer
        yahooIdCalculation.yahooId = yahooIdCalculation.options.yahooId || null;
        yahooIdCalculation.partNumber = yahooIdCalculation.options.partNumber || null;
        yahooIdCalculation.productName = yahooIdCalculation.options.productName || null;
        yahooIdCalculation.mpn = yahooIdCalculation.options.mpn || null;
        yahooIdCalculation.brand = yahooIdCalculation.options.brand || null;
        yahooIdCalculation.manufacturerPN = yahooIdCalculation.options.manufacturerPN || null;
        yahooIdCalculation.gtin = yahooIdCalculation.options.gtin || null;
        //check if value is blank, if blank change value for yahooId
        if (yahooIdCalculation.isYahooIdBlank()) {
            yahooIdCalculation.triggerChange();
            calculateProductUrl();
            
        }

        //setup and listen for yahooId change
        yahooIdCalculation.setupEvents();
    }

}

$(function () {
    $('[data-name="ItemPoints"]').removeAttr('disabled');
    $('[data-name="hazmat_type"]').removeAttr('disabled');
     //SKYG-718
    if ($('[data-name="IdentifierExists"]').val() === "") {
        $('[data-name="IdentifierExists"]').val(0);
    }
    $('[data-name="IdentifierExists"]').attr('readonly', true);
    $('[data-name="GoogleItemIdOverride"]').attr('readonly', true);
    $('[data-name="product_url"]').attr('readonly', true);
    var dimensions = $('[data-name="dimensions"]');
    var itempoints = $('[data-name="ItemPoints"]');
    var height = $('[data-name="Shipping_Height"]');
    var width = $('[data-name="Shipping_Width"]');
    var length = $('[data-name="Shipping_Length"]');
   

    //var cfAutocalculateItempointsIfEmptyOnStart; = true
    //auto-calculate itempoints \\on-load
    if (udh.settings.customfields.autocalculateItemPointsIfEmpty) {
        if (itempoints && !itempoints.val())
            calculateItemPoints();
    }

    if (height) {
        $(height)
            .change(function () {
                calculateItemPoints();
            });
    }
    if (width) {
        $(width)
            .change(function () {
                calculateItemPoints();
            });
    }
    if (length) {
        $(length)
            .change(function () {
                calculateItemPoints();
            });
    }

    //if (dimensions) {
    //    $(dimensions)
    //        .change(function () {
    //            calculateItemPoints();
    //        });
    //}

    //auto calculate commodity code (on load)
    var scheduleB = $('[data-name="schedule_b"]');
    if (udh.settings.customfields.autocalculateCommodityCodeIfEmpty) {
        var commodityCode = $('[data-name="CommodityCode"]');
        if (commodityCode && !commodityCode.val()) {
            calculateCommodityCode();
        }
    }

    if (scheduleB) {
        $(scheduleB)
            .change(function () {
                calculateCommodityCode();
            });
    }

    //listen to changes for reorder point
    var reorderPointUpdatedOn = $("#PricingCustomField_reorderQuantity");
    var reorderPoint = $("#PricingCustomField_reorderQuantityUpdatedOn");
    if (reorderPoint && reorderPointUpdatedOn) {
        $("body")
            .on("change",
                "#PricingCustomField_reorderQuantity",
                function() {
                    console.log("reorder point has changed -- on");
                    calculateReorderPointUpdatedOn();

                });
    }

    //listen to changes for target quantity
    var targetQuantityUpdatedOn = $("#PricingCustomField_reorderPoint");
    var targetQuantity = $("#PricingCustomField_reorderPointUpdatedOn");
    if (targetQuantity && targetQuantityUpdatedOn) {
        //targetQuantity.change(function() {
        //    calculateTargetQuantityUpdatedOn();
        //});
        $("body")
            .on("change",
                "#PricingCustomField_reorderPoint",
                function() {
                    calculateTargetQuantityUpdatedOn();
                });
    }

    var yahooIdOption = {
        mode: udh.constants.mode.FORM, //mode is FORM or HOT (default is FORM)

        //target property - yahooId - calculated based on mpn & partNumber & name
        yahooId: $('[data-name="YahooId"]'), //hot.getSettings().data[i]["YahooId"]

        //source properties - local part number (LocalSku), name & manufacturer part number (MPN)
        partNumber: ((typeof $('#LocalSku')[0] !== 'undefined') ? $('#LocalSku') : $("#KitSKU")),
        productName: $('#Name'),
        mpn: $('[data-name="manufacturer"]'),
        brand: $('#BrandId'),
        manufacturerPN: $('[data-name="MPN"]'),
        gtin: $('[data-name="GTIN"]')
       
    };

    /*
    var hotOption = {
        mode: udh.constants.mode.HOT, //mode is FORM or HOT (default is FORM)

        //target property - yahooId - calculated based on mpn & partNumber & name
        yahooId: hot.getSettings().data[i]["YahooId"],

        //source properties - local part number (LocalSku), name & manufacturer part number (MPN)
        partNumber: hot.getSettings().data[i]["LocalSku"],
        name: hot.getSettings().data[i]["Name"],
        mpn: hot.getSettings().data[i]["MPN"]
    }
    */

    yahooIdCalculation.initialize(yahooIdOption);

    //auto calculate product-url (on load)
    var yahooId = $('[data-name="YahooId"]');
    if (udh.settings.customfields.autocalculateProductUrlIfEmpty) {
        var productUrl = $('[data-name="product_url"]');
        if (productUrl && !productUrl.val()) {
            calculateProductUrl();
        }
    }
    //if (udh.settings.customfields.autocalculateMobileLinkUrlIfEmpty) {
    //    var mobileLinkUrl = $('[data-name="MobileLink"]');
    //    if (mobileLinkUrl && !mobileLinkUrl.val()) {
    //        calculateMobileLinkUrl();
    //    }
    //}
     //SKYG-718
    if (udh.settings.customfields.autocalculateIdentifier) {
        var identifier = $('[data-name="IdentifierExists"]');
        if (identifier) {
            calculateIdentifierExists();
        }
    }
    if (udh.settings.customfields.autocalculateImageUrlIfEmpty) {
        var imageLinkUrl = $('[data-name="image"]');
        if (imageLinkUrl && !imageLinkUrl.val()) {
            calculateImageUrl();
        }
    }

    if (yahooId) {
        $(yahooId)
            .change(function () {
                calculateProductUrl();
              //  calculateMobileLinkUrl();
                calculateImageUrl();
            });
        $(yahooId)
            .change();
    }

    var UnitedNationsNumber = $('[data-name = "UnitedNationsNumber"]');
    var UnitedNationsClass = $('[data-name = "UnitedNationsClass"]');
    var PackingGroup = $('[data-name = "PackingGroup"]');
    var HazmatBoxFee = $('[data-name="HazmatBoxFee"]');
    var BatchLotTracking = $('[data-name="BatchLotTracking"]');
    var FulfillmentCenter = $('[data-name="FulfillmentCenter"]');
    //$("[data-name='FulfillmentCenter'] option:first-child").attr("disabled", "disabled");//DaniUpdate1212020
  
    $(HazmatBoxFee).change(function () {
        var data = $(HazmatBoxFee).val().trim();
        if (data === "" || data === null) {
            HazmatBoxFee.val("0.00");
        }
    })
    $(BatchLotTracking).change(function () {
        var data = $(BatchLotTracking).val().trim();
        if (data === "" || data === null) {
            BatchLotTracking.val("No");
        }
    })
    
    if (UnitedNationsNumber && UnitedNationsClass && PackingGroup)
    {
        $(UnitedNationsNumber).change(function () {
            calculateHazmatType();
        })

        $(UnitedNationsClass).change(function () {
            calculateHazmatType();
        })

        $(PackingGroup).change(function () {
            calculateHazmatType();
        })
    }


    function calculateHazmatType() {
        var UnitedNationsNumber = $('[data-name = "UnitedNationsNumber"]');
        var UnitedNationsClass = $('[data-name = "UnitedNationsClass"]');
        var PackingGroup = $('[data-name = "PackingGroup"]');
        var hazmatType = $('[data-name = "hazmat_type"]');
        var hazmatTypeValue = UnitedNationsNumber.val() + ' ' + UnitedNationsClass.val() + ' ' + PackingGroup.val();
        hazmatTypeValue = hazmatTypeValue.replace('  ', ' ').trim();
        hazmatType.val(hazmatTypeValue);
    }
});



