﻿
//custom field manager viewmodel
var CustomFieldManagerViewModel = function (vm) {

    var self = this;
    self.CustomField = ko.observable(vm != null && vm.CustomField != null ? vm.CustomField : '');
    self.FieldValue = ko.observable(vm != null && vm.FieldValue != null ? vm.FieldValue : '');
    self.FieldValueBoolean = ko.observable(vm != null && vm.FieldValueBoolean != null ? vm.FieldValueBoolean : '');
    self.AllowedValuesSdtId = ko.observable();
    self.FieldAlias = ko.observable(vm != null && vm.FieldAlias != null ? vm.FieldAlias : '');

    self.FieldValueIsDirty = ko.observable(false);

    self.CustomField.subscribe(function (val) {

        if (val == null)
            return;

        var customFieldId = val;
        if (customFieldId !== "") {

            viewModel.hasCustomFieldValue(true);

            var countDuplication = 0;

            //Check for duplicate
            for (var i = 0; i < viewModel.CustomTemplates().length; i++) {

                var customTemplate = viewModel.CustomTemplates()[i];

                if (customTemplate != null && customTemplate.CustomField() != null) {
                    if (parseFloat(customFieldId) === parseFloat(customTemplate.CustomField()) && $.isNumeric(customFieldId) === true) {

                        countDuplication++;

                    }
                }
            }

            if (parseFloat(countDuplication) > 1) {

                // found existing custom field
                alertmsg('red', 'Custom Field cannot be duplicate');
                self.CustomField('');

                return;
            }

            RenderElementWithDataType(customFieldId, "false");

            return;
        }
        viewModel.hasCustomFieldValue(false);

    });

    self.FieldValue.subscribe(function (val) {

        var customFieldId = self.CustomField();
        var oldValue = $.grep(productCatalogVm.CustomTemplates, function (element, index) {
            return element.CustomField === customFieldId;
        });

        var newValue = val;
        // //console.log(oldValue[0].FieldValueBoolean);
        ////console.log(newValue);
        if (oldValue.length > 0) {
            if (oldValue[0].FieldValue !== newValue)
                self.FieldValueIsDirty(true);
            else
                self.FieldValueIsDirty(false);
        }
        else
            self.FieldValueIsDirty(true);
    });

    self.FieldValueBoolean.subscribe(function () {
        var customFieldId = self.CustomField();
        var oldValue = $.grep(productCatalogVm.CustomTemplates, function (element, index) {
            return element.CustomField === customFieldId;
        });

        var newValue = self.FieldValueBoolean();
        // //console.log(oldValue[0].FieldValueBoolean);
        // //console.log(newValue);

        if (oldValue.length > 0) {
            if (oldValue[0].FieldValueBoolean !== newValue)
                self.FieldValueIsDirty(true);
            else
                self.FieldValueIsDirty(false);
        }
        else
            self.FieldValueIsDirty(true);
    });

    self.AllowedValuesSdtId.subscribe(function () {
        var customFieldId = self.CustomField();
        var oldValue = $.grep(productCatalogVm.CustomTemplates, function (element, index) {
            return element.CustomField === customFieldId;
        });

        var newValue = self.AllowedValuesSdtId();
        //console.log(oldValue[0].FieldValueBoolean);
        //console.log(newValue);
        if (oldValue.length > 0) {
            if (oldValue[0].AllowedValuesSdtId !== newValue)
                self.FieldValueIsDirty(true);
            else
                self.FieldValueIsDirty(false);
        }
        else
            self.FieldValueIsDirty(true);
    });

    self.ValidateFieldValue = function () {

    }

}
