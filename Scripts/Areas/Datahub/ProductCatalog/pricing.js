﻿/* 
    objPrice:
        -   Cost                    -- the cost of an item
        -   MarginPercent           -- the gross margin percent of an item
        -   MarginAmount            -- the gross margin amount of an item
        -   Price                   -- the selling price of an item
        -   SalePrice               -- the sale price of an item
        -   SalePriceMarginPercent  -- the sale price margin percent of an item
        -   IsCostLocked            -- flag to get/set if margin is locked (false by default)
        -   IsMarginLocked          -- flag to get/set if margin is locked (false by default)

    Example:
        var objPrice = { Cost: 100, MarginPercent: 10, MarginAmount: 11.1111, Price: 111.1111, SalePrice: 0, SalePriceMarginPercent: 0, IsCostLocked: false, IsMarginLocked: false };

*/

var PricingBuilder = function () {

       var cost;                    //-- the cost of an item
       var marginPercent;           //-- the gross margin percent of an item
       var marginAmount;            //-- the gross margin amount of an item
       var price;                   //-- the selling price of an item
       var salePrice;               //-- the sale price of an item
       var salePriceMarginPercent;  //-- the sale price margin percent of an item
       var isCostLocked;            //-- flag to get/set if margin is locked (false by default)
       var isMarginLocked;          //-- flag to get/set if margin is locked (false by default)

    var pricingLevel = new Array();
    pricingLevel.push(new PricingBuilder());

    var options = {};

    function privateMethods() {

    }

    function notifyPriceChange() {
        console.log("Price has changed to ", price);

        var c = {
            'price': price,
            'marginpercent': marginpercent,
            'cost': cost
        }

        //check condition

    }

    return {
        changePrice: function (p, callback) {
            price = p;

            callback(price);

        },
        calculateCost: function (price, marginPercent) {
            if (price == 0.0 || marginPercent == 100.0)
                return 0.0;
            return (price * (1 - marginPercent / 100));
        },

        getInstance: function () {
            return new PricingBuilder();
        }
    }
}


var pb = new PricingBuilder();

// console.log(pb.calculateCost(10,10));
console.log(pb.changePrice(10, function (obj) {
    console.log("From notify ", obj);
}));
// console.log(pb.changePrice(20));
// console.log(pb.changePrice(30));