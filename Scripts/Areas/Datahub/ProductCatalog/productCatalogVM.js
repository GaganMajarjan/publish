﻿//product catalog viewmodel
var marginNegativeDueToCostChange = false;
var costChanged = false;
var marginLockedOnPE = false;
var bypassNegativeMarginDueToFifo = false;

var ProductCatalogViewModel = function (productCatalogVm) {
    var self = this;
    var optionModel = "";

    self.vendorProduct = ko.observable();

    self.StoreId = ko.observable((productCatalogVm != null && productCatalogVm.StoreId != null) ? productCatalogVm.StoreId : '');
    // Properties - General
    self.Id = ko.observable((productCatalogVm != null && productCatalogVm.Id != null) ? productCatalogVm.Id : '');
    self.Name = ko.observable((productCatalogVm != null && productCatalogVm.Name != null) ? productCatalogVm.Name : '');
    self.LocalSku = ko.observable((productCatalogVm != null && productCatalogVm.LocalSku != null) ? productCatalogVm.LocalSku.toUpperCase() : '');
    
    self.Location = ko.observable((productCatalogVm != null && productCatalogVm.Location != null) ? productCatalogVm.Location : '');
    self.ShortDescription = ko.observable((productCatalogVm != null && productCatalogVm.ShortDescription != null) ? productCatalogVm.ShortDescription : '');
    self.IsDisabled = ko.observable((productCatalogVm != null && productCatalogVm.IsDisabled != null) ? productCatalogVm.IsDisabled : false);

    // Properties - Pricing
    self.PrimaryVendorProductId = ko.observable((productCatalogVm != null && productCatalogVm.PrimaryVendorProductId != null) ? productCatalogVm.PrimaryVendorProductId : null);
    //self.Cost = ko.observable((productCatalogVm != null && productCatalogVm.SelectedCost != null) ? productCatalogVm.SelectedCost : 0)
        //.extend({ numeric: decimal_places });

    self.Cost = ko.observable((productCatalogVm != null && productCatalogVm.Cost != null) ? productCatalogVm.Cost : 0)
        .extend({ numeric: decimal_places });
    self.FifoCost = ko.observable((productCatalogVm != null && productCatalogVm.FifoCost != null) ? productCatalogVm.FifoCost : 0)
        .extend({ numeric: decimal_places });
    self.SelectedCost = ko.observable((productCatalogVm != null && productCatalogVm.SelectedCost != null) ? productCatalogVm.SelectedCost : productCatalogVm.Cost)
       .extend({ numeric: decimal_places });
    self.CostOption = ko.observable((productCatalogVm != null && productCatalogVm.CostOption != null) ? productCatalogVm.CostOption.toString() : "1");
    
    self.IsCostLocked = ko.observable((productCatalogVm != null && productCatalogVm.IsCostLocked != null) ? productCatalogVm.IsCostLocked : false);
    self.MarginPercent = ko.observable((productCatalogVm != null && productCatalogVm.MarginPercent != null) ? productCatalogVm.MarginPercent : '')
        .extend({ numeric: decimal_places });

    self.IsMarginLocked = ko.observable((productCatalogVm != null && productCatalogVm.IsMarginLocked != null) ? productCatalogVm.IsMarginLocked : false);
    marginLockedOnPE = self.IsMarginLocked();
    self.MarginAmount = ko.observable((productCatalogVm != null && productCatalogVm.MarginAmount != null) ? productCatalogVm.MarginAmount : '')
        .extend({ numeric: decimal_places });

    self.Price = ko.observable((productCatalogVm != null && productCatalogVm.Price != null) ? productCatalogVm.Price : '')
        .extend({ numeric: decimal_places });

    self.SellingPrice = ko.observable((productCatalogVm != null && productCatalogVm.SellingPrice != null) ? productCatalogVm.SellingPrice : '')
        .extend({ numeric: 2 });
   
    self.Note = ko.observable((productCatalogVm != null && productCatalogVm.Note != null) ? productCatalogVm.Note : '');

    self.IsSalePriceLocked = ko.observable((productCatalogVm != null && productCatalogVm.IsSalePriceLocked != null) ? productCatalogVm.IsSalePriceLocked : false);
    self.IsStoreEnabled = ko.observable(isStoreEnabled);
    self.PricingLevels = ko.observableArray([]);

    //self.CanSave = true;
    //pricing custom fields
    //self.case_qty = ko.observable((productCatalogVm != null && productCatalogVm.PricingCustomField != null && productCatalogVm.PricingCustomField.case_qty != null) ? productCatalogVm.PricingCustomField.case_qty : '');
    //self.exclusive_minimum_quantity = ko.observable((productCatalogVm != null && productCatalogVm.PricingCustomField != null && productCatalogVm.PricingCustomField.exclusive_minimum_quantity != null) ? productCatalogVm.PricingCustomField.exclusive_minimum_quantity : '');
    //self.reorderPoint = ko.observable((productCatalogVm != null && productCatalogVm.PricingCustomField != null && productCatalogVm.PricingCustomField.reorderPoint != null) ? productCatalogVm.PricingCustomField.reorderPoint : '');
    //self.reorderQuantity = ko.observable((productCatalogVm != null && productCatalogVm.PricingCustomField != null && productCatalogVm.PricingCustomField.reorderQuantity != null) ? productCatalogVm.PricingCustomField.reorderQuantity : '');
    //self.quantityonhand = ko.observable((productCatalogVm != null && productCatalogVm.PricingCustomField != null && productCatalogVm.PricingCustomField.quantityonhand != null) ? productCatalogVm.PricingCustomField.quantityonhand : '');

    //self.PricingCustomField = ko.observable(productCatalogVm != null ? new PricingCustomFieldVM(productCatalogVm.PricingCustomField) : new PricingCustomFieldVM(null));
    self.PricingCustomField = productCatalogVm != null && productCatalogVm.PricingCustomField != null ? new PricingCustomFieldVM(productCatalogVm.PricingCustomField) : new PricingCustomFieldVM(null);

    //custom fields
    self.CustomFieldsRequired = ko.observableArray([]);
    self.CustomFieldsAdditional = ko.observableArray([]);
    self.CustomFieldsReadOnly = ko.observableArray([]);
    self.CustomTemplates = ko.computed(function () {
        return self.CustomFieldsRequired().concat(self.CustomFieldsAdditional());
    });
    self.CustomTemplatesAll = ko.computed(function () {
        return self.CustomFieldsRequired().concat(self.CustomFieldsReadOnly()).concat(self.CustomFieldsAdditional());
    });
    self.CountReqAndReadonly = ko.computed(function () {
        return (parseInt(self.CustomFieldsRequired().length) + parseInt(self.CustomFieldsReadOnly().length));
    });
    self.CountReq = ko.computed(function () {
        return (parseInt(self.CustomFieldsRequired().length));
    });
    //self.Count = ko.computed(function () {
    //    return self.CustomFieldsRequired().length + self.CustomFieldsReadOnly().length + self.CustomFieldsAdditional().length;
    //});

    self.PlatformQualifier = ko.observable((productCatalogVm != null && productCatalogVm.PlatformQualifier != null) ? productCatalogVm.PlatformQualifier : '');
    self.PlatformQualifiers = ko.observableArray((productCatalogVm != null && productCatalogVm.PlatformQualifiers != null) ? productCatalogVm.PlatformQualifiers : []);

    // Properties - Identifiers
    self.ISBN = ko.observable((productCatalogVm != null && productCatalogVm.ISBN != null) ? productCatalogVm.ISBN : '');
    self.NSN = ko.observable((productCatalogVm != null && productCatalogVm.NSN != null) ? productCatalogVm.NSN : '');
    self.UPC = ko.observable((productCatalogVm != null && productCatalogVm.UPC != null) ? productCatalogVm.UPC : '');
    self.EAN = ko.observable((productCatalogVm != null && productCatalogVm.EAN != null) ? productCatalogVm.EAN : '');
    self.MPN = ko.observable((productCatalogVm != null && productCatalogVm.MPN != null) ? productCatalogVm.MPN : '');
    self.ASIN = ko.observable((productCatalogVm != null && productCatalogVm.ASIN != null) ? productCatalogVm.ASIN : '');
    self.UOM = ko.observable((productCatalogVm != null && productCatalogVm.UOM != null) ? productCatalogVm.UOM : '');
    self.IdentifierExists = ko.observable((productCatalogVm != null && productCatalogVm.IdentifierExists != null) ? productCatalogVm.IdentifierExists : '');

    //SKYG-718
   
    self.HazmatBoxFee = ko.observable((productCatalogVm != null && productCatalogVm.HazmatBoxFee != null) ? productCatalogVm.HazmatBoxFee : '').extend({ numeric: decimal_places });
    self.FulfillmentCenter = ko.observable((productCatalogVm != null && productCatalogVm.FulfillmentCenter != null) ? productCatalogVm.FulfillmentCenter : '');
   
    self.BatchLotTracking = ko.observable((productCatalogVm != null && productCatalogVm.BatchLotTracking != null) ? productCatalogVm.BatchLotTracking : '');

    //SKYG-718 add platformQualifiers.
    // Properties - Dimensions
    self.Weight = ko.observable((productCatalogVm != null && productCatalogVm.Weight != null) ? productCatalogVm.Weight : '');
    self.Height = ko.observable((productCatalogVm != null && productCatalogVm.Height != null) ? productCatalogVm.Height : '');
    self.Width = ko.observable((productCatalogVm != null && productCatalogVm.Width != null) ? productCatalogVm.Width : '');
    self.Length = ko.observable((productCatalogVm != null && productCatalogVm.Length != null) ? productCatalogVm.Length : '');
    self.ProductCategoryId = ko.observable((productCatalogVm != null && productCatalogVm.ProductCategoryId != null) ? productCatalogVm.ProductCategoryId : '');
    self.ProductCategoryIds = ko.observableArray((productCatalogVm != null && productCatalogVm.ProductCategoryIds != null) ? productCatalogVm.ProductCategoryIds : []);


    self.BrandId = ko.observable((productCatalogVm != null && productCatalogVm.BrandId != null) ? productCatalogVm.BrandId : '');
    //SKYG-723
    self.QOH = ko.observable((productCatalogVm !== null && productCatalogVm.QOH !== null) ? productCatalogVm.QOH : '');
    self.CaseQuantity = ko.observable((productCatalogVm !== null && productCatalogVm.PricingCustomField.case_qty !== null) ? productCatalogVm.PricingCustomField.case_qty : '');
    //self.SellingSku = ko.observable((productCatalogVm != null && productCatalogVm.LocalSku != null) ?
    //    (self.CaseQuantity() != '' && self.CaseQuantity() != "1" && self.CaseQuantity() != "0") ? productCatalogVm.LocalSku.toUpperCase() + '-X' + self.CaseQuantity() : productCatalogVm.LocalSku.toUpperCase() : '');
    self.SellingSku = ko.observable((productCatalogVm != null && productCatalogVm.SellingSku != null) ? productCatalogVm.SellingSku : '');
    self.supplierId = ko.observable();
    self.supplierPartNumber = ko.observable();
    self.supplierName = ko.observable();
    self.url = ko.observable('');
    self.urlTitle = ko.observable('');
    self.PricingLevelsArchive = ko.observableArray([]);
    self.PreviousMarginPct = ko.observable((productCatalogVm != null && productCatalogVm.MarginPercent != null) ? productCatalogVm.MarginPercent : '')
        .extend({ numeric: decimal_places });
    self.Module = ko.observable('');
/* changes 6242020*/
    // self.IdentifierExists = ko.observable((productCatalogVm != null && productCatalogVm.IdentifierExists != null) ? (productCatalogVm.IdentifierExists = productCatalogVm.IdentifierExists!==true ? 0:1): '');

    self.isSetPrimary = ko.observable(false);
    self.RecalcMarginPct = ko.observable('');
    self.IsPriceNegative = ko.observable(false);
    self.IsValidate = ko.observable(false);
    self.SkyGeekPN = ko.observable((productCatalogVm != null && productCatalogVm.Id != null) ? productCatalogVm.SkyGeekPN : '');
    self.GoogleItemIdOverride = ko.observable((productCatalogVm != null && productCatalogVm.GoogleItemIdOverride != null) ? productCatalogVm.GoogleItemIdOverride : '');
    self.urlClick = function () {
        var fullUrl = urlEditVendorProductCatalog + '&id=' + self.Id() + '&returnUrl=' + window.location.href;
        window.location = fullUrl;
    }
    self.supplierId.subscribe(function (val) {
        if (val == null) {
            self.supplierPartNumber('');
            self.url('');
            self.urlTitle('');
            $('#view-vendor-product-catalog').hide();
            return;
        }
        $('#view-vendor-product-catalog').show();

        self.supplierPartNumber(val.VendorPartNumber);

        self.url(urlEditVendorProductCatalog + '/' + val.vpcId + '?returnUrl=' + window.location.href);
        self.urlTitle('Vendor product catalog details for ' + val.nameCost);
        self.supplierName(val.name);

        rebuilttooltip();
    });

    self.LocalSku.subscribe(function (val) {
        if (val == null || val == "") {
            // self.SellingSku('');
            return;
        }
       
        //else
        //    self.SellingSku(CalculateSellingSku(val, self.PricingCustomField.case_qty()));
          
        
    });
    
    self.IsCostPriceEqual = ko.observable(false);
    self.IsByPassCheckCostPrice = ko.observable(false);
  
    self.IsMarginNegative = ko.observable(false);
  
    // Properties - Clearance (sale)
    self.hasSalePriceForClearance = ko.observable((productCatalogVm != null && productCatalogVm.HasSalePriceForClearance != null) ? productCatalogVm.HasSalePriceForClearance : false);

    self.SalePrice = ko.observable((productCatalogVm != null && productCatalogVm.SalePrice != null) ? productCatalogVm.SalePrice : '')
        .extend({ numeric: decimal_places });

    self.TotalSalePrice = ko.observable((productCatalogVm != null && productCatalogVm.TotalSalePrice != null) ? productCatalogVm.TotalSalePrice : '')
        .extend({ numeric: decimal_places });

    self.salePriceForClearanceMarginPercent = ko.observable((productCatalogVm != null && productCatalogVm.SalePriceMarginPercent != null) ? productCatalogVm.SalePriceMarginPercent : '').extend({ numeric: decimal_places });


    self.IsSellingPrice = ko.observable(false);
    self.IsCaseQty = ko.observable(false);
    self.IsPrice = ko.observable(false);

    self.CoreExchangeFee = ko.observable((productCatalogVm != null && productCatalogVm.CoreExchangeFee != null) ? productCatalogVm.CoreExchangeFee : 0)
        .extend({ numeric: 2 });

    self.CrateFee = ko.observable((productCatalogVm != null && productCatalogVm.CrateFee != null) ? productCatalogVm.CrateFee : 0)
        .extend({ numeric: 2 });
    
   
    //if (self.SellingPrice() === null || self.SellingPrice() === '' || self.SellingPrice() === 0) {
        CalculateSellingPrice();
    //}
    SetCaseQuantityDefault();

    self.TotalMarginAmount = ko.observable((productCatalogVm != null && productCatalogVm.MarginAmount != null) ? productCatalogVm.MarginAmount * self.CaseQuantity() : '')
        .extend({ numeric: decimal_places });
   // SetTotalMarginAmountDefault();
    self.IsClickedEnabled = ko.observable(false);
    function SetCaseQuantityDefault() {
        if (self.PricingCustomField.case_qty() === null || self.PricingCustomField.case_qty() === '' || self.PricingCustomField.case_qty() <= 0) {
            self.PricingCustomField.case_qty("1");
            self.CaseQuantity("1");
        }
    }

    //function SetTotalMarginDefault() {

    //    self.TotalMargin(self.CaseQuantity * self.MarginAmount);
    //}
   
    if (self.IsDisabled() === false) {
        $("#PlatformQualifier").prop("disabled", false);
    } else {
        $("#PlatformQualifier").prop("disabled", true);
    }
    self.IsDisabled.subscribe(function (val) {
      
        if (val === false) {
            self.IsClickedEnabled(true);
            $("#PlatformQualifier").prop("disabled", false);
        }
        else {

            $("#PlatformQualifier").val(null).trigger('change');
            self.IsClickedEnabled(false);
            alertmsg("red", "PlatformQualifier will also disabled upon disabling Product");
            $("#PlatformQualifier").prop("disabled", true);
            return false;
        }
       
        
    });
    self.hasSalePriceForClearance.subscribe(function (val) {
        if (val === false) {
            self.SalePrice('0');
            self.salePriceForClearanceMarginPercent('0');
            self.TotalSalePrice('0');
        }
        else {
            if (self.salePriceForClearanceMarginPercent() === 0 && self.SalePrice() === 0) {
                self.SalePrice(self.SelectedCost());

                //self.TotalSalePrice(calculateNewSellingPrice(self.SelectedCost(), self.PricingCustomField.case_qty()));
                //self.TotalSalePrice(self.SelectedCost());
            }
        }
        changeLevel2 = "";
    });
    
    // Create inventory supplier
    self.createInventorySupplierId = ko.observable();

    // Computed
    self.ShowPricing = ko.computed(function () {
        //return (productCatalogVm.VendorProduct != null && productCatalogVm.VendorProduct.Id != null && productCatalogVm.VendorProduct.Id > 0);

        //return productCatalogVm.Pricing != null;

        return ((productCatalogVm.VendorProduct != null && productCatalogVm.VendorProduct.Id != null && productCatalogVm.VendorProduct.Id > 0) || productCatalogVm.Pricing != null);

        //return (self.Name() && self.LocalSku()) ? true : false;
    });

    //self.IsForNew = ko.observable(false);

    // Functions/Events
    self.addNewVendorProduct = function () {
        self.vendorProduct(new VendorProductCatalogViewModel());
        //self.IsForNew(true);
        $('#addNewVendorProduct').attr('disabled', true);
        $('#pricing-and-purchasing').addClass('non-editable');
        //clear spq, uom, dropship
        $('#Spq').val('');
        $('#uomListDrop').val('');
        $('#CanDropship').prop('checked', false);
        //click general tab of vpc
        activateBootStrapTab('vpc-general');
        self.flashBackground('.vendor-product-tab li', '#bbd3ee', 1000);

    }

    self.flashBackground = function (li, flashColor, flashDuration) {
        $('.vendor-product-tab li').css("background-color", flashColor)
        setTimeout(function () {
            $('.vendor-product-tab li').attr("style", "background-color:none");
        }, flashDuration);
    }

    self.addPricingLevel = function () {

        for (var i = 0; i < self.PricingLevels().length; i++) {
            var quantityFrom = self.PricingLevels()[i].QuantityFrom();
            //var quantityTo = self.PricingLevels()[i].QuantityTo();
            if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                alertmsg('red', 'Quantity should be greater than 1');
                return;
            }

            var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
            var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

            if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                alertmsg('red', 'Margin %  should be greater than 0');
                return;
            }

            if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                alertmsg('red', 'Margin $  should be greater than 0');
                return;
            }

        }
        if (self.PricingLevels().length > 1) {
            var quantityFromCurrent = self.PricingLevels()[self.PricingLevels().length - 1].QuantityFrom();
            var quantityFromPrevious = self.PricingLevels()[self.PricingLevels().length - 2].QuantityFrom();

            if (quantityFromPrevious != null && quantityFromCurrent != null && parseFloat(quantityFromPrevious) >= parseFloat(quantityFromCurrent)) {
                alertmsg('red', 'Current "Quantity" should be greater than the previous "Quantity"');
                return;
            }
        }

        self.PricingLevels.push(new PIMPricingLevelViewModel());
        rebuilttooltip();
    };

    self.addCustomField = function () {

        self.CustomFieldsAdditional.push(new CustomFieldManagerViewModel());
        $(".customFieldSelect2").select2({ width: '100%' });

    };

    self.removeAdditionalCustomField = function (customTemplate) {
        self.CustomFieldsAdditional.remove(customTemplate);

    };

    self.removePricingLevel = function (pricingLevel) {

        self.PricingLevels.remove(pricingLevel);
    };

    self.hasCustomFieldTemplateValue = ko.observable(true);
    self.hasCustomFieldValue = ko.observable(false);
    self.validate = function (options) {

       // if (costChanged === true) {
        var alertNegativeMargin = self.validateNegativeMargin() || marginNegativeDueToCostChange;
      
        if ((alertNegativeMargin === true || (self.MarginPercent() < 10 && self.IsMarginLocked() !== true && self.Cost() !== 0)) && bypassNegativeMarginDueToFifo === false ) {
            self.Module = $('ul[role=tablist]>li').first().text();
            self.RecalcMarginPct = self.MarginPercent();
            self.IsValidate = true;
                $('#MarginNegativeDueToFifoCostModal').modal('show');
                return;
            }


            if (self.PricingLevels().length > 0) {
                for (var i = 0; i < self.PricingLevels().length; i++) {
                    var pricingLevel = self.PricingLevels()[i];
                    if (pricingLevel != null) {
                        if (pricingLevel.GrossMarginPercent() < 0) {
                            self.RecalcMarginPct = self.MarginPercent();
                            self.Module = $('ul[role=tablist]>li').first().text();
                            self.IsPriceNegative = true;
                            self.IsValidate = true;
                            $('#MarginLeveNegativeDueToFifoCostModal').modal('show');
                            return;
                        }
                        else {
                            self.IsPriceNegative = false;
                        }

                    }
                }
            }

       // }
        //options start (validation)
        var bypassCostThresholdCheck = false;
        var bypassCostRangeCheck = false;
        var bypassPriceRangeCheck = false;
        var bypassPriceAndCostSameCheck = false;
        var bypassQuantityDiscountRoundUpCheck = false;
        var bypassKitQuantityDiscountRoundUpCheck = false;
        //SG-808
        var bypassValidateProductAmountCheck = false;
        var bypassSaleMarginGreaterThanProductMargin = true;

        if (options) {

            //if (options.bypassSpqAndQplCheck)
            //    bypassSpqAndQplCheck = true;

            if (options.bypassCostThresholdCheck)
                bypassCostThresholdCheck = true;

            if (options.bypassCostRangeCheck)
                bypassCostRangeCheck = true;

            if (options.bypassPriceRangeCheck)
                bypassPriceRangeCheck = true;

            if (options.bypassPriceAndCostSameCheck)
                bypassPriceAndCostSameCheck = true;

            if (options.bypassQuantityDiscountRoundUpCheck)
                bypassQuantityDiscountRoundUpCheck = true;

            if (options.bypassKitQuantityDiscountRoundUpCheck)
                bypassKitQuantityDiscountRoundUpCheck = true;
            if (options.bypassValidateProductAmountCheck)
                bypassValidateProductAmountCheck = true;
            if (options.bypassSaleMarginGreaterThanProductMargin) {
                bypassSaleMarginGreaterThanProductMargin = true;
            }
        }

        //validate
        var isValid = validateCustomField();
        if (!isValid)
            return false;
        // SG-771 validation for product_url and YahooId
        isValid = validateYahooIdWithProductUrl();
        if (!isValid)
            return false;

        isValid = validateYahooId();
        if (!isValid)
            return false;

        //isValid = validateItemPoints();
        //if (!isValid)
        //    return false;

        isValid = validateCommodityCode();
        if (!isValid)
            return false;

        //if (bypassSpqAndQplCheck !== true) {
        //    isValid = validateSpqAndQpl();
        //    if (!isValid)
        //        return false;
        //}
        if (bypassCostThresholdCheck !== true) {
            //calculate cost threshold change
            var percentChange = self.getCostThreshold(self.SelectedCost());
            if (percentChange >= 150 || percentChange <= -150) {
                $('#CostThresholdModal').modal("show");
                return false;
            }
        }
        //if (bypassCostRangeCheck !== true) {
        //    isValid = validateCostRange();
        //    if (!isValid)
        //        return false;
        //}
        //if (bypassPriceRangeCheck !== true) {
        //    isValid = validatePriceRange();
        //    if (!isValid)
        //        return false;
        //}
        if (bypassCostRangeCheck !== true || bypassPriceRangeCheck !== true) {
            isValid = validateCostPriceRange();
            if (!isValid)
                return false;
        }
      
        if (bypassPriceAndCostSameCheck !== true) {
            isValid = validatePriceAndCostSame();
            if (!isValid)
                return false;
        }
        if (bypassQuantityDiscountRoundUpCheck !== true) {
            options.bypassQuantityDiscountRoundUpCheck = true;
            isValid = validateQuantityDiscountRoundUp(null);
            if (!isValid)
                return false;
        }

        //if (bypassSaleMarginGreaterThanProductMargin != true) {

        //    isValid = validateSaleMarginGreaterThanProductMargin();
        //    if (!isValid)
        //        return false;

        //}
        //SG-808
        //if (bypassValidateProductAmountCheck !== true) {
        //    var minLineOrderAmountforProductVendor = $('#VendorProduct_MinLineOrderAmount').val();
        //    if (minLineOrderAmountforProductVendor === undefined) {

        //        isValid = validateCostAndMinQtyWithMinLineOrder(productCatalogVm.VendorProduct.MinLineOrderAmount, self.vendorProduct().Cost());
        //    }
        //    else {
        //        isValid = validateCostAndMinQtyWithMinLineOrder(minLineOrderAmountforProductVendor, self.vendorProduct().Cost());
        //    }

        //    if (!isValid)
        //        return false;
        //}

        //options end (validation)
        return true;
    }

    self.save = function (options) {
        //SG-803 setting flag to display pop-up on orderby change
        OrderByValueChanged = false;
        //validate
        var isValid = self.validate(options);
        if (!isValid)
            return;
      
       
        var hazmatTypeId = $('[data-name = "hazmat_type"]');
        if (hazmatTypeId)
            $(hazmatTypeId).removeAttr('disabled')
        if (self.Id() !== "" && self.Id() !== "0" && self.Id() !== "-1" && self.Id() !== 0 && self.Id() !== -1) {
            self.edit(options);
        } else {
            self.create(options);
        }
    }

    self.create = function (options) {

        //validate
        self.setFieldReferenceValue(); //SKYG-718
        var isValid = self.validate(options);
        if (!isValid)
            return;

        if (showAllProductCatalogTabs !== null && showAllProductCatalogTabs === "True") {

            for (var i = 0; i < self.PricingLevels().length; i++) {

                var quantityFrom = self.PricingLevels()[i].QuantityFrom();
                //var quantityTo = self.PricingLevels()[i].QuantityTo();
                if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                    alertmsg('red', 'Please enter Quantity greater than one');
                    return;
                }

                var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
                var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

                if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                    alertmsg('red', 'Margin %  should be greater than 0');
                    return;
                }

                if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                    alertmsg('red', 'Margin $  should be greater than 0');
                    return;
                }
            }


        }

        var workflowId = getQueryStringByName("workflowId");
        var stepId = getQueryStringByName("stepId");
        var vendorProductId = getQueryStringByName("vendorProductId");
        var guid = getQueryStringByName("guid");
        var stagingId = getQueryStringByName("stagingId");
        var logUserUploadId = getQueryStringByName("logUserUploadId");
        var errorType = getQueryStringByName("errorType");

        

        var data = ko.toJSON({
            viewModel: self,
            workflowId: workflowId,
            stepId: stepId,
            vendorProductId: vendorProductId,
            logUserUploadId: logUserUploadId,
            stagingId: stagingId,
            errorType: errorType,
            guid: guid
        });

        //convert string to object
        data = JSON.parse(data);

        //add property to object
        data.viewModel.CustomTemplates = getCustomFieldJson(productCatalogVm.CustomTemplateGroup, '.cf-by-group .cf', true);
        var obj = null;
        if (data.viewModel.CustomTemplates.length > 0) {
            obj = data.viewModel.CustomTemplates.find(o => o.CustomField === $('[data-name="YahooId"]').attr("id"));
        }
       
        var pf = $("#PlatformQualifier option:selected").text();
        // if ((data.viewModel.IsDisabled) || (pf === "Yahoo" && viewModel.IsClickedEnabled() === true)) {

        if (obj === null && (data.viewModel.IsDisabled || (pf === "Yahoo" && viewModel.IsClickedEnabled() === true))) {
        var YahooId = $('[data-name="YahooId"]').val();
            var customFieldVm = new CustomFieldManagerViewModel();
            customFieldVm.CustomField = $('[data-name="YahooId"]').attr("id"); //this is the customFieldId

            customFieldVm.FieldValue = YahooId || '';
            customFieldVm.FieldValueIsDirty = true; //this has changed, so set IsDirty

            //add values to bypass modelstate validation
            var customFields = {
                name: 'not null',
                fieldDataTypeSdvId: '0',
                customFieldTemplateId: '0',
                fieldDescription: '',
                fieldAlias: 'YahooId',
                description: '',
                fieldReferenceSysInformationSchemaId: "",
                fieldReferenceSysInformationSchemaIdKit: ""
            };

            customFieldVm.CustomFields = customFields;
            data.viewModel.CustomTemplates.push(customFieldVm);

        }
        //check if yahooId validation has to be bypassed (occurs if user clicks buttons to override existing YahooId)
        if (options && options.triggerActionId) {
            data.viewModel.triggerActionId = options.triggerActionId;
        }

        //convert back to string
        data = JSON.stringify(data);
        if (!isNaN(self.salePriceForClearanceMarginPercent()) && self.salePriceForClearanceMarginPercent() >= self.MarginPercent() && self.salePriceForClearanceMarginPercent() != 0) {
            alertmsg('red', 'Sale Price Margin is greater than Price Margin !!');
            return;

        }

        self.createThis(data);

    };

    self.createThis = function (data) {
        // Ajax call
        $.ajax({
            type: 'POST',
            url: createUrl,
            cache: false,
            contentType: 'application/json; charset=utf-8',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {
                if (response != null && response.Success === true) {
                    alertmsg('green', response.Message);

                    var returnUrl = response.Data;
                    if (returnUrl) {
                        //window.location.href = returnUrl;
                        //window.location.reload();
                        window.location.href = returnUrl;
                    }

                    var suggestionUrl = getQueryStringByName("returnUrl");
                    if (suggestionUrl) {
                        var productSuggestionParams = "";
                        if (suggestionUrl.indexOf("ValidateProductSuggestion") > -1) {
                            var vendorId = getQueryStringByName("vendorId");
                            var skuType = getQueryStringByName("skuType");
                            var errorType = getQueryStringByName("errorType");
                            var logUserUploadId = getQueryStringByName("logUserUploadId");
                            var guid = getQueryStringByName("guid");
                            var stagingId = getQueryStringByName("stagingId");

                            productSuggestionParams = "?vendorId=" +
                                vendorId +
                                "&skuType=" +
                                skuType +
                                "&errorType=" +
                                errorType +
                                "&logUserUploadId=" +
                                logUserUploadId +
                                "&stagingId=" +
                                stagingId +
                                "&productId=" +
                                response.Data +
                                "&guid=" +
                                guid;
                        }
                        if (productSuggestionParams !== "")
                            window.location = suggestionUrl + productSuggestionParams;
                        else
                            window.location = suggestionUrl;
                    }
                } else if (response != null && response.Success === false) {
                    //console.log(response);
                    if (response.Data && response.Data === "warn") {
                        //let user see warning for 0.8 seconds, then proceed to create
                        alertmsg('yellow', response.Message);
                        setTimeout(function () {
                            self.editThis(data);
                        },
                            3000);
                    } else if (response.Data && response.Data.ConfirmUser) {
                        //this is a label, use text() to set its text message
                        $("#yahooIdConfirmationMessageLabel").text(response.Message);
                        //show the modal
                        $("#YahooIdConfirmationModal").modal("show");
                        SGloadingRemove();
                    } else {
                        alertmsg('red', response.Message);
                        SGloadingRemove();
                    }
                } else {
                    alertmsg('red', errorOccurredMessage);
                    SGloadingRemove();
                }
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });

    }

    self.edit = function (options) {
      //  debugger;
        var optionModel = options;
        self.setFieldReferenceValue(); //SKYG-718
        if (OrderByValueChanged == true)
            $("#" + confirmSavePopupId).modal("show");

        else {

            var isValid = self.validate(options);
            if (!isValid)
                return;


            if (showAllProductCatalogTabs !== null && showAllProductCatalogTabs === "True") {

                if (self.Cost() == null || self.Cost() <= 0) {
                    alertmsg('red', 'Cost should be greater than 0');
                    return;
                }

                for (var i = 0; i < self.PricingLevels().length; i++) {
                    var quantityFrom = self.PricingLevels()[i].QuantityFrom();
                    //var quantityTo = self.PricingLevels()[i].QuantityTo();
                    if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                        alertmsg('red', 'Please enter Quantity greater than one. Save Cancelled.');
                        return;
                    }

                    var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
                    var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

                    if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                        alertmsg('red', 'Margin %  should be greater than 0');
                        return;
                    }

                    if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                        alertmsg('red', 'Margin $  should be greater than 0');
                        return;
                    }
                }
            }

            if (!isNaN(self.salePriceForClearanceMarginPercent()) && self.salePriceForClearanceMarginPercent() >= self.MarginPercent() && self.salePriceForClearanceMarginPercent() != 0) {
                alertmsg('red', 'Sale Price Margin is greater than Price Margin !!')
                return;

            }

            var workflowId = getQueryStringByName("workflowId");
            var stepId = getQueryStringByName("stepId");
            var guid = getQueryStringByName("guid");
            var stagingId = getQueryStringByName("stagingId");
            var logUserUploadId = getQueryStringByName("logUserUploadId");
            var errorType = getQueryStringByName("errorType");

            


            var data = ko.toJSON({
                viewModel: self,
                workflowId: workflowId,
                stepId: stepId,
                logUserUploadId: logUserUploadId,
                stagingId: stagingId,
                errorType: errorType,
                guid: guid
            });

            //convert string to object
            data = JSON.parse(data);
           
            //add property to object
            data.viewModel.CustomTemplates = getCustomFieldJson(productCatalogVm.CustomTemplateGroup, '.cf-by-group .cf', true);
            var obj = null;
            if (data.viewModel.CustomTemplates.length > 0) {
                obj = data.viewModel.CustomTemplates.find(o => o.CustomField === $('[data-name="YahooId"]').attr("id"));
            }
            
            var pf = $("#PlatformQualifier option:selected").text();
            //if ((data.viewModel.IsDisabled && obj === null) || pf === "Yahoo") {
            if (obj === null && (data.viewModel.IsDisabled || (pf === "Yahoo" && viewModel.IsClickedEnabled() === true))) {
                var YahooId = $('[data-name="YahooId"]').val();
                var customFieldVm = new CustomFieldManagerViewModel();
                customFieldVm.CustomField = $('[data-name="YahooId"]').attr("id"); //this is the customFieldId
                
                customFieldVm.FieldValue = YahooId || '';
                customFieldVm.FieldValueIsDirty = true; //this has changed, so set IsDirty

                //add values to bypass modelstate validation
                var customFields = {
                    name: 'not null',
                    fieldDataTypeSdvId: '0',
                    customFieldTemplateId: '0',
                    fieldDescription: '',
                    fieldAlias: 'YahooId',
                    description: '',
                    fieldReferenceSysInformationSchemaId:  "" ,
                    fieldReferenceSysInformationSchemaIdKit:  ""
                };

                customFieldVm.CustomFields = customFields;
                data.viewModel.CustomTemplates.push(customFieldVm);
                
            }
            //check if yahooId validation has to be bypassed (occurs if user clicks buttons to override existing YahooId)
            if (options && options.triggerActionId) {
                data.viewModel.triggerActionId = options.triggerActionId;
            }

            if (options && options.bypassKitQuantityDiscountRoundUpCheck) {
                data.viewModel.bypassKitQuantityDiscountRoundUpCheck = options.bypassKitQuantityDiscountRoundUpCheck;
            }

            //convert back to string
            data = JSON.stringify(data);

            // Ajax call
            $.ajax({
                type: 'POST',
                url: urlValidateKitChildMarginThreshold,
                cache: false,
                contentType: 'application/json; charset=utf-8',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    //$("#btnsave").attr('disabled', true);
                    SGloading('Validating');
                },
                success: function (response) {
                    if (response != null && response.Success === true) {

                        self.editThis(data);
                    }
                    else if (response != null && response.Success === false) {
                        if (response.Data && response.Data === "warn") {
                            //let user see warning for 0.8 seconds, then proceed to create
                            alertmsg('yellow', response.Message);
                            setTimeout(function () {
                                self.editThis(data);
                            }, 3000);
                        }
                        else if (response.Data && response.Data === "popup") {
                            options.bypassKitQuantityDiscountRoundUpCheck = true;
                            $('#hiddenQuantityDiscountRoundUpValue').val(JSON.stringify(options));
                            $('#QuantityDiscountRoundUpModal').modal('show');
                            $("#quantityDiscountRoundUpIsKit").text("KIT: ");
                            SGloadingRemove();
                        }
                        else if (response.Data && response.Data.ConfirmUser) {
                            //this is a label, use text() to set its text message
                            $("#yahooIdConfirmationMessageLabel").text(response.Message);
                            //show the modal
                            $("#YahooIdConfirmationModal").modal("show");
                            SGloadingRemove();
                        }
                        else {
                            alertmsg('red', response.Message);
                            SGloadingRemove();
                        }
                    }
                    else {
                        alertmsg('red', errorOccuredMessage);
                        SGloadingRemove();
                    }
                },
                complete: function () {
                    //$("#btnsave").attr('disabled', false);
                    //SGloadingRemove();
                },
                error: function (xhr, status, error) {
                    //reset variables here
                    alert(status + ' ' + error);
                    SGloadingRemove();
                }

            }
           );
        }

    };



    self.getCostThreshold = function (cost) {

        var oldVal = productCatalogVm.Cost;
        var newVal = cost;
        var percentChange = ((newVal - oldVal) / oldVal) * 100;
        if (oldVal === 0 || oldVal === null)
            percentChange = 0;
        return percentChange;

    }

    self.editThis = function (data) {
        // Ajax call
        $.ajax({
            type: 'POST',
            url: editUrl,
            cache: false,
            contentType: 'application/json; charset=utf-8',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {
                if (response != null && response.Success === true) {
                    alertmsg('green', response.Message);

                    var returnUrl = response.Data;
                    if (returnUrl)
                        window.location.href = returnUrl;

                    var suggestionUrl = getQueryStringByName("returnUrl");
                    if (suggestionUrl)
                        window.location = suggestionUrl;

                    //preserve current active tab and reload
                    var activeHash = getActiveHash();
                    if (activeHash !== "") {
                        window.location = window.location.toString().split('#')[0] + activeHash;
                    }
                    window.location.reload();

                }
                else if (response != null && response.Success === false) {
                    if (response.Data && response.Data === "warn") {
                        //let user see warning for 0.8 seconds, then proceed to create
                        alertmsg('yellow', response.Message);
                        setTimeout(function () {
                            self.editThis(data);
                        }, 3000);
                    }
                    else if (response.Data && response.Data.ConfirmUser) {
                        //this is a label, use text() to set its text message
                        $("#yahooIdConfirmationMessageLabel").text(response.Message);
                        //show the modal
                        $("#YahooIdConfirmationModal").modal("show");
                        SGloadingRemove();
                    }
                    else {
                        alertmsg('red', response.Message);
                        SGloadingRemove();
                    }
                }
                else {
                    alertmsg('red', errorOccuredMessage);
                    SGloadingRemove();
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });
    }

    $('#btnYesCostThresholdModal').click(function () {
        $('#CostThresholdModal').modal("hide");
        self.save({
            bypassCostThresholdCheck: true
          
        });
    });
    $('#SellingPrice').attr('readonly', true);





    $('#btnSaveConfirmationDialogueYes').click(function () {
      
        var options = optionModel;
        var isValid = self.validate(options);
        if (!isValid)
            return;


        if (showAllProductCatalogTabs !== null && showAllProductCatalogTabs === "True") {

            if (self.Cost() == null || self.Cost() <= 0) {
                alertmsg('red', 'Cost should be greater than 0');
                return;
            }

            for (var i = 0; i < self.PricingLevels().length; i++) {
                var quantityFrom = self.PricingLevels()[i].QuantityFrom();
                //var quantityTo = self.PricingLevels()[i].QuantityTo();
                if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                    alertmsg('red', 'Please enter Quantity greater than one. Save Cancelled.');
                    return;
                }

                var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
                var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

                if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                    alertmsg('red', 'Margin %  should be greater than 0');
                    return;
                }

                if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                    alertmsg('red', 'Margin $  should be greater than 0');
                    return;
                }
            }
        }


        var workflowId = getQueryStringByName("workflowId");
        var stepId = getQueryStringByName("stepId");
        var guid = getQueryStringByName("guid");
        var stagingId = getQueryStringByName("stagingId");
        var logUserUploadId = getQueryStringByName("logUserUploadId");
        var errorType = getQueryStringByName("errorType");

        var data = ko.toJSON({
            viewModel: self,
            workflowId: workflowId,
            stepId: stepId,
            logUserUploadId: logUserUploadId,
            stagingId: stagingId,
            errorType: errorType,
            guid: guid
        });

        //convert string to object
        data = JSON.parse(data);

        //add property to object
        data.viewModel.CustomTemplates = getCustomFieldJson(productCatalogVm.CustomTemplateGroup, '.cf-by-group .cf', true);

        //check if yahooId validation has to be bypassed (occurs if user clicks buttons to override existing YahooId)
        if (options && options.triggerActionId) {
            data.viewModel.triggerActionId = options.triggerActionId;
        }

        if (options && options.bypassKitQuantityDiscountRoundUpCheck) {
            data.viewModel.bypassKitQuantityDiscountRoundUpCheck = options.bypassKitQuantityDiscountRoundUpCheck;
        }

        //convert back to string
        data = JSON.stringify(data);

        // Ajax call
        $.ajax({
            type: 'POST',
            url: urlValidateKitChildMarginThreshold,
            cache: false,
            contentType: 'application/json; charset=utf-8',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Validating');
            },
            success: function (response) {
                if (response != null && response.Success === true) {

                    self.editThis(data);
                }
                else if (response != null && response.Success === false) {
                    if (response.Data && response.Data === "warn") {
                        //let user see warning for 0.8 seconds, then proceed to create
                        alertmsg('yellow', response.Message);
                        setTimeout(function () {
                            self.editThis(data);
                        }, 3000);
                    }
                    else if (response.Data && response.Data === "popup") {
                        options.bypassKitQuantityDiscountRoundUpCheck = true;
                        $('#hiddenQuantityDiscountRoundUpValue').val(JSON.stringify(options));
                        $('#QuantityDiscountRoundUpModal').modal('show');
                        $("#quantityDiscountRoundUpIsKit").text("KIT: ");
                        SGloadingRemove();
                    }
                    else if (response.Data && response.Data.ConfirmUser) {
                        //this is a label, use text() to set its text message
                        $("#yahooIdConfirmationMessageLabel").text(response.Message);
                        //show the modal
                        $("#YahooIdConfirmationModal").modal("show");
                        SGloadingRemove();
                    }
                    else {
                        alertmsg('red', response.Message);
                        SGloadingRemove();
                    }
                }
                else {
                    alertmsg('red', errorOccuredMessage);
                    SGloadingRemove();
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                //SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
                SGloadingRemove();
            }

        }
       );
    }
    );

    

    $("#btnDisableOtherProductUdhConfirmationModal").click(function () {
        $("#YahooIdConfirmationModal").modal("hide");
        self.save({
            bypassCostThresholdCheck: true,
            bypassCostRangeCheck: true,
            bypassPriceRangeCheck: true,
            bypassPriceAndCostSameCheck: true,
            triggerActionId: 1,
            bypassValidateProductAmountCheck: true

        });
    });

    $("#btnKeepBothUdhConfirmationModal").click(function () {
        $("#YahooIdConfirmationModal").modal("hide");
        self.save({
            bypassCostThresholdCheck: true,
            bypassCostRangeCheck: true,
            bypassPriceRangeCheck: true,
            bypassPriceAndCostSameCheck: true,
            triggerActionId: 2,
            bypassValidateProductAmountCheck: true

        });
    });

    $("#btnYesMarginNegativeDueToFifoCostModal").click(function () {
       
        viewModel.MarginPercent(25);
        viewModel.IsMarginLocked(true);
       
        $("#table-pricing-level")[0].children[2].remove();
        $('#add-more-pricing-level').prop('disabled', true)
        $('#btnNoPriceRangeModal').click();
        bypassNegativeMarginDueToFifo = true;
        viewModel.PricingLevelsArchive = viewModel.PricingLevels();
        viewModel.PricingLevels([]);

        if (isSetPrimaryValid === true && sessionOption !== "") {
            sessionOption.isMarginLocked = true;
            sessionOption.IsMarginNegative = true;
            viewModel.IsValidate = true;
            setPrimary(sessionOption);
        }

        else {
            sessionOption.IsMarginNegative = false;
            self.save({
                bypassCostThresholdCheck: false,
                bypassCostRangeCheck: false,
                bypassPriceRangeCheck: false,
                bypassPriceAndCostSameCheck: false,
                bypassValidateProductAmountCheck: false

            });

        }
       
    })

    $('#btnYesMarginLevelNegativeCostModal').click(function () {

        $("#table-pricing-level")[0].children[2].remove();
        $('#add-more-pricing-level').prop('disabled', true);
        viewModel.PricingLevelsArchive = viewModel.PricingLevels();
        viewModel.PricingLevels([]);
        $('#btnNoMarginLevelNegativeCostModal').click();

        if (isSetPrimaryValid === true && sessionOption !== "") {
            sessionOption.isMarginLocked = true;
            viewModel.IsValidate = true;
            setPrimary(sessionOption);
        }
        else {

            self.save({
                bypassCostThresholdCheck: false,
                bypassCostRangeCheck: false,
                bypassPriceRangeCheck: false,
                bypassPriceAndCostSameCheck: false,
                bypassValidateProductAmountCheck: false

            });
        }


    })
    

    self.supplierInfo = function () {
        $('#supplier-modal').modal("show");
    };

    self.viewStagingData = function () {
        $('#staging-data-dialog').modal("show");
    };

    self.setPrimary = function () {
        //$('#supplier-modal').modal("show");
        if (self.supplierId() == null) {
            alertmsg('red', 'Please select primary supplier first');
            return;
        }

        //if cost is locked, cannot set primary
        if (self.IsCostLocked() == true) {
            alertmsg('red', 'Cannot set primary when cost is locked');
            return;
        }

        var vendorProductId = self.supplierId().vpcId;
        var productId = self.Id();

        // Ajax call
        $.ajax({
            type: 'POST',
            url: vendorProductSetPrimaryUrl,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { vendorProductId: vendorProductId, productId: productId },
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {
                if (response === "success") {

                    changeLevel = "";
                    self.isSetPrimary(true);
                    // Change cost to primary vendor's cost
                        self.Cost(self.supplierId().VPPRCost);
                        // Animation
                        $('#Cost').addClass('btn-warning');//.css('background-color', 'green');
                        setTimeout(function () {
                            $('#Cost').removeClass('btn-warning'); //.css('background-color', '');
                        }, 2000);

                        // Lock cost
                        self.IsCostLocked(true);

                        alertmsg('green', Alert_VendorProductSetPrimarySuccess);
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });

    };

    self.setPrimaryOnLoad = function () {

        if (self.supplierId() == null) {
            return;
        }

        var vendorProductId = self.supplierId().vpcId;
        var productId = self.Id();

        // Ajax call
        $.ajax({
            type: 'POST',
            url: vendorProductSetPrimaryUrl,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { vendorProductId: vendorProductId, productId: productId },
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Processing');
            },
            success: function (response) {

                if (response === "success") {
                    //changeLevel = 1;
                    self.isSetPrimary(true);
                        setTimeout(function () {
                            //show the primary vendor by default 
                            //(index=0 is --Choose, index=1 is the vendor that needs to be set primary)
                            $("#supplier").prop('selectedIndex', 1).change();
                        }, 500);

                        // Change cost to primary vendor's cost
                        if (self.Cost() == null || self.Cost() === '' || self.Cost() === 0)
                            self.Cost(supplierList[0].VPPRCost);

                        // Lock cost
                        self.IsCostLocked(true);
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                //alert(status + ' ' + error);
            }
        });

    };

    self.createInventorySupplier = function () {
        $('#create-inventory-supplier-modal').modal("show");
    };

    self.LockVendorProductCost = ko.observable(false);

    self.Cost.subscribe(function (val) {
        if (viewModel.isSetPrimary() === true && viewModel.IsByPassCheckCostPrice() === false) {

            var result = costpricevalidationFromProduct(viewModel);
            if (!result) {
                self.IsCostPriceEqual(true);
                return;
            }
            else {
                self.IsCostPriceEqual(false);
            }
        }
        if (val === 0) {
            alertmsg('yellow', 'Vendor Cost cannot be zero');
            return;
        }

        var minLineOrderAmountforProductVendor = $('#VendorProduct_MinLineOrderAmount').val();
        if (minLineOrderAmountforProductVendor === undefined) {

            minLineOrderAmountforProductVendor = productCatalogVm.VendorProduct.MinLineOrderAmount;
        }
        validateCostAndMinQtyWithMinLineOrder(minLineOrderAmountforProductVendor, val);
        //tie up cost of pricing and purchasing
        if (viewModel.Id() === "" || viewModel.Id() === -1) {
            if (self.LockVendorProductCost() == false)
                viewModel.vendorProduct().Cost(val);
        }
        if (viewModel.Id() !== "" && viewModel.Id() !== -1 && viewModel.vendorProduct().Id() !== "" && viewModel.vendorProduct().Id() !== -1) {
            if (self.LockVendorProductCost() == false)
                viewModel.vendorProduct().Cost(val);
        }
        if (viewModel.isSetPrimary() === false) {
            var todayDate = GetTodayDate();
            viewModel.vendorProduct().DateLastCostUpdate = todayDate;
            $('#DateLastCostUpdate').val(todayDate);
        }

        CalculateSelectedCost(self.CostOption());
        //self.CostOption(self.CostOption());
    });

    self.PricingCustomField.case_qty.subscribe(function (val) {
        val = val.trim();
        self.SellingSku(CalculateSellingSku(self.SkyGeekPN(), val));
        if (val === null || val === '') {
            val = 1;
            self.PricingCustomField.case_qty("1");
         
        }
        if (self.IsCaseQty() === false) {
            self.IsCaseQty(true);
            CalculateSellingPrice();
            CalculateTotalSalePrice();
          
            
        }
        self.CaseQuantity(val);
        CalculateTotalMargin(val, self.MarginAmount());
        self.IsCaseQty(false);
        
        
    });
    // Subscribe to changes
    self.SelectedCost.subscribe(function (val) {
        costChanged = true;
        marginNegativeDueToCostChange = false;
        if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("COST") > -1) {
            changeLevel = "";
            return;
        }
        val = parseFloat(val);

        if ($.isNumeric(val) === false) {
            return;
        }

        if (val === 0) {
            alertmsg('yellow', 'Cost cannot be zero');
            return;
        }


        var myCost = val;
        //var todayDate = GetTodayDate();
        //viewModel.vendorProduct().DateLastCostUpdate = todayDate;
        //$('#DateLastCostUpdate').val(todayDate);

        var myGrossMarginAmount = parseFloat(self.MarginAmount());
        var myGrossMarginPercent = parseFloat(self.MarginPercent());
        var mySellingPrice = parseFloat(self.Price());

        // Validation

        //check if cost has exceeded 150% threshold
        var percentChange = self.getCostThreshold(val);
        if (percentChange >= 150 || percentChange <= -150) {
            alertmsg('yellow', 'Selected Cost change threshold of +150% or -150% has been exceeded');
        }

        //self.CanSave(true);

        if (myGrossMarginAmount === 0 && myGrossMarginPercent === 0) {
            self.Price(val);
            return;
        }
       
        // Calculation
        if (myGrossMarginPercent !== 0 && self.IsMarginLocked()) {
            if (changeLevel.indexOf('SAP') === -1) {
                mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);
            }
        }
        else if (mySellingPrice !== 0)
        {
            if (changeLevel.indexOf('MAP') === -1) {
                myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
                //if (myGrossMarginPercent < 0) {
                //    marginNegativeDueToCostChange = true;
                //}
                //else { marginNegativeDueToCostChange = false;}
            }
        }

        changeLevel += "COST,";

        //if saleprice has value, update it
        if (self.hasSalePriceForClearance())
        {

            //if ((changeLevel2.match(/,/g) || []).length === 1 || changeLevel2.indexOf("SPS") > -1) {
            //    changeLevel2 = "";
            //    return;
            //}
            if (self.IsMarginNegative()) {
                var salePrice1 = self.SalePrice();
                if (salePrice1 !== 0) {
                    var newSalePriceMarginPercent1 = calculateGrossMarginPercent(val, salePrice1);
                    changeLevel2 += "SPS,";
                    self.salePriceForClearanceMarginPercent(newSalePriceMarginPercent1);
                }
            }
            if (self.IsMarginLocked())
            {
                var salePriceMarginPercent = self.salePriceForClearanceMarginPercent();
              //  if (salePriceMarginPercent !== 0) {
                    var newSalePrice = calculateSellingPrice(val, salePriceMarginPercent);
                    changeLevel2 += "SPS,";

                    self.SalePrice(newSalePrice);
               // }
            }
           
            else {
                var salePrice = self.SalePrice();
                if (salePrice !== 0) {
                    var newSalePriceMarginPercent = calculateGrossMarginPercent(val, salePrice);
                    changeLevel2 += "SPS,";

                    self.salePriceForClearanceMarginPercent(newSalePriceMarginPercent);
                }

            }
            
           
        }

        //if quantity discount is there, update their value
        if (self.PricingLevels().length > 0) {
            for (var i = 0; i < self.PricingLevels().length; i++) {
                var pricingLevel = self.PricingLevels()[i];
                if (pricingLevel !== null) {

                    if (self.IsMarginLocked()) {
                        var margin = pricingLevel.GrossMarginPercent();
                        pricingLevel.GrossMarginPercent(0);
                        pricingLevel.GrossMarginPercent(margin);
                    }
                    else {
                        var price = pricingLevel.PriceForPricingLevel();
                        pricingLevel.PriceForPricingLevel(0);
                        pricingLevel.PriceForPricingLevel(price);

                    }

                }
            }
        }

        // Set values
        if (!self.IsMarginLocked()) {
            self.MarginPercent((myGrossMarginPercent));
           
        }
        self.MarginAmount((mySellingPrice - myCost));
        if (self.IsMarginLocked())
            self.Price((mySellingPrice));
            changeLevel = "";

        

    });

    self.CostOption.subscribe(function (val) {
        CalculateSelectedCost(val);
    })

    function CalculateSelectedCost(val) {
        if (val === "3") {
            self.SelectedCost(self.Cost());
        }
        else if (val === "2") {
            self.SelectedCost(self.FifoCost() === 0 ? self.Cost() : self.FifoCost());
        }
        else if (val === "1") {
            self.SelectedCost(self.Cost() > self.FifoCost() ? self.Cost() : self.FifoCost());
        }

    }

    function CalculateSellingPrice() {
       // if (self.IsSellingPrice() === false) {
            var sp = calculateNewSellingPrice(self.Price(),self.PricingCustomField.case_qty());
            self.SellingPrice(sp);
        //}
    }
    function CalculatePrice() {
        if (self.IsPrice() === false) {
            var price = CalculatepriceOnSPChange(self.SellingPrice(), self.PricingCustomField.case_qty());
            self.Price(price);
        }
    }

    function CalculateTotalSalePrice() {
        var tsp = calculateNewSellingPrice(self.SalePrice(), self.PricingCustomField.case_qty());
        self.TotalSalePrice(tsp);
        
    }
   
    self.MarginPercent.subscribe(function (val) {

        ////////console.log('MarginPercent');
        ////////console.log(changeLevel);

        if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("MAP") > -1) {
            changeLevel = "";
            return;
        }

        val = parseFloat(val);
        if ($.isNumeric(val) === false) {
            return;
        }

        if (val === 0) {
            self.MarginAmount(0);
            self.Price(self.SelectedCost());
            return;
        }

        if (parseFloat(val) >= 100) {
            alertmsg('red', 'Margin percent cannot be greater than or equal to 100');
            self.MarginAmount(0);
            self.MarginPercent(0);
            self.Price(self.SelectedCost());
            return;
        }

        var myCost = parseFloat(self.SelectedCost());
        var myGrossMarginAmount = parseFloat(self.MarginAmount());
        var myGrossMarginPercent = parseFloat(val);
        var mySellingPrice = parseFloat(self.Price());

        // Validation
      
      

        if (myCost === 0 && mySellingPrice === 0) //amount check not required
            return;

        // Calculation
        if (myCost !== 0) {
            if (changeLevel.indexOf('SAP') === -1) {
                mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);
               
            }
        } else if (mySellingPrice !== 0) {
            if (changeLevel.indexOf('COST') === -1) {
                myCost = calculateCost(mySellingPrice, myGrossMarginPercent);
            }
        }

        if (changeLevel.indexOf('MAA') === -1) {
            myGrossMarginAmount = mySellingPrice - myCost;
        }

        changeLevel += "MAP,";
        if (!self.IsMarginLocked() || changeLevel.indexOf('SAP') > -1 || changeLevel.indexOf('MAP') > -1) {
            self.MarginAmount((myGrossMarginAmount));
        }
       
      
    });

    self.MarginAmount.subscribe(function (val) {
        ////////console.log('MarginAmount');
        ////////console.log(changeLevel);

        if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("MAA") > -1) {
            changeLevel = "";
            return;
        }

       

        val = parseFloat(val);
        if ($.isNumeric(val) === false) {
            return;
        }
        if (val === 0) {
            self.MarginPercent(0);
            self.Price(self.SelectedCost());
            return;
        }

        var myCost = parseFloat(self.SelectedCost());
        var myGrossMarginAmount = parseFloat(val);
        var myGrossMarginPercent = parseFloat(self.MarginPercent());
        var mySellingPrice = parseFloat(self.Price());

        // Validation
        if (myCost === 0 && mySellingPrice === 0) //percent check not required
            return;

        // Calculation
        if (myCost !== 0) {
            if (changeLevel.indexOf('SAP') === -1) {
                mySellingPrice = myCost + myGrossMarginAmount;
            }
        } else if (mySellingPrice !== 0) {
            if (changeLevel.indexOf('COST') === -1) {
                myCost = mySellingPrice - myGrossMarginAmount;
            }
        }
       

        changeLevel += "MAA,";

      
        if (!self.IsSalePriceLocked()) {
            self.Price((mySellingPrice));
        }
        CalculateTotalMargin(self.CaseQuantity(),val)

        //if (!self.IsCostLocked())
        //    self.Cost((myCost));

    });

    self.Price.subscribe(function (val) {
        ////////console.log('Price');
        ////////console.log(changeLevel);

        CalculateSellingPrice();
        if (viewModel.isSetPrimary() === true && viewModel.IsByPassCheckCostPrice() === false) {
            var result = costpricevalidationFromProduct(viewModel);
            if (!result) {
                self.IsCostPriceEqual(true);
                return;
            }
            else {
                self.IsCostPriceEqual(false);
            }
        }
        if (self.IsCostPriceEqual() === false) {
            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("SAP") > -1) {
                changeLevel = "";
                return;
            }

            val = parseFloat(val);

            if ($.isNumeric(val) === false) {
                return;
            }

            if (val === 0) {

                if (self.MarginPercent() == 0) {
                    alertmsg('yellow', 'Both selling price and margin cannot be zero (0)');
                    //self.CanSave(false);
                }

                return;
            }

            //added start
            //if SellingPrice = Cost, then set Margin% = 0 and Margin$ = 0 (warning to the user that SellingPrice = Cost)
            if (val == self.SelectedCost()) {
                self.MarginPercent(0);
                self.MarginAmount(0);

                //warning to the user that SellingPrice = Cost
                //alertmsg('yellow', 'Selling Price and Cost are same');
                //self.CanSave(false);
                //   validateCostPriceRange();
                return;
            }

            //added end
            //self.CanSave(true);

            var myCost = parseFloat(self.SelectedCost());
            var myGrossMarginAmount = parseFloat(self.MarginAmount());
            var myGrossMarginPercent = parseFloat(self.MarginPercent());
            var mySellingPrice = parseFloat(val);


            // Validation
            if (myCost === mySellingPrice) {
                return;
            }

            if (myCost === 0 && myGrossMarginAmount === 0 && myGrossMarginPercent === 0)
                return;

            // Calculation SKTG-370 commented
            //if (self.IsMarginLocked()) {
            //    if (changeLevel.indexOf('COST') === -1) {
            //        myCost = mySellingPrice - myGrossMarginAmount;
            //    }
            //}
            if (myCost !== 0) {
                if (changeLevel.indexOf('MAP') === -1 && (!self.IsMarginLocked || changeLevel === "")) {
                    myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
                }
            } else if (myGrossMarginPercent !== 0) {
                if (changeLevel.indexOf('COST') === -1) {
                    myCost = calculateCost(mySellingPrice, myGrossMarginPercent);
                }
            }

            changeLevel += "SAP,";
          
            // Set values
            // self.MarginAmount((self.Price() - self.Cost()));
            myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
            self.MarginPercent((myGrossMarginPercent));
            if (self.IsPrice() === false) {
                self.IsPrice(true);
                CalculateSellingPrice();
               
            }
            self.IsPrice(false);

            //commented for SKY-370
            //if (!self.IsMarginLocked()) {
            //    //self.MarginAmount((self.Price() - self.Cost()));
            //    self.MarginPercent((myGrossMarginPercent));

            //}
            //self.MarginAmount();   <-- commented
        }
       
    });

    //self.SellingPrice.subscribe(function (val) {
       
    //    if (self.IsSellingPrice() === false) {
    //        self.IsSellingPrice(true);
    //        CalculatePrice();
    //    }
       
    //    self.IsSellingPrice(false);
    //});

    // Check for lock (only 2 lock at most can be locked)
    self.IsCostLocked.subscribe(function (val) {
        if (self.Cost() === 0) {

            $('#LockCost').change(function () {
                alertmsg('red', 'Cannot lock when cost is zero');
            });
            self.IsCostLocked(false);
            $(".vendor-product-tab #Cost").attr("disabled", false);
            $(".vendor-product-tab #CostPerLot").attr("disabled", false);
            $(".vendor-product-tab #QuantityPerLot").attr("disabled", false);
            $(".vendor-product-tab #OrderBy").attr("disabled", false);
        }
        if (self.IsMarginLocked() === true && val === true) {
            alertmsg('red', 'Cannot lock cost when margin is locked');
            self.IsCostLocked(false);
            $(".vendor-product-tab #Cost").attr("disabled", false);
            $(".vendor-product-tab #CostPerLot").attr("disabled", false);
            $(".vendor-product-tab #QuantityPerLot").attr("disabled", false);
            $(".vendor-product-tab #OrderBy").attr("disabled", false);
        }
        else {
            // self.IsMarginLocked(!val);
            $(".vendor-product-tab #Cost").attr("disabled", val);
            $(".vendor-product-tab #CostPerLot").attr("disabled", val);
            $(".vendor-product-tab #QuantityPerLot").attr("disabled", val);
            $(".vendor-product-tab #OrderBy").attr("disabled", val);
        }
    });
    self.IsMarginLocked.subscribe(function (val) {
        if (self.MarginAmount() == 0 || self.MarginPercent() == 0) {
            alertmsg('red', 'Cannot lock when margin is zero');
            self.IsMarginLocked(false);
        }
        self.IsSalePriceLocked(val);
        if (val === true && self.IsCostLocked() === true) {
            alertmsg('red', 'Cannot lock margin when cost is locked');
            self.IsMarginLocked(false);
        }
        //self.IsCostLocked(!val);
    });

    /*
    self.IsCostLocked.subscribe(function (val) {
        if (self.Cost() === 0) {

            $('#LockCost').change(function () {
                alertmsg('red', 'Cannot lock when cost is zero');
            });
            self.IsCostLocked(false);
        }
        self.IsMarginLocked(!val);
        // self.IsMarginLocked(!val);
    });
    self.IsMarginLocked.subscribe(function (val) {
        if (self.MarginAmount() == 0 || self.MarginPercent() == 0) {
            alertmsg('red', 'Cannot lock when margin is zero');
            self.IsMarginLocked(false);
        }
        self.IsSalePriceLocked(val);
        self.IsCostLocked(!val);
        //self.IsCostLocked(!val);
    });
    */

    // Clearance computation
    self.SalePrice.subscribe(function (val)
    {
        CalculateTotalSalePrice();
        if ((changeLevel2.match(/,/g) || []).length === 1 || changeLevel2.indexOf("SPS") > -1) {
            changeLevel2 = "";
            return;
        }

        if ($.isNumeric(val) === false) {
            return;
        }

        //if (val === 0) {
        //    self.salePriceForClearanceMarginPercent(0);
        //    return;
        //}

        var grossMarginpercent = calculateGrossMarginPercent(self.SelectedCost(), val);
        if (grossMarginpercent !== 0) {
            changeLevel2 += "SPS,";
        }
       

        self.salePriceForClearanceMarginPercent(grossMarginpercent);
        //self.TotalSalePrice(CalculateTotalSalePrice());
        //self.TotalSalePrice(calculateNewSellingPrice(self.SalePrice(), self.PricingCustomField.case_qty()));
       
    });
    
    self.salePriceForClearanceMarginPercent.subscribe(function (val) {

        if ((changeLevel2.match(/,/g) || []).length === 1 || changeLevel2.indexOf("SPMP") > -1) {
            changeLevel2 = "";
            return;
        }
      
        if ($.isNumeric(val) === false) {
            return;
        }
        if (!self.hasSalePriceForClearance()) {
            return;
        }
        if (val === 0) {
            self.SalePrice(self.SelectedCost());
            return;
        }

        if (parseFloat(val) >= 100) {
            alertmsg('red', 'Sale Price margin percent cannot be greater than or equal to 100');
            self.SalePrice(self.SelectedCost());
            return;
        }

        var sellingPrice = calculateSellingPrice(self.SelectedCost(), val);
        if (sellingPrice !== 0) {
            changeLevel2 += "SPMP,";
        }

        self.SalePrice(sellingPrice);

    });
  


    //console.log(vendorProductCatalogVmJson);
    var vpc = new VendorProductCatalogViewModel(vendorProductCatalogVmJson);
    
    //assign vendor product catalog as a child of product catalog
    self.vendorProduct(vpc);
    self.validateNegativeMargin = function () {
        if (self.MarginPercent() < 0) {
            return true;
        }
       

        return false;

    }

    self.test = function () {
        marginLockedOnPE = self.IsMarginLocked();
        disableMarginLevel = self.IsMarginLocked();
        productCatalogVm.IsMarginLocked = self.IsMarginLocked();
        viewModel.vendorProduct().vpcMarginLocked(self.IsMarginLocked());
    }
     //SKYG-718
    self.setFieldReferenceValue = function () {
        viewModel.IdentifierExists = $('[data-name = "IdentifierExists"]').val() !== "1" ? false : true;
        viewModel.HazmatBoxFee = $('[data-name="HazmatBoxFee"]').val();
        viewModel.ASIN = $('[data-name="ASIN"]').val();
        viewModel.EAN = $('[data-name="EAN"]').val();
        viewModel.NSN = $('[data-name="NSN"]').val();
        viewModel.UPC = $('[data-name="UPC"]').val();
        //viewModel.UOM = $('[data-name="UOM"]').val();
        viewModel.ISBN = $('[data-name="ISBN"]').val();
        viewModel.MPN = $('[data-name="MPN"]').val();
        
        
    }

    function CalculateSellingSku(Sku, caseqty) {

        if ((caseqty !== '' || caseqty !== null || caseqty !== undefined) && caseqty > 1) {
            // return up((caseQuantity * price),2);
            return Sku + '-X' + caseqty;
        }

        else
            return Sku;
    }

    function CalculateTotalMargin(caseqty, marginAmount) {
        if (marginAmount >= 0 && caseqty > 0) {
            self.TotalMarginAmount(caseqty * marginAmount);
        }
    }
    
}

$('#MinimumOrderQuantity').focusout(function () {
    var minLineOrderAmountforProductVendor = $('#VendorProduct_MinLineOrderAmount').val();
    if (minLineOrderAmountforProductVendor === undefined) {

        minLineOrderAmountforProductVendor = productCatalogVm.VendorProduct.MinLineOrderAmount;
    }
    validateCostAndMinQtyWithMinLineOrder(minLineOrderAmountforProductVendor, viewModel.vendorProduct().Cost());
    
});







