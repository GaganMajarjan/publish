﻿
//quantity discount round up - validation

function validateQuantityDiscountRoundUp(options) {

    options = options || {};

    //do necessary checks
    if (viewModel && viewModel.PricingLevels()
        && viewModel.PricingLevels().length > 0
        && viewModel.PlatformQualifiers()
        && viewModel.PlatformQualifiers().indexOf("Yahoo") > -1) {

        var distinctPrice = [];
        var pricingLevels = viewModel.PricingLevels();

        //get the base selling price and round up
        var price = viewModel.Price();
        price = udh.calculation.RoundUp(price);
        distinctPrice.push(price);

        //get pricing levels selling price and round up
        for (var i = 0; i < pricingLevels.length; i++) {
            var thisPrice = pricingLevels[i].Price();
            thisPrice = udh.calculation.RoundUp(thisPrice);
            distinctPrice.push(thisPrice);
        }

        //get distinct values of price
        distinctPrice = distinctPrice.filter(udh.filters.distinctArrayValues);
        var allPricesLength = 1 + pricingLevels.length; //1 for base selling price

        var isValid = distinctPrice.length == allPricesLength;
        if (!isValid) {
            $('#QuantityDiscountRoundUpModal').modal('show');
            $('#quantityDiscountRoundUpIsKit').text('');
            //var s = {
            //    vendorProductId: options.vendorProductId || null,
            //    productId: options.productId || null,
            //    isMarginLocked: options.isMarginLocked || false
            //};

            var opt = JSON.stringify(options);

            $('#hiddenQuantityDiscountRoundUpValue').val(opt);

            return false;
        }
    }

    //If we reach here, it's valid, return true
    return true;

    /*
    console.log("if we reach here, check for kit");
    //If we reach here, it's valid for product. Check for kit and return true
    if ($("#kit-sku").length > 0) {
       
        var productId = viewModel.Id();
        var cost = viewModel.Cost();
        var d = { productId: productId, cost: cost };
        d = JSON.stringify(d);
        // Ajax call
        $.ajax({
            type: 'POST',
            url: validateKitPricingRoundUpUrl,
            cache: false,
            contentType: 'application/json; charset=utf-8',
            data: d,
            dataType: 'json',
            beforeSend: function() {
                SGloading('Validating');
            },
            success: function(response) {
                if (response != null && response.Success === true) {
                    return true;
                } else if (response != null && response.Success === false) {
                    if (response.Data && response.Data === "popup") {
                        var locallyStoredCallback = { store: callback };
                        locallyStoredCallback = JSON.stringify(locallyStoredCallback);
                        localStorage.setItem("storeLocally", locallyStoredCallback);
                        //callback();
                    } else {
                        udh.notify.error(response.Message);
                        SGloadingRemove();
                    }
                } else {
                    udh.notify.error(errorOccurredMessage);
                    SGloadingRemove();
                }
            },
            error: function(xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
                SGloadingRemove();
            }
        });
    }

    */
}

//quantity discount round up - button click (yes)

$('#btnYesQuantityDiscountRoundUpModal')
    .click(function () {
        //requirement: the form/javascript that includes this .js file should have viewModel as its parent MVVM
        if (typeof viewModel !== 'undefined' && viewModel !== null) {
            $('#QuantityDiscountRoundUpModal').modal('hide');

            //extra values are passed for set primary
            var options = $('#hiddenQuantityDiscountRoundUpValue').val();
            options = JSON.parse(options);

            //console.log(options);

            //call set primary when productId and vendorProductId is supplied
            if (options.vendorProductId && options.productId) {

                //options.bypassCostThresholdCheck = true;
                //options.bypassSpqAndQplCheck = true;
                //options.bypassCostRangeCheck = true;
                //options.bypassPriceRangeCheck = true;
                //options.bypassPriceAndCostSameCheck = true;
                options.bypassQuantityDiscountRoundUpCheck = true;

                setPrimary(options);

            } else { //call save
                //options = null;

                options = {
                    bypassCostThresholdCheck: true,
                    bypassCostRangeCheck: true,
                    bypassPriceRangeCheck: true,
                    bypassPriceAndCostSameCheck: true,
                    bypassQuantityDiscountRoundUpCheck: true,
                    bypassKitQuantityDiscountRoundUpCheck: options.bypassKitQuantityDiscountRoundUpCheck || false
                };
                viewModel
                    .bypassKitQuantityDiscountRoundUpCheck = options.bypassKitQuantityDiscountRoundUpCheck || false;
                viewModel.save(options);

            }
        } else {
            console.log('viewModel not defined');
        }

    });

$('.btnNoQuantityDiscountRoundUpModal')
    .click(function () {
        //window.location.reload();
        var options = $('#hiddenQuantityDiscountRoundUpValue').val();
        options = JSON.parse(options);

        if (options.vendorProductId && options.productId) {
            viewModel.Cost(FormatAmount(options.oldCost));
        }

        $('#hiddenQuantityDiscountRoundUpValue').val(null);
        $('#QuantityDiscountRoundUpModal').modal('hide');
        if (typeof viewModel !== 'undefined' && viewModel !== null) {
            viewModel.bypassQuantityDiscountRoundUpCheck = false;
            viewModel.bypassKitQuantityDiscountRoundUpCheck = false;
        }
        //remove bootstrap modal overlay effect
        $(".modal-backdrop").removeClass("modal-backdrop");
        return false;
    });
