﻿$(document).ready(function () {
    catalogManagerSearch = true;
})

function initializeProductCatalogDataTable() {
    $('#ProductCatalogDataTable').dataTableWithFilter({
        "destroy": true,
        "bProcessing": true,
        "bJQueryUI": true,
        "bServerSide": true,
        "bFilter": false,
        "scrollX": true,
        "bSort": false,
        "bScrollCollapse": true,

        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": tableToolsPath,
            "aButtons": tableToolsOptions
        },
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlProductCatalogManager,
        "sServerMethod": "POST",

        // Initialize our custom filtering buttons and the container that the inputs live in
        filterOptions: { searchButton: "ProductCatalogSearchText", clearSearchButton: "ProductCatalogClearSearch", searchContainer: "ProductCatalogSearchContainer" },
        "drawCallback": function (settings) {
            //tooltip
            rebuilttooltip();
        }
    });
}

initializeProductCatalogDataTable();
setTimeout(customJqueryUIpara('.sg-datatable-wrap'), 500);

$('.filter').click(function () {
    $(this).hide();
    $(this).siblings('.filterhide').show();
    $(this).parents('.panel').find('.panel-filter').show();
});

$('.filterhide').click(function () {
    $(this).hide();
    $(this).siblings('.filter').show();
    $(this).parents('.panel').find('.panel-filter').hide();

});

$('#btnAssociate').click(function () {
    var i = 0;
    var productId = 0;
    $('input[type=checkbox][class=ProductCatalogSelector]').each(function () {
        if (this.checked) {
            i++;
            productId = this.value;
        }
    });

    if (i === 0) { alertmsg('red', 'Please select a product to associate'); e.preventDefault(); }
    else if (i > 1) {
        alertmsg('red', 'Please select single product to associate');
        e.preventDefault();
    } else {
      
        //hide the current row
        $('input[type="checkbox"][value="' + productId + '"]').parent().parent().hide();

        $.ajax({
            type: 'POST',
            url: urlAssociateWithProduct,
            data: {
                vendorProductCatalogId: vendorProductCatalogId,
                productCatalogId: productId

            },
          
            beforeSend: function () {
              SGloading('Loading');
            },
            success: function (response) {
                if (response != null && response.Success === true) {
                    alertmsg('green', response.Message);
                    window.location.href = response.Data;
                } else if (response != null && response.Success === false) {
                    alertmsg('red', response.Message);
                } else {
                    alertmsg('red', msg_errorOccured);
                }
            },
            complete: function () {
               SGloadingRemove();
            },
            error: function (xhr, status, error) {
              alert(status + ' ' + error);
            }
        });
    }

});

$('#selectAllProductCatalog').click(function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.ProductCatalogSelector').prop("checked", true);
    } else {
        $('.ProductCatalogSelector').prop("checked", false);
    }
});