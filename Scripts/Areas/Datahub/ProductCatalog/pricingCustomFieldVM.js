﻿var PricingCustomFieldVM = function(pricingCustomFieldVm) {
    var self = this;

    self.AssotiationId = ko.observable(pricingCustomFieldVm != null && pricingCustomFieldVm.AssotiationId != null ? pricingCustomFieldVm.AssotiationId : 0);
    self.IsKit = ko.observable(pricingCustomFieldVm != null && pricingCustomFieldVm.IsKit != null ? pricingCustomFieldVm.IsKit : false);
    self.case_qty = ko.observable(pricingCustomFieldVm != null && pricingCustomFieldVm.case_qty != null ? pricingCustomFieldVm.case_qty : '');
    self.exclusive_minimum_quantity = ko.observable(pricingCustomFieldVm != null && pricingCustomFieldVm.exclusive_minimum_quantity != null ? pricingCustomFieldVm.exclusive_minimum_quantity : '');
    self.reorderPoint = ko.observable(pricingCustomFieldVm != null && pricingCustomFieldVm.reorderPoint != null ? pricingCustomFieldVm.reorderPoint : '');
    self.reorderPointUpdatedOn = ko.observable(pricingCustomFieldVm != null && pricingCustomFieldVm.reorderPointUpdatedOn != null ? pricingCustomFieldVm.reorderPointUpdatedOn : '');
    self.reorderQuantity = ko.observable(pricingCustomFieldVm != null && pricingCustomFieldVm.reorderQuantity != null ? pricingCustomFieldVm.reorderQuantity : '');
    self.reorderQuantityUpdatedOn = ko.observable(pricingCustomFieldVm != null && pricingCustomFieldVm.reorderQuantityUpdatedOn != null ? pricingCustomFieldVm.reorderQuantityUpdatedOn : '');
   // SKYG - 723

    //self.quantityonhand = ko.observable(pricingCustomFieldVm != null && pricingCustomFieldVm.quantityonhand != null ? pricingCustomFieldVm.quantityonhand : '');
}