﻿$(document).ready(function () {
    $('.filter').click(function () {
        $(this).hide();
        $(this).siblings('.filterhide').show();
        $(this).parents('.panel').find('.panel-filter').show();
    });

    $('.filterhide').click(function () {
        $(this).hide();
        $(this).siblings('.filter').show();
        $(this).parents('.panel').find('.panel-filter').hide();

    });

 
    function initializeAllCustomFieldsDataTable() {
        $('#CustomFieldDataTable').dataTableWithFilter({
            "destroy": true,
            "bJQueryUI": true,
            "bServerSide": true,
            "bFilter": false,
            "scrollX": true,
            "bSort": false,
            "bScrollCollapse": true,

            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": tableToolsPath,
                "aButtons": tableToolsOptions
            },
            "aLengthMenu": dataTableMenuLength,
            "iDisplayLength": dataTableDisplayLength,
            "sAjaxSource": urlAllCustomFields,
            "sServerMethod": "POST",

            // Initialize our custom filtering buttons and the container that the inputs live in
            filterOptions: { searchButton: "PlatformSearch", clearSearchButton: "ClearSearch", searchContainer: "PlatformSearchContainer" },
            "fnPreDrawCallback": function () {
                // gather info to compose a message
                var ob = this;
                SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                //SGtableloading('fa-plane', '', this);


            },
            "fnDrawCallback": function (settings) {
                // in case your overlay needs to be put away automatically you can put it here
                var divid = this.closest('.dataTables_wrapper').attr('id');
                $("#CustomFieldEditorPartialHolder").empty();
                SGtableloadingRemove(divid);
                rebuilttooltip();
            }
        });
    }

    initializeAllCustomFieldsDataTable();
    console.log(exportPlatformsCollection);
    exportPlatformsCollection = JSON.parse(exportPlatformsCollection);
    //
    $('body').on('click', '#CustomFieldDataTable tr', function (e) {
        var i = 0;
        var customfieldId = 0;
        $('input[type=checkbox][class=ProductCatalogSelector]').each(function () {
            if (this.checked) {
                i++;
                customfieldId = this.value;
            }
        });

        $.ajax({
            url: urlGetCustomFieldDetail,
            type: 'POST',
            data: {
                id: customfieldId
            },

            beforeSend: function () {
                SGloading('Loading');
            },
            success: function (response) {

                $("#CustomFieldEditorPartialHolder").empty();
                $('#CustomFieldEditorPartialHolder').html(response);

            },
            complete: function () {
                SGloadingRemove();
                //$('#files').val("");



                TblpgntBtm();

            },
            error: function (xhr, status, error) {
                alert(status + ' ' + error);
            }
        });






    });

    
        
        $(document).on('click', "#addMoreRow", function () {
        var labelId = $('#qualifiedExportPlatforms tr:last td input').attr('value');
        var numberedId = "0";
        if (labelId != null)
            numberedId = parseInt(labelId) + 1;

        numberedId = parseInt(numberedId);

        $('#qualifiedExportPlatforms tbody').append('<tr id="tr_' + numberedId + '">' +

            '<td style="display:none;"><input type="hidden" id="' + numberedId + '" class="sourceLabel" name="label[]" value="' + numberedId + '" /></td>' +
            '<td><select id="ExportPlatforms_' + numberedId + '__ExportPlatformSdvId" name="ExportPlatforms[' + numberedId + '].ExportPlatformSdvId" class="sourceDrop" style="width:100%;" name="Value"></select></td> ' +
            '<td><div class="input-group"><input id="ExportPlatforms_' + numberedId + '__PlatformFieldAlias" name="ExportPlatforms[' + numberedId + '].PlatformFieldAlias" type="text" class="sg-tooltip form-control" data-toggle="tooltip" data-placement="left" title="Platform Field Alias"/>' +
            '<div class="input-group-btn"><a id="remove_' + numberedId + '" class="btn btn-danger removeBtn"  data-toggle="tooltip" data-placement="top" title="remove the row"><i class="fa fa-close"></i></a></div>' +
            '</div></td>' +

            '</tr>');


        $.each(exportPlatformsCollection, function (index, value) {
            $("#ExportPlatforms_" + numberedId + "__ExportPlatformSdvId").append(new Option(value.Text, value.Value));
        });

        $("#ExportPlatforms_" + numberedId + "__ExportPlatformSdvId").select2();

    });

    $(document).on('click', ".removeBtn", function () {

        var dataTypeId = $(this).attr('id');
        var numberedId = dataTypeId.substr(dataTypeId.indexOf('_') + 1, dataTypeId.length);

        $("#tr_" + numberedId).remove();


        var labelId = $('#qualifiedExportPlatforms tr:last td input').attr('value');
        labelId = parseInt(labelId);


        for (var i = 0; i <= labelId; i++) {

            var newNumber = parseInt(numberedId) + parseInt(i) + 1;

            var idFormat = "{0}{1}{2}".f("ExportPlatforms_", newNumber.toString(), "__ExportPlatformSdvId");


            if ($("#" + idFormat).length) {

                console.log('test');

                var idFormatForNew = "{0}_{1}__{2}".format("ExportPlatforms", (newNumber - 1).toString(), "ExportPlatformSdvId");
                var nameFormatForNew = "{0}[{1}].{2}".format("ExportPlatforms", (newNumber - 1).toString(), "ExportPlatformSdvId");
                $("#" + idFormat).attr("name", nameFormatForNew);
                $("#" + idFormat).attr("id", idFormatForNew);


                idFormat = "{0}_{1}__{2}".format("ExportPlatforms", newNumber.toString(), "PlatformFieldAlias");
                idFormatForNew = "{0}_{1}__{2}".format("ExportPlatforms", (newNumber - 1).toString(), "PlatformFieldAlias");
                nameFormatForNew = "{0}[{1}].{2}".format("ExportPlatforms", (newNumber - 1).toString(), "PlatformFieldAlias");
                $("#" + idFormat).attr("name", nameFormatForNew);
                $("#" + idFormat).attr("id", idFormatForNew);


                $("#" + newNumber.toString()).attr("value", (newNumber - 1));
                $("#" + newNumber.toString()).attr("id", (newNumber - 1).toString());

                $("#remove_" + newNumber.toString()).attr("id", "remove_" + (newNumber - 1).toString());
                $("#tr_" + newNumber.toString()).attr("id", "tr_" + (newNumber - 1).toString());



            }
        }

        $.each(exportPlatformsCollection, function (index, value) {
            $("#ExportPlatforms_" + numberedId + "__ExportPlatformSdvId").append(new Option(value.Text, value.Value));
        });
    });


  

    //$("#ExportPlatforms_" + numberedId + "__ExportPlatformSdvId").select2();






});