﻿var deleteButtonClicked = "";

if (customFieldMessage != null && customFieldMessage !== "") {
    alertmsg("green", customFieldMessage);

    customFieldMessage = "";
}

var counter = 0;
var id = 0;
var customfieldData;
var customfieldGroup;

function getSingleCustomFieldTemplateId() {
    counter = 0;
    id = 0;
    $('input[type=checkbox][class=listCustomFieldTemplate]').each(function () {
        if (this.checked) {
            id = this.value;
            counter++;
        }
    });
    return id;
}

function getMultipleCustomFieldTemplateIds() {
    //counter = 0;
    //id = 0;
    var ids = "";
    $('input[type=checkbox][class=listCustomFieldTemplate]').each(function () {
        if (this.checked) {
            ids = ids + this.value + ",";
        }
    });
    return ids;
}

function getSingleCustomFieldGroupId() {
    counter = 0;
    id = 0;

    $('input[type=checkbox][class=customfieldGroupList]').each(function () {
        if (this.checked) {
            id = this.value;
            counter++;
        }
    });
    return id;
}

function getSingleCustomFieldId() {
    counter = 0;
    id = 0;

    $('input[type=checkbox][class=customFieldList]').each(function () {
        if (this.checked) {
            id = this.value;
            counter++;
        }
    });
    return id;
}

function getMultipleCustomFieldIds() {
    var ids = '';
    $('input[type=checkbox][class=customFieldList]').each(function () {
        if (this.checked) {
            ids += this.value +",";
        }
    });

    return ids;
}

function initializeCustomFieldTemplate() {
    $('#customTemplateGrid').DataTable({
        "destroy": true,
        "scrollX": true,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlGetCustomFieldTemplates,
        "bServerSide": true,
        "bSort": false,
        "bFilter": false,

        "aoColumns": [
                        {

                            "bSearchable": false,
                            "bSortable": false,
                            "mRender": function (data, type, full) {
                                return '<input type="checkbox" class="listCustomFieldTemplate" value="' + data + '">'
                            }
                        },
                        {

                            "bSortable": false,
                            "sName": "Name"
                        },
                       
                        {

                            "bSortable": false,
                            "sName": "Description"
                        }],
        "fnPreDrawCallback": function () {
            // gather info to compose a message
            var ob = this;
            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
            //SGtableloading('fa-plane', '', this);


        },
        "fnDrawCallback": function () {
            // in case your overlay needs to be put away automatically you can put it here
            var divid = $(this).closest('.dataTables_wrapper').attr('id');
            SGtableloadingRemove(divid);
        }
    });

   TblpgntBtm();

}

function initializeCustomFieldGroup() {

    var customTemplateId = getSingleCustomFieldTemplateId();

    if (customTemplateId === "" || counter > 1) {
        alertmsg('red', 'Please select one custom field template');
        return;
    }

    // Ajax call
    $.ajax({
        type: 'POST',
        url: urlGetCustomFieldGroups,
        cache: false,
        data: { 'customFieldTemplateId': customTemplateId },
        dataType: 'json',
        beforeSend: function () {
            SGloading('Loading');
        },
        success: function (response) {

            try {

                if (typeof response.aaData[0] !== 'undefined' && typeof response.aaData[0][0] !== 'undefined' && response.aaData[0][0] == "Error") {
                    alertmsg("red", "Unable to load");
                    return;
                }

                $('#grid-custom-field-group').html('');

                $('#grid-custom-field-group').append('<table id="customFieldGroupTable"><thead><tr></tr></thead></table>');
                
                //add heading to table
                $.each(response.sColumns, function (key, value) {
                    $('#customFieldGroupTable thead tr').append('<th>' + value + '</th>');
                });

                //add data to table via datatable
                customfieldGroup = $('#customFieldGroupTable').DataTable({
                    "bServerSide": true,
                    "destroy": true,
                    "scrollX": false,
                    "bSort": false,
                    "bFilter": false,
                    "aLengthMenu": dataTableMenuLength,
                    "iDisplayLength": dataTableDisplayLength,
                    "sAjaxSource": urlGetCustomFieldGroups + "?customFieldTemplateId=" + customTemplateId,
                    "sServerMethod": "POST",
                    "aoColumns": [
                    {
                        "sName": "Id",
                        "bSearchable": false,
                        "bSortable": false,
                        "width": "10",
                        "mRender": function (data, type, full) {
                            return '<input type="checkbox" class="customfieldGroupList" value="' + data + '">'
                        }
                    },
                    {
                        "sName": "Name"
                    },
                    {
                        "sName": "Description"
                    }
                    ],
                    "fnPreDrawCallback": function () {
                        // gather info to compose a message
                        var ob = this;
                        SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                        //SGtableloading('fa-plane', '', this);
                    },
                    "fnDrawCallback": function () {
                        // in case your overlay needs to be put away automatically you can put it here
                        var divid = $(this).closest('.dataTables_wrapper').attr('id');
                        SGtableloadingRemove(divid);
                    }
                });
              //  //TblpgntBtm();

            }
            catch (e) {
                alert(e);
            }
        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            console.log(xhr + ' ' + status + ' ' + error);
        }

    });

    TblpgntBtm();
}

function initializeCustomField() {

    var customFieldTemplateId = getSingleCustomFieldTemplateId();
    var customFieldGroupId = getSingleCustomFieldGroupId();

    if ((customFieldTemplateId === "" && customFieldGroupId === "") ) {
        alertmsg('red', 'Please select one customfield template or customfield group');
        return;
    }

    if (customFieldTemplateId === '')
        customFieldTemplateId = 0;
    if (customFieldGroupId === '')
        customFieldGroupId = 0;


    // Ajax call
    $.ajax({
        type: 'POST',
        url: urlGetCustomFields,
        cache: false,
        data: { 'customFieldTemplateId': customFieldTemplateId, 'customFieldGroupId': customFieldGroupId },
        dataType: 'json',
        beforeSend: function () {
            SGloading('Loading');
        },
        success: function (response) {

            try {
                $('#grid-custom-field').html('');

                try {
                    if (response.aaData[0][0] == "Error") {
                        alertmsg("red", "Unable to load");
                        return;
                    }
                } catch (e) {
                    // do nothing, continue ahead
                }

                $('#grid-custom-field').append('<table id="customFieldTable"><thead><tr></tr></thead></table>');
                
                //add heading to table
                $.each(response.sColumns, function (key, value) {
                    $('#customFieldTable thead tr').append('<th>' + value + '</th>');
                });

                //add data to table via datatable
                customfieldData = $('#customFieldTable').DataTable({
                    "sAjaxSource": urlGetCustomFields + "?customFieldTemplateId=" + customFieldTemplateId + "&customFieldGroupId=" + customFieldGroupId,
                    "destroy": true,
                    "scrollX": false,
                    "bServerSide": true,
                    "bSort": false,
                    "bFilter": false,
                    "aLengthMenu": dataTableMenuLength,
                    "iDisplayLength": dataTableDisplayLength,
                    "aoColumns": [
                        {
                            "sName": "Id",
                            "width": "10",
                            "mRender": function (data, type, full) {
                                return '<input type="checkbox" class="customFieldList" value="' + data + '">'
                            }
                        },

                   {
                       "sName": "Name"
                   },
                   {
                       "sName": "FieldAlias"
                   },
                   {
                       "sName": "FieldDataType"
                   }
                    ],
                    "fnPreDrawCallback": function () {
                        // gather info to compose a message
                        var ob = this;
                        SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                        //SGtableloading('fa-plane', '', this);


                    },
                    "fnDrawCallback": function () {
                        // in case your overlay needs to be put away automatically you can put it here
                        var divid = $(this).closest('.dataTables_wrapper').attr('id');
                        SGtableloadingRemove(divid)
                    }
                });


                TblpgntBtm();
              // //TblpgntBtm();

            }
            catch (e) {
                alert(e);
            }
        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            alert(status + ' ' + error);
        }
    });
    TblpgntBtm();

}

initializeCustomFieldTemplate();

$('body').on('click', '#customTemplateGrid tr', function (e) {
    setTimeout(function() {
        initializeCustomFieldGroup();
        TblpgntBtm();
        initializeCustomField();
        //TblpgntBtm();
    }, 200);

});

$('body').on('click', '#customFieldGroupTable tr', function (e) {
    setTimeout(function () {
        initializeCustomField();
        //TblpgntBtm();
    }, 200);

});

$('#selectAllCheckBoxCustomTemplate').click(function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.listCustomFieldTemplate').prop("checked", true);
    } else {
        $('.listCustomFieldTemplate').prop("checked", false);
    }
});

$("#rearrangeCustomFieldBtn").click(function(e) {
    var templateId = 0;

    //ensure that only one custom field template is checked for re-arranged
    if ($('input[type=checkbox][class=listCustomFieldTemplate]:checked').length != 1) {
        alertmsg("red", "Please select one Custom Field Template to rearrange the groups/fields.");
        return;
    }

    //get template id
    $('input[type=checkbox][class=listCustomFieldTemplate]').each(function () {
        if (this.checked) {
            templateId = this.value;
        }
    });

    urlRearrangeCustomFields += "?CustomFieldTemplateId=" + templateId;

    window.location = urlRearrangeCustomFields;

    //if (e.ctrlKey) {
    //    window.open(urlRearrangeCustomFields, "_blank");
    //} else {
    //    window.location = urlRearrangeCustomFields;
    //}
});

$("#createCustomFieldBtn").click(function () {
    var template = 0, group = 0;
    $('input[type=checkbox][class=listCustomFieldTemplate]').each(function () {
        if (this.checked) {
            template = this.value;
            
        }
    });
    
    $('input[type=checkbox][class=customfieldGroupList]').each(function () {
        if (this.checked) {
            group = this.value;
        }
    });

    window.location = urlCreateCustomField + "?template=" + template + "&group=" + group;
});

$("#createCustomFieldGroupdBtn").click(function () {
    var template = 0;
    $('input[type=checkbox][class=listCustomFieldTemplate]').each(function () {
        if (this.checked) {
            template = this.value;

        }
    });
    
    window.location = urlCreateCustomFieldGroup + "?template=" + template;
});

$("#createCustomFieldTemplateBtn").click(function () {
    window.location = urlCreateCustomFieldTemplate;
});

$("#editCustomFieldBtn").click(function() {

    var customFieldId = getSingleCustomFieldId();

    if (customFieldId != null && customFieldId !== 0) {
        window.location = urlEditCustomField + "?id=" + customFieldId;
    } else {
        alertmsg("red", "Please select one Custom Field to edit.");
    }
});

$("#editCustomFieldGroupBtn").click(function () {
    
        var customFieldGroupId = getSingleCustomFieldGroupId();

        if (customFieldGroupId != null && customFieldGroupId !== 0) {
            window.location = urlEditCustomFieldGroup + "?id=" + customFieldGroupId;
        } else {
            alertmsg("red", "Please select one Custom Field Group to edit.");
        }

});

$("#editCustomFieldTemplateBtn").click(function () {
    
        var customFieldTemplateId = getSingleCustomFieldTemplateId();

        if (customFieldTemplateId != null && customFieldTemplateId !== 0) {
            window.location = urlEditCustomFieldTemplate + "?id=" + customFieldTemplateId;
        } else {
            alertmsg("red", "Please select one Custom Field Template to edit.");
        }

});

$("#deleteCustomFieldBtn").click(function() {
    var selectedIds = getMultipleCustomFieldIds();
    if (!selectedIds) {
        alertmsg('red', 'Please select custom field to delete.');
        return;
    }

    deleteButtonClicked = "customfield";
    $("#divConfirmDeleteTitle").text("Delete Custom Field(s)");
    $("#divConfirmDelete .modal-body").text("Are you sure you want to delete the selected 'Custom Field(s)'? The association for export platform mapping will also be deleted.");
    $("#CustomFieldIdsToDelete").val(selectedIds);
    $('#divConfirmDelete').modal("show");
    

});

$("#deleteCustomFieldGroupBtn").click(function() {
    var selectedIds = getSingleCustomFieldGroupId();
    if (counter !== 1) {
        alertmsg('red', 'Please select custom field group to delete.');
        return;
    }

    deleteButtonClicked = "customfieldgroup";
    //console.log($("#divConfirmDeleteTitle").innerHTML);
    $("#divConfirmDeleteTitle").text("Delete Custom Field Group(s)");
    $("#divConfirmDelete .modal-body").text("Are you sure you want to delete the selected 'Custom Field Group(s)'?");
    $("#CustomFieldIdsToDelete").val(selectedIds);
    $('#divConfirmDelete').modal("show");
});

$("#deleteCustomFieldTemplateBtn").click(function() {

    var selectedIds = getMultipleCustomFieldTemplateIds();
    if (!selectedIds) {
        alertmsg('red', 'Please select custom field template to delete.');
        return;
    }
    
    deleteButtonClicked = "customfieldtemplate";
    $("#divConfirmDeleteTitle").text("Delete Custom Field Template(s)");
    $("#divConfirmDelete .modal-body").text("Are you sure you want to delete the selected 'Custom Field Template(s)'? It will delete the related Field Groups & Fields.");

    $("#CustomFieldIdsToDelete").val(selectedIds);
    $('#divConfirmDelete').modal("show");

});


$('#divConfirmDeleteYes').click(function () {
    var customFieldIds = $("#CustomFieldIdsToDelete").val();

    var urlForDelete = "";
    var type = "";
    if (deleteButtonClicked === "customfield") {
        urlForDelete = urlDeleteCustomField;
        type = "Custom Field(s)";
    } else if (deleteButtonClicked === "customfieldgroup") {
        urlForDelete = urlDeleteCustomFieldGroup;
        type = "Custom Field Group(s)";
    } else if (deleteButtonClicked === "customfieldtemplate") {
        urlForDelete = urlDeleteCustomFieldTemplate;
        type = "Custom Field Template(s)";
    }


    // Ajax call
    $.ajax({
        type: 'POST',
        url: urlForDelete,
        cache: false,
        data: { 'commaSeparatedIds': customFieldIds },
        dataType: 'json',
        success: function (response) {
            if (response === "OK") {
                
                if (deleteButtonClicked === "customfield") {
                    initializeCustomField();
                } else if (deleteButtonClicked === "customfieldgroup") {
                    initializeCustomFieldGroup();
                    initializeCustomField();
                } else if (deleteButtonClicked === "customfieldtemplate") {
                    initializeCustomFieldTemplate();
                    initializeCustomFieldGroup();
                    initializeCustomField();
                } else {
                    location.reload();
                }

                alertmsg("green", type + " deleted successfully");
            } else if (response === "FAILURE") {

                var message = "";
                if (type == "Custom Field(s)") {
                    message = "Failure while deleteting the " + type + ". The custom field(s) is associated with product catalog item(s).";
                }

                else if (type == "Custom Field Group(s)") {
                    message = "Failure while deleteting the " + type + ". Please delete the custom field(s) of the group first.";
                }

                else  {
                    message = "Failure while deleteting the " + type + ". Please delete the custom field group(s) first.";
                }


                alertmsg("red", message);
            }
            else {
                alertmsg("red", response);
            }
        },
        error: function (err) {
            location.reload();
            alertmsg("red", "Error while deleting the " + type);
        }
    });


});


$('#selectAllCheckBoxPricingQualifier').click(function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.listCustomFieldManger').prop("checked", true);
    } else {
        $('.listCustomFieldManger').prop("checked", false);
    }
});


$('.expand-cat-one').click(function () {
    //$table.floatThead('reflow');
    $('.expand-btn').hide();
    $('.collapse').show();
    $('.sg-catalog-one').show();
    $('.sg-catalog-one').removeClass('col-md-4').addClass('col-md-12');
    $('.sg-catalog-two').hide();
    $('.sg-catalog-three').hide();

    initializeCustomFieldTemplate();
    setTimeout(TblpgntBtm, 200);
    //TblpgntBtm();
    //InitializeDataTables();
});

$('.expand-cat-two').click(function() {
    //$table.floatThead('reflow');

    //var defaultValueId = $("#defaultValueLoadId").val();
    //if (defaultValueId == "") {
    //    return;
    //}
    var templateId = getSingleCustomFieldTemplateId();
    if (templateId == "") {
        return;
    }
    $('.expand-btn').hide();
    $('.collapse').show();
    $('.sg-catalog-two').show();
    $('.sg-catalog-two').removeClass('col-md-4').addClass('col-md-12');
    $('.sg-catalog-one').hide();
    $('.sg-catalog-three').hide();

    initializeCustomFieldGroup();
    setTimeout(function() {
        TblpgntBtm1();
    }, 200);

});

$('.expand-cat-three').click(function () {
    //$table.floatThead('reflow');

    var templateId = getSingleCustomFieldTemplateId();
    var groupId = getSingleCustomFieldGroupId();

    if (templateId == "" && groupId == "") {
        return;
    }
    $('.expand-btn').hide();
    $('.collapse').show();
    $('.sg-catalog-three').show();
    $('.sg-catalog-three').removeClass('col-md-4').addClass('col-md-12');
    $('.sg-catalog-one').hide();
    $('.sg-catalog-two').hide();

   initializeCustomField();
  
    //TblpgntBtm();
   setTimeout(TblpgntBtm1, 200);
});
$('.collapse').click(function () {
    //$table.floatThead('reflow');

    $('.collapse').hide();
    $('.expand-btn').show();
    $('.sg-catalog-one, .sg-catalog-two, .sg-catalog-three').show();
    $('.sg-catalog-one').removeClass('col-md-12').addClass('col-md-4');
    $('.sg-catalog-two').removeClass('col-md-12').addClass('col-md-4');
    $('.sg-catalog-three').removeClass('col-md-12').addClass('col-md-4');

    initializeCustomFieldTemplate();

    //setTimeout(TblpgntBtm, 200);
    //getSingleDefaultValueId();
    if (counter == 1) {
       initializeCustomFieldGroup();
        //TblpgntBtm();
       initializeCustomField();
        //TblpgntBtm();

    }

    //TblpgntBtm();

    setTimeout(TblpgntBtm, 200);
    //getSingleCustomFieldTemplateId();
    //if (counter == 1) {
    //   initializeCustomFieldGroup();
    //   // //TblpgntBtm();
    //   initializeCustomField();
    //    ////TblpgntBtm();
    //}

  // TblpgntBtm();
});




