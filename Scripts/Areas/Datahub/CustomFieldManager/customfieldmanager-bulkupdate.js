﻿//declaration
var hot;

$(function () {
    SGloading("Loading");
    //var fileSelector = $("#file");
    //var fileData = [];
    var container = document.getElementById("hot-bulk-update");
    //var columnsHeader = [];
    //var columns = [];

    //var sourceYesNo = ["Yes", "No"];

    var loadColumnHeaders = function() {
        var colHeaders = [
            "CFGroup", "CFGroupDisplayOrder", "CFName", "CFAlias", "CFDisplayOrder"
            //, "CFIsActive", "CFIsReadOnly","CFIsNullable", "YahooAlias", "YahooStandardField"
        ];
        return colHeaders;
    }

    var loadCustomFieldColumns = function() {
        var columns = [
            {
                name: "CFGroup",
                data: "CustomFieldGroup",
                type: "text",
                readOnly: true
            },
            {
                name: "CFGroupDisplayOrder",
                data: "CustomFieldGroupDisplayOrder",
                type: "numeric"
            },
            {
                name: "CFName",
                data: "CustomFieldName",
                type: "text",
                readOnly: true
        },
            {
                name: "CFAlias",
                data: "CustomFieldAlias",
                type: "text",
                readOnly: true
            },
            {
                name: "CFDisplayOrder",
                data: "CustomFieldDisplayOrder",
                type: "numeric"
            }
            /*,
            {
                name: "CFIsActive",
                data: "CustomFieldIsActive",
                type: "dropdown",
                source: sourceYesNo
            },
            {
                name: "CFIsReadOnly",
                data: "CustomFieldIsReadOnly",
                type: "dropdown",
                source: sourceYesNo
            },
            {
                name: "CFIsNullable",
                data: "CustomFieldIsNullable",
                type: "dropdown",
                source: sourceYesNo
            },
            {
                name: "YahooAlias",
                data: "YahooAlias",
                type: "text"
            },
            {
                name: "YahooStandardField",
                data: "YahooStandardField",
                type: "dropdown",
                source: sourceYesNo
            }*/
        ];

        return columns;
    }

    var loadCustomFieldData = function (data) {
        var columns = loadCustomFieldColumns();
        var colHeaders = loadColumnHeaders();
        var handsontableSettings = UdhHandsonTable.getSettings({
            data: data,
            columns: columns,
            colHeaders: colHeaders,
            fixedColumnsLeft: 0
        });

        hot = new Handsontable(container, handsontableSettings);

        if (data.length > 1 && hot.isEmptyRow(data.length - 1)) {
            //remove last row if last row is empty row (handsontable)
            hot.alter('remove_row', parseInt(data.length - 1));
        }

        setTimeout(function() {
            hot.render();
            SGloadingRemove();
        },
            2000);
    }

    //callback, show data to handsontable
    var renderHandsontable = function (data) {
        
        return function (data) {
            loadCustomFieldData(data);
        }
    }

    /*
    var papaParseOptions = {
        fileSelector: fileSelector,
        fileData: fileData,
        header: columnsHeader,
        columns: columns//,
        //callback: renderHandsontable(fileData)
    };

    UdhPapaParse.getConfiguration(papaParseOptions);
    */

    $('#btn-export')
        .click(function() {
            handsontable2ExportFormat.download(hot, "customfieldmanager-bulkupdate.csv", "csv", true);
        });

    $('#btn-save').click(function () {

            var data = hot.getSettings().data;

            $.ajax({
                url: urlSaveBulkUpdate,
                type: 'POST',
                data: { "data": data },
                dataType: "json",
                beforeSend: function () {
                    SGloading('Loading');
                },
                success: function (response) {
                    if (response != null && response.Status === true) {
                        alertmsg('green', response.Message);
                    } else if (response != null && response.Status === false) {
                        alertmsg('red', response.Message);
                    } else {
                        alertmsg('red', msgErrorOccured);
                    }
                },
                complete: function () {
                    SGloadingRemove();
                },
                error: function (err) {
                    console.log(err);
                }
            });
        });

    //load custom fields by default
    bulkUpdateData = bulkUpdateData.replace(/\\"/g, '"').replace(/"/g, '\\"');
    bulkUpdateData = decodeHtml(bulkUpdateData);
    bulkUpdateData = fixNewLineOnly(bulkUpdateData);
    //console.log(bulkUpdateData);

    loadCustomFieldData(bulkUpdateData);

});