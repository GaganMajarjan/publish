﻿if (customFieldMessage != null && customFieldMessage !== "") {
    alertmsg("green", customFieldMessage);
}


exportPlatformsCollection = JSON.parse(exportPlatformsCollection);

$('#btnCancel').click(function () {
    window.location = urlIndex;
});

$("select").select2();

exportPlatformsSelected = JSON.parse(exportPlatformsSelected);
if (exportPlatformsSelected != null) {
    $.each(exportPlatformsSelected, function(index, value) {
        var idSelected = "{0}_{1}__{2}".format("ExportPlatforms", index.toString(), "ExportPlatformSdvId");

        if ($("#" + idSelected).length) {
            $("#" + idSelected).select2("val", value.ExportPlatformSdvId);
        }
    });
}

$("#customFieldTemplateDrop").change(function (val) {

    var templateId = val.val;
    if (templateId == null || templateId === "")
        templateId = 0;

    $.ajax({
        type: 'POST',
        url: urlGetCustomFieldGroupsForTemplate,
        cache: false,
        data: { 'templateId': templateId },
        dataType: 'json',
        success: function (response) {

            if (response != null && response !== "") {

                var jsonResult = $.parseJSON(response);
                console.log(jsonResult);

                var selectOptions = $('<select>');
                selectOptions.append(
                    $('<option></option>').val("").html(pleaseSelect));

                $.each(jsonResult, function (index, val) {

                    selectOptions.append(
                            $('<option></option>').val(val.Id).html(val.Name)
                        );
                });

                $("#customFieldGroupDrop")
                                .find('option')
                                .remove()
                                .end()
                                .append(selectOptions.html());

            }
        },

        error: function (err) {
            alertmsg("red", errorOccurred);
        }
    });
});


$(document).on('click', ".removeBtn", function () {

    var dataTypeId = $(this).attr('id');
    var numberedId = dataTypeId.substr(dataTypeId.indexOf('_') + 1, dataTypeId.length);

    $("#tr_" + numberedId).remove();


    var labelId = $('#qualifiedExportPlatforms tr:last td input').attr('value');
    labelId = parseInt(labelId);


    for (var i = 0; i <= labelId; i++) {

        var newNumber = parseInt(numberedId) + parseInt(i) + 1;

        var idFormat = "{0}{1}{2}".f("ExportPlatforms_", newNumber.toString(), "__ExportPlatformSdvId");


        if ($("#" + idFormat).length) {

            console.log('test');

            var idFormatForNew = "{0}_{1}__{2}".format("ExportPlatforms", (newNumber - 1).toString(), "ExportPlatformSdvId");
            var nameFormatForNew = "{0}[{1}].{2}".format("ExportPlatforms", (newNumber - 1).toString(), "ExportPlatformSdvId");
            $("#" + idFormat).attr("name", nameFormatForNew);
            $("#" + idFormat).attr("id", idFormatForNew);


            idFormat = "{0}_{1}__{2}".format("ExportPlatforms", newNumber.toString(), "PlatformFieldAlias");
            idFormatForNew = "{0}_{1}__{2}".format("ExportPlatforms", (newNumber - 1).toString(), "PlatformFieldAlias");
            nameFormatForNew = "{0}[{1}].{2}".format("ExportPlatforms", (newNumber - 1).toString(), "PlatformFieldAlias");
            $("#" + idFormat).attr("name", nameFormatForNew);
            $("#" + idFormat).attr("id", idFormatForNew);


            $("#" + newNumber.toString()).attr("value", (newNumber - 1));
            $("#" + newNumber.toString()).attr("id", (newNumber - 1).toString());

            $("#remove_" + newNumber.toString()).attr("id", "remove_" + (newNumber - 1).toString());
            $("#tr_" + newNumber.toString()).attr("id", "tr_" + (newNumber - 1).toString());



        }
    }


});

$('#addMoreRow').click(function () {

    var labelId = $('#qualifiedExportPlatforms tr:last td input').attr('value');
    var numberedId = "0";
    if (labelId != null)
        numberedId = parseInt(labelId) + 1;

    numberedId = parseInt(numberedId);

    $('#qualifiedExportPlatforms tbody').append('<tr id="tr_' + numberedId + '">' +

        '<td style="display:none;"><input type="hidden" id="' + numberedId + '" class="sourceLabel" name="label[]" value="' + numberedId + '" /></td>' +
        '<td><select id="ExportPlatforms_' + numberedId + '__ExportPlatformSdvId" name="ExportPlatforms[' + numberedId + '].ExportPlatformSdvId" class="sourceDrop" style="width:100%;" name="Value"></select></td> ' +
        '<td><div class="input-group"><input id="ExportPlatforms_' + numberedId + '__PlatformFieldAlias" name="ExportPlatforms[' + numberedId + '].PlatformFieldAlias" type="text" class="sg-tooltip form-control" data-toggle="tooltip" data-placement="left" title="Platform Field Alias"/>' +
        '<div class="input-group-btn"><a id="remove_' + numberedId + '" class="btn btn-danger removeBtn"  data-toggle="tooltip" data-placement="top" title="remove the row"><i class="fa fa-close"></i></a></div>' +
        '</div></td>' +

        '</tr>');


    $.each(exportPlatformsCollection, function (index, value) {
        $("#ExportPlatforms_" + numberedId + "__ExportPlatformSdvId").append(new Option(value.Text, value.Value));
    });

    $("#ExportPlatforms_" + numberedId + "__ExportPlatformSdvId").select2();

});
$('#fieldDataTypeDrop').change(function () {
    var value = $(this).val();
    if (value == "68314") {
        $('#allowedValuesSdtIdDrop').select2().val('');
        $('#allowedValuesSdtIdDrop').select2().val('');
        $('#allowedValuesSdtIdDrop').attr('disabled', 'disabled');
    } else {
        $('#allowedValuesSdtIdDrop').removeAttr('disabled', 'disabled');
    }
});
$('#allowedValuesSdtIdDrop').change(function () {
    $('#fieldDataTypeDrop').select2().val('');
    var $value = $('#fieldDataTypeDrop')
    if ($value !== " ") {
        $value.select2('val','68312');
    }
});