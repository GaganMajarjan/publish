﻿
/* tooltip */
var showToolTip = function () {

    var ulQuestionList = $('ul.question-list');
    var hasContent = $('#feed-generator .wtSpreader thead tr th').length;

    if (hasContent >= 1) {
        ulQuestionList.css('display', 'block');
    } else {
        ulQuestionList.css('display', 'none');

    }
    
};


var container = document.getElementById('feed-generator');


//data = JSON.parse(data);

//columns = JSON.parse(columns);

if (data === '' || data === '[{}]') {
    $("#btn-generate-feed").hide();
    $("#btn-upload-feed").hide();
    $("#searchResultCountLabel").hide();

}
data = fixJsonQuote(data);

if (columns != null)
    columns = fixJsonQuote(columns);

var getDefaultHandsonValue = function (columns) {
    var defaultValue = {};
    for (var i = 0; i < columns.length; i++) {
        var data = columns[i].data;
        var type = columns[i].type;
        if (type === 'checkbox') {
            defaultValue[data] = false;
        } else {
            defaultValue[data] = null;
        }
    }
    return defaultValue;
}

var dataSchema = getDefaultHandsonValue(columns);

var settings = {
    data: data,
    dataSchema: dataSchema,
    rowHeaders: false,
    //stretchH: 'all',
    fillHandle: 'vertical',
    columnSorting: false,
    search: true,
    fixedRowsTop: 1,
    fixedColumnsLeft: 0,
    contextMenu: true,
    licenseKey: 'non-commercial-and-evaluation',
    columns: columns,
    minSpareRows: 0
};

var hot = new Handsontable(container, settings);

hot.render();

function parseRow(infoArray, index, csvContent) {
    var sizeData = _.size(hot.getSettings().data);
    if (index < sizeData - 1) {
        dataString = "";
        _.each(infoArray, function (col, i) {
            dataString += _.contains(col, ",") ? "\"" + col + "\"" : col;
            dataString += i < _.size(infoArray) - 1 ? "," : "";
        });

        csvContent += index < sizeData - 2 ? dataString + "\n" : dataString;
    }
    return csvContent;
}

$("#btn-generate-feed").click(function () {

    //for client side export
    var feedExportObject = '';

    if (hot.getSettings().data.length === 0) {
        alertmsg('red', 'Please load data to save');
        return;
    }
    feedExportObject = JSON.stringify(hot.getSettings().data);

    //for server side export
    var storeId = '0';
    if ($("#storeId").length)
        storeId = $("#storeId").val();

    var exportPlatformId = $("#exportPlatform").val();

    if (storeId != null && storeId !== '') {

        var feedRules = "";
        if ($("#productSuppressionFilterDiv:visible").length === 1) {
            feedRules = $('#builder').queryBuilder('getSQL', false);
            feedRules = feedRules.sql.replace("\n", " ");
        }

        feedRules = normalizeFeedRule(feedRules);

        var exportFormat = $("#exportFormat").val();
        console.log('export feed format ');
        console.log(exportFormat);
        if (exportFormat == null || exportFormat === "")
            exportFormat = "tsv";


        // Ajax call
        $.ajax({
            type: 'POST',
            url: urlGetFeedExportDataForFilter,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: {
                'storeId': storeId,
                'platformId': exportPlatformId,
                'fileFormat': exportFormat,
                'filterRules': feedRules,
                'feedExportData': feedExportObject
            },
            dataType: 'json',
            beforeSend: function() {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function(response) {

                if (response != null && response.Status === true) {

                    //var exportFormat = $("#exportFormat").val();
                    //console.log('export feed format ');
                    //console.log(exportFormat);
                    //if (exportFormat == null || exportFormat === "")
                    //    exportFormat = "tsv";

                    //handsontable2ExportFormat.download(hot, "product-feed." + exportFormat, exportFormat, false);

                    window.location = urlDownloadFile + "?storeId=" + storeId + "&platformId=" + exportPlatformId +
                        "&fileFormat=" + exportFormat + "&filterRules=" + feedRules;

                } else if (response != null && response.Status === false) {
                    alertmsg('red', response.Message);
                }


            },
            complete: function() {
                SGloadingRemove();
            },
            error: function(xhr, status, error) {

                alertmsg("red", msgErrorOccured);
                SGloadingRemove();

            }

        });

    }
});

$('#searchFeed').click(function () {
    getPaginatedGrid();
});                               

$("#exportPlatform").change(function (val) {

    var platformId = val.currentTarget.value;
    if (platformId == null || platformId === "") {
        return;
    }

    $.ajax({
        url: urlGetPlatformSpecificColumns,
        data: { platformId: platformId },
        type: 'GET',
        cache: false,
        traditional: true,
        beforeSend: function () {

            SGloading('Loading');


        },
        success: function (response) {

            if (response != null && response.Status === true) {

                try {
                    var jsonData = JSON.parse(response.Data);
                    if (jsonData.filters != null) {
                        $('#builder').queryBuilder(jsonData);
                        $("#productSuppressionFilterDiv").css('display', 'block');

                    } else {
                        $("#productSuppressionFilterDiv").css('display', 'none');
                    }

                } catch (e) {
                    //  console.log(e);
                    //   alertmsg("red", e);
                    SGloadingRemove();
                    $("#productSuppressionFilterDiv").css('display', 'none');

                } 
                
                
            } else if (response != null && response.Status === false) {
                alertmsg("red", response.Message);
                $("#productSuppressionFilterDiv").css('display', 'none');

            } else {
                alertmsg("red", errorOccurredMessage);
                $("#productSuppressionFilterDiv").css('display', 'none');


            }


        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (xhr, status, error) {

            //reset variables here
            alertmsg("red", status + ' ' + error);
            SGloadingRemove();
            $("#productSuppressionFilterDiv").css('display', 'none');

        }
    });

});

var normalizeFeedRule = function(feedRule) {

    var patternForIn = /(IN\((.*)\))/gi;
    if (feedRule.match(patternForIn)) {
        feedRule = feedRule.replace(",", "','");
    }
    
    return feedRule;
}

var renderFeedGeneratorGrid = function(storeId,exportPlatformId,feedRules,pageNumber, pageSize, totalRows)
{
    

    // Ajax call
    $.ajax({
        type: 'POST',
        url: urlGetFeedGenerator,
        cache: false,
        //contentType: 'application/json; charset=utf-8',
        data: {
            'storeId': storeId,
            'selectedPlatformId': exportPlatformId,
            'feedRules': feedRules,
            'offsetRows': pageNumber,
            'fetchRows': pageSize,
            'isCountOnly':false
        },
        dataType: 'json',
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Loading');
        },
        success: function (response) {

            if (response != null && response.Status === true) {
                //alertmsg('green', response.Message);

                var data = response.Data.data;
                var cols = response.Data.columns;
                var rows = response.Data.rowsCount;

                data = JSON.parse(data);
                cols = JSON.parse(cols);

                hot.updateSettings({
                    data: data,
                    columns: cols
                });

                //calculate page numbering
                var fromCount = (pageNumber - 1) * pageSize + 1;
                var toCount = (pageNumber * pageSize > totalRows) ? totalRows : pageNumber * pageSize;

                $('#feed-generator').show();

                $("#btn-generate-feed").show();
                $("#btn-upload-feed").show();
                $("#searchResultCountLabel").show();
                $("#searchResultCountLabel").text(searchResultMessage.format(fromCount, toCount, totalRows));

                showToolTip();


            } else if (response != null && response.Status === false) {
                alertmsg('red', response.Message);
                try {
                    hot.clear();
                    hot.updateSettings({
                        data: null,
                        columns: null
                    });
                    $('#feed-generator').hide();
                    $("#btn-generate-feed").hide();
                    $("#btn-upload-feed").hide();
                    $("#searchResultCountLabel").hide();


                } catch (er) {
                    //alertmsg("red",msgErrorOccured);
                }
            } else {
                alertmsg('red', msgErrorOccured);
                try {
                    hot.clear();
                    hot.updateSettings({
                        data: null,
                        columns: null
                    });
                    $('#feed-generator').hide();
                    $("#searchResultCountLabel").hide();
                    $("#btn-generate-feed").hide();
                    $("#btn-upload-feed").hide();

                } catch (er) {
                    // alertmsg("red", msgErrorOccured);


                }
            }
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            // alert(status + ' ' + error);
            alertmsg("red", msgErrorOccured);

            try {
                hot.clear();
                hot.updateSettings({
                    data: null,
                    columns: null
                });
                $('#feed-generator').hide();
                $("#searchResultCountLabel").hide();
                $("#btn-generate-feed").hide();
                $("#btn-upload-feed").hide();


            } catch (er) {
                //alertmsg("red", msgErrorOccured);


            }
        }

    });
}

var getPaginatedGrid = function () {

    var storeId = '0';
    if ($("#storeId").length)
        storeId = $("#storeId").val();

    var exportPlatformId = $("#exportPlatform").val();

    if (storeId != null && storeId !== '') {

        var feedRules = "";
        if ($("#productSuppressionFilterDiv:visible").length === 1) {
            feedRules = $('#builder').queryBuilder('getSQL', false);
            feedRules = feedRules.sql.replace("\n", " ");
        }

        feedRules = normalizeFeedRule(feedRules);
        $.ajax({
            type: 'POST',
            url: urlGetFeedGeneratorCount,

            data: {
                'storeId': storeId,
                'selectedPlatformId': exportPlatformId,
                'feedRules': feedRules
            },
            beforeSend: function() {

                SGloading('Loading');
            },
            success: function(response) {

                if (response != null && response.Status === true) {

                    renderFeedGeneratorGrid(storeId, exportPlatformId, feedRules, 1, 20, response.Data);
                    $("#pagination").pagination({
                        items: response.Data,
                        itemsOnPage: 20,
                        cssStyle: 'light-theme',
                        onPageClick: function(e) {
                            renderFeedGeneratorGrid(storeId, exportPlatformId, feedRules, e, 20, response.Data);
                        }
                    });
                }
                else if (response != null && response.Status === false) {
                    alertmsg("red", response.Message);
                }

            },
            complete: function() {

                SGloadingRemove();

            },
            error: function(xhr, status, error) {

                console.log(status + ' ' + error);
            }
        });

    }
}
