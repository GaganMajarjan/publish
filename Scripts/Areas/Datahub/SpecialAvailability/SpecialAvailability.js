﻿//var apiUrl = "http://202.166.206.49:8081/DEV_API/api";
//var apiUrl = "http://localhost:60936/api";
$("#btndeleteavailabilityOpen").hide();
//for variable previousItemId
// hide content depending upon the item Id value if the item id value is changed
// if changed again search for availability
var previousItemId = "";
//for vairable localskucode
// user may search both localsku or yahooid so  getvalue localsku if data present 
var localSkuCode = "";
//for variable keIdStore ->same as localskucode 
var keIdStore = "";

var supplierforDropdown = "";
// for remote supplier not in use
var remoteDataConfigAPI = function (remoteDataUrl, minimumInputLength, delay, placeholder,localsku) {

    minimumInputLength = typeof minimumInputLength !== 'undefined' ? minimumInputLength : 1;
    delay = typeof delay !== 'undefined' ? delay : 250;
    placeholder = typeof placeholder !== 'undefined' ? placeholder : '--Choose';
    console.log("localsku: " + localsku);
    var config = {

        placeholder: placeholder,
        allowClear: true,
        delay: delay,
        minimumInputLength: minimumInputLength,
        ajax: {
            url: remoteDataUrl,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    code: localsku,
                    searchTerm: params.term, // search term
                    page: params.page
                   
                };
            },
            processResults: function (data, params) {
                var json_data = JSON.stringify(data);
                if (json_data) {
                    params.page = params.page || 1;

                    data = JSON.parse(json_data);
                    var select2Data = $.map(data, function (obj) {
                        obj.id = obj.Id;
                        obj.text = obj.Text;

                        return obj;
                    });

                    return {
                        results: select2Data,
                        pagination: {
                            more: data.more
                        }
                    };
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; },
        //templateResult: formatRepo,
        //templateSelection: formatRepoSelection
        templateResult: function (option) {
            if (option.loading) return option.text;
            return "<option>" + option.Text + "</option>";
        },
        templateSelection: function (option) {
            return option.full_name || option.text;
        }
    };

    return config;
}

var loadSupplier = function(localSkuCode) {
    $('#SupplierId').select2(remoteDataConfigAPI(apiUrl + "/supplier?", 3, 250, pleaseSelectMessage, localSkuCode));
}
//  for remote supplier not in useupto here

$("#refreshAvailability").click(function () {

    var searchText = $("#keid").val();
    if (isEmptyOrSpaces(searchText)) {
        document.getElementById('availabilityId').value = "";
        //document.getElementById('keid').value = "";
        document.getElementById('availabilityMessage').value = "";
        document.getElementById('leadweight').value = "";
        document.getElementById('expiration').value = "";
        document.getElementById('notes').value = "";
        //$("#SupplierId").empty().append('<option value="' + "" + '">' + "" + '</option>').val("").trigger('change');
        $("label.messagebatch").replaceWith("<label class='messagebatch'>" + "" + "</label>");

        $(".more-info").addClass("hidden", 50);
        return;
    }
    $.ajax({
        url: apiUrl + "/availabilityrefresh",
        beforeSend: function () {
            SGloading('Loading');
        },
        complete: function () {
            SGloadingRemove();
        },
        type: 'GET',
        data: {
            searchText: searchText
        },
        success: function (data) {
            if (data) {
    document.getElementById('availabilityId').value = "";
                //document.getElementById('keid').value = "";
    document.getElementById('availabilityMessage').value = "";
    document.getElementById('leadweight').value = "";
    document.getElementById('expiration').value = "";
    document.getElementById('notes').value = "";
    //$("#SupplierId").empty().append('<option value="' + "" + '">' + "" + '</option>').val("").trigger('change');
    $("label.messagebatch").replaceWith("<label class='messagebatch'>" + "" + "</label>");
   
    $(".more-info").addClass("hidden", 50);
                alertmsg("green", "Availability for this item has been refreshed.");
            }
        }
    });

});

$("#keid").focusout(function () {
    if (previousItemId != $("#keid").val()) {
        previousItemId = $("#keid").val();
        $("#saveAvailability").attr("disabled", true);
        $(".more-info").addClass("hidden", 50);
    }
});

$("#availabilityMessage").focusout(function() {
    var availabilityMessage = $("#availabilityMessage").val();

    if (Math.floor(availabilityMessage) == availabilityMessage && $.isNumeric(availabilityMessage)) {
        $("#leadWeightDiv").hide();
        document.getElementById('leadweight').value = "";
    } else {
        $("#leadWeightDiv").show();
    }
});

$("#searchAvailability").click(function () {
    $("#saveAvailability").attr("disabled", false);
    $("#leadWeightDiv").show();
    var searchText = $("#keid").val();
 
    $.ajax({
        url: apiUrl + '/availability?searchText=' + searchText,
        type: 'GET',
        success: function (data) {
            console.log(data);
            if (data.LocalSku == "norecordfound") {
                alertmsg("red", "This item does not exist. Enter a valid Item ID.");
                $(".more-info").addClass("hidden", 50);
                return;
            }
            localSkuCode = data.LocalSku;
            //  load supplier dropdown depending on availability searched
            var supurl = apiUrl + "/supplier?code=" + localSkuCode + "&searchTerm=" + searchText;
         
            $.ajax({
                url: supurl,
                type: 'GET',
                beforeSend: function () {
                    SGloading('Loading');
                },
                complete: function () {
                    SGloadingRemove();
                },
                success: function (supResponse) {
                    $("#SupplierId").empty();
                    $.grep(supResponse, function (n, i) {
                        if (isEmptyOrSpaces(data.SupplierId)) {
                            if (n.Text.indexOf("-- Primary Supplier") >= 0) {
                                $("#SupplierId").append('<option selected=true value=' + n.Id + '>' + n.Text + '</option>');

                            } else {
                                $("#SupplierId").append('<option value=' + n.Id + '>' + n.Text + '</option>');
                            }

                        } else {

                            if (n.Id == data.SupplierId) {
                                $("#SupplierId").append('<option selected=true value=' + n.Id + '>' + n.Text + '</option>');
                            } else {
                                $("#SupplierId").append('<option value=' + n.Id + '>' + n.Text + '</option>');
                            }
                        }

                    });
          
           
            keIdStore = data.KeId;
            $(".more-info").removeClass("hidden", 500);

            var d = new Date();
            var day = d.getDate();
            var month = d.getMonth() + 1; //javascript month needs to be incremented by 1
            var year = d.getFullYear();

            // if availability not present 
            if (isEmptyOrSpaces(data.AvailabilityId) || data.AvailabilityId == "null" || data.AvailabilityId == null) {
                $("label.messagebatch").replaceWith("<label class='messagebatch'>" + "" + "</label>");
                document.getElementById('availabilityId').value = "";
                document.getElementById('availabilityMessage').value = "";
                document.getElementById('leadweight').value = "";
                document.getElementById('expiration').value = "";
                document.getElementById('notes').value =  "\r\n Created on " + year + "-" + month + "-" + day + " by " + userName + " ";
                //$("#SupplierId").val(data.SupplierId);
                $("#saveAvailability").html('Create');
                $("#setToNever").click();
             
                return;
            } else {
            // here availability present so populate data
                $("label.messagebatch").replaceWith("<label class='messagebatch'>&nbsp; [ " + data.KeId + " ] </label>");
                document.getElementById('availabilityId').value = data.AvailabilityId;
                keIdStore = data.KeId;
         
            var test = data.AvailabilityMessage;
            var removedStartMessage = test.replace("<<", "").replace(">>", "");
            document.getElementById('availabilityMessage').value = removedStartMessage;
            document.getElementById('leadweight').value = data.LeadWeight;
            document.getElementById('expiration').value = data.Expiration;
        
            data.Notes += "\r\n Updated on " + year + "-" + month + "-" + day + " by " + userName + " ";
            document.getElementById('notes').value = data.Notes;
            //$("#SupplierId").empty().append('<option value="' + data.SupplierId + '">' + data.SupplierName + '</option>').val(data.SupplierId).trigger('change');
                //$("#SupplierId").val(data.SupplierId);
      
            //    $("#SupplierId").val(data.SupplierId).html(data.SupplierName);
            //    document.getElementById('SupplierId').value = data.SupplierId;
            //    //console.log($("#SupplierId").find("option[value='" + data.SupplierId + "']"));
            //    //$("#SupplierId").find("option[value='" + data.SupplierId + "']").attr("selected", true);
            //    $("#SupplierId option[text='" + data.SupplierName + "']").attr("selected", "selected");
            $("#saveAvailability").html('Update');
                supplierforDropdown = data.SupplierId;
                $("#SupplierId").find("option[value=" + supplierforDropdown + "]").val(supplierforDropdown);

            }
            // to hide lead weight according to value of availabilitymessage
            $("#availabilityMessage").focusout();
                }
            });
        },
        error: function (x, y, z) {
            console.log(x + '\n' + y + '\n' + z);
        }
    });

   
});

$("#deleteAvailability").click(function() {

    var availabilityId = $("#availabilityId").val();
    if (isEmptyOrSpaces(availabilityId)) {
        alertmsg("red", "Please search Item to delete");
        return;
    }
    $("#btndeleteavailabilityOpen").click();


});

$("#btndeletefromModel").click(function () {

    var availabilityId = $("#availabilityId").val();
    var deleteUrl = apiUrl + '/availability/' + availabilityId;
    $("label.messagebatch").replaceWith("<label class='messagebatch'>" + "" + "</label>");
    $.ajax({
        url: deleteUrl,
        type: 'DELETE',
        success: function (data) {
            document.getElementById('availabilityId').value = "";
            //document.getElementById('keid').value = "";
            document.getElementById('availabilityMessage').value = "";
            document.getElementById('leadweight').value = "";
            document.getElementById('expiration').value = "";
            document.getElementById('notes').value = "";
            //$("#SupplierId").empty().append('<option value="' + "" + '">' + "" + '</option>').val("").trigger('change');
            $("label.messagebatch").replaceWith("<label class='messagebatch'>" + "" + "</label>");

            $(".more-info").addClass("hidden", 50);
            alertmsg("green", "Availability for this item has been deleted.");
            $("#deletemodelNobtn").click();
        }
    });
});

$("#saveAvailability").click(function () {
   
    var availabilityId = $("#availabilityId").val();
    var keId = keIdStore;// $("#keid").val();
    var supplierId = $("#SupplierId").val();
    var availabilityMessage = $("#availabilityMessage").val();
    var leadWeight = $("#leadweight").val();
    var expiration = $("#expiration").val();
    var notes = $("#notes").val();
    var availabilityUrl = apiUrl + '/availability';

    if (isEmptyOrSpaces(keId)) {
        alertmsg("red", "ItemID is required");
        return;
    }
    if (isEmptyOrSpaces(supplierId)) {
        alertmsg("red", "Supplier is required");
        return;
    }
    if (isEmptyOrSpaces(availabilityMessage)) {
        alertmsg("red", "Availability Details is required");
        return;
    }
    if (Math.floor(availabilityMessage) == availabilityMessage && $.isNumeric(availabilityMessage)) {

    } else {
        if (isEmptyOrSpaces(leadWeight)) {
            alertmsg("red", "Lead Weight Details is required");
            return;
        }
    }
  



    if (isEmptyOrSpaces(availabilityId)) {
      
        // Create Availability

        $.ajax({
            url: availabilityUrl,
            beforeSend: function () {
                SGloading('Loading');
            },
            complete: function () {
                SGloadingRemove();
            },
            type: 'POST',
            data: {
                AvailabilityId: availabilityId,
                KeId: keId,
                SupplierId: supplierId,
                AvailabilityMessage: availabilityMessage,
                LeadWeight: leadWeight,
                Expiration: expiration,
                Notes: notes
            },
            success: function (data) {
                if (data) {
                    alertmsg("green", "Successfully Created");
                    $("#searchAvailability").click();
                }
            }
        });
    } else {

        // Update availability
       
        $.ajax({
            url: availabilityUrl,
            beforeSend: function () {
                SGloading('Loading');
            },
            complete: function () {
                SGloadingRemove();
            },
            type: 'PUT',
            data: {
                AvailabilityId: availabilityId,
                KeId: keId,
                SupplierId: supplierId,
                AvailabilityMessage: availabilityMessage,
                LeadWeight: leadWeight,
                Expiration: expiration,
                Notes: notes
            },
            success: function (data) {
                if (data) {
                    alertmsg("green", "Successfully Updated");
                    $("#searchAvailability").click();
                }
            }
        });
    }

});

