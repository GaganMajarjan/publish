﻿$(document).ready(function () {


$('#PcColumns').change(function () {
    var selectedIndex = $("#PcColumns")[0].selectedIndex;
    $("#VpcColumns")[0].selectedIndex = selectedIndex;
});

$('#VpcColumns').change(function () {
    var selectedIndex = $("#VpcColumns")[0].selectedIndex;
    $("#PcColumns")[0].selectedIndex = selectedIndex;
});

//$('#btnSearch').click(function () {

//    // Ajax call
//    $.ajax({
//        type: 'POST',
//        url: urlFuzzyMatchDetail,
//        cache: false,
//        //contentType: 'application/json; charset=utf-8',
//        data: { 'vendorId': vendorId, 'vpcColumns': vpc, 'pcColumns': pc, 'scoreThreshold': threshold },
//        dataType: 'json',
//        beforeSend: function () {
//            //$("#btnsave").attr('disabled', true);
//            SGloading('Loading');
//        },
//        success: function (response) {
//            try {
//                //Show breadcrumb information
//                //$('#errorBreadcumb').html(fuzzyMatch + " > " + vendorName);

//                $('#testGridDiv').html('');
//                try {
//                    if (response.aaData[0][0] == "Error") {
//                        alertmsg("red", "Unable to load fuzzy match suggestion");
//                        return;
//                    }
//                } catch (e) {
//                    // do nothing, continue ahead
//                }

//                if (response.iTotalRecords == "0") {
//                    alertmsg("red", "No fuzzy match suggestion found at " + threshold + "% threshold. Please try with lower threshold value or different combination.");
//                    return;
//                }

//                $('#testGridDiv').append('<div class="col-md-12"><div class="sg-datatable-wrap"><table id="testGrid"><thead><tr></tr></thead></table></div></div>');

//                //add heading to table
//                $.each(response.sColumns, function (key, value) {
//                    $('#testGrid thead tr').append('<th>' + value + '</th>');
//                });
//                //add data to table via datatable
//                var table = $('#testGrid').DataTable({
//                    //destroy: true,
//                    data: response.aaData,
//                    displayLength: 25
//                });

//            } catch (e) { }
//        },
//        complete: function () {
//            //$("#btnsave").attr('disabled', false);
//            SGloadingRemove();
//        },
//        error: function (xhr, status, error) {
//            //reset variables here
//            alert(status + ' ' + error);
//        }
//    });
//});


$('#filterExpand').click(function () {
    $('#SearchText').click();
});


$('#FuzzyMatchDataTable').dataTableWithFilter({
    "destroy": true,
    "bProcessing": true,
     "bJQueryUI": true,
    "bServerSide": true,
    "bFilter": false,
    "scrollX": true,
    "bSort": false,
    "stateSave": true,
    "bScrollCollapse": true,

    "dom": 'T<"clear">lfrtip',
    "tableTools": {
        "sSwfPath": tableToolsPath,
        "aButtons": tableToolsOptions
    },
   
   
   "aLengthMenu": dataTableMenuLength,
    "iDisplayLength": dataTableDisplayLength,
    "sAjaxSource": urlFuzzyMatchDataTable,
    "sServerMethod": "POST",
   
 

    // Initialize our custom filtering buttons and the container that the inputs live in
    filterOptions: { searchButton: "SearchText", clearSearchButton: "ClearSearch", searchContainer: "SearchContainer" }

});
setTimeout(customJqueryUIpara('.sg-datatable-wrap'), 500);
});