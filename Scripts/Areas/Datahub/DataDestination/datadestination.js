﻿
$(document).ready(function () {
    //$(function () {
        //set the main frame height on page load and page resize
        
    //});

    if (dataDestinationColsUpdated != null && dataDestinationColsUpdated != "") {
        alertmsg("green", dataDestinationColsUpdated);
    }
});

$('#btnImportTemplate').click(function() {
    window.location = urlImportTemplate;

});
$('.sg-add-destination').click(function (e) {
    $(this).addClass('selected');
    $('.sg-show-select').show() /*.width($('.sg-map-panel').width() - $('.sg-add-destination').width() - 30) */;
});

$('body').click(function (e) {
    if (!($(e.target).hasClass('sg-add-destination') || $(e.target).closest('.select2-container').length)) {
        $('.sg-add-destination').removeClass('selected');
        $('.sg-show-select').hide();
    }
    console.log(e.target);
});
    
$('#selectAllCheckBoxDataDestination').click(function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.Vendorselector').prop("checked", true);
    } else {
        $('.Vendorselector').prop("checked", false);
    }
});


$('#butnDeleteDestTableYes').click(function () {

    var DestTblName = $("#DestinationTableNamesforDelete").val();
    $.ajax({
        url: urlDeleteDestinationTable,
        data: {
            DestTblNames: DestTblName
        },
        type: 'POST',
        cache: false,
        success: function (data) {
            if (data == "ok") {
                location.reload();
            }

        }
    });

});


$('#butnDeleteDestinationTable').click(function (e) {
    var i = 0;
    var destinationTableNames = "";
    $('input[type=checkbox][class=Vendorselector]').each(function () {

        if (this.checked) {
            i++;
            destinationTableNames = destinationTableNames + this.value + ",";
        }
    });

    if (i == 0) {
        alertmsg("red", "Please select a destination table to delete.");
        e.preventDefault();
    } else {
        $("#DestinationTableNamesforDelete").val(destinationTableNames);

        $("#DeleteDestTableModel").modal('show');
    }
});


$("#cancleDestTable").hide();
$("#getDestCols").hide();

function ucFirstAllWords(str) {
    var pieces = str.split(" ");
    for (var i = 0; i < pieces.length; i++) {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
}

$('#cancleDestTable').click(function () {
    window.location.reload();
});



$('#getDestCols').click(function () {
    var DestColdata = [];
    var i = 1;
    $('.sourceLabel').each(function () {
        var displayName = $(this).val();
        var order = $('#order' + i).val();
        var tableName = $('#tableName' + i).text();
        var destcols = order + "," + displayName + "," + tableName;
        //console.log(destcols);
        DestColdata.push(destcols);
        i++;
    });
    var tableName = $("#tags123").val();


    $.ajax({
        url: urlDestinationColumnSetting,
        data: { DestColdata: DestColdata, tableName: tableName },
        type: 'POST',
        cache: false,
        traditional: true,
        success: function (data) {
            window.location.href = data;
        }
    });

});

$('#btnUpdateDestTbl').click(function (e) {
    var destinationTableId = "";
    var i = 0;
    
    $('input[type=checkbox][class=Vendorselector]').each(function () {

        if (this.checked) {
            i++;
            destinationTableId = this.value;
        }
    });

    if (destinationTableId == 0) {
        alertmsg("red", "Please select destination table to edit.");
        e.preventDefault();
    }
    else {
        window.location = updateDestTableurl + "?DestTbl=" + destinationTableId + "";
    }

});

$("#destinationTable").select2();

$("#destinationTable").change(function() {
  
    if (this.value !== '') {
        var tabname = $("#destinationTable option:selected").text();
        
        $('#destcoldata').find('.tg-4eph').each(function () {
            $(this).remove();
        });

        var i = 1;

        $.ajax({
            url: urlDataSpecifications,
            data: { tableName: tabname },
            type: 'POST',
            cache: false,
            success: function (tableRows) {

                var tableData = $.parseJSON(tableRows);
                $.each(tableData, function (index, value) {
                    var name = value.COLUMN_NAME.replace(/\_/g, ' ');
                    var cap = ucFirstAllWords(name);
                    $('#destcoldata').append('<tr><td class="tg-4eph">' + value.ORDINAL_POSITION + '</td><td class="tg-4eph">' + value.COLUMN_NAME + '</td><td class="tg-4eph">' + '<input class="sourceLabel sg-tooltip" data-toggle="tooltip" data-placement="right" title="Click to edit" name="label[]" id="alias' + i + '" type="text" /><input type="hidden" id="order' + i + '" value="' + value.COLUMN_NAME + '" />' + '</td><td class="tg-4eph">' + value.DATA_TYPE + '</td><td class="tg-4eph">' + value.CHARACTER_MAXIMUM_LENGTH + '</td><td " id="tableName' + i + '"style="display:none;">' + value.TABLE_NAME + '</td></tr>');
                    document.getElementById('alias' + i).value = cap;
                    i++;
                });


                $("#tabletoshow").hide();
                $("#tableshowdata").show();
                $("#getDestCols").show();
                $("#cancleDestTable").show();
                rebuilttooltip();
            }
        });
    }

});

$(function () {

    $("#tags123").autocomplete({
        source: urlAutoCompleteDestinationSetting,

        select: function (event, ui) {

            var tabname = ui.item.value;

            $('#destcoldata').find('.tg-4eph').each(function () {
                $(this).remove();
            });

            var i = 1;

            $.ajax({
                url: urlDataSpecifications,
                data: { tableName: ui.item.value },
                type: 'POST',
                cache: false,
                success: function (tableRows) {

                    var tableData = $.parseJSON(tableRows);
                    $.each(tableData, function (index, value) {
                        var name = value.COLUMN_NAME.replace(/\_/g, ' ');
                        var cap = ucFirstAllWords(name);
                        $('#destcoldata').append('<tr><td class="tg-4eph">' + value.ORDINAL_POSITION + '</td><td class="tg-4eph">' + value.COLUMN_NAME + '</td><td class="tg-4eph">' + '<input class="sourceLabel sg-tooltip" data-toggle="tooltip" data-placement="right" title="Click to edit" name="label[]" id="alias' + i + '" type="text" /><input type="hidden" id="order' + i + '" value="' + value.COLUMN_NAME + '" />' + '</td><td class="tg-4eph">' + value.DATA_TYPE + '</td><td class="tg-4eph">' + value.CHARACTER_MAXIMUM_LENGTH + '</td><td " id="tableName' + i + '"style="display:none;">' + value.TABLE_NAME + '</td></tr>');
                        document.getElementById('alias' + i).value = cap;
                        i++;
                    });
                    

                    $("#tabletoshow").hide();
                    $("#tableshowdata").show();
                    $("#getDestCols").show();
                    $("#cancleDestTable").show();
                    rebuilttooltip();
                }
            });

        }
    });
});

$('#destinationTableShow').dataTable({
    "deferRender": true,
    "bServerSide": true,
    "aLengthMenu": dataTableMenuLength,
    "iDisplayLength": dataTableDisplayLength,
    "destroy": true,
    "scrollX": true,
    "sAjaxSource": urlGetDestinationTable,
    "bProcessing": true,
    "aoColumns": [
                    {

                        "bSearchable": false,
                        "bSortable": false,
                        "mRender": function (data, type, full) {
                            return '<input type="checkbox" class="Vendorselector" value="' + data + '">'
                        }
                    },
                    {

                        "bSortable": false,
                        "sName": "Name"
                    },
                    {
                                
                        "bSortable": false,
                        "sName": "CreatedDate"
                    }
    ]
});