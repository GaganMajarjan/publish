﻿
jsonErrorClassification = ko.utils.parseJson(jsonErrorClassification);

udh.settings.hideDismissButton = false; //set this setting to true to hide dismiss button

var ErrorReportVm = function () {
    
    var self = this;

    self.errorClassificationList = ko.mapping.fromJS(jsonErrorClassification);

    self.errorReportList = ko.mapping.fromJS([]); // ko.mapping.fromJS(jsonErrorDetail);

    self.selectedData = ko.observable();

    self.errorClick = function (val) {

        var errorName = val.ErrorName();
        //var id = val.id();
        self.selectedData(errorName);

        $.ajax({
            type: 'POST',
            url: urlErrorReportDetails,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'vendorId': vendorId, 'errorName': errorName }, //string vendorName, string errorName
            dataType: 'json',
            beforeSend: function () {

                SGloading('Loading');
            },

            success: function (response) {
                //console.log("success", response);
                fnReloadErrorDetailsGrid(response, errorName);
                //ko.mapping.fromJS(response, self.errorReportList);

            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();

                if(udh.settings.hideDismissButton)
                    $(".dismiss-span").hide();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });
    };

    self.clickedDismissAllError = ko.observable();
    self.clickedDismissError = ko.observable();

    self.errorDismissClick = function (val) {
        
        var errorCount = val.COUNT();
        var errorName = val.ErrorName();
        //store clicked dismiss record, to use if user clicks 'yes'
        self.clickedDismissAllError(val);
        $("#dismissImportExceptionMessageLabel")
                                  .text("Are you sure you want to dismiss all {0} item(s) for '{1}'?".format(errorCount, errorName));
        $("#DismissImportExceptionModal").modal("show");
    }

    self.processClick = function (data) {
        if (data.ProcessingErrorFile() == null) {
            alert("No data recorded for download.");
            return;
        }

        if (data == "All") {
            //alert('all');
            return;
        }
        var queryTable = data.ProcessingErrorFile();
        var errorType = data.ErrorCode();// 61003;
        //     //console.log(errorType);

        if (errorType == '') {
            window.location = urlDownload + '?fullPath=' + queryTable;
        }

        //return;
        $.ajax({
            type: 'POST',
            url: urlDownloadIndex,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'queryTable': queryTable, 'errorType': errorType },
            //dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {

                window.location = urlDownload + '?fullPath=' + response;
                //$.fileDownload($(this).prop('href'), {
                //    successCallback: function (url) {

                //        //$preparingFileModal.dialog('close');
                //    },
                //    failCallback: function (responseHtml, url) {

                //        //$preparingFileModal.dialog('close');
                //    }
                //});
                //window.location = filePath + '?file=' + queryTable;
                //window.location = 'http://www.google.com';
                //try {
                //    response = JSON.parse(response);
                //    alert(response);
                //}
                //catch (e) {
                //    alert('Error' + e);
                //}
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });

    };
}

var viewModel = new ErrorReportVm();
ko.applyBindings(viewModel);

var dismissAllErrorLines = function (vendorId, errorName) {
    $.ajax({
        type: 'POST',
        url: urlDismissAllErrorLines,
        cache: false,
        data: { 'vendorId': vendorId, 'errorName': errorName },
        dataType: 'json',
        beforeSend: function () {
            SGloading('Loading');
        },
        success: function (response) {

            console.log(response);

            if (response && response.Status) {
                udh.notify.success(response.Message || "Operation completed successfully");
                window.location.reload();
            }
            else if (response && response.Status == false) {
                udh.notify.error(response.Message || "Operation failed");
            } else {
                udh.notify.error(errorOccurred);
            }
        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            alert(status + ' ' + error);
        }
    });
}

var dismissSelectedErrorLine = function (stagingId, errorName, vendorId) {

    $.ajax({
        type: 'POST',
        url: urlDismissSelectedErrorLine,
        cache: false,
        data: { 'stagingId': stagingId, 'errorName': errorName, 'vendorId': vendorId },
        dataType: 'json',
        beforeSend: function () {
            SGloading('Loading');
        },
        success: function (response) {

            if (response && response.Status) {
                
                localStorage.setItem("errorName", errorName);
                udh.notify.success(response.Message || "Operation completed successfully");
                window.location.reload();
              
                
            }
            else if (response && response.Status == false) {
                udh.notify.error(response.Message || "Operation failed");
            } else {
                udh.notify.error(errorOccurred);
            }
        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            
            //reset variables here
            alert(status + ' ' + error);
        }
    });
}

function CallonLoadFunction()
{
    
        var previousClass = "";
        var newClass = "";
        $("li.errorCatagory")
            .each(function () {
                // for coloring and adding header
                //var errorCount = $(this).find("span.ErrorCategoryValue").text();
                previousClass = newClass;
                var errorClass = $(this).find("input.ErrorCategoryValue").text();
                //console.log(errorClass);

                if (!errorClass.trim()) {
                    //console.log("Enter");
                    errorClass = "Others";
                }
                var errorClassWithNoSpaces = errorClass.replace(/\s+/g, '');
                newClass = errorClassWithNoSpaces;

                $(this).addClass(errorClassWithNoSpaces);

                if (previousClass !== newClass && newClass !== 'All' && newClass !== "") {
                    headerClassName = newClass + "header";
                    $(this).before("<li class=" + headerClassName + ">" + errorClass + "</li>");

                }

                // for adding description as tool tip
                var errorDescription = $(this).find("input.ErrorCategoryDescription").text();
                $($(this).find('span.ErrorsDescription')[0]).attr('title', errorDescription);

            });
    
        //$("li.VendorPricingExceptions").prepend("<li>VendorPricingExceptions</li>");
        //$("li.GeneralExceptions").prepend("<li>GeneralExceptions</li>");

   
    $("body")
        .on("click",
            ".dismiss-span",
            function (row) {
                var selectedRowStagingId = $(this)[0].dataset.stagingid;
                $("#dismissImportExceptionMessageLabel")
                    .text("Are you sure you want to dismiss all the selected item(s)?");

                row.selectedRowStagingId = selectedRowStagingId;
                viewModel.clickedDismissAllError(null);
                viewModel.clickedDismissError(row);
                $("#DismissImportExceptionModal").modal("show");

            });

    $("#btnYesDismissImportExceptionModal")
        .click(function () {
            if (viewModel.clickedDismissAllError()) {
                //dismiss of left-pane
                $("#DismissImportExceptionModal").modal("hide");

                var item = viewModel.clickedDismissAllError();

                var errorName = item.ErrorName();
                dismissAllErrorLines(vendorId, errorName);

            } else {
                //dimiss of right-grid
                $("#DismissImportExceptionModal").modal("hide");

                var thisError = viewModel.clickedDismissError();

                var selectedRow = $($(thisError)[0].target).parent().parent();
                var errorName = selectedRow.find('td')[0].innerHTML;
                var stagingId = viewModel.clickedDismissError().selectedRowStagingId;

                //dismissSelectedErrorLine(queryTable, errorType, vendorId);
                dismissSelectedErrorLine(stagingId, errorName, vendorId);
            }
        });

    if (udh.settings.hideDismissButton)
        $(".dismiss-left-span").hide();
    
    var errorName = localStorage.getItem('errorName');

    if( errorName !=null)
    {
                $.ajax({
                    type: 'POST',
                    url: urlErrorReportDetails,
                    cache: false,
                    //contentType: 'application/json; charset=utf-8',
                    data: { 'vendorId': vendorId, 'errorName': errorName }, //string vendorName, string errorName
                    dataType: 'json',
                    beforeSend: function () {

                        SGloading('Loading');
                    },

                    success: function (response) {
                        fnReloadErrorDetailsGrid(response, errorName)
                    },
                    complete: function () {
                        //$("#btnsave").attr('disabled', false);
                        SGloadingRemove();

                        if (udh.settings.hideDismissButton)
                            $(".dismiss-span").hide();
                    },
                    error: function (xhr, status, error) {
                        //reset variables here
                        alert(status + ' ' + error);
                    }
                });
                
               
     }
       
 }
   

$(document)
    .ready(function () {
        CallonLoadFunction();
       
    });


function fnReloadErrorDetailsGrid(response, errorName)
{

    try {
        if (response.linkUrl) {
            //alert(response.link);
            window.location = response.linkUrl;
            return;
        }

        //Show breadcrumb information
        //$('#errorBreadcumb').html(errorCategory + " > " + vendorName + " > " + errorName);
        //$('#level2').removeClass('active');
        $('#level3').addClass('active').html(errorName);

        $('#testGridDiv').html('');

        try {
            if (response.aaData[0][0] == "Error") {
                alertmsg("red", "Unable to load fuzzy match suggestion");
                return;
            }
        } catch (e) {
            // do nothing, continue ahead
        }

        $('#testGridDiv').append('<table id="testGrid"><thead><tr></tr></thead></table>');
        ////clear heading
        //$('#testGrid thead tr').html('');
        //add heading to table
        $.each(response.sColumns, function (key, value) {
            $('#testGrid thead tr').append('<th>' + value + '</th>');
        });
        //add data to table via 

        var table = $('#testGrid').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "destroy": true,
            "scrollX": false,
            "bSort": false,
            "bFilter": false,
            "aLengthMenu": dataTableMenuLength,
            "iDisplayLength": dataTableDisplayLength,
            "sAjaxSource": urlErrorReportDetails + '?vendorId=' + vendorId + '&errorName=' + errorName,
            "sServerMethod": "POST",
            "fnPreDrawCallback": function () {
                // gather info to compose a message
                var ob = this;
                SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                //SGtableloading('fa-plane', '', this);
            },
            "fnDrawCallback": function () {
                // in case your overlay needs to be put away automatically you can put it here
                var divid = this.closest('.dataTables_wrapper').attr('id');
                SGtableloadingRemove(divid);

                //dialog box preparation for right-box
                // self.clickedDismissAllError(val);

                if (udh.settings.hideDismissButton)
                    $(".dismiss-span").hide();
            }

        });
      
        TblpgntBtm();

        if (udh.settings.hideDismissButton)
            $(".dismiss-span").hide();

    } catch (e) {
        alert(e);
    }

}
