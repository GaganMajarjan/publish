﻿$(document).ready(function () {
    var messageBoard;
    var flag = 1;
    var fileTrackFlag = 0;
    var flagProductCount = 1;
    var testing = $('.testing').height();
    $("#filterDetail").append("<option value=" + 0 + ">--Choose</option>");
    $("#filterDetail").prop("disabled", true).hide();

    //$("#importErrorGrid").DataTable({
    //    paging: false,
    //    searching: false,
    //    sort: false
    //});

    var importStat = function() {
        $('#importErrorGrid').dataTable({
            "destroy": true,
            "deferRender": true,
            "bServerSide": true,
            "bSort": false,
            //"aLengthMenu": dataTableMenuLength,
            "iDisplayLength": 10,
            "sAjaxSource": urlGetImportStatisticsAggregates,
            "bProcessing": true,
            "oLanguage": {
                "sProcessing": " "
            },
            "bPaginate": true,
            "bFilter": false,
            "bLengthChange": false,
            "bInfo": false,
            "scrollX": true,
            dom: "rtiS",
            scrollY: 190,
            scroller: {
                loadingIndicator: true
            },
            "drawCallback": function(settings) {
                //alert('gotcha');
                $('#importErrorGrid-refresh').removeClass("fa-spin");
            },
            "fnPreDrawCallback": function () {
                // gather info to compose a message
                var ob = this;
                SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                //SGtableloading('fa-plane', '', this);


            },
            "fnDrawCallback": function () {
                // in case your overlay needs to be put away automatically you can put it here
                var divid = this.closest('.dataTables_wrapper').attr('id');
                SGtableloadingRemove(divid);
                rebuilttooltip();
            }
        });
    }

    importStat();

    $('#barChart-refresh').addClass("fa-spin");
    //$('#importErrorGrid-refresh').addClass("fa-spin");
    //$('#messageBoard-refresh').addClass("fa-spin");

    $("#MessageBackBtn").click(function () {
        $("#filterDetail").empty();
        $("#filterDetail").append("<option value=" + 0 + ">--Choose</option>");
        document.getElementById('messageboardFilter').value = "0";
        $("#filterDetail").prop("disabled", true).hide();
        document.getElementById('logUserUploadId').value = "";
        $("#messageboardFilter option[value='-1']").remove();
        fileTrackFlag = 0;
        messagebusFxnVar();
    });

    function initializeMessageBoard() {
        //messageBoard-refresh
        //$('#messageBoard-refresh').addClass("fa-spin");
        //messageBoard = $('#MessageBoardDataTable').dataTable({
        //    "destroy": true,
        //    "deferRender": true,
        //    "bServerSide": true,
        //    "bSort": false,
        //    //"aLengthMenu": dataTableMenuLength,
        //    "iDisplayLength": dataTableDisplayLength,
        //    "sAjaxSource": urlMessageBoard,
        //    "bProcessing": true,
        //    "bPaginate": true,
        //    "bFilter": false,
        //    "bLengthChange": false,
        //    "bInfo": false,
        //    "scrollX": true,
        //    dom: "rtiS",
        //    scrollY: 200,
        //    scroller: {
        //        loadingIndicator: true
        //    },
        //    "drawCallback": function (settings) {
        //        //alert('gotcha');
        //        //$('#messageBoard-refresh').removeClass("fa-spin");
        //    }
        //});
    }

    $('#messageBoard-refresh').click(function() {
        initializeMessageBoard();
    });

    //function getImportStatisticsAggregates() {
    //    $.ajax({
    //        url: urlGetImportStatisticsAggregates,
    //        contentType: 'application/html ; charset:utf-8',
    //        type: 'POST',
    //        dataType: 'Json',
    //        beforeSend: function () {
    //            $('#importErrorGrid-refresh').addClass("fa-spin");
    //        }
    //    }).success(function (result) {
    //        if (result != null && result !== '' && result !== '[]') {
    //            try {

    //                if (result.Status === true) {

    //                    var res = JSON.parse(result.Data);

    //                    $('#importErrorGrid tbody').html('');

    //                    $.grep(res, function(data, index) {

    //                        var vendor = data.Vendor;
    //                        var vendorId = data.VendorId;
    //                        var fileCount = data.FileCount;
    //                        var recordCount = data.Count;

    //                        var row = '<tr>' +
    //                            '<td><a href="' + urlErrorReport + '/' + vendorId + '">' + vendor + '</a></td>' +
    //                            '<td>' + fileCount + '</td>' +
    //                            '<td>' + recordCount + '</td>' +
    //                            '</tr>';
    //                        $('#importErrorGrid tbody').append(row);

    //                    });

    //                } else {
    //                    alertmsg('red', result.Message);
    //                }

    //            } catch (e) {
    //            }
    //        }

    //        $('#importErrorGrid-refresh').removeClass("fa-spin");

    //    }).error(function () {
    //        $('#importErrorGrid-refresh').removeClass("fa-spin");
    //    });

    //};

    //getImportStatisticsAggregates();

    $('#importErrorGrid-refresh').click(function() {
        //if spinning, do perform ajax request. another ajax request is already in progress
        var currentClass = $('#importErrorGrid-refresh').attr('class');
        if (currentClass.indexOf('fa-spin') > -1 || currentClass.indexOf('fa-pulse') > -1) {
            return;
        }

        $('#importErrorGrid-refresh').addClass("fa-spin");

        try {
            importStat();
        } catch (e) {
        }

        $('#importErrorGrid-refresh').removeClass("fa-spin");

    });

    var barchartFxnVar = function getProductCountBarChart() {
        $.ajax({
            url: urlProductCount,
            contentType: 'application/html ; charset:utf-8',
            type: 'GET',
            timeout: 10000,
            dataType: 'Json',
            beforeSend: function () {
                $('#barChart-refresh').addClass("fa-spin");
            }
        }).success(function (result) {

           

            var columns = "[";
            var jsonData = "[{";
            $.each(result, function (key, value) {
                columns += "\"" + value.VendorName + "\"" + ",";
                jsonData += "\"" + value.VendorName + "\"" + ":" + "\"" + value.VendorProductCount + "\"" + ",";
            });
            columns = columns.slice(0, -1);
            jsonData = jsonData.slice(0, -1);
            columns += "]";
            jsonData += "}]";

            try {

                var statJson = $.parseJSON(jsonData);
                var statCols = $.parseJSON(columns);
                //console.log(vendorStat);
                var chart = c3.generate({
                    bindto: "#barChart",
                    data: {
                        type: 'bar',
                        json: statJson,
                        keys: {
                            value: statCols
                        }
                    },

                    bar: {
                        width: {
                            ratio: 0.66 // this makes bar width 66% of length between ticks
                        }
                    },
                    size: {
                        height: 200
                    },
                    axis: {
                        y: {
                            tick: {
                                format: d3.format("s")
                            }
                        }
                    },
                    tooltip: {
                        format: {
                            title: function(d) { return 'Vendor'; },
                            value: d3.format(',')
                            //            value: d3.format(',') // apply this format to both y and y2
                        }
                    }
                });
            } catch (ex) {

            }

            $('#barChart-refresh').removeClass("fa-spin");


        }).error(function () {
            $('#barChart-refresh').removeClass("fa-spin");
        });

    }
    var messagebusFxnVar = function getAllMessages() {
        var masterFilter = $("#messageboardFilter").val();
        var slaveFilter = $("#filterDetail").val();
        var fileTrack = $("#logUserUploadId").val();
        var tbl = $('#messagesTable');
        $.ajax({
            url: urlNotification,
            data: { masterFilter: masterFilter, slaveFilter: slaveFilter, fileTrack: fileTrack },
            contentType: 'application/html ; charset:utf-8',
            type: 'GET',
            beforeSend: function () {
                //SGloading('Loading');
            },
            complete: function () {
                //SGloadingRemove();
            },
            dataType: 'html'
        }).success(function (result) {
            tbl.empty().append(result);
           
        }).error(function () {

        });
    }

    var setMessageFlag = function messageFlagFxn() {
        flag = 1;
        messagebusFxnVar();
    }
    //var setProductCount = function productCountFlagFxn() {
    //    flagProductCount = 1;
    //    barchartFxnVar();
    //}
   
    barchartFxnVar();
   
       
    $(function () {
         //Declare a proxy to reference the hub.
        var notifications = $.connection.messagesHub;
        //var productCountNotification = $.connection.productCountHub;


        //productCountNotification.client.updateProductCount = function () {
        //    if (flagProductCount === 1) {
        //        flagProductCount = 0;
        //        setTimeout(setProductCount, 1000);
        //    }


        //};
        // Create a function that the hub can call to broadcast messages.
        notifications.client.updateMessages = function () {
            if (flag === 1) {
                flag = 0;
                setTimeout(setMessageFlag, 1000);
            }
        };

         //Start the connection.
        $.connection.hub.start().done(function () {

            messagebusFxnVar();
            //barchartFxnVar();

        }).fail(function (e) {
            alert(e);
        });
    });

    $('#barChart-refresh').click(function () {

        //if spinning, do perform ajax request. another ajax request is already in progress
        var currentClass = $('#barChart-refresh').attr('class');
        if (currentClass.indexOf('fa-spin') > -1 || currentClass.indexOf('fa-pulse') > -1)
            return;

        barchartFxnVar();
    });

    //setheight();


    //$('#mostSearchedGrid').dataTable({
    //    "deferRender": true,
    //    "bServerSide": true,
    //    "bSort": false,
    //    "aLengthMenu": dataTableMenuLength,
    //    "iDisplayLength": dataTableDisplayLength,
    //    "sAjaxSource": urlMostSearched,
    //    "bProcessing": true,
    //    "bPaginate": false,
    //    "bFilter": false,
    //    "bInfo": false
    //});


    $("#messageboardFilter").change(function () {
        $("#filterDetail").empty();
        $("#filterDetail").prop("disabled", true).hide();
        if ($("#messageboardFilter").val() !== "") {
            $.ajax({
                url: urlgetDetailedFilter,
                data: { filterType: $("#messageboardFilter").val() },
                type: 'POST',
                cache: false,
                success: function (data) {
                    $("#filterDetail").empty();
                    $("#filterDetail").append("<option value=" + 0 + ">--Choose</option>");
                    console.log(data);
                    if (data === "All") {
                        $("#filterDetail").prop("disabled", true).hide();
                        document.getElementById('logUserUploadId').value = "";
                        $("#messageboardFilter option[value='-1']").remove();
                        fileTrackFlag = 0;
                        messagebusFxnVar();

                    }
                    else if ($("#messageboardFilter").val() === "1") {
                        //  console.log(data);
                        $.each(data, function (index, value) {
                            //console.log(value.Value);
                            if (value.Text === "Information") {
                                $("#filterDetail").append("<option class='alert-info' value=" + value.Value + ">" + value.Text + "</option>");
                            } else if (value.Text === "Warning") {
                                $("#filterDetail").append("<option class='alert-warning' value=" + value.Value + ">" + value.Text + "</option>");
                            } else if (value.Text === "Success") {
                                $("#filterDetail").append("<option class='alert-success' value=" + value.Value + ">" + value.Text + "</option>");
                            } else if (value.Text === "Failure") {
                                $("#filterDetail").append("<option class='alert-danger' value=" + value.Value + ">" + value.Text + "</option>");
                            } else {
                                $("#filterDetail").append("<option class='' value=" + value.Value + ">" + value.Text + "</option>");
                            }

                        });
                        document.getElementById('logUserUploadId').value = "";
                        $("#messageboardFilter option[value='-1']").remove();
                        fileTrackFlag = 0;
                        $("#filterDetail").prop("disabled", false).show();

                    }
                    else {

                        $.each(data, function (index, value) {
                            $("#filterDetail").append("<option value=" + value.Value + ">" + value.Text + "</option>");
                        });
                        document.getElementById('logUserUploadId').value = "";
                        $("#messageboardFilter option[value='-1']").remove();
                        fileTrackFlag = 0;
                        $("#filterDetail").prop("disabled", false).show();
                    }
                }
            });

        }
        else {
            $("#filterDetail").empty();
            $("#filterDetail").prop("disabled", true).hide();
        }
    });

    $("#filterDetail").change(function () {
        messagebusFxnVar();
    });
    //$( ".alek" ).on( "click", function() {
    $("#messagesTable").on("click", ".importTrail", function () {

        var id = $(this).attr('id');
        if (fileTrackFlag === 0)
            $("#messageboardFilter").append("<option value=" + -1 + ">File Import Trail</option>");
        fileTrackFlag = 1;
        $("#messageboardFilter").val(-1);
        $("#filterDetail").empty();
        $("#filterDetail").append("<option value=" + 0 + ">--Choose</option>");
        $("#filterDetail").prop("disabled", true).hide();
        document.getElementById('logUserUploadId').value = id;
        messagebusFxnVar();
    });

});

//todo: check
function getId(divId) {
    var id = "";
    var mulitiId = "";
    var selectedrow = eval("grid" + divId).getSelectedRows();
    for (var i = 0; i <= selectedrow.length; i++) {
        var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
        if (d != null && d != "undefined") {
            multiId += d.id + "_";
            id = d.id;
        }
    }
    if (mulitiId == "") {
        return { error: true, message: "NOTSELECTED" }
    }
    else if (mulitiId.match(/_/g).length > 1) {
        return { error: true, message: "MULTISELECTED" }
    }
    return id;
}

//todo: check
function getValue(divId) {
    var val = null;
    var mulitiVal = "";
    var selectedrow = eval("grid" + divId).getSelectedRows();
    for (var i = 0; i <= selectedrow.length; i++) {
        var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
        if (d != null && d != "undefined") {
            multiVal += d.id + "_";
            val = d;
        }
    }
    if (multiVal == "") {
        return { error: true, message: "NOTSELECTED" }
    }
    else if (multiVal.match(/_/g).length > 1) {
        return { error: true, message: "MULTISELECTED" }
    }
    return val;
}

$(function () {
    //$("#importErrorGrid").DataTable({
    //    paging: false,
    //    searching: false,
    //    bInfo: false,
    //    "order": [[2, "desc"]]
    //});

    //$("#importErrorGrid").DataTable();

    var btnClick = $('.filter-icon');
    if (btnClick) {
        btnClick.click(function () {
            $('i', btnClick).toggleClass('fa-rotate-180');
            //$(".sg-more-option").slideToggle("slow");
            $('div.panel-filter').toggle();
        });
    }

    //$('body').click(function (e) {
    //    if (!($(e.target).hasClass('fa-toggle-down') || $(e.target).closest('.filter-input').length)) {
    //        //$('.sg-add-destination').removeClass('selected');
    //        //console.log($(e.target));
    //        $('div.panel-filter').hide();
    //    }
    //});

  
});


