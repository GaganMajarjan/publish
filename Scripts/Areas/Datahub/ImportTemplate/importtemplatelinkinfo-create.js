﻿$(document).ready(function () {
    $("#importTemplateLinkColumn").prop("disabled", true);
    //$("#slaveTableId").prop("disabled", true);
    
    //$("#masterTableId").change(function () {
    //    if ($("#masterTableId").val() !== "") {
    //        $("#slaveTableId").prop("disabled", false);
    //    } else {
    //        $("#slaveTableId").prop("disabled", true);
    //    }
    //});

    $("#saveImportTemplate").click(function() {
        $.ajax({
            url: urlAddImportTemplate,
            data: {
                masterTableId: $("#masterTableId").val(),
                slaveTableId: $("#slaveTableId").val(),
                linkedBySlaveColumn: $("#importTemplateLinkColumn").val()
            },
            type: 'POST',
            cache: false,
            success: function (data) {
                if (data == 'success') {
                alertmsg('green', 'Successfully created Import Template');
                window.location = urlImportTemplate;
                }
            }
        });
    });

    $("#slaveTableId").change(function () {
        if ($("#slaveTableId").val() !== "") {
            $.ajax({
                url: urlGetDestinationColumns,
                data: { destinationTableId: $("#slaveTableId").val() },
                type: 'POST',
                cache: false,
                success: function (data) {
                    $.each(data, function (index, value) {
                        $("#importTemplateLinkColumn").append("<option value="+value.Value+">" + value.Text + "</option>");

                    });
                    $("#importTemplateLinkColumn").prop("disabled", false);
                }
            });

        }
        else {
            $("#importTemplateLinkColumn").empty();
            $("#importTemplateLinkColumn").prop("disabled", true);
        }
    });



});