﻿$(document).ready(function() {
    $('#btnDeleteVendor').hide();
    $('#btnDeleteITTableDialogue').hide();
  var importTemplateDataTable =   $('#ImportTemplateDataTable').dataTable({
        "deferRender": true,
        "bServerSide": true,
        "bSort": false,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlImportTemplateDataTable,
        "bProcessing": true,
        "scrollX": true

    });
    
  var importTemplateLinkInfoDataTable = $('#ImportTemplateLinkInfoDataTable').dataTable({
        "deferRender": true,
        "bServerSide": true,
        "bSort": false,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlImportTemplateLinkInfoDataTable,
        "bProcessing": true,
        "scrollX": true

    });
  

  
   
  $('#btnDisplayColumn').click(function (e) {
      var i = 0;
      var importTemplateTableId;
      $('input[type=checkbox][class=ImportTemplateCheckBoxList]').each(function () {
          if (this.checked) {
              i++;
              importTemplateTableId = this.value;
          }
      });
      if (i === 0) { alertmsg('red', 'Please select import template table to select columns'); e.preventDefault(); }
      else {
          window.location.href = urlShowDestinationColumns + "?importTemplateTableId=" + importTemplateTableId;
      }
  });

  $('#btnDeleteImportTemplateTable').click(function (e) {
      var i = 0;
      $('input[type=checkbox][class=ImportTemplateCheckBoxList]').each(function () {
          if (this.checked) {
              i++;
          }
      });
      if (i === 0) { alertmsg('red', 'Please select import template table to delete'); e.preventDefault(); }
      else {
          $('#btnDeleteITTableDialogue').click();
      }
  });

  $('#btnDeleteImpTemplatefromModel').click(function (e) {

      var eachIds = [];
      $('input[type=checkbox][class=ImportTemplateCheckBoxList]').each(function () {
          if (this.checked) {
              eachIds.push(this.value);
          }
      });

      jQuery.ajaxSettings.traditional = true;
      $.ajax({
          url: urlDeleteImportTemplateTable,
          data: {
              importTemplateTableIds: eachIds
          },
          type: 'POST',
          success: function (data) {
              alertmsg("green", data);
              $("#deleteimportTemplateNobtn").click();
              importTemplateDataTable.fnReloadAjax();
          }
      });
  });
 

    $('#btnDelete').click(function (e) {
        var i = 0;
        $('input[type=checkbox][class=ImportTemplateSelector]').each(function () {
            if (this.checked) {
                i++;
            }
        });
        if (i === 0) { alertmsg('red', 'Please select import template to delete'); e.preventDefault(); }
        else {
            $('#btnDeleteVendor').click();
        }
    });
  
    $('#btnDeletefromModel').click(function (e) {

        var eachIds = [];
        $('input[type=checkbox][class=ImportTemplateSelector]').each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });

        jQuery.ajaxSettings.traditional = true;
        $.ajax({
            url: urlDeleteImportTemplate,
            data: {
                importTemplateIds: eachIds
            },
            type: 'POST',
            success: function (data) {
                alertmsg("green", data);
                $("#deletevendorNobtn").click();
                importTemplateLinkInfoDataTable.fnReloadAjax();
            }
        });
    });


    
    $('#btnAddImportTemplate').click(function () {

        window.location.href = urlAddImportTemplate;
    });


    $('#btnAddImportTemplateTable').click(function () {

        window.location.href = urlAddImportTemplateTable;
    });


});