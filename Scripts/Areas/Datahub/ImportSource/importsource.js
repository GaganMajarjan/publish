﻿var stepped = 0, chunks = 0, rows = 0;
var start, end;
var parser;
var pauseChecked = false;
var printStepChecked = false;
var header = [];
var numberOfColumns = 0;


$(".sourceDropDatatype").each(function () {
    $(this).val($(this).attr("value"));
 
    // ...
});

$(function () {
    //$('#importsource1').css('width', '100%');
    //$('#importsource2').hide();

    //var importSourceIdInUrl = getQueryStringByName('importsourceId');
    //if (importSourceIdInUrl != null && importSourceIdInUrl !== "") {
    //    $('#importsource1').css('width', '50%');
    //    $('#importsource2').show();
    //}
    $(".importSourcepartialHolder").on("click", "#parse", function () {
        stepped = 0;
        chunks = 0;
        rows = 0;
        var txt = "";
        var files = $('#files')[0].files;
        var config = buildConfig();

        if (files.length > 0) {
        
            start = performance.now();
            header = [];
            $('#files').parse({
                config: config,
                before: function (file, inputElem) {
                  //  console.log("Parsing file:", file);
                },
                complete: function () {
                    //      console.log("Done with all files.");
                    var skipRow = document.getElementById("startrow").value;
                    $.ajax({
                        url: urlGetImportSourceColumn,
                        type: 'POST',
                        data: {
                            headers: header,
                            skipRow: skipRow
                        },

                        beforeSend: function () {
                            SGloading('Loading');
                        },
                        success: function (response) {

                       
                           
                            $("#ImportSourceColumnPlaceHolder").empty();

                            $.post(urlGetNumberOfColumn, function (returnedData) {
                                numberOfColumns = returnedData.NumberOfColumn;
                                document.getElementById("NoOfVariables").value = numberOfColumns;


                                var headerlistComma = "";

                                $.each(returnedData.HeaderList, function (index, value) {
                                    headerlistComma += value + ",";

                                });

                                $("#impSrcClmns").val(headerlistComma);



                            });

                            $('#ImportSourceColumnPlaceHolder').html(response);
                        },
                        complete: function () {
                            SGloadingRemove();
                            //$('#files').val("");

                          

                            TblpgntBtm();

                        },
                        error: function (xhr, status, error) {
                            alertmsg("yellow","Unable to parse selected file.");
                        }
                    });

                }
            });
        }
        else {
            

     
            var noOfCols = document.getElementById("NoOfVariables").value;
            numberOfColumns = noOfCols;
            //alert(noOfCols);
            if (noOfCols === "0" || noOfCols === "") {

                document.getElementById("NoOfVariables").value = 1;
                numberOfColumns = 1;
            }
            var skipRow = document.getElementById("startrow").value; 
            //     console.log("inside to ");
                $.ajax({
                    url: urlGetImportSourceColumnFromNumber + "?numberOfColumn=" + numberOfColumns + "&skipRow=" + skipRow,
                    type: 'POST',
                    //data: {
                    //    numberOfColumn: noOfCols
                    //},

                    beforeSend: function () {
                        SGloading('Loading');
                    },
                    success: function (response) {
                        //    console.log(numberOfColumns);

                    //    document.getElementById("NoOfVariables").value = $("#NoOfVairables").val();
                        $("#ImportSourceColumnPlaceHolder").empty();
                        $('#ImportSourceColumnPlaceHolder').html(response);

                    },
                    complete: function () {

                        SGloadingRemove();
                    },
                    error: function (xhr, status, error) {
                        alert(status + ' ' + error);
                    }
                });

           

        }
    });

    $('#submit-unparse').click(function () {
        var input = $('#input').val();
        var delim = $('#delimiter').val();

        var results = Papa.unparse(input, {
            delimiter: delim
        });

    });

    $('#insert-tab').click(function () {
        $('#delimiter').val('\t');
    });

    //If source type has only one value, select it by default; then disable it
    if ($('#ImportSourceType')[0].length === 2) {
        $('#ImportSourceType')[0].selectedIndex = 1;
        //trigger change
        $('#ImportSourceType').change();
        $('#ImportSourceType').attr('disabled', 'disabled');
    }

});


function buildConfig() {
    return {
        delimiter: "",
        newline: "\n",
        header: true,
        dynamicTyping: "",
        preview: parseInt(0),
        step: undefined,
        encoding: "",
        worker: "",
        comments: "",
        complete: completeFn,
        error: errorFn,
        download: "",
        fastMode: "",
        skipEmptyLines: "",
        chunk: undefined,
        beforeFirstChunk: undefined
    };

    function getLineEnding() {

        if ($('#newline-n').is(':checked'))
            return "\n";
        else if ($('#newline-r').is(':checked'))
            return "\r";
        else if ($('#newline-rn').is(':checked'))
            return "\r\n";
        else
            return "";
    }
}

function stepFn(results, parserHandle) {
    stepped++;
    rows += results.data.length;

    parser = parserHandle;

    if (pauseChecked) {
        console.log(results, results.data[0]);
        parserHandle.pause();
        return;
    }

    if (printStepChecked)
        console.log(results, results.data[0]);
}

function chunkFn(results, streamer, file) {
    if (!results)
        return;
    chunks++;
    rows += results.data.length;

    parser = streamer;

    if (printStepChecked)
        console.log("Chunk data:", results.data.length, results);

    if (pauseChecked) {
        console.log("Pausing; " + results.data.length + " rows in chunk; file:", file);
        streamer.pause();
        return;
    }
}

function errorFn(error, file) {
    console.log("ERROR:", error, file);
}

function completeFn() {
    end = performance.now();
    if (!$('#stream').prop('checked')
			&& !$('#chunk').prop('checked')
			&& arguments[0]
			&& arguments[0].data)
        rows = arguments[0].data.length;
    var headerJson = arguments[0].meta.fields;
    //console.log(arguments[0]);

    numberOfColumns = 0;
    $.each(headerJson, function (key, value) {
        numberOfColumns += 1;
        header.push(value);

    });
    //   console.log("Finished input (async). Time:", end - start, arguments);
    //  console.log("Rows:", rows, "Stepped:", stepped, "Chunks:", chunks);
}

$(document).on('click', ".removeBtn", function () {

    var dataTypeId = $(this).attr('id');
    var numberedId = dataTypeId.substr(dataTypeId.indexOf('_') + 1, dataTypeId.length);
 
    $("#tr_" + numberedId).remove();

    if (document.getElementById('NoOfVariables')) {
   

    numberOfColumns = $("#NoOfVariables").val();
    numberOfColumns = parseInt(numberOfColumns) - 1;
    document.getElementById("NoOfVariables").value = numberOfColumns;
    }
});

$(document).on('click', "#addMoreRow", function (e) {
    $('#ImportSourceColumnPlaceHolder tr:last').attr('id');
    var labelId = null;
    if ($('#ImportSourceColumnPlaceHolder tr:last').attr('id') !== "impSrcColHeader") {
        labelId = $('#ImportSourceColumnPlaceHolder tr:last').attr('id').split('_')[1];
    }
  

    var numberedId = parseInt(1);
    if (labelId != null && labelId !== "")
        numberedId = parseInt(labelId) + 1;
 
    if (parseInt(isStore) === 0) {

        $('#ImportSourceColumnPlaceHolder tbody').append('<tr id="tr_' + numberedId + '">' +
            '<td style="display: none"><input name="Index" type="hidden" value=' + numberedId + ' /></td>' +
            '<td>' +
            '<span class="sg-tooltip" title="Click to edit" data-placement="right" data-toggle="tooltip">' +
            '<input id="' + numberedId + '_Name" class="form-control" type="text" value="" name="[' + numberedId + '].Name">' +
            '</span></td><td>' +
            '<span class="sg-tooltip" title="Click to edit" data-placement="right" data-toggle="tooltip">' +
            '<input id="' + numberedId + '_DisplayName" class="form-control" type="text" value="" name="[' + numberedId + '].DisplayName">' +
            '</span></td><td>' +
            '<a id="remove_' + numberedId + '" class="btn btn-danger removeBtn" title="remove the row" data-placement="top" data-toggle="tooltip">' +
            '<i class="fa fa-close"></i>' +
            '</a></td>' +
            '</tr>');
        $('#ImportSourceColumnPlaceHolder tr:last td:nth-child(2)').find('.form-control').focus();
    } else {
        $('#ImportSourceColumnPlaceHolder tbody').append('<tr id="tr_' + numberedId + '">' +
          '<td style="display: none"><input name="Index" type="hidden" value=' + numberedId + ' /></td>' +
          '<td>' +
          '<span class="sg-tooltip" title="Click to edit" data-placement="right" data-toggle="tooltip">' +
          '<input id="' + numberedId + '_Name" class="form-control" type="text" value="" name="[' + numberedId + '].Name">' +
          '</span></td><td>' +
          '<span class="sg-tooltip" title="Click to edit" data-placement="right" data-toggle="tooltip">' +
          '<input id="' + numberedId + '_DisplayName" class="form-control" type="text" value="" name="[' + numberedId + '].DisplayName">' +
          '</span></td><td>' +
           '<span class="sg-tooltip" title="VARCHAR" data-placement="right" data-toggle="tooltip">' +
            '<select id="' + numberedId + '" class="sourceDrop " value="VARCHAR" style="width:100%;" name="[' + numberedId + '].DataType">' +
            '</span></td><td>' +
            '<span class="sg-tooltip" title="Click to edit" data-placement="left" data-toggle="tooltip">' +
            '<input class=" form-control" type="text" value="100" name="[' + numberedId + '].CharacterMaximumLength" data-val-number="The field Character Maximum Length must be a number." data-val="true">' +
            '</span></td><td class="input-group-btn">' +
          '<a id="remove_' + numberedId + '" class="btn btn-danger removeBtn" title="remove the row" data-placement="top" data-toggle="tooltip">' +
          '<i class="fa fa-close"></i>' +
          '</a></td>' +
          '</tr>');
        $('#ImportSourceColumnPlaceHolder tr:last td:nth-child(2)').find('.form-control').focus();

    }
    $.each(dataTypeCollection, function (index, value) {
        $("#" + numberedId).append(new Option(value.Text, value.Value));
    });
    numberOfColumns = parseInt(numberOfColumns) + 1;
    document.getElementById("NoOfVariables").value = numberOfColumns;
    //$("#" + numberedId + "_dd").select2();
    e.preventDefault();
});

$('#btnCancel').click(function () {
    window.location = urlStaticDataIndex;
});

$(document).on('change', "#vendor", function () {

        var selectedText = $('#vendor option:selected').text();
   
    document.getElementById('Name').value = selectedText;
    $('#Name').attr('readonly', true);
        $('#Name').mask('w', { translation: { 'w': { pattern: /[a-zA-Z0-9_ ]/, recursive: true } } });
        $('#Name').mask("w", custom_options);

    });
$(document).on('change', "#ImportSourceType", function () {
 
    var selectedId = $(this).val();

    if (selectedId !== "") {

        $.ajax({
            url: urlSourceParamFrmType,
            data: {
                id: selectedId
            },

            beforeSend: function() {
                SGloading('Loading');
            },
            success: function(response) {
                $("#ImportSourceColumnPlaceHolder").empty();
                $('#partialPlaceHolder').html(response);
            },
            complete: function() {

                SGloadingRemove();
                var vendorId = getQueryStringByName('vendorId');

                if (vendorId != null && vendorId !== "") {
                    $('#vendor').val(vendorId);
                    var selectedText = $('#vendor option:selected').text();
                    document.getElementById('Name').value = selectedText;
                    $('#Name').mask('w', { translation: { 'w': { pattern: /[a-zA-Z0-9_ ]/, recursive: true } } });

                }

                rebuilttooltip();

            },
            error: function(xhr, status, error) {
                alert(status + ' ' + error);
            }
        });


    }

});

$(document).on('click', "#generateimpsrcColmTable", function (e) {

    var col = $("#impSrcClmns").val();
    var arr = col.split(',');

    var i = 0;

    $.each(arr, function (index, value) {
        if (i < numberOfColumns) {
            if (document.getElementById(i + '_Name'))
                document.getElementById(i + '_Name').value = value;
            if (document.getElementById(i + '_DisplayName'))
        document.getElementById(i + '_DisplayName').value = value;
        }
        i++;
      
           
    });

    e.preventDefault();
});


$(document).on('change', "#NoOfVariables", function () {
   
    $('#files').val("");
    $("#parse").click();
    TblpgntBtm();
    //$('#importsource1').css('width', '50%');
    //$('#importsource2').show();
});
$(document).on('change', "#files", function() {
    $("#parse").click();
    TblpgntBtm();
});

if (successData !== '') {
    alertmsg('green', successData);
    successData = '';
}
if (errorMsg !== '') {
    alertmsg('red', errorMsg);
    errorMsg = '';
}
$("#viewMapping").click(function () {
    if (destinationId > 0) {
        window.open((returnMappingUrl + "?sourceId=" + sourceId + "&destinationId=" + destinationId), '_blank');
    }
    else {
        alertmsg("red", "DataSource Is Not Mapped");
    }
});
rebuilttooltip();

//if(hasError==="True")
//{
//    $('#importsource1').css('width', '50%');
//    $('#importsource2').show();
//}
