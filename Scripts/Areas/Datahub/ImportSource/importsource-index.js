﻿$(document).ready(function () {

    if (importSourceColumnUpdated != null && importSourceColumnError != "")
    {
        alertmsg("green",  importSourceColumnUpdated);
    }
    if (importSourceColumnUpdated != null && importSourceColumnError != "")
    {
        alertmsg("red", importSourceColumnError);
    }

   
});

$('#butnDeleteImpSourceYes').click(function () {

    var ImpSourceIds = $("#ImpSrcIdsforDelete").val();

    $.ajax({
        url: urlDeleteImportSourceSetting,
        data: {
            ImportSourceIds: ImpSourceIds
        },
        type: 'POST',
        cache: false,
        success: function (data) {
            if (data == "ok") {

                importSourceDataTable.fnReloadAjax();
                alertmsg("green", "Data Source(s) deleted successfully");
                $("#deleteImpSourceNobtn").click();
                //  location.reload();
            }
            if (data == "columns") {
                $("#deleteImpSourceNobtn").click();
                alertmsg("yellow", "Please delete columns of data source");
            }
        }
    });
});

//$("#updatesourceName").hide();
//document.getElementById('updatesourceName').value = "For validate model state, update to same name is not given, since we have remote validation used so this is needed to maintain";
    
$('#vendor').change(function () {
    document.getElementById('vendortextBox').value = $('#vendor').val();
});

$('#selectAllCheckBoxImportSource').click(function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.Vendorselector').prop("checked", true);
    } else {
        $('.Vendorselector').prop("checked", false);
    }
});


var importSourceDataTable = $('#importSourceDataTable').dataTable({
    "destroy": true,
    "bJQueryUI": true,
    "bScrollCollapse": true,
    "bSort": false,
    "bServerSide": true,
    "aLengthMenu": dataTableMenuLength,
    "iDisplayLength": dataTableDisplayLength,
    "scrollX": true,
    "sAjaxSource": urlGetImportSourceSetting,
    "bProcessing": true,
    "deferRender": true,
    "fnPreDrawCallback": function () {
        // gather info to compose a message
        var ob = this;
        SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
        //SGtableloading('fa-plane', '', this);


    },
    "fnDrawCallback": function () {
        // in case your overlay needs to be put away automatically you can put it here
        var divid = this.closest('.dataTables_wrapper').attr('id');
        SGtableloadingRemove(divid);
    }
});


$('#btnScheduleToBackground').click(function (e) {
    var i = 0;
    var importSourceId = 0;
    $('input[type=checkbox][class=Vendorselector]').each(function () {

        if (this.checked) {
            i++;
            importSourceId = this.value;
        }
    });

    if (i == 0) { alertmsg('red', 'Please Select an Import Source to schedule task'); e.preventDefault(); }
    else if (i > 1) {
        alertmsg('red', 'Please select single Import Source to schedule task');
        e.preventDefault();
    } else {
        window.location = urlBackgroundScheduling + "?importSourceId=" + importSourceId;
    }
});


$('#btnUpdateImportSource').click(function (e) {
    var i = 0;
    var importSourceId = 0;
    $('input[type=checkbox][class=Vendorselector]').each(function () {

        if (this.checked) {
            i++;
            importSourceId = this.value;
        }
    });

    if (i == 0) { alertmsg('red', 'Please Select an Import Source to edit'); e.preventDefault(); }
    else if (i > 1) {
        alertmsg('red', 'Please select single Import Source to edit');
        e.preventDefault();
    } else {
        window.location = urlUpdateImportSource +"?importSourceId="+importSourceId;
    }
});

$('#butnDeleteImpSource').click(function(e) {
    var i = 0;
    var importSourceIds = "";
    $('input[type=checkbox][class=Vendorselector]').each(function() {
        if (this.checked) {
            i++;
            importSourceIds = importSourceIds + this.value +"_";
        }
    });
    if (i == 0) {
        alertmsg("red", "Please select an import source to delete.");
        e.preventDefault();
    } else {
        $("#ImpSrcIdsforDelete").val(importSourceIds);

        $("#DeleteImportSourceModel").modal('show');
    }
});


$('#btnColumnsImportSource').click(function (e) {
    var importSourceId = "";
    var divId = "ImportSourceGrid";
    var i = 0;
    var importSourceId = "";
    $('input[type=checkbox][class=Vendorselector]').each(function () {
        if (this.checked) {
            i++;
            importSourceId = this.value ;
        }
    });

    if (importSourceId === 0) {
                    alertmsg("red", "Please select import source to configure columns.");
                    e.preventDefault();
         }
    else {
          window.location = updateDestTableurl + "?importSource=" + importSourceId + "";
         }
    
});

$('#btnCreateImportSource').click(function () {
    window.location = urlCreateImportSource;
});