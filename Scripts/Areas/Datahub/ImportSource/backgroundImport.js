﻿function OnSuccess(data) {
    if (data.Success) {
        alertmsg("green", data.Message);
    } else {
        alertmsg("red", data.Message);
    }
}

$(document).ready(function () {

    $("#hourlyOption").hide();
    $("#minutelyOption").hide();
    $("#weeklyOption").hide();
    $.ajax({
        type: 'GET',
        url: urlgetscheduledData,
        data: { 'importSourceId': importSourceId },
        dataType: "json",
        success: function (result) {
            //console.log(result);
    
            if (result.Success) {

                document.getElementById('importSource').value = result.Data.ImportSource;
                document.getElementById('startTime').value = result.Data.StartRunTime;
                $('#frequencyType').val(result.Data.Frequency).change();
                $('#DailyFrequencyType').val(result.Data.DailyFrequencyType).change();
                document.getElementById('specificTime').value = result.Data.SpecificTime;
                document.getElementById('Hour').value = result.Data.Hour;
                document.getElementById('Minute').value = result.Data.Minute;
               $('#statusBackgroundImport').prop('checked', result.Data.Status);
                $("#weeklyfrequencyType").select2("val", result.Data.WeeklyFrequencyType);

            }
           

        },
        error: function (result) {

        }
    });


    function HideUnrelatedFields() {

        $("#atSpecificTimeOption").hide();
        $("#hourlyOption").hide();
        $("#minutelyOption").hide();

    }

    //$('#test').attr('readonly', true);

    

    $("#btnCancel").click(function () {
        window.location = urlImportSource;
    });

    $("#Hour").change(function () {
        var hourValue = $("#Hour").val();
        if (hourValue > 23) {
            document.getElementById('Hour').value = "23";
        }
    });

    $("#Minute").change(function () {
        var minuteValue = $("#Minute").val();
        if (minuteValue > 59) {
            document.getElementById('Minute').value = "59";
        }
    });

    $("#frequencyType").change(function () {
        var str = $('option:selected', this).text();

        if (str === "Weekly") {
            //$("#DailyFrequencyType").val("1");
            //HideUnrelatedFields();
            //$("#atSpecificTimeOption").show();
            $("#weeklyOption").show();

        } else if (str === "Daily") {
            //$("#DailyFrequencyType").val("1");
            //HideUnrelatedFields();
            //$("#atSpecificTimeOption").show();
            $("#weeklyOption").hide();
        } else if (str === "Monthly") {
            //$("#DailyFrequencyType").val("1");
            //HideUnrelatedFields();
            //$("#atSpecificTimeOption").show();
            $("#weeklyOption").hide();

        }
    });

    $("#DailyFrequencyType").change(function () {
        var str = $('option:selected', this).text();

        if (str === "Hourly") {
            HideUnrelatedFields();
            $("#hourlyOption").show();
        } else if (str === "SpecifiedTime") {
            HideUnrelatedFields();
            $("#atSpecificTimeOption").show();
        } else if (str === "Minutely") {
            HideUnrelatedFields();
            $("#minutelyOption").show();
        }
    });


    $("#startTime").datetimepicker({
        minDate: '0'
    });
    $("#specificTime").timepicker();

    $("#weeklyfrequencyType").select2({
        maximumSelectionSize: 7
    });

});