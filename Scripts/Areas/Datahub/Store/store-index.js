﻿$(document).ready(function () {


    if (importError != "") {
        alertmsg('red', importError);
    }


    $('#btnDeleteStore').hide();
    
    $('#btnImport').click(function (e) {
      
            var sThisVal = 0, i = 0;
            var impsrc = 0;
            $('input[type=checkbox][class=Storeselector]').each(function () {
                sThisVal = (this.checked ? i++ : 0);
                if (this.checked) {
                    impsrc = this.value;
                }
            });
            if (i == 0) { alertmsg('red', 'Please select store to import'); e.preventDefault(); }
            else if (i > 1) { alertmsg('red', 'Please select single store to import'); e.preventDefault(); }
            else {
              
                $.ajax({
                    url: urlStoreCheckForImport,
                    data: {
                        storeId: impsrc
                    },
                    type: 'GET',

                    success: function (data) {
                        

                        if (!data.Success) {
                            if (data.Data === "processing") {
                                alertmsg("green", data.Message);
                                e.preventDefault();
                            } else if (data.Data === "error") {
                                alertmsg("red", data.Message);
                                e.preventDefault();
                            }
                            else
                            {
                                window.location = urlMapping + "?FileId=" + data.Data;
                                e.preventDefault();
                            }
                        }
                        else {
                            window.location = urlImport + "?relationshipId=" + data.Data.RelationshipId + "&sourceTypeId=" + data.Data.SourceTypeId;

                            e.preventDefault();

                        }

                    }
                });
                e.preventDefault();

            }
        
    });

    $('#selectAllCheckBoxStore').click(function () {
        var thisCheck = $(this);
        if (thisCheck.is(':checked')) {
            $('.Storeselector').prop("checked", true);
        } else {
            $('.Storeselector').prop("checked", false);
        }
    });


    $('#btnMap').click(function (e) {
        var i = 0;
        var storeId = 0;
        $('input[type=checkbox][class=Storeselector]').each(function () {
            if (this.checked) {
                i++;
                storeId = this.value;
            }
        });

        if (i === 0) { alertmsg('red', 'Please select store to Map import source'); e.preventDefault(); }
        else if (i > 1) { alertmsg('red', 'Please select single store to Map import source'); e.preventDefault(); }
        else { window.location = urlMapImportSource + "?id=" + storeId; }
    });


    $('#btnUpdateStore').click(function (e) {
        var i = 0;
        var storeId = 0;
        $('input[type=checkbox][class=Storeselector]').each(function () {

            if (this.checked) {
                i++;
                storeId = this.value;
            }
        });

        if (i === 0) { alertmsg('red', 'Please select store to edit'); e.preventDefault(); }
        else if (i > 1) { alertmsg('red', 'Please select single store to edit'); e.preventDefault(); }
        else { window.location = urlEditStore + "?Id=" + storeId; }
    });

    $('#btnAddStore').click(function () {

        window.location.href = urlAddStore;
    });


    $('#btnDelete').click(function (e) {
        var i = 0;
        $('input[type=checkbox][class=Storeselector]').each(function () {
            if (this.checked) {
                i++;
            }
        });
        if (i === 0) { alertmsg('red', 'Please select store(s) to delete'); e.preventDefault(); }
        else {
            $('#btnDeleteStore').click();
        }
    });

    $('#btnDeletefromModel').click(function (e) {

        var eachIds = [];
        $('input[type=checkbox][class=Storeselector]').each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });

        jQuery.ajaxSettings.traditional = true;
        $.ajax({
            url: urlDeleteStore,
            data: {
                storeIds: eachIds
            },
            type: 'POST',
            success: function (data) {
                alertmsg("green", data);

                $("#deletestoreNobtn").click();

                $("#StoreDataTable").dataTable().fnReloadAjax();
            }
        });
    });

    $('#StoreDataTable').dataTable({
        "deferRender": true,
        "bServerSide": true,
        "bSort": false,
        "stateSave": true,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlSupplier,
        "bProcessing": true,
        "scrollX": true

    });
});