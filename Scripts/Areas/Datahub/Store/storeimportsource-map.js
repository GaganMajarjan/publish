﻿


$("#btnMapImportSource").click(function () {
    alert(storeId);
    alert($("#importSource").val());
    $.ajax({
        url: urlMapStoreImportSource,
        data: {
            storeId: storeId,
            importSourceId: $("#importSource").val()
        },
        type: 'POST',
        cache: false,
        success: function (data) {
            if (data.Success) {
                alertmsg('green', data.Message);
                window.location = urlStore;
            } else {
                alertmsg('red', data.Message);
            }
        }
    });
});