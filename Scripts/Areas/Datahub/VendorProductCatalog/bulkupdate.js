﻿var fileData;
var columns1 = [];
var container = document.getElementById('vendor-product-bulk-update');
var hot;
var matchedColumnIndex = [];

var vendorId = getQueryStringByName("vendorId");
if (!vendorId) {
    $("#Vendor").select2(remoteDataConfig(urlGetVendorListForSelect2, 3, 250, msgPleaseSelect));
}
var qohCustomFields = [];
var replacementColumnsXml = ["VendorPartNumber", "QOH", "ShelfLife"];
var columnsXml = ["Material", "Avail Qty"];

var bulkUpdateErrorManager = function(option) {
    option = option || {};
    var retainControls = option.retainControls || true;
    var retainHotData = option.retainHotData || false;

    if (retainControls) {
        $("#import-qoh-sheet").removeClass("hidden");
        $("#import-qoh-setting").removeClass("hidden");
    } else {
        $("#import-qoh-sheet").addClass("hidden");
        $("#import-qoh-setting").addClass("hidden");
        qohCustomFields = [];
    }

    if (!retainHotData && hot) {
        hot.updateSettings({ data: [] });
    }
};

Array.prototype.selectQohXml = function (columns, replacementColumns, parentNode, vendorId) {
    //console.log(columns, replacementColumns, parentNode, vendorId);
    var array = this;
    var xmlData = "";
    replacementColumns = replacementColumns || columns;
    $.grep(array, function (element, index) {
        var data = "";
        var vendorPartNumber = "";
        var shelfLife = "";

        var isValueNotNull = false;
        for (var i = 0; i < columns.length; i++) {
            //  var colName = replacementColumns[i];
            var colName = qohCustomFields[i].Name;
            //console.log(value);
            var value = element[columns[i]];

            value = Encoder.htmlEncode(value);
            //console.log(value);

            if (colName == "VendorPartNumberColumn") {
                vendorPartNumber = value;
            }
            else if (colName == "ShelfLifeColumn") {
                data += "<{0}><![CDATA[{1}]]></{0}>".format(colName, value);
            }
            else if (colName == "QOHColumn") {

                if (value) { //if QOH value is blank, don't construct XML for it
                    data += "<{0}><![CDATA[{1}]]></{0}>".format(colName, value);
                    isValueNotNull = true;
                } else {
                    isValueNotNull = false;
                }
            }
           
            
        }
        if (isValueNotNull) {
            xmlData += "<{0} VendorId='{2}' VendorPartNumber='{3}'>{1}</{0}>".format(parentNode, data, vendorId, vendorPartNumber);
        }
    });
    return xmlData;
}


$("#Vendor")
    .change(function(e) {
        vendorId = $("#Vendor").val();
        if (vendorId) {

            //adjustment for Styles Logistics
            if (vendorId == -1)
                vendorId = 0;

            $.ajax({
                type: "GET",
                url: urlGetBulkUpdateVendorSettings,
                cache: false,
                contentType: 'application/json; charset=utf-8',
                data: { 'vendorId': vendorId },
                dataType: "json",
                beforeSend: function() {
                    //$("#btnsave").attr('disabled', true);
                    $(".new-row").remove();
                    SGloading("Reading setting");
                },
                success: function (response) {
                    if (response) {
                        if (response.Status === true) {

                            var data = response.Data;
                            //console.log(data);
                            data = data.replace(/\\"/g, '"').replace(/"/g, '\\"');
                            data = decodeHtml(data);
                            data = fixNewLineOnly(data);

                            var qoh = false;
                            var vendorPartNumber = false;
                            if (data != undefined && data.length > 0) {
                                for (i = 0; i < data.length ; i++) {
                                    if (data[i].Name == "QOHColumn")
                                    { qoh = true; }
                                    if (data[i].Name == "VendorPartNumberColumn")
                                    { vendorPartNumber = true; }
                                }
                            }
                            if (qoh == false) {
                                alertmsg("red", "Column for QOH setting not found", -1);
                                return;
                            }
                            if (vendorPartNumber == false) {
                                alertmsg("red", "Vendor Part Number Column Name for QOH setting not found", -1);
                                return;
                            }

                            qohCustomFields = data;

                            $.each(qohCustomFields,
                                function (index, value) {
                                    var template = document.getElementById("table-qoh-settings-row-template").innerHTML;
                                    template = template.format(value.FieldAlias, value.FieldValue);
                                    $("#table-qoh-settings").append("<tr class='new-row'>{0}</tr>".format(template));
                                });

                            //for (var i = 0; i < data.length; i++) {
                            //    var currentData = data[i];
                            //    console.log(currentData);
                            //    console.log(currentData.Name);
                            //    console.log(currentData.FieldValue);
                            //}
                           
                            //$("#li-VendorPartNumberColumn").text(qohCustomFields.VendorPartNumberColumn);
                            //$("#li-QOHColumn").text(qohCustomFields.QOHColumn);

                            bulkUpdateErrorManager({ retainControls: response.Status, retainHotData: true });

                            alertmsg("green", response.Message, -1);
                        } else {
                            bulkUpdateErrorManager({ retainControls: response.Status });
                            alertmsg("red", response.Message, -1);
                        }
                    } else {
                        bulkUpdateErrorManager({ retainControls: response.Status });
                        alertmsg("red", msgErrorOccured, -1);
                    }
                },
                complete: function() {
                    //$("#btnsave").attr('disabled', false);
                    SGloadingRemove();
                },
                error: function(xhr, status, error) {
                    //reset variables here
                    console.log(status + ' ' + error);
                    alertmsg("red", msgErrorOccured + " while reading setting", -1);
                }
            });
        } else {
            bulkUpdateErrorManager();
        }
    });

//get columns of file (using papa-parse)
function getFileHeaders(evt, callback) {
    var file = evt.target.files[0];
    //console.log(file);
    var columns = [];

    Papa.parse(file, {
        delimiter: "",	// auto-detect
        newline: "",	// auto-detect
        header: false,
        dynamicTyping: false,
        preview: 0,
        encoding: "",
        worker: false,
        comments: false,
        step: undefined,
        error: undefined,
        download: false,
        skipEmptyLines: false,
        chunk: undefined,
        fastMode: undefined,
        beforeFirstChunk: undefined,
        withCredentials: undefined,
        complete: callback
    });

    return columns;
}

$("#vendor-file")
    .change(function (e) {
        //fix for Styles Logistics (vendorId = 0)
        vendorId = parseInt(vendorId);
        if (!isFinite(vendorId) || vendorId < 0) {
            alertmsg("red", "Please select a vendor first", -1);
            return;
        }

        var config = buildConfig();
        var files = $('#vendor-file')[0].files;

        var fileSize = files[0].size;
        if (fileSize > 1*1024*1024) { //1MB
            alertmsg("red", "Selected file is too large for browser ({0}MB), maximum allowed size via browser is 1MB".format(fileSize/1024/1024), -1);
            return;
        }

        var colHeaders = $("#chkFileContainsHeader").prop("checked");

        //For Papa Parse v4.1.2, it does not auto-detect delimiter for file having two columns. so specify delimiter (,)
        config.delimiter = ",";
        config.header = colHeaders;

        console.log(config);

        if (files.length > 0) {
            $('#vendor-file')
                .parse({
                    config: config,
                    before: function(file, inputElem) {
                        //console.log("Parsing file:", file);
                        SGloading("Loading");
                        columns1 = [];
                        if (hot) {
                            hot.updateSettings({ data: [] });
                        }
                    },
                    complete: function() {
                        SGloadingRemove();
                       
                        //console.log(columns1);
                        // //console.log(columns);
                        //console.log(fileData);
                        var cleansedColumns = [];
                        //console.log(columns1);

                        
                            $.grep(columns1,
                                function(n, i) {
                                    var columnString = JSON.stringify(n)
                                        .replace(/\=\\\"/g, "")
                                        .replace(/\\\"\"/g, "\"")
                                        .replace(/\\\"/g, "''")
                                        .replace(/ˏ/g, ",")
                                        .replace(/\\r/g, "")
                                        .replace(/\\n/g, "");

                                    //console.log(columnString);

                                    var parsedColumns = fixJsonQuote(columnString);
                                    ReplaceSingle2Quote(parsedColumns);
                                    cleansedColumns.push(parsedColumns);

                                });
                        
                        //console.log(cleansedColumns);

                        //check if columns in file (cleansedColumns) contains columns in setting (qohCustomFields)
                        var matchedColumn = [];
                        matchedColumnIndex = [];
                        //if (colHeaders) {
                            $.grep(cleansedColumns,
                                function(fileValue, iFile) {
                                    for (var i = 0; i < qohCustomFields.length; i++) {
                                        //if (qohCustomFields[i].FieldValue.toLowerCase() == (fileValue.data || fileValue.title).toLowerCase())
                                        //    matchedColumn.push(qohCustomFields[i].FieldValue);
                                        if (fileValue.data && qohCustomFields[i].FieldValue.toLowerCase() == fileValue.data.toLowerCase()) {
                                            matchedColumn.push(qohCustomFields[i].FieldValue);
                                        }
                                        else if (fileValue.title && qohCustomFields[i].FieldValue.toLowerCase() == fileValue.title.toLowerCase()) {
                                            matchedColumn.push(fileValue.title);
                                            matchedColumnIndex.push(iFile);
                                        }
                                        else if (colHeaders == false) {
                                            if (fileValue.title && qohCustomFields[i].NoHeaderField.toLowerCase() == fileValue.title.toLowerCase()) {
                                                matchedColumn.push(fileValue.title);
                                                matchedColumnIndex.push(iFile);
                                            }

                                        }
                                    }
                                });
                            //console.log(matchedColumn);
                        //}

                        //if (colHeaders) {
                            if (matchedColumn.length == 0) {
                                alertmsg("red", "File does not contain any column in QOH columns information", -1);
                                bulkUpdateErrorManager({ retainControls: true, retainHotData: false });
                                return;
                            }
                            if (matchedColumn.length != qohCustomFields.length) {
                                alertmsg("red", "File does not contain all column(s) in QOH columns information", -1);
                                bulkUpdateErrorManager({ retainControls: true, retainHotData: false });
                                return;
                            }
                        //}
                      
                        columnsXml = matchedColumn;
                       
                        //data to show to handsontable
                        var cleansedData = fileData;

                        columns1 = [];
                        var settings = {
                            data: cleansedData,
                            colHeaders: true,
                            rowHeaders: true,
                            //stretchH: 'all',
                            columnSorting: false,
                            search: true,
                            fixedRowsTop: 0,
                            fixedColumnsLeft: 1,
                            contextMenu: contextMenu,
                            licenseKey: 'non-commercial-and-evaluation',
                            columns: cleansedColumns,
                            columnsIndex: matchedColumnIndex, //custom property columnsIndex
                            minSpareRows: 0,
                            manualColumnResize: true,
                            //manualColumnMove: true,
                            skipEmptyLines: true
                        };

                        //if (colHeaders) {
                        //    settings.columns = cleansedColumns;
                        //}

                        hot = new Handsontable(container, settings);
                        if (cleansedData.length > 1 && hot.isEmptyRow(cleansedData.length - 1)) {
                            hot.alter('remove_row', parseInt(cleansedData.length - 1));
                        }

                        TblpgntBtm1();

                    },
                    error: function(xhr, status, error) {
                        alert(status + ' ' + error);
                    }
                });
        }

    });

$("#btn-save")
    .click(function() {
        //validation
        vendorId = parseInt(vendorId);
        if (!isFinite(vendorId) || vendorId < 0) {
            alertmsg("red","Please select a vendor first", -1);
            return;
        }
        if (qohCustomFields.length < 1) {
            alertmsg("red", "No setting for QOH", -1);
            return;
        }
        if (!hot || !hot.getSettings().data || hot.getSettings().data.length <= 0) {
            alertmsg("red", "No data to save", -1);
            return;
        }
        SGloading("Loading");

        var colHeaders = $("#chkFileContainsHeader").prop("checked");

        var sourceColumnsXml = colHeaders ? columnsXml : matchedColumnIndex;

        //var columnsXml = ["Material", "Avail Qty"];
        //var replacementColumnsXml = ["VendorPartNumber", "QOH"];
        var originalData = hot.getSettings().data; //.slice(0,2);
        //var myData = originalData.slice(0, 100).selectXml(columns, replacementColumns, "VendorProduct");

        var myData = originalData.selectQohXml(sourceColumnsXml, replacementColumnsXml, "VendorProduct", vendorId);

        myData = "<UDHVendorProductUpdate>{0}</UDHVendorProductUpdate>".format(myData);

        //console.log(myData);
        //SGloadingRemove();
        //return;
        myData = JSON.stringify(myData);
        $.ajax({
            type: "POST",
            url: urlBulkUpdate,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'vendorId': vendorId, 'xmlData': myData },
            dataType: "json",
            beforeSend: function() {
                //$("#btnsave").attr('disabled', true);
                //console.log("myData being sent to server");
                //console.log(myData);
                SGloading("Loading");
            },
            success: function(response) {
                if (response) {
                    if (response.Status === true) {
                        alertmsg("green", response.Message, -1);
                    } else {
                        alertmsg("red", response.Message, -1);
                    }
                } else {
                    alertmsg("red", msgErrorOccured, -1);
                }
            },
            complete: function() {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function(xhr, status, error) {
                //reset variables here
                console.log(status + ' ' + error);
                alertmsg("red", msgErrorOccured + "here", -1);
            }
        });
        //console.log(myData);
    });
