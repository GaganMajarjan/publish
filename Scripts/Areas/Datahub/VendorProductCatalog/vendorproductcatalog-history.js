﻿$(function () {
    var vendorProductHistoryDataTable = null;
    var loadVendorProductHistoryTable = function () {
        if (vendorProductHistoryDataTable == null) {
            vendorProductHistoryDataTable = $('#PIMVendorProductHistoryDataTable')
                .dataTable({
                    "deferRender": true,
                    "bServerSide": true,
                    "stateSave": false,
                    "bSort": true,
                    "searching": false,
                    "paging": true,
                    "aLengthMenu": dataTableMenuLength,
                    "iDisplayLength": 20,
                    //"iDisplayLength": dataTableDisplayLength,
                    "sAjaxSource": getVendorProductHistoryTable,
                    "bProcessing": true,
                    "scrollX": true,
                    "fnPreDrawCallback": function () {
                        // gather info to compose a message
                        SGloading("Loading");
                        //SGtableloading('fa-plane', '', this);
                    },
                    searchDelay: 500,
                    "fnDrawCallback": function () {
                        // in case your overlay needs to be put away automatically you can put it here
                        SGloadingRemove();
                        //rebuilttooltip();
                    },
                    "order": [[3, "desc"]]
                    //,
                    //"oLanguage": {
                    //    "sEmptyTable": "No history found for this product"
                    //}

                });

            // Ajax call
            $.ajax({
                type: 'GET',
                url: getVendorProductSeomStatus,
                cache: false,
                contentType: 'application/json; charset=utf-8',
                //dataType: 'json',
                //data: data,
                beforeSend: function () {
                    //$("#btnsave").attr('disabled', true);
                    SGloading('Loading');
                },
                success: function (response) {
                    if (response != null && response.Status === true) {
                        if (response.Data) {
                            //$("#divVPCHistoryExportedToSEOM").addClass("text-success");
                            $("#spanVPCHistoryExportedToSEOM").text(response.Data);

                            //Waiting, In Progress, Success, Failed, Synchronized
                            if (response.Data == "Waiting")
                                $("#divVPCHistoryExportedToSEOM").addClass("text-warning");
                            else if (response.Data == "In Progress")
                                $("#divVPCHistoryExportedToSEOM").addClass("text-info");
                            else if (response.Data == "Success")
                                $("#divVPCHistoryExportedToSEOM").addClass("text-success");
                            else if (response.Data == "Failed")
                                $("#divVPCHistoryExportedToSEOM").addClass("text-danger");
                            else if (response.Data == "Synchronized")
                                $("#divVPCHistoryExportedToSEOM").addClass("text-success");
                        }
                        //alertmsg("green", response.Message);
                    }
                    else if (response != null && response.Status === false) {
                        $("#divVPCHistoryExportedToSEOM").addClass("text-danger");
                        alertmsg("red", response.Message);
                    } else {
                        $("#divVPCHistoryExportedToSEOM").addClass("text-danger");
                        alertmsg("red", "An Error occurred");
                    }

                },
                complete: function () {
                    //$("#btnsave").attr('disabled', false);
                    SGloadingRemove();
                    $("#spanVPCHistoryExportedToSEOM").removeClass("fa").removeClass("fa-plane").removeClass("fa-spin");
                },
                error: function (xhr, status, error) {
                    //reset variables here
                    alert(status + ' ' + error);
                }
            });
        }

    }

    //loadHistoryTable();

    $("#tab_vpc_history")
        .on('click',
            function (e) {
                e.preventDefault();
                //vendorProductHistoryDataTable
                loadVendorProductHistoryTable();
                //vendorProductHistoryDataTable=$('#PIMProductvendorProductHistoryDataTable').dataTable();
                $(this).tab('show');
            });

})