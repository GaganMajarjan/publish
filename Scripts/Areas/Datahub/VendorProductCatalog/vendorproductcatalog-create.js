﻿
var dataAlreadyCreated = 0;
var associatewithExisting = 0;
var byPassOrderLineAmountForAssociate = false;
$(document).ready(function () {
    $("#btnVendorProductUpdate").hide();
    $('#vendor').select2();
    // $('#vendor').select2(remoteDataConfig(urlGetVendorListForSelect2, 3, 250, pleaseSelectMessage));


    //commented for handsontable js

    // var purchasingRuleColumnsViewBag = [
    //"Description",
    //"Cost",
    //"OnOrder",
    //"QuantityPerLot",
    //"LotPartNumber",
    //"LotBarcode",
    //"CostPerLot",
    //"OrderBy",
    //"MinimumOrderQuantity",
    //"MinimumOrderUnit",
    //"ManufacturersBarcode",
    //"LotDescription",
    //"LeadTimeInDays",
    //"CanNotDropship",
    //"IsDisabled"
    // ];
    //Handsontable.js related content commented
    //function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
    //    Handsontable.renderers.TextRenderer.apply(this, arguments);
    //    td.style.fontWeight = 'bold';
    //    td.style.color = 'green';
    //    td.style.background = '#CEC';
    //}

    //function negativeValueRenderer(instance, td, row, col, prop, value, cellProperties) {
    //    Handsontable.renderers.TextRenderer.apply(this, arguments);

    //    // if row contains negative number
    //    if (parseInt(value, 10) < 0) {
    //        // add class "negative"
    //        td.style.color = 'red';
    //    }


    //    if (!value || value === '') {
    //        td.style.background = '#EEE';
    //    }

    //    else {
    //        if (value === 'Nissan') {
    //            td.style.fontStyle = 'italic';
    //        }
    //        td.style.background = '';
    //    }
    //}

    //var setting = {
    //    startRows: 2,
    //    startCols: 15,
    //    rowHeaders: true,
    //    colHeaders: purchasingRuleColumnsViewBag,
    //    minSpareRows: 1,
    //     manualColumnResize: true,
    //     //manualColumnMove: true, 
    //    persistentState: false,
    //    contextMenu: true,
    //    removeRowPlugin: true,
    //    outsideClickDeselects: false,
    //    //contextMenuCopyPaste: {
    //    //    swfPath: 'swf/ZeroClipboard.swf'
    //    //},
    //    onSelection: function(row, col, row2, col2) {
    //        var meta = this.getCellMeta(row2, col2);

    //        if (meta.readOnly) {
    //            this.updateSettings({ fillHandle: false });
    //        } else {
    //            this.updateSettings({ fillHandle: true });
    //        }
    //    },
    //    cells: function(row, col, prop) {
    //        var cellProperties = {};

    //        if (row === 0 || this.instance.getData()[row][prop] === 'readOnly') {
    //            //cellProperties.readOnly = true; // make cell read-only if it is first row or the text reads 'readOnly'
    //            $("#vendorProductPurchasingRuleGrid").handsontable('setDataAtCell', 0, 0, "test");
    //            $("#vendorProductPurchasingRuleGrid").handsontable("setCellReadOnly", 0, 0);

    //        }
    //        if (row === 0) {
    //            cellProperties.renderer = firstRowRenderer; // uses function directly
    //        } else {
    //            cellProperties.renderer = "negativeValueRenderer"; // uses lookup map
    //        }

    //        return cellProperties;
    //    }
    //};

    //console.log(purchasingRuleColumnsViewBag);
    ////purchasingRuleColumnsViewBag = JSON.parse(purchasingRuleColumnsViewBag);
    //console.log(purchasingRuleColumnsViewBag);

    //Handsontable.renderers.registerRenderer('negativeValueRenderer', negativeValueRenderer);
    //var container = document.getElementById('vendorProductPurchasingRuleGrid'),
    //  settings = setting;

    //var hot = new Handsontable(container, settings);

    //hot.render();
    //var rowHeaders = document.getElementById('rowHeaders');
    ////Handsontable.dom.addEvent(rowHeaders, 'click', function () {
    ////    hot.updateSettings({
    ////        rowHeaders: this.checked
    ////    });
    ////});

    //var searchFiled = document.getElementById('search_field');

    //function onlyExactMatch(queryStr, value) {
    //    return queryStr.toString() === value.toString();
    //}

    //Handsontable.dom.addEvent(searchFiled, 'keyup', function (event) {
    //    var queryResult = hot.search.query(this.value);

    //    //console.log(queryResult);
    //    hot.render();
    //});


});


// Purchasing Level related js commented
//var qualifiersList = [];

//qualifiersListViewBag = JSON.parse(qualifiersListViewBag);

//$.each(qualifiersListViewBag, function (index, val) {
//    qualifiersList.push({
//        PricingQualifierId: val.Id,
//        name: val.Name,
//        value: val.MultiplicationFactor
//    });
//});


//console.log(unitOfMeasureListViewBag);
//unitOfMeasureListViewBag = JSON.parse(unitOfMeasureListViewBag);
//var unitOfMeasureList = [];
//$.each(unitOfMeasureListViewBag, function (index, val) {
//    unitOfMeasureList.push({
//        name: val.Description,
//        value: val.Id
//    });
//});

//ko.bindingHandlers.datepicker = {
//    init: function (element, valueAccessor, allBindingsAccessor) {
//        var options = allBindingsAccessor().datepickerOptions || {},
//            $el = $(element);

//        //initialize datepicker with some optional options
//        $el.datepicker(options);

//        //handle the field changing
//        ko.utils.registerEventHandler(element, "change", function () {
//            var observable = valueAccessor();
//            observable($el.datepicker("getDate"));
//        });

//        //handle disposal (if KO removes by the template binding)
//        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
//            $el.datepicker("destroy");
//        });

//    },
//    update: function (element, valueAccessor) {
//        var value = ko.utils.unwrapObservable(valueAccessor()),
//            $el = $(element),
//            current = $el.datepicker("getDate");

//        if (value - current !== 0) {
//            $el.datepicker("setDate", value);
//        }
//    }
//};
//var vendorProductCatalogVmJson = $.parseJSON(vendorProductCatalogVm);

var vendorProductCatalogVmJson = decodeHtml(vendorProductCatalogVm);//.replace(/&singlequot;/g, "'");
//console.log(vendorProductCatalogVmJson);
vendorProductCatalogVmJson = fixJsonQuote(vendorProductCatalogVmJson);
ReplaceSingle2Quote(vendorProductCatalogVmJson);

//var ProductCatalogViewModel = function (name, cost) {
//    var self = this;
  

//    // Check condition and adjust
//    //purchasingLevels = purchasingLevels != null ? purchasingLevels : [];

//    self.Name = ko.observable((vendorProductCatalogVmJson != null && vendorProductCatalogVmJson.Name != null) ? vendorProductCatalogVmJson.Name : '');
//    self.VendorPartNumber = ko.observable(vendorProductCatalogVmJson.VendorPartNumber);
//    self.Description = ko.observable(vendorProductCatalogVmJson.Description);
//    self.IsDisabled = ko.observable(vendorProductCatalogVmJson.IsDisabled);
//    self.VendorId = ko.observable(vendorProductCatalogVmJson.VendorId);
//    self.ProductId = ko.observable(vendorProductCatalogVmJson.ProductId);

//    // Properties - Purchasing
//    self.Cost = ko.observable(FormatAmount(vendorProductCatalogVmJson.Cost,4));

//    self.OrderBy = ko.observable(vendorProductCatalogVmJson.OrderBy);
//    self.MinimumOrderQuantity = ko.observable(vendorProductCatalogVmJson.MinimumOrderQuantity);
//    self.MinimumOrderUnit = ko.observable(vendorProductCatalogVmJson.MinimumOrderUnit);
//    self.ManufacturersBarcode = ko.observable(vendorProductCatalogVmJson.ManufacturersBarcode);
//    self.CanDropship = ko.observable(vendorProductCatalogVmJson.CanDropship);
//    self.CanNotDropship = ko.computed(function() {
//        return !self.CanDropship();
//    });
//    self.LeadTimeInDays = ko.observable(vendorProductCatalogVmJson.LeadTimeInDays);
//    self.CostPerLot = ko.observable(vendorProductCatalogVmJson.CostPerLot);
//    self.OnOrder = ko.observable(vendorProductCatalogVmJson.OnOrder);
//    self.QuantityPerLot = ko.observable(vendorProductCatalogVmJson.QuantityPerLot);
//    self.LotPartNumber = ko.observable(vendorProductCatalogVmJson.LotPartNumber);
//    self.LotBarcode = ko.observable(vendorProductCatalogVmJson.LotBarcode);
//    self.LotDescription = ko.observable(vendorProductCatalogVmJson.LotDescription);
//    self.ProductId = ko.observable(vendorProductCatalogVmJson.ProductId);
//    self.VendorProductId = ko.observable(vendorProductCatalogVmJson.VendorProductId);
//    self.Id = ko.observable(vendorProductCatalogVmJson.Id);  ///For VendorProductPurchasingRule Id
//    self.CreateTs = ko.observable(vendorProductCatalogVmJson.CreateTs !== null ? JSONDateWithTime(vendorProductCatalogVmJson.CreateTs) : "");
//    self.CreateUser = ko.observable(vendorProductCatalogVmJson.CreateUser);
//    self.DateLastCostUpdate = ko.observable(vendorProductCatalogVmJson.DateLastCostUpdate !== null ? JSONDateWithTime(vendorProductCatalogVmJson.DateLastCostUpdate) : "");
//    self.DateCreated = ko.observable(vendorProductCatalogVmJson.DateCreated !== null ? JSONDateWithTime(vendorProductCatalogVmJson.DateCreated) : "");
//    self.DateLastUpdated = ko.observable(vendorProductCatalogVmJson.DateLastUpdated !== null ? JSONDateWithTime(vendorProductCatalogVmJson.DateLastUpdated) : "");
//    self.Uom = ko.observable(vendorProductCatalogVmJson.Uom !== null ? vendorProductCatalogVmJson.Uom : "");
//    self.Spq = ko.observable(vendorProductCatalogVmJson.Spq !== null ? vendorProductCatalogVmJson.Spq : "");

//    //// Properties - General
//    //self.Name = ko.observable(name);
//    //self.VendorPartNumber = ko.observable();
//    //self.Description = ko.observable();
//    //self.IsDisabled = ko.observable();
//    //self.VendorId = ko.observable();

//    //// Properties - Purchasing
//    //self.Cost = ko.observable(cost);

//    //self.OrderBy = ko.observable();
//    //self.MinimumOrderQuantity = ko.observable();
//    //self.MinimumOrderUnit = ko.observable();
//    //self.ManufacturersBarcode = ko.observable();
//    //self.CanNotDropship = ko.observable();
//    //self.LeadTimeInDays = ko.observable();

//    //self.OnOrder = ko.observable();
//    //self.QuantityPerLot = ko.observable();
//    //self.LotPartNumber = ko.observable();
//    //self.LotBarcode = ko.observable();
//    //self.LotDescription = ko.observable();
//    //self.ProductId = ko.observable(getQueryStringByName("productId"));





//    //self.PurchasingLevels = ko.observableArray(purchasingLevels);
//    //self.unit = ko.observable();
//    //self.qualifier = ko.observable();
//    //self.UOM = ko.observable();
//    //self.EffectiveDateStart = ko.observable();
//    //self.EffectiveDateEnd = ko.observable();

//    //// Functions/Events
//    //self.addPurchasingLevel = function () {
//    //    self.PurchasingLevels.push(new PIMPricingLevelViewModel());
//    //};

//    //self.removePurchasingLevel = function (purchasingLevel) {
//    //    //console.log(pricingLevel);
//    //    self.PurchasingLevels.remove(purchasingLevel);
//    //};

//    //computed functions
//    self.IsQplEnabled = ko.computed(function () {
//        if (self.OrderBy() === 'L' || self.OrderBy() === 'Q') {

//            return true;
//        }

//        self.QuantityPerLot("");
//        return false;
//    });

//    self.IsByLot = ko.computed(function () {

//        if (self.OrderBy() === 'L') {
//            //if (self.OrderBy() === 'L' && self.QuantityPerLot() !== "") {

//            return true;
//        }

//        self.QuantityPerLot("");
//        self.LotDescription("");
//        self.LotPartNumber("");
//        //self.CostPerLot("");

//        return false;
//    });

//    self.CostPerLot = ko.computed(function () {
       
//        if (self.OrderBy() === "L" && self.QuantityPerLot() && self.Cost()) {

//            return FormatAmount(self.QuantityPerLot() * self.Cost());
//        }

//        if (self.OrderBy() === "" || self.OrderBy() === null || self.OrderBy() === "L")
//            return self.CostPerLot();

//        return "";
//    });


//    self.IsDisabledForEdit = ko.computed(function () {
//        return false;
//    });

   
//    //Save click
//    self.save = function (form) {
//        //console.log(ko.toJSON(self));
//        //var form = $('#__AjaxAntiForgeryForm');
//        //var token = $('[name=__RequestVerificationToken]').val();
//        dataAlreadyCreated = 0;
//        associatewithExisting = 0;
//        var workflowId = getQueryStringByName("workflowId");
//        var stepId = getQueryStringByName("stepId");
//        var stagingId = getQueryStringByName("stagingId");
//        var guid = getQueryStringByName("guid");
//        var logUserUploadId = getQueryStringByName("logUserUploadId");
//        var errorType = getQueryStringByName("errorType");
//        //console.log(token);
//        // Ajax call
//        $.ajax({
//            type: 'POST',
//            url: createUrl,
//            cache: false,
//            contentType: 'application/json; charset=utf-8',
//            dataType: 'json',
//            data:
//               ko.toJSON({
//                   viewModel: self,
//                   workflowId: workflowId,
//                   stepId: stepId,
//                   stagingId: stagingId,
//                   guid: guid,
//                   logUserUploadId: logUserUploadId,
//                   errorType: errorType,
//                   alreadyExist: dataAlreadyCreated,
//                   assotiateWithExisting: associatewithExisting
//               }),

//            beforeSend: function () {
//                //$("#btnsave").attr('disabled', true);
//                SGloading('Loading');
//            },
//            success: function (response) {
              

//                if (response != null && response.Success === true) {
//                    alertmsg("green", response.Message);
//                    if (response.Data)
//                        window.location.href = response.Data;
//                    var returnUrl = getQueryStringByName("returnUrl");
//                    if (returnUrl)
//                        window.location.href = returnUrl;
//                }
//                else if (response != null && response.Success === false) {
//                    if (response.Message === "AlreadyExist") {
//                        dataAlreadyCreated = 1;
//                        alertmsg("red", "VendorProduct with same VendorPartNumber already exist for selected vendor");
//                        //  $("#btnVendorProductUpdate").click();
//                    }else
//                    alertmsg("red", response.Message);
//                } else {
//                    alertmsg("red", "An Error occurred");
//                }
//            },
//            complete: function () {
//                //$("#btnsave").attr('disabled', false);
//                SGloadingRemove();
//            },
//            error: function (xhr, status, error) {
//                //reset variables here
//                alert(status + ' ' + error);
//            }
//        });
//        // To actually transmit to server as a regular form post, write this: ko.utils.postJson($("form")[0], self.PricingLevels);
//    };

//};

//Purchasing Level view model commented

//var PIMPricingLevelViewModel = function (unitCost, qualifier,unit, newCost) {
//    var self = this;
//    self.PricingQualifierId = ko.observable();
//    self.unitCost = ko.observable(0);
//    self.unit = ko.observable('');
//    self.qualifier = ko.observable('');
//    self.newCost = ko.observable(0);
//    self.Margin = ko.observable(0);
//    self.UnitOfMeasureSdvId = ko.observable(0);
//    self.NewUnitPrice = ko.observable(0);

//    self.qualifier.subscribe(function (val) {

//        if (val == null)
//            return;
//        //console.log(parseFloat(viewModel.SalePrice()) * parseFloat(val.value));


//        self.PricingQualifierId(val.PricingQualifierId);
//        self.newCost(FormatAmount(parseFloat(viewModel.Cost()) * parseFloat(val.value)));
//        self.Margin(FormatAmount((1 - parseFloat(val.value)) * viewModel.Cost()));
//    });

//    self.unit.subscribe(function (val) {

//        if (val == null)
//            return;
//        //console.log(parseFloat(viewModel.SalePrice()) * parseFloat(val.value));


//        self.UnitOfMeasureSdvId(val.Id);
//    });

//    self.unitCost.subscribe(function (val) {

//        if (val == null)
//            return;

//        self.Price(self.unitCost());
//    });


//}

var viewModel = new VendorProductCatalogViewModel(vendorProductCatalogVmJson);
ko.applyBindings(viewModel);

// Activate jQuery Validation
//$("#__AjaxAntiForgeryForm").validate();

$("#btnCancel").click(function () {
    var returnUrl = getQueryStringByName("returnUrl");
    if (returnUrl !== '')
        window.location = returnUrl;
    else {
        window.location = urlCatalogManagerIndex;
    }
});

$(function () {
    //$('#CostPerLot').change(function () {
    //    var val = $('#CostPerLot').val();

    //    var quantityPerLot = viewModel.QuantityPerLot();

    //    var cost = parseFloat(val) / parseFloat(quantityPerLot);

    //    viewModel.Cost(FormatAmount(cost));

    //});

    activateBootStrapTab("vpc-general");

    //for create, enable vendor
    $('#vendor').prop('disabled', false);
});


$("#CreateAssociateVendorProductCatalog").click(function () {
    var form = $('#__AjaxAntiForgeryForm');
    var token = $('[name=__RequestVerificationToken]').val();

    dataAlreadyCreated = 0;
    associatewithExisting = 1;
    var workflowId = getQueryStringByName("workflowId");
    var stepId = getQueryStringByName("stepId");
    var stagingId = getQueryStringByName("stagingId");
    var guid = getQueryStringByName("guid");
    var logUserUploadId = getQueryStringByName("logUserUploadId");
    var errorType = getQueryStringByName("errorType");

    //SG-808
    //if (!byPassOrderLineAmountForAssociate)
    //    var minLineAmount = vendorProductCatalogVmJson.MinLineOrderAmount === null ? $('#MinLineOrderAmount').val() : vendorProductCatalogVmJson.MinLineOrderAmount ;
    //var isValid = (validateCostAndMinQtyWithMinLineOrder(minLineAmount))
    //if(!isValid)
    //    return;

   
    //console.log(token);
    // Ajax call
    $.ajax({
        type: 'POST',
        url: createUrl,
        cache: false,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data:
                ko.toJSON({
                    viewModel: viewModel,
                    workflowId: workflowId,
                    stepId: stepId,
                    stagingId: stagingId,
                    guid: guid,
                    logUserUploadId: logUserUploadId,
                    errorType: errorType,
                    alreadyExist: dataAlreadyCreated,
                    assotiateWithExisting: associatewithExisting
                }),
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Loading');
        },
        success: function (response) {
            $("#UpdatePurchasingNobtn").click();
            if (response != null && response.Success === true) {
                alertmsg("green", response.Message);
                if (response.Data)
                    window.location.href = response.Data;
                var returnUrl = getQueryStringByName("returnUrl");
                if (returnUrl)
                    window.location.href = returnUrl;
            }
            else if (response != null && response.Success === false) {
                if (response.Message === "AlreadyExist") {
                    //  $("#btnVendorProductUpdate").click();
                    alertmsg("red", "VendorProduct with same VendorPartNumber already exist for selected vendor");
                } else
                    alertmsg("red", response.Message);
            } else {
                alertmsg("red", "An Error occurred");
            }
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();


        },
        error: function (xhr, status, error) {
            //reset variables here
            alert(status + ' ' + error);
        }
    });
});

function AsscociateProduct()
{
    $.ajax({
        type: 'POST',
        url: createUrl,
        cache: false,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data:
                ko.toJSON({
                    viewModel: viewModel,
                    workflowId: workflowId,
                    stepId: stepId,
                    stagingId: stagingId,
                    guid: guid,
                    logUserUploadId: logUserUploadId,
                    errorType: errorType,
                    alreadyExist: dataAlreadyCreated,
                    assotiateWithExisting: associatewithExisting
                }),
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Loading');
        },
        success: function (response) {
            $("#UpdatePurchasingNobtn").click();
            if (response != null && response.Success === true) {
                alertmsg("green", response.Message);
                if (response.Data)
                    window.location.href = response.Data;
                var returnUrl = getQueryStringByName("returnUrl");
                if (returnUrl)
                    window.location.href = returnUrl;
            }
            else if (response != null && response.Success === false) {
                if (response.Message === "AlreadyExist") {
                    //  $("#btnVendorProductUpdate").click();
                    alertmsg("red", "VendorProduct with same VendorPartNumber already exist for selected vendor");
                } else
                    alertmsg("red", response.Message);
            } else {
                alertmsg("red", "An Error occurred");
            }
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();


        },
        error: function (xhr, status, error) {
            //reset variables here
            alert(status + ' ' + error);
        }
    });
}
    var form = $('#__AjaxAntiForgeryForm');
    var token = $('[name=__RequestVerificationToken]').val();

    dataAlreadyCreated = 0;
    var workflowId = getQueryStringByName("workflowId");
    var stepId = getQueryStringByName("stepId");
    var stagingId = getQueryStringByName("stagingId");
    var guid = getQueryStringByName("guid");
    var logUserUploadId = getQueryStringByName("logUserUploadId");
    var errorType = getQueryStringByName("errorType");

    $("#UpdatePurchasingNobtn").click(function () {
        dataAlreadyCreated = 0;
        associatewithExisting = 0;

    });

    $("#btnUpdatePurchasing").click(function () {
        var workflowId = getQueryStringByName("workflowId");
        var stepId = getQueryStringByName("stepId");
        var stagingId = getQueryStringByName("stagingId");
        var guid = getQueryStringByName("guid");
        var logUserUploadId = getQueryStringByName("logUserUploadId");
        var errorType = getQueryStringByName("errorType");
        dataAlreadyCreated = 1;
        //console.log(token);
        // Ajax call
        $.ajax({
            type: 'POST',
            url: createUrl,
            cache: false,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data:
               ko.toJSON({
                   viewModel: viewModel,
                   workflowId: workflowId,
                   stepId: stepId,
                   stagingId: stagingId,
                   guid: guid,
                   logUserUploadId: logUserUploadId,
                   errorType: errorType,
                   alreadyExist: dataAlreadyCreated,
                   assotiateWithExisting: associatewithExisting
               }),

            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {
                $("#UpdatePurchasingNobtn").click();
                if (response != null && response.Success === true) {
                    alertmsg("green", response.Message);
                    if (response.Data)
                        window.location.href = response.Data;
                    var returnUrl = getQueryStringByName("returnUrl");
                    if (returnUrl)
                        window.location.href = returnUrl;
                }
                else if (response != null && response.Success === false) {
                    if (response.Message === "AlreadyExist") {
                        $("#btnVendorProductUpdate").click();
                    } else
                        alertmsg("red", response.Message);
                } else {
                    alertmsg("red", "An Error occurred");
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();


            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });
    });

    $(".datetimeDataType").datepicker();
