﻿//pricing viewmodel
//var showPurchasingLevel = ConfigurationManager.AppSettings["ShowPurchasingLevel"];
var VendorProductPurchaseRuleLevelVM = function (vm) {

    var self = this;
    var vpcChangeLevel = "";
    var vpcChangeLevel3 = "";

    self.QuantityFrom = ko.observable(vm == null ? 1 : vm.QuantityFrom != null ? vm.QuantityFrom : '');
    //self.QuantityTo = ko.observable(vm == null ? 1 : vm.QuantityTo != null?vm.QuantityTo:'');
    self.GrossMarginPercent = ko.observable(vm == null ? /*(viewModel.MarginPercent())*/ 0 : vm.GrossMarginPercent != null ? (vm.GrossMarginPercent) : '')
                                     .extend({ numeric: decimal_places });

    self.GrossMarginAmount = ko.observable(vm == null ? /*(viewModel.MarginAmount())*/ 0 : vm.GrossMarginAmount != null ? (vm.GrossMarginAmount) : '')
                                         .extend({ numeric: decimal_places });

    self.PriceForPricingLevel = ko.observable(vm == null ? 0 : vm.Price != null ? (vm.Price) : '')
                                         .extend({ numeric: decimal_places });

    self.Price = ko.computed(function () {

        if (viewModel) {
            if (self.PriceForPricingLevel() == viewModel.Cost()) {
                alertmsg("yellow",
                    "Warning: Selling price for pricing level '{0}' and cost '{0}' cannot be same"
                    .format(self.PriceForPricingLevel(), viewModel.Cost()));
            }
        }

        return self.PriceForPricingLevel();
    });

    self.GrossMarginPercent.subscribe(function (val) {

        if (val < 0)
        {
            //marginNegativeDueToCostChange = true;
        }
        if ((vpcChangeLevel.match(/,/g) || []).length === 2 || vpcChangeLevel.indexOf("GMP") > -1) {
            vpcChangeLevel = "";

            return;
        }
        if ((vpcChangeLevel3.match(/,/g) || []).length === 1 || vpcChangeLevel3.indexOf("GMP") > -1) {
            vpcChangeLevel3 = "";
            vpcChangeLevel = "";

            return;
        }

        val = parseFloat(val);

        if ($.isNumeric(val) === false) {
            return;
        }
        if (val === 0)
            return;
        if (parseFloat(val) >= 100) {
            alertmsg('red', 'Margin percent cannot be greater than or equal to 100');
            self.GrossMarginPercent(0);
            self.GrossMarginAmount(0);
            return;
        }
        var myCost = parseFloat(viewModel.Cost());
        var myGrossMarginAmount = 0;
        var myGrossMarginPercent = parseFloat(val);
        var mySellingPrice = 0;

        // Validation
        if (myCost === 0 || myGrossMarginPercent === 0) //amount check not required
            return;

        // Calculation

        mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);
        myGrossMarginAmount = mySellingPrice - myCost;

        vpcChangeLevel += "GMP,";
        vpcChangeLevel3 += "GMP,";
        // Set values
        self.GrossMarginAmount(FormatAmount(myGrossMarginAmount));

        self.PriceForPricingLevel(FormatAmount(mySellingPrice));

        var model;
        if (viewModel.vendorProduct) {
            //case from productcatalog: vendorproduct is child of product viewmodel
            model = viewModel.vendorProduct();
        } else {
            //case from vendorproductcatalog: viewmodel is vpc
            model = viewModel;
        }
        model.HasPurchasingLevelChanged(true);
        //////////////console.log('outside GrossMarginPercent');
    });

    self.PriceForPricingLevel.subscribe(function (val) {
        //////////////console.log('inside PriceForPricingLevel');


        if ((vpcChangeLevel.match(/,/g) || []).length === 2 || vpcChangeLevel.indexOf("SEP") > -1) {
            vpcChangeLevel = "";

            return;
        }

        if ((vpcChangeLevel3.match(/,/g) || []).length === 1 || vpcChangeLevel3.indexOf("SEP") > -1) {
            vpcChangeLevel3 = "";
            vpcChangeLevel = "";

            return;
        }

        val = parseFloat(val);
        if ($.isNumeric(val) === false) {
            return;
        }
        if (val === 0)
            return;

        var myCost = parseFloat(viewModel.Cost());
        var myGrossMarginPercent = parseFloat(self.GrossMarginPercent());
        var mySellingPrice = parseFloat(val);



        // Validation
        if (myCost === mySellingPrice) {
            return;
        }
        if (myCost === 0 || mySellingPrice === 0)
            return;

        // Calculation

        if (myCost !== 0) {
            myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);

        }

        vpcChangeLevel += "SEP,";
        vpcChangeLevel3 += "SEP,";
        // Set values
        //

       // if (!viewModel.IsMarginLocked()) {

            self.GrossMarginPercent(FormatAmount(myGrossMarginPercent));

        //}
        self.GrossMarginAmount(FormatAmount(mySellingPrice - myCost));

        var model;
        if (viewModel.vendorProduct) {
            //case from productcatalog: vendorproduct is child of product viewmodel
            model = viewModel.vendorProduct();
        } else {
            //case from vendorproductcatalog: viewmodel is vpc
            model = viewModel;
        }
        model.HasPurchasingLevelChanged(true);
        vpcChangeLevel3 = "";
        vpcChangeLevel = "";
    });

    self.QuantityFrom.subscribe(function (val) {
        var quantityFrom = parseFloat(val);

        var currentIndex = 0;

        var model;
        if (viewModel.vendorProduct) {
            //case from productcatalog: vendorproduct is child of product viewmodel
            model = viewModel.vendorProduct();
        } else {
            //case from vendorproductcatalog: viewmodel is vpc
            model = viewModel;
        }

        for (var i = 0; i < model.PurchasingLevels().length; i++) {
            if (model.PurchasingLevels()[i].QuantityFrom() === val) {
                currentIndex = i;
                break;
            }
        }

        if (currentIndex > 0) {

            var previousPricingLevel = model.PurchasingLevels()[currentIndex - 1];
            var addedPricingLevel = model.PurchasingLevels()[currentIndex];

            if (previousPricingLevel != null && previousPricingLevel.QuantityFrom() != null && previousPricingLevel.QuantityFrom() !== "") {
                if (parseFloat(previousPricingLevel.Quantity) < parseFloat(addedPricingLevel.QuantityFrom)) {
                    alertmsg('red', 'Previous Quantity (' + previousPricingLevel.QuantityFrom + ') should be less than current quantity (' + val.quantityFrom + '.');

                    self.QuantityFrom('');
                    return;
                }
            }

        }

    });

}

var VendorProductPurchaseRuleLevelVMList = function (vmList) {
    var vendorProductPurchaseRuleLevelVmList = [];
    $.each(vmList,
        function (index, vm) {
            vendorProductPurchaseRuleLevelVmList.push(new VendorProductPurchaseRuleLevelVM(vm));
        });
    return vendorProductPurchaseRuleLevelVmList;
}
