﻿
function validateSpqAndQpl() {

    //check P,Q or L
    var orderBy = $('#OrderBy').val();
    if (orderBy === 'undefined' || orderBy === null || orderBy === "")
        return true;

    //get values
    var spq = $('#Spq').val();
    var qpl = $('#QuantityPerLot').val();

    if (
        //should be order by lot
        (orderBy === "L" || orderBy === "Q")
        //check spq validity
        && typeof spq !== 'undefined' && spq !== null
        //check qpl validity
        && typeof qpl !== 'undefined' && qpl !== null
        //check non-equality of spq and qpl
        && spq !== qpl) {
        //show message
        $('#SpqQplDoNotMatchModal').modal('show');
        //alertmsg("red", "Standard Package Quantity and Quantity Per Lot are not same");
        return false;
    }


    if (orderBy === "P" && spq > 1) {
        $('#SpqNotMathcedForOrderByPieceModal').modal('show');


        document.getElementById('SpqNotMathcedForOrderByPieceModal').childNodes[1].childNodes[1].childNodes[3].innerHTML = '';

        var h = document.createElement("H5");
        var t = document.createTextNode('Vendor Product is  \'Order by Piece\'  but the Feed Standard Package Quantity is ' + spq);
        h.appendChild(t);
        var br = document.createElement("br")
        h.appendChild(br);
        var br1 = document.createElement("br")
        h.appendChild(br1);
        var t = document.createTextNode(' Do you still want to continue?');
        h.appendChild(t);

        document.getElementById('SpqNotMathcedForOrderByPieceModal').childNodes[1].childNodes[1].childNodes[3].append(h);


        return false;

    }

    //if we reach here, it's valid. return true
    return true;
}


$('#btnYesSpqNotMathcedForOrderByPieceModal').click(function () {
    //requirement: the form/javascript that includes this .js file should have viewModel as its parent MVVM
    if (typeof viewModel !== 'undefined' && viewModel !== null) {
        $('#SpqNotMathcedForOrderByPieceModal').modal('hide');
        viewModel.save({
            bypassCostRangeCheck: false,
            bypassPriceRangeCheck: false
        });
        //SG-803
        OrderByValueChanged = false;
    } else {
        console.log('viewModel not defined');
    }
});

function validateCostRange() {
    var cost = $('#Cost').val();
    if (cost) {
        if (!isNaN(parseFloat(cost)) && isFinite(parseFloat(cost)) && parseFloat(cost) > 0 && parseFloat(cost) <= 0.02) {
            $('#CostRangeModal').modal('show');
          //  alertmsg('red', 'Rounded Selected Cost = Rounded Selling Price, Increase Margin or Enable Sales Price to continue');
            return false;
        }
    }

    //if we reach here, it's valid. return true
    return true;
}

$('#btnYesSpqQplDoNotMatchModal').click(function () {
    //requirement: the form/javascript that includes this .js file should have viewModel as its parent MVVM
    if (typeof viewModel !== 'undefined' && viewModel !== null) {
        $('#SpqQplDoNotMatchModal').modal('hide');
        viewModel.save({
            bypassCostRangeCheck: false,
            bypassPriceRangeCheck: false
        });
        //SG-803
        OrderByValueChanged = false;
    } else {
        console.log('viewModel not defined');
    }
});

$('#btnYesCostRangeModal').click(function () {
    //requirement: the form/javascript that includes this .js file should have viewModel as its parent MVVM
    if (typeof viewModel !== 'undefined' && viewModel !== null) {
        $('#CostRangeModal').modal('hide');
        viewModel.save({
            bypassCostRangeCheck: true,
            bypassCostThresholdCheck: true,
            bypassPriceRangeCheck: false
        });
    } else {
        console.log('viewModel not defined');
    }

});


//validate price range
function validatePriceRange() {
    var price = $('#Price').val();
    if (price) {
        if (!isNaN(parseFloat(price)) && isFinite(parseFloat(price)) && parseFloat(price) > 0 && parseFloat(price) <= 0.02) {
            $('#PriceRangeModal').modal('show');
           // alertmsg('red', 'Rounded Selected Cost = Rounded Selling Price, Increase Margin or Enable Sales Price to continue');
            return false;
        }
    }

    //if we reach here, it's valid. return true
    return true;
}

function validateCostPriceRange() {
    var price = $('#Price').val();
    var cost = $('#SelectedCost').val();
    if (price && cost) {
        if (!isNaN(parseFloat(price)) && isFinite(parseFloat(price)) && parseFloat(price) > 0
            && !isNaN(parseFloat(cost)) && isFinite(parseFloat(cost)) && parseFloat(cost) > 0) {
            price = roundToTwo(price);
            cost = roundToTwo(cost);
            if (price === cost) {
                alertmsg('red', 'Rounded Selected Cost = Rounded Selling Price, Increase Margin or Enable Sales Price to continue');
                return false;
            }
        }
    }

    //if we reach here, it's valid. return true
    return true;
}
function roundToTwo(num) {
    return +(Math.round(num + "e+2") + "e-2");
}

$('#btnYesPriceRangeModal').click(function () {
    //requirement: the form/javascript that includes this .js file should have viewModel as its parent MVVM
    if (typeof viewModel !== 'undefined' && viewModel !== null) {
        $('#PriceRangeModal').modal('hide');
        viewModel.save({
            bypassCostRangeCheck: true,
            bypassPriceRangeCheck: true
        });
    } else {
        console.log('viewModel not defined');
    }

});

//validate price and cost same
function validatePriceAndCostSame() {
    var price = $('#Price').val();
    var cost = $('#Cost').val();

    if (price && cost)
        if (!isNaN(parseFloat(price)) && isFinite(parseFloat(price)) && parseFloat(price) > 0
            && !isNaN(parseFloat(cost)) && isFinite(parseFloat(cost)) && parseFloat(cost) > 0
            && price == cost) {
            $('#PriceAndCostSameModal').modal('show');
            return false;
        }

    //if we reach here, it's valid. return true
    return true;
}

$('#btnYesPriceAndCostSameModal').click(function () {
    //requirement: the form/javascript that includes this .js file should have viewModel as its parent MVVM
    if (typeof viewModel !== 'undefined' && viewModel !== null) {
        $('#PriceAndCostSameModal').modal('hide');
        viewModel.save({
            bypassCostThresholdCheck: true,
            bypassCostRangeCheck: true,
            bypassPriceRangeCheck: true,
            bypassPriceAndCostSameCheck: true
        });
    } else {
        console.log('viewModel not defined');
    }
});

//SG-808 validate MinLineOrder

function validateCostAndMinQtyWithMinLineOrder(minLineOrderAmount, cost) {

    var toBeQty = 0;

    var mou = $('#MinimumOrderUnit').val();
    var orderBy = $('#OrderBy').val();
    if (cost == undefined) {
        cost = $('#Cost').val();
    }

    if (mou === 'undefined' || mou === null || mou === "")
        return true;

    if (orderBy === 'undefined' || orderBy === null || orderBy === "")
        return true;

    if (cost == 0 || cost == undefined || cost == '')
        return true;





    if (minLineOrderAmount != undefined && minLineOrderAmount != 0 && minLineOrderAmount != '') {
        if (minLineOrderAmount != undefined) {

            var qty = $('#MinimumOrderQuantity').val();

            if (mou === 'L' && orderBy == 'L') {
                var qoh = $('#QuantityPerLot').val();
                if (qoh !== 0 && qoh !== '' && qoh !== undefined) {
                    toBeQty = Math.ceil(Math.ceil(minLineOrderAmount) / (cost * qoh));
                }
            }
            else {
                toBeQty = Math.ceil(Math.ceil(minLineOrderAmount) / cost);
            }
            // if (toBeQty > qty) {

            $('#MinimumOrderQuantity').val(toBeQty);
            var model;
            if (viewModel.vendorProduct) {
                //case from productcatalog: vendorproduct is child of product viewmodel
                viewModel.vendorProduct().MinimumOrderQuantity = toBeQty;

            } else {
                //case from vendorproductcatalog: viewmodel is vpc
                viewModel.MinimumOrderQuantity = toBeQty;
            }
            //$('#VendorProductNotMatchedLineOrderAmountModal').modal('show');
            //document.getElementById('VendorProductNotMatchedLineOrderAmountModal').childNodes[1].childNodes[1].childNodes[3].innerHTML = '';
            //var h = document.createElement("H5");
            //var t = document.createTextNode('Minimum Order Quantity should be at least ' + toBeQty + ' based on the Minimum Line Order Amount of $' + minLineOrderAmount + '.')
            //h.appendChild(t);
            //var br = document.createElement("br")
            //h.appendChild(br);
            ////var br1 = document.createElement("br")
            ////h.appendChild(br1);
            //var t = document.createTextNode(' Do you still want to continue?');
            //h.appendChild(t);
            //document.getElementById('VendorProductNotMatchedLineOrderAmountModal').childNodes[1].childNodes[1].childNodes[3].append(h);


            // return false;
            //}
        }
        return true;

    }
    return true;
}

$('#btnYesVendorProductNotMatchedLineOrderAmountModal').click(function () {

    if (typeof viewModel !== 'undefined' && viewModel !== null) {
       // $('#VendorProductNotMatchedLineOrderAmountModal').modal('hide');
        $('#btnNoVendorProductNotMatchedLineOrderAmountModal').click()

        if (associatewithExisting === 1) {
            // byPassOrderLineAmountForAssociate = true;
            AsscociateProduct();
        }
        else {

            viewModel.save({
                bypassCostThresholdCheck: true,
                bypassCostRangeCheck: true,
                bypassPriceRangeCheck: true,
                bypassPriceAndCostSameCheck: true,
                bypassQuantityDiscountRoundUpCheck: true,
                bypassValidateProductAmountCheck: true
            });
        }
    }

    else { console.log('viewModel is undefined') }
});

function validateNegativeMarginFromVPEditor(data) {

    $.ajax({
        type: 'GET',
        url: validateProductMargin,
        cache: false,
        async: false,
        contentType: 'application/json; charset=utf-8',
        data: { VendorCatalogId: data.Id(), cost: data.Cost() },
        dataType: 'json',
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Validating');
        },
        success: function (response) {
            var data = response.Data.split(',')
            newMargin = parseFloat(data[0]) || 0;
            newMarginLevel = parseFloat(data[1]) || 0;
            saleMargin = parseFloat(data[2]) || 0;
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            alert(status + ' ' + error);
            SGloadingRemove();
        }

    })

}
function costpricevalidation(data) {
    var result = false;
    $.ajax({
        type: 'GET',
        url: validatecostprice,
        cache: false,
        async: false,
        contentType: 'application/json; charset=utf-8',
        data: { VendorCatalogId: data.Id(), cost: data.Cost(), IsMarginLocked: null },
        dataType: 'json',
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Validating');
        },
        success: function (response) {
            var r = response.Data;
            if (r.Status === 1) {
                alertmsg('red', r.StatusMessage);
                result = false;
            }
            else if (r.Status === 2) {
                alertmsg('yellow', r.StatusMessage);
                result = true;
            }
            else {
                result = true;
            }
        },
        complete: function () {

            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            alert(status + ' ' + error);
            SGloadingRemove();
        }

    });
    return result;
}
function costpricevalidationFromProduct(data) {
    var result = false;
    $.ajax({
        type: 'GET',
        url: validatecostprice,
        cache: false,
        async: false,
        contentType: 'application/json; charset=utf-8',
        data: { VendorCatalogId: data.vendorProduct().Id(), cost: data.Cost(), IsMarginLocked: data.IsMarginLocked() },
        dataType: 'json',
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Validating');
        },
        success: function (response) {
            var r = response.Data;
            if (r.Status === 1) {
                alertmsg('red', r.StatusMessage);
                result = false;
            }
            else if (r.Status === 2) {
                alertmsg('yellow', r.StatusMessage);
                result = true;
            }
            else {
                result = true;
            }
        },
        complete: function () {

            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            alert(status + ' ' + error);
            SGloadingRemove();
        }

    });
    return result;
}

function validateSaleMarginGreaterThanProductMargin() {

    var saleMargin = $('#SalePriceForClearanceMarginPercent').val();
    var productMargin = $('#MarginPercent').val();

    if (saleMargin && productMargin) {

        if (parseFloat(saleMargin) == 0 && parseFloat(productMargin) == 0) {
            return true;
        }
        if (parseFloat(saleMargin) >= parseFloat(productMargin)) {
            $('#SaleMarginModal').modal('show');
            return false;
        }


    }
    return true;
}

$('#btnYesSaleMarginModal').click(function () {

    $('#btnNoSaleMarginModal').click();
    viewModel.IsValidate = true;
    bypassNegativeMargin = true;
    byPassSaleMargin = true;
    viewModel.save({
        bypassCostRangeCheck: true,
        bypassValidateProductAmountCheck: true,
        bypassPriceRangeCheck: true,
        bypassPriceAndCostSameCheck: true,
        bypassQuantityDiscountRoundUpCheck: true,
        bypassKitQuantityDiscountRoundUpCheck: true,
        bypassValidateProductAmountCheck: true,
        bypassSaleMarginGreaterThanProductMargin:true
    })
})
      