﻿//SG-803
var OrderByValueChanged = false;
var optionModelForVendorProduct = "";

var vpprChangeLevel = "";
var hasUserChangedVpprCost = false;

var dataAlreadyCreated = 0;
var associatewithExisting = 0;

var disableMarginLevel = false;
var bypassNegativeMargin = false;
var newMargin = 75;
var newMarginLevel = 0;
var byPassSaleMargin = false;
var saleMargin = 0;




var VendorProductCatalogViewModel = function (vpcViewModel) {
    var self = this;

   
    self.vpcMarginLocked = ko.observable();

    self.vpcMarginLocked.subscribe(function (val) {
        self.vpcMarginLocked(val);
      
    })

    self.disableMarginLevel = function () {
        if (typeof (marginLockedOnPE) !== 'undefined') {
            if (marginLockedOnPE === true) {
                return true;
            }
            else { return false; }
        }
        else {
            return true;
        }
    }
    disableMarginLevel = self.disableMarginLevel();

    // Properties - General
    self.Name = ko.observable((vpcViewModel != null && vpcViewModel.Name != null) ? vpcViewModel.Name : '');
    self.VendorPartNumber = ko.observable((vpcViewModel != null && vpcViewModel.VendorPartNumber != null) ? vpcViewModel.VendorPartNumber : '');
    self.Description = ko.observable((vpcViewModel != null && vpcViewModel.Description != null) ? vpcViewModel.Description : '');
    self.IsDisabled = ko.observable((vpcViewModel != null && vpcViewModel.IsDisabled != null) ? vpcViewModel.IsDisabled : false);
    self.VendorId = ko.observable((vpcViewModel != null && vpcViewModel.VendorId != null) ? vpcViewModel.VendorId.toString() : '');
    self.VendorName = ko.observable((vpcViewModel != null && vpcViewModel.VendorName != null) ? vpcViewModel.VendorName.toString() : '');
    // Properties - Purchasing
    self.Cost = ko.observable((vpcViewModel != null && vpcViewModel.Cost != null) ? FormatAmount(vpcViewModel.Cost, 4) : '');

    self.Cost.subscribe(function (val) {

        if (isNaN(val) || val === "") {
            return;
        }

        //tie up price of pricing and purchasing
        if (typeof(viewModel.vendorProduct) !== "undefined") { //if it's pc, then it has vendorProduct as child. if it's vpc, it won't have vendorProduct as child
            if ((viewModel.Id() === "" || viewModel.Id() === -1) && self.ProductId() === -1) {
                viewModel.Cost(val);
            }
            if (viewModel.Id() !== "" && viewModel.Id() !== -1 && self.Id() !== "" && self.Id() !== -1) {
                viewModel.Cost(val);
            }
        }

        //fix: show cost per lot only when 'by the Lot' is selected
        if (self.IsByLot()) {
            if ((vpprChangeLevel.match(/,/g) || []).length === 2 || vpprChangeLevel.indexOf("VPPRCOST,") > -1) {
                vpprChangeLevel = "";
                return;
            }

            vpprChangeLevel += "VPPRCOST,";

            var qtyPerLot = self.QuantityPerLot();
            if (qtyPerLot === "" || qtyPerLot === 0)
                return;
            var costPerLot = FormatAmount(val * qtyPerLot);
            if (costPerLot !== FormatAmount(self.CostPerLot()) && costPerLot !== 0) {
                if (vpprChangeLevel.indexOf("CPL,") === -1) {
                    self.CostPerLot(costPerLot);
                }
            }

        }

        var minLineOrderAmountforProductVendor = $('#MinLineOrderAmount').val();
        if (minLineOrderAmountforProductVendor === undefined) {

            minLineOrderAmountforProductVendor = vendorProductCatalogVmJson.MinLineOrderAmount;
        }
        validateCostAndMinQtyWithMinLineOrder(minLineOrderAmountforProductVendor, val);

        //if quantity discount is there, update their value
        

        var date = GetTodayDate();
        try
        {
           self.DateLastCostUpdate(date);
        }
        catch
        {
            self.DateLastCostUpdate=date;
        }
        $('#DateLastCostUpdate').val(date);
       
    });

    self.OrderBy = ko.observable((vpcViewModel != null && vpcViewModel.OrderBy != null) ? vpcViewModel.OrderBy : 'P');
    self.OrderByText = ko.computed(function() {
        if (self.OrderBy() === 'P') {
            return 'by the Piece';
        }
        else if (self.OrderBy() === 'L') {
            return 'by the Lot';
        }
        //else if (self.OrderBy() === 'Q') {
        //    return 'by the Piece in Fixed Lot';
        //}//SKYG-820
        return '';
    });
    self.MinimumOrderQuantity = ko.observable((vpcViewModel != null && vpcViewModel.MinimumOrderQuantity != null) ? vpcViewModel.MinimumOrderQuantity : 1);
    self.MinimumOrderUnit = ko.observable((vpcViewModel != null && vpcViewModel.MinimumOrderUnit != null) ? vpcViewModel.MinimumOrderUnit : 'P');
    self.MinimumOrderUnitText = ko.computed(function () {
        if (self.MinimumOrderUnit() === 'P') {
            return 'by the Piece';
        }
        else if (self.MinimumOrderUnit() === 'L') {
            return 'by the Lot';
        }
        //else if (self.MinimumOrderUnit() === 'Q') {
        //    return 'by the Piece in Fixed Lot';
        //}
        return '';
    });
    self.ManufacturersBarcode = ko.observable((vpcViewModel != null && vpcViewModel.ManufacturersBarcode != null) ? vpcViewModel.ManufacturersBarcode : '');

    self.CanNotDropship = ko.observable((vpcViewModel != null && vpcViewModel.CanNotDropship != null) ? vpcViewModel.CanNotDropship : '');
    self.CanDropship = ko.observable((vpcViewModel != null && vpcViewModel.CanNotDropship != null) ? !vpcViewModel.CanNotDropship : false);

    self.LeadTimeInDays = ko.observable((vpcViewModel != null && vpcViewModel.LeadTimeInDays != null) ? vpcViewModel.LeadTimeInDays : '');

    self.OnOrder = ko.observable((vpcViewModel != null && vpcViewModel.OnOrder != null) ? vpcViewModel.OnOrder : '0');
    self.QuantityPerLot = ko.observable((vpcViewModel != null && vpcViewModel.QuantityPerLot != null) ? vpcViewModel.QuantityPerLot : '');
    self.LotPartNumber = ko.observable((vpcViewModel != null && vpcViewModel.LotPartNumber != null) ? vpcViewModel.LotPartNumber : '');
    self.LotBarcode = ko.observable((vpcViewModel != null && vpcViewModel.LotBarcode != null) ? vpcViewModel.LotBarcode : '');
    self.LotDescription = ko.observable((vpcViewModel != null && vpcViewModel.LotDescription != null) ? vpcViewModel.LotDescription : '');
    self.MinLineOrderAmount = ko.observable((vpcViewModel != null && vpcViewModel.MinLineOrderAmount != null) ? vpcViewModel.MinLineOrderAmount : '');
    self.ProductId = ko.observable((vpcViewModel != null && vpcViewModel.ProductId != null) ? vpcViewModel.ProductId : -1);
    self.VendorProductId = ko.observable((vpcViewModel != null && vpcViewModel.VendorProductId != null) ? vpcViewModel.VendorProductId : '');
    self.Id = ko.observable((vpcViewModel != null && vpcViewModel.Id != null) ? vpcViewModel.Id : -1);  ///For VendorProductPurchasingRule Id
    self.CreateTs = ko.observable((vpcViewModel != null && vpcViewModel.CreateTs !== null) ? JSONDateWithTime(vpcViewModel.CreateTs) : '');
    self.CreateUser = ko.observable((vpcViewModel != null && vpcViewModel.CreateUser != null) ? vpcViewModel.CreateUser : '');
    self.DateLastCostUpdate = ko.observable((vpcViewModel != null && vpcViewModel.DateLastCostUpdate !== null) ? FormattedDate(vpcViewModel.DateLastCostUpdate) : '');
    self.DateCreated = ko.observable((vpcViewModel != null && vpcViewModel.DateCreated !== null) ? JSONDateWithTime(vpcViewModel.DateCreated) : '');
    self.DateLastUpdated = ko.observable((vpcViewModel != null && vpcViewModel.DateLastUpdated !== null) ? JSONDateWithTime(vpcViewModel.DateLastUpdated) : '');
    //SG-517 fix: Vendor Product Edit - Default Unit of Measure to Each
    self.Uom = ko.observable((vpcViewModel != null && vpcViewModel.Uom !== null) ? vpcViewModel.Uom : "Each");
    self.Spq = ko.observable((vpcViewModel != null && vpcViewModel.Spq !== null) ? vpcViewModel.Spq : "");

    self.Qoh = ko.observable((vpcViewModel != null && vpcViewModel.Qoh !== null) ? vpcViewModel.Qoh : "");

    self.ShelfLife = ko.observable((vpcViewModel != null && vpcViewModel.ShelfLife !== null) ? vpcViewModel.ShelfLife : "");

    //SG-555: Added QohUpdatedOn property for UI (works both for product-catalog and vendor-product-catalog screen)
    self.QohUpdatedOn = ko.observable((vpcViewModel != null && vpcViewModel.QohUpdatedOn !== null) ? FormattedDate(vpcViewModel.QohUpdatedOn) : "");
   
    self.WorkflowId = ko.observable((vpcViewModel != null && vpcViewModel.WorkflowId != null) ? vpcViewModel.WorkflowId : '');
    self.StepId = ko.observable((vpcViewModel != null && vpcViewModel.StepId != null) ? vpcViewModel.StepId : '');

    //for purchasing level
    self.PurchasingLevels = ko.observableArray((vpcViewModel != null && vpcViewModel.PurchasingLevels != null && vpcViewModel.PurchasingLevels.length > 0) ? VendorProductPurchaseRuleLevelVMList(vpcViewModel.PurchasingLevels) : []);
    self.HasPurchasingLevelChanged = ko.observable(false);
    self.IsQohdateChanged = ko.observable(false);
    self.IsCostDateChanged = ko.observable(false);
    self.NonCancelNonReturn = ko.observable((vpcViewModel != null && vpcViewModel.NonCancelNonReturn != null) ? vpcViewModel.NonCancelNonReturn : false);
    self.CoreCharge = ko.observable((vpcViewModel != null && vpcViewModel.CoreCharge != null) ? FormatAmount(vpcViewModel.CoreCharge, 2) : '');
    self.addPurchasingLevel = function () {
        for (var i = 0; i < self.PurchasingLevels().length; i++) {
            var quantityFrom = self.PurchasingLevels()[i].QuantityFrom();
            //var quantityTo = self.PurchasingLevels()[i].QuantityTo();
            if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                alertmsg('red', 'Quantity should be greater than 1');
                return;
            }

            if (showPurchasingLevel === true) {
                var marginPercent = self.PurchasingLevels()[i].GrossMarginPercent();
                var marginAmount = self.PurchasingLevels()[i].GrossMarginAmount();

                if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                    alertmsg('red', 'Margin %  should be greater than 0');
                    return;
                }

                if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                    alertmsg('red', 'Margin $  should be greater than 0');
                    return;
                }
            }

        }
        if (self.PurchasingLevels().length > 1) {
            var quantityFromCurrent = self.PurchasingLevels()[self.PurchasingLevels().length - 1].QuantityFrom();
            var quantityFromPrevious = self.PurchasingLevels()[self.PurchasingLevels().length - 2].QuantityFrom();

            if (quantityFromPrevious != null && quantityFromCurrent != null && parseFloat(quantityFromPrevious) >= parseFloat(quantityFromCurrent)) {
                alertmsg('red', 'Current "Quantity" should be greater than the previous "Quantity"');
                return;
            }
        }

        self.PurchasingLevels.push(new VendorProductPurchaseRuleLevelVM());

        $("#vpc-purchasinglevel input[type=text]")
            .change(purchasingLevelChange);

        rebuilttooltip();
    };

    self.removePurchasingLevel = function (purchasingLevel) {
        self.PurchasingLevels.remove(purchasingLevel);
        self.HasPurchasingLevelChanged(true);
    };

    //computed functions
    self.IsQplEnabled = ko.computed(function () {
        if (self.OrderBy() === 'L' || self.OrderBy() === 'Q') {
            //if (typeof vpcViewModel !== 'undefined' && vpcViewModel !== null && vpcViewModel.QuantityPerLot !== null) {
            //    if (self.QuantityPerLot() != null && self.QuantityPerLot().toString().trim() === "")
            //        self.QuantityPerLot(vpcViewModel.QuantityPerLot);
            //}
            ////else {
            ////    self.QuantityPerLot(FormatAmount(self.CostPerLot() / self.Cost()));
            ////}
            return true;
        }

        //self.QuantityPerLot(""); //change1111
        return false;
    });

    self.IsByLot = ko.computed(function () {

        if (self.OrderBy() === 'L' && self.QuantityPerLot() !== "") {
            //if (typeof vpcViewModel !== 'undefined' && vpcViewModel !== null && vpcViewModel.QuantityPerLot !== null) {

            //    if (self.QuantityPerLot() != null && self.QuantityPerLot().toString().trim() === "")
            //        self.QuantityPerLot(vpcViewModel.QuantityPerLot);
            //    if (self.LotDescription() != null && self.LotDescription().toString().trim() === "")
            //        self.LotDescription(vpcViewModel.LotDescription);
            //    if (self.LotPartNumber() != null && self.LotPartNumber().toString().trim() === "")
            //        self.LotPartNumber(vpcViewModel.LotPartNumber);
            //}
            return true;
        }
        else if (self.OrderBy() === 'L') { //changed1111
            return true;
        }

        ////self.QuantityPerLot("");
        self.LotDescription("");
        self.LotPartNumber("");

        return false;
    });

    self.CostPerLot = ko.observable((vpcViewModel != null && vpcViewModel.CostPerLot != null) ? vpcViewModel.CostPerLot : '');
  

    //self.CostPerLot = ko.computed(function () {
    //    if (self.OrderBy() === "L" && self.QuantityPerLot() && self.Cost()) {
    //        return FormatAmount(self.QuantityPerLot() * self.Cost());
    //    }

    //    return "";
    //});

    self.CostPerLot.subscribe(function (val) {
        if (isNaN(val) || val === "" || val === 0) {
            self.CostPerLot("");
            return;
        }

        if (isNaN(parseFloat(val)) || parseFloat(val) === 0) {
            self.CostPerLot("");
            return;
        }

        if (isNaN(self.QuantityPerLot()) || self.QuantityPerLot() === "" || self.QuantityPerLot() === 0)
            return;

        var value = val / self.QuantityPerLot();
        if (isNaN(value))
            return;
        if (value === 0)
            return;

        if ((vpprChangeLevel.match(/,/g) || []).length === 2 || vpprChangeLevel.indexOf("CPL,") > -1) {
            vpprChangeLevel = "";
            return;
        }

        vpprChangeLevel += "CPL,";

        var cost = FormatAmount(val / self.QuantityPerLot());
        if (cost !== FormatAmount(self.Cost())) {
            if (vpprChangeLevel.indexOf("VPPRCOST,") === -1 && hasUserChangedVpprCost === true) {
                hasUserChangedVpprCost = false;
                self.Cost(cost);
            }
        }

    });

    self.QuantityPerLot.subscribe(function (val) {

        if (isNaN(val) || val === "" || val === 0) {
            return;
        }

        if (isNaN(self.Cost()) || self.Cost() === "" || self.Cost() === 0)
            return;

        var value = val * self.Cost();
        if (isNaN(value))
            return;
        if (value === 0)
            return;

        //fix: show cost per lot only when 'by the Lot' is selected
        if (self.IsByLot()) {
            if ((vpprChangeLevel.match(/,/g) || []).length === 2 || vpprChangeLevel.indexOf("QPL,") > -1) {
                vpprChangeLevel = "";
                return;
            }

            vpprChangeLevel += "QPL,";
            var costPerLot = FormatAmount(value);
            if (costPerLot !== FormatAmount(self.CostPerLot())) {
                if (vpprChangeLevel.indexOf("CPL,") === -1) {
                    self.CostPerLot(costPerLot);
                }
            }
        }
    });

    self.IsDisabledForEdit = ko.computed(function () {
        //for create allow vendor dropdown to be editable, for edit don't allow vendor dropdown to be editable
        return self.ProductId() !== -1;
        //return (productCatalogVm != null && productCatalogVm.Id !== null && productCatalogVm.Id !== "" && ( == null && vpcViewModel.ProductId === false));
    });

    //Save click
    self.save = function (options) {
        //SG-803 setting flag to display pop-up on orderby change
        OrderByValueChanged = false;

        isValid = self.validate(options);
        if (!isValid)
            return;

        if (self.Id() !== "" && self.Id() !== "0" && self.Id() !== "-1" && self.Id() !== 0 && self.Id() !== -1) {
            self.edit(options);
        } else {
            self.create(options);
        }
    }

    self.validate = function (options) {
        //options start (validation)

       
        var bypassCostRangeCheck = false;
        //SG-808
        var bypassValidateProductAmountCheck = false;

        if (options) {
            //if (options.bypassSpqAndQplCheck)
            //    bypassSpqAndQplCheck = true;
            if (options.bypassCostRangeCheck)
                bypassCostRangeCheck = true;
            //SG-808
            if (options.bypassValidateProductAmountCheck)
            {
                bypassValidateProductAmountCheck = true;
            }
        }

        //if (bypassSpqAndQplCheck !== true) {
        //    isValid = validateSpqAndQpl();
        //    if (!isValid)
        //        return false;
        //}

        //if (bypassCostRangeCheck !== true) {
        //    isValid = validateCostRange();
        //    if (!isValid)
        //        return false;
        //}
        if (bypassCostRangeCheck !== true) {
            isValid =  costpricevalidation(viewModel);
            if (!isValid)
                return false;
        }
        
        //if (bypassValidateProductAmountCheck != true)
        //{
        //    var minLineAmount = $('#MinLineOrderAmount').val();
           
        //    if (minLineAmount !== undefined)
        //        isValid = validateCostAndMinQtyWithMinLineOrder(minLineAmount);
        //    else
        //        isValid = validateCostAndMinQtyWithMinLineOrder(vendorProductCatalogVmJson.MinLineOrderAmount);

        //    if (!isValid)
        //        return false;

        //}

        //options end (validation)
        return true;
    }

    //Edit click
    self.edit = function (options) {
        sessionOption = options;
        //SG-803 
        if (OrderByValueChanged == true) {
            optionModelForVendorProduct = options; 
            $("#" + PopIdForVendorProduct).modal("show");
        }

        else {
            //console.log(ko.toJSON(self));
            var form = $('#__AjaxAntiForgeryForm');
            var token = $('[name=__RequestVerificationToken]').val();
            //console.log(token);
            var data = ko.toJSON(self);

            if (bypassNegativeMargin === false) {
                validateNegativeMarginFromVPEditor(viewModel);
                //if (saleMarginValidation == true)
                //    alertmsg('yellow','Sale Price Margin is greater than Price Margin !!');
                if (newMargin < 10) {
                    SGloadingRemove();
                    $('#MarginNegativeDueToVPCostModal').modal('show');
                    return;
                }

                if (newMarginLevel <= 0) {
                    SGloadingRemove();
                    $('#VPMarginLeveNegativeDueToCostModal').modal('show');
                    return;
                }

               

            }

            if (saleMargin >= newMargin) {
                alertmsg('red', 'Sale Price Margin is greater than Price Margin. Please try again after adjusting margin in Product')
                //$('#SaleMarginModal').modal('show');
                return;
            }

            var isValid = self.validate(options);
            if (!isValid)
                return false;

            

            // Ajax call
            $.ajax({
                type: 'POST',
                url: editUrl,
                cache: false,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: data,
                beforeSend: function () {
                    //$("#btnsave").attr('disabled', true);
                    SGloading('Loading');
                },
                success: function (response) {

                    if (response != null && response.Success === true) {
                        alertmsg("green", response.Message);

                        if (response.Data)
                            window.location.href = response.Data;
                        var returnUrl = getQueryStringByName("returnUrl");
                        if (returnUrl)
                            window.location.href = returnUrl;
                    }
                    else if (response != null && response.Success === false) {
                        alertmsg("red", response.Message);
                    } else {
                        alertmsg("red", "An Error occurred");
                    }

                },
                complete: function () {
                    //$("#btnsave").attr('disabled', false);
                    SGloadingRemove();
                },
                error: function (xhr, status, error) {
                    //reset variables here
                    alert(status + ' ' + error);
                }
            });
        }
        // To actually transmit to server as a regular form post, write this: ko.utils.postJson($("form")[0], self.PurchasingLevels);
    };

    self.Qoh.subscribe(function (val) {
        var date = GetTodayDate();
        self.QohUpdatedOn(date);
        $('#QohUpdatedOn').val(date);

    });



    $("#btnSaveConfirmationDialogueForVendorProductYes").click(function () {


        var options = optionModelForVendorProduct;
        $("#" + PopIdForVendorProduct).modal("hide");

        //console.log(ko.toJSON(self));
        var form = $('#__AjaxAntiForgeryForm');
        var token = $('[name=__RequestVerificationToken]').val();
        //console.log(token);

        var isValid = self.validate(options);
        if (!isValid)
            return false;

        var data = ko.toJSON(self);

        // Ajax call
        $.ajax({
            type: 'POST',
            url: editUrl,
            cache: false,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {

                if (response != null && response.Success === true) {
                    alertmsg("green", response.Message);

                    if (response.Data)
                        window.location.href = response.Data;
                    var returnUrl = getQueryStringByName("returnUrl");
                    if (returnUrl)
                        window.location.href = returnUrl;
                }
                else if (response != null && response.Success === false) {
                    alertmsg("red", response.Message);
                } else {
                    alertmsg("red", "An Error occurred");
                }

            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });

    })

    $("#btnYesMarginNegativeDueToVPCostModal").click(function () {
        $('#btnNoMarginNegativeDueToVPCostModal').click();
        bypassNegativeMargin = true;
        newMargin = 25;
        self.edit(sessionOption);
    });

    $("#btnYesVPMarginLevelNegativeCostModal").click(function () {
        $('#btnNoVPMarginLevelNegativeCostModal').click();
        bypassNegativeMargin = true;
        self.edit(sessionOption);
    })
    
    $('#QohUpdatedOn').change(function () {
        self.IsQohdateChanged(true);
    });
    $('#DateLastCostUpdate').change(function () {
        self.IsCostDateChanged(true);
    });

    //Create click
    self.create = function (options) {
        //console.log(ko.toJSON(self));
        var form = $('#__AjaxAntiForgeryForm');
        var token = $('[name=__RequestVerificationToken]').val();

        var isValid = self.validate(options);
        if (!isValid)
            return false;

        dataAlreadyCreated = 0;
        associatewithExisting = 0;
        var workflowId = getQueryStringByName("workflowId");
        var stepId = getQueryStringByName("stepId");
        var stagingId = getQueryStringByName("stagingId");
        var guid = getQueryStringByName("guid");
        var logUserUploadId = getQueryStringByName("logUserUploadId");
        var errorType = getQueryStringByName("errorType");
        //console.log(token);

        var data = ko.toJSON({
            viewModel: self,
            workflowId: workflowId,
            stepId: stepId,
            stagingId: stagingId,
            guid: guid,
            logUserUploadId: logUserUploadId,
            errorType: errorType,
            alreadyExist: dataAlreadyCreated,
            assotiateWithExisting: associatewithExisting
        });

        //data.viewModel.DateLastCostUpdate = $("#DateLastCostUpdate").val();
        //data.viewModel.QohUpdatedOn = $("#QohUpdatedOn").val();

        // Ajax call
        $.ajax({
            type: 'POST',
            url: createUrl,
            cache: false,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {
                if (response != null && response.Success === true) {
                    alertmsg("green", response.Message);
                    if (response.Data)
                        window.location.href = response.Data;
                    var returnUrl = getQueryStringByName("returnUrl");
                    if (returnUrl)
                        window.location.href = returnUrl;
                }
                else if (response != null && response.Success === false) {
                    if (response.Message === "AlreadyExist") {
                        dataAlreadyCreated = 1;
                        alertmsg("red", "VendorProduct with same VendorPartNumber already exist for selected vendor");
                        //  $("#btnVendorProductUpdate").click();
                    }
                    else
                        alertmsg("red", response.Message);
                }
                   

                else {
                    alertmsg("red", "An Error occurred");
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                alert(status + ' ' + error);
            }
        });
        // To actually transmit to server as a regular form post, write this: ko.utils.postJson($("form")[0], self.PurchasingLevels);
    };
    //$('#Cost').change(function () {
    //    vpprChangeLevel = "";
    //    hasUserChangedVpprCost = true;
    //    var date = GetTodayDate();
    //    self.DateLastCostUpdate(date);
    //    $('#DateLastCostUpdate').val(date);
    //});
};




$('#CostPerLot').change(function () {
    vpprChangeLevel = "";
});

$('#QuantityPerLot').change(function () {
    vpprChangeLevel = "";
});

$('#OrderBy').change(function () {
   
    var orderBy = $('#OrderBy').val();
    if (orderBy === "P") {
        //quantity per lot, lot description, lot part number, cost per lot --> blank
        //if (viewModel.vendorProduct) {
        //    viewModel.vendorProduct.QuantityPerLot('');
        //    viewModel.vendorProduct.LotDescription('');
        //    viewModel.vendorProduct.LotPartNumber('');
        //    viewModel.vendorProduct.CostPerLot('');
        //} else {
        //    viewModel.QuantityPerLot('');
        //    viewModel.LotDescription('');
        //    viewModel.LotPartNumber('');
        //    viewModel.CostPerLot('');
        //}

        $('#QuantityPerLot').val('').change();
        $('#LotDescription').val('').change();
        $('#LotPartNumber').val('').change();
        $('#CostPerLot').val('').change();
    }
    else if (orderBy === "Q") {
        //lot description, lot part number, cost per lot --> blank

        //if (viewModel.vendorProduct) {
        //    viewModel.vendorProduct.LotDescription('');
        //    viewModel.vendorProduct.LotPartNumber('');
        //    viewModel.vendorProduct.CostPerLot('');
        //} else {
        //    viewModel.LotDescription('');
        //    viewModel.LotPartNumber('');
        //    viewModel.CostPerLot('');
        //}

        $('#LotDescription').val('').change();
        $('#LotPartNumber').val('').change();
        $('#CostPerLot').val('').change();
    }
    else if (orderBy === "L") {
        var qpl = $('#QuantityPerLot').val();
        var cost = $('#Cost').val();
        var cpl = qpl * cost;
        if (!isNaN(cpl) || cpl > 0) {
            //if (viewModel.vendorProduct) {
            //    viewModel.vendorProduct.CostPerLot(FormatAmount(cpl));
            //} else {
            //    viewModel.CostPerLot(FormatAmount(cpl));

            //}
            $('#CostPerLot').val(FormatAmount(cpl)).change();
        }
    }
});

$('#vpc-purchasing').on('change', '#CostPerLot', function () {
    var val = $('#CostPerLot').val();

    //var quantityPerLot = viewModel.vendorProductCatalog.QuantityPerLot();

    var model;
    if (viewModel.vendorProduct) {
        //case from productcatalog: vendorproduct is child of product viewmodel
        model = viewModel.vendorProduct();
    } else {
        //case from vendorproductcatalog: viewmodel is vpc
        model = viewModel;
    }

    var quantityPerLot = model.QuantityPerLot();

    var cost = parseFloat(val) / parseFloat(quantityPerLot);

    if (!isNaN(cost) && parseFloat(cost) !== 0)
        model.Cost(FormatAmount(cost));

});

$('#vpc-purchasing').on('change', '#QuantityPerLot', function () {
    var val = $('#QuantityPerLot').val();

    var model;
    if (viewModel.vendorProduct) {
        //case from productcatalog: vendorproduct is child of product viewmodel
        model = viewModel.vendorProduct();
    } else {
        //case from vendorproductcatalog: viewmodel is vpc
        model = viewModel;
    }

    var cost = model.Cost();

    //calculate and set costper lot for 'by the Lot'
    if (model.IsByLot()) {
        var costPerLot = FormatAmount(val * cost);
        if (costPerLot !== model.CostPerLot()) {
            model.CostPerLot(costPerLot);
        }
    }
});

// $('#OrderBy').change(function() {
$('#vpc-purchasing').on('change', '#OrderBy', function () {

    var val = $('#OrderBy').val();
    if (val === "")
        return;

    else
    {
        //SG-803
        OrderByValueChanged = true;
        count = 1; 

     if (val === "L") {
        //nothing to clear
    }
    else if (val === "P") {
        //clear qtyperlot and costperlot
        $('#QuantityPerLot').val('').change();
        $('#CostPerLot').val('').change();
        //execute below lines two times to clear dropdown
        $('#lotDescriptionDrop').select2().val('');
        $('#lotDescriptionDrop').select2().val('');
    }
    else if (val === "Q") {
        //clear costperlot
        $('#CostPerLot').val('').change();
        //execute below lines two times to clear dropdown
        $('#lotDescriptionDrop').select2().val('');
        $('#lotDescriptionDrop').select2().val('');
    }
    }

    var model;
    if (viewModel.vendorProduct) {
        //case from productcatalog: vendorproduct is child of product viewmodel
        model = viewModel.vendorProduct();
    } else {
        //case from vendorproductcatalog: viewmodel is vpc
        model = viewModel;
    }

    var isByLot = model.IsByLot();
    if (!isByLot) {
        $('#LotPartNumber').val('').change();
    }

    var isQplEnabled = model.IsQplEnabled();
    if (!isQplEnabled) {
        $('#QuantityPerLot').val('').change();
    }
});

var purchasingLevelChange = function() {
    var model;
    if (viewModel.vendorProduct) {
        //case from productcatalog: vendorproduct is child of product viewmodel
        model = viewModel.vendorProduct();
    } else {
        //case from vendorproductcatalog: viewmodel is vpc
        model = viewModel;
    }
    model.HasPurchasingLevelChanged(true);
};

$(function() {
    $("#vpc-purchasinglevel input[type=text]")
        .change(purchasingLevelChange);
});

function getMinLineOrderAmountForVendor(value)
{
    $.ajax({
        type: 'GET',
        url: GetUrl,
       // cache: false,
      //  contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        data: { vendorId: value },
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
           // SGloading('Loading');
        },
        success: function (response) {
            vendorProductCatalogVmJson.MinLineOrderAmount = response.Data;
            $('#MinLineOrderAmount').val(response.Data);
            $('#VendorProduct_MinLineOrderAmount').val(response.Data);
            //validateCostAndMinQtyWithMinLineOrder(response.Data, viewModel.Cost());
            var vendorProductCost = parseFloat(viewModel.vendorProduct().Cost()) || 0;
            if(vendorProductCost !== 0)
                validateCostAndMinQtyWithMinLineOrder(response.Data, vendorProductCost);

        }
    });
}

$('#MinimumOrderQuantity').focusout(function () {
    var minLineOrderAmountforProductVendor = $('#MinLineOrderAmount').val();
    if (minLineOrderAmountforProductVendor === undefined) {

        minLineOrderAmountforProductVendor = vendorProductCatalogVmJson.MinLineOrderAmount;
    }
    validateCostAndMinQtyWithMinLineOrder(minLineOrderAmountforProductVendor, viewModel.Cost());

});

$('#vpc-purchasing').on('change', '#MinimumOrderUnit', function () {

    var minLineOrderAmountforProductVendor = $('#MinLineOrderAmount').val();
    if (minLineOrderAmountforProductVendor === undefined) {

        minLineOrderAmountforProductVendor = vendorProductCatalogVmJson.MinLineOrderAmount;
    }
    validateCostAndMinQtyWithMinLineOrder(minLineOrderAmountforProductVendor, viewModel.Cost());
})


