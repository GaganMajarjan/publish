﻿$(document).ready(function () {

    $('#MappingDataTable').dataTable({
        "deferRender": true,
        "bServerSide": true,
        "bSort": false,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlGetMappedDataTable,
        "bProcessing": true,
        "scrollX": true,
        "fnPreDrawCallback": function () {
            // gather info to compose a message
            var ob = this;
            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
            //SGtableloading('fa-plane', '', this);


        },
        "fnDrawCallback": function () {
            // in case your overlay needs to be put away automatically you can put it here
            var divid = this.closest('.dataTables_wrapper').attr('id');
            SGtableloadingRemove(divid)
        }

    });
});