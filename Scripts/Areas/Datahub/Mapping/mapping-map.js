﻿

if (mappingDataViewBag != null && mappingDataViewBag !== "")
    mappingDataViewBag = fixJsonQuote(mappingDataViewBag);
else
    mappingDataViewBag = null;
//console.log(mappingDataViewBag);
//console.log(mappingDataViewBag);


if (productMatchAgainstTableColumnListViewBag != null && productMatchAgainstTableColumnListViewBag !== "")
    productMatchAgainstTableColumnListViewBag = fixJsonQuote(productMatchAgainstTableColumnListViewBag);
else
    productMatchAgainstTableColumnListViewBag = null;
//console.log(productMatchAgainstTableColumnListViewBag);


if (transformationRuleListViewBag != null && transformationRuleListViewBag !== "")
    transformationRuleListViewBag = fixJsonQuote(transformationRuleListViewBag);
else
    transformationRuleListViewBag = null;
//console.log(transformationRuleListViewBag);
//console.log(importSourceColumnListViewBag);


if (importSourceColumnListViewBag != null && importSourceColumnListViewBag !== "")
    importSourceColumnListViewBag = fixJsonQuote(importSourceColumnListViewBag);
else
    importSourceColumnListViewBag = null;

//console.log(importSourceColumnListViewBag);


if (destinationTableColumnListViewBag != null && destinationTableColumnListViewBag !== "")
    destinationTableColumnListViewBag = fixJsonQuote(destinationTableColumnListViewBag);
else
    destinationTableColumnListViewBag = null;

//console.log(destinationTableColumnListViewBag);


var productMatchAgainstTableColumnList = [];
if (productMatchAgainstTableColumnListViewBag != null) {
    $.each(productMatchAgainstTableColumnListViewBag, function(index, val) {
        productMatchAgainstTableColumnList.push({
            Text: val.Text,
            Value:val.Value
        });
  
    });
}

var transformationRuleList = [];
if (transformationRuleListViewBag != null) {
    $.each(transformationRuleListViewBag, function (index, val) {
        transformationRuleList.push({
            Text: val.Text,
            Value: val.Value
        });

    });
}

var importSourceColumnList = [];
if (importSourceColumnListViewBag != null) {
    $.each(importSourceColumnListViewBag, function (index, val) {
        importSourceColumnList.push({
            Text: val.Text,
            Value: val.Value
        });

    });
}

var destinationTableColumnList = [];

if (destinationTableColumnListViewBag != null) {
    $.each(destinationTableColumnListViewBag, function (index, val) {
        destinationTableColumnList.push({
            Text: val.Text,
            Value: val.Value
        });

    });
}


var MappingViewModel = function () {


    var self = this;
    self.ImportSource = ko.observable(importSourceId);
    self.DestinationTable = ko.observable(destinationTableId);
    self.MappingLevels = ko.observableArray([]);
    
    self.mapButton = function(form) {
        self.MappingLevels([]);

        var importSourceId = self.ImportSource();
        var destinationId = self.DestinationTable();
        if (importSourceId !== "" && destinationId !== "" && importSourceId != null && destinationId != null) {

            $.ajax({
                url: urlMapSourceToDestination,
                data: {
                    sourceId: importSourceId, destinationId: destinationId,
                    workflowId: workflowId,
                    stepId: stepId
                },
                type: 'POST',
                cache: false,
                success: function (data) {
                    if (data !== "ERROR")
                        window.location.href = data;
                    else {
                        alertmsg("red", errorOccurredMessage);
                    }
                }
            });


        }
        
    }

    self.saveMappingButton = function(form) {
            //console.log(ko.toJSON(self));
            // Ajax call

            $.ajax({
                type: 'POST',
                url: urlSaveMapping,
                cache: false,
                contentType: 'application/json; charset=utf-8',
                data: ko.toJSON({
                    vm: self,
                    workflowId : workflowId, 
                    stepId : stepId
        }),
            dataType: 'json',
            beforeSend: function () {
                SGloading('Loading');
            },
            success: function (response) {
                if (response != null && response.Status === true) {

                    if (response.Message === "true") {
                        window.location.href = response.Data;
                    } else {
                    alertmsg('green', response.Message);
                    $('#vendor-list-link').show();
                    }
                }
                else if (response != null && response.Status === false) {
                    alertmsg('red', response.Message);
                }
                else {
                    alertmsg('red', errorOccurredMessage);
                }
            },
            complete: function () {
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
               alertmsg("red", status + ' ' + error);
            }
        });
    }

    self.removeMappingLevel = function(mappingLevel) {
        self.MappingLevels.remove(mappingLevel);
    }

    self.addMoreMappingLevel = function(form) {
        self.MappingLevels.push(new MappingLevelViewModel());

        $("select").select2({ width: '100%' });

        $("body").tooltip({ selector: '[data-toggle="tooltip"]' });
    
    }

    self.viewDataSourcebtn = function () {

        window.open(urlViewDataSourceFile + "?FileId=" + fileId);
    }


    self.continueWithImportDivYesButton = function () {

        SGloading('Scheduling ...');
        $.ajax({
            url: urlScheduleFileImport,
            data: { fileId: fileId },
            type: 'POST',
            cache: false,
            success: function (response) {
                
                if (response != null && response.Status === true) {

                    $("label.messagebatch").replaceWith("<label class='messagebatch'>" + response.Message + "</label>");
                    SGloadingRemove();

                    //alertmsg("green", response.Message);
                    $("#btnMappingCompleteModelOpen").click();

                }
                else if (response != null && response.Status === false) {
                    SGloadingRemove();
                    alertmsg("red", response.Message);
                }

                else {
                    SGloadingRemove();
                    alertmsg("red", errorOccurredMessage);
                }
                
            },
            error: function (err) {
                SGloadingRemove();
                alertmsg("red", errorOccurredMessage);

            }
        });
    }


    self.continueWithImportDivNo = function () {

    }

    self.continueWithImportButton = function() {
        SGloading('Scheduling ...');
        $.ajax({
            url: urlScheduleFileImport,
            data: { fileId: fileId },
            type: 'POST',
            cache: false,
            success: function (response) {

                if (response != null && response.Status === true) {

                    $("label.messagebatch").replaceWith("<label class='messagebatch'>" + response.Message + "</label>");
                    SGloadingRemove();

                    //alertmsg("green", response.Message);
                    $("#btnMappingCompleteModelOpen").click();

                }
                else if (response != null && response.Status === false) {
                    SGloadingRemove();
                    alertmsg("red", response.Message);
                }

                else {
                    SGloadingRemove();
                    alertmsg("red", errorOccurredMessage);
                }

            },
            error: function (err) {
                SGloadingRemove();
                alertmsg("red", errorOccurredMessage);

            }
        });
    }

};

var MappingLevelViewModel = function(vm) {


    var self = this;

    self.ImportSourceColumnSelected = ko.observable((vm != null && vm.ImportSourceColumnSelected != null)?vm.ImportSourceColumnSelected:"");
    self.SuggestionIdentifierSelected = ko.observable((vm != null && vm.ImportSourceColumnSelected != null)?vm.SuggestionIdentifierSelected:"");
    self.TransformationRuleSelected = ko.observable((vm != null && vm.TransformationRuleSelected != null)?vm.TransformationRuleSelected:"");
    self.DestinationTableColumnSelected = ko.observable((vm != null && vm.DestinationTableColumnSelected != null) ? vm.DestinationTableColumnSelected : "");

    self.suggestionIdentifierList = ko.observableArray(productMatchAgainstTableColumnList);
    self.transformationRuleList = ko.observableArray(transformationRuleList);
   
    //self.importSourceColumnList = ko.observableArray(importSourceColumnList);
    //self.destinationTableColumnList = ko.observableArray(destinationTableColumnList);

    self.IsDirectImport = ko.observable((vm != null && vm.IsDirectImport != null)?vm.IsDirectImport:false);
    self.IsTransformationRuleUsed = ko.observable((vm != null && vm.IsTransformationRuleUsed != null) ? vm.IsTransformationRuleUsed : false);

    self.IsTransformationRuleUsed.subscribe(function(val) {
        if (val === false) {
            self.TransformationRuleSelected('');
        }
    });
};

var viewModel = new MappingViewModel();
ko.applyBindings(viewModel);

if (mappingDataViewBag != null) {
    $.each(mappingDataViewBag, function (index, value) {
        viewModel.MappingLevels.push(new MappingLevelViewModel(value));
    });
}

$("select").select2({width:'100%'});

if (isBackgroundImportScheduled === "True") {
    var importSourceName = $("#impsrc option:selected").text();
    var labelText = $('<div>').html(backgroundImportScheduledMessage).text().format(importSourceName);

    $("#backgroundImportScheduledDivForLabel").show();
    $("#backgroundImportScheduledLabel").show();
    $("#backgroundImportScheduledLabel").text(labelText);
    $("#backgroundImportScheduledDiv").css('pointer-events', 'none');

}

$('#btnCancel').click(function () {
    window.location = urlMapping;
});