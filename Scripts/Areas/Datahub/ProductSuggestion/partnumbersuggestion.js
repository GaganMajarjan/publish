﻿
$(document).ready(function () {
    //slickTable();

    var winheight = $(window).height()
    var fhoffset = $('.sg-table-sleek-wrap').offset();
    $('.sg-table-sleek-wrap').height((Math.floor(winheight - fhoffset.top - 180)) / 2);
    $('.sg-adj-nav').height(winheight - 100);

    //$("#dialog-product-catalog").modal('hide');
    //$("#dialog-vendor-product-catalog").modal('hide');

    try {

        //$("#suggestionProductCatalogGrid").DataTable();
        //$('#suggestionVendorProductCatalogGrid').DataTable();

        //var table = $('#suggestionProductCatalogGrid').DataTable();
        //new $.fn.dataTable.FixedHeader(table);

        //var table = $('#suggestionVendorProductCatalogGrid').DataTable();
        //new $.fn.dataTable.FixedHeader(table);

        // Suggestion view model
        var suggestionViewModel = function () {
            var self = this;

            // List declaration
            self.matchSuggestionList = ko.mapping.fromJS([]);
            self.productCatalogList = ko.mapping.fromJS([]);
            self.vendorProductCatalogList = ko.mapping.fromJS([]);

            // Hide PC & VPC by default
            self.showProductCatalog = ko.observable(false);
            self.showVendorProductCatalog = ko.observable(false);

            // Load matched suggestion list
            self.matchSuggestionList = ko.mapping.fromJS(skuMatchedSuggestionAggData);
            //self.refreshMatchSuggestionList();

            // For checkbox
            self.productCatalogCheckedIdList = ko.observableArray([]);
            self.vendorProductCatalogCheckedIdList = ko.observableArray([]);

            self.productCatalogMasterCheck = ko.observable(false);
            self.productCatalogMasterCheck.subscribe(function (val) {
                ko.utils.arrayForEach(self.productCatalogList(), function (item) {
                    item.IsChecked(val);
                });
            });
            self.vendorProductCatalogMasterCheck = ko.observable(false);
            self.vendorProductCatalogMasterCheck.subscribe(function (val) {
                ko.utils.arrayForEach(self.vendorProductCatalogList(), function (item) {
                    item.IsChecked(val);
                });
            });

            // For selected data

            self.selectedData = ko.observable();

            self.productCatalogItemClick = function (val) {
                var destinationColumnName = val.DestinationColumnName;
                var importSourceColumnId = val.ImportSourceColumnId;

                self.selectedData(destinationColumnName);
                self.showProductCatalog(true);
                self.showVendorProductCatalog(false);
                self.productCatalogCheckedIdList([]);
                self.vendorProductCatalogCheckedIdList([]);

                //      //console.log(skuType);

                // Ajax call
                $.ajax({
                    type: 'POST',
                    url: urlProductCatalogDetails,
                    cache: false,
                    //contentType: 'application/json; charset=utf-8',
                    data: { 'destinationColumnName': destinationColumnName, 'vendorId': vendorId, 'skuType': skuType },
                    dataType: 'json',
                    beforeSend: function () {
                        //$("#btnsave").attr('disabled', true);
                        SGloading('Loading');
                    },
                    success: function (response) {

                        response = JSON.parse(response);
                        try {
                            //--------------------------------------
                            //Scenario: (do not delete this comment)
                            //--------------------------------------
                            //IsChecked -> 0/1 (0 default)
                            //Id has id of array;
                            //self.productCatalogCheckedIdList is observableArray <-- change this
                            //--------------------------------------
                            var mappingOptions = {
                                'IsChecked': {
                                    create: function (options) {
                                        var temp = ko.observable(options.data);
                                        temp.subscribe(function (val) {
                                            var value = options.parent.Id();
                                            if (options.parent.IsChecked() == true) {
                                                self.productCatalogCheckedIdList.push(value);
                                            }
                                            else {
                                                self.productCatalogCheckedIdList.remove(value);
                                            }
                                        });
                                        return temp;
                                    }
                                }
                            }
                            ko.mapping.fromJS(response, mappingOptions, self.productCatalogList);
                            //ko.mapping.fromJS(response, self.productCatalogList);

                        }
                        catch (e) {
                            alert(e);
                        }
                    },
                    complete: function () {
                        //$("#btnsave").attr('disabled', false);
                        SGloadingRemove();
                    },
                    error: function (xhr, status, error) {
                        //reset variables here
                        alert(status + ' ' + error);
                    }
                });
            };

            self.productSuggestionClick = function () {
                //var selectedProductCatalogId = self.getSelectedProductCatalogId();
                var selectedRow = self.getSelectedProductCatalogRow();
                var selectedProductCatalogId = selectedRow.ProductCatalogId();
                var importSourceColumnId = selectedRow.ImportSourceColumnId();

                //self.showVendorProductCatalog(true);

                // Ajax call
                $.ajax({
                    type: 'POST',
                    url: urlVendorProductCatalogDetails,
                    cache: false,
                    //contentType: 'application/json; charset=utf-8',
                    data: { 'productCatalogId': selectedProductCatalogId, 'importSourceColumnId': importSourceColumnId, 'vendorId': vendorId, 'skuType': skuType },
                    dataType: 'json',
                    beforeSend: function () {
                        //$("#btnsave").attr('disabled', true);
                        SGloading('Loading');
                    },
                    success: function (response) {

                        response = JSON.parse(response);
                        //           //console.log(response);
                        try {
                            //--------------------------------------
                            //Scenario: (do not delete this comment)
                            //--------------------------------------
                            //IsChecked -> 0/1 (0 default)
                            //Id has id of array;
                            //self.vendorProductCatalogCheckedIdList is observableArray <-- change this
                            //--------------------------------------
                            var mappingOptions = {
                                'IsChecked': {
                                    create: function (options) {
                                        var temp = ko.observable(options.data);
                                        temp.subscribe(function (val) {
                                            var value = options.parent.Id();
                                            if (options.parent.IsChecked() == true) {
                                                self.vendorProductCatalogCheckedIdList.push(value);
                                            }
                                            else {
                                                self.vendorProductCatalogCheckedIdList.remove(value);
                                            }
                                        });
                                        return temp;
                                    }
                                }
                            }
                            ko.mapping.fromJS(response, mappingOptions, self.vendorProductCatalogList);

                            //show vendor product catalog
                            self.showVendorProductCatalog(true);


                        }
                        catch (e) {
                            alert(e);
                            self.showVendorProductCatalog(false);
                        }
                    },
                    complete: function () {
                        //$("#btnsave").attr('disabled', false);
                        SGloadingRemove();

                        rebuilttooltip();
                    },
                    error: function (xhr, status, error) {
                        //reset variables here
                        alert(status + ' ' + error);
                    }
                });

            };

            self.selectedItemsMatchedSuggestion = function () {
                var selectedId = self.getSelectedProductCatalogId();
            }

            self.getSelectedImportSourceColumnId = function () {
                var selectedId = null;
                ko.utils.arrayForEach(self.productCatalogList(), function (val) {
                    if (val.IsChecked() == true)
                        selectedId = val.importSourceColumnId();
                });
                return selectedId;
            }

            self.getSelectedVendorProductCatalogRow = function () {
                var row = null;
                ko.utils.arrayForEach(self.vendorProductCatalogList(), function (val) {
                    if (val.IsChecked() == true)
                        row = val;
                });
                return row;
            }

            self.getSelectedProductCatalogRow = function () {
                var row = null;
                ko.utils.arrayForEach(self.productCatalogList(), function (val) {
                    if (val.IsChecked() == true)
                        row = val;
                });
                return row;
            };

            self.productRowClick = function (val) {

                val.IsChecked(!val.IsChecked());

                if (val.IsChecked() == true)
                    self.productCatalogCheckedIdList.push(val.Id());
                else
                    self.productCatalogCheckedIdList.remove(val.Id());

            }

            self.vendorProductRowClick = function (val) {
                val.IsChecked(!val.IsChecked());
            }

            self.productDetailClick = function () {

                var selectedPCRow = self.getSelectedProductCatalogRow();
                var selectedPCId = selectedPCRow.ProductCatalogId();

                $.getJSON(urlGetProductCatalogById + '?Id=' + selectedPCId, function (dataList) {

                    if (dataList == "" || dataList == null) {
                        alertmsg("yellow", "No record found");
                    }
                    else {

                        if (dataList.LocalSku != null) document.getElementById('LocalSku').value = dataList.LocalSku;
                        if (dataList.Location != null) document.getElementById('Location').value = dataList.Location;
                        if (dataList.Name != null) document.getElementById('Name').value = dataList.Name;
                        if (dataList.Priority != null) document.getElementById('Priority').value = dataList.Priority;
                        if (dataList.ShortDescription != null) document.getElementById('Description').value = dataList.ShortDescription;
                        if (dataList.IsEnabled != null) {
                            if (dataList.IsEnabled == true) $('#IsEnabled123').val(1);
                            else if (dataList.IsEnabled == false) $('#IsEnabled123').val(0);

                        } else $('#IsEnabled123').val(2);
                        if (dataList.SalePrice != null) document.getElementById('SalePrice').value = dataList.SalePrice;
                        if (dataList.Price != null) document.getElementById('Price').value = dataList.Price;
                        if (dataList.Weight != null) document.getElementById('Weight').value = dataList.Weight;
                        if (dataList.Height != null) document.getElementById('Height').value = dataList.Height;
                        if (dataList.Width != null) document.getElementById('Width').value = dataList.Width;
                        if (dataList.Length != null) document.getElementById('Length').value = dataList.Length;
                        if (dataList.Material != null) document.getElementById('Material').value = dataList.Material;
                        if (dataList.ISBN != null) document.getElementById('ISBN').value = dataList.ISBN;
                        if (dataList.NSN != null) document.getElementById('NSN').value = dataList.NSN;
                        if (dataList.UPC != null) document.getElementById('UPC').value = dataList.UPC;
                        if (dataList.EAN != null) document.getElementById('EAN').value = dataList.EAN;
                        if (dataList.MPN != null) document.getElementById('MPN').value = dataList.MPN;
                        if (dataList.ASIN != null) document.getElementById('ASIN').value = dataList.ASIN;
                        if (dataList.ProductId != null) document.getElementById('ProductCatalogUpdateId').value = dataList.ProductId;

                    }
                });

                $("#dialog-product-catalog").modal('show');

            };

            self.vendorProductDetailClick = function () {

                var selectedVPCRow = self.getSelectedVendorProductCatalogRow();
                var selectedVPCId = selectedVPCRow.VendorProductCatalogId();

                $.getJSON(urlGetVendorProductCatalogById + '?Id=' + selectedVPCId, function (dataList) {
                    if (dataList == "" || dataList == null) {
                        alertmsg("yellow", "No record found");
                    }
                    else {
                        //document.getElementById('VendorProductIdTxtBox').value = selectedVPCId;
                        if (dataList.VendorPartNumber != null) document.getElementById('VPCVendorPartNumber').value = dataList.VendorPartNumber;
                        if (dataList.Name != null) document.getElementById('VPCName').value = dataList.Name;
                        if (dataList.Cost != null) document.getElementById('VPCCost').value = dataList.Cost;
                        if (dataList.Description != null) document.getElementById('VPCDescription').value = dataList.Description;
                        if (dataList.MinimumOrderUnit != null) document.getElementById('VPCMinimumOrderUnit').value = dataList.MinimumOrderUnit;
                        if (dataList.MinimumOrderQuantity != null) document.getElementById('VPCMinimumOrderQuantity').value = dataList.MinimumOrderQuantity;
                        if (dataList.IsDisabled != null) {
                            if (dataList.IsDisabled == true) $('#VPCIsDisabled').val(1);
                            else if (dataList.IsDisabled == false) $('#VPCIsDisabled').val(0);
                        } else {
                            $('#VPCIsDisabled').val(2);
                        }
                        if (dataList.CanNotDropship != null) {
                            if (dataList.CanNotDropship == true) $('#VPCCanNotDropship').val(1);
                            else if (dataList.CanNotDropship == false) $('#VPCCanNotDropship').val(0);
                        } else {
                            $('#VPCCanNotDropship').val(2);
                        }
                        if (dataList.OnOrder != null) document.getElementById('VPCOnOrder').value = dataList.OnOrder;
                        if (dataList.QuantityPerLot != null) document.getElementById('VPCQuantityPerLot').value = dataList.QuantityPerLot;
                        if (dataList.LotPartNumber != null) document.getElementById('VPCLotPartNumber').value = dataList.LotPartNumber;
                        if (dataList.LotBarcode != null) document.getElementById('VPCLotBarcode').value = dataList.LotBarcode;
                        if (dataList.CostPerLot != null) document.getElementById('VPCCostPerLot').value = dataList.CostPerLot;
                        if (dataList.OrderBy == null) {
                            document.getElementById('VPCOrderBy').value = 2;
                        } else {
                            document.getElementById('VPCOrderBy').value = dataList.OrderBy;
                        }
                        if (dataList.ManufacturersBarcode != null) document.getElementById('VPCManufacturersBarcode').value = dataList.ManufacturersBarcode;
                        if (dataList.LotDescription != null) document.getElementById('VPCLotDescription').value = dataList.LotDescription;
                        if (dataList.LeadTimeInDays != null) document.getElementById('VPCLeadTimeInDays').value = dataList.LeadTimeInDays;
                    }
                });

                $("#dialog-vendor-product-catalog").modal('show');

            };

            self.vendorProductAssignClick = function () {
                var selectedVPC = self.getSelectedVendorProductCatalogRow();
                var selectedPartNumber = selectedVPC.VendorPartNumber();

                if (selectedPartNumber == '') {
                    alertmsg('red', "Please enter 'Part No.' first!");
                    return;
                }

                $("#assign-modal").modal('show');

            };

            self.vendorProductRejectClick = function () {
                var selectedVPC = self.getSelectedVendorProductCatalogRow();
                var selectedPartNumber = selectedVPC.VendorPartNumber();

                $("#reject-modal").modal('show');

            };

            self.assignPartNumberClick = function () {

                var selectedPC = self.getSelectedProductCatalogRow();
                var selectedVPC = self.getSelectedVendorProductCatalogRow();

                var selectedProductCatalogId = selectedPC.ProductCatalogId();
                var selectedVendorProductCatalogId = selectedVPC.VendorProductCatalogId();
                var selectedVendorPartNumber = selectedVPC.VendorPartNumber();

                // Ajax call
                $.ajax({
                    type: 'POST',
                    url: urlAssignPartNumber,
                    cache: false,
                    //contentType: 'application/json; charset=utf-8',
                    data: { 'productId': selectedProductCatalogId, 'vendorProductCatalogId': selectedVendorProductCatalogId, 'vendorPartNumber': selectedVendorPartNumber },
                    dataType: 'json',
                    beforeSend: function () {
                        //$("#btnsave").attr('disabled', true);
                        SGloading('Loading');
                    },
                    success: function (response) {
                        if (parseInt(response) > 0) {

                            ////remove the item from product catalog list ???
                            //for (var i = self.productCatalogList().length - 1; i >= 0; i--) {
                            //    var val = self.productCatalogList()[i];
                            //    if (val.ProductCatalogId() == selectedProductCatalogId) {
                            //        self.productCatalogList.remove(val);
                            //    }
                            //}

                            //remove the item FROM PIMVendor product catalog list
                            //for (var i = self.vendorProductCatalogList().length - 1; i >= 0; i--) {
                            //    var val = self.vendorProductCatalogList()[i];
                            //    if (val.VendorProductCatalogId() == selectedVendorProductCatalogId) {
                            //        self.vendorProductCatalogList.remove(val);
                            //    }
                            //}

                            //remove the item FROM PIMVendor product catalog list
                            var id = selectedVPC.Id();
                            //fadeoOut the selected item
                            $("#" + id + " td").css('background-color', '#FFE8DE');
                            $("#" + id).fadeOut(1000);
                            //remove from list after fadeOut is completed
                            setTimeout(function () {
                                self.vendorProductCatalogList.remove(selectedVPC);
                            }, 1000);


                            //self.vendorProductCatalogList.remove(selectedVPC);


                            //update the count
                            self.refreshMatchSuggestionList();

                            alertmsg('green', "Assign part number successful!");

                        }
                        else {
                            alertmsg('red', "Assign part number failed!");
                        }
                    },
                    complete: function () {
                        //$("#btnsave").attr('disabled', false);
                        SGloadingRemove();
                    },
                    error: function (xhr, status, error) {
                        //reset variables here
                        alert(status + ' ' + error);
                    }
                });
            }

            self.rejectPartNumberClick = function () {

                var selectedPC = self.getSelectedProductCatalogRow();
                var selectedVPC = self.getSelectedVendorProductCatalogRow();

                var selectedProductCatalogId = selectedPC.ProductCatalogId();
                var selectedVendorProductCatalogId = selectedVPC.VendorProductCatalogId();
                var selectedVendorPartNumber = selectedVPC.VendorPartNumber();

                // Ajax call
                $.ajax({
                    type: 'POST',
                    url: urlRejectPartNumber,
                    cache: false,
                    //contentType: 'application/json; charset=utf-8',
                    data: { 'vendorProductCatalogId': selectedVendorProductCatalogId, 'vendorPartNumber': selectedVendorPartNumber },
                    dataType: 'json',
                    beforeSend: function () {
                        //$("#btnsave").attr('disabled', true);
                        SGloading('Loading');
                    },
                    success: function (response) {
                        if (parseInt(response) > 0) {

                            ////remove the item from product catalog list ???
                            //for (var i = self.productCatalogList().length - 1; i >= 0; i--) {
                            //    var val = self.productCatalogList()[i];
                            //    if (val.ProductCatalogId() == selectedProductCatalogId) {
                            //        self.productCatalogList.remove(val);
                            //    }
                            //}

                            //remove the item FROM PIMVendor product catalog list
                            //for (var i = self.vendorProductCatalogList().length - 1; i >= 0; i--) {
                            //    var val = self.vendorProductCatalogList()[i];
                            //    if (val.VendorProductCatalogId() == selectedVendorProductCatalogId) {
                            //        self.vendorProductCatalogList.remove(val);
                            //    }
                            //}

                            //remove the item FROM PIMVendor product catalog list
                            var id = selectedVPC.Id();
                            //fadeoOut the selected item
                            $("#" + id + " td").css('background-color', '#FFE8DE');
                            $("#" + id).fadeOut(1000);
                            //remove from list after fadeOut is completed
                            setTimeout(function () {
                                self.vendorProductCatalogList.remove(selectedVPC);
                            }, 1000);


                            //self.vendorProductCatalogList.remove(selectedVPC);


                            //update the count
                            self.refreshMatchSuggestionList();

                            alertmsg('green', "Reject part number successful!");

                        }
                        else {
                            alertmsg('red', "Reject part number failed!");
                        }
                    },
                    complete: function () {
                        //$("#btnsave").attr('disabled', false);
                        SGloadingRemove();
                    },
                    error: function (xhr, status, error) {
                        //reset variables here
                        alert(status + ' ' + error);
                    }
                });
            }

            self.refreshMatchSuggestionList = function () {
                //urlSuggestionAggregates
                //self.matchSuggestionList
                // Ajax call
                self.vendorProductCatalogCheckedIdList([]);

                window.location.reload();

                //$.ajax({
                //    type: 'POST',
                //    url: urlSuggestionAggregates,
                //    cache: false,
                //    //contentType: 'application/json; charset=utf-8',
                //    data: { skuType: skuType },
                //    dataType: 'json',
                //    success: function (response) {
                //        response = JSON.parse(response);

                //        for (var i = 0; i < self.matchSuggestionList().length; i++) {
                //            var oldVal = self.matchSuggestionList()[i];
                //            var newVal = response[i];

                //            self.matchSuggestionList()[i].Count(newVal.Count);
                //        }
                //        //self.matchSuggestionList = ko.mapping.fromJS([]);
                //        //ko.mapping.fromJS(skuMatchedSuggestionAggData, self.matchSuggestionList);

                //        //ko.mapping.fromJS(response, self.matchSuggestionList);
                //        //self.productCatalogCheckedIdList([]);
                //        self.vendorProductCatalogCheckedIdList([]);
                //    },
                //    error: function (xhr, status, error) {
                //        //reset variables here
                //        alert(status + ' ' + error);
                //    }
                //});
            }

            self.newProductClick = function () {
                //alert('new vendor product clicked');
                var selectedVPC = self.getSelectedVendorProductCatalogRow();

                $(".product-catalog-editor #LocalSku").val(selectedVPC.VendorPartNumber());
                $(".product-catalog-editor #Name").val(selectedVPC.Name());
                $(".product-catalog-editor #Priority").val('');
                $(".product-catalog-editor #Location").val('');
                $(".product-catalog-editor #ShortDescription").val(selectedVPC.Description());

                var isEnabled = 2;
                if (selectedVPC.IsDisabled() != null) {
                    if (selectedVPC.IsDisabled() == true) isEnabled = 0;
                    else if (dataList.IsEnabled == false) isEnabled = 1;
                }
                $(".product-catalog-editor #IsEnabled").val(isEnabled);
                $(".product-catalog-editor #SalePrice").val(selectedVPC.Cost());
                $(".product-catalog-editor #Price").val('');
                $(".product-catalog-editor #Weight").val('');
                $(".product-catalog-editor #Height").val('');
                $(".product-catalog-editor #Width").val('');
                $(".product-catalog-editor #Length").val('');
                $(".product-catalog-editor #Material").val('');
                $(".product-catalog-editor #ISBN").val('');
                $(".product-catalog-editor #NSN").val('');
                $(".product-catalog-editor #UPC").val('');
                $(".product-catalog-editor #EAN").val('');
                $(".product-catalog-editor #MPN").val('');
                $(".product-catalog-editor #ASIN").val('');

                $("#createProductCatalog").modal('show');

            };

            self.createNewProduct = function () {

                // Validate
                if ($(".product-catalog-editor #LocalSku").val() == '') {
                    alertmsg('red', 'Please enter Part Number');
                    return;
                }

                //if (confirm('hide')) {
                //    $("#createProductCatalog").modal('hide');
                //}

                //var pcLocalSKU = $(".product-catalog-editor #LocalSku").val(),
                //var pcName = $(".product-catalog-editor #Name").val(),
                //var pcPriority = $(".product-catalog-editor #Priority").val(),
                //var pcLocation = $(".product-catalog-editor #Location").val(),
                //var pcShortDescription = $(".product-catalog-editor #ShortDescription").val(),
                //var pcIsEnabled = $(".product-catalog-editor #IsEnabled").val(),
                //var pcSalePrice = $(".product-catalog-editor #SalePrice").val(),
                //var pcPrice = $(".product-catalog-editor #Price").val(''),
                //var pcWeight = $(".product-catalog-editor #Weight").val(),
                //var pcHeight = $(".product-catalog-editor #Height").val(),
                //var pcWidth = $(".product-catalog-editor #Width").val(),
                //var pcLength = $(".product-catalog-editor #Length").val(),
                //var pcMaterial = $(".product-catalog-editor #Material").val(),
                //var pcISBN = $(".product-catalog-editor #ISBN").val(),
                //var pcNSN = $(".product-catalog-editor #NSN").val(),
                //var pcUPC = $(".product-catalog-editor #UPC").val(),
                //var pcEAN = $(".product-catalog-editor #EAN").val(),
                //var pcMPN = $(".product-catalog-editor #MPN").val(),
                //var pcASIN = $(".product-catalog-editor #ASIN").val(),

                var selectedVPC = self.getSelectedVendorProductCatalogRow();

                var myProductCatalogViewModel =
                {
                    "Name": $(".product-catalog-editor #Name").val(),
                    "ProductId": 0, // $(".product-catalog-editor #Id").val(),
                    "LocalSku": $(".product-catalog-editor #LocalSku").val(),
                    "Location": $(".product-catalog-editor #Location").val(),
                    "Name": $(".product-catalog-editor #Name").val(),
                    "Priority": $(".product-catalog-editor #Priority").val(),
                    "ShortDescription": $(".product-catalog-editor #ShortDescription").val(),
                    "IsEnabled": $(".product-catalog-editor #IsEnabled").val(),
                    "SalePrice": $(".product-catalog-editor #SalePrice").val(),
                    "Price": $(".product-catalog-editor #Price").val(),
                    "Weight": $(".product-catalog-editor #Weight").val(),
                    "Height": $(".product-catalog-editor #Height").val(),
                    "Width": $(".product-catalog-editor #Width").val(),
                    "Length": $(".product-catalog-editor #Length").val(),
                    "Material": $(".product-catalog-editor #Material").val(),
                    "ISBN": $(".product-catalog-editor #ISBN").val(),
                    "NSN": $(".product-catalog-editor #NSN").val(),
                    "UPC": $(".product-catalog-editor #UPC").val(),
                    "EAN": $(".product-catalog-editor #EAN").val(),
                    "MPN": $(".product-catalog-editor #MPN").val(),
                    "ASIN": $(".product-catalog-editor #ASIN").val(),
                    "PrimaryVendorProductId": selectedVPC.VendorProductCatalogId()
                };

                // Ajax call
                $.ajax({
                    type: 'POST',
                    url: urlCreateProductCatalog,
                    cache: false,
                    //contentType: 'application/json; charset=utf-8',
                    //data: { productCatalogViewModel: ProductCatalogViewModel },
                    data: myProductCatalogViewModel,
                    dataType: 'json',
                    success: function (response) {
                        if (response == "success") {


                            //remove the item FROM PIMVendor product catalog list
                            var id = selectedVPC.Id();
                            //fadeoOut the selected item
                            $("#" + id + " td").css('background-color', '#FFE8DE');
                            $("#" + id).fadeOut(1000);
                            //remove from list after fadeOut is completed
                            setTimeout(function () {
                                self.vendorProductCatalogList.remove(selectedVPC);
                            }, 1000);

                            alertmsg('green', 'Create new product successful!');
                            //smallAlertmsg('assignModalYes', 'green', 'Create new product successful!');
                            $("#createProductCatalog").modal('hide');

                            //update the count
                            self.refreshMatchSuggestionList();

                        }
                        else {
                            //alertmsg('red', response == "" ? 'Create new product failed!' : response);
                            smallAlertmsg('assignModalYess', 'red', response == "" ? 'Create new product failed!' : response);
                        }
                    },
                    error: function (xhr, status, error) {
                        //reset variables here
                        alert(status + ' ' + error);
                    }
                });

            }
        }

        var vm = new suggestionViewModel();
        ko.applyBindings(vm);

        function slickTable() {
            $('body').on('click', '.sg-table-sleek tr input', function (e) {

                if ($(e.target).is(':checked')) {
                    //$(this).addClass('selected');
                }
                else {
                    //$(this).removeClass('selected');
                }

            });

            $('.selectAll').change(function () {
                if ($(this).is(':checked')) {
                    $('.sg-table-sleek tr').addClass('selected');
                }
                else {
                    $('.sg-table-sleek tr').removeClass('selected');
                }
            });

        };

        $('#suggestionProductCatalogGrid').DataTable();

    }
    catch (e) {
        alert('error' + e);
    }

});