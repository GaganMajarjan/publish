﻿
$(document).ready(function () {
    //slickTable();

    var winheight = $(window).height();
    var fhoffset = $('.sg-table-sleek-wrap').offset();
    $('.sg-table-sleek-wrap').height((Math.floor(winheight - fhoffset.top - 180)) / 2);
    $('.sg-adj-nav').height(winheight - 100);
    $('#grid-div').append('<table id="gridDataTable"><thead><tr></tr></thead></table>');

    //add heading to table
    var i = 0;
    var includedColumns = [];

    //  console.log(returnObject);

    $.each(returnObject.sColumns, function (key, value) {
        // console.log(value);
        var headerRemove = value.replace(/_/gi, " ");
        //console.log(value);
        if (i > 0) {
            $('#gridDataTable thead tr').append('<th>' + headerRemove + '</th>');
            includedColumns.push({ "sName": value });

        } else {
            $('#gridDataTable thead tr').append('<th>' + "" + '</th>');
            includedColumns.push({
                "sName": value,
                "bSearchable": false,
                "bSortable": false,
                "width": "10",

                "mRender": function (data, type, full) {
                    return '<input type="checkbox" class="listStagingDataGuid" value="' + data + '">'
                }
            });

        }
        i++;
    });


    //add data to table via datatable
    //var table = $('#gridDataTable').DataTable({
    //    destroy: true,
    //    data: returnObject.aaData,
    //    "scrollX": true,
    //    "aLengthMenu": dataTableMenuLength,
    //    "iDisplayLength": dataTableDisplayLength,
    //    "aoColumns": includedColumns
    //});


    var table = $('#gridDataTable').dataTable({

        "bProcessing": true,
        "bServerSide": true,
        "destroy": true,
        "scrollX": true,
        "bSort": false,
        "bFilter": false,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlGetSuggestionUnmatched + '?vendorId=' + vendorId + '&skuType=' + skuType + '&errorType=' + errorType + '&logUserUploadId=' + logUserUploadId + '&stagingId=' + stagingId,
        "sServerMethod": "POST",
        "aoColumns": includedColumns


    });

    $('#addNewProduct').click(function () {
       
        var i = 0;
        var checkboxIds = "";
        $('input[type=checkbox][class=listStagingDataGuid]').each(function () {

            if (this.checked) {
                i++;
                checkboxIds = checkboxIds + this.value;
            }
        });

        if (i === 0)
            alertmsg("red", "Please select one to add.");
        else if (i > 1)
            alertmsg("red", "Please select only one to add.");
        else {
            setTimeout(function () {
                $('#gridDataTable tbody .selected').hide();

            }, 500);
             window.open(urlVendorProductCatalogCreate +   "?workflowId=" + 2 +  "&stepId=" + 5
                + "&stagingId=" + stagingId + "&logUserUploadId=" + logUserUploadId + "&vendorId=" + vendorId + '&errorType=' + errorType + "&guid=" + checkboxIds);

        }


    });

    $('#btnRejectId').click(function () {
        var idsToReject = "";
        var i = 0;
        var checkboxIds = "";
        $('input[type=checkbox][class=listStagingDataGuid]').each(function () {

            if (this.checked) {
                i++;
                checkboxIds = checkboxIds + this.value;
                idsToReject += this.value + ",";
            }
        });

        if (i === 0)
            alertmsg("red", "Please select one to reject.");
        //else if (i > 1)
        //    alertmsg("red", "Please select only one to reject.");
        else {
            // Ajax call
            $.ajax({
                type: 'POST',
                url: urlRejectProductCatalog,
                cache: false,
                //contentType: 'application/json; charset=utf-8',
                //vendorId, skuType, errorType, logUserUploadId, stagingId, guid
                data: { 'vendorId': vendorId, 'skuType': skuType, 'errorType': errorType, 'logUserUploadId': logUserUploadId, 'stagingId': stagingId, 'guid': idsToReject },
                dataType: 'json',
                beforeSend: function () {
                    //$("#btnsave").attr('disabled', true);
                    SGloading('Loading');
                },
                success: function (response) {
                    //console.log(response);
                    //console.log(JSON.parse(response));

                    try {
                        if (response.Status === true) {
                            alertmsg('green', response.Message);

                            $('#gridDataTable tbody .selected td').css('background-color', 'red');
                            setTimeout(function () {
                                $('#gridDataTable tbody .selected').hide();

                            }, 500);

                        } else {
                            alertmsg('red', response.Message);
                        }
                    }
                    catch (e) {
                        console.log('Error occured');
                        console.log(e);
                    }
                },
                complete: function () {
                    //$("#btnsave").attr('disabled', false);
                    SGloadingRemove();
                },
                error: function (xhr, status, error) {
                    //reset variables here
                    console.log(status + ' ' + error);
                }
            });

            //window.location = urlRejectProductCatalog + "?returnUrl=" + urlProductSuggestionValidation + "&vendorId=" + vendorId + "&skuType=" + skuType + "&errorType=" + errorType
            //    + "&logUserUploadId=" + logUserUploadId + "&stagingId=" + stagingId + "&guid=" + checkboxIds;

        }


    });

    try {

        // Suggestion view model
        var suggestionViewModel = function () {
            var self = this;

            // List declaration
            self.vendorProductCatalogList = ko.mapping.fromJS([]);

            // Show VPC by default
            self.showVendorProductCatalog = ko.observable(true);

            // For checkbox
            self.vendorProductCatalogCheckedIdList = ko.observableArray([]);

            self.vendorProductCatalogMasterCheck = ko.observable(false);
            self.vendorProductCatalogMasterCheck.subscribe(function (val) {
                ko.utils.arrayForEach(self.vendorProductCatalogList(), function (item) {
                    item.IsChecked(val);
                });
            });

            // For selected data
            self.selectedData = ko.observable();

            self.getSelectedVendorProductCatalogRow = function () {
                var row = null;
                ko.utils.arrayForEach(self.vendorProductCatalogList(), function (val) {
                    if (val.IsChecked() == true)
                        row = val;
                });
                return row;
            }

            self.vendorProductRowClick = function (val) {
                val.IsChecked(!val.IsChecked());
            }

            self.vendorProductDetailClick = function () {

                var selectedVPCRow = self.getSelectedVendorProductCatalogRow();
                var selectedVPCId = selectedVPCRow.VendorProductCatalogId();

                $.getJSON(urlGetVendorProductCatalogById + '?Id=' + selectedVPCId, function (dataList) {
                    if (dataList == "" || dataList == null) {
                        alertmsg("yellow", "No record found");
                    }
                    else {
                        //document.getElementById('VendorProductIdTxtBox').value = selectedVPCId;
                        if (dataList.VendorPartNumber != null) document.getElementById('VPCVendorPartNumber').value = dataList.VendorPartNumber;
                        if (dataList.Name != null) document.getElementById('VPCName').value = dataList.Name;
                        if (dataList.Cost != null) document.getElementById('VPCCost').value = dataList.Cost;
                        if (dataList.Description != null) document.getElementById('VPCDescription').value = dataList.Description;
                        if (dataList.MinimumOrderUnit != null) document.getElementById('VPCMinimumOrderUnit').value = dataList.MinimumOrderUnit;
                        if (dataList.MinimumOrderQuantity != null) document.getElementById('VPCMinimumOrderQuantity').value = dataList.MinimumOrderQuantity;
                        if (dataList.IsDisabled != null) {
                            if (dataList.IsDisabled == true) $('#VPCIsDisabled').val(1);
                            else if (dataList.IsDisabled == false) $('#VPCIsDisabled').val(0);
                        } else {
                            $('#VPCIsDisabled').val(2);
                        }
                        if (dataList.CanNotDropship != null) {
                            if (dataList.CanNotDropship == true) $('#VPCCanNotDropship').val(1);
                            else if (dataList.CanNotDropship == false) $('#VPCCanNotDropship').val(0);
                        } else {
                            $('#VPCCanNotDropship').val(2);
                        }
                        if (dataList.OnOrder != null) document.getElementById('VPCOnOrder').value = dataList.OnOrder;
                        if (dataList.QuantityPerLot != null) document.getElementById('VPCQuantityPerLot').value = dataList.QuantityPerLot;
                        if (dataList.LotPartNumber != null) document.getElementById('VPCLotPartNumber').value = dataList.LotPartNumber;
                        if (dataList.LotBarcode != null) document.getElementById('VPCLotBarcode').value = dataList.LotBarcode;
                        if (dataList.CostPerLot != null) document.getElementById('VPCCostPerLot').value = dataList.CostPerLot;
                        if (dataList.OrderBy == null) {
                            document.getElementById('VPCOrderBy').value = 2;
                        } else {
                            document.getElementById('VPCOrderBy').value = dataList.OrderBy;
                        }
                        if (dataList.ManufacturersBarcode != null) document.getElementById('VPCManufacturersBarcode').value = dataList.ManufacturersBarcode;
                        if (dataList.LotDescription != null) document.getElementById('VPCLotDescription').value = dataList.LotDescription;
                        if (dataList.LeadTimeInDays != null) document.getElementById('VPCLeadTimeInDays').value = dataList.LeadTimeInDays;
                    }
                });

                $("#dialog-vendor-product-catalog").modal('show');

            };

            self.refreshMatchSuggestionList = function () {
                self.vendorProductCatalogCheckedIdList([]);

                //urlSuggestionAggregates
                //self.matchSuggestionList

                //window.location.reload();

                // Ajax call
                //$.ajax({
                //    type: 'POST',
                //    url: urlSuggestionAggregates,
                //    cache: false,
                //    //contentType: 'application/json; charset=utf-8',
                //    data: { skuType: skuType },
                //    dataType: 'json',
                //    success: function (response) {
                //        response = JSON.parse(response);

                //        for (var i = 0; i < self.matchSuggestionList().length; i++) {
                //            var oldVal = self.matchSuggestionList()[i];
                //            var newVal = response[i];

                //            self.matchSuggestionList()[i].Count(newVal.Count);
                //        }
                //        //self.matchSuggestionList = ko.mapping.fromJS([]);
                //        //ko.mapping.fromJS(skuMatchedSuggestionAggData, self.matchSuggestionList);

                //        //ko.mapping.fromJS(response, self.matchSuggestionList);
                //        //self.productCatalogCheckedIdList([]);
                //        self.vendorProductCatalogCheckedIdList([]);
                //    },
                //    error: function (xhr, status, error) {
                //        //reset variables here
                //        alert(status + ' ' + error);
                //    }
                //});
            }

            self.newProductClick = function () {
                //var selectedVPC = self.getSelectedVendorProductCatalogRow();
                ////  //console.log(ko.toJSON(selectedVPC));
                //$(".product-catalog-editor #LocalSku").val(selectedVPC.VendorPartNumber());
                //$(".product-catalog-editor #Name").val(selectedVPC.Name());
                //$(".product-catalog-editor #Priority").val('');
                //$(".product-catalog-editor #Location").val('');
                //$(".product-catalog-editor #ShortDescription").val(selectedVPC.Description());

                ////    //console.log(ko.toJSON(selectedVPC));

                //var isEnabled = 2;
                //try {
                //    if (selectedVPC.IsDisabled() != null) {
                //        if (selectedVPC.IsDisabled() == true) isEnabled = 0;
                //        else if (dataList.IsEnabled == false) isEnabled = 1;
                //    }
                //}
                //catch (e) {
                //    isEnabled = selectedVPC.IsEnabled();
                //}
                //$(".product-catalog-editor #IsEnabled").val(isEnabled);
                //$(".product-catalog-editor #SalePrice").val(selectedVPC.Cost());
                //$(".product-catalog-editor #Price").val('');
                //$(".product-catalog-editor #Weight").val('');
                //$(".product-catalog-editor #Height").val('');
                //$(".product-catalog-editor #Width").val('');
                //$(".product-catalog-editor #Length").val('');
                //$(".product-catalog-editor #Material").val('');
                //$(".product-catalog-editor #ISBN").val('');
                //$(".product-catalog-editor #NSN").val('');
                //$(".product-catalog-editor #UPC").val('');
                //$(".product-catalog-editor #EAN").val('');
                //$(".product-catalog-editor #MPN").val('');
                //$(".product-catalog-editor #ASIN").val('');

                //$("#createProductCatalog").modal('show');

                window.open(urlProductCataglogCreate, '_blank');


            };

            self.createNewProduct = function () {

                var selectedVPC = self.getSelectedVendorProductCatalogRow();

                // Validate
                if ($(".product-catalog-editor #LocalSku").val() == '') {
                    alertmsg('red', 'Please enter Part Number');
                    return;
                }

                var myProductCatalogViewModel =
                {
                    "Name": $(".product-catalog-editor #Name").val(),
                    "ProductId": 0, // $(".product-catalog-editor #Id").val(),
                    "LocalSku": $(".product-catalog-editor #LocalSku").val(),
                    "Location": $(".product-catalog-editor #Location").val(),
                    //"Name": $(".product-catalog-editor #Name").val(),
                    "Priority": $(".product-catalog-editor #Priority").val(),
                    "ShortDescription": $(".product-catalog-editor #ShortDescription").val(),
                    "IsEnabled": $(".product-catalog-editor #IsEnabled").val(),
                    "SalePrice": $(".product-catalog-editor #SalePrice").val(),
                    "Price": $(".product-catalog-editor #Price").val(),
                    "Weight": $(".product-catalog-editor #Weight").val(),
                    "Height": $(".product-catalog-editor #Height").val(),
                    "Width": $(".product-catalog-editor #Width").val(),
                    "Length": $(".product-catalog-editor #Length").val(),
                    "Material": $(".product-catalog-editor #Material").val(),
                    "ISBN": $(".product-catalog-editor #ISBN").val(),
                    "NSN": $(".product-catalog-editor #NSN").val(),
                    "UPC": $(".product-catalog-editor #UPC").val(),
                    "EAN": $(".product-catalog-editor #EAN").val(),
                    "MPN": $(".product-catalog-editor #MPN").val(),
                    "ASIN": $(".product-catalog-editor #ASIN").val(),
                    "PrimaryVendorProductId": selectedVPC.VendorProductCatalogId()
                };

                // Ajax call
                $.ajax({
                    type: 'POST',
                    url: urlCreateProductCatalog,
                    cache: false,
                    //contentType: 'application/json; charset=utf-8',
                    //data: { productCatalogViewModel: ProductCatalogViewModel },
                    data: myProductCatalogViewModel,
                    dataType: 'json',
                    success: function (response) {
                        //    //console.log(response);
                        if (response == "success") {
                            //remove the item FROM PIMVendor product catalog list
                            var id = selectedVPC.Id();

                            //fadeoOut the selected item
                            $("#" + id + " td").css('background-color', '#FFE8DE');
                            $("#" + id).fadeOut(1000);

                            //remove from list after fadeOut is completed
                            setTimeout(function () {
                                self.vendorProductCatalogList.remove(selectedVPC);
                            }, 1000);

                            alertmsg('green', 'Create new product successful!');
                            //smallAlertmsg('assignModalYes', 'green', 'Create new product successful!');
                            $("#createProductCatalog").modal('hide');

                            //update the count
                            self.refreshMatchSuggestionList();

                        }
                        else {
                            //alertmsg('red', response == "" ? 'Create new product failed!' : response);
                            smallAlertmsg('assignModalYess', 'red', response == "" ? 'Create new product failed!' : response,200);
                        }
                    },
                    error: function (xhr, status, error) {
                        //reset variables here
                        alert(status + ' ' + error);
                    }
                });

            }

            self.vendorProductRejectClick = function () {
                var selectedVPC = self.getSelectedVendorProductCatalogRow();
                var selectedPartNumber = selectedVPC.VendorPartNumber();

                $("#reject-modal").modal('show');
            };

            self.rejectPartNumberClick = function () {

                //var selectedPC = self.getSelectedProductCatalogRow();
                var selectedVPC = self.getSelectedVendorProductCatalogRow();

                //var selectedProductCatalogId = selectedPC.ProductCatalogId();
                var selectedVendorProductCatalogId = selectedVPC.VendorProductCatalogId();
                var selectedVendorPartNumber = selectedVPC.VendorPartNumber();

                // Ajax call
                $.ajax({
                    type: 'POST',
                    url: urlRejectPartNumber,
                    cache: false,
                    //contentType: 'application/json; charset=utf-8',
                    data: { 'vendorProductCatalogId': selectedVendorProductCatalogId, 'vendorPartNumber': selectedVendorPartNumber },
                    dataType: 'json',
                    beforeSend: function () {
                        //$("#btnsave").attr('disabled', true);
                        SGloading('Loading');
                    },
                    success: function (response) {
                        if (parseInt(response) > 0) {

                            ////remove the item from product catalog list ???
                            //for (var i = self.productCatalogList().length - 1; i >= 0; i--) {
                            //    var val = self.productCatalogList()[i];
                            //    if (val.ProductCatalogId() == selectedProductCatalogId) {
                            //        self.productCatalogList.remove(val);
                            //    }
                            //}

                            //remove the item FROM PIMVendor product catalog list
                            //for (var i = self.vendorProductCatalogList().length - 1; i >= 0; i--) {
                            //    var val = self.vendorProductCatalogList()[i];
                            //    if (val.VendorProductCatalogId() == selectedVendorProductCatalogId) {
                            //        self.vendorProductCatalogList.remove(val);
                            //    }
                            //}

                            //remove the item FROM PIMVendor product catalog list
                            var id = selectedVPC.Id();
                            //fadeoOut the selected item
                            $("#" + id + " td").css('background-color', '#FFE8DE');
                            $("#" + id).fadeOut(1000);
                            //remove from list after fadeOut is completed
                            setTimeout(function () {
                                self.vendorProductCatalogList.remove(selectedVPC);
                            }, 1000);


                            //self.vendorProductCatalogList.remove(selectedVPC);


                            //update the count
                            self.refreshMatchSuggestionList();

                            alertmsg('green', "Reject part number successful!");

                            window.location.reload();

                        }
                        else {
                            alertmsg('red', "Reject part number failed!");
                        }
                    },
                    complete: function () {
                        //$("#btnsave").attr('disabled', false);
                        SGloadingRemove();
                    },
                    error: function (xhr, status, error) {
                        //reset variables here
                        alert(status + ' ' + error);
                    }
                });
            }


            // adjust width
            $(".sg-adj-content").css('padding-left', '20px');

            // adjust height
            var winheight = $(window).height();
            var fhoffset = $('.sg-table-sleek-wrap').offset();
            $('.sg-table-sleek-wrap').height((Math.floor(winheight - fhoffset.top - 180)));
            $('.sg-adj-nav').height(winheight - 100);


            var mappingOptionsVPC = {
                'IsChecked': {
                    create: function (options) {
                        var temp = ko.observable(options.data);
                        temp.subscribe(function (val) {
                            var value = options.parent.Id();
                            if (options.parent.IsChecked() == true) {
                                self.vendorProductCatalogCheckedIdList.push(value);
                            }
                            else {
                                self.vendorProductCatalogCheckedIdList.remove(value);
                            }
                        });
                        return temp;
                    }
                }
            }

            try {
                unmatchedData = JSON.parse(unmatchedData);
                //unmatchedData = ko.toJSON(ko.utils.parseJson(unmatchedData));
                ko.mapping.fromJS(unmatchedData, mappingOptionsVPC, self.vendorProductCatalogList);
            } catch (e) {
                //      //console.log(e);
            }
        }

        var vm = new suggestionViewModel();
        ko.applyBindings(vm);


        function slickTable() {
            $('body').on('click', '.sg-table-sleek tr input', function (e) {

                if ($(e.target).is(':checked')) {
                    //$(this).addClass('selected');
                }
                else {
                    //$(this).removeClass('selected');
                }

            });

            $('.selectAll').change(function () {
                if ($(this).is(':checked')) {
                    $('.sg-table-sleek tr').addClass('selected');
                }
                else {
                    $('.sg-table-sleek tr').removeClass('selected');
                }
            });

        };

        $('#suggestionVendorProductCatalogGrid').DataTable();

    }
    catch (e) {
        //console.log(e);
    }

    //$("#vpc-vendor-part-number").on('keyup', function (e) {
    //    //console.log(e);
    //    //var val = $(this).val();
    //    var self = this;
    //    var val = String.fromCharCode(e.keyCode);

    //    $(".vpc-multi-edit-vendor-part-number").each(function (index, element) {
    //        //var aa = $(this).val();
    //        //console.log(index);
    //        //console.log($(element).val());
    //        if (self != this)
    //            $(element).val($(element).val() + val);
    //        return;

    //        var aa = $(element).val();
    //        alert(aa);
    //        return;
    //        var checkedRow = existingVal.parent.parent.parent.find('td:first-child input[type="checkbox"]');
    //        if (checkedRow.is(':checked')) {
    //            existingVal.val(existingVal.val() + val);
    //        }
    //    });
    //});



});