﻿$(document).ready(function () {
    //try {

    //    $.fn.dataTableExt.sErrMode = 'throw';
    //} catch (e) {
    //    console.log(e);
    //}
    var counter = 0;
    var guid = 0;

    var winheight = $(window).height();
    var fhoffset = $('.sg-table-sleek-wrap').offset();
    $('.sg-table-sleek-wrap').height((Math.floor(winheight - fhoffset.top - 180)) / 2);
    $('.sg-adj-nav').height(winheight - 100);

    $('#grid-div').append('<table id="testGrid"><thead><tr></tr></thead></table>');

    //add heading to table
    var i = 0;
    var includedColumns = [];

    function getSingleStagingDataGuid() {
        counter = 0;
        $('input[type=checkbox][class=listStagingDataGuid]').each(function () {
            if (this.checked) {
                id = this.value;
                counter++;
            }
        });
        return id;
    }

    function getSingleProductCatalogData() {
        counter = 0;
        $('input[type=checkbox][class=listproductCatalog]').each(function () {
            if (this.checked) {
                id = this.value;
                counter++;
            }
        });
        return id;
    }


    function initializeSuggestionPossibles() {

        var includedSuggestedColumns = [];
        var j = 0;
        var guidvalue = $("#hdntxtboxSuggestionLoadGuid").val();

        if (guidvalue == "" || counter != 1) {
            alertmsg('red', 'Please select one staging data');
            return;
        }
        

        // Ajax call
        $.ajax({
            type: 'POST',
            url: urlGetSuggestedData,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'guid': guidvalue },
            dataType: 'json',
            beforeSend: function () {
              
                SGloading('Loading');
            },
            success: function (response) {
                response = JSON.parse(response);
                
          
                try {
                    $('#grid-suggestion').html('');

                    try {
                        if (response.aaData[0][0] == "Error") {
                            alertmsg("red", "Unable to load");
                            return;
                        }
                    } catch (e) {
                        // do nothing, continue ahead
                    }

                    $('#grid-suggestion').append('<table id="suggestedDatatable"><thead><tr></tr></thead></table>');
               

                 $.each(response.sColumns, function (key, value) {

                       

                        if (j > 0) {
                          $('#suggestedDatatable thead tr').append('<th>' + value + '</th>');
                          includedSuggestedColumns.push({ "sName": value });
                            console.log(j);

                        } else {
                           $('#suggestedDatatable thead tr').append('<th>' + "" + '</th>');
                            includedSuggestedColumns.push({
                                "sName": value,
                                "bSearchable": false,
                                "bSortable": false,
                                "sWidth": 10,

                                "mRender": function (data, type, full) {
                                    return '<input type="checkbox" class="listproductCatalog" value="' + data + '">'
                                }
                            });
                            console.log(j);

                        }
                        j++;
                     
                    });

                    $("#vendorProductCatalog").show();



                    $('#suggestedDatatable').dataTable({
                        "bProcessing": true,
                        "bServerSide": true,
                        "scrollX": true,
                        "bSort": false,
                        "bFilter": false,
                        "aLengthMenu": dataTableMenuLength,
                        "iDisplayLength": dataTableDisplayLength,
                        "sAjaxSource": urlProductSuggestedDataTable + '?guid=' + guidvalue,
                        "sServerMethod": "POST",
                        "aoColumns": includedSuggestedColumns

                    });

                    //$('#suggestedDatatable').DataTable({
                    //    "destroy": true,
                    //    "data": response.aaData,
                    //    "scrollX": true,
                    //    "aLengthMenu": dataTableMenuLength,
                    //    "iDisplayLength": dataTableDisplayLength,
                    //    "aoColumns": includedSuggestedColumns
                    //});

                }
                catch (e) {
                    alert(e);
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });
        
           
    }

    $.each(returnObject.sColumns, function (key, value) {

        var headerRemove_ = value.replace(/_/gi, " ");
        if (i < 16) {
       
        if (i > 0) {
            $('#testGrid thead tr').append('<th>' + headerRemove_ + '</th>');
            includedColumns.push({ "sName": value });

        } else {
            $('#testGrid thead tr').append('<th>' + "" + '</th>');
            includedColumns.push({
                "sName": value,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": 10,

                "mRender": function(data, type, full) {
                    return '<input type="checkbox" class="listStagingDataGuid" value="' + data + '">'
                }
            });

        }
        i++;
       }

    });
    
   

    var table = $('#testGrid').dataTable({
       
        "bProcessing": true,
        "bServerSide": true,
        "destroy": true,
        "scrollX": true,
        "bSort": false,
        "bFilter": false,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlProductSuggestionDataTable + '?vendorId=' + vendorId + '&skuType=' + skuType + '&errorType=' + errorType + '&logUserUploadId=' + logUserUploadId + '&stagingId=' + stagingId,
        "sServerMethod": "POST",
        "aoColumns": includedColumns
        

    });
    //add data to table via datatable
    //var table = $('#testGrid').DataTable({
    //    destroy: true,
    //    data: returnObject.aaData,
    //    "scrollX": true,
    //    "aLengthMenu": dataTableMenuLength,
    //    "iDisplayLength": dataTableDisplayLength,
    //    "aoColumns": includedColumns
    //});


   



    $('#btnAssignSuggestedData').click(function () {

        var productId = getSingleProductCatalogData();

        if (productId == "" || counter != 1) {
            alertmsg('red', 'Please select product catalog data to associate');
            return;
        }

        var guidvalue = $("#hdntxtboxSuggestionLoadGuid").val();
        if (guidvalue == "") {
            return;
        }

        var validationUrl = createUrl + "?vendorId=" + vendorId + "&skuType=" + skuType + "&errorType=" + errorType + "&logUserUploadId=" + logUserUploadId + "&stagingId=" + stagingId + "&productId=" + productId + "&guid=" + guidvalue;
 
  window.location.href = validationUrl;
        // Ajax call
        //$.ajax({
        //    type: 'POST',
        //    url: urlAssignSuggestedData,
        //    cache: false,
        //    data: {
        //        'productId': productId,
        //        'guid': guidvalue,
        //        'stagingId': stagingId
        //    },
        //    dataType: 'json',
        //    beforeSend: function () {

        //        SGloading('Loading');
        //    },
        //    success: function (response) {
        //        if (response=="success") {
        //            alertmsg('green', 'Successfully Assigned');
        //            window.location.reload();
        //        }
        //    },
        //    complete: function () {

        //        SGloadingRemove();
        //    },
        //    error: function (xhr, status, error) {

        //        alert(status + ' ' + error);
        //    }
        //});

    });

    $('#btnLoadSuggestion').click(function () {

        document.getElementById('hdntxtboxSuggestionLoadGuid').value = getSingleStagingDataGuid();

        initializeSuggestionPossibles();
       
    });



    try {

        function slickTable() {
            $('body').on('click', '.sg-table-sleek tr input', function (e) {

                if ($(e.target).is(':checked')) {
                    //$(this).addClass('selected');
                }
                else {
                    //$(this).removeClass('selected');
                }

            });

            $('.selectAll').change(function () {
                if ($(this).is(':checked')) {
                    $('.sg-table-sleek tr').addClass('selected');
                }
                else {
                    $('.sg-table-sleek tr').removeClass('selected');
                }
            });

        };

        //$('#suggestionProductCatalogGrid').DataTable();

    }
    catch (e) {
        alert('error' + e);
    }

});