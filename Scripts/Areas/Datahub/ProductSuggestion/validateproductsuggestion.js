﻿$("#productSuggestionTable").dataTable();


function OnSuccess(data) {


    $('.stagingValue').css('background', 'none');

    $("input.ErrorDescriptionText").each(function () {
        var element = $(this);
        element.val("");

    });


    if (data == "success") {
        alertmsg('green', 'Product Catalog assign to staging data');
        window.location.href = urlProductSuggestion;
    }
    else if (data == "error") {
        alertmsg('yellow', 'Required field not mapped');
    }
    else {
        $.each(data, function (index, value) {
            //console.log(value);
            if (document.getElementById(value.ErrorAtColumn)) {
                $("#" + value.ErrorAtColumn + "").css('background', 'red');
                $("#" + value.ErrorAtColumn + "_description").val(value.ErrorDescription);

            } else {
                alertmsg('red', value.ErrorDescription);

            }
        });
    }
}


