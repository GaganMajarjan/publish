﻿$(document).ready(function () {
    initializeUomDataTable();
});

function initializeUomDataTable() {
    $('#UomDataTable').dataTable({
        "destroy": true,
        "bJQueryUI": true,
        "bServerSide": true,
        "bFilter": true,
        "scrollX": true,
        "bSort": false,
        "bScrollCollapse": true,

        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": tableToolsPath,
            "aButtons": tableToolsOptions
        },
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlListUom,
        "sServerMethod": "GET",

        // Initialize our custom filtering buttons and the container that the inputs live in
        //filterOptions: { searchButton: "ProductCatalogSearchText", clearSearchButton: "ProductCatalogClearSearch", searchContainer: "ProductCatalogSearchContainer" },
        "fnPreDrawCallback": function () {
            // gather info to compose a message
            var ob = this;
            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
            //SGtableloading('fa-plane', '', this);


        },
        "fnDrawCallback": function (settings) {
            // in case your overlay needs to be put away automatically you can put it here
            var divid = this.closest('.dataTables_wrapper').attr('id');
            SGtableloadingRemove(divid);
            rebuilttooltip();
        }
    });
}

$('#btnAddUom').click(function () {

    window.location.href = urlCreateUom;
})

$('#btnUpdateUom').click(function () {
    var i = 0;
    var selectedId = 0;
    $('input[type=checkbox][class=PIMUomSelector]').each(function () {
        if (this.checked) {
            i++;
            selectedId = this.value;
        }
    });
    if (i === 0) {
        udh.notify.error('Please select uom to edit');
        e.preventDefault();
    } else if (i > 1) {
        udh.notify.error('Please select single uom to edit');
        e.preventDefault();
    } else {
        window.location.href = urlUpdateUom + "?Id=" + selectedId;
    }

})

var eachIds = [];
$('#btnDeleteUom').click(function () {

    eachIds = [];
    var selector = 'input[type=checkbox][class=PIMUomSelector]';
    $(selector)
        .each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });

    if (eachIds.length == 0) {
        alertmsg('Please select at least one uom')
        return;
    }

    $('#DeleteUomModal').modal('show');

})


$('#btnDeleteUomModalYes').click(function () {

    $.ajax({
        url: urlDeleteUom,
        data: { 'idList': eachIds },
        type: 'POST',
        beforeSend: function () {
            SGloading("Loading");
        },
        success: function (data) {
            if (data && data.Status === true) {
                udh.notify.success(data.Message);
                $('#UomDataTable').dataTable().fnReloadAjax();
                if ($('#selectAllCheckBoxUom').prop("checked") == true) {
                    $("#selectAllCheckBoxUom").prop("checked", false);
                }
            } else {
                udh.notify.error(data.Message);
            }

        },
        complete: function () {
            eachIds = [];
            SGloadingRemove();
            $('#btnDisableUomModalNo').click();

        }
    });
})





