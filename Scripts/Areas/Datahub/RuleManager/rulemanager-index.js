﻿$(document).ready(function() {


//Note: no single-quote in 'html.raw(...)' below for mention-data

    //Render ruleGrid as datatable
    function initializeRuleGrid() {
        $('#RuleGrid').DataTable({
            "destroy": true,
            "scrollX": false,
            "aLengthMenu": dataTableMenuLength,
            "iDisplayLength": dataTableDisplayLength,
            "sAjaxSource": urlGetTransformationRuleDataTable,
            "bServerSide": true,
            "bProcessing": true,
            "bSort": false,
            "bFilter": false,
            "aoColumns": [
                {
                    "bSearchable": false,
                    "bSortable": false,
                    "mRender": function(data, type, full) {
                        return '<input type="checkbox" class="chkRuleClass" value="' + data + '">'
                    }
                },
                {
                    "bSortable": false,
                    "sName": "RuleName"
                },
                {
                    "bSortable": false,
                    "sName": "RuleType"
                },
                {
                    "bSortable": false,
                    "sName": "CreatedBy"
                },
                {
                    "bSortable": false,
                    "sName": "CreatedDate"
                },
                {
                    "bSortable": false,
                    "sName": "Status"
                }
            ],
            "fnPreDrawCallback": function () {
                // gather info to compose a message
                var ob = this;
                SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                //SGtableloading('fa-plane', '', this);


            },
            "fnDrawCallback": function () {
                // in case your overlay needs to be put away automatically you can put it here
                var divid = this.closest('.dataTables_wrapper').attr('id');
                SGtableloadingRemove(divid)
            }
        });
    }

    initializeRuleGrid();


    $('#btnLoadRuleDetail').click(function() {

        //Get id from UI
        var ruleId = '';
        var count = 0;

        $('input[type=checkbox][class=chkRuleClass]').each(function() {
            if (this.checked) {
                ruleId = $(this).val();
                count++;
            }
        });

        //Validation (check one)
        if (count != 1) {
            alertmsg('red', 'Please select one rule');
            return;
        }

        //Get details from controller - ajax call
        $.ajax({
            type: 'POST',
            url: urlGetTransformationRule,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'id': ruleId },
            dataType: 'json',
            beforeSend: function() {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function(response) {
                //console.log(response);
                if (response == false) {
                    alertmsg('red', "Load failed");
                    return;
                }

                $('#FormulaName').val(response.FormulaName);
                $('#CheckCondition').val(response.CheckCondition);
                $('#PassCondition').val(response.PassCondition);
                $('#FailCondition').val(response.FailCondition);

            },
            complete: function() {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function(xhr, status, error) {
                //reset variables here
                //alert(status + ' ' + error);
                window.reload();
            }
        });


        //Display result


    });


    $('#btnUpdateRuleManager').click(function() {
        var i = 0;
        var checkboxIds = "";
        $('input[type=checkbox][class=chkRuleClass]').each(function () {

            if (this.checked) {
                i++;
                checkboxIds = checkboxIds + this.value;
            }
        });

        if (i == 0)
            alertmsg("red", "Please select one to edit.");
        else if (i > 1)
            alertmsg("red", "Please select only one to edit.");
        else {
            window.location = urlEdit + "/" + checkboxIds;
        }
    });

    $('#btnCreateRuleManager').click(function() {
        window.location = urlCreateRuleManager;
    });


    $('#btnDeleteRuleManager').click(function() {
        var i = 0;
        var checkboxIds = "";
        $('input[type=checkbox][class=chkRuleClass]').each(function () {

            if (this.checked) {
                i++;
                checkboxIds = checkboxIds + this.value + ",";
            }
        });

        if (i == 0)
            alertmsg("red", "Please select one to delete.");

        else {
            $("#idContainerHiddenFieldForDelete").val(checkboxIds);
            $('#divConfirmDelete').modal("show");
        }
    });


    $('#btndivConfirmDeleteYes').click(function (e) {
        var IdsCommaSeparated = $("#idContainerHiddenFieldForDelete").val();
        //console.log(IdsCommaSeparated);
        //console.log(urlDelete);
        $.ajax({
            url: urlDelete,
            data: { deleteIdsCommaSeparated: IdsCommaSeparated },
            type: 'POST',
            cache: false,
            dataType: "json",
            success: function (data) {
                if (data.Status === "OK") {
                    location.reload();
                    alertmsg("green", data.StatusMessage);
                }
                else {
                    //console.log(data);
                    alertmsg("red", data.StatusMessage);
                }
            }
        });

    });

    $('#btnDisableRuleManagerr').click(function () {
        var i = 0;
        var checkboxIds = "";
        $('input[type=checkbox][class=chkRuleClass]').each(function () {

            if (this.checked) {
                i++;
                checkboxIds = checkboxIds + this.value + ",";
            }
        });

        if (i == 0)
            alertmsg("red", "Please select one to disable.");

        else {
            $("#idContainerHiddenFieldForDelete").val(checkboxIds);
            $('#divConfirmDisable').modal("show");
        }
    });
    
    $('#btndivConfirmDisableYes').click(function (e) {
        var IdsCommaSeparated = $("#idContainerHiddenFieldForDelete").val();
        
        $.ajax({
            url: urlDisable,
            data: { disableIdsCommaSeparated: IdsCommaSeparated },
            type: 'POST',
            cache: false,
            dataType: "json",
            success: function (data) {
                if (data == "OK") {
                    location.reload();
                    alertmsg("green", "The selected rule(s) disabled successfully.");
                }
                else {
                    //console.log(data);
                    alertmsg("red", "An Error occured while disabling the selected rule(s).");
                }
            }
        });

    });
});