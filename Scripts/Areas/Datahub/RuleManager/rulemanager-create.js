//Initialize global variables & select2.js for all Dropdownlists
var mentionJsDataContainer = "";
var queryBuilderJsDataContainer = "";
$("select").select2();

$("#withRuleCombinationDiv").hide();
$("#withDirectQueryDiv").show();
$("#FailStatement").hide();

//For Direct Query Rules

var populateQueryBuilder = function (id) {

    if (queryBuilderJsDataContainer != null && queryBuilderJsDataContainer.filters != null) {
        $("#" + id).queryBuilder(queryBuilderJsDataContainer);
    }
}

var addRule = function () {

    var scopeTypeId = $('#FormulaScopeTypeSdvId').val();
    var scopeId = $('#FormulaScopeId').val();
    if (scopeTypeId == '' || scopeId == '') {
        alertmsg("red", selectOneRuleScopeMessage);
        return;
    }

    //var labelId = $('#ruleManagerUl li:last-child div').attr('id');
    var labelId = $('#ruleManagerUl').children('li').last().attr('id');
    var numberedId = "1";
    if (labelId != null)
        numberedId = parseInt(labelId.substr(labelId.indexOf('_') + 1, labelId.length)) + 1;

    $('#ruleManagerUl')
        .append(
            $('<li>').attr({ id: 'tr_' + numberedId, class: 'list-rule' })
            .append('<div class="row"><div id="builder_' + numberedId + '" class="col-md-7 ruleBuilder equalheight"></div>' +
                '<span class="col-md-3 equalheight"><textarea id="passStatement_' + numberedId + '" style="resize:vertical;" type="text" class="form-control custom-rule-container pass-statementContainer"  placeholder = "Type your pass statement here to execute if condition is met. e.g. @CostPerLot / @QuantityPerLot"></textarea> </span>' +
                '<span class="col-md-2"><button onclick = "updateNewHeight(this);" id="remove_' + numberedId + '" type="button" class="btn btn-xs btn-danger pull-left remove-btn" data-delete="rule" data-toggle="tooltip" data-placement="top" title="' + removeRuleMessage + '"> <i class="glyphicon glyphicon-remove"></i> ' + deleteMessage + ' </button> </span> <div>'));

    populateQueryBuilder("builder_" + numberedId);

    $(".custom-rule-container").mention({
        delimiter: '@',
        users: mentionJsDataContainer.users
    });
}

function PopulateMentionJs(scopeTypeId, scopeId) {

    $.ajax({
        url: urlGetMentionJsData,
        data: { scopeTypeId: scopeTypeId, scopeId: scopeId },
        type: 'GET',
        cache: false,
        traditional: true,
        beforeSend: function () {

            SGloading('Loading');

        },
        success: function (data) {
            if (data !== "EMPTY") {
                mentionJsDataContainer = JSON.parse(data);

                $(".custom-rule-container").mention({
                    delimiter: '@',
                    queryBy: ['name', 'username'],
                    users: mentionJsDataContainer.users
                });

            }
        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            alertmsg("red", status + ' ' + error);
        }
    });
}

$("#FormulaScopeTypeSdvId").change(function (val) {

    $("#FormulaScopeId").empty();
    var typeId = val.currentTarget.value;
    if (typeId == null || typeId === "") {
        $("#FormulaScopeId").append(new Option(pleaseSelectMessage, ""));
        return;
    }

    $.ajax({
        url: urlGetFormulaScopeList,
        data: { scopeTypeId: typeId },
        type: 'GET',
        cache: false,
        traditional: true,
        beforeSend: function () {

            SGloading('Loading');


        },
        success: function (data) {

            if (data !== "EMPTY") {
                var jsondata = $.parseJSON(data);

                $("#FormulaScopeId").append(new Option(pleaseSelectMessage, ""));
                $.each(jsondata, function (index, value) {
                    $("#FormulaScopeId").append(new Option(value.Name, value.Id));
                });
            }


        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            alertmsg("red", status + ' ' + error);
        }
    });
});

$('#addMoreRule').click(function () {

    addRule();

    $('button[data-add="group"]').hide();

    if ($('#ruleManagerUl li').length > 1) {
        $("#FailStatement").show();
    } else {
        $("#FailStatement").hide();

    }

});

$(document).on('click', ".remove-btn", function () {

    var dataTypeId = $(this).attr('id');
    var numberedId = dataTypeId.substr(dataTypeId.indexOf('_') + 1, dataTypeId.length);

    $("#tr_" + numberedId).remove();

    if ($('#ruleManagerUl li').length > 1) {
        $("#FailStatement").show();
    } else {
        $("#FailStatement").hide();

    }
    $('.equalheight').equalHeights();

});


//For Combinatorial Rules
var addSelectedRule = function (ruleId, ruleText, parameters) {

    var labelId = $('#selectedRulesUl li:last').attr('id');

    var numberedId = "1";
    if (labelId != null)
        numberedId = parseInt(labelId.substr(labelId.indexOf('_') + 1, labelId.length)) + 1;
    if ($("#" + ruleId).length) {
        alertmsg("red", "Duplication of rule selection is not allowed. Please select unique one.");
        return;
    }

    $('#selectedRulesUl')
        .append(
            $('<li>').attr('id', 'row_' + numberedId)
            .append('<span class="col-md-5"><strong><label id="' + ruleId + '" class="rulesSelected">' + ruleText + '</label></strong></span>' +
                '<span class="col-md-6"><label id="parameters_' + ruleId + '" class="rulesSelected">' + parameters + '</label></span>' +
                '<span class="col-md-1 text-right"><button id="removeRule_' + numberedId + '" type="button" class="btn btn-xs btn-danger  remove-selected-rule" data-delete="rule" data-toggle="tooltip" data-placement="top" title="' + removeRuleMessage + '"> <i class="glyphicon glyphicon-remove"></i> ' + removeMessage + ' </button> </span>'));


}

$(document).on('click', 'button[data-add="rule"]', function (e) {
    var list = $(e.target).closest("li");
    if (list.length) {
        list.find('.equalheight').css('height', 'auto');
        //console.log('hey I m trigger');
        list.find('.equalheight').equalHeights();
    }
});


$(document).on('click', 'button[data-delete="rule"]', function () {
    //var uplist = $(f.target).parentsUntil($('#ruleManagerUl'));
    $(".list-rule").each(function () {
        $(this).find('.equalheight').css('height', 'auto');
        $(this).find('.equalheight').equalHeights();
    });

});


$(document).on('click', ".remove-selected-rule", function () {

    var dataTypeId = $(this).attr('id');
    var numberedId = dataTypeId.substr(dataTypeId.indexOf('_') + 1, dataTypeId.length);

    $("#row_" + numberedId).remove();

});

var getQueryBuilderComponent = function (isInEditMode) {

    var scopeTypeId = $('#FormulaScopeTypeSdvId').val();
    var scopeId = $('#FormulaScopeId').val();
    if (scopeTypeId === '' || scopeId === '')
        return;

    $.ajax({
        url: urlGetColumnsForQueryBuilder,
        data: { scopeId: scopeId, scopeTypeId: scopeTypeId, isInEditMode: isInEditMode },
        type: 'GET',
        cache: false,
        traditional: true,
        beforeSend: function () {
            SGloading('Loading');
        },
        success: function (response) {
            try {


                if (response != null && response.Status === true) {
                    queryBuilderJsDataContainer = $.parseJSON(response.Data);
                    if (isInEditMode) {

                        if (ifElseBlockJsonViewBag && ifElseBlockJsonViewBag !== "[]") {

                            var ifElseBlockJson = fixSQLRule(ifElseBlockJsonViewBag);

                            var totalNumberOfRows = Object.keys(ifElseBlockJson).length;
                            var tableRows = $("#ruleManagerUl li").length;
                            while (parseInt(tableRows) - 1 < parseInt(totalNumberOfRows)) {
                                addRule();
                                tableRows = tableRows + 1;
                            }

                            var i = 1;
                            
                            $.each(ifElseBlockJson, function (index, val) {
                                //$("#builder_" + i).queryBuilder('setRulesFromSQL', val.IfCondition);
                                //console.log(val.IfCondition);
                                //console.log(fixJsonQuote(val.IfCondition));

                                $("#builder_" + i).queryBuilder('setRules', val.IfCondition[0]);
                                $("#passStatement_" + i).val(val.ElseStatement);
                                i++;
                            });

                            if ($('#ruleManagerUl li').length > 1) {
                                $("#FailStatement").show();
                            } else {
                                $("#FailStatement").hide();

                            }
                            $('button[data-add="group"]').hide();
                            var failStatement = $('#ruleCombinationConditionDiv').html(failStatementViewBag).text();
                            $("#FailStatement").val(failStatement);
                        }
                    }

                }
                else if (response != null && response.Status === false) {

                    //alertmsg("red", response.Message);
                }
            } catch (e) {
                SGloadingRemove();
                //console.log(e);
                alertmsg("red", errorOccuredMessage);

            }

        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            alertmsg("red", status + ' ' + error);
        }
    });

}

var getRuleCombination = function (isInEditMode) {

    var scopeTypeId = $('#FormulaScopeTypeSdvId').val();
    var scopeId = $('#FormulaScopeId').val();
    if (scopeTypeId === '' || scopeId === '')
        return;
    
    $.ajax({
        url: urlGetAvailableRules,
        data: { formulaScopeId: scopeId, formulaScopeTypeId: scopeTypeId },
        type: 'GET',
        cache: false,
        traditional: true,
        beforeSend: function () {
            SGloading('Loading');
        },
        success: function (response) {

            if (response != null && response.Status === true) {

                var jsondata = $.parseJSON(response.Data);
                $("#availableRulesList").append(new Option(pleaseSelectMessage, ""));
                //populate available rules list dropdown
                $.each(jsondata, function (index, value) {
                    //var option = new Option(value.Description, value.Id);
                    var option = '<option value="' + value.Id + '" data-parameters="' + value.Parameters +
                        '">' + value.Description + '</option>';

                    //option.setAttribute('data-parameters', value.Parameters);
                    var optGroupWithOption = "<optgroup label='" + value.Name + "'>" +
                        option + "</optgroup>";

                    $("#availableRulesList").append(optGroupWithOption);
                });

                //populate rule combination query builder
                $("#ruleCombinationQuerybuilder").queryBuilder(queryBuilderJsDataContainer);

                $('button[data-add="group"]').hide();

                //prepare edit UI
                if (isInEditMode) {

                    if (ruleCombinationJsonViewBag && ruleCombinationJsonViewBag !== "[]") {

                        var ruleCombination = fixSQLRule(ruleCombinationJsonViewBag);
                        $.each(ruleCombination, function (index, val) {
                            addSelectedRule(val.Id, val.Name, val.Parameters);

                        });

                    }

                    if (ruleCombinationCondition) {
                        var condition = $('#ruleCombinationConditionDiv').html(ruleCombinationCondition).text();
                        $("#ruleCombinationQuerybuilder").queryBuilder('setRules', JSON.parse(condition)[0]);
                    }

                }

            }
            else if (response != null && response.Status === false) {
                $("#availableRulesList").append(new Option(pleaseSelectMessage, ""));
                if ($('[id="HasChildRules_1"]').is(':checked')) {
                    alertmsg("red", response.Message);
                }
            }


        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            alertmsg("red", status + ' ' + error);
        }
    });
}

$("#FormulaScopeId").change(function () {

    var scopeTypeId = $('#FormulaScopeTypeSdvId').val();
    var scopeId = $('#FormulaScopeId').val();
    if (scopeTypeId !== "" && scopeId !== "") {
        PopulateMentionJs(scopeTypeId, scopeId);

        getQueryBuilderComponent(false);

        $("#availableRulesList").empty();

        setTimeout(function () {
            getRuleCombination(false);
        }, 1000);


    }

});

$("#availableRulesList").change(function () {

    var selectedRuleValue = $("#availableRulesList").val();
    var selectedRuleText = $("#availableRulesList option:selected").parent().attr('label');
    var selectedParams = $("#availableRulesList option:selected").data("parameters");

    if (selectedRuleValue == null || selectedRuleValue === "") {
        //$("#availableRulesList").append(new Option(pleaseSelectMessage, ""));
        return;
    }

    addSelectedRule(selectedRuleValue, selectedRuleText, selectedParams);

});

$("#HasChildRules_0").change(function () {
    $("#withRuleCombinationDiv").hide();
    $("#withDirectQueryDiv").show();
});

$("#HasChildRules_1").change(function () {

    var scopeTypeId = $('#FormulaScopeTypeSdvId').val();
    var scopeId = $('#FormulaScopeId').val();
    if (scopeTypeId == '' || scopeId == '') {
        alertmsg("red", selectOneRuleScopeMessage);
        $("#HasChildRules_0").prop('checked', 'checked');
        return;
    }
   // $('button[data-add="group"]').hide();

    $("#withDirectQueryDiv").hide();
    $("#withRuleCombinationDiv").show();

    $("#ruleCombinationQuerybuilder").queryBuilder(queryBuilderJsDataContainer);


});

//Save Rule Information
$('#btn-save').click(function () {

    SaveRuleData();

});

function SaveRuleData() {
    $("#ruleCombinationQuerybuilder").queryBuilder(queryBuilderJsDataContainer);

    //Construct formula view model
    var formulaModel = new FormulaViewModel();
    formulaModel.FormulaName = $('#FormulaName').val();
    formulaModel.FormulaScopeId = $('#FormulaScopeId').val();
    formulaModel.FormulaScopeTypeSdvId = $('#FormulaScopeTypeSdvId').val();
    formulaModel.FormulaTypeSdvId = $('#FormulaTypeSdvId').val();
    formulaModel.FailStatement = $("#FailStatement").val();
    formulaModel.FormulaDescription = $("#FormulaDescription").val();
    formulaModel.Id = $("#Id").val();

    var tempParamHolder = [];
    var validationError = '';

    if (formulaModel.FormulaName === "" || formulaModel.FormulaScopeId === "" || formulaModel.FormulaTypeSdvId === "" || formulaModel.FormulaScopeTypeSdvId === "") {
        alertmsg("red", "Please enter the required fields");
        return;
    }
    
    
    $('.ruleBuilder').each(function () {

        var sqlQuery = $("#" + $(this).attr('id')).queryBuilder('getSQL', false);
        var sqlJson = $("#" + $(this).attr('id')).queryBuilder('getRules', false);

        if (sqlQuery.sql === "") {
            validationError = 'Please enter the values for all If conditions';
            alertmsg("red", validationError);
            return;
        }
        formulaModel.SqlJsonString.push(JSON.stringify(sqlJson));
        formulaModel.CheckConditions.push(sqlQuery.sql.replace("\n", " "));
    });

    $('.pass-statementContainer').each(function () {

        if ($(this).val() === "") {
            validationError = 'Please enter the values for all Then statements';

            alertmsg("red", "Please enter the values for all Then statements");
            return;
        }

        if ($(this).val().indexOf("=") > -1) {
            ////console.log($(this).val());

            validationError = 'Equality operator cannot be used in Then statements';

            alertmsg("red", "Equality operator cannot be used in Then statements");
            return;
        }

        formulaModel.PassStatements.push($(this).val());
    });

    if (validationError !== '') {
        alertmsg("red", validationError);
        return;
    }

    $('.ruleBuilder, .custom-rule-container').each(function () {

        var query = "";
        if ($(this).attr('class').indexOf('ruleBuilder') > -1) {

            query = $("#" + $(this).attr('id')).queryBuilder('getSQL', false).sql.replace("\n", " ");
        } else {

            query = $(this).val();
        }

        var paramCollection = getMentions(query);
        if (paramCollection != null) {
            $.each(paramCollection, function (i, val) {
                tempParamHolder.push(val);
            });

        }
    });

    $('.rulesSelected').each(function () {
        formulaModel.ChildRules.push($(this).attr('id'));
        ////console.log(formulaModel.ChildRules);
    });

    formulaModel.FormulaParameters = tempParamHolder;
    formulaModel.IsRuleFromCombination = $('[id="HasChildRules_1"]').is(':checked');

    if (formulaModel.IsRuleFromCombination === false && (formulaModel.FailStatement === "" || formulaModel.FailStatement == null)) {
        alertmsg("red", "Please enter the value for default value section.");
        return;
    }
    
    formulaModel.RuleCombinationCondition = $("#ruleCombinationQuerybuilder").queryBuilder('getSQL', false).sql.replace("\n", "");
    formulaModel.SqlJsonStringForCombination = JSON.stringify($("#ruleCombinationQuerybuilder").queryBuilder('getRules', false));
    if (formulaModel.RuleCombinationCondition) {
        var paramCollection = getMentions(formulaModel.RuleCombinationCondition);
        if (paramCollection) {
            $.each(paramCollection, function(i, val) {
                formulaModel.RuleCombinationParameters.push(val);
            });
        }
    }
    

    //// Ajax call
    $.ajax({
        type: 'POST',
        url: urlCreateRulesManager,
        cache: false,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(formulaModel),
        dataType: 'json',
        beforeSend: function () {
            SGloading('Saving Rule...');
        },
        success: function (response) {
            console.log(response);
            if (response != null && response.Status === true) {
                alertmsg('green', response.Message);
                window.location = urlIndexRuleManager;
            }
            else if (response != null && response.Status === false) {
                alertmsg('red', response.Message);
            } else {
                alertmsg('red', errorOccurredMessage);
            }
        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            alert(status + ' ' + error);
        }
    });


}

//Common Methods
var getMentions = function (val) {
    //console.log(val);
    while (parseInt(val.indexOf('QUERY__BUILDER__')) > -1) {
        val = val.replace("QUERY__BUILDER__", "@");
    }
    //(@[a-zA-Z_0-9%]+(([-][a-zA-Z_%]([a-zA-Z_0-9%]+)?))+)|(@[a-zA-Z0-9_%]+)
    //var pattern = /\B@[a-z0-9_-]+/gi;

    //var pattern = /\B(@[a-zA-Z_0-9%]+(([-][a-zA-Z_%]([a-zA-Z_0-9%]+)?))+)|(@[a-zA-Z0-9_%]+)/gi;

    var output = val.match(pattern);
    return output;
}

var FormulaViewModel = function () {

    this.Id = 0;
    this.FormulaName = "";
    this.FormulaScopeTypeSdvId = "";
    this.FormulaTypeSdvId = "";
    this.FormulaScopeId = "";
    this.FormulaDescription = "";

    this.CheckConditions = [];
    this.PassStatements = [];
    this.FailStatement = "";
    this.FormulaParameters = [];
    this.ChildRules = [];
    this.IsRuleFromCombination = "";
    this.RuleCombinationCondition = "";
    this.RuleCombinationParameters = [];
    this.SqlJsonString = [];
    this.SqlJsonStringForCombination = "";

}

var GenerateEditUi = function () {
   
    //For Single Rule component
    getQueryBuilderComponent(true);

    SGloading('Loading');
    //For Rules Combination component
    setTimeout(function() {
        getRuleCombination(true);
        SGloadingRemove();
    },2000);

    //For showing the appropriate rule div - Rule or Rule Combination
    if ($('[id="HasChildRules_1"]').is(':checked')) {

        $("#withDirectQueryDiv").hide();
        $("#withRuleCombinationDiv").show();
        
    }

    //For populating the mention.js data for columns
    if (mentionJsData != null && mentionJsData !== "") {

        mentionJsDataContainer = fixJsonQuote(mentionJsData);
        $(".custom-rule-container").mention({
            delimiter: '@',
            queryBy: ['name', 'username'],
            users: mentionJsDataContainer.users
        });
    }

    
}
