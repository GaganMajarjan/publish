﻿var stepped = 0, chunks = 0, rows = 0;
var start, end;
var parser;
var pauseChecked = true;
var printStepChecked = true;
var header = [];
var fileData;
var delimiterData = "";
var newLineVariable = "\n";
var fileNameGlobal = "";
var loopingPasing = true;
function generate_table(header, data) {
    var odd_even = false;
    var tbl_body = "";
    var tbl_row = "";
    var flag = 0;
    var count = 0;
  
    $.each(data, function() {
        tbl_row = "";
        if (flag === 0) {
                $.each(header, function(k, v) {
                tbl_row += "<th>" + v + "</th>";
                count++;
            });
            tbl_body += "<thead><tr class=\"" + (odd_even ? "odd" : "even") + "\">" + tbl_row + "</tr></thead>";
            flag = 1;

            tbl_row = "";
            $.each(this, function (k, v) {
            tbl_row += "<td>" + v + "</td>";
            });
            tbl_body += "<tr class=\"" + (odd_even ? "odd" : "even") + "\">" + tbl_row + "</tr>";
            odd_even = !odd_even;

        } else {
            $.each(this, function(k, v) {
            tbl_row += "<td>" + v + "</td>";
        });

        tbl_body += "<tr class=\"" + (odd_even ? "odd" : "even") + "\">" + tbl_row + "</tr>";
        odd_even = !odd_even;


        }
    });
  
    $("#previewTable").html(tbl_body);

}


function buildConfig() {
    return {
        delimiter: "",
        newline: newLineVariable,
        header: true,
        quoteChar: '"',
        escapeChar: '"',
        dynamicTyping: "",
        preview: parseInt(5),
        step: undefined,
        encoding: "",
        worker: "",
        comments: "",
        complete: completeFn,
        error: errorFn,
        download: "",
        fastMode: "",
        skipEmptyLines: "",
        chunk: undefined,
        beforeFirstChunk: undefined,
    };

    function getLineEnding() {

        if ($('#newline-n').is(':checked'))
            return "\n";
        else if ($('#newline-r').is(':checked'))
            return "\r";
        else if ($('#newline-rn').is(':checked'))
            return "\r\n";
        else
            return "";
    }
}

function stepFn(results, parserHandle) {
    stepped++;
    rows += results.data.length;

    parser = parserHandle;

    if (pauseChecked) {
        //console.log(results, results.data[0]);
        parserHandle.pause();
        return;
    }

    if (printStepChecked) {
        //console.log(results, results.data[0]);
    }
}

function chunkFn(results, streamer, file) {
    if (!results)
        return;
    chunks++;
    rows += results.data.length;

    parser = streamer;

    if (printStepChecked)
        //console.log("Chunk data:", results.data.length, results);

    if (pauseChecked) {
        //console.log("Pausing; " + results.data.length + " rows in chunk; file:", file);
        streamer.pause();
        return;
    }
}

function errorFn(error, file) {
    //console.log("ERROR:", error, file);

}

function completeFn() {
    end = performance.now();
    if (!$('#stream').prop('checked')
			&& !$('#chunk').prop('checked')
			&& arguments[0]
			&& arguments[0].data)
        rows = arguments[0].data.length;
    var headerJson = arguments[0].meta.fields;
    //console.log();
    delimiterData = arguments[0].meta.delimiter;
    //console.log(arguments[0].data);
    fileData = arguments[0].data;
    if (jQuery.isEmptyObject(fileData)) {
        if (loopingPasing) {
           loopingPasing = false;
           newLineVariable = "\r\n";
            $("#parse").click();
        }
    } else {
        loopingPasing = true;
    }
    //console.log(arguments[0].meta.fields);
    //$("#filePreview").replaceWith(" <label id='filePreview'>Top 5 rows of the selected file</label>");
    $.each(headerJson, function (key, value) {

        header.push(value);
    });
    //   //console.log("Finished input (async). Time:", end - start, arguments);
    //  //console.log("Rows:", rows, "Stepped:", stepped, "Chunks:", chunks);
}
$(document).on('change', "#fileuploadInput", function () {
    $("#parse").click();
});

$("#parse").click(function () {
    stepped = 0;
    chunks = 0;
    rows = 0;
    var txt = "";
    var files = $('#fileuploadInput')[0].files;
    var config = buildConfig();
    var filename = $("#fileuploadInput").val();
    var extension = filename.replace(/^.*\./, '').toLowerCase();
        if (extension !== "csv" && extension !== "tsv" && extension !== "txt")
            return;
    if (files.length > 0) {
        fileNameGlobal = filename;
        start = performance.now();
        header = [];
        $('#fileuploadInput').parse({
            config: config,
            before: function (file, inputElem) {
                //console.log("Parsing file:", file);
            },
            complete: function () {
                //console.log("Done with all files.");
                //$("#OtherDelimeter").val(arguments[0].meta.delimiter);
                    $.ajax({
                        url: urlValidateImportFile,
                        type: 'POST',
                        data: {
                            headers: header,
                            delimiter: delimiterData,
                            relationshipId: relationshipId,
                            sourceTypeId: sourceTypeId,
                            fileData: fileData
                        },

                        beforeSend: function() {
                            SGloading('Loading');
                        },
                        success: function(response) {
                            var headerFinal = [];
                            $.each(response.Data, function(key, value) {
                                headerFinal.push(value);
                           });

                            if (!response.Status) {
                                $('#filePreview').text("");
                              $("#previewTable").html("");
                                $("#fileuploadInput").val("");
                                alertmsg("red", response.Message);
                            } else {
                                $('#filePreview').text("Top rows of the selected file ");
                         
                                $("#previewTable").html("");
                                alertmsg("green", response.Message);
                                if (!jQuery.isEmptyObject(fileData)) {
                                    generate_table(headerFinal, fileData);
                                }
                                if ($("#previewTable").html()==="")
                                $('#filePreview').text("No preview available");
                             
                            }


                        },
                        complete: function() {
                            SGloadingRemove();
                            if (!jQuery.isEmptyObject(fileData)) {
                                $("#previewTable").DataTable({
                                    "destroy": true,
                                    "deferRender": true,
                                    paging: false,
                                    searching: false,
                                    bInfo: false,
                                    "order": false
                                });
                            }
                        },
                        error: function(xhr, status, error) {
                            //alert(status + ' ' + error);
                        }
                    }); // ajax close
            }
        });
    }
    else {

    }
});




$("#butnCancelImport").click(function () {
    window.location = urlVendorIndex;

   
});
$(document).ready(function() {

   
$.ajax({
    url: urlGetImportSource,
    data: {
        relationshipId : relationshipId,
        sourceTypeId : sourceTypeId
    },
    type: 'POST',
    success: function (data) {
        
        if (data.FileNameFormat == "XML") {
           
            
        } else {
           
        $(':radio').removeAttr('checked');
        //console.log(data.FieldTerminator);
        $('input[value="' + data.FieldTerminator + '"]').prop("checked", true);
        if (data.StringDelimiter !="")
            $("#delimiterValue").val(data.StringDelimiter);
        }
    }
});

});

$('#butnImportSourcefileYes').click(function (e) {

    var upfilename = $("#fileuploadInput").val();

    //console.log(upfilename);
    if (upfilename.trim() === "") {

        e.preventDefault();
        alertmsg('red', 'Please select file to upload.');
        e.preventDefault();
    }
    //var strs = upfilename.split(".");
    //if (strs[1] != "csv" && strs[1] != "xls" && strs[1] != "txt") {
    //    alertmsg('red', 'Please select valid file to upload.');
    //    e.preventDefault();
    //}
    else {
        SGloading('Uploading');   // display loading
    }
});

var dateValue = new Date();
var dateFormat = "MM/dd/yyyy".toUpperCase();
dateValue = moment(dateValue).format(dateFormat);
$('#EffectivePriceDate').val(dateValue);

jQuery('#EffectivePriceDate').datepicker({
    dateFormat: dateFormat.toLowerCase().replace("yyyy","yy")
});