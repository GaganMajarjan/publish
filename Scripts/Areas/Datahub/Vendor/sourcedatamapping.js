﻿var staging = fileId;
//$(".sourceDrop").change(function () {
//    var drpdown = this.id;
//    //   //console.log("dropdown Id:"+drpdown);
//    $.ajax({
//        url: urlDataMappingCheck,
//        data: { DestId: this.id, SourceId: this.value + "_dd", FileId: staging },
//        type: 'POST',
//        cache: false,
//        success: function (data) {
//            if (data != "ok") {
//                $('h5.Message').replaceWith("<h5 class='Message'>Message: " + data + "</h5>");
//                $('#' + drpdown).find('option:first').attr('selected', 'selected');
//                $('#' + drpdown).val($('#' + drpdown).find('option:first').val());
//                $("#DataMappingErrorBtn").click();
//            }
//        }
//    });
//});

$('#saveSourceDestinationMappingbtn').click(function () {
    var iMaped = "";
    $('.sourceLabel').each(function () {
        var ddId = '#' + $(this).val();
        var dropVal = $(ddId).select2("val");

        if (dropVal != "" && $(this).val() != "")
            iMaped += dropVal + "_" + $(this).val() + ",";
    });
    SGloading('Mapping..');
    $.ajax({
        url: urlDataMappingRelations,
        data: { FileId: staging, dataMapped: iMaped },
        type: 'POST',
        cache: false,
        success: function (data) {
            //var hostname = window.location.origin
            /// window.location.href = data;
            $("label.messagebatch").replaceWith("<label class='messagebatch'>" + data + "</label>");

            SGloadingRemove();
            $("#btnMappingCompleteModelOpen").click();

        }
    });

});


$('#togglesourceDestination').click(function () {
    window.location = urlDataMapping + "?FileId=" + staging;
});



$(document).ajaxComplete(function () {
    SGmappeddata();
});

$('document').ready(function () {

    $('.sg-staging-grid > div').height($(window).height() - 160);
    SGmappeddata();
    SGAfterdata();
    SGloading('Loading..');

    $("select").select2();


    $.ajax({
        url: urlUserAutoMappping,
        data: {
            LogUserUploadId: staging
        },
        type: 'GET',
        cache: false,
        success: function (data) {

            
            SGloadingRemove();
            //console.log(data);
            if (data !== "NoData") {
            $.each(data, function (index, value) {
                //console.log(value.Destination);
                //console.log($("#" + value.Destination).length);
                //document.getElementById(value.Destination).value = value.Source;
                $("#" + value.Destination).select2("val", value.Source);
                //$('#' + value.Destination).val(value.Source);

            });

            }

            //if (backgroundScheduled === "True") {
            //    alertmsg("yellow", "Mapping disabled for background import");

            //}
        }
    });
    $('#map').height($(window).height() - 125);


    $('#NexttoDataSync').click(function () {
        var iMaped = "";
        var dropdownIds = [];
        $('.sourceLabel').each(function () {
            var ddId = '#' + $(this).val();

            var dropVal = $(ddId).select2("val");

            if (dropVal != "" && $(this).val() != "")
                iMaped += dropVal + "_" + $(this).val() + ",";
            dropdownIds.push(dropVal);
        });
        var i = 0, j = 0, flag = 0;
        var data = "";
        for (i = 0; i < dropdownIds.length; ++i) {
            var test = dropdownIds[i];
            for (j = i + 1; j < dropdownIds.length; j++) {
                if (test == dropdownIds[j] && test != "") {
                    flag = 1;
                    data = test;
                    break;
                }
            }
        }
        if (false) {
            $.ajax({
                url: urlDataMappingGetRepeatedSource,
                data: { srcColId: data },
                type: 'POST',
                cache: false,
                success: function (data) {
                    //var hostname = window.location.origin
                    alertmsg("red", "Multiple mapping of source column: " + data);
                }
            });
        }
        else {
            var flagRequired = 1;
            var mappedIDs = iMaped.split(",");
            $.ajax({
                url: urlGetRequiredDestinationCols,
                data: {
                    LogUserUploadId: staging,
                },
                type: 'GET',
                cache: false,
                success: function (data) {
                    $.each(data, function (index1, value1) {
                        var requiredFieldMapped = 0;
                        $.each(mappedIDs, function (index2, value2) {
                            var destid = value2.split("_");
                            if (destid[0] != "") {

                                if (value1.Id == destid[1]) { requiredFieldMapped = 1; }
                            }
                        });
                        if (requiredFieldMapped == 0) {
                            flagRequired = 0;
                            alertmsg("red", "Destination field required: " + value1.Name);
                        }

                    });

                    if (flagRequired == 1) {
                        $.ajax({
                            url: urlCheckMappingChange,
                            data: { fileId: staging, dataMapped: iMaped },
                            type: 'GET',
                            cache: false,
                            success: function (data123) {
                                if (data123 == "change") {
                                    $("#saveMappingModelbtn").click();


                                }
                                else if (data123 != "change") {
                                    SGloading('Mapping..');
                                    $.ajax({
                                        url: urlDataMappingRelations,
                                        data: { FileId: staging, dataMapped: iMaped },
                                        type: 'POST',
                                        cache: false,
                                        success: function (data) {
                                            $("label.messagebatch").replaceWith("<label class='messagebatch'>" + data + "</label>");
                                            SGloadingRemove();
                                            $("#btnMappingCompleteModelOpen").click();

                                        }
                                    });
                                }
                            }
                        });
                    }

                }
            });

        }
    });


    $(".layout .message").fadeIn().delay(2000).fadeOut();
});

$('#viewDataSourcebtn').click(function() {

    window.open(urlViewDataSourceFile + "?FileId=" + fileId);
});