﻿$(document).ajaxComplete(function () {
    SGmappeddata();
});

var importAgainUrl = "";

var staging = fileId;


$('#togglesourceDestination').click(function () {
    window.location = urlSourceDataMapping +"?FileId=" + staging;
});
//$(".sourceDrop").change(function () {
//    var drpdown = this.id;
//    //   //console.log("dropdown Id:"+drpdown);
//    $.ajax({
//        url: urlDataMappingCheck,
//        data: { DestId: this.value, SourceId: this.id, FileId: staging },
//        type: 'POST',
//        cache: false,
//        success: function (data) {
//            if (data != "ok") {
//                $('h5.Message').replaceWith("<h5 class='Message'>Message: " + data + "</h5>");
//                $('#' + drpdown).find('option:first').attr('selected', 'selected');
//                $('#' + drpdown).val($('#' + drpdown).find('option:first').val());
//                $("#DataMappingErrorBtn").click();
//            }
//        }
//    });
//});


$('document').ready(function () {
    $('.sg-staging-grid > div').height($(window).height() - 160);
    SGmappeddata();
    SGAfterdata();
    SGloading('Loading..');

    $("select").select2();

    $.ajax({
        url: urlUserAutoMapping,
        data: {
            LogUserUploadId: staging
        },
        type: 'GET',
        cache: false,
        success: function (data) {
            $.each(data, function (index, value) {
                $('#' + value.Source + "_dd").select2("val", value.Destination);
            });

            
            SGloadingRemove();

        }
    });

    $('#saveMappingbtn123').click(function () {
        SGloading('Mapping..');
        var iMaped = "";
        $('.sourceLabel').each(function () {
            var ddId = '#' + $(this).val() + '_dd';

            var dropVal = $(ddId).select2("val");

            if (dropVal != "")
                iMaped += $(this).val() + "_" + dropVal + ",";
        });
        $.ajax({
            url: urlDataMappingRelations,
            data: { FileId: staging, dataMapped: iMaped },
            type: 'POST',
            cache: false,
            success: function (data) {
                //var hostname = window.location.origin
                $("label.messagebatch").replaceWith("<label class='messagebatch'>" + data + "</label>");
                SGloadingRemove();
                $("#btnMappingCompleteModelOpen").click();
                // window.location.href = data;
            }
        });

    });

    $('#map').height($(window).height()-125);
      

     
    $(".layout .message").fadeIn().delay(2000).fadeOut();


});


$('#NexttoDataSync').click(function () {
    var iMaped = "";
    var dropdownIds = [];
    $('.sourceLabel').each(function () {
        var ddId = '#' + $(this).val() + '_dd';

        var dropVal = $(ddId).select2("val");
        if (dropVal != "")
            iMaped += $(this).val() + "_" + dropVal + ",";
        dropdownIds.push(dropVal);
    });
    var i, j, flag = 0, data;
    for (i = 0; i < dropdownIds.length; ++i) {
        var test = dropdownIds[i];
        for (j = i + 1; j < dropdownIds.length; j++) {
            if (test == dropdownIds[j] && test != "") {
                flag = 1;
                data = test;
                break;
            }
        }
    }
    if (false) {
        $.ajax({
            url: urlDataMappingGetRepeatedDestination,
            data: { destColId: data },
            type: 'POST',
            cache: false,
            success: function (data) {
                alertmsg("red", "Multiple mapping of destination column: " + data);
            }
        });
    }
    else {
        var flagRequired = 0;
        var mappedIDs = iMaped.split(",");
        $.ajax({
            url: urlgetRequiredDestinationCols,
            data: {
                LogUserUploadId: staging,
            },
            type: 'GET',
            cache: false,
            success: function (data) {
                //console.log(data);
                $.each(data, function (index1, value1) {
                    var requiredFieldMapped = 0;
                    $.each(mappedIDs, function (index2, value2) {
                        var destid = value2.split("_");
                        if (value1.Id == destid[1]) { requiredFieldMapped = 1; }
                    });
                    if (requiredFieldMapped == 0) {
                        flagRequired = 1;
                        alertmsg("red", "Destination field required: " + value1.Name);
                    }
                });

                if (flagRequired == 0) {

                    $.ajax({
                        url: urlCheckMappingChange,
                        data: { fileId: staging, dataMapped: iMaped },
                        type: 'GET',
                        cache: false,
                        success: function (data123) {
                            if (data123 == "change") {
                                $("#saveMappingModelbtn").click();


                            }
                            else if (data123 != "change") {
                                $.ajax({
                                    url: urlDataMappingRelations,
                                    data: { FileId: staging, dataMapped: iMaped },
                                    type: 'POST',
                                    cache: false,
                                    success: function (data) {
                                        $("label.messagebatch").replaceWith("<label class='messagebatch'>" + data + "</label>");
                                        SGloadingRemove();
                                        $("#btnMappingCompleteModelOpen").click();
                                    }
                                });
                            }
                        }
                    });


                }
                 
            }

        });
         

    }
});

$('#viewDataSourcebtn').click(function () {

    window.open(urlViewDataSourceFile + "?FileId=" + fileId);
});