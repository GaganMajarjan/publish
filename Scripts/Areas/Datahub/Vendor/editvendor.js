﻿var bypassMinOrderCountCheck = false;
var oldMinOrderAmount = parseFloat($('#MinLineOrderAmount').val());


$(document)
    .ready(function() {
       
        //Set states if country is United States
        $('#Country')
            .change(function(value) {
                var selectedCountry = $(this).val();
                if (selectedCountry === 'United States') {
                    $("#State")
                        .autocomplete({
                            source: urlStateList
                        });
                } else {
                    //$('#State').val('');
                    $("#State")
                        .autocomplete({
                            source: null
                        });
                }
            });

        $("#Url")
            .change(function() {
                var url = $(this).val();
                if (url && url.toLowerCase().indexOf("www.") == 0) {
                    url = "http://" + url;
                    if (url.toLowerCase().lastIndexOf(".com") == -1)
                        url += ".com";
                    $(this).val(url);
                } else if (url && url.toLowerCase().indexOf("http://") == -1) {
                    url = "http://www." + url;
                    if (url.toLowerCase().lastIndexOf(".com") == -1)
                        url += ".com";
                    $(this).val(url);
                }
            });

        $.ajax({
            url: urlGetVendorCustomFieldsForUpdate + "?vendorId=" + vendorId,

            beforeSend: function() {
                //SGloading('Loading');
            },
            success: function(response) {
                //$("#ImportSourceColumnPlaceHolder").empty();
                $('#partialPlaceHolder').html(response);
                $(".datetimeDataType").datepicker({ showButtonPanel: true });
            },
            complete: function() {

                //  SGloadingRemove();
                rebuilttooltip();
                setTimeout(function() {
                        $('select').select2();
                        //$(".datetimeDataType").datepicker();
                        //$(".datetimeDataType").datepicker({

                        //    showButtonPanel: true
                        //});

                       
                    },
                    200);
            },
            error: function (xhr, status, error) {
                bypassMinOrderCountCheck = false;
                alertmsg("yellow", "Unable to load Custom Fields");
            }
        });

        //$('select').select2();

     

        $("#btn-update-vendor")
            .click(function() {
                //if ($('#form0').validate().errorList.length == 0 && $('#form0').valid()) {
                //    SGloading('Processing');
                //    return true;
                //}
                if ($("input[data-name='NbrDaysQOHConsideredStale']").val().indexOf('.') !== -1) {
                    $("input[data-name='NbrDaysQOHConsideredStale']").parents('.input-group').addClass('has-error');
                    alertmsg('red', 'NbrDaysQOHConsideredStale must contain Valid Natural Numbers Only');
                    return false;
                }

                if ($('#form0').validate().errorList.length > 0 || !$('#form0').valid()) {
                    bypassMinOrderCountCheck = false;
                    return false;
                }
                if ($("input[data-name='ResetQOHForStaleDate']").val() === "true") {
                    if ($("input[data-name='NbrDaysQOHConsideredStale']").val() > 0) {
                        $("input[data-name='NbrDaysQOHConsideredStale']").parents('.input-group').removeClass('has-error');
                    }
                    
                    else {
                        $("input[data-name='NbrDaysQOHConsideredStale']").parents('.input-group').addClass('has-error');
                        return false;
                       
                    }
                }
                
                var options = $("#hiddenNameChangeValidationOptions").val() || "{}";
                options = JSON.parse(options);
                
                if (!options.bypassVendorNameChangeCheck) {
                    //check if name has changed
                    if ($("#default-vendor-name").val().trim() != $("#vendorName").val().trim()) {
                        $("#VendorNameChangeConfirmModal").modal("show");
                        return false;
                    }
                }
                var minOrderLineAmount = parseFloat($('#MinLineOrderAmount').val());
                
                if (!bypassMinOrderCountCheck && oldMinOrderAmount !== minOrderLineAmount) {
                  
                        $.ajax({
                            type: 'GET',
                            url: urlCheckMinOrderLine,
                            async: false,
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            data: { "vendorId": vendorId, "minOrderLine": minOrderLineAmount },
                            beforeSend: function () {
                                //$("#btnsave").attr('disabled', true);
                                SGloading('Loading');
                            },
                            success: function (response) {

                                if (response > 0) {
                                    $('#VendorProductNotMatchedLineOrderAmountModal').modal('show');

                                    document.getElementById('VendorProductNotMatchedLineOrderAmountModal').childNodes[1].childNodes[1].childNodes[3].innerHTML = '';
                                    var h = document.createElement("H5");
                                    var t = document.createTextNode('Minimum Order Line has changed. All Vendor Product Minimum Order Quantity will now change. Export will occur for ' + [response] + ' Vendor Products to SEOM.')
                                    h.appendChild(t);
                                    var br = document.createElement("br")
                                    h.appendChild(br);
                                    //var br1 = document.createElement("br")
                                    //h.appendChild(br1);
                                    var t = document.createTextNode('Would you like to proceed?');
                                    h.appendChild(t);
                                    document.getElementById('VendorProductNotMatchedLineOrderAmountModal').childNodes[1].childNodes[1].childNodes[3].append(h);
                                    SGloadingRemove();
                                    return false;
                                }
                                else {
                                    SGloadingRemove();
                                    bypassMinOrderCountCheck = true;
                                    $("#btn-update-vendor").click();
                                    return false;
                                }

                            },
                            error: function (response) {
                                alert('error');
                                SGloadingRemove();
                                bypassMinOrderCountCheck = false;
                            }


                        });
                 
                    return false;

                    //SGloading('Processing');
                }
            });

        $("#btnYesVendorNameChangeConfirmModal")
            .click(function() {
                var options = {
                    bypassVendorNameChangeCheck: true
                };
                $("#VendorNameChangeConfirmModal").modal("hide");
                $("#hiddenNameChangeValidationOptions").val(JSON.stringify(options));
                $("#btn-update-vendor").click();
                //$('#form0').submit();

            });

        $("#btnNoVendorNameChangeConfirmModal")
            .click(function() {
                $("#hiddenNameChangeValidationOptions").val("{}");
                $("#VendorNameChangeConfirmModal").modal("hide");
            });

        $('#btnYesVendorProductNotMatchedLineOrderAmountModal').click(function () {

            bypassMinOrderCountCheck = true;
            $('#VendorProductNotMatchedLineOrderAmountModal').modal('hide');
            $("#btn-update-vendor").click();
        });

        $('#btnNoVendorProductNotMatchedLineOrderAmountModal').click(function () {
            bypassMinOrderCountCheck = false;
            $('#VendorProductNotMatchedLineOrderAmountModal').modal('hide');
            return false;
        });
       
     
    });





