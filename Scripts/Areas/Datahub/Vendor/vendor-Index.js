﻿$(document).ready(function () {

    $('#btnDeleteVendor').hide();
    $('#btnEnableDisableVendor').hide();

    if (GBDAdmin === "True" || Admin === "True") {
        $('#VendorDataTable').dataTable({
            "deferRender": true,
            "bServerSide": true,
            "stateSave" : true,
            "bSort": false,
            "aLengthMenu": dataTableMenuLength,
            "iDisplayLength": dataTableDisplayLength,
            "sAjaxSource": urlVendor,
            //"bProcessing": true,
            "scrollX": true
            ,
            "fnPreDrawCallback": function() {
                // gather info to compose a message
                var ob = this;
                SGtableloading({class:'fa-plane', message:'Processing...', objct: ob});
                //SGtableloading('fa-plane', '', this);
               
            },
            searchDelay: 500,
            "fnDrawCallback": function () {
                // in case your overlay needs to be put away automatically you can put it here
                var divid = this.closest('.dataTables_wrapper').attr('id');
                SGtableloadingRemove(divid);
                rebuilttooltip();
            }

        });
    } else {
        $('#VendorDataTable').dataTable({
            "deferRender": true,
            "bServerSide": true,
            "bSort": false,
            "aLengthMenu": dataTableMenuLength,
            "iDisplayLength": dataTableDisplayLength,
            "sAjaxSource": urlVendorUser,
            //"bProcessing": true,
            "fnPreDrawCallback": function() {
                // gather info to compose a message
                var divid = this.closest('.dataTables_wrapper').attr('id');
                SGtableloading('<i class="fa fa-spinner fa-spin"> </i> <br><center>Loading</center>', divid);
                
            },
            "scrollX": true,
            searchDelay: 500, //for 500ms search delay
            "drawCallback": function (settings) {
                //tooltip
                rebuilttooltip();
            },
            "fnDrawCallback": function () {
                // in case your overlay needs to be put away automatically you can put it here
                var divid = this.closest('.dataTables_wrapper').attr('id');
                SGtableloadingRemove(divid)
            }
        });
    }

    $('#btnMap').click(function (e) {
        var i = 0;
        var vendorId = 0;
        $('input[type=checkbox][class=Vendorselector]').each(function () {
            if (this.checked) {
                i++;
                vendorId = this.value;
            }
        });

        if (i === 0) { alertmsg('red', 'Please select a vendor to map Import Source'); e.preventDefault(); }
        else if (i > 1) { alertmsg('red', 'Please select a vendor to map Import Source'); e.preventDefault(); }
        else { window.location = urlMapImportSource + "?id=" + vendorId; }
    });

    if (importError != "") {
        alertmsg('red', importError);
    }

    if (VendorMessage !== "") {
        alertmsg("green", VendorMessage);
    }

    $('#selectAllCheckBoxVendor').click(function () {
        var thisCheck = $(this);
        if (thisCheck.is(':checked')) {
            $('.Vendorselector').prop("checked", true);
        } else {
            $('.Vendorselector').prop("checked", false);
        }
    });

    $('#btnAdd').click(function() {
        window.location.href = urlCreateVendor;
    });

    $('#btnEdit').click(function (e) {
        var i = 0;
        var vendorId = 0;
        $('input[type=checkbox][class=Vendorselector]').each(function () {

            if (this.checked) {
                i++;
                vendorId = this.value;
            }
        });

        if (i === 0) { alertmsg('red', 'Please select a vendor to Edit'); e.preventDefault(); }
        else if (i > 1) { alertmsg('red', 'Please select a vendor to Edit'); e.preventDefault(); }
        else { window.location = urlEditVendor + "?vendorId=" + vendorId; }
    });

    $('#btnEnableDisablefromModel').click(function (e) {
        var eachIds  = [];
        $('input[type=checkbox][class=Vendorselector]').each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });
        jQuery.ajaxSettings.traditional = true;
        $.ajax({
            url: urlEnableDisableVendors,
            data: {
                vendorIds: eachIds
            },
            type: 'POST',
            success: function (data) {
                alertmsg("green", data);
                $("#enabledisablevendorNobtn").click();
                $("#VendorDataTable").dataTable().fnReloadAjax();
            }
        });
    });
  
    $('#btnEnableDisable').click(function (e) {
        var i = 0;
        $('input[type=checkbox][class=Vendorselector]').each(function () {

            if (this.checked) {
                i++;
            }
        });

        if (i === 0) { alertmsg('red', 'Please select vendor(s) to Enable/Disable'); e.preventDefault(); }
        else {
            $('#btnEnableDisableVendor').click();
        }

    });

    $('#btnDelete').click(function(e) {
        var i = 0;
      $('input[type=checkbox][class=Vendorselector]').each(function () {
          if (this.checked) {
                i++;
            }
        });
        if (i === 0) { alertmsg('red', 'Please select vendor(s) to Delete'); e.preventDefault(); }
      else {
            $('#btnDeleteVendor').click();
        }
    });

    $('#btnDeletefromModel').click(function (e) {
   
        var eachIds  = [];
        $('input[type=checkbox][class=Vendorselector]').each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });

        jQuery.ajaxSettings.traditional = true;
        $.ajax({
            url: urlDeleteVendors,
            data: {
                vendorIds: eachIds
            },
            type: 'POST',
            beforeSend: function() {
                SGloading('Processing');
            },
            success: function (response) {
                if (response) {
                    console.log(response);
                    console.log(response.Status);
                    if (response.Status === true) {
                        alertmsg('green', response.Message);
                        //$("#deletevendorNobtn").click();
                        $("#VendorDataTable").dataTable().fnReloadAjax();
                    } else {
                        alertmsg('red', response.Message);
                    }
                } else {
                    alertmsg('red', msg_errorOccured);
                }
                //alertmsg("green", data);
            },
            complete: function () {
                $('#DeleteVendorModal').modal('hide');
                SGloadingRemove();
                rebuilttooltip();

            },
            error: function(xhr, status, error) {
                alertmsg("red", error);
            }
        });
    });

    $('#btnImport').click(function (e) {
        var sThisVal = 0, i = 0;
        var impsrc = 0;
        $('input[type=checkbox][class=Vendorselector]').each(function () {
            sThisVal = (this.checked ? i++ : 0);
            if (this.checked) {
                impsrc = this.value;
                document.getElementById('ImportSourceuploadId').value = impsrc;
            }
        });
        if (i == 0) { alertmsg('red', 'Please select a vendor to Import Data.'); e.preventDefault(); }
        else if (i > 1) { alertmsg('red', 'Please select a vendor to Import Data.'); e.preventDefault(); }
        else {
            impsrc = $("#ImportSourceuploadId").val();
            var vendorName = "";
            $.ajax({
                url: urlCheckDestinationTable,
                data: {
                    vendorId: impsrc
                },
                type: 'GET',

                success: function (data) {
                    console.log(data);
                 
                    if (!data.Success) {
                        alertmsg("red", data.Message);
                        console.log(data.Message);
                        e.preventDefault();
                    }
                    else {
                        window.location = urlImportVendor + "?relationshipId=" + data.Data.RelationshipId +"&sourceTypeId="+data.Data.SourceTypeId;
                        e.preventDefault();
                    }
                }
            });
            e.preventDefault();
        }
    });

    var initialButton = '<i class="fa  fa-power-off"></i> ' + msg_enable + '/' + msg_disable;
    var initialTitle = '<i class="fa  fa-power-off"></i> ' + msg_title.format(msg_enable + '/' + msg_disable, msg_vendor);
    var initialMessage = msg_confirm.format(msg_enable + '/' + msg_disable, msg_vendor);

    var disableButton = '<i class="fa  fa-toggle-off"></i> ' + msg_disable;
    var disableTitle = '<i class="fa  fa-toggle-off"></i> ' + msg_title.format(msg_disable, msg_vendor);
    var disableMessage = msg_confirm.format(msg_disable, msg_vendor);

    var enableButton = '<i class="fa  fa-toggle-on"></i> ' + msg_enable;
    var enableTitle = '<i class="fa  fa-toggle-on"></i> ' + msg_title.format(msg_enable, msg_vendor);
    var enableMessage = msg_confirm.format(msg_enable, msg_vendor);

    var toggleButtons = function (enable) {
        if (enable === true) {
            $('#btnEnableDisable').html(enableButton);
            $('#modal-title-enable-disable').html(enableTitle);
            $('#modal-body-enable-disable').html(enableMessage);
        } else if (enable === false) {
            $('#btnEnableDisable').html(disableButton);
            $('#modal-title-enable-disable').html(disableTitle);
            $('#modal-body-enable-disable').html(disableMessage);
        } else {
            $('#btnEnableDisable').html(initialButton);
            $('#modal-title-enable-disable').html(initialTitle);
            $('#modal-body-enable-disable').html(initialMessage);
        }
    }

    $('body').on('click', '#VendorDataTable tr', function (e) {
        var parentRow = this;
    
        $('input[type=checkbox][class=Vendorselector]').each(function () {
            var child = this;

            if (child.checked) {
                var abc = $(this).closest('td').next().find("i");

                if (abc.prop('class') === "fa fa-ban") {
              
                    toggleButtons(true);
                } else {
                
                    toggleButtons(false);
                } 
                //var value = $(parentRow).find(':last-child').text();
                //console.log(this);
                //var value = $(parentRow).children().last().text();
                //    if (value != null) {
                //    if (value === msg_no) {
                //        toggleButtons(false);
                //    } else if (value === msg_yes) {
                //        toggleButtons(true);
                //        return;
                //    } else {
                //        toggleButtons(null);
                //    }
                //} else {
                //    toggleButtons(null);
                //}
            }
        });       
    });
});
