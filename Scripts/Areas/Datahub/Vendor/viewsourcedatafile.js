﻿$(document).ready(function() {
  //  console.log('test1');
    initializeTableHeaders();
   // console.log('test-last');
});


function initializeTableHeaders() {

    $.ajax({
        "dataType": 'json',
        "type": "GET",
        "url": urlGetStagingColumns,
        "data": {fileId : fileId},
        "success": function (data) {

            data = "[" + data + "]";
            data = JSON.parse(data);
         ///   console.log("json: ");
         //   console.log(data);
         //   console.log("json end");
         //   console.log(data[0].COLUMN_NAME);
            
            var jsonResultData = ConvertJsonToTable(data, "stagingDataTable", "display", null);
            $('#stagingDataDiv').html(jsonResultData);
            $('#stagingDataDiv tbody tr').remove();


         //   console.log("jsonResultData: " + jsonResultData);
            renderDataTable();
            $.fn.dataTable.ext.errMode = 'throw';
          //  console.log('test2');
        },

        "error":function(err) {
            alertmsg("red", "Staging Table Columns not found.");
        }
    });

   
}

function initializeDataTableColumns() {

    $.ajax({
        "url": urlGetStagingDataTableColumnDefs,
        "type": "GET",
        "data":{fileId:fileId},
        "dataType": 'json',
        "success": function(data) {
            //once we have column definitions, use them to initialize  table 
          //  console.log("initializeDataTableColumns: " + data);
            renderDataTable(data);
         //   console.log('test3');
        },

        "error": function (err) {
            alertmsg("red", "Data Table Columns not found.");
        }
    });
}


function renderDataTable() {

    $('#stagingDataTable').dataTable({
        "deferRender": true,
        "bServerSide": true,
        "iDisplayLength": 10,
        "destroy": true,
        "sAjaxSource": urlGetStagingTable +"?fileId="+fileId,
        "bProcessing": true,
        "scrollX": true
        
    });

    setTimeout(TblpgntBtm, 50);
   
}