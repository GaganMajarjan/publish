﻿
$(document).ready(function () {
   
    if (vendorCustomFieldCheck) {
   
        $.ajax({
            url: urlGetVendorCustomFields,

            beforeSend: function() {
                //SGloading('Loading');
            },
            success: function(response) {
                //$("#ImportSourceColumnPlaceHolder").empty();
                $('#partialPlaceHolder').html(response);
                $(".datetimeDataType").datepicker({ showButtonPanel: true });
            },
            complete: function() {

                //  SGloadingRemove();
                rebuilttooltip();

                setTimeout(function() {
                    $('select').select2();
                    $(".datetimeDataType").datepicker();
                }, 200);
            },
            error: function(xhr, status, error) {
                alertmsg("yellow", "Unable to load Custom Fields");
            }
        });
    }

    $('#vendorName').on('input', function (e) {
        $('#importSourceConfigure').hide();
    });

    if (VendorMessage !== "") {
        alertmsg("green", VendorMessage);
    }

    //Set states if country is United States
    $('#Country').change(function(value) {
        var selectedCountry = $(this).val();
        if (selectedCountry === 'United States') {
            $("#State").autocomplete({
                source: urlStateList
            });
        } else {
            //$('#State').val('');
            $("#State").autocomplete({
                source: null
            });
        }
    });

    $("#Url")
       .change(function () {
           var url = $(this).val();
           if (url && url.toLowerCase().indexOf("www.") == 0) {
               url = "http://" + url;
               if (url.toLowerCase().lastIndexOf(".com") == -1)
                   url += ".com";
               $(this).val(url);
           }
           else if (url && url.toLowerCase().indexOf("http://") == -1) {
               url = "http://www." + url;
               if (url.toLowerCase().lastIndexOf(".com") == -1)
                   url += ".com";
               $(this).val(url);
           }
       });

    $('#Name').focus();

    //$('select').select2();

   

});

$("#btnaddvendor").click(function () {
      if ($("input[data-name='NbrDaysQOHConsideredStale']").val().indexOf('.') !== -1) {
        $("input[data-name='NbrDaysQOHConsideredStale']").parents('.input-group').addClass('has-error');
        alertmsg('red', 'NbrDaysQOHConsideredStale must contain Valid Natural Numbers Only');
        return false;
    }
    if ($("input[data-name='ResetQOHForStaleDate']").val() === "true") {
        if ($("input[data-name='NbrDaysQOHConsideredStale']").val() > 0) {
            $("input[data-name='NbrDaysQOHConsideredStale']").parents('.input-group').removeClass('has-error');
        }
       
        else {
            $("input[data-name='NbrDaysQOHConsideredStale']").parents('.input-group').addClass('has-error');
            return false;       
        }
    }
});
