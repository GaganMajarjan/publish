﻿
$("#btnMapImportSource").click(function () {
    $.ajax({
        url: urlMapVendorImportSource,
        data: {
            vendorId: vendorId,
            importSourceId: $("#importSource").val()
        },
        type: 'POST',
        cache: false,
        success: function (data) {
            if (data.Success) {
                alertmsg('green', data.Message);
                window.location = urlVendor;
            } else {
                alertmsg('red', data.Message);
            }
        }
    });
});