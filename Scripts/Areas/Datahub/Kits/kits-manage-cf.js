﻿/***
changeLevels List:
 COST = cost
 MAP= Margin Percent
 MAA = Margin Amount
 SAP = Sale Price
 GMP = Gross Margin Percent
 GMA = Gross Margin Amount
 SEP = Selling Price
***/
$(document)
    .ready(function () {
        $("#ProductCategoryId")
            .select2({
                tags: true,
                placeholder: '--Please select a category'
            });
    });

 var changeLevel = "";
 var changeLevel2 = "";
var changeLevel3 = "";
var isCreate = false;
var isNoValidate = false;
var isYesNegativeePricingDialog = false;
var stagingIdsLstToDelete = [];
var dataToDelete = [];
//Json parse viewbag elements
//decode viewModel
kitsVm = kitsVm.replace(/\\"/g, '"').replace(/"/g, '\\"');

kitsVm = decodeHtml(kitsVm);
kitsVm = decodeHtml(kitsVm);
kitsVm = fixNewLineOnly(kitsVm);

////Parse viewModel
//if (kitsVm != null && kitsVm != "") {
//    //console.log(kitsVm);
//    kitsVm = fixJsonSingleQuoteWithNewline(kitsVm);
//    //console.log(kitsVm);
//    ReplaceSingle2Quote(kitsVm);
//} else
//    kitsVm = null;

//parse KitItem data
kitPricingData = kitPricingData.replace(/\\"/g, '"').replace(/"/g, '\\"');
    kitPricingData = decodeHtml(kitPricingData);
    kitPricingData = decodeHtml(kitPricingData);
    kitPricingData = fixNewLineOnly(kitPricingData);
//kitPricingData = decodeHtml(kitPricingData);

//data for handsontable
var data = kitPricingData;

//Parse kit pricing viewModel
//if (data && data !== "[]") {
//    data = fixJsonQuote(data);
//    ReplaceSingle2Quote(data);
//} else
//    data = null;

var cost = 0;
var price = 0;
var marginPercent = 0;
var marginAmount = 0;
var vendorProductCost = 0;
var pIdList = [];
    unitOfMeasureListViewBag = fixJsonQuote(unitOfMeasureListViewBag);

    var unitOfMeasureList = [];
    $.each(unitOfMeasureListViewBag, function(index, val) {
        unitOfMeasureList.push(val.TargetUOM);
    });

//Calculate Cost and Price if is in Create Mode
    if (kitsVm == null || kitsVm.Cost == null) {
        if (data) {
            $.each(data, function(index, val) {
                cost = cost + parseFloat(val.ProductCost) * parseFloat(val.Quantity);
                price = price + parseFloat(val.ProductPrice) * parseFloat(val.Quantity);
                vendorProductCost = vendorProductCost + parseFloat(val.VendorProductCost) * parseFloat(val.Quantity);
                pIdList.push(val.Id);
            });

            //Price equal to cost for margin 0
            price = cost;
            //marginPercent = calculateGrossMarginPercent(cost, price);
            //marginAmount = convertToGrossMarginAmount(cost, marginPercent);
        }
    }
    else if (kitsVm && kitsVm.Cost) {
        cost = kitsVm.SelectedCost;
        vendorProductCost = kitsVm.Cost;
        price = kitsVm.Price;
        marginPercent = kitsVm.MarginPercent;
        marginAmount = kitsVm.MarginAmount;
    }
$('#SellingPrice').attr('readonly', true);//viewmodels
//product catalog viewmodel
    var PimKitsViewModel = function(kitsVm) {
        var self = this;
        self.StoreId = ko.observable((kitsVm != null && kitsVm.StoreId != null) ? kitsVm.StoreId : '');

        // Properties - General
        self.Id = ko.observable((kitsVm != null && kitsVm.Id != null) ? kitsVm.Id : '');
        self.Name = ko.observable((kitsVm != null && kitsVm.Name != null) ? kitsVm.Name : '');
        self.KitSKU = ko.observable((kitsVm != null && kitsVm.KitSKU != null) ? kitsVm.KitSKU.toUpperCase() : '');
        self.Location = ko.observable((kitsVm != null && kitsVm.Location != null) ? kitsVm.Location : '');
        self.ShortDescription = ko.observable((kitsVm != null && kitsVm.ShortDescription != null) ? kitsVm.ShortDescription : '');
        self.IsDisabled = ko.observable((kitsVm != null && kitsVm.IsDisabled != null) ? kitsVm.IsDisabled : false);

        // Properties - Pricing

        self.IsCostLocked = ko.observable((kitsVm != null && kitsVm.IsCostLocked != null) ? kitsVm.IsCostLocked : true);
        self.IsMarginLocked = ko.observable((kitsVm != null && kitsVm.IsMarginLocked != null) ? kitsVm.IsMarginLocked : false);
        self.Cost = ko.observable(vendorProductCost).extend({ numeric: decimal_places });
        self.SelectedCost = ko.observable('').extend({ notify: 'always' });
        self.SelectedCost(cost.toFixed(2));
        //SG-322 fix: SP get calculated on the basis of margin=0 (if it's create, don't auto-calculate SP. Let user enter a SP or margin)
        self.Price = ko.observable(kitsVm.KitSKU == null ? 0 : price).extend({ numeric: decimal_places });

        self.MarginPercent = ko.observable(marginPercent).extend({ numeric: decimal_places });

        self.MarginAmount = ko.observable(marginAmount).extend({ numeric: decimal_places });

        self.Note = ko.observable((kitsVm != null && kitsVm.Note != null) ? kitsVm.Note : '');

        self.IsSalePriceLocked = ko.observable((kitsVm != null && kitsVm.IsSalePriceLocked != null) ? kitsVm.IsSalePriceLocked : false);
        self.IsStoreEnabled = ko.observable(isStoreEnabled);
        self.PricingLevels = ko.observableArray([]);
        self.PricingLevelsArchive = ko.observableArray([]);

        //self.PricingCustomField = ko.observable(kitsVm != null ? new PricingCustomFieldVM(kitsVm.PricingCustomField) : new PricingCustomFieldVM(null));
        self.PricingCustomField = kitsVm != null && kitsVm.PricingCustomField != null ? new PricingCustomFieldVM(kitsVm.PricingCustomField) : new PricingCustomFieldVM(null);

        self.CustomFieldsRequired = ko.observableArray([]);
        self.CustomFieldsAdditional = ko.observableArray([]);
        self.CustomFieldsReadOnly = ko.observableArray([]);
        self.CustomTemplates = ko.computed(function() {
            return self.CustomFieldsRequired().concat(self.CustomFieldsAdditional());
        });
        self.CustomTemplatesAll = ko.computed(function() {
            return self.CustomFieldsRequired().concat(self.CustomFieldsReadOnly()).concat(self.CustomFieldsAdditional());
        });
        self.CountReqAndReadonly = ko.computed(function() {
            return (parseInt(self.CustomFieldsRequired().length) + parseInt(self.CustomFieldsReadOnly().length));
        });
        self.CountReq = ko.computed(function() {
            return (parseInt(self.CustomFieldsRequired().length));
        });
        self.PlatformQualifier = ko.observable((kitsVm != null && kitsVm.PlatformQualifier != null) ? kitsVm.PlatformQualifier : '');
        self.PlatformQualifiers = ko.observableArray((kitsVm != null && kitsVm.PlatformQualifiers != null) ? kitsVm.PlatformQualifiers : []);

        // Properties - Identifiers
        self.ISBN = ko.observable((kitsVm != null && kitsVm.ISBN != null) ? kitsVm.ISBN : '');
        self.NSN = ko.observable((kitsVm != null && kitsVm.NSN != null) ? kitsVm.NSN : '');
        self.UPC = ko.observable((kitsVm != null && kitsVm.UPC != null) ? kitsVm.UPC : '');
        self.EAN = ko.observable((kitsVm != null && kitsVm.EAN != null) ? kitsVm.EAN : '');
        self.MPN = ko.observable((kitsVm != null && kitsVm.MPN != null) ? kitsVm.MPN : '');
        self.ASIN = ko.observable((kitsVm != null && kitsVm.ASIN != null) ? kitsVm.ASIN : '');
        self.IdentifierExists = ko.observable((kitsVm != null && kitsVm.IdentifierExists != null) ? kitsVm.IdentifierExists : '');
        // Properties - Dimensions
        self.Weight = ko.observable((kitsVm != null && kitsVm.Weight != null) ? kitsVm.Weight : '');
        self.Height = ko.observable((kitsVm != null && kitsVm.Height != null) ? kitsVm.Height : '');
        self.Width = ko.observable((kitsVm != null && kitsVm.Width != null) ? kitsVm.Width : '');
        self.Length = ko.observable((kitsVm != null && kitsVm.Length != null) ? kitsVm.Length : '');

        self.UOM = ko.observable((kitsVm != null && kitsVm.UOM != null) ? kitsVm.UOM : '');
        self.ProductCategoryId = ko.observable((kitsVm != null && kitsVm.ProductCategoryId != null) ? kitsVm.ProductCategoryId : '');
        self.ProductCategoryKitIds = ko.observable((kitsVm != null && kitsVm.ProductCategoryKitIds != null) ? kitsVm.ProductCategoryKitIds : []);
        self.BrandId = ko.observable((kitsVm != null && kitsVm.BrandId != null) ? kitsVm.BrandId : '');
        self.QOH = ko.observable((kitsVm !== null && kitsVm.QOH !== null) ? kitsVm.QOH : '');
        self.PreviousMarginPct = ko.observable(marginPercent)
            .extend({ numeric: decimal_places });
        self.Module = ko.observable('');
        self.RecalcMarginPct = ko.observable('');
        // Properties - Clearance (sale)
        self.hasSalePriceForClearance = ko.observable((kitsVm != null && kitsVm.HasSalePriceForClearance != null) ? kitsVm.HasSalePriceForClearance : false);
        self.SalePrice = ko.observable((kitsVm != null && kitsVm.SalePrice != null) ? kitsVm.SalePrice : '')
            .extend({ numeric: decimal_places });
        self.TotalSalePrice = ko.observable((kitsVm != null && kitsVm.TotalSalePrice != null) ? kitsVm.TotalSalePrice : '')
            .extend({ numeric: decimal_places });

        self.IsMarginNegative = ko.observable(false);
        self.salePriceForClearanceMarginPercent = ko.observable((kitsVm != null && kitsVm.SalePriceMarginPercent != null) ? kitsVm.SalePriceMarginPercent : '')
            .extend({ numeric: decimal_places });
       
           //SKYG-718
        self.SellingPrice = ko.observable((kitsVm != null && kitsVm.SellingPrice != null) ? kitsVm.SellingPrice : '')
            .extend({ numeric: 2 });
       
        self.HazmatBoxFee = ko.observable((kitsVm != null && kitsVm.HazmatBoxFee != null) ? kitsVm.HazmatBoxFee : '').extend({ numeric: decimal_places });
        self.FulfillmentCenter = ko.observable((kitsVm != null && kitsVm.FulfillmentCenter != null) ? kitsVm.FulfillmentCenter : '');
        self.BatchLotTracking = ko.observable((kitsVm != null && kitsVm.BatchLotTracking != null) ? kitsVm.BatchLotTracking : '');

        self.CoreExchangeFee = ko.observable((kitsVm != null && kitsVm.CoreExchangeFee != null) ? kitsVm.CoreExchangeFee : 0)
            .extend({ numeric: 2 });

        self.CrateFee = ko.observable((kitsVm != null && kitsVm.CrateFee != null) ? kitsVm.CrateFee : 0)
            .extend({ numeric: 2 });
        SetCaseQuantityDefault();
        self.SkyGeekPN = ko.observable((kitsVm != null && kitsVm.SkyGeekPN != null) ? kitsVm.SkyGeekPN : '');
        self.GoogleItemIdOverride = ko.observable((kitsVm != null && kitsVm.GoogleItemIdOverride != null) ? kitsVm.GoogleItemIdOverride : '');

        

        function SetCaseQuantityDefault() {
            if (self.PricingCustomField.case_qty() === null || self.PricingCustomField.case_qty() === '' || self.PricingCustomField.case_qty() <= 0) {
                self.PricingCustomField.case_qty("1");
                self.CaseQuantity("1");
            }
        }
        self.hasSalePriceForClearance.subscribe(function (val) {
            if (val === false) {
                self.SalePrice('0');
                self.salePriceForClearanceMarginPercent('0');
                self.TotalSalePrice('0');
            }
            else {
                if (self.salePriceForClearanceMarginPercent() === 0 && self.SalePrice() === 0) {
                    self.SalePrice(self.SelectedCost());
                }
            }
            changeLevel2 = "";
        });
        self.IsValidate = ko.observable(false);
        self.IsPriceNegative = ko.observable(false);
        self.CaseQuantity = ko.observable((kitsVm !== null && kitsVm.PricingCustomField.case_qty !== null) ? kitsVm.PricingCustomField.case_qty : '');
        //self.SellingSku = ko.observable((kitsVm != null && kitsVm.KitSKU != null) ?
        //    (self.CaseQuantity() != '' && self.CaseQuantity() != "1" && self.CaseQuantity() != "0") ? kitsVm.KitSKU.toUpperCase() + '-X' + self.CaseQuantity() : kitsVm.KitSKU.toUpperCase() : '');
        self.SellingSku = ko.observable((kitsVm != null && kitsVm.SellingSku != null) ? kitsVm.SellingSku : '');

        self.IsSellingPrice = ko.observable(false);
        self.IsCaseQty = ko.observable(false);
        self.IsPrice = ko.observable(false);
       // if (self.SellingPrice() === null || self.SellingPrice() === '' || self.SellingPrice() === 0) {
            CalculateSellingPrice();
       // }
        self.IsClickedEnabled = ko.observable(false);
        // Functions/Events
        self.addPricingLevel = function() {

            for (var i = 0; i < self.PricingLevels().length; i++) {
                var quantityFrom = self.PricingLevels()[i].QuantityFrom();
                //var quantityTo = self.PricingLevels()[i].QuantityTo();
                if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                    alertmsg("red", 'Quantity should be greater than 1');
                    return;
                }

                var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
                var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

                if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                    alertmsg("red", 'Margin %  should be greater than 0');
                    return;
                }

                if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                    alertmsg("red", 'Margin $  should be greater than 0');
                    return;
                }


            }
            if (self.PricingLevels().length > 1) {
                var quantityFromCurrent = self.PricingLevels()[self.PricingLevels().length - 1].QuantityFrom();
                var quantityFromPrevious = self.PricingLevels()[self.PricingLevels().length - 2].QuantityFrom();


                if (quantityFromPrevious != null && quantityFromCurrent != null && parseFloat(quantityFromPrevious) >= parseFloat(quantityFromCurrent)) {
                    alertmsg("red", 'Current "Quantity" should be greater than the previous "Quantity"');
                    return;
                }
            }


            self.PricingLevels.push(new PIMPricingLevelViewModel());
            rebuilttooltip();
        };

        self.removePricingLevel = function(pricingLevel) {

            self.PricingLevels.remove(pricingLevel);
        };

        self.hasCustomFieldTemplateValue = ko.observable(true);
        self.hasCustomFieldValue = ko.observable(false);

        self.getCostThreshold = function(cost) {

            var oldVal = kitsVm.Cost;
            var newVal = cost;
            var percentChange = ((newVal - oldVal) / oldVal) * 100;
            if (oldVal === 0 || oldVal === null)
                percentChange = 0;
            return percentChange;

        }

        $('#btnYesCostThresholdModal').click(function() {
            self.save(true);
        });
        if (self.IsDisabled() === false) {
            $("#PlatformQualifier").prop("disabled", false);
        } else {
            $("#PlatformQualifier").prop("disabled", true);
        }
        self.IsDisabled.subscribe(function (val) {
            
            if (val === false) {
                self.IsClickedEnabled(true);
                $("#PlatformQualifier").prop("disabled", false);
            }
            else {
                $("#PlatformQualifier").val(null).trigger('change');
                self.IsClickedEnabled(false);
                alertmsg("red", "PlatformQualifier will also disabled upon disabling Product");
                $("#PlatformQualifier").prop("disabled", true);
                return false;
            }
           
        });

       

        self.validate = function (options) {

            // SG-771 validation for product_url and YahooId

            //var productUrl = $('[data-name="product_url"]');
            //var yahooId = $('[data-name="YahooId"]');

            //if (productUrl && yahooId) {

            //    if (yahooId.val() !== "") {
            //        var ExpectedProductValue = "http://www.skygeek.com/{0}.html".format(yahooId.val());

            //        if (productUrl.val() !== ExpectedProductValue) {
            //            alertmsg('red', ' Product-url \'' + productUrl.val() + '\' is invalid. Expected value is \'' + ExpectedProductValue + '\' (calculated from YahooId)');
            //            return;
            //        }
            //    }
            //}



            

            var bypassCostThresholdCheck = false;
            var bypassPriceRangeCheck = false;
            var bypassPriceAndCostSameCheck = false;
            var bypassQuantityDiscountRoundUpCheck = false;
            var bypassNegativeMargin = false;
            var bypassSaleMarginGreaterThanProductMargin = false;

            var negativeMargin = self.validateNegativeMargin();
            if (!isYesNegativeePricingDialog) {
                if (negativeMargin === true && bypassNegativeMargin === false) {
                    
                    $('#MarginNegativeDueToCostModal').modal('show');
                    return;
                }
                else if (self.IsPriceNegative() && negativeMargin === false) {
                    $('#MarginLeveNegativeDueToFifoCostModal').modal('show');
                    return;
                }
               
            }

            if (options) {
                if (options.bypassCostThresholdCheck)
                    bypassCostThresholdCheck = true;

                if (options.bypassPriceRangeCheck)
                    bypassPriceRangeCheck = true;

                if (options.bypassPriceAndCostSameCheck)
                    bypassPriceAndCostSameCheck = true;

                if (options.bypassQuantityDiscountRoundUpCheck)
                    bypassQuantityDiscountRoundUpCheck = true;
            }

            //validate
            var isValid = validateCustomField();
            if (!isValid)
                return false;

            // SG-771 validation for product_url and YahooId
            isValid = validateYahooIdWithProductUrl();
            if (!isValid)
            return false;

            isValid = validateYahooId();
            if (!isValid)
                return false;

            //isValid = validateItemPoints();
            //if (!isValid)
            //    return false;

            isValid = validateCommodityCode();
            if (!isValid)
                return false;

            if (bypassCostThresholdCheck !== true) {
                //calculate cost threshold change
                var percentChange = self.getCostThreshold(self.SelectedCost());
                if (percentChange >= 150 || percentChange <= -150) {
                    $('#CostThresholdModal').modal("show");
                    return false;
                }
            }

            //if (bypassPriceRangeCheck !== true) {
            //    isValid = validatePriceRange();
            //    if (!isValid)
            //        return false;
            //}
            if (bypassPriceRangeCheck !== true) {
                isValid = validateCostPriceRange();
                if (!isValid)
                    return false;
            }
            if (bypassPriceAndCostSameCheck !== true) {
                isValid = validatePriceAndCostSame();
                if (!isValid)
                    return false;
            }
            if (bypassQuantityDiscountRoundUpCheck !== true) {
                options.bypassQuantityDiscountRoundUpCheck = true;
                isValid = validateQuantityDiscountRoundUp(null);
                if (!isValid)
                    return false;
            }

            //if (bypassSaleMarginGreaterThanProductMargin != true) {

            //    isValid = validateSaleMarginGreaterThanProductMargin();
            //    if (!isValid)
            //        return false;

            //}

            //options end (validation)
            return true;
        }

        self.save = function(options) {
            //validate
            var isValid = self.validate(options);
            if (!isValid)
                return;

            var hazmatTypeId = $('[data-name = "hazmat_type"]');
            if (hazmatTypeId)
                $(hazmatTypeId).removeAttr('disabled')

            if (self.Id() !== "" && self.Id() !== "0" && self.Id() !== "-1" && self.Id() !== 0 && self.Id() !== -1) {
                self.edit(options);
            } else {
                self.create(options);
            }
        }

        self.create = function (options) {

            self.setFieldReferenceValue(); //SKYG-718
        
            //validate
            isCreate = true;
           
            var isValid = self.validate(options);
            if (!isValid)
                return;

            for (var i = 0; i < self.PricingLevels().length; i++) {

                var quantityFrom = self.PricingLevels()[i].QuantityFrom();
                //var quantityTo = self.PricingLevels()[i].QuantityTo();
                if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                    alertmsg("red", 'Please enter Quantity greater than one');
                    return;
                }

                var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
                var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

                if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                    alertmsg("red", 'Margin %  should be greater than 0');
                    return;
                }

                if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                    alertmsg("red", 'Margin $  should be greater than 0');
                    return;
                }
            }


          
            if (self.hasSalePriceForClearance()) {

                if (!isNaN(self.salePriceForClearanceMarginPercent()) && self.salePriceForClearanceMarginPercent() >= self.MarginPercent() && self.salePriceForClearanceMarginPercent() != 0) {
                    alertmsg('red', 'Sale Price Margin is greater than Price Margin !!')
                    return;

                }
                if (self.IsMarginNegative()) {
                    var salePrice1 = self.SalePrice();
                    var newSalePrice1 = calculateGrossMarginPercent(self.SelectedCost(), salePrice1);
                    changeLevel2 += "SPS,";
                    self.IsMarginNegative(false);
                    self.salePriceForClearanceMarginPercent(newSalePrice1);
                }
                if (self.IsMarginLocked()) {
                    var salePriceMarginPercent = self.salePriceForClearanceMarginPercent();
                    var newSalePrice = calculateSellingPrice(self.SelectedCost(), salePriceMarginPercent);
                    changeLevel2 += "SPS,";
                    self.SalePrice(newSalePrice);
                }

                else {

                    var salePrice = self.SalePrice();
                    var newSalePrice2 = calculateGrossMarginPercent(self.SelectedCost(), salePrice);
                    changeLevel2 += "SPS,";
                    self.salePriceForClearanceMarginPercent(newSalePrice2);
                }
            }
            var data = ko.toJSON({
                viewModel: self,
                kitJsonString: JSON.stringify(hot.getSettings().data)
            });

            //convert string to object
            data = JSON.parse(data);

            //add property to object
            data.viewModel.CustomTemplates = getCustomFieldJson(kitsVm.CustomTemplateGroup, '.cf-by-group .cf', true);
           
            var obj = null;
            if (data.viewModel.CustomTemplates.length > 0) {
                obj = data.viewModel.CustomTemplates.find(o => o.CustomField === $('[data-name="YahooId"]').attr("id"));
            }
            var pf = $("#PlatformQualifier option:selected").text();
            //if ((data.viewModel.IsDisabled) || (pf === "Yahoo" && viewModel.IsClickedEnabled() === true)) {
            if (obj === null && (data.viewModel.IsDisabled || (pf === "Yahoo" && viewModel.IsClickedEnabled() === true))) {

                var YahooId = $('[data-name="YahooId"]').val();
                var customFieldVm = new CustomFieldManagerViewModel();
                customFieldVm.CustomField = $('[data-name="YahooId"]').attr("id"); //this is the customFieldId

                customFieldVm.FieldValue = YahooId || '';
                customFieldVm.FieldValueIsDirty = true; //this has changed, so set IsDirty

                //add values to bypass modelstate validation
                var customFields = {
                    name: 'not null',
                    fieldDataTypeSdvId: '0',
                    customFieldTemplateId: '0',
                    fieldDescription: '',
                    fieldAlias: 'YahooId',
                    description: '',
                    fieldReferenceSysInformationSchemaId: "",
                    fieldReferenceSysInformationSchemaIdKit: ""
                };

                customFieldVm.CustomFields = customFields;
                data.viewModel.CustomTemplates.push(customFieldVm);

            }
            //check if yahooId validation has to be bypassed (occurs if user clicks buttons to override existing YahooId)
            if (options && options.triggerActionId) {
                data.viewModel.triggerActionId = options.triggerActionId;
            }




            //convert back to string
            data = JSON.stringify(data);

            // Ajax call
            $.ajax({
                type: 'POST',
                url: createUrl,
                cache: false,
                contentType: 'application/json; charset=utf-8',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    //$("#btnsave").attr('disabled', true);
                    SGloading('Loading');
                },
                success: function(response) {
                    if (response != null && response.Status === true) {
                        alertmsg("green", response.Message);

                        var returnUrl = response.Data;
                        if (returnUrl)
                            window.location.href = returnUrl;

                        var suggestionUrl = getQueryStringByName("returnUrl");
                        if (suggestionUrl) {
                            window.location = suggestionUrl;
                        }
                    } else if (response != null && response.Status === false) {
                        if (response.Data && response.Data === "warn") {
                            //show warning for 0.8 seconds, then show success message
                            alertmsg("yellow", response.Message);
                            setTimeout(function() {
                                alertmsg("green", "Kit created successfully");
                                },3000);
                        }
                        else if (response.Data && response.Data.ConfirmUser) {
                            //this is a label, use text() to set it's text message
                            $("#yahooIdConfirmationMessageLabel").text(response.Message);
                            //show the modal
                            $("#YahooIdConfirmationModal").modal("show");
                            SGloadingRemove();
                        } else {
                            alertmsg("red", response.Message);
                            SGloadingRemove();
                        }
                    } else {
                        alertmsg("red", errorOccurredMessage);
                        SGloadingRemove();
                    }
                },
                complete: function() {
                    //$("#btnsave").attr('disabled', false);
                    SGloadingRemove();
                },
                error: function(xhr, status, error) {
                    //reset variables here
                    alert(status + ' ' + error);
                }
            });

        };

        self.edit = function (options) {

            if (dataToDelete !== null && dataToDelete.length > 0) {
                var ids = dataToDelete.map(x => x.KitItemId);
                    $.ajax({
                    type: 'POST',
                    data: JSON.stringify({ kitIds: ids}),
                    dataType: 'json',
                    cache: false,
                    contentType: 'application/json',
                    url: deleteKitItem,
                    beforeSend: function () {
                        //SGloading('Loading');
                    },
                    success: function (response) {
                        
                       
                    }
                });
            }
            dataToDelete = null;
            self.setFieldReferenceValue(); //SKYG-718
            //validate
            var isValid = self.validate(options);
          
            if (!isValid)
                return;

            for (var i = 0; i < self.PricingLevels().length; i++) {
                var quantityFrom = self.PricingLevels()[i].QuantityFrom();
                //var quantityTo = self.PricingLevels()[i].QuantityTo();
                if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                    alertmsg("red", 'Please enter Quantity greater than one. Save Cancelled.');
                    return;
                }

                var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
                var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

                if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                    alertmsg("red", 'Margin %  should be greater than 0');
                    return;
                }

                if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                    alertmsg("red", 'Margin $  should be greater than 0');
                    return;
                }

            }
            if (self.hasSalePriceForClearance()) {
                if (!isNaN(self.salePriceForClearanceMarginPercent()) && self.salePriceForClearanceMarginPercent() >= self.MarginPercent() && self.salePriceForClearanceMarginPercent() != 0) {
                    alertmsg('red', 'Sale Price Margin is greater than Price Margin !!')
                    return;

                }

                if (self.IsMarginNegative()) {
                    var salePrice1 = self.SalePrice();
                    var newSalePrice1 = calculateGrossMarginPercent(self.SelectedCost(), salePrice1);
                    changeLevel2 += "SPS,";
                    self.IsMarginNegative(false);
                    self.salePriceForClearanceMarginPercent(newSalePrice1);
                }
                if (self.IsMarginLocked()) {
                    var salePriceMarginPercent = self.salePriceForClearanceMarginPercent();
                    var newSalePrice = calculateSellingPrice(self.SelectedCost(), salePriceMarginPercent);
                    changeLevel2 += "SPS,";
                    self.SalePrice(newSalePrice);
                }

                else {

                    var salePrice = self.SalePrice();
                    var newSalePrice2 = calculateGrossMarginPercent(self.SelectedCost(), salePrice);
                    changeLevel2 += "SPS,";
                    self.salePriceForClearanceMarginPercent(newSalePrice2);
                }
            }
            var data = ko.toJSON({
                viewModel: self,
                kitJsonString: JSON.stringify(hot.getSettings().data)
            });

            //convert string to object
            data = JSON.parse(data);

            //add property to object
            data.viewModel.CustomTemplates = getCustomFieldJson(kitsVm.CustomTemplateGroup, '.cf-by-group .cf', true);
            var obj = null;
            if (data.viewModel.CustomTemplates.length > 0) {
                obj = data.viewModel.CustomTemplates.find(o => o.CustomField === $('[data-name="YahooId"]').attr("id"));
            }
            var pf = $("#PlatformQualifier option:selected").text();
           // if ((data.viewModel.IsDisabled) || (pf === "Yahoo" && viewModel.IsClickedEnabled() === true)) {
     if (obj === null && (data.viewModel.IsDisabled || (pf === "Yahoo" && viewModel.IsClickedEnabled() === true))) {
           var YahooId = $('[data-name="YahooId"]').val();
                var customFieldVm = new CustomFieldManagerViewModel();
                customFieldVm.CustomField = $('[data-name="YahooId"]').attr("id"); //this is the customFieldId

                customFieldVm.FieldValue = YahooId || '';
                customFieldVm.FieldValueIsDirty = true; //this has changed, so set IsDirty

                //add values to bypass modelstate validation
                var customFields = {
                    name: 'not null',
                    fieldDataTypeSdvId: '0',
                    customFieldTemplateId: '0',
                    fieldDescription: '',
                    fieldAlias: 'YahooId',
                    description: '',
                    fieldReferenceSysInformationSchemaId: "",
                    fieldReferenceSysInformationSchemaIdKit: ""
                };

                customFieldVm.CustomFields = customFields;
                data.viewModel.CustomTemplates.push(customFieldVm);

            }
            //check if yahooId validation has to be bypassed (occurs if user clicks buttons to override existing YahooId)
            if (options && options.triggerActionId) {
                data.viewModel.triggerActionId = options.triggerActionId;
            }

            //convert back to string
            data = JSON.stringify(data);

            // Ajax call
            $.ajax({
                type: 'POST',
                url: urlEditKit,
                cache: false,
                contentType: 'application/json; charset=utf-8',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    //$("#btnsave").attr('disabled', true);
                    SGloading('Loading');
                },
                success: function(response) {
                    if (response != null && response.Status === true) {
                        alertmsg("green", response.Message);
                        
                        var returnUrl = response.Data;
                        if (returnUrl)
                            window.location.href = returnUrl;

                        var suggestionUrl = getQueryStringByName("returnUrl");
                        if (suggestionUrl)
                            window.location = suggestionUrl;
                        else
                            window.location.reload();
                    } else if (response != null && response.Status === false) {
                        if (response.Data && response.Data === "warn") {
                            //show warning for 0.8 seconds, then show success message
                            alertmsg("yellow", response.Message);
                            setTimeout(function() {
                                    alertmsg("green", "Kit updated successfully!");
                                    window.location.reload();
                                },
                                3000);
                        } else if (response.Data && response.Data.ConfirmUser) {
                            //this is a label, use text() to set it's text message
                            $("#yahooIdConfirmationMessageLabel").text(response.Message);
                            //show the modal
                            $("#YahooIdConfirmationModal").modal("show");
                            SGloadingRemove();
                        } else {
                            alertmsg("red", response.Message);
                            SGloadingRemove();
                        }
                    } else {
                        alertmsg("red", errorOccurredMessage);
                        SGloadingRemove();
                    }
                },
                complete: function() {
                    //$("#btnsave").attr('disabled', false);
                    SGloadingRemove();
                },
                error: function(xhr, status, error) {
                    //reset variables here
                    alert(status + ' ' + error);
                }
            });

        };

        self.validateNegativeMargin = function () {
            if (self.MarginPercent() < 10 && self.IsMarginLocked() !== true) {
                self.RecalcMarginPct = self.MarginPercent();
                self.Module = $('ul[role=tablist]>li').first().text();
                self.IsValidate = true;
                self.IsPriceNegative(false);
                return true;
            }
            if (self.PricingLevels().length > 0) {
                for (var i = 0; i < self.PricingLevels().length; i++) {
                    var pricingLevel = self.PricingLevels()[i];
                    if (pricingLevel != null) {
                        if (pricingLevel.GrossMarginPercent() < 0) {
                            self.Module = $('ul[role=tablist]>li').first().text();
                            self.RecalcMarginPct = self.MarginPercent();
                            self.IsPriceNegative(true);
                            self.IsValidate = true;
                            $("#table-pricing-level")[0].children[2].remove();
                            $('#add-more-pricing-level').prop('disabled', true);
                            viewModel.PricingLevels([]);
                            return false;
                        }
                        else {
                            self.IsPriceNegative(false);
                        }

                    }
                }
            }

            return false;

        }
        self.setFieldReferenceValue = function () {
            viewModel.IdentifierExists = $('[data-name = "IdentifierExists"]').val() !== "1" ? false : true;
            viewModel.HazmatBoxFee = $('[data-name="HazmatBoxFee"]').val();
            viewModel.ASIN = $('[data-name="ASIN"]').val();
            viewModel.EAN = $('[data-name="EAN"]').val();
            viewModel.NSN = $('[data-name="NSN"]').val();
            viewModel.UPC = $('[data-name="UPC"]').val();
            //viewModel.UOM = $('[data-name="UOM"]').val();
            viewModel.ISBN = $('[data-name="ISBN"]').val();
            viewModel.MPN = $('[data-name="MPN"]').val();

        }

        //$("#btnYesUdhConfirmationModal").click(function () {
        //    $("#YahooIdConfirmationModal").modal("hide");
        //    self.save({
        //        bypassCostThresholdCheck: true,
        //        bypassSpqAndQplCheck: true,
        //        bypassCostRangeCheck: true,
        //        bypassPriceRangeCheck: true,
        //        bypassPriceAndCostSameCheck: true,
        //        bypassYahooIdCheck: true
        //    });
        //});
        $('#btnYesMarginLevelNegativeCostModal').click(function () {

            //$("#table-pricing-level")[0].children[2].remove();
            //$('#add-more-pricing-level').prop('disabled', true);
            //viewModel.PricingLevels([]);
            self.isYesNegativeePricingDialog = true;
            $('#btnNoMarginLevelNegativeCostModal').click();
            self.edit({
                bypassCostThresholdCheck: false,
                bypassCostRangeCheck: false,
                bypassPriceRangeCheck: false,
                bypassPriceAndCostSameCheck: false,
                bypassValidateProductAmountCheck: false

            });


        })
        $("#btnDisableOtherProductUdhConfirmationModal").click(function () {
            $("#YahooIdConfirmationModal").modal("hide");
            self.save({
                bypassCostThresholdCheck: true,
                bypassCostRangeCheck: true,
                bypassPriceRangeCheck: true,
                bypassPriceAndCostSameCheck: true,
                triggerActionId: 1,
                bypassValidateProductAmountCheck: true
            });
        });

        $("#btnKeepBothUdhConfirmationModal").click(function () {
            $("#YahooIdConfirmationModal").modal("hide");
            self.save({
                bypassCostThresholdCheck: true,
                bypassCostRangeCheck: true,
                bypassPriceRangeCheck: true,
                bypassPriceAndCostSameCheck: true,
                triggerActionId: 2,
                bypassValidateProductAmountCheck: true
            });
        });

        self.KitSKU.subscribe(function (val) {
            if (val == null || val == "") {
                //self.SellingSku('');
                return;
            }
         
            //else
            //    self.SellingSku(CalculateSellingSku(val, self.PricingCustomField.case_qty()));
        });

        self.SelectedCost.subscribe(function (val) {

            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("COST") > -1) {
                changeLevel = "";
                return;
            }

            val = parseFloat(val);

            if ($.isNumeric(val) === false) {
                return;
            }

            if (val === 0)
                return;


            var myCost = val;
            var myGrossMarginAmount = parseFloat(self.MarginAmount());
            var myGrossMarginPercent = parseFloat(self.MarginPercent());
            var mySellingPrice = parseFloat(self.Price());
           // self.SelectedCost(myCost);

            // Validation

            //check if cost has exceeded 150% threshold
            var percentChange = self.getCostThreshold(val);
            if (percentChange >= 150 || percentChange <= -150) {
                alertmsg("yellow", 'Cost change threshold of +150% or -150% has been exceeded');
            }

            if (myGrossMarginAmount === 0 && myGrossMarginPercent === 0) {
                self.Price(val);
                return;
            }
            // Calculation
            if (self.IsMarginLocked() === true) {
                if (myGrossMarginPercent !== 0) {
                    if (changeLevel.indexOf('SAP') === -1) {
                        mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);
                    }
                } else if (mySellingPrice !== 0) {
                    if (changeLevel.indexOf('MAP') === -1) {
                        myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
                    }
                }
            }

            else {
                if (mySellingPrice !== 0) {
                    if (changeLevel.indexOf('MAP') === -1) {
                        myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
                    }
                }
                else if (myGrossMarginPercent !== 0) {
                    if (changeLevel.indexOf('SAP') === -1) {
                        mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);
                    }
                }

            }

            changeLevel += "COST,";

            //if saleprice has value, update it
            if (self.hasSalePriceForClearance()) {

                //if ((changeLevel2.match(/,/g) || []).length === 1 || changeLevel2.indexOf("SPS") > -1) {
                //    changeLevel2 = "";
                //    return;
                //}
                if (self.IsMarginNegative()) {
                    var salePrice1 = self.SalePrice();
                    var newSalePrice1 = calculateGrossMarginPercent(val, salePrice1);
                    changeLevel2 += "SPS,";
                    self.IsMarginNegative(false);
                    self.salePriceForClearanceMarginPercent(newSalePrice1);
                }
                if (self.IsMarginLocked()) {
                    var salePriceMarginPercent = self.salePriceForClearanceMarginPercent();
                    var newSalePrice = calculateSellingPrice(val, salePriceMarginPercent);
                    changeLevel2 += "SPS,";
                    self.SalePrice(newSalePrice);
                }

                else {

                    var salePrice = self.SalePrice();
                    var newSalePrice2 = calculateGrossMarginPercent(val, salePrice);
                    changeLevel2 += "SPS,";
                    self.salePriceForClearanceMarginPercent(newSalePrice2);
                }
            }

            //if quantity discount is there, update their value
            if (self.PricingLevels().length > 0) {
                for (var i = 0; i < self.PricingLevels().length; i++) {
                    var pricingLevel = self.PricingLevels()[i];
                    if (pricingLevel !== null) {

                        if (self.IsMarginLocked()) {
                            var margin = pricingLevel.GrossMarginPercent();
                            pricingLevel.GrossMarginPercent(0);
                            pricingLevel.GrossMarginPercent(margin);
                        }
                        else {
                            var price = pricingLevel.PriceForPricingLevel();
                            pricingLevel.PriceForPricingLevel(0);
                            pricingLevel.PriceForPricingLevel(price);

                        }

                    }
                }
            }

            // Set values
            if (!self.IsMarginLocked()) {
                self.MarginPercent((myGrossMarginPercent));
            }
            self.MarginAmount((mySellingPrice - myCost));

            if (!self.IsSalePriceLocked()) {
                self.Price((mySellingPrice));

            }

        });
       
        self.MarginPercent.subscribe(function(val) {

            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("MAP") > -1) {
                changeLevel = "";
                return;
            }

            val = parseFloat(val);
            if ($.isNumeric(val) === false) {
                return;
            }
            if (val === 0) {
                self.MarginAmount(0);
                self.Price(self.SelectedCost());
                return;
            }

            if (parseFloat(val) >= 100) {
                alertmsg("red", 'Margin percent cannot be greater than or equal to 100');
                self.MarginAmount(0);
                self.MarginPercent(0);
                self.Price(self.SelectedCost());
                return;
            }
            var myCost = parseFloat(self.SelectedCost());
            var myGrossMarginAmount = parseFloat(self.MarginAmount());
            var myGrossMarginPercent = parseFloat(val);
            var mySellingPrice = parseFloat(self.Price());

            // Validation
            if (myCost === 0 && mySellingPrice === 0) //amount check not required
                return;

            // Calculation
            if (myCost !== 0) {
                if (changeLevel.indexOf('SAP') === -1) {
                    mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);
                }
            } else if (mySellingPrice !== 0) {
                if (changeLevel.indexOf('COST') === -1) {
                    myCost = calculateCost(mySellingPrice, myGrossMarginPercent);
                }
            }
            if (changeLevel.indexOf('MAA') === -1 || changeLevel.indexOf('NEGMARG')>-1) {
                myGrossMarginAmount = mySellingPrice - myCost;
                changeLevel = '';
            }
            changeLevel += "MAP,";

            // Set values
            //if (!self.IsCostLocked && !self.IsMarginLocked())
            //    self.SelectedCost((myCost));

            if (!self.IsMarginLocked())
                self.MarginAmount((myGrossMarginAmount));
            
        });

        self.MarginAmount.subscribe(function(val) {

            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("MAA") > -1) {
                changeLevel = "";
                return;
            }

            val = parseFloat(val);
            if ($.isNumeric(val) === false) {
                return;
            }
            if (val === 0) {
                self.MarginPercent(0);
                self.Price(self.SelectedCost());
                return;
            }

            var myCost = parseFloat(self.SelectedCost());
            var myGrossMarginAmount = parseFloat(val);
            var myGrossMarginPercent = parseFloat(self.MarginPercent());
            var mySellingPrice = parseFloat(self.Price());
            
            // Validation
            if (myCost === 0 && mySellingPrice === 0) //percent check not required
                return;

            // Calculation
            if (myCost !== 0) {
                if (changeLevel.indexOf('SAP') === -1) {
                    mySellingPrice = myCost + myGrossMarginAmount;
                }
            } else if (mySellingPrice !== 0) {
                if (changeLevel.indexOf('COST') === -1) {
                    myCost = mySellingPrice - myGrossMarginAmount;
                }
            }

            changeLevel += "MAA,";
            
            if (!self.IsSalePriceLocked()) {
                self.Price((mySellingPrice));

            }
            //self.SelectedCost((myCost));       
           


        });

        self.PricingCustomField.case_qty.subscribe(function (val) {
            val = val.trim();
            self.SellingSku(CalculateSellingSku(self.SkyGeekPN(), val));
            if (val === null || val === '') {
                val = 1;
                self.PricingCustomField.case_qty("1");
            }
            if (self.IsCaseQty() === false) {
                self.IsCaseQty(true);
                CalculateSellingPrice();
               // CalculatePrice();
                CalculateTotalSalePrice();
            }
            self.CaseQuantity(val);
            self.IsCaseQty(false);

        });

        function CalculateSellingPrice() {
            //if (self.IsSellingPrice() === false) {
                var sp = calculateNewSellingPrice(self.Price(), self.PricingCustomField.case_qty());
                self.SellingPrice(sp);
            //}
        }
        function CalculatePrice() {
            if (self.IsPrice() === false) {
                var price = CalculatepriceOnSPChange(self.SellingPrice(), self.PricingCustomField.case_qty());
                self.Price(price);
            }
        }

        function CalculateTotalSalePrice() {
            var tsp = calculateNewSellingPrice(self.SalePrice(), self.PricingCustomField.case_qty());
            self.TotalSalePrice(tsp);

        }

        function CalculateCaseQty() {
            if (self.IsCaseQty() === false) {
                var cquantity = CalculateCaseQuantityOnSPChange(self.SellingPrice(), self.Price());
                self.PricingCustomField.case_qty(cquantity);
            }
        }
        
        self.Price.subscribe(function(val) {
            CalculateSellingPrice();
            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("SAP") > -1) {
                changeLevel = "";
                return;
            }

            val = parseFloat(val);
            if ($.isNumeric(val) === false) {
                return;
            }
            if (val === 0) {
                if (self.MarginPercent() == 0) {
                    alertmsg("yellow", 'Both selling price and margin cannot be zero (0)');
                    //self.CanSave(false);
                }

                return;
            }

           // added start
           // if SellingPrice = Cost, then set Margin% = 0 and Margin$ = 0 (warning to the user that SellingPrice = Cost)
            if (val == self.SelectedCost()) {
                self.MarginPercent(0);
                self.MarginAmount(0);

                //warning to the user that SellingPrice = Cost
                //alertmsg("yellow", 'Selling Price and Cost are same');
                //self.CanSave(false);
                //validateCostPriceRange();
                return;
            }
            //added end

            var myCost = parseFloat(self.SelectedCost());
            var myGrossMarginAmount = parseFloat(self.MarginAmount());
            var myGrossMarginPercent = parseFloat(self.MarginPercent());
            var mySellingPrice = parseFloat(val);

            // Validation
            if (myCost === mySellingPrice) {
                return;
            }
            if (myCost === 0 && myGrossMarginAmount === 0 && myGrossMarginPercent === 0)
                return;

            // Calculation
            //if (self.IsMarginLocked()) {
            //    if (changeLevel.indexOf('COST') === -1) {
            //        myCost = mySellingPrice - myGrossMarginAmount;
            //    }
            //}
            if (myCost !== 0) {
                if (changeLevel.indexOf('MAP') === -1) {
                    myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
                }
            } else if (myGrossMarginPercent !== 0) {
                if (changeLevel.indexOf('COST') === -1) {
                    myCost = calculateCost(mySellingPrice, myGrossMarginPercent);
                }
            }

            changeLevel += "SAP,";

            // Set values
            //

            //if (!self.IsMarginLocked()) {
            //    self.MarginAmount((self.Price() - self.SelectedCost()));
            //}
                myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
                self.MarginPercent((myGrossMarginPercent));
              if (!self.IsCostLocked && !self.IsMarginLocked())
                self.SelectedCost((myCost));

            // }
            changeLevel = "";

            if (self.IsPrice() === false) {
                self.IsPrice(true);
                CalculateSellingPrice();
               // CalculateCaseQty();
            }
            self.IsPrice(false);

        });

        /*
        self.SellingPrice.subscribe(function (val) {
           
            if (self.IsSellingPrice() === false) {
                self.IsSellingPrice(true);
                CalculatePrice();
            }
            
            self.IsSellingPrice(false);
        });*/

        // Check for lock (only 2 lock at most can be locked)
        self.IsCostLocked.subscribe(function(val) {
            if (self.SelectedCost() === 0) {
                $('#LockCost').change(function() {
                    alertmsg("red", 'Cannot lock when cost is zero');
                });
                self.IsCostLocked(false);
            }
            if (self.IsMarginLocked() === true && val === true) {
                alertmsg("red", 'Cannot lock cost when margin is locked');
                self.IsCostLocked(false);
            }
            // self.IsMarginLocked(!val);
        });

        self.IsMarginLocked.subscribe(function (val) {
            self.IsCostLocked = false;
            if (self.MarginAmount() == 0 || self.MarginPercent() == 0) {
                alertmsg("red", 'Cannot lock when margin is zero');
                self.IsMarginLocked(false);
            }
            //self.IsSalePriceLocked(val);
            //if (val === true && self.IsCostLocked() === true) {
            //    alertmsg("red", 'Cannot lock margin when cost is locked');
            //    self.IsMarginLocked(false);
            //}
        });

        // Clearance computation
        self.SalePrice.subscribe(function(val) {

            CalculateTotalSalePrice();
            if ((changeLevel2.match(/,/g) || []).length === 1 || changeLevel2.indexOf("SPS") > -1) {
                changeLevel2 = "";
                return;
            }

            if ($.isNumeric(val) === false) {
                return;
            }

            //if (val === 0) {
            //    self.salePriceForClearanceMarginPercent(0);
            //    return;
            //}

            var grossMarginpercent = calculateGrossMarginPercent(self.SelectedCost(), val);

            if (grossMarginpercent !== 0) {
                changeLevel2 += "SPS,";
            }


            self.salePriceForClearanceMarginPercent(grossMarginpercent);

        });

        self.salePriceForClearanceMarginPercent.subscribe(function(val) {

            if ((changeLevel2.match(/,/g) || []).length === 1 || changeLevel2.indexOf("SPMP") > -1) {
                changeLevel2 = "";
                return;
            }

            if ($.isNumeric(val) === false) {
                return;
            }
            if (!self.hasSalePriceForClearance()) {
                return;
            }
            if (val === 0) {
                self.SalePrice(self.SelectedCost());
                return;
            }

            if (parseFloat(val) >= 100) {
                alertmsg("red", 'Sale Price margin percent cannot be greater than or equal to 100');
                self.SalePrice(self.SelectedCost());
                return;
            }

            var sellingPrice = calculateSellingPrice(self.SelectedCost(), val);

            if (sellingPrice !== 0) {
                changeLevel2 += "SPMP,";
            }

            self.SalePrice(sellingPrice);

        });

        $('#btnYesCostThresholdModal').click(function() {
            $('#CostThresholdModal').modal('hide');
            self.save({
                bypassCostThresholdCheck: true
            });
        });

        $("#btnYesMarginNegativeDueToCostModal").click(function () {
            $('#MarginNegativeDueToCostModal').hide();
            changeLevel += "NEGMARG";
            viewModel.MarginPercent(25);
            viewModel.IsMarginLocked(true);
            self.IsMarginNegative(true);
            $("#table-pricing-level")[0].children[2].remove();
            $('#add-more-pricing-level').prop('disabled', true)
            $('#btnNoMarginNegativeDueToCostModal').click()
           
            bypassNegativeMargin = true;
            viewModel.PricingLevelsArchive=viewModel.PricingLevels();
            viewModel.PricingLevels([]);
            if (isCreate) {
                self.create({
                    bypassCostThresholdCheck: false,
                    bypassCostRangeCheck: false,
                    bypassPriceRangeCheck: false,
                    bypassPriceAndCostSameCheck: false,
                    bypassValidateProductAmountCheck: false

                });

            }
            else {
                self.edit({
                    bypassCostThresholdCheck: false,
                    bypassCostRangeCheck: false,
                    bypassPriceRangeCheck: false,
                    bypassPriceAndCostSameCheck: false,
                    bypassValidateProductAmountCheck: false

                });
            }

        })
        
    };

//pricing viewmodel
    var PIMPricingLevelViewModel = function(vm) {

        var self = this;
        self.PIMPricingId = ko.observable((vm != null && vm.PIMPricingId != null) ? vm.PIMPricingId : '');

        self.Id = ko.observable((vm != null && vm.Id != null) ? vm.Id : '');

        self.QuantityFrom = ko.observable(vm == null ? 1 : vm.QuantityFrom != null ? vm.QuantityFrom : '');
        //self.QuantityTo = ko.observable(vm == null ? 1 : vm.QuantityTo != null?vm.QuantityTo:'');
        self.GrossMarginPercent = ko.observable(vm == null ? /*(viewModel.MarginPercent())*/ 0 : vm.GrossMarginPercent != null ? (vm.GrossMarginPercent) : '')
            .extend({ numeric: decimal_places });
        self.GrossMarginAmount = ko.observable(vm == null ? /*(viewModel.MarginAmount())*/ 0 : vm.GrossMarginAmount != null ? (vm.GrossMarginAmount) : '')
            .extend({ numeric: decimal_places });
        self.PriceForPricingLevel = ko.observable(vm == null ? 0 : vm.Price != null ? (vm.Price) : '')
            .extend({ numeric: decimal_places });

        self.Price = ko.computed(function() {

            if (self.PriceForPricingLevel() == viewModel.SelectedCost()) {
                alertmsg("yellow", "Warning: Selling price for pricing level '{0}' and cost '{0}' cannot be same".format(self.PriceForPricingLevel(), viewModel.SelectedCost()));
            }

            return self.PriceForPricingLevel();
        });

        self.GrossMarginPercent.subscribe(function(val) {

            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("GMP") > -1) {
                changeLevel = "";
                return;
            }

            if ((changeLevel3.match(/,/g) || []).length === 1 || changeLevel3.indexOf("GMP") > -1) {
                changeLevel3 = "";
                changeLevel = "";

                return;
            }

            val = parseFloat(val);
            if ($.isNumeric(val) === false) {
                return;
            }
            if (val === 0)
                return;
            if (parseFloat(val) >= 100) {
                alertmsg("red", 'Margin percent cannot be greater than or equal to 100');
                self.GrossMarginPercent(0);
                self.GrossMarginAmount(0);
                return;
            }

            var myCost = parseFloat(viewModel.SelectedCost());
            var myGrossMarginAmount = 0;
            var myGrossMarginPercent = parseFloat(val);
            var mySellingPrice = 0;

            // Validation
            if (myCost === 0 || myGrossMarginPercent === 0) //amount check not required
                return;

            // Calculation

            mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);

            myGrossMarginAmount = mySellingPrice - myCost;

            changeLevel += "GMP,";
            changeLevel3 += "GMP,";

            // Set values
            self.GrossMarginAmount(myGrossMarginAmount);

            self.PriceForPricingLevel(mySellingPrice);

        });

        self.PriceForPricingLevel.subscribe(function(val) {

            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("SEP") > -1) {
                changeLevel = "";
                return;
            }

            if ((changeLevel3.match(/,/g) || []).length === 1 || changeLevel3.indexOf("SEP") > -1) {
                changeLevel3 = "";
                changeLevel = "";

                return;
            }

            val = parseFloat(val);
            if ($.isNumeric(val) === false) {
                return;
            }
            if (val === 0)
                return;

            var myCost = parseFloat(viewModel.SelectedCost());
            var myGrossMarginPercent = parseFloat(self.GrossMarginPercent());
            var mySellingPrice = parseFloat(val);

            // Validation
            if (myCost === mySellingPrice) {
                return;
            }
            if (myCost === 0 || mySellingPrice === 0)
                return;

            // Calculation

            if (myCost !== 0) {
                myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);

            }

            changeLevel += "SEP,";
            changeLevel3 += "SEP,";
            // Set values
            //

          //  if (!viewModel.IsMarginLocked()) {
                self.GrossMarginPercent(myGrossMarginPercent);
           // }
            self.GrossMarginAmount(mySellingPrice - myCost);
        });

        self.QuantityFrom.subscribe(function(val) {
            var quantityFrom = parseFloat(val);

            var currentIndex = 0;
            for (var i = 0; i < viewModel.PricingLevels().length; i++) {
                if (viewModel.PricingLevels()[i].QuantityFrom() === val) {
                    currentIndex = i;
                    break;
                }
            }

            if (currentIndex > 0) {

                var previousPricingLevel = viewModel.PricingLevels()[currentIndex - 1];
                var addedPricingLevel = viewModel.PricingLevels()[currentIndex];

                if (previousPricingLevel != null && previousPricingLevel.QuantityFrom() != null && previousPricingLevel.QuantityFrom() !== "") {
                    if (parseFloat(previousPricingLevel.Quantity) < parseFloat(addedPricingLevel.QuantityFrom)) {
                        alertmsg("red", 'Previous Quantity (' + previousPricingLevel.QuantityFrom + ') should be less than current quantity (' + val.quantityFrom + '.');

                        self.QuantityFrom('');
                        return;
                    }
                }
            }
        });

       

       
}

function CalculateSellingSku(sku, caseqty) {

    if ((caseqty !== '' || caseqty !== null || caseqty !== undefined) && caseqty > 1) {
        // return up((caseQuantity * price),2);
        return sku + '-X' + caseqty;
    }

    else
        return sku;
}

//apply ko bindings to viewmodel
    var viewModel = new PimKitsViewModel(kitsVm);

    ko.applyBindings(viewModel);

////To trigger change for SalePrice so that Sale Margin % will be updated
//viewModel.SalePrice(0);
//viewModel.SalePrice((kitsVm != null && kitsVm.SalePrice != null) ? kitsVm.SalePrice : '0');

//disable location in edit mode
    if (typeof viewModel !== 'undefined' && viewModel.Id() !== "" && viewModel.Id() > 0) {
        $('#Location').prop('disabled', true);
    }

//pricing levels for edit mode
    if (kitsVm != null && kitsVm.PricingLevels != null) {
        $.each(kitsVm.PricingLevels, function(indexPricingLevels, valuePricingLevels) {
            viewModel.PricingLevels.push(new PIMPricingLevelViewModel(valuePricingLevels));
        });

    }

    changeLevel = "";

// Activate jQuery Validation
    $("form").validate({ submitHandler: viewModel.save });

    $("#btnCancel").click(function () {

        var returnUrl = getQueryStringByName("returnUrl");
        if (returnUrl != null && returnUrl !== "") {
            window.location = returnUrl;
        }

    });

    $('#btnAddKitItemProduct').click(function () {
    $('#dialog-add-kititemproduct').modal('show');
    return;
});

    $("#PlatformQualifier").select2({
        maximumSelectionSize: 3
    });

    $(".customFieldSelect2").select2({ width: '100%' });

//for Kit Items
/*
    var getDefaultHandsonValue = function(columns) {
        var defaultValue = {};
        for (var i = 0; i < columns.length; i++) {
            var data = columns[i].data;
            var type = columns[i].type;
            if (type === 'checkbox') {
                defaultValue[data] = false;
            } else {
                defaultValue[data] = null;
            }
        }
        return defaultValue;
    }
    */
    if (columns != null) {
        columns = fixJsonQuote(columns);
      //  dataSchema = getDefaultHandsonValue(columns);
}


$(document).ready(function () {
    var data = kitPricingData;
    datatable(kitPricingData);
    initializeKitProductDataTable();
  
   
});
/*
    var
        data = data,
        container = document.getElementById('kits-table'),
        settings = {
            data: data,
            colHeaders: true,
            rowHeaders: true,
            columnSorting: false,
            //search: true,
            columns: columns,
            dataSchema: dataSchema,
            fixedRowsTop: 0,
            stretchH: 'all',
            fixedColumnsLeft: 0,
            //colHeaders: columnsSource,
            contextMenu: true,
             licenseKey: 'non-commercial-and-evaluation',
            //columns: [{ title: 'Kit SKU', type='text' }, { title: 'Part SKU' }, { title: 'Item Name' }, { title: 'Quantity' }],
            minSpareRows: 0,
            manualColumnResize: true,
            
            //manualColumnMove: true,
            afterChange: function(changes, source) {
                try {
                    if (changes != null) {
                        var rowOffset = 0;
                        var colOffset = 0;

                        for (var i = 0; i < changes.length; i++) {
                            if (changes[i] != null) {

                                // [ [row, prop, oldVal, newVal], ... ].

                                var prop = changes[i][1];
                                var columnsList = hot.getSettings().columns;
                                var colIndex;

                                //var quantity = hot.getDataAtProp("Quantity")[rowIndex];
                                var componentPrice = 0;
                                var componentCost = 0;
                                var componentVendorProductCost = 0;

                                for (var j = 0; j < hot.getSettings().data.length; j++) {
                                    var quantity = 0;
                                    var price = 0;
                                    var cost = 0;
                                    if (hot.getDataAtProp("Quantity")[j]) {
                                        quantity = parseFloat(hot.getDataAtProp("Quantity")[j]);
                                    }
                                    if (hot.getDataAtProp("ProductPrice")[j]) {
                                        price = parseFloat(hot.getDataAtProp("ProductPrice")[j]);
                                    }

                                    if (hot.getDataAtProp("ProductCost")[j]) {
                                        cost = parseFloat(hot.getDataAtProp("ProductCost")[j]);
                                    }
                                    if (hot.getDataAtProp("VendorProductCost")[j]) {
                                        vendorProductCost = parseFloat(hot.getDataAtProp("VendorProductCost")[j]);
                                    }

                                    componentPrice += price * quantity;
                                    componentCost += cost * quantity;
                                    componentVendorProductCost += vendorProductCost * quantity;

                                }

                                //trigger cost change
                              
                                changeLevel = '';
                                viewModel.Cost(componentVendorProductCost);
                                viewModel.SelectedCost(componentCost);
                                changeLevel = '';
                                changeLevel3 = '';

                            }
                        }
                    }
                } catch (ex) {

                }
            }

        },


     hot = new Handsontable(container, settings);

*/
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        if ($(e.target).attr('id') === "kit-items") {
            hot.render();
        }
    });

    $('#btnCancel, #btnCancelone').click(function () {
        window.location = urlKitIndex;
    });

    $(".datetimeDataType").datepicker();


//validate price range
    function validatePriceRange() {
        var price = $('#Price').val();
        if (price) {
            if (!isNaN(parseFloat(price)) && isFinite(parseFloat(price)) && parseFloat(price) > 0 && parseFloat(price) <= 0.02) {
              $('#PriceRangeModal').modal("show");
                return false;
            }
        }

        //if we reach here, it's valid. return true
        return true;
    }
    function validateCostPriceRange() {
        var price =$('#Price').val();
        var cost =$('#SelectedCost').val();
        if (price && cost) {
            if (!isNaN(parseFloat(price)) && isFinite(parseFloat(price)) && parseFloat(price) > 0
                && !isNaN(parseFloat(cost)) && isFinite(parseFloat(cost)) && parseFloat(cost) > 0) {
                price = roundToTwo(price);
                cost = roundToTwo(cost);
                if (price === cost) {
                    alertmsg('red', 'Rounded Selected Cost = Rounded Selling Price, Increase Margin or Enable Sales Price to continue');
                    return false;
                }
            }
        }

        //if we reach here, it's valid. return true
        return true;
    }
    function roundToTwo(num) {
    return +(Math.round(num + "e+2") + "e-2");
}
    $('#btnYesPriceRangeModal').click(function() {
        //requirement: the form/javascript that includes this .js file should have viewModel as its parent MVVM
        if (typeof viewModel !== 'undefined' && viewModel !== null) {
            $('#PriceRangeModal').modal('hide');
            viewModel.save({
                bypassCostThresholdCheck: true,
                bypassCostRangeCheck: true,
                bypassPriceRangeCheck: true
            });
        } else {
            console.log('viewModel not defined');
        }

    });

//validate price and cost same
    function validatePriceAndCostSame() {
        var price = $('#Price').val();
        var cost = $('#SelectedCost').val();

        if (price && cost)
            if (!isNaN(parseFloat(price)) && isFinite(parseFloat(price)) && parseFloat(price) > 0
                && !isNaN(parseFloat(cost)) && isFinite(parseFloat(cost)) && parseFloat(cost) > 0
                && price == cost) {
                $('#PriceAndCostSameModal').modal("show");
                return false;
            }

        //if we reach here, it's valid. return true
        return true;
    }

    $('#btnYesPriceAndCostSameModal').click(function() {
        //requirement: the form/javascript that includes this .js file should have viewModel as its parent MVVM
        if (typeof viewModel !== 'undefined' && viewModel !== null) {
            $('#PriceAndCostSameModal').modal('hide');
            viewModel.save({
                bypassCostThresholdCheck: true,
                bypassCostRangeCheck: true,
                bypassPriceRangeCheck: true,
                bypassPriceAndCostSameCheck: true
            });
        } else {
            console.log('viewModel not defined');
        }
    });
//} catch (e) {
//    alertmsg("red", e.toString());
//}


function validateSaleMarginGreaterThanProductMargin() {

    var saleMargin = $('#SalePriceForClearanceMarginPercent').val();
    var productMargin = $('#MarginPercent').val();

    if (saleMargin && productMargin) {

        if (parseFloat(saleMargin) == 0 && parseFloat(productMargin) == 0) {
            return true;
        }
        if (parseFloat(saleMargin) >= parseFloat(productMargin)) {
            $('#SaleMarginModal').modal('show');
            return false;
        }


    }
    return true;
}

$('#btnYesSaleMarginModal').click(function () {

    $('#btnNoSaleMarginModal').click();
    viewModel.IsValidate = true;
    bypassNegativeMargin = true;
    byPassSaleMargin = true;
    viewModel.save({
      
        bypassCostRangeCheck: true,
        bypassValidateProductAmountCheck: true,
        bypassCostRangeCheck: true,
        bypassPriceRangeCheck: true,
        bypassPriceAndCostSameCheck: true,
        bypassQuantityDiscountRoundUpCheck: true,
        bypassKitQuantityDiscountRoundUpCheck: true,
        bypassValidateProductAmountCheck: true,
        bypassSaleMarginGreaterThanProductMargin: true
    })
})




function initializeKitProductDataTable() {
    $('#PIMProductAssociateDataTable').dataTableWithFilter({
        "destroy": true,
        "bJQueryUI": true,
        "bServerSide": true,
        "bFilter": true,
        "scrollX": true,
        "bSort": false,
        "bScrollCollapse": true,
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": tableToolsPath,
            "aButtons": tableToolsOptions
        },
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlListProducts,
        "sServerMethod": "POST",

        // Initialize our custom filtering buttons and the container that the inputs live in
        filterOptions: { searchButton: "ProductCatalogSearchText", clearSearchButton: "ProductCatalogClearSearch", searchContainer: "ProductCatalogSearchContainer" },
        "fnPreDrawCallback": function () {
            // gather info to compose a message
            var ob = this;
            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
            //SGtableloading('fa-plane', '', this);


        },
        "fnDrawCallback": function (settings) {
            // in case your overlay needs to be put away automatically you can put it here
            var divid = this.closest('.dataTables_wrapper').attr('id');
            SGtableloadingRemove(divid);
            rebuilttooltip();
        }
        
    });
}

$('#btnAssociateProduct').click(function () {
    var eachIds = [];
    var productIdList = '';
    var totalSelectedItemsLength = 0;
    var totalvalidItems = 0;
    var eachKitIds = [];
    var kitFlag = 0;
    var disabledProduct = 0;
    var sameProduct = 0;
    $('input[type=checkbox][class=ProductCatalogSelector]').each(function () {

        if (this.checked) {
            totalSelectedItemsLength++;
            //for disabled product check
            if ($(this).parent('td').next('td').find('div').find('.fa-ban')[0]) {
                disabledProduct = 1;
            }
            if ($(this).parent('td').find('i')[0]) { // for primary product
                    if (pIdList.includes(parseInt(this.value))) {
                        sameProduct = 1;
                    }
                    else {
                        pIdList.push(parseInt(this.value));
                        eachIds.push(this.value);
                        totalvalidItems++;
                    }

                }
            }

         });

   
    if (sameProduct == 1) {
        alertmsg('red', 'Can\'t add  existing product in Kit!');
        return false;
    }
    if (disabledProduct == 1) {
        alertmsg('red', 'Can\'t update Kit using disabled product!');
        return false;
    }
  
    if (totalvalidItems === 0 || totalvalidItems !== totalSelectedItemsLength) {
        alertmsg('red', 'Please select primary products for kit update');
        return false;
    }

  
    var productIdForPricingValidation = '';
    $.each(eachIds, function (index, val) {
        var productId = $("#product_" + val).data("value");
        var productName = $("#product_" + val).text();
        productIdList += productId + ',';
        productIdForPricingValidation += productId + ',';
    });

    //To create Kit product must have pricing information
    productIdForPricingValidation = productIdForPricingValidation.substr(0, productIdForPricingValidation.length - 1);
    
           
                
    $.ajax({
        type: 'POST',
        url: urlKitCreatePricingValidation,
        data: {
            'productIdList': productIdForPricingValidation
        },
        beforeSend: function () {
            SGloading('Loading');
        },
        success: function (response) {

            if (response.Status) {

               //ensure that cost is not zero
                $('#PIMProductAssociateDataTable>tbody>tr').each(function () {

                    var self = $(this);
                    //if(self.class == 'selected')
                    //console.log(self);
                    if (self[0].className.indexOf('selected') > -1) {
                        //console.log(self[0]);
                        //console.log(self[0].td);
                        ttt = self[0];
                    }
                    //price - 3
                    //cost - 8
                });
                //ensure that price is not zero

                var productIdList = '';
                var kitId = '';
                $.each(eachIds, function (index, val) {
                    var productId = $("#product_" + val).data("value");
                    productIdList += productId + ',';
                });

                productIdList = productIdList.substr(0, productIdList.length - 1);
                kitId = viewModel.Id();
                var adata = {
                    kitId: kitId,
                    productIdList: productIdList,
                    returnUrl:''
                };
             
                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(adata),
                    dataType: 'json',
                    cache: false,
                    contentType: 'application/json',
                    url: associateProducts,
                    beforeSend: function () {
                        SGloading('Loading');
                    },
                    success: function (response) {
                        if (response != null && response.Status === true) {
                           // alertmsg("green", response.Message);
                            $('#dialog-add-kititemproduct').modal('hide');
                           
                            var kpd=response.Data.kitPricingData;
                            
                            kpd = kpd.replace(/\\"/g, '"').replace(/"/g, '\\"');
                            kpd = decodeHtml(kpd);
                            kpd = decodeHtml(kpd);
                            kpd = fixNewLineOnly(kpd);

                            $.each(kpd, function (index, val) {
                                kitPricingData.push(val);
                            });
                            recalculateAssociate(kitPricingData);
                            datatable(kitPricingData);
                        }
                        else if (response != null && response.Status === false) {
                            if (response.Data && response.Data === "warn") {
                                //show warning for 0.8 seconds, then show success message
                                alertmsg("yellow", response.Message);
                                setTimeout(function () {
                                    alertmsg("green", "KitItem added successfully!");
                                    window.location.reload();
                                },
                                    3000);
                            } else if (response.Data && response.Data.ConfirmUser) {
                                //this is a label, use text() to set it's text message
                                $("#yahooIdConfirmationMessageLabel").text(response.Message);
                                //show the modal
                                $("#YahooIdConfirmationModal").modal("show");
                                SGloadingRemove();
                            } else {
                                alertmsg("red", response.Message);
                                SGloadingRemove();
                            }
                        }
                        else {
                            alertmsg("red", errorOccurredMessage);
                            SGloadingRemove();
                        }
                    },
                    complete: function () {
                        //$("#btnsave").attr('disabled', false);
                        SGloadingRemove();
                    },
                    error: function (xhr, status, error) {
                        //reset variables here
                        alert(status + ' ' + error);
                    }
                });

            } else {
                alertmsg("red", response.Message);

            }


        },
        complete: function () {
            //SGloadingRemove();
        },
        error: function (xhr, status, error) {
            alert(status + ' ' + error);
        }
    });

});

function datatable(kitPricingData) {
    var data = kitPricingData;
    $.each(data, function (index, val) {
        pIdList.push(val.Id);
    });
    var getDefaultHandsonValue = function (columns) {
        var defaultValue = {};
        for (var i = 0; i < columns.length; i++) {
            var data = columns[i].data;
            var type = columns[i].type;
            if (type === 'checkbox') {
                defaultValue[data] = false;
            } else {
                defaultValue[data] = null;
            }
        }
        return defaultValue;
    }

    if (columns != null) {
      //  columns = fixJsonQuote(columns);
        dataSchema = getDefaultHandsonValue(columns);
    }
        data = data,
        container = document.getElementById('kits-table'),
        settings = {
            data: data,
            colHeaders: true,
            rowHeaders: true,
            columnSorting: false,
            //search: true,
            columns: columns,
            dataSchema: dataSchema,
            fixedRowsTop: 0,
            stretchH: 'all',
            fixedColumnsLeft: 0,
            //colHeaders: columnsSource,
            contextMenu: true,
            licenseKey: 'non-commercial-and-evaluation',
            //columns: [{ title: 'Kit SKU', type='text' }, { title: 'Part SKU' }, { title: 'Item Name' }, { title: 'Quantity' }],
            minSpareRows: 0,
            manualColumnResize: true,
            beforeRemoveRow: function (index, amount) {
                if (viewModel.Id() !== "" && viewModel.Id() !== "0" && viewModel.Id() !== "-1" && viewModel.Id() !== 0 && viewModel.Id() !== -1) {
                    data = kitPricingData[index];
                    dataToDelete.push(data);
                    remove = true;
                } else {
                    remove = true;
                }
                
                
                //$.ajax({
                //    type: 'POST',
                //    data: JSON.stringify({ kitIds: data.KitItemId }),
                //    dataType: 'json',
                //    cache: false,
                //    contentType: 'application/json',
                //    url: deleteKitItem,
                //    beforeSend: function () {
                //        SGloading('Loading');
                //    },
                //    success: function (response) {
                //        remove = true;
                //    }
                //});
               
            },
            afterRemoveRow: function (index, amount) {
                recalculateAssociate(kitPricingData);
                remove = false;
                SGloadingRemove();
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            },
            //manualColumnMove: true,
            afterChange: function (changes, source) {
                try {
                    if (changes != null) {
                        var rowOffset = 0;
                        var colOffset = 0;

                        for (var i = 0; i < changes.length; i++) {
                            if (changes[i] != null) {

                                // [ [row, prop, oldVal, newVal], ... ].

                                var prop = changes[i][1];
                                var columnsList = hot.getSettings().columns;
                                var colIndex;


                                //var quantity = hot.getDataAtProp("Quantity")[rowIndex];
                                var componentPrice = 0;
                                var componentCost = 0;
                                var componentVendorProductCost = 0;

                                for (var j = 0; j < hot.getSettings().data.length; j++) {
                                    var quantity = 0;
                                    var price = 0;
                                    var cost = 0;
                                    if (hot.getDataAtProp("Quantity")[j]) {
                                        quantity = parseFloat(hot.getDataAtProp("Quantity")[j]);
                                    }
                                    if (hot.getDataAtProp("ProductPrice")[j]) {
                                        price = parseFloat(hot.getDataAtProp("ProductPrice")[j]);
                                    }

                                    if (hot.getDataAtProp("ProductCost")[j]) {
                                        cost = parseFloat(hot.getDataAtProp("ProductCost")[j]);
                                    }
                                    if (hot.getDataAtProp("VendorProductCost")[j]) {
                                        vendorProductCost = parseFloat(hot.getDataAtProp("VendorProductCost")[j]);
                                    }
                                    if (hot.getDataAtProp("SellingSku")[j]) {
                                        var parentSellingSku = hot.getDataAtProp("SellingSku")[j];
                                        parentSellingSkuArray = parentSellingSku.split('-X');

                                        if (quantity > 1) {
                                            hot.getSettings().data[j]["SellingSku"] = parentSellingSkuArray[0] + '-X' + quantity;
                                           
                                        }
                                        else
                                            hot.getSettings().data[j]["SellingSku"] = parentSellingSkuArray[0];

                                        hot.render();
                                    }

                                    componentPrice += price * quantity;
                                    componentCost += cost * quantity;
                                    componentVendorProductCost += vendorProductCost * quantity;

                                }

                                //trigger cost change

                                changeLevel = '';
                                viewModel.Cost(componentVendorProductCost.toFixed(2));
                                viewModel.SelectedCost(componentCost.toFixed(2));
                                changeLevel = '';
                                changeLevel3 = '';

                            }
                        }
                    }
                } catch (ex) {

                }
            },
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            outsideClickDeselects: false 

        },
        hot = new Handsontable(container, settings);
}
//function recalculate(data) {
//    var cost = 0;
//    var price = 0;
//    var vendorProductCost = 0;
//    $.each(data, function (index, val) {
//        cost = cost + parseFloat(val.ProductCost) * parseFloat(val.Quantity);
//        price = price + parseFloat(val.ProductPrice) * parseFloat(val.Quantity);
//        vendorProductCost = vendorProductCost + parseFloat(val.VendorProductCost) * parseFloat(val.Quantity);
//    });
//    viewModel.Cost(vendorProductCost);
//    viewModel.SelectedCost(cost);

//    if (viewModel.MarginPercent() === 0) {
//        //Price equal to cost for margin 0
//        price = cost;
//    }
//    viewModel.Price(price);
//}

function recalculateAssociate(data) {
    var cost = 0;
    var vendorProductCost = 0;
    $.each(data, function (index, val) {
        cost = cost + parseFloat(val.ProductCost) * parseFloat(val.Quantity);
        vendorProductCost = vendorProductCost + parseFloat(val.VendorProductCost) * parseFloat(val.Quantity);
    });
    viewModel.Cost(vendorProductCost.toFixed(2));
    viewModel.SelectedCost(cost.toFixed(2));

    
}
//$('#deletebutton').on('click', function () {
//    //container = document.getElementById('kits-table');
//    var selection = $('kits-table').handsontable('getSelected');
//    $('kits-table').handsontable('alter', 'remove_row', selection[0], selection[2]);
//});
