﻿var kitDatatable;
//data = fixJsonQuote(data);

//var getDefaultHandsonValue = function (columns) {
//    var defaultValue = {};
//    for (var i = 0; i < columns.length; i++) {
//        var data = columns[i].data;
//        var type = columns[i].type;
//        if (type === 'checkbox') {
//            defaultValue[data] = false;
//        } else {
//            defaultValue[data] = null;
//        }
//    }
//    return defaultValue;
//}

//if (columns != null) {
//    columns = fixJsonQuote(columns);
//    dataSchema = getDefaultHandsonValue(columns);
//}

//var
//    data = data ,
//    container = document.getElementById('kits-table'),
//    settings = {
//        data: data,
//        colHeaders: true,
//        rowHeaders: true,
//        columnSorting: false,
//        //search: true,
//        columns: columns,
//        dataSchema: dataSchema,
//        fixedRowsTop: 0,
//        fixedColumnsLeft: 2,
//        //colHeaders: columnsSource,
//        contextMenu: true,
//        //columns: [{ title: 'Kit SKU', type='text' }, { title: 'Part SKU' }, { title: 'Item Name' }, { title: 'Quantity' }],
//        minSpareRows: 0,
//         manualColumnResize: true,
//         manualColumnMove: true
//    },
//    hot;

//hot = new Handsontable(container, settings);


$('#btnEdit').click(function (e) {
    var i = 0;
    var kitId = 0;
    $('input[type=checkbox][class=Kitselector]').each(function () {

        if (this.checked) {
            i++;
            kitId = this.value;
        }
    });

    if (i === 0) {
        alertmsg('red', 'Please select kit to edit');
        e.preventDefault();
    } else if (i > 1) {
        alertmsg('red', 'Please select single kit to edit');
        e.preventDefault();
    } else {
        var url = urlEditKit + "?Id=" + kitId;
        if (e.ctrlKey)
            window.open(url, '_blank');
        else
            window.location = url;
    }
});


$('#btnDeleteKit').hide();

function initializeKitDatatable() {

    kitDatatable = $('#kit-data-table').dataTable({
        "deferRender": true,
        "bServerSide": true,
        "bSort": false,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlKitDataTable,
        "bProcessing": true,
        "destroy": true,
        "scrollX": true,
        "stretchH": 'All',
        "fnPreDrawCallback": function () {
            // gather info to compose a message
            var ob = this;
            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
            //SGtableloading('fa-plane', '', this);


        },
        "fnDrawCallback": function () {
            // in case your overlay needs to be put away automatically you can put it here
            var divid = this.closest('.dataTables_wrapper').attr('id');
            SGtableloadingRemove(divid)
        }
    });

}

initializeKitDatatable();

$(function() {
    getKitItem('');
});

//var kitItemDataTable;

var getKitItem = function (kitId) {
    

    $("#kit-item-Div").html("");
    $('#kit-item-Div').append('<table id="kit-item-data-table"><thead><tr></tr></thead></table>');

    //add heading to table
    if (KitItemDataTableColumnsViewData) {
        
        var sColumns = decodeHtml(KitItemDataTableColumnsViewData);
        sColumns = fixJsonQuote(sColumns);
        
        $.each(sColumns, function (key, value) {
            $('#kit-item-data-table thead tr').append('<th>' + value + '</th>');
        });

        kitItemDataTable = $('#kit-item-data-table').dataTable({
            "deferRender": true,
            "bServerSide": true,
            "bSort": false,
            "aLengthMenu": dataTableMenuLength,
            "iDisplayLength": dataTableDisplayLength,
            "sAjaxSource": urlKitItemDataTable + '?kitId=' + kitId,
            "bProcessing": true,
            "scrollX": true,
            "destroy": true,

            //"fnServerData": function(sSource, aoData, fnCallback) {
            //    aoData.push({ "kitId": kitId });
            //    $.ajax({
            //        "dataType": 'json',
            //        "contentType": "application/json; charset=utf-8",
            //        "type": "GET",
            //        "url": sSource,
            //        "data": aoData,
            //        "success": function(msg) {
            //            var json = jQuery.parseJSON(msg.d);
            //            fnCallback(json);
            //        },
            //        error: function(xhr, textStatus, error) {
            //            if (typeof console == "object") {
            //                //console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
            //            }
            //        }
            //    });
            //},
            "drawCallback": function(settings) {
                //tooltip
                rebuilttooltip();
                TblpgntBtm();
            },
            "fnPreDrawCallback": function () {
                // gather info to compose a message
                var ob = this;
                SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                //SGtableloading('fa-plane', '', this);
          


            },
            "fnDrawCallback": function () {
                // in case your overlay needs to be put away automatically you can put it here
                var divid = this.closest('.dataTables_wrapper').attr('id');
                SGtableloadingRemove(divid);
                TblpgntBtm();
            }
        });
    }
}


$('#btnDelete').click(function (e) {
    var i = 0;
    $('input[type=checkbox][class=Kitselector]').each(function () {
        if (this.checked) {
            i++;
        }
    });
    if (i === 0) {
        alertmsg('red', 'Please select kit item to delete');
        e.preventDefault();
    } else {
        $('#btnDeleteKit').click();
    }
});

$('#btnDeletefromModel').click(function (e) {

    SGloading('Processing...');
    var eachIds = [];
    $('input[type=checkbox][class=Kitselector]').each(function () {
        if (this.checked) {
            eachIds.push(this.value);
        }
    });

    jQuery.ajaxSettings.traditional = true;
    $.ajax({
        url: urlDeleteKits,
        data: {
            kitIds: eachIds
        },
        type: 'POST',
        success: function (data) {
            alertmsg("green", data);
            $("#deletekitNobtn").click();
            kitDatatable.fnReloadAjax();
            SGloadingRemove();
        }
    });
});

$('#btnKitPricing').click(function (e) {
    var eachIds = [];
    var kitItemIdList = '';
    $('input[type=checkbox][class=Kitselector]').each(function () {
        if (this.checked) {
            eachIds.push(this.value);
            kitItemIdList += (this.value.toString() + "_0_1,"); //_0 (for no vpc) _1 (for isKit)
        }
    });
    if (eachIds.length === 0) {
        alertmsg('red', 'Please select kit item(s) for pricing');
        e.preventDefault();
        return;
    }
    ////console.log(kitIdList);
    kitItemIdList = kitItemIdList.substring(0, kitItemIdList.length - 1);

    var url = urlKitPricing + '?productIdList=' + kitItemIdList+'&isKit=true';

    //http://localhost:56771/Datahub/BulkPricing?productIdList=256_0_1,266_0_1,270_0_1

    if (e.ctrlKey)
        window.open(url, '_blank');
    else
        window.location.href = url;

});

$('#btnLoadKitItem').click(function (e) {
    var eachIds = [];
    var kitId = '';
    $('input[type=checkbox][class=Kitselector]').each(function () {
        if (this.checked) {
            eachIds.push(this.value);
            kitId = this.value;
        }
    });
    if (eachIds.length !== 1) {
        alertmsg('red', 'Please select one kit');
        e.preventDefault();
        return;
    }
    getKitItem(kitId);
});

$('body').on('click', '#kit-data-table tr input[type=checkbox], #kit-data-table tr', function (e) {
   
    if (e.target.tagName.toLowerCase() !== 'a') {
        setTimeout(function () {
            var count = 0;
            var kitId = '';
            $('input[type=checkbox][class=Kitselector]').each(function () {
                if (this.checked) {
                    count++;
                    kitId = this.value;
                }
            });

            //do not load when multiple items are selected or control key is pressed
            if (count === 1 && !e.ctrlKey) {
                getKitItem(kitId);
            } else if (count === 0) {
                getKitItem('');
            }

        }, 200);
    }

       


});

$("#btn-add-kit-items").click(function(e) {
    var i = 0;
    var kitId = 0;
    $('input[type=checkbox][class=Kitselector]').each(function () {
        if (this.checked) {
            i++;
            kitId = $(this).val();
        }
    });
    if (i === 0) {
        alertmsg('red', message_SelectKit_ToAddItems);
        e.preventDefault();
    } else {
        try {
            var url = urlCatalogIndex + "&kitId=" + kitId;
            $("#add-kitItems-link").attr('href', url);
            console.log(url);
            $("#dialog-add-kit-items").modal('show');
        } catch (e) {
            console.log(e);
        }

    }


});


$("#kit-item-Div").on("click", "#selectAllCheckBoxKitItems", function () {
  var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.KitItemselector').prop("checked", true);
    } else {
        $('.KitItemselector').prop("checked", false);
    }
});


