﻿/***
changeLevels List:
 COST = cost
 MAP= Margin Percent
 MAA = Margin Amount
 SAP = Sale Price
 GMP = Gross Margin Percent
 GMA = Gross Margin Amount
 SEP = Selling Price
***/
try {
    $("#ProductCategoryId").select2();

    var changeLevel = "";
    var changeLevel2 = "";
    var changeLevel3 = "";

//Json parse viewbag elements
//decode viewModel
    kitsVm = kitsVm.replace(/\\"/g, '"').replace(/"/g, '\\"');
    kitsVm = decodeHtml(kitsVm);//.replace(/&amp;singlequot;/g, "'").replace(/&singlequot;/g, "'");

//Parse viewModel
    if (kitsVm && kitsVm !== "[]") {

        kitsVm = fixJsonQuote(kitsVm);
        ReplaceSingle2Quote(kitsVm);
    } else
        kitsVm = null;

//parse value required custom fields 
    if (valueRequiredCustomFieldsViewBag)
        valueRequiredCustomFieldsViewBag = fixJsonQuote(valueRequiredCustomFieldsViewBag);
    else
        valueRequiredCustomFieldsViewBag = null;


//parse read only fields
    if (customFieldsReadOnlyViewBag)
        customFieldsReadOnlyViewBag = fixJsonQuote(customFieldsReadOnlyViewBag);
    else
        customFieldsReadOnlyViewBag = null;

    unitOfMeasureListViewBag = fixJsonQuote(unitOfMeasureListViewBag);

    var unitOfMeasureList = [];
    $.each(unitOfMeasureListViewBag, function(index, val) {
        unitOfMeasureList.push(val.Description);
    });
//parse additinal customfields viewbag
    customFieldsAdditionalViewBag = fixJsonQuote(customFieldsAdditionalViewBag);

//push additional customfields to list
    var customFieldsAdditionalViewBagList = [];
    $.each(customFieldsAdditionalViewBag, function(index, val) {
        customFieldsAdditionalViewBagList.push({
            Name: val.Name,
            Id: val.Id,
            DataType: val.DataType,
            AllowedValuesSdtId: val.AllowedValuesSdtId,
            DefaultValue: val.DefaultValue
        });
    });
//push readOnly Fields
    var customFieldsReadOnlyViewBagList = [];
    $.each(customFieldsReadOnlyViewBag, function(index, val) {
        customFieldsReadOnlyViewBagList.push({
            Name: val.Name,
            Id: val.Id,
            DataType: val.DataType,
            AllowedValuesSdtId: val.AllowedValuesSdtId,
            DefaultValue: val.DefaultValue
        });
    });
//parse required customfields viewbag
    customFieldsRequiredViewBag = fixJsonQuote(customFieldsRequiredViewBag);

//push required customfields to list
    var customFieldsRequiredViewBagList = [];
    $.each(customFieldsRequiredViewBag, function(index, val) {
        customFieldsRequiredViewBagList.push({
            Name: val.Name,
            Id: val.Id,
            DataType: val.DataType,
            AllowedValuesSdtId: val.AllowedValuesSdtId,
            DefaultValue: val.DefaultValue
        });
    });

//Concat required and additional list to
    var customFieldsGenerateDomList = customFieldsRequiredViewBagList.concat(customFieldsAdditionalViewBagList);
//parse KitItem data
    var data = fixJsonQuote(kitPricingData);
    var cost = 0;
    var price = 0;
    var marginPercent = 0;
    var marginAmount = 0;

//Calculate Cost and Price if is in Create Mode
    if (kitsVm == null || kitsVm.Cost == null) {
        if (data) {
            $.each(data, function(index, val) {
                cost = cost + parseFloat(val.ProductCost) * parseFloat(val.Quantity);
                price = price + parseFloat(val.ProductPrice) * parseFloat(val.Quantity);
            });

            //Price equal to cost for margin 0
            price = cost;
            //marginPercent = calculateGrossMarginPercent(cost, price);
            //marginAmount = convertToGrossMarginAmount(cost, marginPercent);
        }
    } else if (kitsVm && kitsVm.Cost) {
        cost = kitsVm.Cost;
        price = kitsVm.Price;
        marginPercent = kitsVm.MarginPercent;
        marginAmount = kitsVm.MarginAmount;
    }

//construct control elements as per data type
    function ConstructControlForDataType(currentIndex, dataType, cssClass, hasEmptyValue, allowedValueSdtId, defaultValue) {

        if (dataType === "DATETIME" || dataType === "DATE") {

            $("#itemTextbox_" + currentIndex)
                .show()
                .attr("class", "form-control " + cssClass)
                .datepicker();
            $("#itemCheckbox_" + currentIndex).hide();
            if ($("#drop_" + currentIndex).data("select2")) {
                $("#drop_" + currentIndex).select2("destroy");
                $("#drop_" + currentIndex).hide();

            } else {
                $("#drop_" + currentIndex).hide();

            }

            if (hasEmptyValue === "true") {
                $("#itemTextbox_" + currentIndex)
                    .val("").change();

                if (defaultValue) {
                    $("#itemTextbox_" + currentIndex)
                        .val(defaultValue).change();
                }

            }


        } else if (dataType === "BIT") {
            $("#itemCheckbox_" + currentIndex).show();
            $("#itemTextbox_" + currentIndex).hide();
            if ($("#drop_" + currentIndex).data("select2")) {
                $("#drop_" + currentIndex).select2("destroy");
                $("#drop_" + currentIndex).hide();

            } else {
                $("#drop_" + currentIndex).hide();

            }


        } else {

            if (allowedValueSdtId != null && allowedValueSdtId !== 0) {

                $("#itemCheckbox_" + currentIndex).hide();
                $("#itemTextbox_" + currentIndex).hide();
                $("#drop_" + currentIndex)
                    .show();


                $.ajax({
                    url: urlGetStaticDataValues,
                    type: 'POST',
                    data: { "staticDataTypeId": allowedValueSdtId },
                    dataType: "json",
                    beforeSend: function() {
                        SGloading('Loading');
                    },
                    success: function(result) {

                        if (result === "NO_DATA") {
                            alertmsg("red", "No options exists for given static data type, please add statice values first.");
                            return;
                        }

                        var jsonResult = $.parseJSON(result);

                        //Create a new select element and then append all options. Once loop is finished, append it to actual DOM object.
                        //This way we'll modify DOM for only one time!

                        var selectOptions = $('<select>');
                        selectOptions.append(
                            $('<option></option>').val("").html(pleaseSelectText));

                        $.each(jsonResult, function(index, val) {

                            selectOptions.append(
                                $('<option></option>').val(val.Id).html(val.Name)
                            );
                        });

                        //clear first before append
                        $("#drop_" + currentIndex).html("");

                        $("#drop_" + currentIndex).append(selectOptions.html());

                        if (defaultValue) {
                            viewModel.CustomTemplatesAll()[currentIndex].AllowedValuesSdtId(defaultValue);
                        }

                        //get the selected value for dropdown list
                        var selectedData = viewModel.CustomTemplatesAll()[currentIndex];
                        var returnedData = $.grep(kitsVm.CustomTemplates, function(element, index) {
                            return element.CustomField === selectedData.CustomField();
                        });

                        if (kitsVm.CustomTemplates[currentIndex] != null) {
                            //var dropSelectedValue = kitsVm.CustomTemplates[currentIndex].FieldValue;
                            if (typeof returnedData !== 'undefined') {
                                if (returnedData !== null && returnedData.length > 0) {
                                    var dropSelectedValue = returnedData[0].FieldValue;

                                    if (dropSelectedValue != null && dropSelectedValue !== "" && dropSelectedValue !== 0) {

                                        viewModel.CustomTemplatesAll()[currentIndex].AllowedValuesSdtId(dropSelectedValue);
                                    }
                                }
                            }
                        }

                        $("#drop_" + currentIndex).select2({ width: '100%' });

                    },
                    complete: function() {
                        SGloadingRemove();
                    },
                    error: function(err) {

                    }
                });
            } else {


                $("#itemTextbox_" + currentIndex)
                    .datepicker("destroy")
                    .show()
                    .attr("class", "form-control " + cssClass);
                $("#itemCheckbox_" + currentIndex).hide();
                if ($("#drop_" + currentIndex).data("select2")) {
                    $("#drop_" + currentIndex).select2("destroy");
                    $("#drop_" + currentIndex).hide();

                } else {
                    $("#drop_" + currentIndex).hide();

                }


                if (hasEmptyValue === "true") {
                    $("#itemTextbox_" + currentIndex)
                        .val("").change();

                    if (defaultValue) {
                        $("#itemTextbox_" + currentIndex)
                            .val(defaultValue).change();
                    }
                }

            }
        }

    }

//get data type of elements
    function RenderElementWithDataType(customFieldId, isForAll) {

        if (isForAll === "true") {

            for (var i = 0; i < viewModel.CustomTemplatesAll().length; i++) {

                customFieldId = viewModel.CustomTemplatesAll()[i].CustomField();
                var returnedData = $.grep(customFieldsGenerateDomList, function(element, index) {
                    return element.Id === customFieldId;
                });

                if (returnedData.length > 0) {
                    var selectedDataType = returnedData[0].DataType;
                    var selectedAllowedValueSdtId = returnedData[0].AllowedValuesSdtId;
                    var defaultValueForAll = returnedData[0].DefaultValue;

                    if (selectedDataType == null)
                        selectedDataType = "VARCHAR(MAX)";
                    ConstructControlForDataType(i, selectedDataType, selectedDataType.replace(/ *\([^)]*\) */g, "").toLowerCase() + "DataType", "false", selectedAllowedValueSdtId, defaultValueForAll);
                }

            }

        } else {
            var currentIndex = 0;
            for (var i = 0; i < viewModel.CustomTemplatesAll().length; i++) {
                if (viewModel.CustomTemplatesAll()[i].CustomField() === customFieldId) {
                    currentIndex = i;
                    break;
                }
            }


            //Search for DataType 
            var returnedData = $.grep(customFieldsGenerateDomList, function(element, index) {
                return element.Id === customFieldId;
            });

            if (returnedData.length > 0) {
                var selectedDataType = returnedData[0].DataType;
                var selectedAllowedValueSdtId = returnedData[0].AllowedValuesSdtId;
                var defaultValue = returnedData[0].DefaultValue;

                ConstructControlForDataType(currentIndex, selectedDataType, selectedDataType.replace(/ *\([^)]*\) */g, "").toLowerCase() + "DataType", "true", selectedAllowedValueSdtId, defaultValue);
                viewModel.CustomTemplatesAll()[currentIndex].FieldAlias(returnedData[0].Name);

            }
        }

    }


//viewmodels
//product catalog viewmodel
    var PimKitsViewModel = function(kitsVm) {
        var self = this;
        self.StoreId = ko.observable((kitsVm != null && kitsVm.StoreId != null) ? kitsVm.StoreId : '');

        // Properties - General
        self.Id = ko.observable((kitsVm != null && kitsVm.Id != null) ? kitsVm.Id : '');
        self.Name = ko.observable((kitsVm != null && kitsVm.Name != null) ? kitsVm.Name : '');
        self.KitSKU = ko.observable((kitsVm != null && kitsVm.KitSKU != null) ? kitsVm.KitSKU : '');
        self.Location = ko.observable((kitsVm != null && kitsVm.Location != null) ? kitsVm.Location : '');
        self.ShortDescription = ko.observable((kitsVm != null && kitsVm.ShortDescription != null) ? kitsVm.ShortDescription : '');
        self.IsDisabled = ko.observable((kitsVm != null && kitsVm.IsDisabled != null) ? kitsVm.IsDisabled : false);

        // Properties - Pricing

        self.IsCostLocked = ko.observable((kitsVm != null && kitsVm.IsCostLocked != null) ? kitsVm.IsCostLocked : true);
        self.IsMarginLocked = ko.observable((kitsVm != null && kitsVm.IsMarginLocked != null) ? kitsVm.IsMarginLocked : false);
        self.Cost = ko.observable(cost)
            .extend({ numeric: decimal_places });

        self.Price = ko.observable(price)
            .extend({ numeric: decimal_places });

        self.MarginPercent = ko.observable(marginPercent)
            .extend({ numeric: decimal_places });

        self.MarginAmount = ko.observable(marginAmount)
            .extend({ numeric: decimal_places });

        self.IsSalePriceLocked = ko.observable((kitsVm != null && kitsVm.IsSalePriceLocked != null) ? kitsVm.IsSalePriceLocked : false);
        self.IsStoreEnabled = ko.observable(isStoreEnabled);
        self.PricingLevels = ko.observableArray([]);

        self.CustomFieldsRequired = ko.observableArray([]);
        self.CustomFieldsAdditional = ko.observableArray([]);
        self.CustomFieldsReadOnly = ko.observableArray([]);
        self.CustomTemplates = ko.computed(function() {
            return self.CustomFieldsRequired().concat(self.CustomFieldsAdditional());
        });
        self.CustomTemplatesAll = ko.computed(function() {
            return self.CustomFieldsRequired().concat(self.CustomFieldsReadOnly()).concat(self.CustomFieldsAdditional());
        });
        self.CountReqAndReadonly = ko.computed(function() {
            return (parseInt(self.CustomFieldsRequired().length) + parseInt(self.CustomFieldsReadOnly().length));
        });
        self.CountReq = ko.computed(function() {
            return (parseInt(self.CustomFieldsRequired().length));
        });
        self.PlatformQualifier = ko.observable((kitsVm != null && kitsVm.PlatformQualifier != null) ? kitsVm.PlatformQualifier : '');
        self.PlatformQualifiers = ko.observableArray((kitsVm != null && kitsVm.PlatformQualifiers != null) ? kitsVm.PlatformQualifiers : []);

        // Properties - Identifiers
        self.ISBN = ko.observable((kitsVm != null && kitsVm.ISBN != null) ? kitsVm.ISBN : '');
        self.NSN = ko.observable((kitsVm != null && kitsVm.NSN != null) ? kitsVm.NSN : '');
        self.UPC = ko.observable((kitsVm != null && kitsVm.UPC != null) ? kitsVm.UPC : '');
        self.EAN = ko.observable((kitsVm != null && kitsVm.EAN != null) ? kitsVm.EAN : '');
        self.MPN = ko.observable((kitsVm != null && kitsVm.MPN != null) ? kitsVm.MPN : '');
        self.ASIN = ko.observable((kitsVm != null && kitsVm.ASIN != null) ? kitsVm.ASIN : '');

        // Properties - Dimensions
        self.Weight = ko.observable((kitsVm != null && kitsVm.Weight != null) ? kitsVm.Weight : '');
        self.Height = ko.observable((kitsVm != null && kitsVm.Height != null) ? kitsVm.Height : '');
        self.Width = ko.observable((kitsVm != null && kitsVm.Width != null) ? kitsVm.Width : '');
        self.Length = ko.observable((kitsVm != null && kitsVm.Length != null) ? kitsVm.Length : '');

        self.UOM = ko.observable((kitsVm != null && kitsVm.UOM != null) ? kitsVm.UOM : '');
        self.ProductCategoryId = ko.observable((kitsVm != null && kitsVm.ProductCategoryId != null) ? kitsVm.ProductCategoryId : '');
        self.ProductCategoryKitIds = ko.observable((kitsVm != null && kitsVm.ProductCategoryKitIds != null) ? kitsVm.ProductCategoryKitIds : []);
        // Properties - Clearance (sale)
        self.hasSalePriceForClearance = ko.observable((kitsVm != null && kitsVm.HasSalePriceForClearance != null) ? kitsVm.HasSalePriceForClearance : false);
        self.SalePrice = ko.observable((kitsVm != null && kitsVm.SalePrice != null) ? kitsVm.SalePrice : '')
            .extend({ numeric: decimal_places });

        self.salePriceForClearanceMarginPercent = ko.observable((kitsVm != null && kitsVm.SalePrice != null) ? kitsVm.SalePrice : '')
            .extend({ numeric: decimal_places });

        self.hasSalePriceForClearance.subscribe(function(val) {
            if (val === false) {
                self.SalePrice('0');
                self.salePriceForClearanceMarginPercent('0');
            }
            else {
                if (self.salePriceForClearanceMarginPercent() === 0 && self.SalePrice() === 0) {
                    self.SalePrice(self.SelectedCost());
                }
            }
            changeLevel2 = "";
        });


        // Functions/Events
        self.addPricingLevel = function() {

            for (var i = 0; i < self.PricingLevels().length; i++) {
                var quantityFrom = self.PricingLevels()[i].QuantityFrom();
                //var quantityTo = self.PricingLevels()[i].QuantityTo();
                if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                    alertmsg('red', 'Quantity should be greater than 1');
                    return;
                }

                var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
                var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

                if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                    alertmsg('red', 'Margin %  should be greater than 0');
                    return;
                }

                if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                    alertmsg('red', 'Margin $  should be greater than 0');
                    return;
                }


            }
            if (self.PricingLevels().length > 1) {
                var quantityFromCurrent = self.PricingLevels()[self.PricingLevels().length - 1].QuantityFrom();
                var quantityFromPrevious = self.PricingLevels()[self.PricingLevels().length - 2].QuantityFrom();


                if (quantityFromPrevious != null && quantityFromCurrent != null && parseFloat(quantityFromPrevious) >= parseFloat(quantityFromCurrent)) {
                    alertmsg('red', 'Current "Quantity" should be greater than the previous "Quantity"');
                    return;
                }
            }


            self.PricingLevels.push(new PIMPricingLevelViewModel());
            rebuilttooltip();
        };

        self.addCustomField = function() {

            self.CustomFieldsAdditional.push(new CustomFieldManagerViewModel());
            $(".customFieldSelect2").select2({ width: '100%' });

        };

        self.removeAdditionalCustomField = function(customTemplate) {
            self.CustomFieldsAdditional.remove(customTemplate);

        };

        self.removePricingLevel = function(pricingLevel) {

            self.PricingLevels.remove(pricingLevel);
        };

        self.hasCustomFieldTemplateValue = ko.observable(true);
        self.hasCustomFieldValue = ko.observable(false);

        self.save = function(form) {

            for (var i = 0; i < self.PricingLevels().length; i++) {

                var quantityFrom = self.PricingLevels()[i].QuantityFrom();
                //var quantityTo = self.PricingLevels()[i].QuantityTo();
                if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                    alertmsg('red', 'Please enter Quantity greater than one');
                    return;
                }

                var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
                var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

                if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                    alertmsg('red', 'Margin %  should be greater than 0');
                    return;
                }

                if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                    alertmsg('red', 'Margin $  should be greater than 0');
                    return;
                }

                //if (i < self.PricingLevels().length - 1) {

                //    var prevQuantityFrom = self.PricingLevels()[i].QuantityFrom();
                //    var currQuantityFrom = self.PricingLevels()[i + 1].QuantityFrom();

                //    if (prevQuantityFrom != null && currQuantityFrom != null && parseFloat(prevQuantityFrom) >= parseFloat(currQuantityFrom)) {
                //        alertmsg('red', 'Please enter the values for Quantity in ascending order. Save Cancelled.');
                //        return;
                //    }
                //}
            }


            //validation check for custom fields
            var customFieldList = [];

            //validation for the value required custom fields
            for (var i = 0; i < self.CustomFieldsRequired().length; i++) {
                var customFieldSelected = self.CustomFieldsRequired()[i];
                customFieldList.push(customFieldSelected.CustomField());

                var customFieldFromViewBag = $.grep(customFieldsRequiredViewBag, function(element, index) {
                    return element.Id === customFieldSelected.CustomField();
                });

                // if (!customFieldSelected.FieldValue()) {

                var customFieldDataType = "VARCHAR(MAX)";
                if (customFieldFromViewBag.length > 0) {
                    var selectedDataType = customFieldFromViewBag[0].DataType;
                    if (selectedDataType)
                        customFieldDataType = selectedDataType;
                }
                if (customFieldDataType === "BIT" || customFieldDataType === "bit") {
                    self.CustomFieldsRequired()[i].FieldValue(self.CustomFieldsRequired()[i].FieldValueBoolean());
                } else if (customFieldFromViewBag[0].AllowedValuesSdtId) {
                    self.CustomFieldsRequired()[i].FieldValue(self.CustomFieldsRequired()[i].AllowedValuesSdtId());
                }

                //}

                if (customFieldSelected != null && (customFieldSelected.CustomField() == null || $.isNumeric(customFieldSelected.CustomField()) === false || customFieldSelected.CustomField() === 0)) {

                    // found existing custom field with NullOrEmpty 
                    alertmsg('red', customFieldNameRequiredMessage);
                    return;
                }

                if (valueRequiredCustomFieldsViewBag.indexOf(customFieldSelected.CustomField()) > -1 && $.trim(self.CustomFieldsRequired()[i].FieldValue()) === "") {

                    if (customFieldFromViewBag.length > 0) {
                        alertmsg("red", valuRequiredCustomFieldMessage.format(customFieldFromViewBag[0].Name));
                    }
                    return;
                }

            }

            //validation for the additional custom fields
            for (var i = 0; i < self.CustomFieldsAdditional().length; i++) {

                var customTemplate = self.CustomFieldsAdditional()[i];
                customFieldFromViewBag = $.grep(customFieldsAdditionalViewBag, function(element, index) {
                    return element.Id === customTemplate.CustomField();
                });

                if (customTemplate != null && (customTemplate.CustomField() == null || $.isNumeric(customTemplate.CustomField()) === false || customTemplate.CustomField() === 0)) {

                    //found existing custom field with NullOrEmpty 
                    alertmsg('red', customFieldNameRequiredMessage);
                    return;
                }

                if (!customTemplate.FieldValue()) {

                    customFieldDataType = "VARCHAR(MAX)";
                    if (customFieldFromViewBag.length > 0) {
                        selectedDataType = customFieldFromViewBag[0].DataType;
                        if (selectedDataType)
                            customFieldDataType = selectedDataType;
                    }
                    if (customFieldDataType === "BIT" || customFieldDataType === "bit") {
                        self.CustomFieldsAdditional()[i].FieldValue(self.CustomFieldsAdditional()[i].FieldValueBoolean());
                    } else if (customTemplate.AllowedValuesSdtId()) {
                        self.CustomFieldsAdditional()[i].FieldValue(self.CustomFieldsAdditional()[i].AllowedValuesSdtId());
                    }

                }


                if (!self.CustomFieldsAdditional()[i].FieldValue()) {

                    if (customFieldFromViewBag.length > 0) {
                        alertmsg("red", customFieldValueRequiredMessage.format(customFieldFromViewBag[0].Name));
                    }
                    return;
                }
            }

            //validate for the required custom fields
            if (customFieldsRequiredViewBag) {

                if (!customFieldList) {
                    alertmsg("red", fillUpAllRequiredCustomFields);
                    return;
                }

                var isRequiredFieldMapped = true;
                $.each(customFieldsRequiredViewBag, function(index, val) {
                    if (customFieldList.indexOf(val.Id) === -1) {
                        isRequiredFieldMapped = false;

                    }

                });

                if (isRequiredFieldMapped == false) {
                    alertmsg("red", fillUpAllRequiredCustomFields);
                    return;
                }

            }

            //CustomFieldValue updated so
            //combine  CustomFields = CustomFieldRequired + Additional CustomFields
            self.CustomTemplates = self.CustomFieldsRequired().concat(self.CustomFieldsAdditional());

            //var workflowId = getQueryStringByName("workflowId");
            //var stepId = getQueryStringByName("stepId");

            // Ajax call
            $.ajax({
                type: 'POST',
                url: createUrl,
                cache: false,
                contentType: 'application/json; charset=utf-8',
                data: ko.toJSON({
                    viewModel: self,
                    kitJsonString: JSON.stringify(hot.getSettings().data)
                    //stepId: stepId,
                }),
                dataType: 'json',
                beforeSend: function() {
                    //$("#btnsave").attr('disabled', true);
                    SGloading('Loading');
                },
                success: function(response) {

                    if (response != null && response.Status === true) {
                        alertmsg('green', response.Message);

                        var returnUrl = response.Data;
                        if (returnUrl)
                            window.location.href = returnUrl;

                        var suggestionUrl = getQueryStringByName("returnUrl");
                        if (suggestionUrl) {
                            window.location = suggestionUrl;
                        }
                    } else if (response != null && response.Status === false) {
                        alertmsg('red', response.Message);
                    } else {
                        alertmsg('red', errorOccurredMessage);
                    }
                },
                complete: function() {
                    //$("#btnsave").attr('disabled', false);
                    SGloadingRemove();
                },
                error: function(xhr, status, error) {
                    //reset variables here
                    alert(status + ' ' + error);
                }
            });

            // To actually transmit to server as a regular form post, write this: ko.utils.postJson($("form")[0], self.PricingLevels);
        };

        self.edit = function(form) {


            for (var i = 0; i < self.PricingLevels().length; i++) {
                var quantityFrom = self.PricingLevels()[i].QuantityFrom();
                //var quantityTo = self.PricingLevels()[i].QuantityTo();
                if (quantityFrom == null || parseFloat(quantityFrom) < 2 || parseFloat(quantityFrom) > 2147483647) {
                    alertmsg('red', 'Please enter Quantity greater than one. Save Cancelled.');
                    return;
                }

                var marginPercent = self.PricingLevels()[i].GrossMarginPercent();
                var marginAmount = self.PricingLevels()[i].GrossMarginAmount();

                if (marginPercent == null || parseFloat(marginPercent) <= 0) {
                    alertmsg('red', 'Margin %  should be greater than 0');
                    return;
                }

                if (marginAmount == null || parseFloat(marginAmount) <= 0) {
                    alertmsg('red', 'Margin $  should be greater than 0');
                    return;
                }

                //if (i < self.PricingLevels().length - 1) {

                //    var prevQuantityFrom = parseFloat(self.PricingLevels()[i].QuantityFrom());
                //    var currQuantityFrom = parseFloat(self.PricingLevels()[i + 1].QuantityFrom());

                //    if (prevQuantityFrom != null && currQuantityFrom != null && parseFloat(prevQuantityFrom) >= parseFloat(currQuantityFrom)) {
                //        alertmsg('red', 'Please enter the values for Quantity in ascending order. Save Cancelled.');
                //        return;
                //    }
                //}
            }


            //validation check for custom fields
            var customFieldList = [];

            //validation for the value required custom fields
            for (var i = 0; i < self.CustomFieldsRequired().length; i++) {
                var customFieldSelected = self.CustomFieldsRequired()[i];
                customFieldList.push(customFieldSelected.CustomField());

                var customFieldFromViewBag = $.grep(customFieldsRequiredViewBag, function(element, index) {
                    return element.Id === customFieldSelected.CustomField();
                });

                //if (!customFieldSelected.FieldValue()) {

                var customFieldDataType = "VARCHAR(MAX)";
                if (customFieldFromViewBag.length > 0) {
                    var selectedDataType = customFieldFromViewBag[0].DataType;
                    if (selectedDataType)
                        customFieldDataType = selectedDataType;
                }
                if (customFieldDataType === "BIT" || customFieldDataType === "bit") {
                    self.CustomFieldsRequired()[i].FieldValue(self.CustomFieldsRequired()[i].FieldValueBoolean());
                } else if (customFieldFromViewBag[0].AllowedValuesSdtId) {
                    self.CustomFieldsRequired()[i].FieldValue(self.CustomFieldsRequired()[i].AllowedValuesSdtId());
                }

                //}
                if (customFieldSelected != null && (customFieldSelected.CustomField() == null || $.isNumeric(customFieldSelected.CustomField()) === false || customFieldSelected.CustomField() === 0)) {

                    // found existing custom field with NullOrEmpty 
                    alertmsg('red', customFieldNameRequiredMessage);
                    return;
                }

                if (valueRequiredCustomFieldsViewBag.indexOf(customFieldSelected.CustomField()) > -1 && $.trim(self.CustomFieldsRequired()[i].FieldValue()) === "") {

                    if (customFieldFromViewBag.length > 0) {
                        alertmsg("red", "The value for custom field '{0}' is blank. Either put some valid value for this field or remove it.".format(customFieldFromViewBag[0].Name));
                        //alertmsg("red", valuRequiredCustomFieldMessage.format(customFieldFromViewBag[0].Name));
                    }
                    return;
                }

            }

            //validation for the additional custom fields
            for (var i = 0; i < self.CustomFieldsAdditional().length; i++) {

                var customTemplate = self.CustomFieldsAdditional()[i];
                customFieldFromViewBag = $.grep(customFieldsAdditionalViewBag, function(element, index) {
                    return element.Id === customTemplate.CustomField();
                });

                if (customTemplate != null && (customTemplate.CustomField() == null || $.isNumeric(customTemplate.CustomField()) === false || customTemplate.CustomField() === 0)) {

                    //found existing custom field with NullOrEmpty 
                    alertmsg('red', customFieldNameRequiredMessage);
                    return;
                }

                // if (!customTemplate.FieldValue()) {

                customFieldDataType = "VARCHAR(MAX)";
                if (customFieldFromViewBag.length > 0) {
                    selectedDataType = customFieldFromViewBag[0].DataType;
                    if (selectedDataType)
                        customFieldDataType = selectedDataType;
                }
                if (customFieldDataType === "BIT" || customFieldDataType === "bit") {
                    self.CustomFieldsAdditional()[i].FieldValue(self.CustomFieldsAdditional()[i].FieldValueBoolean());
                } else if (customFieldFromViewBag[0].AllowedValuesSdtId) {
                    self.CustomFieldsAdditional()[i].FieldValue(self.CustomFieldsAdditional()[i].AllowedValuesSdtId());
                }

                //}


                if (customFieldDataType.toLowerCase() !== "bit" && !self.CustomFieldsAdditional()[i].FieldValue()) {

                    if (customFieldFromViewBag.length > 0) {
                        alertmsg("red", valueRequiredOtherCustomFieldMessage.format(customFieldFromViewBag[0].Name));
                    }
                    return;
                }
            }

            //validate for the required custom fields
            if (customFieldsRequiredViewBag) {

                if (!customFieldList) {
                    alertmsg("red", fillUpAllRequiredCustomFields);
                    return;
                }

                var isRequiredFieldMapped = true;
                $.each(customFieldsRequiredViewBag, function(index, val) {
                    if (customFieldList.indexOf(val.Id) === -1) {
                        isRequiredFieldMapped = false;

                    }

                });

                if (isRequiredFieldMapped === false) {
                    alertmsg("red", fillUpAllRequiredCustomFields);
                    return;
                }

            }

            //CustomFieldValue updated so
            //combine  CustomFields = CustomFieldRequired + Additional CustomFields
            self.CustomTemplates = self.CustomFieldsRequired().concat(self.CustomFieldsAdditional());

            //var workflowId = getQueryStringByName("workflowId");
            //var stepId = getQueryStringByName("stepId");

            // Ajax call
            $.ajax({
                type: 'POST',
                url: urlEditKit,
                cache: false,
                contentType: 'application/json; charset=utf-8',
                data: ko.toJSON({
                    viewModel: self,
                    kitJsonString: JSON.stringify(hot.getSettings().data)

                }),
                dataType: 'json',
                beforeSend: function() {
                    //$("#btnsave").attr('disabled', true);
                    SGloading('Loading');
                },
                success: function(response) {

                    if (response != null && response.Status === true) {
                        alertmsg('green', response.Message);

                        var returnUrl = response.Data;
                        if (returnUrl)
                            window.location.href = returnUrl;

                        var suggestionUrl = getQueryStringByName("returnUrl");
                        if (suggestionUrl)
                            window.location = suggestionUrl;
                    } else if (response != null && response.Status === false) {
                        alertmsg('red', response.Message);
                    } else {
                        alertmsg('red', errorOccurredMessage);
                    }
                },
                complete: function() {
                    //$("#btnsave").attr('disabled', false);
                    SGloadingRemove();
                },
                error: function(xhr, status, error) {
                    //reset variables here
                    alert(status + ' ' + error);
                }
            });

            // To actually transmit to server as a regular form post, write this: ko.utils.postJson($("form")[0], self.PricingLevels);
        };

        self.Cost.subscribe(function(val) {

            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("COST") > -1) {
                changeLevel = "";
                return;
            }

            val = parseFloat(val);

            if ($.isNumeric(val) === false) {
                return;
            }

            if (val === 0)
                return;

            var myCost = val;
            var myGrossMarginAmount = parseFloat(self.MarginAmount());
            var myGrossMarginPercent = parseFloat(self.MarginPercent());
            var mySellingPrice = parseFloat(self.Price());

            // Validation
            if (myGrossMarginAmount === 0 && myGrossMarginPercent === 0) {
                self.Price(val);
                return;
            }
            // Calculation
            if (myGrossMarginPercent !== 0) {
                if (changeLevel.indexOf('SAP') === -1) {
                    mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);
                }
            } else if (mySellingPrice !== 0) {
                if (changeLevel.indexOf('MAP') === -1) {
                    myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
                }
            }

            changeLevel += "COST,";

            //if quantity discount is there, update their value
            if (self.PricingLevels().length > 0) {
                for (var i = 0; i < self.PricingLevels().length; i++) {
                    var pricingLevel = self.PricingLevels()[i];
                    if (pricingLevel != null) {
                        var margin = pricingLevel.GrossMarginPercent();
                        pricingLevel.GrossMarginPercent(0);
                        pricingLevel.GrossMarginPercent(margin);
                    }
                }
            }

            // Set values
            if (!self.IsMarginLocked()) {

                self.MarginPercent((myGrossMarginPercent));
            }
            self.MarginAmount((mySellingPrice - myCost));
            if (self.IsMarginLocked())
                self.Price((mySellingPrice));

        });

        self.MarginPercent.subscribe(function(val) {

            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("MAP") > -1) {
                changeLevel = "";
                return;
            }

            val = parseFloat(val);
            if ($.isNumeric(val) === false) {
                return;
            }
            if (val === 0)
                return;
            if (parseFloat(val) >= 100) {
                alertmsg('red', 'Margin percent cannot be greater than or equal to 100');
                self.MarginAmount(0);
                self.MarginPercent(0);
                self.Price(self.Cost());
                return;
            }
            var myCost = parseFloat(self.Cost());
            var myGrossMarginAmount = parseFloat(self.MarginAmount());
            var myGrossMarginPercent = parseFloat(val);
            var mySellingPrice = parseFloat(self.Price());

            // Validation
            if (myCost === 0 && mySellingPrice === 0) //amount check not required
                return;

            // Calculation
            if (myCost !== 0) {
                if (changeLevel.indexOf('SAP') === -1) {
                    mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);
                }
            } else if (mySellingPrice !== 0) {
                if (changeLevel.indexOf('COST') === -1) {
                    myCost = calculateCost(mySellingPrice, myGrossMarginPercent);
                }
            }
            if (changeLevel.indexOf('MAA') === -1) {
                myGrossMarginAmount = mySellingPrice - myCost;
            }
            changeLevel += "MAP,";

            // Set values
            if (!self.IsCostLocked() && !self.IsMarginLocked())
                self.Cost((myCost));
            if (!self.IsMarginLocked())
                self.MarginAmount((myGrossMarginAmount));
            //if (!self.IsSalePriceLocked()) {
            //    self.Price((mySellingPrice));

            //}
        });

        self.MarginAmount.subscribe(function(val) {

            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("MAA") > -1) {
                changeLevel = "";
                return;
            }

            val = parseFloat(val);
            if ($.isNumeric(val) === false) {
                return;
            }
            if (val === 0)
                return;

            var myCost = parseFloat(self.Cost());
            var myGrossMarginAmount = parseFloat(val);
            var myGrossMarginPercent = parseFloat(self.MarginPercent());
            var mySellingPrice = parseFloat(self.Price());

            // Validation
            if (myCost === 0 && mySellingPrice === 0) //percent check not required
                return;

            // Calculation
            if (myCost !== 0) {
                if (changeLevel.indexOf('SAP') === -1) {
                    mySellingPrice = myCost + myGrossMarginAmount;
                }
            } else if (mySellingPrice !== 0) {
                if (changeLevel.indexOf('COST') === -1) {
                    myCost = mySellingPrice - myGrossMarginAmount;
                }
            }

            changeLevel += "MAA,";

            if (!self.IsSalePriceLocked()) {
                self.Price((mySellingPrice));

            }
            if (!self.IsMarginLocked()) {
                myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
                self.MarginPercent((myGrossMarginPercent));
            }
            if (!self.IsCostLocked())
                self.Cost((myCost));


        });

        self.Price.subscribe(function(val) {

            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("SAP") > -1) {
                changeLevel = "";
                return;
            }

            val = parseFloat(val);
            if ($.isNumeric(val) === false) {
                return;
            }
            if (val === 0)
                return;

            var myCost = parseFloat(self.Cost());
            var myGrossMarginAmount = parseFloat(self.MarginAmount());
            var myGrossMarginPercent = parseFloat(self.MarginPercent());
            var mySellingPrice = parseFloat(val);

            // Validation
            if (myCost === mySellingPrice) {
                return;
            }
            if (myCost === 0 && myGrossMarginAmount === 0 && myGrossMarginPercent === 0)
                return;

            // Calculation
            if (self.IsMarginLocked()) {
                if (changeLevel.indexOf('COST') === -1) {
                    myCost = mySellingPrice - myGrossMarginAmount;
                }
            }
            if (myCost !== 0) {
                if (changeLevel.indexOf('MAP') === -1) {
                    myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);
                }
            } else if (myGrossMarginPercent !== 0) {
                if (changeLevel.indexOf('COST') === -1) {
                    myCost = calculateCost(mySellingPrice, myGrossMarginPercent);
                }
            }

            changeLevel += "SAP,";

            // Set values
            //

            if (!self.IsMarginLocked()) {
                //self.MarginAmount((self.Price() - self.Cost()));
                self.MarginPercent((myGrossMarginPercent));

            }

        });

        // Check for lock (only 2 lock at most can be locked)
        self.IsCostLocked.subscribe(function(val) {
            if (self.Cost() == 0) {
                alertmsg('red', 'Cannot lock when cost is zero');
                self.IsCostLocked(false);
            }
            if (self.IsMarginLocked() === true && val === true) {
                alertmsg('red', 'Cannot lock cost when margin is locked');
                self.IsCostLocked(false);
            }
            // self.IsMarginLocked(!val);
        });
        self.IsMarginLocked.subscribe(function(val) {
            if (self.MarginAmount() == 0 || self.MarginPercent() == 0) {
                alertmsg('red', 'Cannot lock when margin is zero');
                self.IsMarginLocked(false);
            }
            self.IsSalePriceLocked(val);
            //if (val === true && self.IsCostLocked() === true) {
            //    alertmsg('red', 'Cannot lock margin when cost is locked');
            //    self.IsMarginLocked(false);
            //}
            //self.IsCostLocked(!val);
        });


        // Clearance computation
        self.SalePrice.subscribe(function(val) {

            if ((changeLevel2.match(/,/g) || []).length === 1 || changeLevel2.indexOf("SPS") > -1) {
                changeLevel2 = "";
                return;
            }
            var grossMarginpercent = calculateGrossMarginPercent(self.Cost(), val);


            changeLevel2 += "SPS,";

            self.salePriceForClearanceMarginPercent(grossMarginpercent);


        });

        self.salePriceForClearanceMarginPercent.subscribe(function(val) {

            if ((changeLevel2.match(/,/g) || []).length === 1 || changeLevel2.indexOf("SPMP") > -1) {
                changeLevel2 = "";
                return;
            }
            if (!self.hasSalePriceForClearance()) {
                return;
            }
            var sellingPrice = calculateSellingPrice(self.Cost(), val);


            changeLevel2 += "SPMP,";


            self.SalePrice(sellingPrice);


        });

    };

//pricing viewmodel
    var PIMPricingLevelViewModel = function(vm) {

        var self = this;
        self.PIMPricingId = ko.observable((vm != null && vm.PIMPricingId != null) ? vm.PIMPricingId : '');

        self.Id = ko.observable((vm != null && vm.Id != null) ? vm.Id : '');

        self.QuantityFrom = ko.observable(vm == null ? 1 : vm.QuantityFrom != null ? vm.QuantityFrom : '');
        //self.QuantityTo = ko.observable(vm == null ? 1 : vm.QuantityTo != null?vm.QuantityTo:'');
        self.GrossMarginPercent = ko.observable(vm == null ? /*(viewModel.MarginPercent())*/ 0 : vm.GrossMarginPercent != null ? (vm.GrossMarginPercent) : '')
            .extend({ numeric: decimal_places });
        self.GrossMarginAmount = ko.observable(vm == null ? /*(viewModel.MarginAmount())*/ 0 : vm.GrossMarginAmount != null ? (vm.GrossMarginAmount) : '')
            .extend({ numeric: decimal_places });
        self.PriceForPricingLevel = ko.observable(vm == null ? 0 : vm.Price != null ? (vm.Price) : '')
            .extend({ numeric: decimal_places });
        self.GrossMarginPercent.subscribe(function(val) {

            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("GMP") > -1) {
                changeLevel = "";
                return;
            }

            if ((changeLevel3.match(/,/g) || []).length === 1 || changeLevel3.indexOf("GMP") > -1) {
                changeLevel3 = "";
                changeLevel = "";

                return;
            }

            val = parseFloat(val);
            if ($.isNumeric(val) === false) {
                return;
            }
            if (val === 0)
                return;
            if (parseFloat(val) >= 100) {
                alertmsg('red', 'Margin percent cannot be greater than or equal to 100');
                self.GrossMarginPercent(0);
                self.GrossMarginAmount(0);
                return;
            }

            var myCost = parseFloat(viewModel.Cost());
            var myGrossMarginAmount = 0;
            var myGrossMarginPercent = parseFloat(val);
            var mySellingPrice = 0;

            // Validation
            if (myCost === 0 || myGrossMarginPercent === 0) //amount check not required
                return;

            // Calculation

            mySellingPrice = calculateSellingPrice(myCost, myGrossMarginPercent);

            myGrossMarginAmount = mySellingPrice - myCost;

            changeLevel += "GMP,";
            changeLevel3 += "GMP,";

            // Set values
            self.GrossMarginAmount(myGrossMarginAmount);

            self.PriceForPricingLevel(mySellingPrice);

        });

        self.PriceForPricingLevel.subscribe(function(val) {

            if ((changeLevel.match(/,/g) || []).length === 2 || changeLevel.indexOf("SEP") > -1) {
                changeLevel = "";
                return;
            }

            if ((changeLevel3.match(/,/g) || []).length === 1 || changeLevel3.indexOf("SEP") > -1) {
                changeLevel3 = "";
                changeLevel = "";

                return;
            }

            val = parseFloat(val);
            if ($.isNumeric(val) === false) {
                return;
            }
            if (val === 0)
                return;

            var myCost = parseFloat(viewModel.Cost());
            var myGrossMarginPercent = parseFloat(self.GrossMarginPercent());
            var mySellingPrice = parseFloat(val);

            // Validation
            if (myCost === mySellingPrice) {
                return;
            }
            if (myCost === 0 || mySellingPrice === 0)
                return;

            // Calculation

            if (myCost !== 0) {
                myGrossMarginPercent = calculateGrossMarginPercent(myCost, mySellingPrice);

            }

            changeLevel += "SEP,";
            changeLevel3 += "SEP,";
            // Set values
            //

            if (!viewModel.IsMarginLocked()) {
                self.GrossMarginPercent(myGrossMarginPercent);
            }
            self.GrossMarginAmount(mySellingPrice - myCost);
        });

        self.QuantityFrom.subscribe(function(val) {
            var quantityFrom = parseFloat(val);

            var currentIndex = 0;
            for (var i = 0; i < viewModel.PricingLevels().length; i++) {
                if (viewModel.PricingLevels()[i].QuantityFrom() === val) {
                    currentIndex = i;
                    break;
                }
            }

            if (currentIndex > 0) {

                var previousPricingLevel = viewModel.PricingLevels()[currentIndex - 1];
                var addedPricingLevel = viewModel.PricingLevels()[currentIndex];

                if (previousPricingLevel != null && previousPricingLevel.QuantityFrom() != null && previousPricingLevel.QuantityFrom() !== "") {
                    if (parseFloat(previousPricingLevel.Quantity) < parseFloat(addedPricingLevel.QuantityFrom)) {
                        alertmsg('red', 'Previous Quantity (' + previousPricingLevel.QuantityFrom + ') should be less than current quantity (' + val.quantityFrom + '.');

                        self.QuantityFrom('');
                        return;
                    }
                }
            }
        });
    }

//custom field manager viewmodel
    var CustomFieldManagerViewModel = function(vm) {

        var self = this;
        self.CustomField = ko.observable(vm != null && vm.CustomField != null ? vm.CustomField : '');
        self.FieldValue = ko.observable(vm != null && vm.FieldValue != null ? vm.FieldValue : '');
        self.FieldValueBoolean = ko.observable(vm != null && vm.FieldValueBoolean != null ? vm.FieldValueBoolean : '');
        self.AllowedValuesSdtId = ko.observable();
        self.FieldAlias = ko.observable(vm != null && vm.FieldAlias != null ? vm.FieldAlias : '');

        self.CustomField.subscribe(function(val) {

            if (val == null)
                return;


            var customFieldId = val;
            if (customFieldId !== "") {

                viewModel.hasCustomFieldValue(true);

                var countDuplication = 0;

                //Check for duplicate
                for (var i = 0; i < viewModel.CustomTemplates().length; i++) {

                    var customTemplate = viewModel.CustomTemplates()[i];

                    if (customTemplate != null && customTemplate.CustomField() != null) {
                        if (parseFloat(customFieldId) === parseFloat(customTemplate.CustomField()) && $.isNumeric(customFieldId) === true) {

                            countDuplication++;
                        }
                    }
                }

                if (parseFloat(countDuplication) > 1) {

                    // found existing custom field
                    alertmsg('red', 'Custom Field cannot be duplicate');
                    self.CustomField('');

                    return;
                }

                RenderElementWithDataType(customFieldId, "false");

                return;
            }
            viewModel.hasCustomFieldValue(false);

        });

        self.FieldValueIsDirty = ko.observable(false);

        self.CustomField.subscribe(function(val) {

            if (val == null)
                return;


            var customFieldId = val;
            if (customFieldId !== "") {

                viewModel.hasCustomFieldValue(true);

                var countDuplication = 0;

                //Check for duplicate
                for (var i = 0; i < viewModel.CustomTemplates().length; i++) {

                    var customTemplate = viewModel.CustomTemplates()[i];

                    if (customTemplate != null && customTemplate.CustomField() != null) {
                        if (parseFloat(customFieldId) === parseFloat(customTemplate.CustomField()) && $.isNumeric(customFieldId) === true) {

                            countDuplication++;

                        }
                    }
                }

                if (parseFloat(countDuplication) > 1) {

                    // found existing custom field
                    alertmsg('red', 'Custom Field cannot be duplicate');
                    self.CustomField('');

                    return;
                }

                RenderElementWithDataType(customFieldId, "false");

                return;
            }
            viewModel.hasCustomFieldValue(false);

        });

        self.FieldValue.subscribe(function(val) {

            var customFieldId = self.CustomField();
            var oldValue = $.grep(kitsVm.CustomTemplates, function(element, index) {
                return element.CustomField === customFieldId;
            });

            var newValue = val;

            if (oldValue.length > 0) {
                if (oldValue[0].FieldValue !== newValue)
                    self.FieldValueIsDirty(true);
                else
                    self.FieldValueIsDirty(false);
            } else
                self.FieldValueIsDirty(true);
        });

        self.FieldValueBoolean.subscribe(function() {
            var customFieldId = self.CustomField();
            var oldValue = $.grep(kitsVm.CustomTemplates, function(element, index) {
                return element.CustomField === customFieldId;
            });

            var newValue = self.FieldValueBoolean();

            if (oldValue.length > 0) {
                if (oldValue[0].FieldValueBoolean !== newValue)
                    self.FieldValueIsDirty(true);
                else
                    self.FieldValueIsDirty(false);
            } else
                self.FieldValueIsDirty(true);
        });

        self.AllowedValuesSdtId.subscribe(function() {
            var customFieldId = self.CustomField();
            var oldValue = $.grep(kitsVm.CustomTemplates, function(element, index) {
                return element.CustomField === customFieldId;
            });

            var newValue = self.AllowedValuesSdtId();

            if (oldValue.length > 0) {
                if (oldValue[0].AllowedValuesSdtId !== newValue)
                    self.FieldValueIsDirty(true);
                else
                    self.FieldValueIsDirty(false);
            } else
                self.FieldValueIsDirty(true);
        });

    }

//apply ko bindings to viewmodel
    var viewModel = new PimKitsViewModel(kitsVm);
    ko.applyBindings(viewModel);

//To trigger change for SalePrice so that Sale Margin % will be updated
    viewModel.SalePrice(0);
    viewModel.SalePrice((kitsVm != null && kitsVm.SalePrice != null) ? kitsVm.SalePrice : '0');

//pricing levels for edit mode
    if (kitsVm != null && kitsVm.PricingLevels != null) {
        $.each(kitsVm.PricingLevels, function(indexPricingLevels, valuePricingLevels) {
            viewModel.PricingLevels.push(new PIMPricingLevelViewModel(valuePricingLevels));
        });

    }

//custom fields for edit mode
    if (kitsVm != null && kitsVm.CustomTemplates != null) {
        $.each(kitsVm.CustomTemplates, function(index, value) {
            if (value.IsReadOnly === 1) {
                viewModel.CustomFieldsReadOnly.push(new CustomFieldManagerViewModel(value));

            } else if (value.IsRequired === 1) {
                viewModel.CustomFieldsRequired.push(new CustomFieldManagerViewModel(value));

            } else {
                viewModel.CustomFieldsAdditional.push(new CustomFieldManagerViewModel(value));

            }
        });
    }

//construct elements as per data type
    RenderElementWithDataType(-1, "true");

    changeLevel = "";

// Activate jQuery Validation
    $("form").validate({ submitHandler: viewModel.save });

    $("#btnCancel").click(function() {

        var returnUrl = getQueryStringByName("returnUrl");
        if (returnUrl != null && returnUrl !== "") {
            window.location = returnUrl;
        }
    });

    $("#PlatformQualifier").select2({
        maximumSelectionSize: 3
    });

    $(".customFieldSelect2").select2({ width: '100%' });

//for Kit Items


    var getDefaultHandsonValue = function(columns) {
        var defaultValue = {};
        for (var i = 0; i < columns.length; i++) {
            var data = columns[i].data;
            var type = columns[i].type;
            if (type === 'checkbox') {
                defaultValue[data] = false;
            } else {
                defaultValue[data] = null;
            }
        }
        return defaultValue;
    }

    if (columns != null) {
        columns = fixJsonQuote(columns);
        dataSchema = getDefaultHandsonValue(columns);
    }

    var
        data = data,
        container = document.getElementById('kits-table'),
        settings = {
            data: data,
            colHeaders: true,
            rowHeaders: true,
            columnSorting: false,
            //search: true,
            columns: columns,
            dataSchema: dataSchema,
            fixedRowsTop: 0,
            stretchH: 'all',
            fixedColumnsLeft: 0,
            //colHeaders: columnsSource,
            contextMenu: true,
            licenseKey: 'non-commercial-and-evaluation',
            //columns: [{ title: 'Kit SKU', type='text' }, { title: 'Part SKU' }, { title: 'Item Name' }, { title: 'Quantity' }],
            minSpareRows: 0,
            manualColumnResize: true,
            //manualColumnMove: true,
            afterChange: function(changes, source) {
                try {
                    if (changes != null) {
                        var rowOffset = 0;
                        var colOffset = 0;

                        for (var i = 0; i < changes.length; i++) {
                            if (changes[i] != null) {

                                // [ [row, prop, oldVal, newVal], ... ].

                                var prop = changes[i][1];
                                var columnsList = hot.getSettings().columns;
                                var colIndex;

                                //var quantity = hot.getDataAtProp("Quantity")[rowIndex];
                                var componentPrice = 0;
                                var componentCost = 0;

                                for (var j = 0; j < hot.getSettings().data.length; j++) {
                                    var quantity = 0;
                                    var price = 0;
                                    var cost = 0;
                                    if (hot.getDataAtProp("Quantity")[j]) {
                                        quantity = parseFloat(hot.getDataAtProp("Quantity")[j]);
                                    }
                                    if (hot.getDataAtProp("ProductPrice")[j]) {
                                        price = parseFloat(hot.getDataAtProp("ProductPrice")[j]);
                                    }

                                    if (hot.getDataAtProp("ProductCost")[j]) {
                                        cost = parseFloat(hot.getDataAtProp("ProductCost")[j]);
                                    }

                                    componentPrice += price * quantity;
                                    componentCost += cost * quantity;

                                }

                                //trigger cost change
                                viewModel.Cost(0);
                                viewModel.Cost(componentCost);

                            }
                        }
                    }
                } catch (ex) {

                }
            }

        },

        hot = new Handsontable(container, settings);

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        if ($(e.target).attr('id') === "kit-items") {
            hot.render();
        }
    });
   
    $('#btnCancel').click(function() {
        window.location = urlKitIndex;
    });
} catch (e) {
    alertmsg('red', e.toString());
}