﻿data = fixJsonQuote(data);

var getDefaultHandsonValue = function (columns) {
    var defaultValue = {};
    for (var i = 0; i < columns.length; i++) {
        var data = columns[i].data;
        var type = columns[i].type;
        if (type === 'checkbox') {
            defaultValue[data] = false;
        } else {
            defaultValue[data] = null;
        }
    }
    return defaultValue;
}

if (columns != null) {
    columns = fixJsonQuote(columns);
    dataSchema = getDefaultHandsonValue(columns);
}

var
    data = data,
    container = document.getElementById('kits-table'),
    settings = {
        data: data,
        colHeaders: true,
        rowHeaders: true,
        columnSorting: false,
        //search: true,
        columns: columns,
        dataSchema: dataSchema,
        fixedRowsTop: 0,
        fixedColumnsLeft: 1,
        //colHeaders: columnsSource,
        contextMenu: true,
        licenseKey: 'non-commercial-and-evaluation',
        //columns: [{ title: 'Kit SKU', type='text' }, { title: 'Part SKU' }, { title: 'Item Name' }, { title: 'Quantity' }],
        minSpareRows: 0,
        manualColumnResize: true,
        manualColumnMove: true
       
    },
    hot;

hot = new Handsontable(container, settings);

$('#btn-save').click(function () {
    var data = JSON.stringify(hot.getSettings().data);
    var kitName = $("#Name").val();
    var kitSku = $("#KitSKU").val();

    // Ajax call
    $.ajax({
        type: 'POST',
        url: urlEditKit,
        cache: false,
        //contentType: 'application/json; charset=utf-8',
        data: {
            'kitJsonString': data, 'kitName': kitName, 'kitSku': kitSku
        },
        dataType: 'json',
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Loading');
        },
        success: function (response) {
            //console.log(response);
            if (response != null && response.Status === true) {
                alertmsg('green', response.Message);
                isSaved = true;
                window.location = urlKitPricing + "?kitItemIdList=" + response.Data;
            } else if (response != null && response.Status === false) {
                alertmsg('red', response.Message);
            } else {
                alertmsg('red', msgErrorOccured);
            }
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            alert(status + ' ' + error);
        }
    });


});