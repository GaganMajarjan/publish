﻿//[
//       // ['Kit SKU', 'Part SKU', 'Item Name', 'Quantity'],
//      //['01-3272-79', '01-3272-79E', 'Magnaflux Zyglo ZL-60D Water Washable Fluorescent Penetrant - 01-3272-79 - 16 oz. Aerosol Can', '9'],
//      //['01-3328-79', '01-3328-79E', 'Magnaflux Zyglo ZP-4B Dry Developer Powder - 1 Pint Container', '9'],
//      //['01-5352-35', '01-5352-20', 'Magnaflux 01-5352-20 Spotcheck SKD-S2 Non-Aqueous Developer - 1 Gallon Can', '4'],
//      //['051131-01693', '051131-01693-EACH', '3M 01693 Clean Sanding 236U Sandpaper Disc - 5inch - C Weight - 80 Grade - EACH', '50'],
//      //['051131-01694', '051131-01694-EACH', '3M 01694 Clean Sanding 236U Sandpaper Disc 5" C 100 Grade - EACH', '50'],
//      //['051141-20750', '051141-20750-EACH', '3M 20750 Clean Sanding 236U Sandpaper Disc - 6" - C Weight - 100 Grade - EACH', '50'],
//      //['051141-20754', '051141-20754-EACH', '3M 20754 Clean Sanding 236U Sandpaper Disc - 6" - C Weight - 220 Grade - EACH', '50'],
//      //['051141-20758', '051141-20758-EACH', '3M 20758 Clean Sanding 236U Sandpaper Disc - 6" - C Weight - 500 Grade - EACH', '250'],
//      //['610555', '6105', 'CAT Pumps 6105 Crankcase Lubricating Oil - 1 Gallon Bottle', '4'],
//      //['063-00500', '063-00500-EACH', 'Cleveland 063-00500 Pressure Plate','2']

//        //['Kit SKU', 'Part SKU', 'Item Name', 'Quantity'],
//        ['ABSPES','42525','Alcor 42525 EGT Lead - 90 Type K','1'],
//        ['','86255','Alcor 86255 EGT/TIT Thermocouple - 3-1/4 Max Diam','3'],
//        ['','46150','Alcor 46150 EGT Indicator - 2-1/4 Type K - C66850','1'],
//        ['AM-KIT','PP-5665','Arrow Magnolia PP-5665 Poly-Glide Polish - Gallon ','1'],
//        ['','CD-2035-GL','Arrow-Magnolia CD-2035 Carbon-X Aircraft Soap - 1 ','2'],
//        ['','CD-3340','Arrow-Magnolia CD-3340 Fleet Wash Soap - 1 Gallon ','1'],
//        ['','CE-5901-001EA','Arrow Magnolia CE-5901 32 oz. Trigger Spray Bottle','2'],
//        ['BC119N-AD94','IC-AD-94-11','ICOM AD-94-11 Adapter Cup','1'],
//        ['','IC-BC-119N-01','ICOM BC-119N 01 Desktop Rapid Charger Base - 117V','4'],
//        ['GNS430AW-B-I','013-00235-00','Garmin GA-35 GPS/WAAS Antenna','1'],
//        ['','010-00413-01','GNS 430AW (black) receiver w/rack, pilots guide a','1'],
//        ['','010-10201-21','TAWS/Terrain datacard - worldwide','6'],
//        ['','010-10546-02','Jeppesen International datacard - WAAS -(1st card ','1'],
//        ['PA36-SET','36-511','Pan American Tool AN4 #4 Screw Paint Cutter','1'],
//        ['','36-512','Pan American Tool AN6 #6 Screw Paint Cutter','7'],
//        ['','36-517','Pan American Tool AN12 #12-1/4 Screw Paint Cutter','3'],
//        ['','36-666','Pan American Tool 2-1/4 Long Paint Cutter Arbor E','1']
//]

data = fixJsonQuote(data);

var getDefaultHandsonValue = function (columns) {
    var defaultValue = {};
    for (var i = 0; i < columns.length; i++) {
        var data = columns[i].data;
        var type = columns[i].type;
        if (type === 'checkbox') {
            defaultValue[data] = false;
        } else {
            defaultValue[data] = null;
        }
    }
    return defaultValue;
}

if (columns != null) {
    columns = fixJsonQuote(columns);
    dataSchema = getDefaultHandsonValue(columns);
}

var
    data = data ,
    container = document.getElementById('kits-table'),
    settings = {
        data: data,
        colHeaders: true,
        rowHeaders: true,
        columnSorting: false,
        //search: true,
        columns: columns,
        dataSchema: dataSchema,
        fixedRowsTop: 0,
        fixedColumnsLeft: 1,
        //colHeaders: columnsSource,
        contextMenu: true,
        licenseKey: 'non-commercial-and-evaluation',
        //columns: [{ title: 'Kit SKU', type='text' }, { title: 'Part SKU' }, { title: 'Item Name' }, { title: 'Quantity' }],
        minSpareRows: 0,
        manualColumnResize: true,
        manualColumnMove: true
       
    },
    hot;

hot = new Handsontable(container, settings);

$('#btn-save').click(function () {
    var data = JSON.stringify(hot.getSettings().data);
    var kitName = $("#Name").val();
    var kitSku = $("#KitSKU").val();
    
    // Ajax call
    $.ajax({
        type: 'POST',
        url: createUrl,
        cache: false,
        //contentType: 'application/json; charset=utf-8',
        data: { 'kitJsonString': data, 'kitName':kitName,'kitSku':kitSku},
        dataType: 'json',
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Loading');
        },
        success: function (response) {
            //console.log(response);
            if (response != null && response.Status === true) {
                alertmsg('green', response.Message);
                isSaved = true;
                window.location = urlKitPricing + "?kitItemIdList=" + response.Data;
            } else if (response != null && response.Status === false) {
                alertmsg('red', response.Message);
            } else {
                alertmsg('red', msgErrorOccured);
            }
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            console.log(xhr);
            alert(status + ' ' + error);
        }
    });


});

