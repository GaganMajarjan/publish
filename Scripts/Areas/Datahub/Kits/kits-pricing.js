﻿var getDefaultHandsonValue = function (columns) {
    var defaultValue = {};
    for (var i = 0; i < columns.length; i++) {
        var data = columns[i].data;
        var type = columns[i].type;
        if (type === 'checkbox') {
            defaultValue[data] = false;
        } else {
            defaultValue[data] = null;
        }
    }
    return defaultValue;
}
var hot;

$(function () {

    if (data != null)
        data = fixJsonQuote(data);

    if (columns != null) {
        columns = fixJsonQuote(columns);
        dataSchema = getDefaultHandsonValue(columns);
    }

    var searchField = document.getElementById('search_field');

    var originalData = [];
    var errorCoordinates = [];

    var container = document.getElementById('kits-pricing-table');

    var numberOfStartColumnsToExcludeValidation = staticColumnsCount != null ? staticColumnsCount : 16;
    numberOfStartColumnsToExcludeValidation = 8;

    var contextMenu = {
        callback: function(key, options) {
            if (key === 'col_add') {
                setTimeout(function() {
                    var cols = hot.getSettings().columns;
                    var qtyTitle = cols[cols.length - 2].title;
                    var disTitle = cols[cols.length - 1].title;

                    var suffix = cols[cols.length - 1].title[cols[cols.length - 1].title.length - 1];
                    var newSuffix = parseInt(suffix) + 1;
                    var newQtyTitle = qtyTitle.replace(suffix, newSuffix);
                    var newDisTitle = disTitle.replace(suffix, newSuffix);

                    cols.push({ 'title': newQtyTitle, 'data': newQtyTitle, 'type': 'numeric', 'format': '0,0.00[0000]' });
                    cols.push({ 'title': newDisTitle, 'data': newDisTitle, 'type': 'numeric', 'format': '$ 0,0.00[0000]' });

                    hot.updateSettings({
                        columns: cols
                    });

                }, 100);
            } else if (key === 'col_remove') {
                setTimeout(function() {
                    var cols = hot.getSettings().columns;

                    //console.log(cols.length);
                    //console.log(numberOfLastColumnsToExcludeValidation + 2);

                    if (cols.length <= numberOfLastColumnsToExcludeValidation + 2) {
                        alertmsg('red', 'Cannot remove the first Qty/Dis column pair');
                        return;
                    }

                    cols.pop();
                    cols.pop();

                    hot.updateSettings({
                        columns: cols
                    });

                }, 100);
            }
        },
        items: {
            'row_above': {},
            'row_below': {},
            'remove_row': {},
            'hsep1': "---------",
            'col_add': {
                name: 'Insert column (Qty/Dis)',
                disabled: function() {
                    // if not last column, disable this option
                    return !(hot.getSelected()[1] === hot.getSettings().columns.length - 1);
                }
            },
            'col_remove': {
                name: 'Remove column (Qty/Dis)',
                disabled: function() {
                    // if not last column, disable this option
                    return !(hot.getSelected()[1] === hot.getSettings().columns.length - 1);
                }
            },
            'hsep2': "---------",
            'undo': {},
            'redo': {},
            'hsep3': "---------",
            //'make_read_only': {},
            'alignment': {}
        }
    };

    var settings = {
        data: data,
        rowHeaders: true,
        stretchH: 'all',
        columnSorting: true,
        search: true,
        contextMenu: contextMenu,
        licenseKey: 'non-commercial-and-evaluation',
        //fixedRowsTop: 0,
        fixedColumnsLeft: 1,
        columns: columns,
        minSpareRows: 0,
        manualColumnResize: true,
        //manualColumnMove: true,
        dataSchema: dataSchema,
        afterChange: function(changes, source) {
            try {
                if (changes) {
                    for (var i = 0; i < changes.length; i++) {
                        if (changes[i]) {
                            
                            if (changes[i][1].indexOf("GrossMarginPercent") > -1) {
                                var rowIndex = changes[i][0];
                                var currentGrossMarginPercent = (changes[i][3]) ? changes[i][3] : 0;
                                var cost = hot.getDataAtProp("ComponentCost")[rowIndex];
                                var sellingPrice = calculateSellingPrice(cost, currentGrossMarginPercent);

                                hot.setDataAtRowProp(rowIndex, "Price", FormatAmount(sellingPrice));
                            }

                        }
                    }
                }
            } catch (e) {

            } 
        }
    };

    hot = new Handsontable(container, settings);

    $('#btn-save').click(function () {
        var data = JSON.stringify(hot.getSettings().data);
        // Ajax call
        $.ajax({
            type: 'POST',
            url: createUrl,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'kitPricingJsonString': data },
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {
                //console.log(response);
                if (response != null && response.Status === true) {
                    alertmsg('green', response.Message);
                    isSaved = true;
                    window.location.reload();
                } else if (response != null && response.Status === false) {
                    alertmsg('red', response.Message);
                } else {
                    alertmsg('red', msgErrorOccured);
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });


    });

    Handsontable.dom.addEvent(searchField, 'keyup', function (event) {
        var queryResult = hot.search.query(this.value);
        //console.log(queryResult);
        hot.render();
    });

});

function normalizeAmount(value) {
    if (value == null)
        return 0;

    if (!$.isNumeric(value))
        return value;

    return value.toString().replace(/$/gi, '').replace(/,/gi, '').replace(/%/gi, '');

}
