﻿$(document).ready(function() {

    $('#MappingDataTable').dataTable({
        "deferRender": true,
        "bServerSide": true,
        "bSort": false,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlGetMappedDataTable,
        "bProcessing": true,
        "scrollX": true

    });
});