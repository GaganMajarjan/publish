﻿
SGmappeddata();

$("select").select2();

var queryimpsrc = importSourceId;
var querydest = destinationTableId;
$("#ProductcatalogLabel").hide();
if (queryimpsrc !== "" && querydest !== "") {


    $.ajax({
        url: urlAdminAutoMapping,
        data: {
            srcid: queryimpsrc,
            desid: querydest
        },
        type: 'GET',
        cache: false,
        success: function(data) {
            $.each(data, function(index, value) {
                $('#' + value.Source + "_dd").select2("val", value.Destination);
                //$('#' + value.Source + "_dd").select2().select2("val", value.Destination);
            });
            SGmappeddata();

            $.ajax({
                url: urlPCatalogAdminAutoMapping,
                data: {
                    srcid: queryimpsrc
                },
                type: 'GET',
                cache: false,
                success: function(data) {
                    $.each(data, function (index, value) {

                        $('#' + value.ETLImportSourceColumnId + "_PCdd").select2("val", value.ETLProductMatchAgainstTableColumnID);
                        $('#' + value.ETLImportSourceColumnId + "_Chkdd").prop('checked', value.IsMatchFullyRecorded);
                        

                    });
                    SGmappeddata();

                }
            });
            
            $.ajax({
                url: urlTransformationRuleAutoMapping,
                data: {
                    importSourceId: queryimpsrc
                },
                type: 'GET',
                cache: false,
                success: function(data) {
                    if (data !== "EMPTY") {

                        $.each(data, function(index, value) {
                            $('#' + value.ColumnId + "_calcdd").select2("val", value.ETLTransformationRuleId);
                            //$('#' + value.Source + "_dd").select2().select2("val", value.Destination);
                        });
                    }
                    SGmappeddata();


                }
            });
        }
    });
}


$('#MapData').hide();
if (queryimpsrc != "" && querydest != "") {
    $('#impsrc').select2("val", queryimpsrc);
    $('#dest').select2("val", querydest);
    $('#MapData').show();
    $("#ProductcatalogLabel").show();
}

$('#toggleDestinationToSourceButton').click(function () {
    window.location = urlDestinationToSourceMapping + "?sourceId=" + importSourceId + "&destId=" + destinationTableId;
});

$('#MapData').click(function () {
    var iMaped = "";
    var dropdownIds = [];
    $('.sourceLabel').each(function () {
        
        var ddId = '#' + $(this).val() + '_dd';
        var dropVal = $(ddId).select2("val");

        if (dropVal !== "")
            iMaped += $(this).val() + "_" + dropVal + ",";
        dropdownIds.push(dropVal);
    });

    var i, j, flag = 0, data;
    for (i = 0; i < dropdownIds.length; ++i) {
        var test = dropdownIds[i];
        for (j = i + 1; j < dropdownIds.length; j++) {
            if (test === dropdownIds[j] && test !== "") {
                flag = 1;                                     // multiple data mapping check
                data = test;
                break;
            }
        }
    }
    if (false) {  //Not required in case of ECI

        var destinationColumnId = 0;
        if(data != null && data !== '')
            destinationColumnId = data.split('_')[0];
        
        $.ajax({
            url: urlDataMappingGetRepeatedDestination,
            data: { destColId: destinationColumnId },
            type: 'POST',
            cache: false,
            success: function (data) {
                //var hostname = window.location.origin
                alertmsg("red", "Multiple mapping of destination column: " + data);
            }
        });
    }
    else {
        var flagRequired = 0;
        var mappedIDs = iMaped.split(",");
        $.ajax({
            url: urlGetRequiredDestinationCols,
            data: {
                impsrcId: queryimpsrc,
                destid: querydest

            },
            type: 'GET',
            cache: false,
            success: function (data) {

                $.each(data, function (index1, value1) {
                    var requiredFieldMapped = 0;
                   
                    $.each(mappedIDs, function (index2, value2) {
                        //var destid = value2.split("_");
                        var numberedId = value2.substr(value2.indexOf('_') + 1, value2.length);
                        //console.log(value1.Id);
                        if (value1.Id === numberedId) { requiredFieldMapped = 1; }
                    });
                    if (requiredFieldMapped === 0) {
                        flagRequired = 1;
                        alertmsg("red", "Destination field required: " + value1.Name);
                    }
                });

                //console.log('Test for required');
                if (flagRequired == 0) {
                    var PCMapped = "";
                    var PCdropdownIds = [];

                    var calcFormulaMapped = [];

                    $('.pcsourceLabel').each(function () {
                        var pcId = '#' + $(this).val() + '_PCdd';
                        var chkId = '#' + $(this).val() + '_Chkdd';

                        

                        if ($(pcId).select2("val") !== "") {
                            
                            PCMapped += $(this).val() + "_" + $(pcId).select2("val") + "_" + +$(chkId).is(':checked') + ",";
                        }

                    });

                    $('.pcsourceLabelForCalc').each(function () {
                        
                       
                        var calcId = '#' + $(this).val() + '_calcdd';
                        

                        if ($(calcId).select2("val") !== "") {
                            calcFormulaMapped += $(this).val() + "_" + $(calcId).select2("val") + ",";
                        }

                    });

                    
                    $.ajax({
                        url: urlProductCatalogAdminMapping,
                        data: {
                            MappedData: PCMapped,
                            importSourceId: importSourceId
                        },
                        type: 'POST',
                        cache: false,
                        success: function (data) {

                            
                                $.ajax({
                                    url: urlTransformationRuleMapping,
                                    data: { mappedData: calcFormulaMapped, importSourceId: queryimpsrc },
                                    type: 'POST',
                                    cache: false,
                                    success: function(data) {

                                     }
                                  });
                         

                            $.ajax({
                                url: urlAdminMapping,
                                data: { queryImpSrc: queryimpsrc, querydest: querydest, dataMapped: iMaped },
                                type: 'POST',
                                cache: false,
                                success: function (data) {
                                    alertmsg('green', "Column mapping succeeded");
                                    //var hostname = window.location.origin
                                    //   window.location.href = data;
                                    $('#vendor-list-link').show();
                                }

                            });




                        }

                    });





                }

            }

        });


    }
});

//$(".sourceDrop").change(function () {
//    var drpdown = this.id;
//    //console.log("dropdown Id:"+drpdown);
//    $.ajax({
//        url: urlDataMappingCheck,
//        data: { DestId: this.value, SourceId: this.id, FileId: 0 },
//        type: 'POST',
//        cache: false,
//        success: function (data) {
//            if (data != "ok") {
//                $('h5.Message').replaceWith("<h5 class='Message'>Message: " + data + "</h5>");
//                $('#' + drpdown).find('option:first').attr('selected', 'selected');
//                $('#' + drpdown).val($('#' + drpdown).find('option:first').val());
//                $("#DataMappingErrorBtn").click();
//                $('#' + drpdown).parent('.sg-select-map').removeClass('selected');
//            }
//        }

//    });
//});

$('#srctodexmap').click(function () {

    var impsrcId = $("#impsrc").val();
    var destid = $("#dest").val();
    if (impsrcId != "" && destid != "" && impsrcId != null && destid != null) {
        $.ajax({
            url: urlMapSourceToDestination,
            data: { impsrcId: impsrcId, destid: destid },
            type: 'POST',
            cache: false,
            success: function (data) {
                window.location.href = data;
            }
        });


    }
});

$('#recommendMapping').click(function() {

    var impsrcId = $("#impsrc").select2("val");
    var destid = $("#dest").select2("val");

    SGmappeddata();

    var queryimpsrc = impsrcId;
    var querydest = destid;

    if (queryimpsrc != "" && querydest != "") {

        $.ajax({
            url: urlCheckSourceDestinationMappingExists,
            data: {
                importSourceId: queryimpsrc,
                destinationTableId: querydest
            },
            type: 'GET',
            cache: false,
            success: function(data) {
                if (data == "NO") {

                    $.ajax({
                        url: urlKeepRecommendedMappingInViewData,
                        type:'GET',
                        cache:false,
                        success:function(data) {
                            if (data == "SUCCESS")
                                location.reload();
                        }

                    });

                } else if (data = "YES") {

                }

            }
        });
    }
});



function similar_text(first, second, percent) {
   
    if (first === null || second === null || typeof first === 'undefined' || typeof second === 'undefined') {
        return 0;
    }

    first += '';
    second += '';

    var pos1 = 0,
      pos2 = 0,
      max = 0,
      firstLength = first.length,
      secondLength = second.length,
      p, q, l, sum;

    max = 0;

    for (p = 0; p < firstLength; p++) {
        for (q = 0; q < secondLength; q++) {
            for (l = 0;
              (p + l < firstLength) && (q + l < secondLength) && (first.charAt(p + l) === second.charAt(q + l)) ; l++)
                ;
            if (l > max) {
                max = l;
                pos1 = p;
                pos2 = q;
            }
        }
    }

    sum = max;

    if (sum) {
        if (pos1 && pos2) {
            sum += this.similar_text(first.substr(0, pos1), second.substr(0, pos2));
        }

        if ((pos1 + max < firstLength) && (pos2 + max < secondLength)) {
            sum += this.similar_text(first.substr(pos1 + max, firstLength - pos1 - max), second.substr(pos2 + max,
              secondLength - pos2 - max));
        }
    }

    if (!percent) {
        return sum;
    } else {
        return (sum * 200) / (firstLength + secondLength);
    }
}


function SGmappeddata() {
    $('.sg-select-map .sourceDrop  ').each(function () {
        var str = "";
        str = $('option:selected', this).text();

        if (str === "--Choose") {
            $(this).parent('.sg-select-map').removeClass("selected");
        }
        else {
            $(this).parent('.sg-select-map').addClass("selected");
        }
    });

    $(".sourceDrop").change(function () {
        var str = "";
        str = $('option:selected', this).text();
        if (str === "--Choose") {
            $(this).parent('.sg-select-map').removeClass("selected");
        }
        else {
            $(this).parent('.sg-select-map').addClass("selected");
        }
    });



    $('.sourceDrop option:contains("sgzB")').addClass('sgzB');
    $('.sourceDrop option:contains("sgzC")').addClass('sgzC');
    $('.sourceDrop option:contains("sgzD")').addClass('sgzD');
    $('.sourceDrop option:contains("sgzDt")').addClass('sgzDt');
    $('.sourceDrop option:contains("sgzF")').addClass('sgzF');
    $('.sourceDrop option:contains("sgzI")').addClass('sgzI');
    $('.sourceDrop option:contains("sgzM")').addClass('sgzM');
    $('.sourceDrop option:contains("sgzV")').addClass('sgzV');
    $('.sourceDrop option:contains("sgzO")').addClass('sgzO');

};

$(function() {
    $('#vendor-list-link').hide();
});
