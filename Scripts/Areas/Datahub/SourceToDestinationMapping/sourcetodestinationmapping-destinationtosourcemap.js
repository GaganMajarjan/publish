﻿

$('#saveSourceDestinationMappingbtn').click(function () {
    var iMaped = "";
    $('.sourceLabel').each(function () {
        var ddId = '#' + $(this).val();
        var dropVal = $(ddId).select2("val");

        if (dropVal != "" && $(this).val() != "")
            iMaped += dropVal + "_" + $(this).val() + ",";
    });
    SGloading('Mapping..');
    $.ajax({
        url: urlDataMappingRelations,
        data: { FileId: staging, dataMapped: iMaped },
        type: 'POST',
        cache: false,
        success: function (data) {
            //var hostname = window.location.origin
            /// window.location.href = data;
            $("label.messagebatch").replaceWith("<label class='messagebatch'>" + data + "</label>");

            SGloadingRemove();
            $("#btnMappingCompleteModelOpen").click();

        }
    });

});


$('#toggleSourceToDestinationButton').click(function () {
    window.location = urlSourceToDestinationMapping + "?impSrcId=" + importSourceId + "&destId=" + destinationTableId;
});



$(document).ajaxComplete(function () {
    SGmappeddata();
});

$('document').ready(function () {
    //$('.sg-staging-grid > div').height($(window).height() - 160);
    //SGmappeddata();
    //SGAfterdata();
    SGloading('Loading..');

    $("select").select2();
    

    $.ajax({
        url: urlAdminMappingForDestination,
        data: {
            importSourceId: importSourceId,
            masterDestinationId: destinationTableId
        },
        type: 'GET',
        cache: false,
        success: function (data) {
            SGloadingRemove();
            console.log(data);
            if (data !== "NoData") {
                $.each(data, function (index, value) {

                  $("#" + value.Destination + "_dd").select2("val", value.Source);

                });

            }

            $.ajax({
                url: urlPCatalogAdminAutoMapping,
                data: {
                    srcid: importSourceId
                },
                type: 'GET',
                cache: false,
                success: function (data) {
                    $.each(data, function (index, value) {

                        $('#' + value.ETLImportSourceColumnId + "_PCdd").select2("val", value.ETLProductMatchAgainstTableColumnID);
                        $('#' + value.ETLImportSourceColumnId + "_Chkdd").prop('checked', value.IsMatchFullyRecorded);


                    });
                    SGmappeddata();

                }
            });

            $.ajax({
                url: urlTransformationRuleAutoMapping,
                data: {
                    importSourceId: importSourceId
                },
                type: 'GET',
                cache: false,
                success: function (data) {
                    if (data !== "EMPTY") {

                        $.each(data, function (index, value) {
                            $('#' + value.ColumnId + "_calcdd").select2("val", value.ETLTransformationRuleId);
                            //$('#' + value.Source + "_dd").select2().select2("val", value.Destination);
                        });
                    }
                    SGmappeddata();


                }
            });


        }
    });
    $('#map').height($(window).height() - 125);


    $('#NexttoDataSync').click(function () {
        var iMaped = "";
        var dropdownIds = [];
        $('.sourceLabel').each(function () {
            var ddId = '#' + $(this).val();

            var dropVal = $(ddId).select2("val");

            if (dropVal != "" && $(this).val() != "")
                iMaped += dropVal + "_" + $(this).val() + ",";
            dropdownIds.push(dropVal);
        });
        var i = 0, j = 0, flag = 0;
        var data = "";
        for (i = 0; i < dropdownIds.length; ++i) {
            var test = dropdownIds[i];
            for (j = i + 1; j < dropdownIds.length; j++) {
                if (test == dropdownIds[j] && test != "") {
                    flag = 1;
                    data = test;
                    break;
                }
            }
        }
        if (false) {
            $.ajax({
                url: urlDataMappingGetRepeatedSource,
                data: { srcColId: data },
                type: 'POST',
                cache: false,
                success: function (data) {
                    //var hostname = window.location.origin
                    alertmsg("red", "Multiple mapping of source column: " + data);
                }
            });
        }
        else {
            var flagRequired = 1;
            var mappedIDs = iMaped.split(",");
            $.ajax({
                url: urlGetRequiredDestinationCols,
                data: {
                    LogUserUploadId: staging,
                },
                type: 'GET',
                cache: false,
                success: function (data) {
                    $.each(data, function (index1, value1) {
                        var requiredFieldMapped = 0;
                        $.each(mappedIDs, function (index2, value2) {
                            var destid = value2.split("_");
                            if (destid[0] != "") {

                                if (value1.Id == destid[1]) { requiredFieldMapped = 1; }
                            }
                        });
                        if (requiredFieldMapped == 0) {
                            flagRequired = 0;
                            alertmsg("red", "Destination field required: " + value1.Name);
                        }

                    });

                    if (flagRequired == 1) {
                        $.ajax({
                            url: urlCheckMappingChange,
                            data: { fileId: staging, dataMapped: iMaped },
                            type: 'GET',
                            cache: false,
                            success: function (data123) {
                                if (data123 == "change") {
                                    $("#saveMappingModelbtn").click();


                                }
                                else if (data123 != "change") {
                                    SGloading('Mapping..');
                                    $.ajax({
                                        url: urlDataMappingRelations,
                                        data: { FileId: staging, dataMapped: iMaped },
                                        type: 'POST',
                                        cache: false,
                                        success: function (data) {
                                            $("label.messagebatch").replaceWith("<label class='messagebatch'>" + data + "</label>");
                                            SGloadingRemove();
                                            $("#btnMappingCompleteModelOpen").click();

                                        }
                                    });
                                }
                            }
                        });
                    }

                }
            });

        }
    });


    $(".layout .message").fadeIn().delay(2000).fadeOut();
});

$('#viewDataSourcebtn').click(function () {

    window.open(urlViewDataSourceFile + "?FileId=" + fileId);
});