﻿$(document).ready(function () {
    initializeBrandDataTable();
});

function initializeBrandDataTable() {
    $('#PIMBrandDataTable').dataTableWithFilter({
        "destroy": true,
        "bJQueryUI": true,
        "bServerSide": true,
        "bFilter": true,
        "scrollX": true,
        "bSort": false,
        "bScrollCollapse": true,

        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": tableToolsPath,
            "aButtons": tableToolsOptions
        },
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlListPIMBrand,
        "sServerMethod": "GET",

        // Initialize our custom filtering buttons and the container that the inputs live in
        filterOptions: { searchButton: "ProductCatalogSearchText", clearSearchButton: "ProductCatalogClearSearch", searchContainer: "ProductCatalogSearchContainer" },
        "fnPreDrawCallback": function () {
            // gather info to compose a message
            var ob = this;
            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
            //SGtableloading('fa-plane', '', this);


        },
        "fnDrawCallback": function (settings) {
            // in case your overlay needs to be put away automatically you can put it here
            var divid = this.closest('.dataTables_wrapper').attr('id');
            SGtableloadingRemove(divid);
            rebuilttooltip();
        }
    });
}

$('#btnAddPIMBrand').click(function () {

    window.location.href = urlCreateBrand;
})

$('#btnUpdatePIMBrand').click(function () {
    var i = 0;
    var selectedId = 0;
    $('input[type=checkbox][class=PIMBrandSelector]').each(function () {
        if (this.checked) {
            i++;
            selectedId = this.value;
        }
    });
    if (i === 0) {
        udh.notify.error('Please select Brand to edit');
        e.preventDefault();
    } else if (i > 1) {
        udh.notify.error('Please select single Brand to edit');
        e.preventDefault();
    } else {
        window.location.href = urlUpdateBrand + "?Id=" + selectedId;
    }

})

var eachIds = [];
$('#btnDisablePIMBrand').click(function () {

    eachIds = [];
    var selector = 'input[type=checkbox][class=PIMBrandSelector]';
    $(selector)
        .each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });

    if (eachIds.length == 0) {
        alertmsg('Please select at least one brand')
        return;
    }

    $('#DisablePIMBrandModal').modal('show');

})


$('#btnDisablePIMBrandModalYes').click(function () {

    $.ajax({
        url: urlDisableBrand,
        data: { 'idList': eachIds },
        type: 'POST',
        beforeSend: function () {
            SGloading("Loading");
        },
        success: function (data) {
            if (data && data.Status === true) {
                udh.notify.success(data.Message);
                $('#PIMBrandDataTable').dataTable().fnReloadAjax();
                if ($('#selectAllCheckBoxPIMBrand').prop("checked") == true) {
                    $("#selectAllCheckBoxPIMBrand").prop("checked", false);
                }
            } else {
                udh.notify.error(data.Message);
            }

        },
        complete: function () {
            eachIds = [];
            SGloadingRemove();
            $('#btnDisablePIMBrandModalNo').click();

        }
    });
})


$('#btnEnablePIMBrand').click(function () {

    eachIds = [];
    var selector = 'input[type=checkbox][class=PIMBrandSelector]';
    $(selector)
        .each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });

    if (eachIds.length == 0) {
        alertmsg('Please select at least one brand')
        return;
    }

    $('#EnablePIMBrandModal').modal('show');

})

$('#btnEnablePIMBrandModalYes').click(function () {

    $.ajax({
        url: urlEnableBrand,
        data: { 'idList': eachIds },
        type: 'POST',
        beforeSend: function () {
            SGloading("Loading");
        },
        success: function (data) {
            if (data && data.Status === true) {
                udh.notify.success(data.Message);
            } else {
                udh.notify.error(data.Message);
            }

        },
        complete: function () {
            eachIds = [];
            SGloadingRemove();
            $('#btnEnablePIMBrandModalNo').click();
            $('#PIMBrandDataTable').dataTable().fnReloadAjax();
            if ($('#selectAllCheckBoxPIMBrand').prop("checked") == true) {
                $("#selectAllCheckBoxPIMBrand").prop("checked", false);
            }
        }
    });
})


