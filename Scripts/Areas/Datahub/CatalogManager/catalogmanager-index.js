﻿$(document).ready(function () {
    //hide productId property FROM PIMVendor product search
    // vendor product depending on the product catalog id
    //setHeightWrap(); 

    catalogManagerSearch = true;
    $("#selectedProductCatalog").hide();
    $("#primaryVendorId").hide();

    $("#ProductCatalogDisabled").val("false");
    $("#VendorProductDisabled").val("false");



    var vendorProductCatalogTable;
    function initializeProductCatalogDataTable() {
        $('#ProductCatalogDataTable').dataTableWithFilter({
            "destroy": true,
            "bJQueryUI": true,
            "bServerSide": true,
            "bFilter": false,
            "scrollX": true,
            "bSort": false,
            "bScrollCollapse": true,

            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": tableToolsPath,
                "aButtons": tableToolsOptions
            },
            "aLengthMenu": dataTableMenuLength,
            "iDisplayLength": dataTableDisplayLength,
            "sAjaxSource": urlProductCatalogManager,
            "sServerMethod": "POST",

            // Initialize our custom filtering buttons and the container that the inputs live in
            filterOptions: { searchButton: "ProductCatalogSearchText", clearSearchButton: "ProductCatalogClearSearch", searchContainer: "ProductCatalogSearchContainer" },
            "fnPreDrawCallback": function () {
                // gather info to compose a message
                var ob = this;
                SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                //SGtableloading('fa-plane', '', this);


            },
            "fnDrawCallback": function (settings) {
                // in case your overlay needs to be put away automatically you can put it here
                var divid = this.closest('.dataTables_wrapper').attr('id');
                SGtableloadingRemove(divid);
                rebuilttooltip();
            }
        });
    }

    function initializeVendorProductCatalogDataTable() {
        vendorProductCatalogTable = $('#VendorProductDataTable').dataTableWithFilter({
            "destroy": true,
            "bServerSide": true,
            "bFilter": false,
            "scrollX": true,
            "bSort": false,
            "sScrollXInner": "110%",
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": tableToolsPath,
                "aButtons": tableToolsOptions
            },
            "aLengthMenu": dataTableMenuLength,
            "iDisplayLength": dataTableDisplayLength,
            "sAjaxSource": urlVendorProductManager,
            "sServerMethod": "POST",
            // Initialize our custom filtering buttons and the container that the inputs live in
            filterOptions: { searchButton: "VendorProductSearch", clearSearchButton: "VendorProductClearSearch", searchContainer: "VendorProductSearchContainer" },
            "fnPreDrawCallback": function () {
                // gather info to compose a message
                var ob = this;
                SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                //SGtableloading('fa-plane', '', this);
            },
            "fnDrawCallback": function () {
                // in case your overlay needs to be put away automatically you can put it here
                var divid = this.closest('.dataTables_wrapper').attr('id');
                SGtableloadingRemove(divid);
                rebuilttooltip();
            }
        });
    }


    $('.filter').click(function () {
        $(this).hide();
        $(this).siblings('.filterhide').show();
        $(this).parents('.panel').find('.panel-filter').show();
    });

    $('.filterhide').click(function () {
        $(this).hide();
        $(this).siblings('.filter').show();
        $(this).parents('.panel').find('.panel-filter').hide();

    });


    //expandview2();

    // Manual clear vendor product data

    $('#ProductCatalogSearchText').click(function () {
        document.getElementById('searchVendorProduct').value = "";
        document.getElementById('VendorProductDisabled').value = "";
        document.getElementById('selectedProductCatalog').value = "";
        document.getElementById('primaryVendorId').value = "";


        $('#VendorProductSearch').click();
    });
    $('#VendorProductManualClearSearch').click(function () {
        document.getElementById('searchVendorProduct').value = "";
        document.getElementById('VendorProductDisabled').value = "";
        $('#VendorProductSearch').click();
    });

    $('#selectAllProductCatalog').click(function () {
        var thisCheck = $(this);
        if (thisCheck.is(':checked')) {
            $('.ProductCatalogSelector').prop("checked", true);
        } else {
            $('.ProductCatalogSelector').prop("checked", false);
        }
    });

    $('#selectAllVendorProduct').click(function () {
        var thisCheck = $(this);
        if (thisCheck.is(':checked')) {
            $('.VendorProductSelector').prop("checked", true);
        } else {
            $('.VendorProductSelector').prop("checked", false);
        }
    });

    initializeProductCatalogDataTable();
    //$('#ProductCatalogSearchText').trigger('click');



    setTimeout(customJqueryUIpara('.sg-catalog-one'), 500);


    initializeVendorProductCatalogDataTable();
    setTimeout(customJqueryUIpara('.sg-catalog-two'), 500);

    $('#VPCGetPrimary').click(function (e) {
        var i = 0;
        var ProductCatalogId = 0;
        $('input[type=checkbox][class=ProductCatalogSelector]').each(function () {

            if (this.checked) {
                i++;
                ProductCatalogId = this.value;
            }
        });

        if (i === 0) {
            alertmsg('red', Alert_productCatalogForPrimaryVendorProduct);
            e.preventDefault();
        }
        else if (i > 1) {
            alertmsg('red', Alert_singleProductCatalogForPrimaryVendorProduct);
            e.preventDefault();
        } else {

            document.getElementById('selectedProductCatalog').value = ProductCatalogId;

            $.ajax({
                url: urlPrimaryVendorProduct,
                data: {
                    'productId': ProductCatalogId
                },
                type: 'POST',
                cache: false,
                success: function (data) {
                    if (data === "") {
                        alertmsg('yellow', Alert_NoPrimaryVendorProduct);
                    } else {
                        document.getElementById('primaryVendorId').value = data;
                        $('#VendorProductSearch').click();
                    }
                }
            });
        }
    });

    $('#btnLoadProductcatalog').click(function (e) {
        $("#ProductCatalogSearchText").click();


    });
    //$('#ProductCatalogSearchText, #VendorProductSearch').click(function () {
    //    $(this).parents('.panel').find('.filterhide').hide();
    //    $(this).parents('.panel-filter').hide();
    //    $(this).parents('.panel').find('.filter').show();

    //});
    $('#VPCSetPrimary').click(function (e) {
        var i = 0;
        var vendorProductId = 0;
        $('input[type=checkbox][class=VendorProductSelector]').each(function () {

            if (this.checked) {
                i++;
                vendorProductId = this.value;
            }
        });

        if (i === 0) { alertmsg('red', Alert_VendorProductSetPrimary); e.preventDefault(); }
        else if (i > 1) {
            alertmsg('red', Alert_singleVendorProductSetPrimary);
            e.preventDefault();
        } else {
            var productId = $("#selectedProductCatalog").val();
            $.ajax({
                url: urlVendorProductSetPrimary,
                data: {
                    vendorProductId: vendorProductId,
                    productId: productId
                },
                type: 'POST',
                cache: false,
                beforeSend: function () {
                    SGloading('Loading');
                },
                success: function (response) {

                    if (response) {
                        if (response.Status === true) {
                            alertmsg('green', response.Message);

                            //if it contains more than one vendorproductcatalog data, suggest user to change cost
                            if ($('#VendorProductDataTable tbody tr').length > 1) {
                                //alert('This product has other vendor products too. Please adjust the cost of product.');
                            };

                            vendorProductCatalogTable.fnReloadAjax();
                        } else {
                            alertmsg('red', response.Message);
                        }
                    } else {
                        alertmsg('red', Alert_ErrorOccurred);
                    }
                },
                complete: function () {
                    SGloadingRemove();
                }
            });
        }
    });

    $('#btnGetVendorProductcatalog').click(function (e) {
        loadVPCData(e);
    });
    $('body').on('click', '#ProductCatalogDataTable tr', function (e) {

        if (e.target.tagName.toLowerCase() !== 'a') {
            loadVPCData(e);
        }

    });
    /*$('#ProductCatalogDataTable').on("change", ".ProductCatalogSelector", function (event) {
        if (this.checked) {
            loadVPCData(event);
        }

    }); */
    //$("#ProductCatalogDataTable").change(function (e) {
    //    var checkedRow = $(this).find('td:first-child input[type="checkbox"]');
    //    if (checkedRow.is(':checked')) {
    //        loadVPCData(e);
    //    }
    //});

    function loadVPCData(e) {
        var i = 0;
        var productCatalogId = 0;
        $('input[type=checkbox][class=ProductCatalogSelector]').each(function () {

            if (this.checked) {
                i++;
                productCatalogId = this.value;
            }
        });

        //console.log(productCatalogId);
        //if (i === 0) { alertmsg('red', Alert_ProductCatalogToLoadVendorProduct); e.preventDefault(); }
        //else if (i > 1) {
        //    alertmsg('red', Alert_singleProductCatalogToLoadVendorProduct);
        //    e.preventDefault();
        //}
        //else 
        {
            document.getElementById('selectedProductCatalog').value = productCatalogId;
            document.getElementById('primaryVendorId').value = "";
            $('#VendorProductSearch').click();
            //vendorproductTable.fnReloadAjax();
        }
    }

    $('#btnEditProductcatalog').click(function (e) {
        var i = 0;
        var ProductCatalogId = 0;
        $('input[type=checkbox][class=ProductCatalogSelector]').each(function () {

            if (this.checked) {
                i++;
                ProductCatalogId = this.value;
            }
        });

        if (i === 0) { alertmsg('red', Alert_ProductCatalogEdit); e.preventDefault(); }
        else if (i > 1) {
            alertmsg('red', Alert_singleProductCatalogToEdit);
            e.preventDefault();
        } else {
            window.location.href = urlProductCatalogEdit + '?id=' + ProductCatalogId + "&returnUrl=" + urlProductCatalogEdit + '?id=' + ProductCatalogId;
            /*
            // Ajax call
            $.ajax({
                type: 'POST',
                url: urlGetVendorProductCatalogCount,
                cache: false,
                //contentType: 'application/json; charset=utf-8',
                data: { productCatalogId: ProductCatalogId },
                dataType: 'json',
                beforeSend: function () {
                    //$("#btnsave").attr('disabled', true);
                    SGloading('Loading');
                },
                success: function (response) {
                    if (response > 0) {
                        window.location.href = urlProductCatalogEdit + '?id=' + ProductCatalogId + "&returnUrl=" + urlReturn;
                    } else {
                        alertmsg('red', Alert_NoPrimaryVendorProduct);
                    }
                },
                complete: function () {
                    //$("#btnsave").attr('disabled', false);
                    SGloadingRemove();
                },
                error: function (xhr, status, error) {
                    //reset variables here
                    alert(status + ' ' + error);
                }
            });
            */
        }
    });

    $('#btnEditVendorProductcatalog').click(function (e) {
        var i = 0;
        var vendorProductId = 0;
        $('input[type=checkbox][class=VendorProductSelector]').each(function () {

            if (this.checked) {
                i++;
                vendorProductId = this.value;
            }
        });

        if (i === 0) { alertmsg('red', Alert_VendorProductToEdit); e.preventDefault(); }
        else if (i > 1) {
            alertmsg('red', Alert_singleVendorProductToEdit);
            e.preventDefault();
        } else {
            window.location.href = urlVendorProductCatalogEdit + '?id=' + vendorProductId + "&returnUrl=" + urlReturn;

        }
    });

    $('#btnAddProductcatalog').click(function (e) {

        window.location.href = urlProductCatalogCreate;// + "?returnUrl=" + urlReturn;
    });

    $('#btnAddVendorProductcatalog').click(function (e) {
        var i = 0;
        var ProductCatalogId = 0;
        $('input[type=checkbox][class=ProductCatalogSelector]').each(function () {
            if (this.checked) {
                i++;
                ProductCatalogId = this.value;
            }
        });

        if (i === 0) { alertmsg('red', Alert_ProductCatalogToCreateVendorProduct); e.preventDefault(); }
        else if (i > 1) {
            alertmsg('red', Alert_singleProductCatalogToCreateVendorProduct);
            e.preventDefault();
        } else {

            window.location.href = urlVendorProductCreate + "?returnUrl=" + urlReturn + "&productId=" + ProductCatalogId;
            //$.ajax({
            //    url: urlVendorProductAdd,
            //    data: {
            //        productId: ProductCatalogId
            //    },
            //    type: 'POST',
            //    cache: false,
            //    success: function (data) {
            //        window.location.href = urlVendorProductCreate + "?returnUrl=" + urlReturn +"&productId=" + ProductCatalogId;
            //    }
            //});

        }
    });




    $('.expand-cat-one').click(function () {

        $('.expand-btn').hide();
        $('.collapse').show();
        $('.sg-catalog-one').show();
        $('.sg-catalog-one').removeClass('col-md-6').addClass('col-md-12');
        $('.sg-catalog-two').hide();
        initializeProductCatalogDataTable();
        setTimeout(customJqueryUIpara('.sg-catalog-one'), 500);
        TblpgntBtm();
    });

    $('.expand-cat-two').click(function () {


        $('.expand-btn').hide();
        $('.collapse').show();
        $('.sg-catalog-two').show();
        $('.sg-catalog-two').removeClass('col-md-6').addClass('col-md-12');
        $('.sg-catalog-one').hide();
        //initializeVendorProductCatalogDataTable();
        //setTimeout(customJqueryUIpara('.sg-catalog-two'), 500);
        //TblpgntBtm();
    });

    $('.collapse').click(function () {


        $('.collapse').hide();
        $('.expand-btn').show();
        $('.sg-catalog-one, .sg-catalog-two').show();
        $('.sg-catalog-one').removeClass('col-md-12').addClass('col-md-6');
        $('.sg-catalog-two').removeClass('col-md-12').addClass('col-md-6');
        initializeProductCatalogDataTable();
        setTimeout(customJqueryUIpara('.sg-catalog-one'), 500);
        //initializeVendorProductCatalogDataTable();
        //setTimeout(customJqueryUIpara('.sg-catalog-two'), 500);

        TblpgntBtm();
    });


});