﻿var stepped = 0, chunks = 0, rows = 0;
var start, end;
var parser;
var pauseChecked = false;
var printStepChecked = false;
var header = [];
var numberOfColumns = 0;
var clientsideData;
var filetypeFlag = false;
var dataFrom = 0; /// product or vendor
var dataId = 0;  // productids or vendor id
var changeLevel = "";
var changeText;
var changeLevelArray = new Array();
var changeLevelArrayPerLot = new Array();
//["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""];
var loadingRemove = 1;
var loadingMessages = ["Loading", "Loading", "Loading"];
var pricingLevelColumns = 0;
var container = document.getElementById('bulk-pricing'),
  hot;
var flag = 0;
var dataSchema;
var productIdList = getQueryStringByName("productIdList");

var searchField = document.getElementById('search_field');

var originalData = [];
var errorCoordinates = [];
var errorRows = [];
var container = document.getElementById('bulk-pricing');

//SG-800
var byPassObjects = {};
byPassObjects.byPassSpqQplDonotMatch = false;
byPassObjects.byPassSpqGreaterThanOneMatch = false;
byPassObjects.byPassMinOrderLineAmountMatch = false;




//var byPassSpqQplDonotMatch = false;
//var byPassSpqGreaterThanOneMatch = false;
//var byPassMinOrderLineAmountMatch = false;
var responseData = "";

var numberOfStartColumnsToExcludeValidation = staticColumnsCount != null ? staticColumnsCount : 20;
numberOfStartColumnsToExcludeValidation = 20;
numberOfLastColumnsToExcludeValidation = 20;

//$('#Vendor').select2();

$('#Vendor').select2(remoteDataConfig(urlGetVendorListForSelect2, 3, 250, pleaseSelectMessage));

$(function () {

    $('#submit-unparse').click(function () {
        var input = $('#input').val();
        var delim = $('#delimiter').val();

        var results = Papa.unparse(input, {
            delimiter: delim
        });

        //console.log("Unparse complete!");
        //console.log("--------------------------------------");
        //console.log(results);
        //console.log("--------------------------------------");
    });

    $('#insert-tab').click(function () {
        $('#delimiter').val('\t');
    });
});

$('#btn-save').click(function () {
    if (!(hot && hot.getSettings().data.length > 0)) {
        alertmsg('red', 'Please load data to save',-1);
        return;
    }
    var data = JSON.stringify(hot.getSettings().data);

    var vendorId = $('#Vendor').val();

    if (orderByChangeFlag === true) {
        $('#SaveConfirmationDialogue').modal('show');
    }
    else {
        // Ajax call
          $.ajax({
            type: 'POST',
            url: createUrl,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'bulkPricingJsonString': data, 'vendorId': vendorId, 'byPassObject': byPassObjects },
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
                //tooltip clear
                markClearRow();
            },
            success: function (response) {
                //console.log(response);
                if (response != null && response.Status === true) {
                    alertmsg('green', response.Message, -1);
                    isSaved = true;
                    byPassObjects.byPassSpqQplDonotMatch = false;
                    byPassObjects.byPassSpqGreaterThanOneMatch = false;
                    byPassObjects.byPassMinOrderLineAmountMatch = false;

                } else if (response != null && response.Status === false) {
                    try {

                        if (response.Message.indexOf('Standard Package Quantity and Quantity Per Lot do not match') >= 0) {
                            responseData = response.Data;
                            $('#SpqQplDoNotMatchModal').modal('show');
                            SGloadingRemove();
                            return;
                        }

                        else if (response.Message.indexOf('Vendor Product is \'Order By Piece\' but the Feed Standard Package Quantity') >= 0) {
                            responseData = response.Data;
                            $('#SpqNotMathcedForOrderByPieceModal').modal('show');


                            //document.getElementById('SpqNotMathcedForOrderByPieceModal').childNodes[1].childNodes[1].childNodes[3].innerHTML = '';

                            //// document.getElementById('VendorProductNotMatchedLineOrderAmountModal').childNodes[1].childNodes[1].childNodes[3].append(document.createElement("h1"))

                            //var h = document.createElement("H5");
                            //var t = document.createTextNode('Vendor Product is  \'Order by Piece\'  but the Feed Standard Package Quantity is ' + spq);
                            //h.appendChild(t);
                            //var br = document.createElement("br")
                            //h.appendChild(br);
                            //var br1 = document.createElement("br")
                            //h.appendChild(br1);
                            //var t = document.createTextNode(' Do you still want to continue?');
                            //h.appendChild(t);

                            //document.getElementById('SpqNotMathcedForOrderByPieceModal').childNodes[1].childNodes[1].childNodes[3].append(h);
                            SGloadingRemove();
                            return;
                        }

                        else if (response.Message.indexOf('MinOrderLineAmount Condition Unsatisfied') >= 0) {
                            responseData = response.Data;
                            $('#VendorProductNotMatchedLineOrderAmountModal').modal('show');
                            SGloadingRemove();
                            return;
                        }
                        else {
                            var errorRowNumber = response.Message.split('.')[0];
                            markStatus(errorRowNumber, response.Message);

                            byPassObjects.byPassSpqQplDonotMatch = false;
                            byPassObjects.byPassSpqGreaterThanOneMatch = false;
                            byPassObjects.byPassMinOrderLineAmountMatch = false;

                            alertmsg('red', response.Message, -1);


                        }
                    }
                    catch (err) {

                    }

                } else {
                    byPassObjects.byPassSpqQplDonotMatch = false;
                    byPassObjects.byPassSpqGreaterThanOneMatch = false;
                    byPassObjects.byPassMinOrderLineAmountMatch = false;
                    alertmsg('red', msgErrorOccured, -1);
                }
                SGloadingRemove();
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
               
            },
            error: function (xhr, status, error) {
                //reset variables here
                byPassObjects.byPassSpqQplDonotMatch = false;
                byPassObjects.byPassSpqGreaterThanOneMatch = false;
                byPassObjects.byPassMinOrderLineAmountMatch = false;

                SGloadingRemove();
                alert(status + ' ' + error);
            }
        });
        }

});

$('#btn-settings').hide();

$('#btn-settings').click(function () {
    $('#settings-well').toggle();
});

$('#chk-autosave').click(function () {
    $('#btn-save').toggle();
});

$('#clear-bulk-pricing').click(function () {
    $('#confirm-clear-bulk-pricing').modal('show');
});

$('#confirm-clear-bulk-pricing-yes').click(function () {
    $('#confirm-clear-bulk-pricing').modal('hide');
    //remove all rows
    hot.alter('remove_row', 0, hot.getSettings().data.length);
    hot.alter('insert_row');
    $('#Vendor').select2('val', '');
});


/**
 * Export to CSV button
 */

$("#exportBulkPricingAll").click(function () {
    
    //if (dataFrom !== 0 && dataId !== 0) {

    if (hot && hot.getSettings().data.length > 0) {
        urlexportBulkData = urlexportBulkData + "?type=" + dataFrom + "&value=" + dataId;
        window.location = urlexportBulkData;
    } else {
        alertmsg("red", "No data to export",-1);
    }
    //}

});



$("#exportBulkPricing").click(function () {

    handsontable2EqualsToFormat.download(hot, "bulk-pricing.csv", "csv", true);
});
var exportCsv = $("#export-csv")[0];
if (exportCsv) {
    Handsontable.dom.addEvent(exportCsv, "mouseup", function (e) {
        ////var colHeaders = $("#bulk-pricing").handsontable("getColHeader");
        //var colHeaders = hot.getSettings().columns;
        //console.log(colHeaders);
        //console.log(colHeaders.title);
        //exportCsv.blur(); // jquery ui hackfix
        //var csvContent = "data:text/csv;charset=utf-8,";
        //csvContent = parseRow(colHeaders, 0, csvContent); // comment this out to remove column headers
        //$.each(hot.getSettings().data, function (infoArray, index) {
        //    csvContent = parseRow(infoArray, index, csvContent);
        //});
        //var encodedUri = encodeURI(csvContent);
        //var link = document.createElement("a");
        //link.setAttribute("href", encodedUri);
        //link.setAttribute("download", "bulk-pricing.csv");
        //link.click();

        var exportFormat = $("#exportFormat").val();
        if (exportFormat == null || exportFormat === "")
            exportFormat = "csv";

        handsontable2csv.download(hot, "product-feed." + exportFormat);

    });
}

//if productIdList is present, hide vendor
var productIdListQueryString = getQueryStringByName('productIdList');
if ($.trim(productIdListQueryString).length > 0) {
    $('#vendor-dropdown').hide();
}

//Get if margin or amount is checked
function isAmountChecked() {
    var checkedRadio = $('input[name=group1]');
    var checked = checkedRadio.filter(':checked')[0];
    return (checked.id === 'chk-showamount');
}

$('#chk-showamount').change(function (val) {
    toggleAmountDiscount();
    setTimeout(KitCostDisabled, 500);
});

$('#chk-showmargin').change(function (val) {
    toggleAmountDiscount();
    setTimeout(KitCostDisabled, 500);
});

Handsontable.dom.addEvent(searchField, 'keyup', function (event) {
    var queryResult = hot.search.query(this.value);
    //console.log(queryResult);
    hot.render();
});


if (!isEmptyOrSpaces(CatalogSearchAllProducts)) {
    dataFrom = 1; /// product 
    dataId = CatalogSearchAllProducts;
    //   getPaginatedGrid(dataFrom, dataId);
    var totalNumber = CatalogSearchAllProducts.split(",").length;

    getData(dataFrom, dataId, 1, totalNumber);
}
if (productIdList !== "") {
    dataFrom = 1; /// product 
    dataId = productIdList;
    //   getPaginatedGrid(dataFrom, dataId);
    var totalNumber = productIdList.split(",").length;

    getData(dataFrom, dataId, 1, totalNumber);
}

$('#Vendor').change(function (value) {

    var vendorId = $("#Vendor").val();

    //reset amount/margin selection to amount
    $('#chk-showmargin').prop('checked', true);
    dataFrom = 2; /// vendor 
    dataId = vendorId;
    getPaginatedGrid(dataFrom, dataId);

});

function getPaginatedGrid(type, value) {
    $("#pagination").show();
    var reqType = type;
    var reqValue = value;
    //  var pagesize = $("#pageSize").val();
    $.ajax({
        type: 'POST',
        url: urlgetBulkPricingPagination,

        data: {
            'type': reqType,
            'value': reqValue,
            'pageSize': 20

        },

        beforeSend: function () {

            SGloading('Loading');
        },
        success: function (response) {

            getData(type, value, 1, 20);
            $("#pagination").pagination({
                items: response,
                itemsOnPage: 20,
                cssStyle: 'light-theme',
                onPageClick: function (e) {
                    getData(type, value, e, 20);
                }
            });

        },
        complete: function () {

            SGloadingRemove();

            //reset amount/margin selection to amount
            $('#chk-showmargin').prop('checked', true);

        },
        error: function (xhr, status, error) {

            alert(status + ' ' + error);
        }
    });

};

$(function () {
    TblpgntBtm1();
});


$('#btnYesSpqQplDoNotMatchModal').click(function () {
    $('#SpqQplDoNotMatchModal').modal('hide');
    //byPassSpqQplDonotMatch = true;
    //byPassMinOrderLineAmountMatch = true;
    responseData.byPassSpqQplDonotMatch = "true";
    byPassObjects = responseData === "" ? byPassObjects : responseData;
    $('#btn-save').click();
   
})


$('#btnYesSpqNotMathcedForOrderByPieceModal').click(function () {
    $('#SpqNotMathcedForOrderByPieceModal').modal('hide');
    responseData.byPassSpqGreaterThanOneMatch = "true";
    byPassObjects = responseData === "" ? byPassObjects : responseData;
    $('#btn-save').click();

})

$('#btnYesVendorProductNotMatchedLineOrderAmountModal').click(function () {

    $('#VendorProductNotMatchedLineOrderAmountModal').modal('hide');
    responseData.byPassMinOrderLineAmountMatch = "true";
    byPassObjects = responseData === "" ? byPassObjects : responseData;

    $('#btn-save').click();

})


$('#btnSaveConfirmationDialogueYes').click(function () {
    $('#SaveConfirmationDialogue').modal('hide');
    orderByChangeFlag = false;
    $('#btn-save').click();
   
})


