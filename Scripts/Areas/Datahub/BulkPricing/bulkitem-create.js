﻿var stepped = 0, chunks = 0, rows = 0;
var start, end;
var parser;
var pauseChecked = false;
var printStepChecked = false;
var header = [];
var numberOfColumns = 0;
var clientsideData;
var filetypeFlag = false;
var dataFrom = 0; /// product or vendor
var dataId = 0;  // productids or vendor id
var changeLevel = "";
var changeText;
var changeLevelArray = new Array();
var changeLevelArrayPerLot = new Array();
    //["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""];
var loadingRemove = 1;
var loadingMessages = ["Loading", "Loading", "Loading"];
var pricingLevelColumns = 0;
var container = document.getElementById('bulk-pricing'),
  hot;
var flag = 0;
var dataSchema;
var productIdList = getQueryStringByName("productIdList");

var searchField = document.getElementById('search_field');

var originalData = [];
var errorCoordinates = [];
var errorRows = [];
var container = document.getElementById('bulk-pricing');

var numberOfStartColumnsToExcludeValidation = staticColumnsCount != null ? staticColumnsCount : 20;
numberOfStartColumnsToExcludeValidation = 20;
numberOfLastColumnsToExcludeValidation = 20;

//$('#Vendor').select2();

$('#Vendor').select2(remoteDataConfig(urlGetVendorListForSelect2, 3, 250, pleaseSelectMessage));


$(function () {
   
    $('#insert-tab').click(function () {
        $('#delimiter').val('\t');
    });
});

$('#btn-save').click(function () {
    if (hot.getSettings().data.length == 0) {
        alertmsg('red', 'Please load data to save', -1);
        return;
    }
    var data = JSON.stringify(hot.getSettings().data);

    data = decodeHtml(data);

    var vendorId = $('#Vendor').val();
    // Ajax call
    $.ajax({
        type: 'POST',
        url: createUrl,
        cache: false,
        //contentType: 'application/json; charset=utf-8',
        data: { 'bulkPricingJsonString': data, 'vendorId': vendorId },
        dataType: 'json',
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Loading');
            //tooltip clear
            markClearRow();
        },
        success: function (response) {
            //console.log(response);
            if (response != null && response.Status === true) {
                alertmsg('green', response.Message, -1);
                isSaved = true;
            } else if (response != null && response.Status === false) {
                try {
                    var errorRowNumber = response.Message.split('.')[0];
                    markStatus(errorRowNumber, response.Message);
                }
                catch (err) {

                }
                alertmsg('red', response.Message, -1);
            } else {
                alertmsg('red', msgErrorOccured, -1);
            }
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            alert(status + ' ' + error);
        }
    });

});

//Get if margin or amount is checked
function isAmountChecked() {
    var checkedRadio = $('input[name=group1]');
    var checked = checkedRadio.filter(':checked')[0];
    return (checked.id === 'chk-showamount');
}

$('#chk-showamount').change(function (val) {
    toggleAmountDiscount();
    setTimeout(DisableCellByValue, 500);
});

$('#chk-showmargin').change(function (val) {
    toggleAmountDiscount();
    setTimeout(DisableCellByValue, 500);
});

Handsontable.dom.addEvent(searchField, 'keyup', function (event) {
    var queryResult = hot.search.query(this.value);
    //console.log(queryResult);
    hot.render();
});

if (!isEmptyOrSpaces(CatalogSearchAllProducts)) {
    dataFrom = 1; /// product 
    dataId = CatalogSearchAllProducts;
    //   getPaginatedGrid(dataFrom, dataId);
    var totalNumber = CatalogSearchAllProducts.split(",").length;

    getData(dataFrom, dataId, 1, totalNumber);
}
if (productIdList !== "") {
    dataFrom = 1; /// product 
    dataId = productIdList;
    //   getPaginatedGrid(dataFrom, dataId);
    var totalNumber = productIdList.split(",").length;

    getData(dataFrom, dataId, 1, totalNumber);
}

$(function() {
    TblpgntBtm1();
});

