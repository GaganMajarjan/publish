﻿var hot;
//var orderBySource = ['L', 'P', 'Q'];
var orderBySource = ['L', 'P'];//SKYG-820

var margin = 'Margin';

$(function () {
  
    var productIdListQueryString = getQueryStringByName("productIdList");
    if (!productIdListQueryString)
        $('#bulk-pricing').hide();

    if (data != null)
        data = fixJsonQuote(data);

    if (columns != null) {
        columns = fixJsonQuote(columns);
        dataSchema = getDefaultHandsonValue(columns);
    }

    var searchField = document.getElementById('search_field');

    var originalData = [];
    var errorCoordinates = [];

    var container = document.getElementById('bulk-pricing');

    var numberOfStartColumnsToExcludeValidation = staticColumnsCount != null ? staticColumnsCount : 16;
    numberOfStartColumnsToExcludeValidation = 15;

    var contextMenu = {
        callback: function (key, options) {
            if (key === 'about') {
                setTimeout(function () {
                    // timeout is used to make sure the menu collapsed before alert is shown
                    $('#clientinfo').modal('show');
                }, 100);
            }
            else if (key === 'clear_table') {
                setTimeout(function () {
                    $('#confirm-clear-bulk-pricing').modal('show');
                }, 100);
            }
            else if (key === 'col_add') {
                setTimeout(function () {
                    var cols = hot.getSettings().columns;
                    var qtyTitle = cols[cols.length - 2].title;
                    var disTitle = cols[cols.length - 1].title;

                    var suffix = cols[cols.length - 1].title[cols[cols.length - 1].title.length - 1];
                    var newSuffix = parseInt(suffix) + 1;
                    var newQtyTitle = qtyTitle.replace(suffix, newSuffix);
                    var newDisTitle = disTitle.replace(suffix, newSuffix);

                    cols.push({ 'title': newQtyTitle, 'data': newQtyTitle, 'type': 'numeric', 'format': '0,0[0000]' });
                    cols.push({ 'title': newDisTitle, 'data': newDisTitle, 'type': 'numeric', 'format': '$ 0,0.0000[0000]' });

                    hot.updateSettings({
                        columns: cols
                    });

                }, 100);
            }
            else if (key === 'col_remove') {
                setTimeout(function () {
                    var cols = hot.getSettings().columns;

                    //console.log(cols.length);
                    //console.log(numberOfLastColumnsToExcludeValidation + 2);

                    console.log(cols.length);
                    console.log(numberOfLastColumnsToExcludeValidation + 2);

                    if (cols.length <= numberOfLastColumnsToExcludeValidation + 2) {
                        alertmsg('red', 'Cannot remove the first Quantity/Discount column pair');
                        return;
                    }

                    cols.pop();
                    cols.pop();

                    hot.updateSettings({
                        columns: cols
                    });

                }, 100);
            }
        },
        items: {
            'row_above': {},
            'row_below': {},
            'remove_row': {},
            'hsep1': "---------",
            'col_add': {
                name: 'Insert column (Quantity/Discount)',
                disabled: function () {
                    // if not last column, disable this option
                    return !(hot.getSelected()[1] === hot.getSettings().columns.length - 1);
                }
            },
            'col_remove': {
                name: 'Remove column (Quantity/Discount)',
                disabled: function () {
                    // if not last column, disable this option
                    return !(hot.getSelected()[1] === hot.getSettings().columns.length - 1);
                }
            },
            'hsep2': "---------",
            'undo': {},
            'redo': {},
            'hsep3': "---------",
            'make_read_only': {},
            'alignment': {},
            'hsep4': "---------",
            //"about": { name: 'About SkyGeek' },
            "clear_table": {
                name: 'Clear everything'
            }
        }
    };

    var changeLevel = 0;
    var settings = {
        data: data,
        colHeaders: true,
        rowHeaders: true,
        //stretchH: 'all',
        columnSorting: false,
        search: true,
        fixedRowsTop: 0,
        fixedColumnsLeft: 2,
        contextMenu: contextMenu,
        licenseKey: 'non-commercial-and-evaluation',
        columns: columns,
        minSpareRows: 0,
        manualColumnResize: true,
        //manualColumnMove: true,
        dataSchema: dataSchema,
        //autoColumnSize: true,
        afterChange: function(changes, source) {
            try {
                if (changes != null) {
                    var rowOffset = 0;
                    var colOffset = 0;

                    isSaved = false;

                    for (var i = 0; i < changes.length; i++) {
                        if (changes[i] != null) {

                            // [ [row, prop, oldVal, newVal], ... ].
                            var rowIndex = changes[i][0];
                            var prop = changes[i][1];
                            var columnsList = hot.getSettings().columns;
                            var colIndex;
                            $.each(columnsList, function(index, value) {
                                if (value.data === prop) {
                                    colIndex = index;
                                }
                            });

                            //If columns ahead of Quantity1, Discount1 are changed, do not perform quantity-discount validation
                            if (colIndex < numberOfStartColumnsToExcludeValidation)
                                return true;

                            //var oldValue = normalizeAmount(changes[i][2]);
                            var newValue = changes[i][3];

                            if (newValue == 'TRUE') {
                                hot.setDataAtCell(rowIndex, colIndex, true);
                                //data[rowIndex][colIndex] = true;
                                //return true;
                            } else if (newValue == 'FALSE') {
                                hot.setDataAtCell(rowIndex, colIndex, false);
                                //data[rowIndex][colIndex] = false;
                                //return true;
                            }

                            newValue = normalizeAmount(changes[i][3]);

                            var current = getQuantityDiscountPair(prop, 'c');
                            var previous = getQuantityDiscountPair(prop, 'p');
                            var next = getQuantityDiscountPair(prop, 'n');

                            //var currentQuantity = hot.getDataAtProp(current.quantity)[1];
                            //var currentDiscount = hot.getDataAtProp(current.discount)[1];
                            var previousQuantity = hot.getDataAtProp(previous.quantity)[i];
                            var previousDiscount = hot.getDataAtProp(previous.discount)[i];
                            var previousMargin = hot.getDataAtProp(previous.margin)[i];
                            var nextQuantity = hot.getDataAtProp(next.quantity)[i];
                            var nextDiscount = hot.getDataAtProp(next.discount)[i];
                            var nextMargin = hot.getDataAtProp(next.margin)[i];
                            var cost = hot.getDataAtProp("Cost")[i];

                            var prevValue;
                            var nextValue;

                            //alternate value (discount <--> margin)
                            var alternateValue;
                            if (prop.indexOf(quantity) > -1) {
                                //quantity
                                prevValue = previousQuantity;
                                nextValue = nextQuantity;
                            } else if (prop.indexOf(discount) > -1) {
                                //discount
                                prevValue = previousDiscount;
                                nextValue = nextDiscount;
                                alternateValue = convertToGrossMarginPercent(cost, newValue);
                                //update margin
                                if (changeLevel === 0) {
                                    changeLevel++;
                                    hot.setDataAtRowProp(rowIndex, current.margin, alternateValue);
                                } else {
                                    changeLevel = 0;
                                }
                            } else if (prop.indexOf(margin) > -1) {
                                prevValue = previousMargin;
                                nextValue = nextMargin;
                                alternateValue = convertToGrossMarginAmount(cost, newValue); //alternate value is discount
                                //update discount
                                if (changeLevel === 0) {
                                    changeLevel++;
                                    hot.setDataAtRowProp(rowIndex, current.discount, alternateValue);
                                } else {
                                    changeLevel = 0;
                                }
                            }

                            //parse to float
                            //oldValue = parseFloat(oldValue);
                            newValue = parseFloat(newValue);
                            prevValue = parseFloat(prevValue);
                            nextValue = parseFloat(nextValue);
                            if (!$.isNumeric(prevValue)) prevValue = Number.MAX_VALUE;;
                            if (!$.isNumeric(nextValue)) nextValue = Number.MIN_VALUE;;

                            var isQuantityDiscountColumnPair = colIndex > numberOfStartColumnsToExcludeValidation
                                && colIndex < columnsList.length - numberOfLastColumnsToExcludeValidation - 1;

                            var coordinate = { 'rowIndex': rowIndex, 'colIndex': colIndex };

                            if (isAmountChecked()) {
                                if ((newValue <= nextValue || newValue >= prevValue) && isQuantityDiscountColumnPair) {
                                    //push coordinate
                                    errorCoordinates.push(coordinate);
                                } else {
                                    //no error in data, remove from error list if it exists
                                    for (var j = 0; j < errorCoordinates.length; j++) {
                                        errorCoordinates = jQuery.grep(errorCoordinates, function(value) {
                                            return (value.rowIndex !== coordinate.rowIndex && value.colIndex !== coordinate.colIndex);
                                        });
                                    }
                                }
                            } else {
                                if ((newValue >= nextValue || newValue <= prevValue) && isQuantityDiscountColumnPair) {

                                    //push coordinate
                                    errorCoordinates.push(coordinate);
                                } else {
                                    //no error in data, remove from error list if it exists
                                    for (var k = 0; k < errorCoordinates.length; k++) {
                                        errorCoordinates = jQuery.grep(errorCoordinates, function(value) {
                                            return (value.rowIndex !== coordinate.rowIndex && value.colIndex !== coordinate.colIndex);
                                        });
                                    }
                                }
                            }
                        }
                    }

                    //console.log('errorCoordinates');
                    //console.log(errorCoordinates);

                    hot.render();

                    if (errorCoordinates != null) {
                        if (errorCoordinates.length > 0 && source == 'paste')
                        //alertmsg('red', 'Total ' + errorCoordinates.length + ' error(s) are shown in red below');
                            alertmsg('red', msgTotalErrors0AreShownInRedBelow.format(errorCoordinates.length));

                        for (var i = 0; i < errorCoordinates.length; i++) {
                            var tableRowIndex = errorCoordinates[i].rowIndex + 1; //include handsontable row (1) for correct display
                            var tableColIndex = errorCoordinates[i].colIndex + 2; //include handsontable column (2) for correct display
                            var selector = "#bulk-pricing .htCore tbody tr:nth-child(" + tableRowIndex + ") td:nth-child(" + tableColIndex + ")";
                            $(selector).attr('style', 'color:red');
                        }
                    }
                }
            } catch (e) {
                //do nothing
            }
        }

    };

    hot = new Handsontable(container, settings);

    $('#Vendor').select2();

    $('#btn-save').click(function () {
        if (hot.getSettings().data.length == 0) {
            alertmsg('red', 'Please load data to save');
            return;
        }
        var data = JSON.stringify(hot.getSettings().data);

        var vendorId = $('#Vendor').val();
        // Ajax call
        $.ajax({
            type: 'POST',
            url: createUrl,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'bulkPricingJsonString': data, 'vendorId': vendorId },
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {
                //console.log(response);
                if (response != null && response.Status === true) {
                    alertmsg('green', response.Message);
                    isSaved = true;
                } else if (response != null && response.Status === false) {
                    alertmsg('red', response.Message);
                } else {
                    alertmsg('red', msgErrorOccured);
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });

    });

    $('#chk-showamount').change(function (val) {
        toggleAmountDiscount();
    });

    $('#chk-showmargin').change(function (val) {
        toggleAmountDiscount();
    });

    Handsontable.dom.addEvent(searchField, 'keyup', function (event) {
        var queryResult = hot.search.query(this.value);
        //console.log(queryResult);
        hot.render();
    });

    //hide settings by default on page load
    //$('#settings-well').hide();
    $('#btn-settings').hide();

    $('#btn-settings').click(function () {
        $('#settings-well').toggle();
    });

    $('#chk-autosave').click(function () {
        $('#btn-save').toggle();
    });

    $('#clear-bulk-pricing').click(function () {
        $('#confirm-clear-bulk-pricing').modal('show');
    });

    $('#confirm-clear-bulk-pricing-yes').click(function () {
        $('#confirm-clear-bulk-pricing').modal('hide');
        //remove all rows
        hot.alter('remove_row', 0, hot.getSettings().data.length);
        hot.alter('insert_row');
        $('#Vendor').select2('val', '');
    });

    function parseRow(infoArray, index, csvContent) {
        var sizeData = _.size(hot.getSettings().data);
        if (index < sizeData - 1) {
            dataString = "";
            _.each(infoArray, function (col, i) {
                dataString += _.contains(col, ",") ? "\"" + col + "\"" : col;
                dataString += i < _.size(infoArray) - 1 ? "," : "";
            });

            csvContent += index < sizeData - 2 ? dataString + "\n" : dataString;
        }
        return csvContent;
    }

    var handsontable2csv = {
        string: function (instance) {
            //var headers = hot.getSettings().columns;
            var headers = instance.getColHeader();

            //var rowHeaders = $("#feed-generator").handsontable("getRowHeader");
            //console.log(headers);
            //console.log(rowHeaders);
            //var csv = "sep=,\n";
            var csv = "";
            csv += headers.join(",") + "\n";

            for (var i = 0; i < instance.countRows() ; i++) {
                var row = [];
                for (var h in headers) {
                    var prop = instance.colToProp(h);
                    var value = instance.getDataAtRowProp(i, prop);
                    row.push(value);
                }

                csv += row.join(",");
                csv += "\n";
            }

            return csv;
        },

        download: function (instance, filename) {
            var csv = handsontable2csv.string(instance);

            var link = document.createElement("a");
            link.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(csv));
            link.setAttribute("download", filename);

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }

    /**
     * Export to CSV button
     */
    var exportCsv = $("#export-csv")[0];
    if (exportCsv) {
        Handsontable.dom.addEvent(exportCsv, "mouseup", function (e) {
            ////var colHeaders = $("#bulk-pricing").handsontable("getColHeader");
            //var colHeaders = hot.getSettings().columns;
            ////console.log(colHeaders);
            ////console.log(colHeaders.title);
            //exportCsv.blur(); // jquery ui hackfix
            //var csvContent = "data:text/csv;charset=utf-8,";
            //csvContent = parseRow(colHeaders, 0, csvContent); // comment this out to remove column headers
            //$.each(hot.getSettings().data, function (infoArray, index) {
            //    csvContent = parseRow(infoArray, index, csvContent);
            //});
            //var encodedUri = encodeURI(csvContent);
            //var link = document.createElement("a");
            //link.setAttribute("href", encodedUri);
            //link.setAttribute("download", "bulk-pricing.csv");
            //link.click();

            var exportFormat = $("#exportFormat").val();
            if (exportFormat == null || exportFormat === "")
                exportFormat = "csv";

            handsontable2csv.download(hot, "product-feed." + exportFormat);

        });
    }

    //if productIdList is present, hide vendor
    var productIdListQueryString = getQueryStringByName('productIdList');
    if ($.trim(productIdListQueryString).length > 0) {
        $('#vendor-dropdown').hide();
    }

});

function normalizeAmount(value) {
    if (value == null)
        return 0;

    if (!$.isNumeric(value))
        return value;

    return value.toString().replace(/$/gi, '').replace(/,/gi, '').replace(/%/gi, '');

}

//Get if margin or amount is checked
function isAmountChecked() {
    var checkedRadio = $('input[name=group1]');
    var checked = checkedRadio.filter(':checked')[0];
    return (checked.id === 'chk-showamount');
}

$('#Vendor').change(function (value) {

    if (value != null) {

        var vendorId = $('#Vendor').select2().val();

        if (vendorId == null)
            return;

        // var vendorId = value.val;

        // Ajax call
        $.ajax({
            type: 'POST',
            url: urlGetBulkPricing,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'vendorId': vendorId },
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {
                //console.log(response);
                if (response != null && response.Status === true) {

                    $('#bulk-pricing').show();

                    var data = response.Data.data;
                    var cols = response.Data.columns;

                    data = fixJsonQuote(data);
                    if (cols != null)
                        cols = fixJsonQuote(cols);

                    dataSchema = getDefaultHandsonValue(cols);

                    //data = JSON.parse(data);
                    //cols = JSON.parse(cols);
                    //dataSchema = JSON.parse(dataSchema);

                    hot.updateSettings({
                        data: data,
                        columns: cols,
                        dataSchema: dataSchema
                    });

                } else if (response != null && response.Status === false) {
                    alertmsg('red', response.Message);
                    try {
                        //hot.clear();
                        //hot.updateSettings({
                        //    data: null,
                        //    columns: null
                        //});

                        hot.alter('remove_row', 0, hot.getSettings().data.length);
                        hot.alter('insert_row');

                        $('#bulk-pricing').hide();
                    } catch (er) {

                    }
                } else {
                    alertmsg('red', msgErrorOccured);
                    try {
                        //hot.clear();
                        //hot.updateSettings({
                        //    data: null,
                        //    columns: null
                        //});

                        hot.alter('remove_row', 0, hot.getSettings().data.length);
                        hot.alter('insert_row');


                        $('#bulk-pricing').hide();
                    } catch (er) {

                    }
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                //alert(status + ' ' + error);
                try {
                    //hot.clear();
                    //hot.updateSettings({
                    //    data: null,
                    //    columns: null
                    //});

                    hot.alter('remove_row', 0, hot.getSettings().data.length);
                    hot.alter('insert_row');

                    $('#bulk-pricing').hide();
                    SGloadingRemove();
                } catch (er) {

                }
            }
        });
        // To actually transmit to server as a regular form post, write this: ko.utils.postJson($("form")[0], self.PricingLevels);
    } else {
        try {
            //hot.clear();
            //hot.updateSettings({
            //    data: null,
            //    columns: null
            //});

            hot.alter('remove_row', 0, hot.getSettings().data.length);
            hot.alter('insert_row');

            $('#bulk-pricing').hide();
        } catch (er) {

        }
    }
});

var getDefaultHandsonValue = function (columns) {
    var defaultValue = {};
    for (var i = 0; i < columns.length; i++) {
        var data = columns[i].data;
        var type = columns[i].type;
        if (type === 'checkbox') {
            defaultValue[data] = false;
        } else {
            defaultValue[data] = null;
        }
    }
    return defaultValue;
}

var getQuantityDiscountPair = function (colName, cpn) {
    if (colName == null)
        return colName;

    var index = colName.replace(quantity, '').replace(discount, '').replace(margin, '');
    if (cpn === 'c') {
        //current -- do not change index
    }
    else if (cpn === 'p') {
        //previous -- decrease index by 1
        index--;
    }
    else if (cpn === 'n') {
        //next -- increase index by 1
        index++;
    }
    return { quantity: quantity + index, discount: discount + index, margin: margin + index };
}

var toggleAmountDiscount = function () {
    var columns = hot.getSettings().columns;
    //console.log(columns);
    for (var i = 0; i < columns.length; i++) {
        var setting = {};

        if (columns[i].data.indexOf(discount) > -1) {
            setting['title'] = columns[i].title.replace(discount, margin);
            setting['data'] = columns[i].data.replace(discount, margin);
            setting['type'] = columns[i].type;
            setting['format'] = columns[i].format.replace('$ ', '') + ' %';
            columns[i] = setting;
        }
        else if (columns[i].data.indexOf(margin) > -1) {
            setting['title'] = columns[i].title.replace(margin, discount);
            setting['data'] = columns[i].data.replace(margin, discount);
            setting['type'] = columns[i].type;
            setting['format'] = '$ ' + columns[i].format.replace(' %', '');
            columns[i] = setting;
        }
    }

    hot.updateSettings(
    {
        columns: columns
    });

}

