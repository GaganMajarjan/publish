﻿var stepped = 0, chunks = 0, rows = 0;
var start, end;
var parser;
var pauseChecked = false;
var printStepChecked = false;
var header = [];
var numberOfColumns = 0;
var clientsideData;
var loadingRemove;
var flag = 0;
var orderByChangeFlag = false;
var greenColor = '#22f4b1';
var blueColor = '#22e5f4';
var recordCount = 0;
var stagingMetadataId = "";
var bulkVendorProductData = "";
var pageLimit = 100;
var ErrorbulkData = "";
var bulkSliceCols = "";
var copyAllcolLst = [];
var stagingIdsLstToDelete = [];
var isbulkVendorProductModule = false;
var errors = [];
var copiedText = undefined;

//var isSalePriceDisabled = false;
//var disabledSalePrice = [];
//var salePriceError = false;
//var salePriceErrorArr = [];
var wasSetPrimary = false;
function markClearRow() {
    for (var rowIndex = 0; rowIndex < errorRows.length; rowIndex++) {
        hot.setCellMeta(errorRows[rowIndex], 0, 'valid', true);
        hot.setCellMeta(errorRows[rowIndex], 0, 'comment', null);
        hot.setCellMeta(errorRows[rowIndex] - 1, 0, 'comment', null);
    }
    //clear error rows for next one
    errorRows = [];
}

function markSuccessRow(rowIndex) {
    var endValue = hot.getSettings().data.length;

    if (rowIndex >= 1 && rowIndex < endValue) {
        hot.setCellMeta(rowIndex - 1, 0, 'comment', 'Record(s) from 1 to {0} saved successfully'.format(rowIndex));
    } else if (rowIndex == 1 && rowIndex < endValue) {
        hot.setCellMeta(rowIndex, 0, 'comment', 'Record(s) from 1 to {0} saved successfully'.format(rowIndex));
    }
}

function markErrorRow(rowIndex, message) {
    errorRows.push(rowIndex - 1);
    hot.setCellMeta(rowIndex - 1, 0, 'valid', false);
    hot.setCellMeta(rowIndex - 1, 0, 'comment', message);
    hot.selectCell(rowIndex - 1, 0);
}

function markStatus(rowNumber, errormessage) {
    if (isNaN(parseInt(rowNumber)))
        return;
    markSuccessRow(rowNumber - 1);
    markErrorRow(rowNumber, errormessage);
    return false;
}

function showErrors(errors) {
    if (!hot) {
        console.debug("hot not initialized. cannot show error message to hot grid");
        return;
    }
    if (!errors) {
        console.debug("no errors to show");
        return;
    }
    for (var i = 0; i < errors.length; i++) {
        markErrorRow(errors[i].RowIndex, errors[i].Message);
    }
}

function buildConfig() {
    return {
        delimiter: ",",
        newline: "", //auto-detect
        header: true,
        dynamicTyping: "",
        preview: parseInt(0),
        step: undefined,
        encoding: "",
        worker: "",
        comments: "",
        complete: completeFn,
        error: errorFn,
        download: "",
        fastMode: "",
        skipEmptyLines: true,
        chunk: undefined,
        beforeFirstChunk: undefined,
    };

    function getLineEnding() {

        if ($('#newline-n').is(':checked'))
            return "\n";
        else if ($('#newline-r').is(':checked'))
            return "\r";
        else if ($('#newline-rn').is(':checked'))
            return "\r\n";
        else
            return "";
    }
}

function stepFn(results, parserHandle) {
    stepped++;
    rows += results.data.length;

    parser = parserHandle;

    if (pauseChecked) {
        //console.log(results, results.data[0]);
        parserHandle.pause();
        return;
    }

    if (printStepChecked) {
    }
    //console.log(results, results.data[0]);
}

function chunkFn(results, streamer, file) {
    if (!results)
        return;
    chunks++;
    rows += results.data.length;

    parser = streamer;

    if (printStepChecked)
        if (pauseChecked) {
            streamer.pause();
            return;
        }
}

function errorFn(error, file) {
    //console.log("ERROR:", error, file);
}

function completeFn() {
    end = performance.now();
    if (!$('#stream').prop('checked')
        && !$('#chunk').prop('checked')
        && arguments[0]
        && arguments[0].data)
        rows = arguments[0].data.length;
    var headerJson = arguments[0].meta.fields;
    clientsideData = arguments;
    //console.log(clientsideData);
    numberOfColumns = 0;
    pricingLevelColumns = 0;
    $.each(headerJson, function (key, value) {
        var fileTypeCheck = "=";
        if (!filetypeFlag) {
            if (value.indexOf(fileTypeCheck) !== -1) {
                filetypeFlag = true;

                customDelimiter = "\",=\"";

                $("#file").trigger("change");

            }
        }

        numberOfColumns += 1;
        if (value.indexOf("Margin") >= 0) {
            pricingLevelColumns++;
        }
        header.push(value);

    });
    //console.log(header);
    //console.log(arguments);

}

function parseRow(infoArray, index, csvContent) {
    var sizeData = _.size(hot.getSettings().data);
    if (index < sizeData - 1) {
        dataString = "";
        _.each(infoArray, function (col, i) {
            dataString += _.contains(col, ",") ? "\"" + col + "\"" : col;
            dataString += i < _.size(infoArray) - 1 ? "," : "";
        });

        csvContent += index < sizeData - 2 ? dataString + "\n" : dataString;
    }
    return csvContent;
}

var contextMenu = {
    callback: function (key, options) {
        var length = options.length - 1;
        var row = options[length].start.row;
        var column = options[length].start.col;

        if (hot.getSettings().columns[column].readOnly === true)
            return;



        var textToCopy = hot.getDataAtCell(row, column);
        

        if (key === 'copy_text_to_all') {
            setTimeout(function () {
            var hotLength = hot.getData().length;

            if (textToCopy !== undefined) {
                //var columnTitle = hot.getSettings().columns[column].title;
                //var indexOfColumn = copyAllcolLst.indexOf(columnTitle);

                //if (columnTitle === "Margin" && copyAllcolLst.indexOf("Selling Price") > -1)
                //{
                //    copyAllcolLst.splice(copyAllcolLst.indexOf("Selling Price"), 1);
                //}

                //if (columnTitle === "Selling Price" && copyAllcolLst.indexOf("Margin") > -1) {
                //    copyAllcolLst.splice(copyAllcolLst.indexOf("Margin"), 1);
                //}
                //if (indexOfColumn > -1) {

                //    copyAllcolLst.splice(indexOfColumn, 1);

                //}
                //copyAllcolLst.push(columnTitle);
                if (textToCopy === " ") {
                    textToCopy = '';
                }
                for (var x = 0; x < hotLength; x++) {
                    hot.setDataAtCell(x, column, textToCopy, '')
                }

                if (isbulkVendorProductModule === true) {
                    var isErrorLoad = getQueryStringByName('ErrorMetaDataId') === "" ? false : true;

                    $.ajax({
                        type: 'POST',
                        url: urlBulkStagingUpdate,
                        data: { 'MetaDataId': stagingMetadataId, 'Column': hot.colToProp(column), 'Value': textToCopy, 'isErrorLoad': isErrorLoad },
                        beforeSend: function () {
                            SGloading('Loading');
                        },
                        success: function () {
                            //SGloadingRemove();
                        },
                        complete: function () { SGloadingRemove(); }
                    })
                     hot.render();
                }
            }
            }, 100);
        }
        else if (key === 'clear_value') {
            
            hot.setDataAtCell(row, column, '{blank}', '');
        }

      
        

        if (key === 'col_add') {
            
            setTimeout(KitCostDisabled, 500);
            setTimeout(function () {
                var cols = hot.getSettings().columns;
                var qtyTitle = cols[cols.length - 2].title;
                var disTitle = cols[cols.length - 1].title;

                var suffix = cols[cols.length - 1].title[cols[cols.length - 1].title.length - 1];
                var newSuffix = parseInt(suffix) + 1;

                var newQtyTitle = qtyTitle.replace(suffix, newSuffix);
                var newDisTitle = disTitle.replace(suffix, newSuffix);

                ////If last header do not contain qty, discount column pair, then set Margin1 and Discount1 as headers to be added
                //if (qtyTitle.indexOf(quantity) === -1) {
                //    newQtyTitle = quantity + '1';
                //}
                //if (disTitle.indexOf(discount) === -1) {
                //    newDisTitle = discount + '1';
                //}

                //check if 
                cols.push({ 'title': newQtyTitle, 'data': newQtyTitle, 'type': 'numeric', 'format': '0,0[0000]' });
                if (isAmountChecked()) {
                    cols.push({ 'title': newDisTitle, 'data': newDisTitle, 'type': 'numeric', 'format': '$ 0,0.0000[0000]' });
                } else {
                    cols.push({ 'title': newDisTitle, 'data': newDisTitle, 'type': 'numeric', 'format': '0,0.0000[0000] %' });
                }
                //check
                //margin %

                hot.updateSettings({
                    columns: cols
                });

            }, 100);
        }
        else if (key === 'remove_col1') {
           
            setTimeout(function () {
            var cols = hot.getSettings().columns;
                
            var columns = column;
            var removeIdx = customFieldIdLst.findIndex(x => x == cols[columns].id);
            if (column === 0) {
                alertmsg('red', 'Cannot remove column LocalSku');
                return;
            }

                var boolean = false;
            //cols.pop();

            //hot.updateSettings({
            //    columns: cols
            //});
            var index = arrRequiredCustomField.findIndex(row => row.includes(cols[columns].title));
            if (index >= 0) {
                  
            var exists = cols.some(y => arrRequiredCustomField[index].includes(y.title));
                if (!exists) {
                    boolean = true;   
                }
            }
            else {
                   boolean = true;
                }
                if (boolean) {
                    $('#CustomFields').val(null).trigger("change");
                    customFieldIdLst.splice(removeIdx, 1);
                    var dataList = hot.getSettings().data;
                    for (var i = 0; i <= hot.countRows(); i++) {
                            var deletecolumn = cols[columns].data;
                            delete dataList[i][""+deletecolumn+""];
                            delete dataList[i][""+deletecolumn + "_id"+""];
                            delete dataList[i][""+deletecolumn + "_name"+""];
                    }

                    cols.splice(columns, 1);
                    hot.updateSettings({
                        columns: cols,
                        data: dataList
                    });
                   
 
                    

                }
            hot.render();
            }, 100);
        }

       
    },
    items: {
        'copy_text_to_all': {
            name: 'Fill Column With This Value'
        },
        'clear_value': {
            name: 'Clear Cell and Set to Blank'
        },
        'remove_row': {}
      
    }
};
var contextMenuBIU = {
    callback: function (key, options) {
        var length = options.length - 1;
        var row = options[length].start.row;
        var column = options[length].start.col;

         

        if (hot.getSettings().columns[column].readOnly === true)
            return;



        var textToCopy = hot.getDataAtCell(row, column);

        if (key === 'copy_text_to_all') {
            setTimeout(function () {
            var hotLength = hot.getData().length;

            if (textToCopy !== undefined) {

                if (textToCopy === " ") {
                    textToCopy = '';
                }
                for (var x = 0; x < hotLength; x++) {
                    hot.setDataAtCell(x, column, textToCopy, '')
                }
                
                if (isbulkVendorProductModule === true) {
                    var isErrorLoad = getQueryStringByName('ErrorMetaDataId') === "" ? false : true;

                    $.ajax({
                        type: 'POST',
                        url: urlBulkStagingUpdate,
                        data: { 'MetaDataId': stagingMetadataId, 'Column': hot.colToProp(column), 'Value': textToCopy, 'isErrorLoad': isErrorLoad },
                        beforeSend: function () {
                            SGloading('Loading');
                        },
                        success: function () {
                            //SGloadingRemove();
                        },
                        complete: function () { SGloadingRemove(); }
                    })
                }

                hot.render();

            }
            }, 100);
        }
        else if (key === 'clear_value') {
            hot.setDataAtCell(row, column, '{blank}', '')
        }
        else if (key === 'copy') {
            copiedText = hot.getDataAtCell(row, column);
        }

        else if (key === 'paste') {
            if (copiedText !== undefined)

                options.forEach(function (option) {
                    var startrow = option.start.row;
                    var endrow = option.end.row;
                    for (var r = startrow ; r <= endrow; r++) {
                        hot.setDataAtCell(r, column, copiedText, '');
                    }

                })


            // hot.setDataAtCell(row, column, copiedText, '');
            //copiedText = undefined;

        }


        //else if (key === 'paste') {
        //    if (copiedText !== undefined)

        //    var startrow = options[length].start.row;
        //    var endrow = options[length].end.row;
        //    for (var r = startrow ; r <= endrow; r++)
        //    {
        //        hot.setDataAtCell(r, column, copiedText, '');
        //    }

        //       // hot.setDataAtCell(row, column, copiedText, '');
        //    //copiedText = undefined;

        //}

        if (key === 'col_add') {
            setTimeout(KitCostDisabled, 500);
            setTimeout(function () {
                var cols = hot.getSettings().columns;
                var qtyTitle = cols[cols.length - 2].title;
                var disTitle = cols[cols.length - 1].title;

                var suffix = cols[cols.length - 1].title[cols[cols.length - 1].title.length - 1];
                var newSuffix = parseInt(suffix) + 1;

                var newQtyTitle = qtyTitle.replace(suffix, newSuffix);
                var newDisTitle = disTitle.replace(suffix, newSuffix);

                ////If last header do not contain qty, discount column pair, then set Margin1 and Discount1 as headers to be added
                //if (qtyTitle.indexOf(quantity) === -1) {
                //    newQtyTitle = quantity + '1';
                //}
                //if (disTitle.indexOf(discount) === -1) {
                //    newDisTitle = discount + '1';
                //}

                //check if 
                cols.push({ 'title': newQtyTitle, 'data': newQtyTitle, 'type': 'numeric', 'format': '0,0[0000]' });
                if (isAmountChecked()) {
                    cols.push({ 'title': newDisTitle, 'data': newDisTitle, 'type': 'numeric', 'format': '$ 0,0.0000[0000]' });
                } else {
                    cols.push({ 'title': newDisTitle, 'data': newDisTitle, 'type': 'numeric', 'format': '0,0.0000[0000] %' });
                }
                //check
                //margin %

                hot.updateSettings({
                    columns: cols
                });

            }, 100);
        }
        else if (key === 'remove_col1') {

            setTimeout(function () {
                var cols = hot.getSettings().columns;
              
                var columns = column;
                var removeIdx = customFieldIdLst.findIndex(x => x == cols[columns].id);

                if (column == 0) {
                    alertmsg('red', 'Cannot remove column LocalSku');
                    return;
                }

                var boolean = false;
                var index = arrRequiredCustomField.findIndex(row => row.includes(cols[columns].title));
                if (index >= 0) {

                    var exists = cols.some(y => arrRequiredCustomField[index].includes(y.title));
                    if (!exists) {
                        boolean = true;
                    }
                }
                else {
                    boolean = true;
                }
                if (boolean) {
                    $('#CustomFields').val(null).trigger("change");
                    customFieldIdLst.splice(removeIdx, 1);
                    var dataList = hot.getSettings().data;
                    for (var i = 0; i < hot.countRows(); i++) {
                            var deletecolumn = cols[columns].data;
                            delete dataList[i][""+deletecolumn+""];
                            delete dataList[i][""+deletecolumn + "_id"+""];
                            delete dataList[i][""+deletecolumn + "_name"+""];
                    }

                    cols.splice(columns, 1);
                    hot.updateSettings({
                        columns: cols,
                        data: dataList
                    });
                   
                 
                    
                    
                }
                hot.render();
            }, 100);
        }

        else if (key === "remove_row") {

           
            if (stagingIdsLstToDelete.length > 0) {
                concatenatedStagingIds = stagingIdsLstToDelete.join();
                $.ajax({
                    type: "POST",
                    url: "DeleteRowsFromStaging",
                    data: { 'MetadataId': stagingMetadataId, "Ids": concatenatedStagingIds },
                    beforeSend: function () {
                        SGloading("Deleting");
                    },
                    success: function () {
                        SGloadingRemove();

                    }


                })
            }

            setTimeout(KitCostDisabled, 500);
        }
    },
    items: {
        'copy': { name: 'Copy' },
        'paste':{name: 'Paste'},
        'copy_text_to_all': {
            name: 'Fill Column With This Value'
        },
        'clear_value': {
            name: 'Clear Cell and Set to Blank'
        },
        'remove_row': {},
        'remove_col1': { name: 'Remove Column' },
        
        
    }
};

var handsontable2csv = {
    string: function (instance) {
        //var headers = hot.getSettings().columns;
        var headers = instance.getColHeader();

        //var rowHeaders = $("#feed-generator").handsontable("getRowHeader");
        //console.log(headers);
        //console.log(rowHeaders);
        //var csv = "sep=,\n";
        var csv = "";
        csv += headers.join(",") + "\n";

        for (var i = 0; i < instance.countRows(); i++) {
            var row = [];
            for (var h in headers) {
                var prop = instance.colToProp(h);
                var value = instance.getDataAtRowProp(i, prop);

                if (typeof value !== "object" && value !== "<undefined></undefined>") {
                    if (value !== null && value !== 'undefined' && typeof value !== 'undefined')
                        row.push("\"" + value + "\"");
                    else {
                        row.push("\"\"");
                    }
                }
            }

            csv += row.join(",");
            csv += "\n";
        }

        return csv;
    },

    download: function (instance, filename) {
        //console.log('download started');
        var csv = handsontable2csv.string(instance);

        var link = document.createElement("a");
        link.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(csv));
        link.setAttribute("download", filename);

        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
}

var getDefaultHandsonValue = function (columns) {
    var defaultValue = {};
    for (var i = 0; i < columns.length; i++) {
        var data = columns[i].data;
        var type = columns[i].type;
        if (type === 'checkbox') {
            defaultValue[data] = false;
        } else {
            defaultValue[data] = null;
        }
    }
    return defaultValue;
}

var getQuantityDiscountPair = function (colName, cpn) {
    if (colName == null)
        return colName;
    var index = colName.replace(quantity, '')
        .replace(marginHeader, '')
        .replace(discountHeader, '')
        .replace(sellingPrice, '')
        .replace(margin, '');

    if (cpn === 'c') {
        //current -- do not change index
    }
    else if (cpn === 'p') {
        //previous -- decrease index by 1
        index--;
    }
    else if (cpn === 'n') {
        //next -- increase index by 1
        index++;
    }
    return { quantity: quantity + index, sellingPrice: sellingPrice + index, margin: margin + index };
}

var getQuantityDiscountPairVpc = function (colName, cpn) {
    if (colName == null)
        return colName;
    var index = colName.replace(/^V/g, '')
        .replace(quantity, '')
        .replace(marginHeader, '')
        .replace(discountHeader, '')
        .replace(discount, '')
        .replace(margin, '')
        .replace(sellingPrice, '');

    if (cpn === 'c') {
        //current -- do not change index
    }
    else if (cpn === 'p') {
        //previous -- decrease index by 1
        index--;
    }
    else if (cpn === 'n') {
        //next -- increase index by 1
        index++;
    }
    return { quantity: 'V' + quantity + index, sellingPrice: 'V' + sellingPrice + index, margin: 'V' + margin + index };
}

var toggleAmountDiscount = function () {
    var columns = hot.getSettings().columns;
    //console.log(columns);
    for (var i = 0; i < columns.length; i++) {
        var setting = {};

        //exclude SalePrice/SalePriceMarginPercent
        if (columns[i].data.indexOf(discount) > -1 && columns[i].data.indexOf("SalePrice") === -1) {
            setting['title'] = columns[i].title.toString().replace(discountHeader, marginHeader);
            setting['data'] = columns[i].data.toString().replace(discount, margin);
            setting['type'] = columns[i].type;
            if (columns[i].format)
                setting['format'] = columns[i].format.toString().replace('$ ', '') + ' %';
            columns[i] = setting;
        }
        else if (columns[i].data.indexOf(margin) > -1 && columns[i].data.indexOf("SalePrice") === -1) {
            setting['title'] = columns[i].title.toString().replace(marginHeader, discountHeader);
            setting['data'] = columns[i].data.toString().replace(margin, discount);
            setting['type'] = columns[i].type;
            if (columns[i].format)
                setting['format'] = '$ ' + columns[i].format.toString().replace(' %', '');
            columns[i] = setting;
        }
    }

    hot.updateSettings(
        {
            columns: columns
        });

}

function ToggleOrderBy() {
    var tableData = hot.getSettings().data.length;

    var rowValue = 1;
    for (var counter = 0; counter < tableData; counter++) {

        var orderBy = hot.getSettings().data[counter]["OrderBy"];
        //SKYg-820
        if (orderBy === "L") {
            //do nothing
        }
        else if (orderBy === "Q") {
            //var qpl = hot.getSettings().data[counter].QuantityPerLot;
            hot.getSettings().data[counter]["CostPerLot"] = null;
            hot.getSettings().data[counter]["LotPartNumber"] = null;
            hot.getSettings().data[counter]["LotDescription"] = null;
            //hot.getSettings().data[counter]["QuantityPerLot"] = qpl; //retain QPL
        }
        else {
            hot.getSettings().data[counter]["CostPerLot"] = null;
            hot.getSettings().data[counter]["LotPartNumber"] = null;
            hot.getSettings().data[counter]["LotDescription"] = null;
            hot.getSettings().data[counter]["QuantityPerLot"] = null;
        }

        rowValue++;
    }
    hot.render();
}

function KitCostDisabled() {

    ToggleOrderBy();

    var tableData = hot.getSettings().data.length;
    //4:costperlot 5:lotpartnumber 11:lotdescription 12:quantityperlot 13:orderby 14:minorderunit 19:setasprimary 20:dropship 21:minimumorderquantity 22:cost (all added by 1)
    //var disableArray = [4, 5, 11, 12, 13, 14, 19, 20, 21, 22];
    //var disableArray = [5, 6, 12, 13, 14, 15, 20, 21, 22, 23];

    var disableArray = [5, 6, 7, 8, 9, 10, 15, 16, 17, 18]; //after removed bulk-pricing-fields

    var rowValue = 1;
    for (var counter = 0; counter < tableData; counter++) {
        var isKitRow = hot.getSettings().data[counter]["IsKit"];

        if (isKitRow === 1) {

            $.each(disableArray, function (index, value) {
                var selector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + value + ")";
                $(selector).attr("class", "non-editable");
            });

        }

        var orderBy = hot.getSettings().data[counter]["OrderBy"];
        var costPerLotColumnIndex = 5;
        var lotPartNumberColumnIndex = 6;
        var lotDescriptionColumnIndex = 7;
        var quantityPerLotColumnIndex = 8;

        if (orderBy === "L") {
            var costPerLotSelector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + costPerLotColumnIndex + ")";
            $(costPerLotSelector).attr("class", "");

            var lotPartNumberSelector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + lotPartNumberColumnIndex + ")";
            $(lotPartNumberSelector).attr("class", "");

            var lotDescriptionSelector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + lotDescriptionColumnIndex + ")";
            $(lotDescriptionSelector).attr("class", "");

            var quantityPerLotSelector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + quantityPerLotColumnIndex + ")";
            $(quantityPerLotSelector).attr("class", "");

        }
        else if (orderBy === "Q") {
            var costPerLotSelector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + costPerLotColumnIndex + ")";
            $(costPerLotSelector).attr("class", "non-editable");
            hot.getSettings().data[counter]["CostPerLot"] = null;

            var lotPartNumberSelector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + lotPartNumberColumnIndex + ")";
            $(lotPartNumberSelector).attr("class", "non-editable");

            var lotDescriptionSelector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + lotDescriptionColumnIndex + ")";
            $(lotDescriptionSelector).attr("class", "non-editable");

            var quantityPerLotSelector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + quantityPerLotColumnIndex + ")";
            $(quantityPerLotSelector).attr("class", "");
        }
        else {
            var costPerLotSelector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + costPerLotColumnIndex + ")";
            $(costPerLotSelector).attr("class", "non-editable");

            var lotPartNumberSelector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + lotPartNumberColumnIndex + ")";
            $(lotPartNumberSelector).attr("class", "non-editable");

            var lotDescriptionSelector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + lotDescriptionColumnIndex + ")";
            $(lotDescriptionSelector).attr("class", "non-editable");

            var quantityPerLotSelector = "#bulk-pricing .htCore tbody tr:nth-child(" + rowValue + ") td:nth-child(" + quantityPerLotColumnIndex + ")";
            $(quantityPerLotSelector).attr("class", "non-editable");
        }

        rowValue++;
    }
}

function performAutoCalculation(data, columns) {

    if (!data) {
        throw new Error("data cannot be null");
    }

    //if (!columns) {
    //    throw new Error("columns cannot be null");
    //}


    for (var rowIndex = 0; rowIndex < data.length; rowIndex++) {

        //fix: kit don't have vendor/vpn so removed
        if (data[rowIndex] !== null && (data[rowIndex].LocalPartNumber || data[rowIndex].LocalSku || data[rowIndex]["Local Part Number"])) {

            //} && data[rowIndex].Vendor && data[rowIndex].VendorPartNumber) {

            //get cost
            //if (data[rowIndex]["Sale Price Margin"] === null) {
            //    data[rowIndex]["Sale Price Margin"] = 0;
            //}


            if (data[rowIndex]["Unit Price"] === undefined) {
                data[rowIndex]["Unit Price"] = data[rowIndex]["UnitPrice"] || 0;
            }



            if (data[rowIndex]["Non Cancel Non Return"] === '1') {
                data[rowIndex]["Non Cancel Non Return"] = "Yes";
            }
            else if (data[rowIndex]["Non Cancel Non Return"] === '0') {
                data[rowIndex]["Non Cancel Non Return"] = "No";
            }
            if (data[rowIndex]["Margin Locked"] === '1') {
                data[rowIndex]["Margin Locked"] = "Yes";
            }
            else if (data[rowIndex]["Margin Locked"] === '0') {
                data[rowIndex]["Margin Locked"] = "No";
            }
            if (data[rowIndex]["Drop Ship"] === '1') {
                data[rowIndex]["Drop Ship"] = "No";
            }
            else if (data[rowIndex]["Drop Ship"] === '0') {
                data[rowIndex]["Drop Ship"] = "Yes";
            }

            var vendorCost = parseFloat(data[rowIndex].Cost) || 0;
            var clientCost = parseFloat(data[rowIndex].Cost) || 0;
            var fifoCost = parseFloat(data[rowIndex]["AvgFifo Cost"]);
            if (!fifoCost) {
                fifoCost = parseFloat(data[rowIndex]["FifoCost"]);
            }
            fifoCost = parseFloat(fifoCost) || 0;

            var selectedCost = parseFloat(data[rowIndex]["Selected Cost"]);
            if (!selectedCost) {
                selectedCost = parseFloat(data[rowIndex]["SelectedCost"]);
            }
            selectedCost = parseFloat(selectedCost) || 0;

            var costOption = data[rowIndex]["Cost Option"];
            if (!costOption) {
                costOption = data[rowIndex]["CostOption"];
            }
            if (!costOption) {
                costOption = "auto calculate cost";
                data[rowIndex]["Cost Option"] = "Auto Calculate Cost";
            }



            if (costOption.toLowerCase() === "auto calculate cost") {
                if (fifoCost > clientCost) {
                    clientCost = fifoCost;
                }
            }

            else if (costOption.toLowerCase() === "fifo cost") {
                if (fifoCost > 0) {
                    clientCost = fifoCost;
                }
            }

            data[rowIndex]["Selected Cost"] = FormatAmount(clientCost);
            data[rowIndex]["SelectedCost"] = FormatAmount(clientCost);


            //check if margin is locked
            var marginLocked = (data[rowIndex]["Margin Locked"] || '').toLowerCase();

            if (marginLocked === 'yes' || marginLocked === '1') { marginLocked = 'true'; }


            if (marginLocked !== '') {

                //for regular margin/discount

                var marginPricing = data[rowIndex]["Margin"];
                marginPricing = parseFloat(marginPricing) || 0;
                //var sellingPrice = data[rowIndex]["Selling Price"] || 0;//SKYG-822
                var sellingPrice = data[rowIndex]["Unit Price"];
                if (sellingPrice === undefined) {
                    sellingPrice = data[rowIndex]["UnitPrice"];
                    if (sellingPrice === undefined) {
                        sellingPrice = data[rowIndex]["Price"] || 0;
                    }
                }

                if (marginLocked === 'true' || sellingPrice === 0) {
                    if (marginPricing !== 0) {
                        //calculate selling price
                        var sellingPrice = calculateSellingPrice(clientCost, marginPricing);
                        sellingPrice = parseFloat(sellingPrice) || 0;
                        //set selling price to data
                        if (sellingPrice > 0) {
                            data[rowIndex]["Price"] = FormatAmount(sellingPrice);
                            //data[rowIndex]["Selling Price"] = FormatAmount(sellingPrice);//SKYG-822
                            data[rowIndex]["Unit Price"] = FormatAmount(sellingPrice);
                            data[rowIndex]["UnitPrice"] = FormatAmount(sellingPrice);
                        }

                        //calculate discount
                        var discount = parseFloat(sellingPrice) - parseFloat(clientCost);
                        discount = parseFloat(discount) || 0;
                        //set discount to data
                        if (discount > 0)
                            data[rowIndex]["Discount"] = FormatAmount(discount);
                    }
                }

                else {
                    var marginSellingPrice = parseFloat(sellingPrice) || 0;
                    data[rowIndex]["Unit Price"] = FormatAmount(marginSellingPrice);
                    var marginPricing = data[rowIndex]["Margin"];
                    if (marginSellingPrice > 0) {
                        marginPricing = calculateGrossMarginPercentFromSellingPrice(clientCost, marginSellingPrice);
                        marginPricing = parseFloat(marginPricing) || 0;
                        data[rowIndex]["Margin"] = FormatAmount(marginPricing);
                    }

                }


                //if sale price exists, calculate sale price margin percent
                var salePrice = data[rowIndex]["SalePrice"];
                // salePrice = parseFloat(salePrice) || 0;

                var salePriceMarginPercent = data[rowIndex]["SalePriceMarginPercent"];
                // salePriceMarginPercent = parseFloat(salePriceMarginPercent) || 0;

                if (salePrice !== null && salePrice !== '') {
                    salePrice = parseFloat(salePrice) || 0;
                }
                if (salePriceMarginPercent !== null && salePriceMarginPercent !== '') {
                    salePriceMarginPercent = parseFloat(salePriceMarginPercent) || 0;
                }
                if (salePrice !== 0) {
                    if (salePriceMarginPercent != 0) {
                        //calculate sale price
                        salePrice = calculateSellingPrice(clientCost, salePriceMarginPercent);
                        //set sale price to data
                        if (salePrice !== null && salePrice !== '') {
                            salePrice = parseFloat(salePrice) || 0;
                        }
                        if (salePrice > 0) {
                            data[rowIndex]["SalePrice"] = FormatAmount(salePrice);
                        }
                    }
                    else if (salePrice > 0) {
                        //calculate sale price margin percent
                        salePriceMarginPercent = calculateGrossMarginPercent(clientCost, salePrice);
                        salePriceMarginPercent = parseFloat(salePriceMarginPercent) || 0;
                        //set sale price margin percent to data
                        if (salePriceMarginPercent != 0) {
                            data[rowIndex]["SalePriceMarginPercent"] = FormatAmount(salePriceMarginPercent);
                        }
                    }
                }
                //calculate quantity discounts
                var processedDiscountIndex = [];
                for (var c in columns) {
                    if (columns.hasOwnProperty(c)) {
                        var colTitle = columns[c].title;

                        //check if it's a quantity/discount column
                        if (colTitle.match(/[a-zA-z]+/)) {

                            //ensure column starts with letters and ends with numbers
                            if (colTitle.match(/\d+/) && colTitle.endsWith(colTitle.match(/\d+/)[0])) {
                                //ending number is pricing level #
                                var k = colTitle.match(/\d+/)[0];

                                //only process not-processed quantity discounts
                                if (processedDiscountIndex.indexOf(k) === -1) {
                                    //get header of pricing level margin #
                                    var marginPricingLevel = data[rowIndex]["Margin" + k];
                                    var discountPricingLevel = data[rowIndex]["Discount" + k];
                                    if (marginPricingLevel) {
                                        //calculate pricing level selling price
                                        var sellingPriceLevel =
                                            FormatAmount(calculateSellingPrice(clientCost, marginPricingLevel));
                                        //calculate pricing level discount
                                        discountPricingLevel = parseFloat(sellingPriceLevel) - parseFloat(clientCost);
                                        //set pricing level discount
                                        if (discountPricingLevel)
                                            data[rowIndex]["Discount" + k] = FormatAmount(discountPricingLevel);

                                        //mark as processed
                                        processedDiscountIndex.push(k);
                                    }
                                    else if (discountPricingLevel) {
                                        // if discount selected - calculate margin
                                        var calculatedMargin = convertToGrossMarginPercent(clientCost, discountPricingLevel);
                                        calculatedMargin = parseFloat(calculatedMargin) || 0;
                                        if (calculatedMargin > 0) {
                                            data[rowIndex]["Margin" + k] = FormatAmount(calculatedMargin);
                                        }

                                        //mark as processed
                                        processedDiscountIndex.push(k);
                                    }

                                    //get header of purchasing level margin #
                                    var marginPurchasingLevel = data[rowIndex]["VMargin" + k];
                                    var discountPurchasingLevel = data[rowIndex]["VDiscount" + k];
                                    if (marginPurchasingLevel) {
                                        //calculate purchasing level selling price
                                        var sellingPriceLevel =
                                            FormatAmount(calculateSellingPrice(clientCost, marginPurchasingLevel));
                                        //calculate purchasing level discount
                                        discountPurchasingLevel = parseFloat(sellingPriceLevel) - parseFloat(clientCost);
                                        //set purchasing level discount
                                        if (discountPurchasingLevel)
                                            data[rowIndex]["VDiscount" + k] = FormatAmount(discountPurchasingLevel);

                                        //mark as processed
                                        processedDiscountIndex.push(k);
                                    }
                                    else if (discountPurchasingLevel) {
                                        // if discount selected - calculate margin
                                        var calculatedMargin = convertToGrossMarginPercent(clientCost, discountPurchasingLevel);
                                        calculatedMargin = parseFloat(calculatedMargin) || 0;
                                        if (calculatedMargin > 0) {
                                            data[rowIndex]["VMargin" + k] = FormatAmount(calculatedMargin);
                                        }

                                        //mark as processed
                                        processedDiscountIndex.push(k);
                                    }
                                }
                            }
                        }
                    }
                }

                //SalePriceRule(data, rowIndex);
            }


            //calculate cost per lot if orderBy = L
            var serverOrderBy = data[rowIndex]["OrderBy"];
            if (serverOrderBy === "L") {
                var quantityPerLot = data[rowIndex]["QuantityPerLot"];
                var costPerLot = data[rowIndex]["CostPerLot"];

                if (quantityPerLot == null || (quantityPerLot != null && parseFloat(quantityPerLot) == 0)) {
                    errors.push({
                        RowIndex: rowIndex,
                        Message: "Order By Lot requires quantity per lot"
                    });
                    //alertmsg('red', '{0}. Order By Lot requires quantity per lot. Fix the data and import again.'.format(rowIndex + 1));
                    //return data;
                }

                //if (clientCost && !costPerLot) {
                //    var newCostPerLot = clientCost * quantityPerLot;
                //    data[rowIndex]["CostPerLot"] = newCostPerLot;
                //} else if(!clientCost && costPerLot) {

                if (vendorCost && !costPerLot) {
                    var newCostPerLot = vendorCost * quantityPerLot;
                    newCostPerLot = parseFloat(newCostPerLot) || 0;
                    if ((parseFloat(data[rowIndex]["CostPerLot"]) || 0) === 0 && newCostPerLot > 0) {
                        data[rowIndex]["CostPerLot"] = FormatAmount(newCostPerLot);
                        costPerLot = newCostPerLot;
                    }
                }

                var newCost = costPerLot / quantityPerLot;
                newCost = parseFloat(newCost) || 0;
                if ((parseFloat(data[rowIndex]["Cost"]) || 0) === 0 && newCost > 0) {
                    data[rowIndex]["Cost"] = newCost;

                    if (costOption.toLowerCase() === "auto calculate cost") {
                        if (fifoCost > cost) {
                            newCost = fifoCost;
                        }
                    }

                    else if (costOption.toLowerCase() === "fifo cost") {
                        if (fifoCost > 0) {
                            newCost = fifoCost;
                        }
                    }

                    if (marginLocked === 'true') {

                        sellingPrice = calculateSellingPrice(newCost, marginPricing);
                        sellingPrice = parseFloat(sellingPrice) || 0;
                        if (sellingPrice > 0)
                            data[rowIndex]["Price"] = FormatAmount(sellingPrice);

                        discount = sellingPrice - newCost;
                        discount = parseFloat(discount) || 0;
                        if (discount > 0)
                            data[rowIndex]["Discount"] = FormatAmount(discount);
                    }

                    else {
                        var marginPercent = calculateGrossMarginPercentFromSellingPrice(newCost, sellingPrice);
                        if (marginPercent > 0) {
                            data[rowIndex]["Margin"] = FormatAmount(marginPercent);
                        }

                    }

                }

                // GetPricingLevels from clientSide table
                var pricingLevels = 0;
                var columnHeaders = columns;
                $.grep(columnHeaders,
                    function (n, i) {
                        var title = n.title;
                        if (title.indexOf("Quantity") >= 0) {
                            //including pricing Quantity
                            // this is compentiate in loop less than operator
                            pricingLevels++;
                        }

                    });

                var cost = newCost || 0;

                if (costOption.toLowerCase() === "auto calculate cost") {
                    if (fifoCost > cost) {
                        cost = fifoCost;
                    }
                }

                else if (costOption.toLowerCase() === "fifo cost") {
                    if (fifoCost > 0) {
                        cost = fifoCost;
                    }
                }
                var fileMargin = data[rowIndex]["Margin"];
                fileMargin = parseFloat(fileMargin) || 0;
                var fileDiscount = data[rowIndex]["Discount"];
                fileDiscount = parseFloat(fileDiscount) || 0;

                if (fileMargin > 0) {
                    var fileCalculatedSellingPrice = calculateSellingPrice(cost, fileMargin);
                    fileCalculatedSellingPrice = parseFloat(fileCalculatedSellingPrice) || 0;
                    var fileCalculatedDiscount = parseFloat(fileCalculatedSellingPrice) - parseFloat(cost);
                    fileCalculatedDiscount = parseFloat(fileCalculatedDiscount) || 0;
                    if (fileCalculatedDiscount > 0) {
                        if ((parseFloat(data[rowIndex]["Discount"]) || 0) === 0)
                            data[rowIndex]["Discount"] = FormatAmount(fileCalculatedDiscount);

                        //check if file data does not have price and calculated selling price is valid
                        if ((parseFloat(data[rowIndex]["Price"]) || 0) === 0 && fileCalculatedSellingPrice > 0)
                            data[rowIndex]["Price"] = FormatAmount(fileCalculatedSellingPrice);
                    }
                } else {
                    // if discount selected - calculate margin
                    var fileCalculatedMargin = convertToGrossMarginPercent(cost, fileDiscount);
                    fileCalculatedMargin = parseFloat(fileCalculatedMargin) || 0;
                    var fileCalculatedSellingPrice = cost + fileDiscount;
                    if (fileCalculatedMargin > 0) {
                        if ((parseFloat(data[rowIndex]["Margin"]) || 0) === 0)
                            data[rowIndex]["Margin"] = FormatAmount(fileCalculatedMargin);
                    }
                    if (fileDiscount > 0 && fileCalculatedSellingPrice > 0) {
                        if ((parseFloat(data[rowIndex]["Price"]) || 0) === 0)
                            data[rowIndex]["Price"] = FormatAmount(fileCalculatedSellingPrice);
                    }
                }

                //if sale price exists, calculate sale price on cost change
                var salePrice = data[rowIndex]["SalePrice"];
                if (salePrice !== null && salePrice !== '') {
                    salePrice = parseFloat(salePrice) || 0;
                }
                if (salePrice > 0) {
                    var salePriceMarginPercent = data[rowIndex]["SalePriceMarginPercent"];
                    var newSalePrice = calculateSellingPrice(cost, salePriceMarginPercent);
                    newSalePrice = parseFloat(newSalePrice) || 0;
                    //ensure that new sale price meets condition to change
                    if (!isNaN(parseFloat(newSalePrice)) && newSalePrice > 0 &&
                        parseFloat(salePrice) !== parseFloat(newSalePrice)) {
                        data[rowIndex]["SalePrice"] = FormatAmount(newSalePrice);
                    }
                }

                //if sale price exists, calculate sale price margin percent
                var salePrice = data[rowIndex]["SalePrice"];
                if (salePrice !== null && salePrice !== '') {
                    salePrice = parseFloat(salePrice) || 0;
                }

                var salePriceMarginPercent = data[rowIndex]["SalePriceMarginPercent"];
                if (salePriceMarginPercent !== null && salePriceMarginPercent !== '') {
                    salePriceMarginPercent = parseFloat(salePriceMarginPercent) || 0;
                }

                if (salePriceMarginPercent != 0) {
                    //calculate sale price
                    salePrice = calculateSellingPrice(cost, salePriceMarginPercent);
                    if (salePrice !== null && salePrice !== '') {
                        salePrice = parseFloat(salePrice) || 0;
                    }
                    //set sale price to data
                    if (salePrice > 0)
                        data[rowIndex]["SalePrice"] = FormatAmount(salePrice);
                }
                else if (salePrice > 0) {
                    //calculate sale price margin percent
                    salePriceMarginPercent = calculateGrossMarginPercent(clientCost, salePrice);
                    if (salePriceMarginPercent !== null && salePriceMarginPercent !== '') {
                        salePriceMarginPercent = parseFloat(salePriceMarginPercent) || 0;
                    }
                    //set sale price margin percent to data
                    if (salePriceMarginPercent != 0)
                        data[rowIndex]["SalePriceMarginPercent"] = FormatAmount(salePriceMarginPercent);
                }

                //calculate pricing levels on cost change
                for (var pricingLevelCounter = 1;
                    pricingLevelCounter <= pricingLevels;
                    pricingLevelCounter++) {
                    var marginPricingLevel = data[rowIndex]["Margin" + pricingLevelCounter];
                    var discountPricingLevel = data[rowIndex]["Discount" + pricingLevelCounter];

                    if (marginPricingLevel) {
                        var sellingPriceLevel = calculateSellingPrice(cost, marginPricingLevel);
                        var discountLevel = parseFloat(sellingPriceLevel) - parseFloat(cost);
                        discountLevel = parseFloat(discountLevel) || 0;
                        if (discountLevel > 0) {
                            data[rowIndex]["Discount" + pricingLevelCounter] = FormatAmount(discountLevel);
                            //hot.setDataAtRowProp(rowIndex, "Discount" + pricingLevelCounter, FormatAmount(discountLevel));
                        }
                    } else {
                        // if discount selected - calculate margin
                        var calculatedMargin = convertToGrossMarginPercent(cost, discountPricingLevel);
                        var MarginL = "Margin" + pricingLevelCounter;
                        calculatedMargin = parseFloat(calculatedMargin) || 0;
                        if (calculatedMargin > 0) {
                            data[rowIndex]["Margin" + pricingLevelCounter] = FormatAmount(calculatedMargin);
                        }
                    }
                }

                //}
            }




            //column header changes on autocalculate event

            if (marginLocked !== '') {
                //if sale price exists, calculate sale price margin percent
                // var sellingPriceFromImport = data[rowIndex]["Selling Price"];//SKYG-822
                var sellingPriceFromImport = data[rowIndex]["Unit Price"];
                if (sellingPriceFromImport === undefined) {
                    sellingPriceFromImport = data[rowIndex]["UnitPrice"];
                }
                if (sellingPriceFromImport !== undefined) {

                    var salePrice1 = data[rowIndex]["Sale Price"];
                    var salePriceMarginPercent1 = data[rowIndex]["Sale Price Margin"];
                    //isNaN(parseFloat(salePrice1) && isNaN(salePriceMarginPercent1))
                    //continue;
                    if (salePrice1 !== null && salePrice1 !== '') {
                        salePrice1 = parseFloat(salePrice1) || 0;
                    }
                    if (salePriceMarginPercent1 !== null && salePriceMarginPercent1 !== '') {
                        salePriceMarginPercent1 = parseFloat(salePriceMarginPercent1) || 0;
                    }

                    if (salePrice1 !== 0) {
                        //if (marginLocked === 'true' || salePrice1 === 0 ) {
                        if (salePriceMarginPercent1 != 0 && salePriceMarginPercent1 !== null) {
                            salePrice1 = calculateSellingPrice(clientCost, salePriceMarginPercent1);
                            //set sale price to data
                            salePrice1 = parseFloat(salePrice1) || 0;
                            if (salePrice1 > 0) {
                                data[rowIndex]["Sale Price"] = FormatAmount(salePrice1);
                            }
                        }

                        else {
                            if (salePrice1 > 0 && clientCost > 0) {
                                //calculate sale price margin percent
                                salePriceMarginPercent1 = calculateGrossMarginPercent(clientCost, salePrice1);
                                salePriceMarginPercent1 = parseFloat(salePriceMarginPercent1) || 0;
                                //set sale price margin percent to data
                                data[rowIndex]["Sale Price Margin"] = FormatAmount(salePriceMarginPercent1);
                                //if (salePriceMarginPercent1 > 0 && salePriceMarginPercent1 < 100) {
                                //    data[rowIndex]["Sale Price Margin"] = FormatAmount(salePriceMarginPercent1);
                                //}
                            }
                        }
                    }


                    //  calculate quantity discounts
                    var processedDiscountIndex = [];
                    for (var c in columns) {
                        if (columns.hasOwnProperty(c)) {
                            var colTitle = columns[c].title;

                            //check if it's a quantity/discount column
                            if (colTitle.match(/[a-zA-z]+/)) {

                                //ensure column starts with letters and ends with numbers
                                if (colTitle.match(/\d+/) && colTitle.endsWith(colTitle.match(/\d+/)[0])) {
                                    //ending number is pricing level #
                                    var k = colTitle.match(/\d+/)[0];

                                    //only process not-processed quantity discounts
                                    if (processedDiscountIndex.indexOf(k) === -1) {
                                        //get header of pricing level margin #
                                        var marginPricingLevel1 = data[rowIndex]["Margin Level " + k];
                                        var sellingPricePricingLevel1 = data[rowIndex]["Price Level " + k];

                                        sellingPricePricingLevel1 = parseFloat(sellingPricePricingLevel1) || 0;
                                        if (sellingPricePricingLevel1 === 0) {
                                            if (marginPricingLevel1) {
                                                //calculate pricing level selling price
                                                var sellingPriceLevel =
                                                    FormatAmount(calculateSellingPrice(clientCost, marginPricingLevel1));
                                                if (sellingPriceLevel)
                                                    data[rowIndex]["Price Level " + k] = sellingPriceLevel;
                                                //mark as processed
                                                processedDiscountIndex.push(k);
                                            }
                                        }
                                        if (marginLocked === 'true') {
                                            if (marginPricingLevel1) {
                                                //calculate pricing level selling price
                                                var sellingPriceLevel =
                                                    FormatAmount(calculateSellingPrice(clientCost, marginPricingLevel1));
                                                if (sellingPriceLevel)
                                                    data[rowIndex]["Price Level " + k] = sellingPriceLevel;
                                                //mark as processed
                                                processedDiscountIndex.push(k);
                                            }
                                            else if (sellingPricePricingLevel1) {
                                                // if discount selected - calculate margin
                                                var calculatedMargin1 = calculateGrossMarginPercentFromSellingPrice(clientCost, sellingPricePricingLevel1);
                                                calculatedMargin1 = parseFloat(calculatedMargin1) || 0;
                                                if (calculatedMargin1 !== 0) {
                                                    data[rowIndex]["Margin Level " + k] = FormatAmount(calculatedMargin1);
                                                }

                                                //mark as processed
                                                processedDiscountIndex.push(k);
                                            }
                                        }

                                        else {

                                            if (sellingPricePricingLevel1) {
                                                // if discount selected - calculate margin
                                                var calculatedMargin1 = calculateGrossMarginPercentFromSellingPrice(clientCost, sellingPricePricingLevel1);
                                                calculatedMargin1 = parseFloat(calculatedMargin1) || 0;
                                                if (calculatedMargin1 !== 0) {
                                                    data[rowIndex]["Margin Level " + k] = FormatAmount(calculatedMargin1);
                                                }

                                                //mark as processed
                                                processedDiscountIndex.push(k);
                                            }



                                        }

                                        //get header of purchasing level margin #
                                        var marginPurchasingLevel1 = data[rowIndex]["VMargin Level " + k];
                                        marginPurchasingLevel1 = parseFloat(marginPurchasingLevel1) || 0;


                                        var sellingPricePurchasingLevel1 = data[rowIndex]["VPrice Level " + k];
                                        if (marginLocked === 'true') {
                                            if (marginPurchasingLevel1 !== 0) {
                                                //calculate purchasing level selling price
                                                var sellingPriceLevel1 =
                                                    FormatAmount(calculateSellingPrice(clientCost, marginPurchasingLevel1));
                                                //calculate purchasing level discount
                                                // discountPurchasingLevel1 = parseFloat(sellingPriceLevel1) - parseFloat(clientCost);
                                                //set purchasing level discount
                                                //if (discountPurchasingLevel1)
                                                //data[rowIndex]["VDiscount Level " + k] = FormatAmount(discountPurchasingLevel1);

                                                if (sellingPriceLevel1 !== 0) {
                                                    data[rowIndex]["VPrice Level " + k] = FormatAmount(sellingPriceLevel1);
                                                }

                                                //mark as processed
                                                processedDiscountIndex.push(k);
                                            }
                                            else if (sellingPricePurchasingLevel1 !== 0) {
                                                // if discount selected - calculate margin
                                                var calculatedMargin1 = calculateGrossMarginPercentFromSellingPrice(clientCost, sellingPricePurchasingLevel1);
                                                calculatedMargin1 = parseFloat(calculatedMargin1) || 0;
                                                if (calculatedMargin1 !== 0) {
                                                    data[rowIndex]["VMargin Level " + k] = FormatAmount(calculatedMargin1);
                                                }

                                                //mark as processed
                                                processedDiscountIndex.push(k);
                                            }
                                        }

                                        else if (sellingPricePurchasingLevel1 !== 0) {
                                            // if discount selected - calculate margin
                                            var calculatedMargin1 = calculateGrossMarginPercentFromSellingPrice(clientCost, sellingPricePurchasingLevel1);
                                            calculatedMargin1 = parseFloat(calculatedMargin1) || 0;
                                            if (calculatedMargin1 !== 0) {
                                                data[rowIndex]["VMargin Level " + k] = FormatAmount(calculatedMargin1);
                                            }

                                            //mark as processed
                                            processedDiscountIndex.push(k);
                                        }

                                    }
                                }
                            }
                        }
                    }

                }
            }


            //calculate cost per lot if orderBy = L
            var serverOrderBy1 = data[rowIndex]["Order By"];
            if ((serverOrderBy1 || '').toLowerCase() === "l") {
                var quantityPerLot1 = data[rowIndex]["Quantity Per Lot"];
                var costPerLot1 = data[rowIndex]["Cost Per Lot"];

                if (quantityPerLot1 == null || (quantityPerLot1 != null && parseFloat(quantityPerLot1) == 0)) {
                    errors.push({
                        RowIndex: rowIndex,
                        Message: "Order By Lot requires quantity per lot"
                    });
                    //alertmsg('red', '{0}. Order By Lot requires quantity per lot. Fix the data and import again.'.format(rowIndex + 1));
                    //return data;
                }


                if (vendorCost && !costPerLot1) {
                    var newCostPerLot = vendorCost * quantityPerLot1;
                    newCostPerLot = parseFloat(newCostPerLot) || 0;
                    if ((parseFloat(data[rowIndex]["Cost Per Lot"]) || 0) === 0 && newCostPerLot > 0) {
                        data[rowIndex]["Cost Per Lot"] = FormatAmount(newCostPerLot);
                        costPerLot1 = newCostPerLot;
                    }
                }

                var newCost = costPerLot1 / quantityPerLot1;
                newCost = parseFloat(newCost) || 0;

                if ((parseFloat(data[rowIndex]["Cost"]) || 0) === 0 && newCost > 0) {
                    data[rowIndex]["Cost"] = newCost;


                    if (costOption.toLowerCase() === "auto calculate cost") {
                        if (fifoCost > newCost) {
                            newCost = fifoCost;
                        }
                    }

                    else if (costOption.toLowerCase() === "fifo cost") {
                        if (fifoCost > 0) {
                            newCost = fifoCost;
                        }
                    }

                    if (marginLocked !== '') {
                        if (marginLocked == 'true') {

                            sellingPrice1 = calculateSellingPrice(newCost, marginPricing);
                            sellingPrice1 = parseFloat(sellingPrice1) || 0;
                            if (sellingPrice1 > 0)
                                //data[rowIndex]["Selling Price"] = FormatAmount(sellingPrice1);//SKYG-822
                                data[rowIndex]["Unit Price"] = FormatAmount(sellingPrice1);
                            data[rowIndex]["UnitPrice"] = FormatAmount(sellingPrice1);
                            discount1 = sellingPrice1 - newCost;
                            discount1 = parseFloat(discount1) || 0;
                            if (discount1 > 0)
                                data[rowIndex]["Discount"] = FormatAmount(discount1);
                        }

                        else {
                            sellingPriceFromImport = parseFloat(sellingPriceFromImport);
                            if (sellingPriceFromImport === 0) {
                                sellingPriceFromImport = calculateSellingPrice(newCost, marginPricing);
                                // data[rowIndex]["Selling Price"] = sellingPriceFromImport;
                                data[rowIndex]["Unit Price"] = sellingPriceFromImport;
                                data[rowIndex]["UnitPrice"] = sellingPriceFromImport;
                            }
                            else {
                                var marginPercent = calculateGrossMarginPercentFromSellingPrice(newCost, sellingPriceFromImport);
                                if (marginPercent !== 0) {
                                    data[rowIndex]["Margin"] = marginPercent;
                                }
                                else {
                                    data[rowIndex]["Margin"] = "";
                                }
                            }

                        }
                    }

                }

                //recalculate costperlot
                var originalCost = parseFloat(data[rowIndex]["Cost"]);
                var calculatedCostPerLot = originalCost * quantityPerLot1;
                calculatedCostPerLot = parseFloat(calculatedCostPerLot);
                if (originalCost !== 0 && calculatedCostPerLot !== 0) {
                    newCost = originalCost;
                    data[rowIndex]["Cost Per Lot"] = FormatAmount(calculatedCostPerLot);
                }



                // GetPricingLevels from clientSide table
                var pricingLevels = 0;
                var columnHeaders = columns;
                $.grep(columnHeaders,
                    function (n, i) {
                        var title = n.title;
                        if (title.indexOf("Quantity") >= 0) {
                            //including pricing Quantity
                            // this is compentiate in loop less than operator
                            pricingLevels++;
                        }

                    });

                var cost = newCost || 0;

                if (costOption.toLowerCase() === "auto calculate cost") {
                    if (fifoCost > cost) {
                        cost = fifoCost;
                    }
                }

                else if (costOption.toLowerCase() === "fifo cost") {
                    if (fifoCost > 0) {
                        cost = fifoCost;
                    }
                }



                if (marginLocked !== '') {
                    var fileMargin = data[rowIndex]["Margin"];
                    fileMargin = parseFloat(fileMargin) || 0;
                    var fileDiscount = data[rowIndex]["Discount"];
                    fileDiscount = parseFloat(fileDiscount) || 0;
                    if (fileMargin > 0) {
                        var fileCalculatedSellingPrice = calculateSellingPrice(cost, fileMargin);
                        fileCalculatedSellingPrice = parseFloat(fileCalculatedSellingPrice) || 0;
                        var fileCalculatedDiscount = parseFloat(fileCalculatedSellingPrice) - parseFloat(cost);
                        fileCalculatedDiscount = parseFloat(fileCalculatedDiscount) || 0;
                        if (fileCalculatedDiscount > 0) {
                            if ((parseFloat(data[rowIndex]["Discount"]) || 0) === 0)
                                data[rowIndex]["Discount"] = FormatAmount(fileCalculatedDiscount);

                            //check if file data does not have price and calculated selling price is valid
                            //if ((parseFloat(data[rowIndex]["Selling Price"]) || 0) === 0 && fileCalculatedSellingPrice > 0)
                            //    data[rowIndex]["Selling Price"] = FormatAmount(fileCalculatedSellingPrice);//SKYG-822
                            if ((parseFloat(data[rowIndex]["Unit Price"]) || 0) === 0 && fileCalculatedSellingPrice > 0)
                                data[rowIndex]["Unit Price"] = FormatAmount(fileCalculatedSellingPrice);
                            data[rowIndex]["UnitPrice"] = FormatAmount(fileCalculatedSellingPrice);
                        }
                    } else {
                        // if discount selected - calculate margin
                        var fileCalculatedMargin = convertToGrossMarginPercent(cost, fileDiscount);
                        fileCalculatedMargin = parseFloat(fileCalculatedMargin) || 0;
                        var fileCalculatedSellingPrice = cost + fileDiscount;
                        if (fileCalculatedMargin > 0) {
                            if ((parseFloat(data[rowIndex]["Margin"]) || 0) === 0)
                                data[rowIndex]["Margin"] = FormatAmount(fileCalculatedMargin);
                        }
                        if (fileDiscount > 0 && fileCalculatedSellingPrice > 0) {
                            //if ((parseFloat(data[rowIndex]["Selling Price"]) || 0) === 0)
                            //    data[rowIndex]["Selling Price"] = FormatAmount(fileCalculatedSellingPrice);//SKYG-822

                            if ((parseFloat(data[rowIndex]["Unit Price"]) || 0) === 0)
                                data[rowIndex]["Unit Price"] = FormatAmount(fileCalculatedSellingPrice);
                            data[rowIndex]["UnitPrice"] = FormatAmount(fileCalculatedSellingPrice);
                        }
                    }

                    //if sale price exists, calculate sale price on cost change
                    salePrice = data[rowIndex]["Sale Price"];
                    if (salePrice !== null && salePrice !== '') {
                        salePrice = parseFloat(salePrice) || 0;
                    }
                    var salePriceMarginPercentage = data[rowIndex]["Sale Price Margin"];
                    if (salePriceMarginPercentage !== null && salePriceMarginPercentage !== '') {
                        salePriceMarginPercentage = parseFloat(salePriceMarginPercentage) || 0;
                    }
                    if (marginLocked === 'true') {
                        if (salePrice === 0) {
                            data[rowIndex]["Sale Price"] = salePrice;
                        }
                       else if (salePriceMarginPercent != 0) {
                            //var salePriceMarginPercent = data[rowIndex]["Sale Price Margin"];
                            var newSalePrice = calculateSellingPrice(cost, salePriceMarginPercent);
                            newSalePrice = parseFloat(newSalePrice) || 0;
                            //ensure that new sale price meets condition to change
                            if (!isNaN(parseFloat(newSalePrice)) && newSalePrice > 0 &&
                                parseFloat(salePrice) !== parseFloat(newSalePrice)) {
                                data[rowIndex]["Sale Price"] = FormatAmount(newSalePrice);
                            }
                        }
                        else if (salePrice > 0) {
                            salePriceMarginPercent = calculateGrossMarginPercent(cost, salePrice);
                            salePriceMarginPercent = parseFloat(salePriceMarginPercent) || 0;
                            //set sale price margin percent to data
                            if (salePriceMarginPercent != 0)
                                data[rowIndex]["Sale Price Margin"] = FormatAmount(salePriceMarginPercent);
                        }

                    }
                    else {
                        if (salePrice === 0) {
                            data[rowIndex]["Sale Price"] = salePrice;
                        }
                        else if (salePrice > 0) {
                            salePriceMarginPercent = calculateGrossMarginPercent(cost, salePrice);
                            salePriceMarginPercent = parseFloat(salePriceMarginPercent) || 0;
                            //set sale price margin percent to data
                            if (salePriceMarginPercent != 0 )
                                data[rowIndex]["Sale Price Margin"] = FormatAmount(salePriceMarginPercent);
                        }
                        else if (salePriceMarginPercentage != 0) {
                            var newSalePrice = calculateSellingPrice(cost, salePriceMarginPercentage);
                            newSalePrice = parseFloat(newSalePrice) || 0;
                            data[rowIndex]["Sale Price"] = FormatAmount(newSalePrice);
                        }

                    }
                    //calculate pricing levels on cost change
                    for (var pricingLevelCounter = 1;
                        pricingLevelCounter <= pricingLevels;
                        pricingLevelCounter++) {
                        var marginPricingLevel = data[rowIndex]["Margin Level " + pricingLevelCounter];
                        var discountPricingLevel = data[rowIndex]["Discount Level " + pricingLevelCounter];
                        var sellingPricePricingLevel = data[rowIndex]["Price Level " + pricingLevelCounter];

                        sellingPricePricingLevel = parseFloat(sellingPricePricingLevel) || 0;
                        marginPricingLevel = parseFloat(marginPricingLevel) || 0;
                        if (sellingPricePricingLevel === 0) {
                            if (marginPricingLevel !== 0) {
                                var sellingPriceLevel = calculateSellingPrice(cost, marginPricingLevel);
                                sellingPriceLevel = parseFloat(sellingPriceLevel) || 0;
                                if (sellingPriceLevel > 0) {
                                    data[rowIndex]["Price Level " + pricingLevelCounter] = FormatAmount(sellingPriceLevel);
                                }

                            }
                        }

                        if (marginLocked == 'true') {

                            if (marginPricingLevel !== 0) {
                                var sellingPriceLevel = calculateSellingPrice(cost, marginPricingLevel);
                                sellingPriceLevel = parseFloat(sellingPriceLevel) || 0;
                                if (sellingPriceLevel > 0) {
                                    data[rowIndex]["Price Level " + pricingLevelCounter] = FormatAmount(sellingPriceLevel);
                                }

                            }
                            else {
                                if (sellingPricePricingLevel !== 0) {
                                    // if discount selected - calculate margin
                                    var calculatedMargin = calculateGrossMarginPercentFromSellingPrice(cost, sellingPricePricingLevel);
                                    var MarginL = "Margin Level " + pricingLevelCounter;
                                    calculatedMargin = parseFloat(calculatedMargin) || 0;
                                    if (calculatedMargin > 0) {
                                        data[rowIndex]["Margin Level " + pricingLevelCounter] = FormatAmount(calculatedMargin);
                                    }
                                }
                            }
                        }

                        else {
                            if (sellingPricePricingLevel !== 0) {
                                var calculatedMargin = calculateGrossMarginPercentFromSellingPrice(cost, sellingPricePricingLevel);
                                var MarginL = "Margin Level " + pricingLevelCounter;
                                calculatedMargin = parseFloat(calculatedMargin) || 0;
                                if (calculatedMargin > 0) {
                                    data[rowIndex]["Margin Level " + pricingLevelCounter] = FormatAmount(calculatedMargin);
                                }
                            }
                        }

                        //For Vendor Purchase Level
                        if (showPurchasingLevel !== true) {
                            var marginVendorPurchaseLevel = data[rowIndex]["VMargin Level " + pricingLevelCounter];
                            var sellingPriceVendorPurchaseLevel = data[rowIndex]["VPrice Level " + pricingLevelCounter];

                            sellingPriceVendorPurchaseLevel = parseFloat(sellingPriceVendorPurchaseLevel) || 0;
                            marginVendorPurchaseLevel = parseFloat(marginVendorPurchaseLevel) || 0;
                            if (sellingPriceVendorPurchaseLevel === 0 || marginLocked === 'true') {

                                if (marginVendorPurchaseLevel) {
                                    var sellingPriceLevel = calculateSellingPrice(cost, marginVendorPurchaseLevel);
                                    sellingPriceLevel = parseFloat(sellingPriceLevel) || 0;
                                    if (sellingPriceLevel > 0) {
                                        data[rowIndex]["VPrice Level " + pricingLevelCounter] = FormatAmount(sellingPriceLevel);
                                    }

                                }
                                else {
                                    // if discount selected - calculate margin
                                    var calculatedMargin = calculateGrossMarginPercentFromSellingPrice(cost, sellingPriceVendorPurchaseLevel);
                                    var MarginL = "VMargin Level " + pricingLevelCounter;
                                    calculatedMargin = parseFloat(calculatedMargin) || 0;
                                    if (calculatedMargin > 0) {
                                        data[rowIndex]["VMargin Level " + pricingLevelCounter] = FormatAmount(calculatedMargin);
                                    }
                                }
                            }

                            else {
                                var calculatedMargin = calculateGrossMarginPercentFromSellingPrice(cost, sellingPriceVendorPurchaseLevel);
                                var MarginL = "VMargin Level " + pricingLevelCounter;
                                calculatedMargin = parseFloat(calculatedMargin) || 0;
                                if (calculatedMargin > 0) {
                                    data[rowIndex]["VMargin Level " + pricingLevelCounter] = FormatAmount(calculatedMargin);
                                }

                            }
                        }
                    }
                }


            }
            var orderByForMinOrder = data[rowIndex]["Order By"];
            var costForMinOrder = data[rowIndex]["Cost"];
            var vendorForMinOrder = data[rowIndex]["Vendor"];
            var minOrderUnit = data[rowIndex]["Minimum Order Unit"];


            if (vendorForMinOrder !== undefined && vendorForMinOrder !== "") {
                var minLineOrder = getVendorMinLineOrder(vendorForMinOrder);
                if (minLineOrder !== '') {
                    if (minOrderUnit === 'L' && orderByForMinOrder === 'L') {
                        var costPerLotForMinOrder = data[rowIndex]["Cost Per Lot"];
                        if (costPerLotForMinOrder === '') {
                            costPerLotForMinOrder = costForMinOrder * data[rowIndex]["Quantity Per Lot"];
                        }
                        data[rowIndex]["Minimum Order Quantity"] = calculateMinOrderQty(minLineOrder, costPerLotForMinOrder)

                    }
                    else {

                        data[rowIndex]["Minimum Order Quantity"] = calculateMinOrderQty(minLineOrder, costForMinOrder)

                    }

                }
            }

            var finalUnitPrice = data[rowIndex]['Unit Price'];
            if (finalUnitPrice === undefined) {
                finalUnitPrice = data[rowIndex]['UnitPrice'];
            }
            var finalCaseQty = data[rowIndex]['CaseQuantity'];
            var finalSellingPrice = calculateNewSellingPrice(finalUnitPrice, finalCaseQty);
            data[rowIndex]["Selling Price"] = finalSellingPrice;

            var finalSalePrice = data[rowIndex]['Sale Price'];
            var finalTotalSalePrice = calculateNewSellingPrice(finalSalePrice, finalCaseQty);

            data[rowIndex]["TotalSalePrice"] = finalTotalSalePrice;
            //make changes here
        }
        //column header changes on autocalculate event end
    }


    return {
        Data: data,
        Errors: errors
    };
}

//function SalePriceRule(data, rowIndex) {
//    var salePrice = data[rowIndex]["Sale Price"] === null ? '' : data[rowIndex]["Sale Price"];
//    var salePriceMargin = data[rowIndex]["Sale Price Margin"] === null ? '' : data[rowIndex]["Sale Price Margin"];
//    var selectedCost = data[rowIndex]["Selected Cost"];

//    if (salePrice === '0' || salePrice === 0) {
//        salePrice = '0.0000';
//    }
//    if (salePriceMargin === '0' || salePriceMargin === 0) {
//        salePriceMargin = '0.0000';
//    }

//    var marginLocked = (data[rowIndex]["Margin Locked"] || '').toLowerCase();
//    if (marginLocked === 'yes' || marginLocked === '1') {
//        marginLocked = 'true';
//    }
//    else {
//        marginLocked = 'false';
//    }
//    if (marginLocked !== '') {
//        if ((salePrice === '0.0000' && (salePriceMargin === '' || salePriceMargin === 'undefined')
//            || (salePrice === '0.0000' && salePriceMargin === '0.0000')
//            || (salePriceMargin > '0.0000' && salePrice === '0.0000')
//            || (salePriceMargin === '{blank}' && salePrice === '0.0000'))) {
//            alertmsg('red', 'Sale Price may not equal zero.', -1);
//            salePriceErrorArr.push({ rowIndex: rowIndex, salePriceError: true });
//            return;

//        }


//        if (salePriceMargin === '0.0000' && (salePrice === '' || salePrice === 'undefined')) {
//            data[rowIndex]["Sale Price"] = FormatAmount(data[rowIndex]["Selected Cost"]);
//        }

//        if (salePriceMargin === '0.0000' && salePrice > '0.0000') {
//            data[rowIndex]["Sale Price"] = FormatAmount(data[rowIndex]["Selected Cost"]);
//        }

//        if (salePrice === '{blank}' && salePriceMargin === '{blank}') {
//            isSalePriceDisabled = true;

//        }

//        if ((salePrice === '' || salePrice === 'undefined') && salePriceMargin === '{blank}') {
//            isSalePriceDisabled = true;
//        }
//        else if (salePrice === '{blank}' && (salePriceMargin === '' || salePriceMargin === 'undefined')) {
//            isSalePriceDisabled = true;
//        }
//        else if (salePrice > '0.0000' && salePriceMargin === '{blank}') {
//            isSalePriceDisabled = true;
//        }
//        else if (salePriceMargin > '0.0000' && salePrice === '{blank}') {
//            isSalePriceDisabled = true;
//        }
//        else {
//            isSalePriceDisabled = false;
//        }

//        //if ((salePrice === '' || salePrice === 'undefined') && (salePriceMargin === '' || salePriceMargin === 'undefined') && marginLocked === 'true') {

//        //    data[rowIndex]["Sale Price"] = FormatAmount(calculateSellingPrice(selectedCost, salePriceMargin));
//        //}
//        //else if ((salePrice === '' || salePrice === 'undefined') && (salePriceMargin === '' || salePriceMargin === 'undefined') && marginLocked === 'false') {
//        //    data[rowIndex]["Sale Price Margin"] = FormatAmount(calculateGrossMarginPercent(selectedCost, salePrice));
//        //}

//        if (salePriceMargin > '0.0000' && (salePrice === '' || salePrice === 'undefined')) {
//            data[rowIndex]["Sale Price"] = FormatAmount(calculateSellingPrice(selectedCost, salePriceMargin));
//        }
//        if ((salePriceMargin === '' || salePriceMargin === 'undefined') && salePrice > '0.0000') {
//            data[rowIndex]["Sale Price Margin"] = FormatAmount(calculateGrossMarginPercent(selectedCost, salePrice));
//        }

//        disabledSalePrice.push({ rowIndex: rowIndex, isSalePriceDisabled: isSalePriceDisabled });

//    }


//}

function buildConfig() { //old name: buildConfig
    return {
        delimiter: "",
        newline: "",
        header: true,
        dynamicTyping: "",
        preview: parseInt(0),
        step: undefined,
        encoding: "",
        worker: "",
        comments: "",
        complete: completeFn,
        error: errorFn,
        download: "",
        fastMode: "",
        skipEmptyLines: true,
        chunk: undefined,
        beforeFirstChunk: undefined,
    };

    function getLineEnding() {

        if ($('#newline-n').is(':checked'))
            return "\n";
        else if ($('#newline-r').is(':checked'))
            return "\r";
        else if ($('#newline-rn').is(':checked'))
            return "\r\n";
        else
            return "";
    }
}

function stepFn(results, parserHandle) {
    stepped++;
    rows += results.data.length;

    parser = parserHandle;

    if (pauseChecked) {
        //console.log(results, results.data[0]);
        parserHandle.pause();
        return;
    }

    if (printStepChecked) {
    }
    //console.log(results, results.data[0]);
}

function chunkFn(results, streamer, file) {
    if (!results)
        return;
    chunks++;
    rows += results.data.length;

    parser = streamer;

    if (printStepChecked)
        //console.log("Chunk data:", results.data.length, results);

        if (pauseChecked) {
            //console.log("Pausing; " + results.data.length + " rows in chunk; file:", file);
            streamer.pause();
            return;
        }
}

function errorFn(error, file) {
    console.log('errorFn');
    //console.log("ERROR:", error, file);
}

function completeFn() {
    end = performance.now();

    //clear file
    $('#file').val('');

    if (!$('#stream').prop('checked')
        && !$('#chunk').prop('checked')
        && arguments[0]
        && arguments[0].data)
        rows = arguments[0].data.length;
    var results = arguments[0];
    if (results.errors.length > 0) {
        var errorMessage = "";
        for (var i = 0; i < results.errors.length; i++) {
            console.log(results.errors[i]);
            errorMessage += "{0} (Row#{1})".format(results.errors[i].message, results.errors[i].row) + "; ";
        }
        alertmsg('red', errorMessage, -1);
        return;
    }
    var headerJson = arguments[0].meta.fields;
    // //console.log(headerJson);
    fileData = [];
    clientsideData = [];
    clientsideData = arguments;
    fileData = arguments[0].data;
    var aaa = arguments;

    columns1 = [];

    //console.log(headerJson);

    if (headerJson) {
        $.each(headerJson,
            function (key, value) {
                header.push(value);
                columns1.push({ 'title': value, 'data': value, 'type': 'text' });

            });
    } else {
        for (var i = 1; i <= arguments[0].data[0].length; i++) {
            var value = "Column" + i;
            header.push(value);
            columns1.push({ 'title': value, 'type': 'text' });
        }
    }
}

var contextMenuSimple = {
    items: {
        'row_above': {},
        'row_below': {},
        'remove_row': {},
        'hsep1': "---------",
        'undo': {},
        'redo': {},
        'hsep3': "---------",
        //'make_read_only': {},
        'alignment': {}
    }
};

function normalizeAmount(value) {
    if (value == null)
        return 0;

    if (!$.isNumeric(value))
        return value;

    return value.toString().replace(/$/gi, '').replace(/,/gi, '').replace(/%/gi, '');

}

var getHandsontableTypeFromCustomFieldDataType = function (customFieldDataType, allowedvalues) {
    console.log(allowedvalues);
    if (allowedvalues) {
        switch (allowedvalues.toLowerCase()) {
            case '"no","yes"':
                return "dropdown"; //'source': '["Yes","No"]'
                break;
            default:
                break;
        }
    }

    if (customFieldDataType) {
        //68310,68311,68312,68314 -- int, float, varchar(max), bit
        switch (customFieldDataType.toLowerCase()) {
            case "int":
                return "numeric"; //format 0,0
                break;
            case "float":
                return "numeric"; //format 0,0.0000
                break;
            case "varchar(max)":
                return "text";
                break;
            case "datetime":
                return "date";
                break;
            //case "bit":
            //    return "checkbox";
            //    break;
            default:
                return "text";
                break;
        }
    }

    return "text";

};

var getHandsontableTypeFromCustomFieldDataType2 = function (customFieldDataType, allowedvalues, columnName) {
    columnName = columnName.trim();
    var returnSetting = {};
    returnSetting.type = "text";


    if (customFieldDataType) {
        //68310,68311,68312,68314 -- int, float, varchar(max), bit
        switch (customFieldDataType.toLowerCase()) {
            case "int":
                returnSetting.type = "numeric";
                returnSetting.format = "0,0";
                break;
            case "float":
                returnSetting.type = "numeric";

                columnName = columnName || "nothing";
                columnName = columnName.toLowerCase();

                if (columnName === "margin" || columnName === "marginpercent" || columnName === "salepricemarginpercent" || columnName === "sale price margin" || columnName.indexOf('margin') >= 0 && columnName.match(/\d+$/))
                    returnSetting.format = "0,0.0000 %";
                //else if (columnName === "selling price" || columnName === "discount" || columnName === "price" || columnName === "sale price" || columnName === "saleprice" || columnName === "cost" || columnName === "costperlot" || columnName === "cost per lot" || columnName.indexOf('price') >= 0 || columnName.indexOf('discount') >= 0 && columnName.match(/\d+$/))
                //    returnSetting.format = "$ 0,0.0000";//SKYG-822
                else if (columnName === "unit price" || columnName === "selling price" || columnName === "totalsaleprice" || columnName === "discount" || columnName === "price" || columnName === "sale price" || columnName === "saleprice" || columnName === "cost" || columnName === "costperlot" || columnName === "cost per lot" || columnName.indexOf('price') >= 0 || columnName.indexOf('discount') >= 0 && columnName.match(/\d+$/))
                    returnSetting.format = "$ 0,0.0000";
                else
                    returnSetting.format = "0,0.0000";
                break;
            case "varchar(max)":
                returnSetting.type = "text";
                break;
            case "datetime":
                returnSetting.dateFormat = "MM/DD/YYYY";
                returnSetting.type = "date";
                break;
            default:
                returnSetting.type = "text";
                break;
        }
    }

    if (allowedvalues) {

      
        if (allowedvalues.length > 0) {
           // allowedvalues[allowedvalues.length] = null;
            returnSetting.type = "dropdown";
            returnSetting.source = allowedvalues;
            returnSetting.allowInvalid = false;
        }
    }

    return returnSetting;

};

function getData(type, value, pageNumber, pagesize) {
    SGloading('Loading');
    var reqType = type;
    var reqValue = value;
    var reqPageNumber = pageNumber;
    var reqPageSize = pagesize;
    $.ajax({
        type: 'POST',
        url: urlgetBulkPricing,
        data: {
            'type': reqType,
            'value': reqValue,
            'pageNumber': reqPageNumber,
            'pageSize': reqPageSize
        },
        beforeSend: function () {

            SGloading('Loading');
            $('.htCommentCell').removeClass('htCommentCell');
            $('.htInvalid').removeClass('htInvalid');

        },
        success: function (response) {

            var data = fixJsonQuote(response.data);
            var columns = fixJsonQuote(response.col);
            //console.log(data);
            dataSchema = getDefaultHandsonValue(columns);

            var settings = {
                data: data,
                colHeaders: true,
                rowHeaders: true,
                //stretchH: 'all',
                columnSorting: false,
                search: true, /////
                fixedRowsTop: 0,
                fixedColumnsLeft: 3,//3 SKYG-819
                contextMenu: contextMenu,////
                licenseKey: 'non-commercial-and-evaluation',
                columns: columns,////
                comments: true,
                minSpareRows: 0,
                manualColumnResize: true,
                //manualColumnMove: true, 
                dataSchema: dataSchema,
                //autoColumnSize: true,
                afterCreateRow: function (index, amount) {
                    data.splice(index, amount);
                },
                afterRemoveRow: function (index, amount) {

                    setTimeout(KitCostDisabled, 500);
                },
                afterSetCellMeta: function (row, col, key, val) {
                    setTimeout(KitCostDisabled, 500);
                },
                afterChange: afterChange,
                afterGetColHeader: afterGetColHeader


            };

            if (flag !== 0) {
                flag = 0;
                hot.destroy();
            }

            hot = new Handsontable(container, settings);


            // cost disabled for Kit 
            setTimeout(KitCostDisabled,
                2000);

            hot.render();
            // end of cost disabled for kit

            flag = 1;

        },
        complete: function () {

            SGloadingRemove();

            //reset amount/margin selection to amount
            $('#chk-showmargin').prop('checked', true);

        },
        error: function (xhr, status, error) {

            alert(status + ' ' + error);
        }
    });

};

function getBulkVendorProductData(type, value, pageNumber, pagesize, isBulkPricing) {
    var reqType = type;
    var reqValue = value;
    var reqPageNumber = pageNumber;
    var reqPageSize = pagesize;
    $.ajax({
        type: 'POST',
        url: getStagingMetadataId,
        data: {
            'type': reqType,
            'value': reqValue,
            'pageNumber': reqPageNumber,
            'pageSize': reqPageSize,
            'isBulkPricing': isBulkPricing
        },
        // async:false,
        beforeSend: function () {
            SGloading('Loading');
        },
        success: function (response) {
            if (response.data.MetadataId !== "") {
                stagingMetadataId = response.data.MetadataId;
                recordCount = response.data.RecordCount;
                $.ajax({
                    type: 'POST',
                    url: getBulkDataFromStaging,
                    data: {
                        'metadataId': stagingMetadataId,
                        'pageNumber': 1,
                        'pageSize': pageLimit || 100,
                        'isBulkPricing': isBulkPricing
                    },
                    beforeSend: function () {
                        $('.htCommentCell').removeClass('htCommentCell');
                        $('.htInvalid').removeClass('htInvalid');
                    },
                    success: function (response) {

                        bulkVendorProductData = fixJsonQuote(response.data);
                        var columns = fixJsonQuote(response.col);
                        
                        columns.splice(0, 2);
                        ////console.log(data);
                        dataSchema = getDefaultHandsonValue(columns);

                        // get dynamic pagination here
                        addDynamicPaginationToBulkVendorProductGrid();

                        var settings = {
                            data: bulkVendorProductData,
                            colHeaders: true,
                            rowHeaders: true,
                            hiddenColumns: { columns: [0] },
                            //stretchH: 'all',
                            columnSorting: false,
                            search: true, /////
                            fixedRowsTop: 0,
                            fixedColumnsLeft: 3,// 3 SKYG-819
                            contextMenu: contextMenu,////
							licenseKey: 'non-commercial-and-evaluation',
                            beforeRemoveRow: function (index, amount) {
                                //console.log('beforeRemove: index: %d, amount: %d', index, amount);
                                for (var r = index; r < index + amount; r++) {
                                    stagingIdsLstToDelete.push(hot.getSettings().data[r].StagingId)
                                }

                                if (stagingIdsLstToDelete.length > 0) {
                                    concatenatedStagingIds = stagingIdsLstToDelete.join();
                                    $.ajax({
                                        type: "POST",
                                        url: "DeleteRowsFromStaging",
                                        data: { 'MetadataId': stagingMetadataId, "Ids": concatenatedStagingIds },
                                        beforeSend: function () {
                                            SGloading("Deleting");
                                        },
                                        success: function () {
                                            SGloadingRemove();

                                        }


                                    })
                                }

                            },
                            columns: columns,////
                            comments: true,
                            minSpareRows: 0,
                            manualColumnResize: true,
                            undo: false,
                            //manualColumnMove: true, 
                            //dataSchema: dataSchema,
                            //autoColumnSize: true,
                            afterCreateRow: function (index, amount) {
                                data.splice(index, amount);
                            },
                            afterRemoveRow: function (index, amount) {

                                setTimeout(KitCostDisabled, 500);
                            },
                            afterSetCellMeta: function (row, col, key, val) {
                                setTimeout(KitCostDisabled, 500);
                            },
                            afterChange: afterChange,
                            afterGetColHeader: afterGetColHeader

                        };

                        if (flag !== 0) {
                            flag = 0;
                            hot.destroy();
                        }

                        hot = new Handsontable(container, settings);
                        //var isCalculateAgain = response.isCalculateAgain;

                        //if (isCalculateAgain === 1) {
                        //    var changes = [];

                        //    for (var i = 0; i < bulkVendorProductData.length; i++) {

                        //        changes.push([i, "Cost", bulkVendorProductData[i].OldCost, bulkVendorProductData[i].Cost]);
                        //    }
                        //    afterChange(changes, "edit");
                        //}

                        // cost disabled for Kit 
                        setTimeout(KitCostDisabled,
                            2000);

                        hot.render();

                        // end of cost disabled for kit

                        flag = 1;

                    },
                    complete: function () {
                        // initialize();
                        SGloadingRemove();

                        //reset amount/margin selection to amount
                        $('#chk-showmargin').prop('checked', true);

                    },
                    error: function (xhr, status, error) {
                        SGloadingRemove();
                        alert(status + ' ' + error);
                    }
                });
            }

            else {

                alertmsg('red', 'Error loading data.', -1);
                SGloadingRemove();
            }
        },
        error: function (xhr, status, error) {
            SGloadingRemove();
            alert(status + ' ' + error);
        }

    })

}

function getBulkErrorDataFromStaging(errorMetadataId) {
    $.ajax({
        type: 'POST',
        url: getBulkDataFromStaging,
        async: false,
        data: {
            'metadataId': errorMetadataId,
            'pageNumber': 1,
            'pageSize': pageLimit || 100,
            'isBulkPricing': isBulkPricing,
            'pagination': false,
            'keySearch': $('#search_field').val() || '',
            'isErrorLoad': true
        },
        beforeSend: function () {

            SGloading('Loading');
        },
        success: function (response) {
            ErrorbulkData = fixJsonQuote(response.data);
            bulkSliceCols = JSON.parse(response.col);
            bulkSliceCols = bulkSliceCols.slice(2);
            var settings = {
                data: ErrorbulkData,
                colHeaders: true,
                rowHeaders: true,
                hiddenColumns: { columns: [0] },
                //stretchH: 'all',
                columnSorting: false,
                search: true, /////
                fixedRowsTop: 0,
                fixedColumnsLeft: 2,// 3 SKYG-819
                contextMenu: contextMenu,////
licenseKey: 'non-commercial-and-evaluation',
                beforeRemoveRow: function (index, amount) {
                    //console.log('beforeRemove: index: %d, amount: %d', index, amount);
                    for (var r = index; r < index + amount; r++) {
                        stagingIdsLstToDelete.push(hot.getSettings().data[r].StagingId)
                    }

                    if (stagingIdsLstToDelete.length > 0) {
                        concatenatedStagingIds = stagingIdsLstToDelete.join();
                        $.ajax({
                            type: "POST",
                            url: "DeleteRowsFromStaging",
                            data: { 'MetadataId': stagingMetadataId, "Ids": concatenatedStagingIds },
                            beforeSend: function () {
                                SGloading("Deleting");
                            },
                            success: function () {
                                SGloadingRemove();

                            }


                        })
                    }

                },
                columns: bulkSliceCols,////
                comments: true,
                minSpareRows: 0,
                manualColumnResize: true,
                afterChange: afterChange,
                afterGetColHeader: afterGetColHeader
            };
            hot = new Handsontable(container, settings);
            hot.render();
        },
        complete: function () {
            SGloadingRemove();


        },
        error: function (xhr, status, error) {
            alert(status + ' ' + error);
        }
    });
}

var orderByChangeOptions = function () {
    //this is order by for vendor product
    var option = {};

    var quantityPerLotColumnIndex = -1;
    var lotDescriptionColumnIndex = -1;
    var lotPartNumberColumnIndex = -1;
    var costPerLotColumnIndex = -1;
    var orderByVPColumnIndex = -1;
    var orderByPColumnIndex = -1;
    var MinimumOrderQuantityIndex = -1;

    if (hot) {
        //get corresponding columns index
        var colList = hot.getSettings().columns;
        for (var i = 0; i < colList.length; i++) {
            var thisCol = colList[i];

            if (thisCol.data == "QuantityPerLot")
                quantityPerLotColumnIndex = i;
            else if (thisCol.data == "LotDescription")
                lotDescriptionColumnIndex = i;
            else if (thisCol.data == "LotPartNumber")
                lotPartNumberColumnIndex = i;
            else if (thisCol.data == "CostPerLot")
                costPerLotColumnIndex = i;
            else if (thisCol.data == "OrderBy" && thisCol.title.indexOf("V") == 0)
                orderByVPColumnIndex = i;
            else if (thisCol.data == "OrderBy" && thisCol.title.indexOf("P") == 0)
                orderByPColumnIndex = i;
            else if (thisCol.data == "MinimumOrderQuantity")
                MinimumOrderQuantityIndex = i;
        }
    }

    option.quantityPerLotColumnIndex = quantityPerLotColumnIndex;
    option.lotDescriptionColumnIndex = lotDescriptionColumnIndex;
    option.lotPartNumberColumnIndex = lotPartNumberColumnIndex;
    option.costPerLotColumnIndex = costPerLotColumnIndex;
    option.orderByVPColumnIndex = orderByVPColumnIndex;
    option.orderByPColumnIndex = orderByPColumnIndex;
    option.MinimumOrderQuantityIndex = MinimumOrderQuantityIndex;

    return option;
}

var orderByChangeOptionsForFile = function () {
    //this is order by for vendor product
    var option = {};

    var quantityPerLotColumnIndex = -1;
    var lotDescriptionColumnIndex = -1;
    var lotPartNumberColumnIndex = -1;
    var costPerLotColumnIndex = -1;
    var orderByVPColumnIndex = -1;
    var orderByPColumnIndex = -1;
    var MinimumOrderQuantityIndex = -1;

    if (hot) {
        //get corresponding columns index
        var colList = hot.getSettings().columns;
        for (var i = 0; i < colList.length; i++) {
            var thisCol = colList[i];

            if (thisCol.data == "Quantity Per Lot")
                quantityPerLotColumnIndex = i;
            else if (thisCol.data == "Lot Description")
                lotDescriptionColumnIndex = i;
            else if (thisCol.data == "Lot Part Number")
                lotPartNumberColumnIndex = i;
            else if (thisCol.data == "Cost Per Lot")
                costPerLotColumnIndex = i;
            else if (thisCol.data == "Order By" && thisCol.title.indexOf("V") == 0)
                orderByVPColumnIndex = i;
            else if (thisCol.data == "Order By" && thisCol.title.indexOf("P") == 0)
                orderByPColumnIndex = i;
            else if (thisCol.data == "Minimum Order Quantity")
                MinimumOrderQuantityIndex = i;
        }
    }

    option.quantityPerLotColumnIndex = quantityPerLotColumnIndex;
    option.lotDescriptionColumnIndex = lotDescriptionColumnIndex;
    option.lotPartNumberColumnIndex = lotPartNumberColumnIndex;
    option.costPerLotColumnIndex = costPerLotColumnIndex;
    option.orderByVPColumnIndex = orderByVPColumnIndex;
    option.orderByPColumnIndex = orderByPColumnIndex;
    option.MinimumOrderQuantityIndex = MinimumOrderQuantityIndex;

    return option;
}


//Adjust data according to order by
var orderByChange = function (rowIndex, option, clearValue) {

    clearValue = true;

    if (hot) {

        var orderBy = hot.getDataAtProp('OrderBy')[rowIndex];

        //if order by is L, dont disable any
        if (orderBy === "L") {
            if (option.quantityPerLotColumnIndex >= 0)
                hot.setCellMeta(rowIndex, option.quantityPerLotColumnIndex, 'readOnly', false);
            if (option.lotDescriptionColumnIndex >= 0)
                hot.setCellMeta(rowIndex, option.lotDescriptionColumnIndex, 'readOnly', false);
            if (option.lotPartNumberColumnIndex >= 0)
                hot.setCellMeta(rowIndex, option.lotPartNumberColumnIndex, 'readOnly', false);
            if (option.costPerLotColumnIndex >= 0)
                hot.setCellMeta(rowIndex, option.costPerLotColumnIndex, 'readOnly', false);


        } else if (orderBy === "Q") {



            if (option.quantityPerLotColumnIndex >= 0)
                hot.setCellMeta(rowIndex, option.quantityPerLotColumnIndex, 'readOnly', false);
            if (option.lotDescriptionColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.lotDescriptionColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.lotDescriptionColumnIndex, "");
            }
            if (option.lotPartNumberColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.lotPartNumberColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.lotPartNumberColumnIndex, "");
            }
            if (option.costPerLotColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.costPerLotColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.costPerLotColumnIndex, "");
            }



            //if order by is P, disable qpl, lotdesc, lotpartno, costperlot
        } else {

            //var minOrderUnit = hot.getDataAtProp('MinimumOrderUnit')[rowIndex];
            //var minLineOrder = "";
            //minLineOrder = getVendorMinLineOrder(hot.getDataAtProp('SupplierName')[rowIndex]);
            //if (minLineOrder !== '') {

            //    hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, hot.getDataAtProp('Cost')[rowIndex])
            //    hot.setDataAtCell(rowIndex, option.MinimumOrderQuantityIndex, calculateMinOrderQty(minLineOrder, hot.getDataAtProp('Cost')[rowIndex]));
            //    hot.render();


            //}

            if (option.quantityPerLotColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.quantityPerLotColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.quantityPerLotColumnIndex, "");
            }
            if (option.lotDescriptionColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.lotDescriptionColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.lotDescriptionColumnIndex, "");
            }
            if (option.lotPartNumberColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.lotPartNumberColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.lotPartNumberColumnIndex, "");
            }
            if (option.costPerLotColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.costPerLotColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.costPerLotColumnIndex, "");
            }


        }

    }
}

var orderByChangeForFile = function (rowIndex, option, clearValue) {

    clearValue = true;

    if (hot) {

        var orderBy = hot.getDataAtProp('Order By')[rowIndex];

        //if order by is L, dont disable any
        if (orderBy === "L") {
            if (option.quantityPerLotColumnIndex >= 0)
                hot.setCellMeta(rowIndex, option.quantityPerLotColumnIndex, 'readOnly', false);
            if (option.lotDescriptionColumnIndex >= 0)
                hot.setCellMeta(rowIndex, option.lotDescriptionColumnIndex, 'readOnly', false);
            if (option.lotPartNumberColumnIndex >= 0)
                hot.setCellMeta(rowIndex, option.lotPartNumberColumnIndex, 'readOnly', false);
            if (option.costPerLotColumnIndex >= 0)
                hot.setCellMeta(rowIndex, option.costPerLotColumnIndex, 'readOnly', false);

            //if order by is Q, disable lotdesc, lotpartno, costperlot
        } else if (orderBy === "Q") {

            if (option.quantityPerLotColumnIndex >= 0)
                hot.setCellMeta(rowIndex, option.quantityPerLotColumnIndex, 'readOnly', false);
            if (option.lotDescriptionColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.lotDescriptionColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.lotDescriptionColumnIndex, "");
            }
            if (option.lotPartNumberColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.lotPartNumberColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.lotPartNumberColumnIndex, "");
            }
            if (option.costPerLotColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.costPerLotColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.costPerLotColumnIndex, "");
            }



            //if order by is P, disable qpl, lotdesc, lotpartno, costperlot
        } else {

            if (option.quantityPerLotColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.quantityPerLotColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.quantityPerLotColumnIndex, "");
            }
            if (option.lotDescriptionColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.lotDescriptionColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.lotDescriptionColumnIndex, "");
            }
            if (option.lotPartNumberColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.lotPartNumberColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.lotPartNumberColumnIndex, "");
            }
            if (option.costPerLotColumnIndex >= 0) {
                hot.setCellMeta(rowIndex, option.costPerLotColumnIndex, 'readOnly', true);
                if (clearValue)
                    hot.setDataAtCell(rowIndex, option.costPerLotColumnIndex, "");
            }


        }

    }
}



var afterChange = function (changes, source) {
    try {
        //setTimeout(KitCostDisabled, 50);

        if (changes !== null) {
            var rowOffset = 0;
            var colOffset = 0;
            isSaved = false;


            for (var i = 0; i < changes.length; i++) {
                if (changes[i] != null) {
                    // [ [row, prop, oldVal, newVal], ... ].
                    var columnsList = hot.getSettings().columns;
                    var rowIndex = changes[i][0];
                    var prop = changes[i][1];

                    var currentCost = hot.getSettings().data[rowIndex]["Cost"];
                    var currentSelectedCost = hot.getSettings().data[rowIndex]["SelectedCost"];
                    if (currentSelectedCost === undefined) {
                        currentSelectedCost = hot.getSettings().data[rowIndex]["Selected Cost"];
                    }
                    var fifoCost = isNaN(parseFloat(hot.getSettings().data[rowIndex]["FifoCost"])) ? 0 : parseFloat(hot.getSettings().data[rowIndex]["FifoCost"]);
                    if (fifoCost === 0) {
                        fifoCost = isNaN(parseFloat(hot.getSettings().data[rowIndex]["AvgFifo Cost"])) ? 0 : parseFloat(hot.getSettings().data[rowIndex]["AvgFifo Cost"]);
                    }
                    var toBeSelectedCost = currentSelectedCost;

                    //check if value can be decoded, if yes then replace the value
                    var previousValue = changes[i][2];
                    var newValue = changes[i][3];
                    var newValueDecoded = decodeHtml(newValue);
                    if (newValue != newValueDecoded) {
                        hot.getSettings().data[rowIndex][prop] = newValueDecoded;
                        hot.render();
                    }

                    //Krisha sale price
                    if (newValue === '{blank}')
                        return;



                    //if change checking is undefined, initialize them
                    if (typeof changeLevelArray[rowIndex] === "undefined") {
                        changeLevelArray[rowIndex] = "";
                    }
                    if (typeof changeLevelArrayPerLot[rowIndex] === "undefined") {
                        changeLevelArrayPerLot[rowIndex] = "";
                    }

                    //source = 'edit' for user changes
                    if (source != null && (source === 'edit' || source === 'paste')) {
                        //reset change checking
                        changeLevelArray[rowIndex] = "";
                        //reset change tracking for per lot for all other conditions except for cost changed as calculation by cost per lot change by user
                        if (changeLevelArrayPerLot[rowIndex] != "CPL," && prop != "Cost")
                            changeLevelArrayPerLot[rowIndex] = "";
                    }

                    if (prop.toLowerCase() === "qoh") {
                        var qoh = hot.getDataAtProp('QOH')[rowIndex];
                        hot.getSettings().data[rowIndex]["QOHUpdatedOn"] = getCurrentDateTime();
                        hot.getSettings().data[rowIndex]["Quantity On Hand Last Updated"] = getCurrentDateTime();
                        hot.render();

                        if (qoh === '' || qoh === 'undefined') {
                            hot.getSettings().data[rowIndex]["QOH"] = 0;
                            hot.render();
                            return;
                        }


                    }


                    if (prop.toLowerCase() == "fifocost" || prop.toLowerCase() === "avgfifo cost") {

                        hot.getSettings().data[rowIndex]["AvgFifo Cost"] = previousValue;
                        hot.render();
                        return;
                    }

                    //Cost Option Changes (FIFO)
                    if (prop.toLowerCase() == "costoption") {

                        var toBeSelectedCost = currentSelectedCost;
                        var costOption = hot.getSettings().data[rowIndex]["CostOption"];
                        if (costOption.toLowerCase() == "auto calculate cost") {

                            if (fifoCost > currentCost) {

                                toBeSelectedCost = fifoCost;
                            }

                            else if (fifoCost < currentCost) {

                                toBeSelectedCost = currentCost;
                            }

                            if (toBeSelectedCost !== currentSelectedCost) {

                                /// get cost change logic here
                                hot.getSettings().data[rowIndex]["SelectedCost"] = toBeSelectedCost;
                                CostchangeAutocalculationInGrid(toBeSelectedCost, rowIndex);

                            }
                        }

                        if (costOption.toLowerCase() == "fifo cost") {
                            if (fifoCost === 0) {
                                hot.getSettings().data[rowIndex]["CostOption"] = "Auto Calculate Cost";
                                toBeSelectedCost = currentCost;
                                hot.getSettings().data[rowIndex]["SelectedCost"] = toBeSelectedCost;
                                CostchangeAutocalculationInGrid(toBeSelectedCost, rowIndex);

                            }
                            else {
                                toBeSelectedCost = fifoCost;
                                if (toBeSelectedCost !== currentSelectedCost) {

                                    /// get cost change logic here
                                    hot.getSettings().data[rowIndex]["SelectedCost"] = toBeSelectedCost;
                                    CostchangeAutocalculationInGrid(toBeSelectedCost, rowIndex);
                                }
                            }
                        }

                        if (costOption.toLowerCase() == "vendor cost") {
                            toBeSelectedCost = currentCost;
                            if (toBeSelectedCost !== currentSelectedCost) {

                                /// get cost change logic here
                                hot.getSettings().data[rowIndex]["SelectedCost"] = toBeSelectedCost;
                                CostchangeAutocalculationInGrid(toBeSelectedCost, rowIndex);
                            }
                        }
                        hot.render();
                        return;

                    }

                    if (prop.toLowerCase() == "cost option") {

                        var toBeSelectedCost = currentSelectedCost;
                        var costOption = hot.getSettings().data[rowIndex]["Cost Option"];
                        if (costOption.toLowerCase() == "auto calculate cost") {

                            if (fifoCost > currentCost) {

                                toBeSelectedCost = fifoCost;
                            }

                            else if (fifoCost < currentCost) {

                                toBeSelectedCost = currentCost;
                            }

                            if (toBeSelectedCost !== currentSelectedCost) {

                                /// get cost change logic here
                                hot.getSettings().data[rowIndex]["Selected Cost"] = toBeSelectedCost;
                                CostchangeAutocalculationInGrid(toBeSelectedCost, rowIndex);

                            }
                        }

                        if (costOption.toLowerCase() == "fifo cost") {

                            if (fifoCost === 0) {
                                hot.getSettings().data[rowIndex]["Cost Option"] = "Auto Calculate Cost";
                                toBeSelectedCost = currentCost;
                                hot.getSettings().data[rowIndex]["Selected Cost"] = toBeSelectedCost;
                                CostchangeAutocalculationInGrid(toBeSelectedCost, rowIndex);

                            }
                            else {
                                toBeSelectedCost = fifoCost;
                                if (toBeSelectedCost !== currentSelectedCost) {

                                    /// get cost change logic here
                                    hot.getSettings().data[rowIndex]["Selected Cost"] = toBeSelectedCost;
                                    CostchangeAutocalculationInGrid(toBeSelectedCost, rowIndex);
                                }
                            }
                        }

                        if (costOption.toLowerCase() == "vendor cost") {
                            toBeSelectedCost = currentCost;
                            if (toBeSelectedCost !== currentSelectedCost) {

                                /// get cost change logic here
                                hot.getSettings().data[rowIndex]["Selected Cost"] = toBeSelectedCost;
                                CostchangeAutocalculationInGrid(toBeSelectedCost, rowIndex);
                            }
                        }

                        hot.render();
                        return;

                    }


                    //for itempoints autocalculation
                    if (prop.toLowerCase() == "shippinglength" || prop.toLowerCase() == "shippingheight" || prop.toLowerCase() == "shippingwidth") {
                        autoCalculateItemPoints(changes[i]);
                        continue;
                    }
                    if (prop.toLowerCase() == "itempoints") {
                        continue;
                    }

                    //for commoditycode autocalculation
                    if (prop.toLowerCase() == "schedule-b" || prop.toLowerCase() == "schedule_b") {
                        autocalculateCommodityCode(changes[i]);
                    }
                    if (prop.toLowerCase() == "commoditycode") {
                        continue;
                    }

                    //for yahooid autocalculation
                    if (prop.toLowerCase() == "code" || prop.toLowerCase() == "localsku" || prop.toLowerCase() == "manufacturer" || prop.toLowerCase() == "name") {
                        autoCalculateYahooId(changes[i]);
                    }

                    if (prop.toLowerCase() == "un number" || prop.toLowerCase() == "un class" || prop.toLowerCase() == "packing group") {
                        autoCalcualteHazmatType(changes[i]);
                    }

                    //for product-url autocalculation
                    if (prop.toLowerCase() == "yahooid") {
                        autocalculateProductUrl(changes[i]);
                        //autocalculateMobileLinkUrl(changes[i]);
                        autocalculateImageUrl(changes[i]);

                    }
                    if (prop.toLowerCase() == "product_url") {
                        continue;
                    }
                    if (prop.toLowerCase() == "product-url") {
                        continue;
                    }
                    //if (prop.toLowerCase() == "mobilelink") {
                    //    continue;
                    //}
                    if (prop.toLowerCase() == "image url") {
                        continue;
                    }

                    //for reorder point autocalculation
                    if (prop.toLowerCase() == "reorder point") {
                        autocalculateReorderPointUpdatedOn(changes[i]);
                    }
                    if (prop.toLowerCase() == "reorder point last updated date") {
                        var reorderPoint = hot.getDataAtProp('Reorder Point')[rowIndex];
                        if (reorderPoint != "" && newValue == "") {
                            hot.getSettings().data[rowIndex]["Reorder Point Last Updated Date"] = FormattedDate(new Date());
                            hot.render();
                        }
                        continue;
                    }

                    //for target quantity autocalculation
                    if (prop.toLowerCase() == "target quantity") {
                        autocalculateTargetQuantityUpdatedOn(changes[i]);
                    }
                    if (prop.toLowerCase() == "target quantity last updated date") {
                        var targetQuantity = hot.getDataAtProp('Target Quantity')[rowIndex];
                        if (targetQuantity != "" && newValue == "") {
                            hot.getSettings().data[rowIndex]["Target Quantity Last Updated Date"] = FormattedDate(new Date());
                            hot.render();
                        }
                        continue;
                    }

                    //for calculative fields
                    if (prop === "OrderBy") {
                        orderByChangeFlag = true;
                        var option = orderByChangeOptions();
                        orderByChange(rowIndex, option);

                        var minOrderUnit = hot.getDataAtProp('MinimumOrderUnit')[rowIndex];
                        var minLineOrder = "";
                        minLineOrder = getVendorMinLineOrder(hot.getDataAtProp('SupplierName')[rowIndex]);
                        if (minLineOrder !== '') {

                            if (newValue === 'L' && minOrderUnit === 'L') {
                                hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, hot.getDataAtProp('CostPerLot')[rowIndex])
                            }

                            else {
                                hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, hot.getDataAtProp('SelectedCost')[rowIndex])
                            }

                        }


                        hot.render();
                        continue;
                    }

                    if (prop === "Order By") {
                        orderByChangeFlagForFile = true;
                        var option = orderByChangeOptionsForFile();
                        orderByChangeForFile(rowIndex, option);

                        var minOrderUnit = hot.getDataAtProp('Minimum Order Unit')[rowIndex];
                        var minLineOrder = "";
                        minLineOrder = getVendorMinLineOrder(hot.getDataAtProp('Vendor')[rowIndex]);
                        if (minLineOrder !== '') {

                            if (newValue === 'L' && minOrderUnit === 'L') {
                                hot.getSettings().data[rowIndex]["Minimum Order Quantity"] = calculateMinOrderQty(minLineOrder, hot.getDataAtProp('Cost Per Lot')[rowIndex])
                            }

                            else {
                                hot.getSettings().data[rowIndex]["Minimum Order Quantity"] = calculateMinOrderQty(minLineOrder, hot.getDataAtProp('Cost')[rowIndex])
                            }

                        }


                        hot.render();
                        continue;
                    }

                    if (prop === "SetToPrimary" && newValue === "Yes") {
                        wasSetPrimary = true;
                        prop = "Cost";
                        newValue = hot.getDataAtProp('Cost')[rowIndex];
                        //hot.getDataAtProp('Cost')[rowIndex].change;
                    }
                    else {
                        wasSetPrimary = false;
                    }

                    if (prop === "IsMarginLocked" && (newValue.toLowerCase() === "true" || newValue.toLowerCase() === "yes")) {

                        var marginPercent = hot.getDataAtProp('Margin')[rowIndex];
                        if (marginPercent === 0) {
                            hot.getSettings().data[rowIndex]["IsMarginLocked"] = "No";
                            alertmsg('red', 'Cannot lock margin when margin percentage is zero', -1);
                            hot.render();
                            return;
                        }
                    }

                    if (prop === "MinimumOrderUnit") {

                        var minLineOrder = "";
                        SGloading("Loading");
                        minLineOrder = getVendorMinLineOrder(hot.getDataAtProp('SupplierName')[rowIndex]);
                        if (minLineOrder !== '') {

                            var orderBy = hot.getDataAtProp('OrderBy')[rowIndex];
                            if (newValue === 'L' && orderBy === 'L') {
                                hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, hot.getSettings().data[rowIndex]["CostPerLot"])
                            }

                            else {
                                hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, hot.getSettings().data[rowIndex]["SelectedCost"])
                            }
                        }
                        SGloadingRemove();
                        hot.render();
                        return;


                    }

                    if (prop === "Minimum Order Unit") {

                        var minLineOrder = "";
                        SGloading("Loading");
                        minLineOrder = getVendorMinLineOrder(hot.getDataAtProp('Vendor')[rowIndex]);
                        if (minLineOrder !== '') {

                            var orderBy = hot.getDataAtProp('Order By')[rowIndex];
                            if (newValue === 'L' && orderBy === 'L') {
                                hot.getSettings().data[rowIndex]["Minimum Order Quantity"] = calculateMinOrderQty(minLineOrder, hot.getSettings().data[rowIndex]["Cost Per Lot"])
                            }

                            else {
                                hot.getSettings().data[rowIndex]["Minimum Order Quantity"] = calculateMinOrderQty(minLineOrder, hot.getSettings().data[rowIndex]["Cost"])
                            }
                        }
                        SGloadingRemove();
                        hot.render();
                        return;


                    }


                    //if non-price related fields are changed, do not proceed ahead
                    if (prop !== "Price" &&
                        prop !== "Cost" &&
                        prop.indexOf(quantity) === -1 &&
                        prop.indexOf("Discount") === -1 &&
                        prop.indexOf(margin) === -1 &&
                        prop.indexOf("Price") === -1 &&
                        prop !== "SalePrice" &&
                        prop !== "SalePriceMarginPercent" &&
                        prop !== "CostPerLot" &&
                        prop !== "QuantityPerLot" &&
                        prop !== "Quantity Per Lot" &&
                        prop !== "Cost Per Lot" &&
                        prop !== "Sale Price Margin" &&
                        prop !== "Sale Price" && (prop !== "Unit Price" || prop !== "UnitPrice") && prop !== "CaseQuantity" && (prop !== "Selling Price" || prop !== "SellingPrice") && prop !== "TotalSalePrice") {
                        return;
                    }


                    var colIndex;
                    $.each(columnsList,
                        function (index, value) {
                            if (value.data === prop) {
                                colIndex = index;
                            }
                        });

                    if (prop === 'Margin') {

                        if (changeLevelArray[rowIndex].indexOf("MAP") > -1 &&
                            (changeLevelArray[rowIndex].match(/,/g) || []).length === 2) {
                            changeLevelArray[rowIndex] = "";
                            continue;
                        }
                        if (newValue === '') {
                            changeLevelArray[rowIndex] += "MAP,";

                            if (changeLevelArray[rowIndex].indexOf('SAP') === -1) {
                                hot.getSettings().data[rowIndex]["Price"] = '';
                                //hot.getSettings().data[rowIndex]["Selling Price"] = '';//SKYG-822
                                hot.getSettings().data[rowIndex]["Unit Price"] = '';
                                hot.getSettings().data[rowIndex]["UnitPrice"] = '';
                            }
                            hot.render();
                            continue;

                        }

                        else {
                            newValue = parseFloat(newValue) || 0;

                            var marginPercent = newValue;
                            var cost = parseFloat(currentSelectedCost) || 0;//hot.getDataAtProp('SelectedCost')[rowIndex];
                            var sellingPrice = calculateSellingPrice(cost, marginPercent);


                            changeLevelArray[rowIndex] += "MAP,";

                            if (changeLevelArray[rowIndex].indexOf('SAP') === -1) {
                                hot.getSettings().data[rowIndex]["Price"] = FormatAmount(sellingPrice);
                                //hot.getSettings().data[rowIndex]["Selling Price"] = FormatAmount(sellingPrice);
                                hot.getSettings().data[rowIndex]["Unit Price"] = FormatAmount(sellingPrice);
                                hot.getSettings().data[rowIndex]["UnitPrice"] = FormatAmount(sellingPrice);
                            }

                            if (changeLevelArray[rowIndex].indexOf('MAA') === -1) {
                                var marginAmount = sellingPrice - cost;
                                hot.getSettings().data[rowIndex]["Discount"] = FormatAmount(marginAmount);
                            }



                            hot.render();
                            continue;
                        }
                    }

                    if (prop === 'Discount') {
                        if (changeLevelArray[rowIndex].indexOf("MAA") > -1 &&
                            (changeLevelArray[rowIndex].match(/,/g) || []).length === 2) {
                            changeLevelArray[rowIndex] = "";
                            continue;
                        }
                        newValue = parseFloat(newValue) || 0;
                        var marginAmount = newValue;
                        var cost = hot.getDataAtProp('SelectedCost')[rowIndex];
                        var sellingPrice = parseFloat(cost) + parseFloat(marginAmount);

                        changeLevelArray[rowIndex] += "MAA,";
                        if (changeLevelArray[rowIndex].indexOf('SAP') === -1) {
                            //hot.setDataAtRowProp(rowIndex, 'Price', FormatAmount(sellingPrice));
                            hot.getSettings().data[rowIndex]["Price"] = FormatAmount(sellingPrice);
                        }
                        if (changeLevelArray[rowIndex].indexOf('MAP') === -1) {
                            var marginPercent = calculateGrossMarginPercent(cost, sellingPrice);
                            //hot.setDataAtRowProp(rowIndex, 'Margin', FormatAmount(marginPercent));
                            hot.getSettings().data[rowIndex]["Margin"] = FormatAmount(marginPercent);
                        }

                        hot.render();

                        continue;
                    }
                    if (prop === 'Price') {
                        if (changeLevelArray[rowIndex].indexOf("SAP") > -1 &&
                            (changeLevelArray[rowIndex].match(/,/g) || []).length === 2) {
                            changeLevelArray[rowIndex] = "";
                            continue;
                        }
                        if (newValue !== '') {

                            newValue = parseFloat(newValue) || 0;
                            var sellingPrice = newValue;
                            var cost = hot.getDataAtProp('SelectedCost')[rowIndex];
                            var marginPercent = calculateGrossMarginPercent(cost, sellingPrice);

                            changeLevelArray[rowIndex] += "SAP,";

                            if (changeLevelArray[rowIndex].indexOf('MAP') === -1) {
                                //hot.setDataAtRowProp(rowIndex, 'Margin', FormatAmount(marginPercent));
                                hot.getSettings().data[rowIndex]["Margin"] = FormatAmount(marginPercent);
                            }
                            if (changeLevelArray[rowIndex].indexOf('MAA') === -1) {
                                var marginAmount = sellingPrice - cost;
                                //hot.setDataAtRowProp(rowIndex, 'Discount', FormatAmount(marginAmount));
                                hot.getSettings().data[rowIndex]["Discount"] = FormatAmount(marginAmount);
                            }

                            hot.render();
                        }
                        continue;
                    }
                    if (prop === 'Cost') {
                        CostchangeOrderByLogic(hot.getSettings().data[rowIndex]["Cost"], rowIndex);

                        // GetPricingLevels from clientSide table
                        var costOption = hot.getSettings().data[rowIndex]["CostOption"];
                        if (costOption === undefined) {
                            costOption = hot.getSettings().data[rowIndex]["Cost Option"];
                        }

                        if (costOption.toLowerCase() === "vendor cost") {
                            hot.getSettings().data[rowIndex]["SelectedCost"] = currentCost;
                            hot.getSettings().data[rowIndex]["Selected Cost"] = currentCost;
                            CostchangeAutocalculationInGrid(currentCost, rowIndex);



                        }
                        else if (costOption.toLowerCase() === "auto calculate cost") {

                            if (currentCost > fifoCost) {

                                toBeSelectedCost = currentCost;
                                hot.getSettings().data[rowIndex]["SelectedCost"] = toBeSelectedCost;
                                hot.getSettings().data[rowIndex]["Selected Cost"] = currentCost;
                                CostchangeAutocalculationInGrid(toBeSelectedCost, rowIndex);
                            }

                            else if (fifoCost > currentCost) {

                                toBeSelectedCost = fifoCost;
                                hot.getSettings().data[rowIndex]["SelectedCost"] = toBeSelectedCost;
                                hot.getSettings().data[rowIndex]["Selected Cost"] = currentCost;
                                CostchangeAutocalculationInGrid(toBeSelectedCost, rowIndex);
                            }

                        }

                        else if (costOption.toLowerCase() === "fifo cost") {
                            //implement  order by logic here.

                        }
                    }

                    if ((prop.indexOf(quantity) > -1 || prop.indexOf("Price") > -1 || prop.indexOf("Discount") > -1 || prop.indexOf(margin) > -1) &&
                        // prop.toLowerCase() !== "quantityperlot" && prop.toLowerCase() != "salepricemarginpercent" && prop.toLowerCase() != 'saleprice' && prop.toLowerCase() != 'selling price' && prop.toLowerCase() != 'sale price' && prop.toLowerCase() != 'sale price margin') {//SKYG-822
                        prop.toLowerCase() !== "quantityperlot" && prop.toLowerCase() !== "salepricemarginpercent" && prop.toLowerCase() !== 'saleprice' && prop.toLowerCase() !== 'unit price' && prop.toLowerCase() !== 'unitprice' && prop.toLowerCase() !== 'casequantity' && prop.toLowerCase() !== 'selling price' && prop.toLowerCase() !== 'sellingprice' && prop.toLowerCase() != 'totalsaleprice' && prop.toLowerCase() != 'sale price' && prop.toLowerCase() != 'sale price margin') {

                        newValue = normalizeAmount(changes[i][3]);

                        var current = getQuantityDiscountPair(prop, 'c');
                        var previous = getQuantityDiscountPair(prop, 'p');
                        var next = getQuantityDiscountPair(prop, 'n');

                        if (prop.indexOf("V") == 0) {
                            current = getQuantityDiscountPairVpc(prop, 'c');
                            previous = getQuantityDiscountPairVpc(prop, 'p');
                            next = getQuantityDiscountPairVpc(prop, 'n');
                        }
                        var previousQuantity = hot.getDataAtProp(previous.quantity)[i];
                        var previousSellingPrice = hot.getDataAtProp(previous.sellingPrice)[i];
                        var previousMargin = hot.getDataAtProp(previous.margin)[i];
                        var nextQuantity = hot.getDataAtProp(next.quantity)[i];
                        var nextSellingPrice = hot.getDataAtProp(next.SellingPrice)[i];
                        var nextMargin = hot.getDataAtProp(next.margin)[i];
                        var cost = hot.getDataAtProp("SelectedCost")[rowIndex];
                        if (cost === undefined) {
                            cost = hot.getDataAtProp("Selected Cost")[rowIndex];
                        }
                        var prevValue;
                        var nextValue;
                        var alternateValue;
                        if (prop.indexOf(quantity) > -1) {
                            //quantity
                            prevValue = previousQuantity;
                            nextValue = nextQuantity;
                        } else if (prop.indexOf("Price") > -1 && prop.toLowerCase() !== "salepricemarginpercent" && prop.toLowerCase() !== 'saleprice' && prop.toLowerCase() !== 'sale price' && prop.toLowerCase() !== 'sale price margin') {
                            //discount

                            if (newValue !== '') {
                                newValue = parseFloat(newValue) || 0;
                                prevValue = previousSellingPrice;
                                nextValue = nextSellingPrice;
                                if (source === 'paste') {
                                    hot.getSettings().data[rowIndex][current.sellingPrice] = newValue;
                                }

                                // alternateValue = convertToGrossMarginPercent(cost, newValue);
                                alternateValue = calculateGrossMarginPercentFromSellingPrice(cost, newValue);
                                alternateValue = parseFloat(alternateValue) || 0;


                                if (!(changeLevelArray[rowIndex].indexOf("MAA") > -1) &&
                                    (changeLevelArray[rowIndex].match(/,/g) || []).length < 2) {

                                    changeLevelArray[rowIndex] += "MAA,";

                                    //update margin on amount change
                                    if (changeLevelArray[rowIndex].indexOf("MAP") === -1) {
                                        hot.setDataAtRowProp(rowIndex, current.margin, FormatAmount(alternateValue));
                                        //if (alternateValue > 0) {
                                        //    hot.getSettings().data[rowIndex][current.margin] = FormatAmount(alternateValue);
                                        //}
                                        //else
                                        //    hot.getSettings().data[rowIndex][current.margin] = "";
                                    } else {
                                        changeLevelArray[rowIndex] = "";
                                    }
                                } else {
                                    changeLevelArray[rowIndex] = "";
                                }
                            }
                            continue;

                        } else if (prop.indexOf(margin) > -1) {
                            prevValue = previousMargin;
                            nextValue = nextMargin;
                            if (newValue === '') {
                                hot.getSettings().data[rowIndex][current.sellingPrice] = "";
                                hot.render();
                                continue;
                            }
                            else {
                                newValue = parseFloat(newValue) || 0;
                                if (source === 'paste') {
                                    hot.getSettings().data[rowIndex][current.margin] = newValue;
                                }

                                //if (newValue > 99.9999) {
                                //    newValue = 0;
                                //    hot.setDataAtRowProp(rowIndex, current.margin, newValue);
                                //    alertmsg("red", "Margin Percent cannot be greater or equal to 100%");
                                //} else if (newValue < 0) {
                                //    newValue = 0;
                                //    hot.setDataAtRowProp(rowIndex, current.margin, newValue);
                                //    alertmsg("red", "Margin Percent cannot be less than 0");
                                //}

                                // alternateValue = convertToGrossMarginAmount(cost, newValue); //alternate value is discount
                                alternateValue = calculateSellingPrice(cost, newValue); //alternateValue is sellingPrice
                                alternateValue = parseFloat(alternateValue) || 0;

                                //update discount
                                if (!(changeLevelArray[rowIndex].indexOf("MAP") > -1) &&
                                    (changeLevelArray[rowIndex].match(/,/g) || []).length < 2) {

                                    changeLevelArray[rowIndex] += "MAP,";

                                    //update amount on margin change
                                    if (changeLevelArray[rowIndex].indexOf("MAA") === -1) {
                                        //hot.setDataAtRowProp(rowIndex, current.discount, FormatAmount(alternateValue));
                                        if (alternateValue > 0)
                                            //hot
                                            //    .getSettings()
                                            //    .data[rowIndex][current.discount] = FormatAmount(alternateValue);
                                            hot
                                                .getSettings()
                                                .data[rowIndex][current.sellingPrice] = FormatAmount(alternateValue);
                                        else
                                            hot.getSettings().data[rowIndex][current.sellingPrice] = "";

                                    } else {
                                        changeLevelArray[rowIndex] = "";
                                    }
                                } else {
                                    changeLevelArray[rowIndex] = "";
                                }
                            }
                            hot.render();
                            continue;
                        }
                    }

                    if (prop === 'SalePrice') {
                        var thisSalePrice = newValue;
                        if (thisSalePrice !== null && thisSalePrice !== "") {
                            var thisCost = hot.getDataAtProp('SelectedCost')[rowIndex];
                            var thisSalePriceMarginPercent = calculateGrossMarginPercent(thisCost, thisSalePrice);
                            thisSalePriceMarginPercent = parseFloat(thisSalePriceMarginPercent) || 0;
                            //ensure that new sale price % meets condition to change
                            //if (thisSalePriceMarginPercent > 0) {
                            hot
                                .getSettings()
                                .data[rowIndex]["SalePriceMarginPercent"] =
                                FormatAmount(thisSalePriceMarginPercent);
                            //} else {
                            //    hot.getSettings().data[rowIndex]["SalePriceMarginPercent"] = 0;
                            //}
                        } else {
                            // hot.getSettings().data[rowIndex]["SalePriceMarginPercent"] = null;
                            hot.getSettings().data[rowIndex]["SalePriceMarginPercent"] = 0;
                            hot.getSettings().data[rowIndex]["SalePrice"] = 0;
                        }
                        var finalCaseQty = hot.getDataAtProp('CaseQuantity')[rowIndex];
                        var finalSalePrice = hot.getDataAtProp('SalePrice')[rowIndex];
                        var finalTotalSalePrice = calculateNewSellingPrice(finalSalePrice, finalCaseQty);

                        hot.getSettings().data[rowIndex]["TotalSalePrice"] = finalTotalSalePrice;
                        hot.render();
                    }
                    else if (prop === 'SalePriceMarginPercent') {
                        var mySalePriceMarginPercent = newValue;
                        if (mySalePriceMarginPercent !== null && mySalePriceMarginPercent !== "") {
                            var myCost = hot.getDataAtProp('SelectedCost')[rowIndex];
                            var mySalePrice = calculateSellingPrice(myCost, mySalePriceMarginPercent);
                            mySalePrice = parseFloat(mySalePrice) || 0;
                            //ensure that new sale price meets condition to change
                            if (mySalePrice > 0) {
                                hot.getSettings().data[rowIndex]["SalePrice"] = FormatAmount(mySalePrice);
                            } else {
                                hot.getSettings().data[rowIndex]["SalePrice"] = 0;
                            }
                        } else {
                            // hot.getSettings().data[rowIndex]["SalePrice"] ="";
                            hot.getSettings().data[rowIndex]["SalePriceMarginPercent"] = 0;
                            hot.getSettings().data[rowIndex]["SalePrice"] = 0;
                        }
                        var finalCaseQty = hot.getDataAtProp('CaseQuantity')[rowIndex];
                        var finalSalePrice = hot.getDataAtProp('SalePrice')[rowIndex];
                        var finalTotalSalePrice = calculateNewSellingPrice(finalSalePrice, finalCaseQty);

                        hot.getSettings().data[rowIndex]["TotalSalePrice"] = finalTotalSalePrice;
                        hot.render();
                    }

                    if (prop === 'QuantityPerLot') {

                        if (!(changeLevelArrayPerLot[rowIndex].indexOf("QPL") > -1) &&
                            (changeLevelArrayPerLot[rowIndex].match(/,/g) || []).length < 2) {
                            changeLevelArrayPerLot[rowIndex] += "QPL,";

                            //Get orderBy
                            var orderBy = hot.getDataAtProp('OrderBy')[rowIndex];

                            //Calculate cost per lot only if it's order by lot
                            if (orderBy === "L") {

                                //calculate costperlot on quantityperlot change
                                var cost1 = hot.getDataAtProp('Cost')[rowIndex];
                                var qtyPerLot1 = newValue;
                                var cosPerLot1 = cost1 * qtyPerLot1;
                                cosPerLot1 = parseFloat(cosPerLot1) || 0;
                                //check for valid value of costperlot

                                if (cosPerLot1 > 0) {
                                    if (changeLevelArrayPerLot[rowIndex].indexOf("CPL") === -1) {
                                        hot.getSettings().data[rowIndex]["CostPerLot"] = FormatAmount(cosPerLot1);
                                        //hot.setDataAtRowProp(rowIndex, 'CostPerLot', FormatAmount(cosPerLot1));
                                    } else {
                                        changeLevelArrayPerLot[rowIndex] = "";
                                    }
                                }
                                var CostPerLot = hot.getDataAtProp('CostPerLot')[rowIndex];
                                if (cost1 === null || cost1 === '' && CostPerLot > 0) {


                                    var qtyPerLot2 = hot.getDataAtProp('QuantityPerLot')[rowIndex];
                                    var cost2 = CostPerLot / qtyPerLot2;
                                    cost2 = parseFloat(cost2) || 0;
                                    hot.getSettings().data[rowIndex]["Cost"] = FormatAmount(cost2);


                                    var costOption = hot.getSettings().data[rowIndex]["CostOption"];
                                    if (costOption === undefined) {
                                        costOption = hot.getSettings().data[rowIndex]["Cost Option"];
                                    }

                                    if (costOption.toLowerCase() === "vendor cost") {
                                        hot.getSettings().data[rowIndex]["SelectedCost"] = FormatAmount(cost2);
                                        hot.getSettings().data[rowIndex]["Selected Cost"] = FormatAmount(cost2);
                                        CostchangeAutocalculationInGrid(FormatAmount(cost2), rowIndex);



                                    }
                                    else if (costOption.toLowerCase() === "auto calculate cost") {

                                        if (FormatAmount(cost2) > fifoCost) {

                                            toBeSelectedCost = FormatAmount(cost2);
                                            hot.getSettings().data[rowIndex]["SelectedCost"] = FormatAmount(cost2);
                                            hot.getSettings().data[rowIndex]["Selected Cost"] = FormatAmount(cost2);
                                            CostchangeAutocalculationInGrid(toBeSelectedCost, rowIndex);
                                        }

                                        else if (fifoCost > FormatAmount(cost2)) {

                                            toBeSelectedCost = fifoCost;
                                            hot.getSettings().data[rowIndex]["SelectedCost"] = toBeSelectedCost;
                                            hot.getSettings().data[rowIndex]["Selected Cost"] = toBeSelectedCost;
                                            CostchangeAutocalculationInGrid(toBeSelectedCost, rowIndex);
                                        }

                                    }






                                }

                            }

                            var minOrderUnit = hot.getDataAtProp('MinimumOrderUnit')[rowIndex];
                            var minLineOrder = "";
                            minLineOrder = getVendorMinLineOrder(hot.getDataAtProp('SupplierName')[rowIndex]);
                            if (minLineOrder !== '') {
                                if (minOrderUnit === 'L' && hot.getDataAtProp('OrderBy')[rowIndex] === 'L') {

                                    hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, hot.getDataAtProp('CostPerLot')[rowIndex])

                                }
                                else {

                                    hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, hot.getDataAtProp('SelectedCost')[rowIndex])

                                }
                            }
                        } else {
                            changeLevelArrayPerLot[rowIndex] = "";
                        }
                    }

                    else if (prop === 'CostPerLot') {

                        if (!(changeLevelArrayPerLot[rowIndex].indexOf("CPL") > -1) &&
                            (changeLevelArrayPerLot[rowIndex].match(/,/g) || []).length < 2) {



                            //Get orderBy
                            var orderBy = hot.getDataAtProp('OrderBy')[rowIndex];

                            //Calculate cost only if it's order by lot
                            if (orderBy === "L") {

                                //calculate cost on costperlot change
                                var costPerLot2 = newValue;
                                var qtyPerLot2 = hot.getDataAtProp('QuantityPerLot')[rowIndex];
                                var cost2 = costPerLot2 / qtyPerLot2;
                                cost2 = parseFloat(cost2) || 0;
                                //check for valid value of costperlot

                                if (cost2 > 0 && cost2 != Infinity) {
                                    if (changeLevelArrayPerLot[rowIndex].indexOf("COST") === -1) {
                                        changeLevelArrayPerLot[rowIndex] += "CPL,";
                                        //hot.getSettings().data[rowIndex]["Cost"] = FormatAmount(cost2);
                                        //Verify for FIFO
                                        hot.setDataAtRowProp(rowIndex, 'Cost', FormatAmount(cost2));
                                    }


                                    else {
                                        changeLevelArrayPerLot[rowIndex] = "";
                                    }
                                }

                                //else if (cost2 == Infinity) {

                                //    var costAgain = hot.getDataAtProp('Cost')[rowIndex];
                                //    var qtyAgain = costPerLot2 * costAgain;
                                //    if (qtyAgain > 0 && qtyAgain != Infinity) {
                                //        hot.setDataAtRowProp(rowIndex, 'QuantityPerLot', FormatAmount(qtyAgain));
                                //    }

                                //}


                            }

                            var minOrderUnit = hot.getDataAtProp('MinimumOrderUnit')[rowIndex];
                            var minLineOrder = "";
                            minLineOrder = getVendorMinLineOrder(hot.getDataAtProp('SupplierName')[rowIndex]);
                            if (minLineOrder !== '') {
                                if (minOrderUnit === 'L' && orderBy === 'L') {

                                    hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, cosPerLot1)

                                }
                                else {

                                    hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, cost1)

                                }
                            }




                        } else {
                            changeLevelArrayPerLot[rowIndex] = "";
                        }
                    }

                    //column header changes on event changes start

                    if (prop === "Primary" && newValue === "Yes") {

                        prop = "Cost";
                        newValue = hot.getDataAtProp('Cost')[rowIndex];
                        //hot.getDataAtProp('Cost')[rowIndex].change;
                    }

                    var columnsList = hot.getSettings().columns;
                    var colIndex;
                    $.each(columnsList,
                        function (index, value) {
                            if (value.data === prop) {
                                colIndex = index;
                            }
                        });



                    // if (prop === 'Selling Price') {
                    if (prop === 'Unit Price' || prop === 'UnitPrice') {//SKYG-822
                        if (changeLevelArray[rowIndex].indexOf("SAP") > -1 &&
                            (changeLevelArray[rowIndex].match(/,/g) || []).length === 2) {
                            changeLevelArray[rowIndex] = "";
                            continue;
                        }
                        if (newValue !== '') {
                            newValue = parseFloat(newValue) || 0;
                            var sellingPrice = newValue;
                            var cost = parseFloat(currentSelectedCost) || 0;
                            var marginPercent = calculateGrossMarginPercent(cost, sellingPrice);

                            changeLevelArray[rowIndex] += "SAP,";

                            if (changeLevelArray[rowIndex].indexOf('MAP') === -1) {
                                //hot.setDataAtRowProp(rowIndex, 'Margin', FormatAmount(marginPercent));
                                hot.getSettings().data[rowIndex]["Margin"] = FormatAmount(marginPercent);
                            }
                            if (changeLevelArray[rowIndex].indexOf('MAA') === -1) {
                                var marginAmount = sellingPrice - cost;
                                //hot.setDataAtRowProp(rowIndex, 'Discount', FormatAmount(marginAmount));
                                hot.getSettings().data[rowIndex]["Discount"] = FormatAmount(marginAmount);
                            }

                            var finalUnitPrice = hot.getDataAtProp('Unit Price')[rowIndex];
                            if (finalUnitPrice === undefined) {
                                finalUnitPrice = hot.getDataAtProp('UnitPrice')[rowIndex];
                            }
                            var finalCaseQty = hot.getDataAtProp('CaseQuantity')[rowIndex];
                            var finalSellingPrice = calculateNewSellingPrice(finalUnitPrice, finalCaseQty);
                            hot.getSettings().data[rowIndex]["SellingPrice"] = finalSellingPrice;

                            hot.render();


                        }
                        continue;
                    }

                    if (prop === 'Cost') {

                        // GetPricingLevels from clientSide table
                        var pricingLevels = 0;
                        var columnHeaders = hot.getSettings().columns;
                        $.grep(columnHeaders,
                            function (n, i) {
                                var title = n.title;
                                if (title.indexOf("Quantity") >= 0) {
                                    //including pricing Quantity
                                    // this is compentiate in loop less than operator
                                    pricingLevels++;
                                }

                            });

                        //pricingLevels = pricingLevels - 5;

                        if ((!(changeLevelArray[rowIndex].indexOf("COST") > -1) &&
                            (changeLevelArray[rowIndex].match(/,/g) || []).length < 2) ||
                            (!(changeLevelArrayPerLot[rowIndex].indexOf("COST") > -1) &&
                                (changeLevelArrayPerLot[rowIndex].match(/,/g) || []).length < 2)) {

                            //var hot.getSettings().data = hot.getSettings().data;
                            newValue = parseFloat(newValue) || 0;

                            var cost = newValue;
                            if (cost === 0) { return; }

                            var marginPercent = hot.getDataAtProp('Margin')[rowIndex];
                            marginPercent = parseFloat(marginPercent) || 0;
                            var marginAmount = hot.getDataAtProp('Discount')[rowIndex];
                            marginAmount = parseFloat(marginAmount) || 0;

                            //Get orderBy
                            var orderBy = hot.getDataAtProp('Order By')[rowIndex];

                            //Calculate cost per lot only if it's order by lot
                            if (orderBy === "L") {
                                //calculate costperlot on cost change
                                var qtyPerLot = hot.getDataAtProp('Quantity Per Lot')[rowIndex];
                                var costPerLot = cost * qtyPerLot;
                                costPerLot = parseFloat(costPerLot) || 0;
                                //check for valid value of costperlot
                                if (costPerLot > 0) {
                                    if (changeLevelArrayPerLot[rowIndex].indexOf("CPL") === -1) {
                                        hot.getSettings().data[rowIndex]["Cost Per Lot"] = FormatAmount(costPerLot);
                                        //hot.setDataAtRowProp(rowIndex, "Cost Per Lot", FormatAmount(costPerLot));
                                    } else {
                                        changeLevelArrayPerLot[rowIndex] = "";
                                    }
                                }
                            }
                            var marginLocked = hot.getSettings().data[rowIndex]["IsMarginLocked"].toLowerCase();

                            if (marginLocked === 'yes' || marginLocked === '1') { marginLocked = 'true'; }
                            //if sale price exists, calculate sale price on cost change
                            if (marginLocked === 'true') {
                                var salePrice = hot.getDataAtProp('Sale Price')[rowIndex];
                                if (salePrice === undefined) {
                                    salePrice = hot.getDataAtProp('SalePrice')[rowIndex];
                                }
                                if (salePrice) {
                                    var salePriceMarginPercent = hot.getDataAtProp('Sale Price Margin')[rowIndex];
                                    if (salePriceMarginPercent === undefined) {
                                        salePriceMarginPercent = hot.getDataAtProp('SalePriceMarginPercent')[rowIndex];
                                    }
                                    var newSalePrice = calculateSellingPrice(toBeSelectedCost, salePriceMarginPercent);
                                    //ensure that new sale price meets condition to change
                                    if (!isNaN(parseFloat(newSalePrice)) &&
                                        parseFloat(salePrice) !== parseFloat(newSalePrice)) {
                                        hot.getSettings().data[rowIndex]["Sale Price"] = FormatAmount(newSalePrice);
                                        hot.getSettings().data[rowIndex]["SalePrice"] = FormatAmount(newSalePrice);
                                        //hot.setDataAtRowProp(rowIndex, "Sale Price", FormatAmount(newSalePrice));
                                    }
                                }
                            }
                            else {
                                var salePrice = hot.getDataAtProp('Sale Price')[rowIndex];
                                if (salePrice === undefined) {
                                    salePrice = hot.getDataAtProp('SalePrice')[rowIndex];
                                }

                                if (salePrice) {

                                    var newSalePriceMarginPercent = calculateGrossMarginPercent(toBeSelectedCost, salePrice);
                                    newSalePriceMarginPercent = FormatAmount(newSalePriceMarginPercent) || newSalePriceMarginPercent;
                                    if (newSalePriceMarginPercent != 0) {
                                        hot.getSettings().data[rowIndex]["Sale Price Margin"] = FormatAmount(newSalePriceMarginPercent);
                                        hot.getSettings().data[rowIndex]["SalePriceMarginPercent"] = FormatAmount(newSalePriceMarginPercent);
                                    }

                                }
                            }
                            var finalSalePrice = hot.getDataAtProp('SalePrice')[rowIndex];
                            if (finalSalePrice === undefined) {
                                finalSalePrice = hot.getDataAtProp('Sale Price')[rowIndex];
                            }
                            var finalCaseQty = hot.getDataAtProp('CaseQuantity')[rowIndex];
                            var finalTotalSalePrice = calculateNewSellingPrice(finalSalePrice, finalCaseQty);

                            hot.getSettings().data[rowIndex]["TotalSalePrice"] = finalTotalSalePrice;
                            hot.render();
                            var minOrderUnit = hot.getDataAtProp('MinimumOrderUnit')[rowIndex];
                            var minLineOrder = "";
                            minLineOrder = getVendorMinLineOrder(hot.getDataAtProp('SupplierName')[rowIndex]);
                            if (minLineOrder !== '') {
                                if (minOrderUnit === 'L' && OrderBy === 'L') {

                                    hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, hot.getSettings().data[rowIndex]["CostPerLot"])

                                }
                                else {

                                    hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, hot.getSettings().data[rowIndex]["Cost"])

                                }
                            }

                            hot.render();

                        } else {
                            changeLevelArray[rowIndex] = "";
                            changeLevelArrayPerLot[rowIndex] = "";
                        }
                        var finalUnitPrice = hot.getDataAtProp('Unit Price')[rowIndex];
                        if (finalUnitPrice === undefined) {
                            finalUnitPrice = hot.getDataAtProp('UnitPrice')[rowIndex];
                        }
                        var finalCaseQty = hot.getDataAtProp('CaseQuantity')[rowIndex];
                        var finalSellingPrice = calculateNewSellingPrice(finalUnitPrice, finalCaseQty);
                        hot.getSettings().data[rowIndex]["SellingPrice"] = finalSellingPrice;

                    }
                    if ((prop.indexOf(quantity) > -1 || prop.indexOf("Price") > -1 || prop.indexOf("Discount") > -1 || prop.indexOf(margin) > -1) &&
                        prop.toLowerCase() !== "quantityperlot" && prop.toLowerCase() != 'sale price' && prop.toLowerCase() != 'sale price margin' && prop.toLowerCase() != "salepricemarginpercent" && prop.toLowerCase() != 'saleprice') {

                        newValue = normalizeAmount(changes[i][3]);

                        var current = getQuantityDiscountPair(prop, 'c');
                        var previous = getQuantityDiscountPair(prop, 'p');
                        var next = getQuantityDiscountPair(prop, 'n');

                        if (prop.indexOf("V") == 0) {
                            current = getQuantityDiscountPairVpc(prop, 'c');
                            previous = getQuantityDiscountPairVpc(prop, 'p');
                            next = getQuantityDiscountPairVpc(prop, 'n');
                        }

                        //var currentQuantity = hot.getDataAtProp(current.quantity)[1];
                        //var currentDiscount = hot.getDataAtProp(current.discount)[1];
                        var previousQuantity = hot.getDataAtProp(previous.quantity)[i];
                        //var previousDiscount = hot.getDataAtProp(previous.discount)[i];
                        var previousSellingPrice = hot.getDataAtProp(previous.sellingPrice)[i];
                        var previousMargin = hot.getDataAtProp(previous.margin)[i];
                        var nextQuantity = hot.getDataAtProp(next.quantity)[i];
                        //var nextDiscount = hot.getDataAtProp(next.discount)[i];
                        var nextSellingPrice = hot.getDataAtProp(next.SellingPrice)[i];
                        var nextMargin = hot.getDataAtProp(next.margin)[i];
                        var cost = hot.getDataAtProp("Cost")[rowIndex];
                        var prevValue;
                        var nextValue;
                        //alternate value (discount <--> margin)
                        var alternateValue;

                        if (prop.indexOf(quantity) > -1) {
                            //quantity
                            prevValue = previousQuantity;
                            nextValue = nextQuantity;
                        } else if (prop.indexOf("Price") > -1 && prop.toLowerCase() != 'sale price' && prop.toLowerCase() != 'sale price margin' && prop.toLowerCase() != "salepricemarginpercent" && prop.toLowerCase() != 'saleprice') {
                            //discount
                            newValue = parseFloat(newValue) || 0;
                            prevValue = previousSellingPrice;
                            nextValue = nextSellingPrice;
                            alternateValue = calculateGrossMarginPercentFromSellingPrice(toBeSelectedCost, newValue);
                            alternateValue = parseFloat(alternateValue) || 0;

                            if (!(changeLevelArray[rowIndex].indexOf("MAA") > -1) &&
                                (changeLevelArray[rowIndex].match(/,/g) || []).length < 2) {

                                changeLevelArray[rowIndex] += "MAA,";

                                //update margin on amount change
                                if (changeLevelArray[rowIndex].indexOf("MAP") === -1) {
                                    hot.getSettings().data[rowIndex][current.margin] = FormatAmount(alternateValue);
                                    //if (alternateValue > 0)
                                    //    hot.getSettings().data[rowIndex][current.margin] = FormatAmount(alternateValue);
                                    //else
                                    //    hot.getSettings().data[rowIndex][current.margin] = "";
                                } else {
                                    changeLevelArray[rowIndex] = "";
                                }
                            } else {
                                changeLevelArray[rowIndex] = "";
                            }
                        } else if (prop.indexOf(margin) > -1) {
                            prevValue = previousMargin;
                            nextValue = nextMargin;
                            newValue = parseFloat(newValue) || 0;



                            // alternateValue = convertToGrossMarginAmount(cost, newValue); //alternate value is discount
                            alternateValue = calculateSellingPrice(toBeSelectedCost, newValue); //alternateValue is sellingPrice
                            alternateValue = parseFloat(alternateValue) || 0;

                            //update discount
                            if (!(changeLevelArray[rowIndex].indexOf("MAP") > -1) &&
                                (changeLevelArray[rowIndex].match(/,/g) || []).length < 2) {

                                changeLevelArray[rowIndex] += "MAP,";

                                //update amount on margin change
                                if (changeLevelArray[rowIndex].indexOf("MAA") === -1) {
                                    //hot.setDataAtRowProp(rowIndex, current.discount, FormatAmount(alternateValue));
                                    if (alternateValue > 0)
                                        //hot
                                        //    .getSettings()
                                        //    .data[rowIndex][current.discount] = FormatAmount(alternateValue);
                                        hot
                                            .getSettings()
                                            .data[rowIndex][current.sellingPrice] = FormatAmount(alternateValue);
                                    else
                                        hot.getSettings().data[rowIndex][current.sellingPrice] = "";

                                } else {
                                    changeLevelArray[rowIndex] = "";
                                }
                            } else {
                                changeLevelArray[rowIndex] = "";
                            }
                        }


                    }

                    if (prop === 'Sale Price') {
                        var thisSalePrice = newValue;
                        if (thisSalePrice !== '' && thisSalePrice !== null) {
                            var thisCost = hot.getDataAtProp('Selected Cost')[rowIndex];
                            var thisSalePriceMarginPercent = calculateGrossMarginPercent(thisCost, thisSalePrice);
                            thisSalePriceMarginPercent = parseFloat(thisSalePriceMarginPercent) || 0;
                            //ensure that new sale price % meets condition to change
                            //if (thisSalePriceMarginPercent >= 0) {
                            hot
                                .getSettings()
                                .data[rowIndex]["Sale Price Margin"] =
                                FormatAmount(thisSalePriceMarginPercent);
                            //}
                            //else {
                            //    hot.getSettings().data[rowIndex]["Sale Price"] = 0;
                            //    hot.getSettings().data[rowIndex]["Sale Price Margin"] = 0;
                            //}
                        }
                        else {
                            hot.getSettings().data[rowIndex]["Sale Price Margin"] = 0;
                            hot.getSettings().data[rowIndex]["Sale Price"] = 0;
                            // hot.getSettings().data[rowIndex]["Sale Price Margin"] = null;
                        }
                        var finalSalePrice = hot.getDataAtProp('Sale Price')[rowIndex];
                        if (finalSalePrice === undefined) {
                            finalSalePrice = hot.getDataAtProp('SalePrice')[rowIndex];
                        }
                        var finalCaseQty = hot.getDataAtProp('CaseQuantity')[rowIndex];
                        var finalTotalSalePrice = calculateNewSellingPrice(finalSalePrice, finalCaseQty);

                        hot.getSettings().data[rowIndex]["TotalSalePrice"] = finalTotalSalePrice;
                        hot.render();
                    }

                    else if (prop === 'Sale Price Margin') {
                        var mySalePriceMarginPercent = newValue;

                        if (mySalePriceMarginPercent !== null && mySalePriceMarginPercent !== '') {
                            var myCost = hot.getDataAtProp('Selected Cost')[rowIndex];

                            var mySalePrice = calculateSellingPrice(myCost, mySalePriceMarginPercent);
                            console.log(mySalePrice)
                            mySalePrice = parseFloat(mySalePrice) || 0;
                            //ensure that new sale price meets condition to change
                            // if (mySalePrice >= 0) {
                            hot.getSettings().data[rowIndex]["Sale Price"] = FormatAmount(mySalePrice);
                            // }
                            //else {
                            //    hot.getSettings().data[rowIndex]["Sale Price"] = 0;
                            //    hot.getSettings().data[rowIndex]["Sale Price Margin"] = 0;
                            //}
                        }
                        else {
                            hot.getSettings().data[rowIndex]["Sale Price"] = 0;
                        }
                        var finalSalePrice = hot.getDataAtProp('Sale Price')[rowIndex];
                        if (finalSalePrice === undefined) {
                            finalSalePrice = hot.getDataAtProp('SalePrice')[rowIndex];
                        }
                        var finalCaseQty = hot.getDataAtProp('CaseQuantity')[rowIndex];
                        var finalTotalSalePrice = calculateNewSellingPrice(finalSalePrice, finalCaseQty);

                        hot.getSettings().data[rowIndex]["TotalSalePrice"] = finalTotalSalePrice;
                        hot.render();
                    }

                    if (prop === 'Quantity Per Lot') {

                        if (!(changeLevelArrayPerLot[rowIndex].indexOf("QPL") > -1) &&
                            (changeLevelArrayPerLot[rowIndex].match(/,/g) || []).length < 2) {
                            changeLevelArrayPerLot[rowIndex] += "QPL,";

                            //Get orderBy
                            var orderBy = hot.getDataAtProp('Order By')[rowIndex];

                            //Calculate cost per lot only if it's order by lot
                            if (orderBy === "L") {

                                //calculate costperlot on quantityperlot change
                                var cost1 = hot.getDataAtProp('Cost')[rowIndex];
                                var qtyPerLot1 = newValue;
                                var cosPerLot1 = cost1 * qtyPerLot1;
                                cosPerLot1 = parseFloat(cosPerLot1) || 0;
                                //check for valid value of costperlot

                                if (cosPerLot1 > 0) {
                                    if (changeLevelArrayPerLot[rowIndex].indexOf("CPL") === -1) {
                                        hot.getSettings().data[rowIndex]["Cost Per Lot"] = FormatAmount(cosPerLot1);
                                        //hot.setDataAtRowProp(rowIndex, 'Cost Per Lot', FormatAmount(cosPerLot1));
                                    } else {
                                        changeLevelArrayPerLot[rowIndex] = "";
                                    }
                                }
                                var CostPerLot = hot.getDataAtProp('CostPerLot')[rowIndex];
                                if (cost1 === null || cost1 === '' && CostPerLot > 0) {


                                    var qtyPerLot2 = hot.getDataAtProp('QuantityPerLot')[rowIndex];
                                    var cost2 = CostPerLot / qtyPerLot2;
                                    cost2 = parseFloat(cost2) || 0;
                                    hot.getSettings().data[rowIndex]["Cost"] = FormatAmount(cost2);
                                }
                            }

                            var minOrderUnit = hot.getDataAtProp('Minimum Order Unit')[rowIndex];
                            var minLineOrder = "";
                            minLineOrder = getVendorMinLineOrder(hot.getDataAtProp('Vendor')[rowIndex]);
                            if (minLineOrder !== '') {
                                if (minOrderUnit === 'L' && hot.getDataAtProp('Order By')[rowIndex] === 'L') {

                                    hot.getSettings().data[rowIndex]["Minimum Order Quantity"] = calculateMinOrderQty(minLineOrder, hot.getDataAtProp('Cost Per Lot')[rowIndex])

                                }
                                else {

                                    hot.getSettings().data[rowIndex]["Minimum Order Quantity"] = calculateMinOrderQty(minLineOrder, hot.getDataAtProp('Cost')[rowIndex])

                                }
                            }



                        } else {
                            changeLevelArrayPerLot[rowIndex] = "";
                        }
                    }
                    else if (prop === 'Cost Per Lot') {

                        if (!(changeLevelArrayPerLot[rowIndex].indexOf("CPL") > -1) &&
                            (changeLevelArrayPerLot[rowIndex].match(/,/g) || []).length < 2) {
                            //Get orderBy
                            var orderBy = hot.getDataAtProp('Order By')[rowIndex];
                            //Calculate cost only if it's order by lot
                            if (orderBy === "L") {

                                //calculate cost on costperlot change
                                var costPerLot2 = newValue;
                                var qtyPerLot2 = hot.getDataAtProp('Quantity Per Lot')[rowIndex];
                                var cost2 = costPerLot2 / qtyPerLot2;
                                cost2 = parseFloat(cost2) || 0;
                                //check for valid value of costperlot

                                if (cost2 > 0 && cost2 != Infinity) {
                                    if (changeLevelArrayPerLot[rowIndex].indexOf("COST") === -1) {
                                        changeLevelArrayPerLot[rowIndex] += "CPL,";
                                        //hot.getSettings().data[rowIndex]["Cost"] = FormatAmount(cost2);
                                        hot.setDataAtRowProp(rowIndex, 'Cost', FormatAmount(cost2));
                                    }


                                    else {
                                        changeLevelArrayPerLot[rowIndex] = "";
                                    }
                                }

                            }

                            var minOrderUnit = hot.getDataAtProp('MinimumOrderUnit')[rowIndex];
                            if (minOrderUnit === undefined) {
                                minOrderUnit = hot.getDataAtProp('Minimum Order Unit')[rowIndex];
                            }
                            var minLineOrder = "";
                            var vendorName = hot.getDataAtProp('SupplierName')[rowIndex];
                            if (vendorName === undefined) {
                                vendorName = hot.getDataAtProp('Vendor')[rowIndex];
                            }
                            minLineOrder = getVendorMinLineOrder(vendorName);
                            if (minLineOrder !== '') {
                                if (minOrderUnit === 'L' && orderBy === 'L') {

                                    hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, cosPerLot1)
                                    hot.getSettings().data[rowIndex]["Minimum Order Quantity"] = calculateMinOrderQty(minLineOrder, cosPerLot1)

                                }
                                else {

                                    hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, cost1)
                                    hot.getSettings().data[rowIndex]["Minimum Order Quantity"] = calculateMinOrderQty(minLineOrder, cost1)

                                }
                            }

                        } else {
                            changeLevelArrayPerLot[rowIndex] = "";
                        }
                    }

                    if (prop === 'CaseQuantity') {
                        var finalCaseQty = hot.getDataAtProp('CaseQuantity')[rowIndex];
                        if (finalCaseQty <= 0) {
                            hot.getSettings().data[rowIndex]["CaseQuantity"] = 1;
                            hot.render();
                        }

                        var finalUnitPrice = hot.getDataAtProp('Unit Price')[rowIndex];
                        if (finalUnitPrice === undefined) {
                            finalUnitPrice = hot.getDataAtProp('UnitPrice')[rowIndex];
                        }
                        var finalSalePrice = hot.getDataAtProp('Sale Price')[rowIndex];
                        if (finalSalePrice === undefined) {
                            finalSalePrice = hot.getDataAtProp('SalePrice')[rowIndex];
                        }

                        var finalTotalSalePrice = calculateNewSellingPrice(finalSalePrice, finalCaseQty);
                        hot.getSettings().data[rowIndex]["TotalSalePrice"] = finalTotalSalePrice;

                        var finalSellingPrice = calculateNewSellingPrice(finalUnitPrice, finalCaseQty);
                        hot.getSettings().data[rowIndex]["SellingPrice"] = finalSellingPrice;

                        hot.render();
                    }
                    //column header changes on event changes end
                    var newCost = hot.getSettings().data[rowIndex]["Cost"];
                    if (currentCost !== newCost & newCost !== 0) {
                        //hot.getSettings().data[rowIndex]["DateLastCostUpdate"] = GetTodayDate();
                        hot.getSettings().data[rowIndex]["DateLastCostUpdate"] = getCurrentDateTime();
                    }
                }
            }

            hot.render();

        }

    } catch (e) {
        //do nothing
        console.log(e);
    }

};

afterGetColHeader = function (col, th) {

    //if(col === 0)
    // if (currentCoulmn.toLowerCase().indexOf('staging') > -1) {
    //th.style.display = 'none';
    // }

    var t = th.firstChild.firstChild.firstChild.nodeValue.trim().toLowerCase();

    switch (t) {
        case "primary": $(th).css('background-color', greenColor);
            break;
        case "disabled": $(th).css('background-color', greenColor);
            break;
        case "name": $(th).css('background-color', greenColor);
            break;
        case "description": $(th).css('background-color', greenColor);
            break;
        case "new vendor part number": $(th).css('background-color', greenColor);
            break;
        case "order by": $(th).css('background-color', greenColor);
            break;
        case "margin": $(th).css('background-color', blueColor);
            break;
        case "discount": $(th).css('background-color', blueColor);
            break;
        //case "selling price": $(th).css('background-color', blueColor);
        //    break;//SKYG-822
        case "unit price": $(th).css('background-color', blueColor);
            break;
        case "selling price": $(th).css('background-color', blueColor);
            break;
        case "totalsaleprice": $(th).css('background-color', blueColor);
            break;
        case "casequantity": $(th).css('background-color', blueColor);
            break;
        case "sale price margin": $(th).css('background-color', blueColor);
            break;
        case "sale price": $(th).css('background-color', blueColor);
            break;
        case "cost": $(th).css('background-color', greenColor);
            break;
        case "quantity per lot": $(th).css('background-color', greenColor);
            break;
        case "cost per lot": $(th).css('background-color', greenColor);
            break;
        case "minimum order unit": $(th).css('background-color', greenColor);
            break;
        case "minimum order quantity": $(th).css('background-color', greenColor);
            break;
        case "drop ship": $(th).css('background-color', greenColor);
            break;
        case "standard package quantity": $(th).css('background-color', greenColor);
            break;
        case "unit of measure": $(th).css('background-color', greenColor);
            break;
        case "lot description": $(th).css('background-color', greenColor);
            break;
        case "lot part number": $(th).css('background-color', greenColor);
            break;
        case "quantity level 1": $(th).css('background-color', blueColor);
            break;
        case "margin level 1": $(th).css('background-color', blueColor);
            break;
        case "price level 1": $(th).css('background-color', blueColor);
            break;
        case "quantity level 2": $(th).css('background-color', blueColor);
            break;
        case "margin level 2": $(th).css('background-color', blueColor);
            break;
        case "price level 2": $(th).css('background-color', blueColor);
            break;
        case "quantity level 3": $(th).css('background-color', blueColor);
            break;
        case "margin level 3": $(th).css('background-color', blueColor);
            break;
        case "price level 3": $(th).css('background-color', blueColor);
            break;
        case "quantity level 4": $(th).css('background-color', blueColor);
            break;
        case "margin level 4": $(th).css('background-color', blueColor);
            break;
        case "price level 4": $(th).css('background-color', blueColor);
            break;
        case "lot barcode": $(th).css('background-color', greenColor);
            break;
        case "manufacturers barcode": $(th).css('background-color', greenColor);
            break;
        case "cost last updated": $(th).css('background-color', greenColor);
            break;
        case "lead time": $(th).css('background-color', greenColor);
            break;
        case "on order": $(th).css('background-color', greenColor);
            break;
        case "quantity on hand": $(th).css('background-color', greenColor);
            break;
        case "quantity on hand last updated": $(th).css('background-color', greenColor);
            break;
        case "vquantity level 1": $(th).css('background-color', greenColor);
            break;
        case "vmargin level 1": $(th).css('background-color', greenColor);
            break;
        case "vprice level 1": $(th).css('background-color', greenColor);
            break;
        case "vquantity level 2": $(th).css('background-color', greenColor);
            break;
        case "vmargin level 2": $(th).css('background-color', greenColor);
            break;
        case "vprice level 2": $(th).css('background-color', greenColor);
            break;
        case "vquantity level 3": $(th).css('background-color', greenColor);
            break;
        case "vmargin level 3": $(th).css('background-color', greenColor);
            break;
        case "vprice level 3": $(th).css('background-color', greenColor);
            break;
        case "vquantity level 4": $(th).css('background-color', greenColor);
            break;
        case "vmargin level 4": $(th).css('background-color', greenColor);
            break;
        case "vprice level 4": $(th).css('background-color', greenColor);
            break;
        case "non cancel non return": $(th).css('background-color', greenColor);
            break;
        case "margin locked": $(th).css('background-color', blueColor);
            break;
    }
}

afterCreateRow = function (a, b) {

}

var normalizeColumns = function (colData) {
    colData = colData
        .replace(/Local Part Number/g, "LocalSku")
        .replace(/Cost Per Lot/g, "CostPerLot")
        .replace(/Lot Part Number/g, "LotPartNumber")
        .replace(/Quantity Per Lot/g, "QuantityPerLot")
        .replace(/Order By/g, "OrderBy")
        .replace(/Min Order Unit/g, "MinOrderUnit")
        // .replace(/Selling Price/g, "Price")//SKYG-822
        .replace(/Unit Price/g, "Price")
        .replace(/Selling Price/g, "SellingPrice")
        .replace(/TotalSalePrice/g, "TotalSalePrice")
        .replace(/CaseQuantity/g, "CaseQuantity")
        .replace(/Sale Price/g, "SalePrice")
        .replace(/Set As Primary/g, "SetAsPrimary")
        .replace(/Dropship/g, "DropShip")
        .replace(/Minimum Order Quantity/g, "MinimumOrderQuantity")
        .replace(/Lot Description/g, "LotDescription")
        .replace(/Margin Dollars/g, "Discount");
    return colData;
}

var normalizeDatas = function (data) {
    var cleansedData = [];

    $.grep(data, function (n, i) {
        var jsonString = JSON.stringify(n).replace(/\=\\\"/g, "");
        var jsonString2 = jsonString.replace(/\\\"\"/g, "\"");
        var jsonString3 = jsonString2.replace(/\\\"/g, "''");
        jsonString3 = normalizeColumns(jsonString3);

        var parsedString = fixJsonQuote(jsonString3);
        ReplaceSingle2Quote(parsedString);
        cleansedData.push(parsedString);

    });

    return cleansedData;
}

$(function () {
    $("#file").change(function () {

        $('#chk-showmargin').prop('checked', true);
        $("#pagination").hide();
        stepped = 0;
        chunks = 0;
        rows = 0;
        var txt = "";
        var files = $('#file')[0].files;
        var config = buildConfig();

        if (files.length > 0) {

            //clear file information and make ready for next import
            //$('#file').val('');

            start = performance.now();
            header = [];
            $('#file').parse({
                config: config,
                before: function (file, inputElem) {
                    //console.log("Parsing file:", file);
                    if (hot) {
                        hot.updateSettings({ data: null });
                    }
                },
                complete: function () {
                    $('#file').val('');

                    /*
                    var manditoryColumnsLength = 20;

                    bishwo = columns1;

                    var pricingLevelColumns = columns1.length - manditoryColumnsLength;

                    //pricing level columns come together in count of 3 (Quantity, Margin, Discount), so adjust pricingLevelColumns
                    pricingLevelColumns = parseInt(pricingLevelColumns / 3);
                    pricingLevelColumns = pricingLevelColumns || 0;
                    */
                    var pricingLevelColumns = 0;
                    columns1 = $.grep(columns1,
                        function (i) {

                            //check if column begins with Quantity and ends with a number (e.g. Quantity1, Quantity2, etc.)
                            if (i.data.indexOf('Quantity') >= 0 && i.data.match(/\d+$/))
                                pricingLevelColumns++;

                            //change name of columns data to our standard form
                            var j = i;
                            j.data = normalizeColumns(j.data);
                            return j;
                        });

                    /*
                    //have one extra pricing level columns pair
                    pricingLevelColumns++;
                    */

                    //note: urlGetBulkPricingHeaders is passed for handsontable from view that has pricing columns, otherwise it's not passed.
                    //so check it
                    if (typeof urlGetBulkPricingHeaders !== 'undefined') {
                        $.ajax({
                            url: urlGetBulkPricingHeaders,
                            type: 'POST',
                            data: {
                                newColumns: pricingLevelColumns
                            },

                            beforeSend: function () {
                                SGloading('Loading');
                            },
                            success: function (response) {
                                var data = clientsideData[0].data;
                                customDelimiter = ",";

                                filetypeFlag = false;

                                data = normalizeDatas(data);

                                //var cleansedData = [];

                                //$.grep(data, function (n, i) {
                                //    var jsonString = JSON.stringify(n).replace(/\=\\\"/g, "");
                                //    var jsonString2 = jsonString.replace(/\\\"\"/g, "\"");
                                //    var jsonString3 = jsonString2.replace(/\\\"/g, "''");
                                //    jsonString3 = normalizeColumns(jsonString3);

                                //    var parsedString = fixJsonQuote(jsonString3);
                                //    ReplaceSingle2Quote(parsedString);
                                //    cleansedData.push(parsedString);
                                //    data = cleansedData;

                                //});

                                var columns = fixJsonQuote(response);

                                if ($('#CheckBoxgetDBValue').is(':checked')) {
                                    loadingRemove = 0;
                                    $.ajax({
                                        url: urlgetBulkPricingFromFile,
                                        type: 'POST',
                                        data: {
                                            jsonString: JSON.stringify(data)
                                        },

                                        beforeSend: function () {
                                            SGloading('Loading');
                                            //show diferent loading messages based on amount of data
                                            if (data.length < 100) {
                                                SGloading(loadingMessages[0]);
                                            } else if (data.length <= 300) {
                                                SGloading(loadingMessages[1]);
                                            } else {
                                                SGloading(loadingMessages[2]);
                                            }

                                        },
                                        success: function (datafromDB) {
                                            var clientData = data;
                                            data = fixJsonQuote(datafromDB.data);
                                            columns = fixJsonQuote(datafromDB.col);

                                            dataSchema = getDefaultHandsonValue(columns);

                                            var settings = {
                                                data: data,
                                                colHeaders: true,
                                                rowHeaders: true,
                                                //stretchH: 'all',
                                                columnSorting: false,
                                                search: true,
                                                fixedRowsTop: 0,
                                                fixedColumnsLeft: 2,//3 SKYG-819
                                                contextMenu: contextMenu,
                                                licenseKey: 'non-commercial-and-evaluation',
                                                columns: columns,
                                                comments: true,
                                                minSpareRows: 0,
                                                manualColumnResize: true,
                                                //manualColumnMove: true,
                                                dataSchema: dataSchema,

                                                //autoColumnSize: true,
                                                afterCreateRow: function (index, amount) {
                                                    data.splice(index, amount);
                                                },
                                                afterRemoveRow: function (index, amount) {

                                                    setTimeout(KitCostDisabled, 500);
                                                },
                                                afterSetCellMeta: function (row, col, key, val) {
                                                    setTimeout(KitCostDisabled, 500);
                                                },
                                                afterChange: afterChange,
                                                afterGetColHeader: afterGetColHeader



                                            };

                                            if (flag !== 0) {
                                                flag = 0;
                                                hot.destroy();
                                            }

                                            hot = new Handsontable(container, settings);

                                            flag = 1;

                                            if (data.length > 1 && hot.isEmptyRow(data.length - 1)) {
                                                hot.alter('remove_row', parseInt(data.length - 1));
                                            }
                                            //  //console.log(hot.countRows());
                                            var bulkchangedata = hot.getSettings().data;

                                            var pricingLevelObtainDb = 0;
                                            $.grep(columns,
                                                function (n, i) {
                                                    if (n.data.indexOf('Quantity') >= 0 && n.data.match(/\d+$/))
                                                        pricingLevelObtainDb++;
                                                });
                                            pricingLevelObtainDb = pricingLevelObtainDb;
                                            for (var rowIndex = 0; rowIndex < hot.countRows(); rowIndex++) {

                                                var cost = hot.getDataAtProp('Cost')[rowIndex];
                                                var localSku = hot.getDataAtProp('LocalSku')[rowIndex];
                                                var vendor = hot.getDataAtProp('Vendor')[rowIndex];

                                                //console.log(clientData);
                                                for (var x in clientData) {
                                                    if (clientData.hasOwnProperty(x)) {
                                                        if (clientData[x] && clientData[x].LocalSku) {
                                                            if (
                                                                localSku.toUpperCase().trim() ===
                                                                clientData[x].LocalSku.toUpperCase().trim() &&
                                                                vendor === clientData[x].Vendor) {

                                                                var clientCost = clientData[x].Cost;

                                                                //for regular margin/discount
                                                                var marginPricing = bulkchangedata[rowIndex]["Margin"];
                                                                if (marginPricing != null) {

                                                                    var sellingPrice =
                                                                        FormatAmount(calculateSellingPrice(clientCost, marginPricing));

                                                                    bulkchangedata[rowIndex]["Discount"] =
                                                                        FormatAmount(parseFloat(sellingPrice) - parseFloat(clientCost));
                                                                }

                                                                //for pricing level margin/discount
                                                                for (var i = 1; i <= pricingLevelObtainDb; i++) {

                                                                    var
                                                                        marginPricingLevel = bulkchangedata[rowIndex][
                                                                            "Margin" + i];
                                                                    //console.log(rowIndex + "  :  " + bulkchangedata[rowIndex]["Margin" + i]);
                                                                    if (marginPricingLevel != null) {

                                                                        var
                                                                            sellingPriceLevel =
                                                                                FormatAmount(calculateSellingPrice(clientCost, marginPricingLevel));
                                                                        bulkchangedata[rowIndex]["Discount" + i] =
                                                                            FormatAmount(parseFloat(sellingPriceLevel) - parseFloat(clientCost));
                                                                    }
                                                                }

                                                                if (cost !== clientCost) {
                                                                    bulkchangedata[rowIndex]["Cost"] = clientCost;
                                                                    var
                                                                        serverMarginPercent = bulkchangedata[rowIndex][
                                                                            "Margin"];

                                                                    var
                                                                        newSellingPrice =
                                                                            calculateSellingPrice(clientCost, serverMarginPercent);
                                                                    bulkchangedata[rowIndex]["Price"] =
                                                                        FormatAmount(newSellingPrice);

                                                                    bulkchangedata[rowIndex]["Discount"] =
                                                                        FormatAmount(parseFloat(newSellingPrice) - parseFloat(clientCost));

                                                                    //calculate cost per lot if orderBy = L
                                                                    var
                                                                        serverOrderBy = bulkchangedata[rowIndex][
                                                                            "OrderBy"];
                                                                    if (serverOrderBy === "L") {
                                                                        var
                                                                            serverQuantityPerLot = bulkchangedata[
                                                                                rowIndex][
                                                                                "QuantityPerLot"];
                                                                        var
                                                                            serverCostPerLot = bulkchangedata[rowIndex][
                                                                                "CostPerLot"];
                                                                        var
                                                                            serverCost = bulkchangedata[rowIndex]["Cost"
                                                                            ];
                                                                        if (serverQuantityPerLot && serverCost) {
                                                                            var
                                                                                newCostPerLot =
                                                                                    clientCost * serverQuantityPerLot;
                                                                            bulkchangedata[rowIndex]["CostPerLot"] =
                                                                                newCostPerLot;
                                                                        } else if (
                                                                            serverQuantityPerLot && serverCostPerLot
                                                                        ) {
                                                                            var
                                                                                newCost =
                                                                                    serverCostPerLot / serverQuantityPerLot;
                                                                            //bulkchangedata[rowIndex]["CostPerLot"] = newCostPerLot;
                                                                            hot
                                                                                .setDataAtRowProp(rowIndex, "Cost", FormatAmount(newCost));
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            hot.render();

                                        },
                                        complete: function () {
                                            SGloadingRemove();
                                            TblpgntBtm1();

                                        },
                                        error: function (xhr, status, error) {
                                            alert(status + ' ' + error);
                                        }
                                    });

                                } else {

                                    data = normalizeDatas(data);

                                    dataSchema = getDefaultHandsonValue(columns);

                                    data = performAutoCalculation(data, columns);

                                    var settings = {
                                        data: data.Data,
                                        colHeaders: true,
                                        rowHeaders: true,
                                        //stretchH: 'all',
                                        columnSorting: false,
                                        search: true,
                                        fixedRowsTop: 0,
                                        fixedColumnsLeft: 2, // 3 SKYG- 819
                                        contextMenu: contextMenu,
                                        licenseKey: 'non-commercial-and-evaluation',
                                        columns: columns,
                                        comments: true,
                                        minSpareRows: 0,
                                        manualColumnResize: true,
                                        //manualColumnMove: true,
                                        dataSchema: dataSchema,
                                        skipEmptyLines: true,
                                        //autoColumnSize: true,
                                        afterCreateRow: function (index, amount) {
                                            data.splice(index, amount);
                                        },

                                        afterRemoveRow: function (index, amount) {

                                            setTimeout(KitCostDisabled, 500);
                                        },
                                        afterSetCellMeta: function (row, col, key, val) {
                                            setTimeout(KitCostDisabled, 500);
                                        },
                                        afterChange: afterChange,
                                        afterGetColHeader: afterGetColHeader

                                    };

                                    if (flag !== 0) {
                                        flag = 0;
                                        hot.destroy();
                                    }

                                    hot = new Handsontable(container, settings);

                                    if (data.Errors && data.Errors.length > 0) {
                                        console.log("Following rows have errors");
                                        console.log(data.Errors);
                                        //showErrors(data.Errors); TODO: make efficient and uncomment
                                    }

                                    flag = 1;

                                    if (data.length > 1 && hot.isEmptyRow(data.length - 1)) {
                                        hot.alter('remove_row', parseInt(data.length - 1));
                                    }
                                }
                                //console.log(response);
                            },
                            complete: function () {
                                if (loadingRemove === 1)
                                    SGloadingRemove();
                                //$('#files').val("");

                                //if file is browsed, no need to export
                                $("#exportBulkPricingAll").prop("disabled", true);

                                TblpgntBtm1();

                            },
                            error: function (xhr, status, error) {
                                alert(status + ' ' + error);
                            }
                        });
                    }

                }
            });
        }
    });

});

//Returns pricing level # if its' a pricing level, otherwise returns null
var getPricingLevel = function (property, entity) {
    if (property != null &&
        property.match(/[a-zA-z]+/) && //column starts with alphabets
        property.match(/\d+/) && //column has numbers
        property.endsWith(property.match(/\d+/)[0])) {
        return property.match(/\d+/);
    }
    return null;
}

Handsontable.dom.addEvent(window, 'hashchange', function (event) {
    var lastCharofUrl = window.location.href.toString().slice(-1);
    if (lastCharofUrl === '#')
        return;

    savePaginateData();
});

var getDataForBulkVendorProduct = function (errorMetadataId, selectedPage, pageLimitFromPage) {
    var isErrorLoad = false;
    if (ErrorbulkData !== "" && ErrorbulkData !== undefined) {
        isErrorLoad = true;

    }
    var page = parseInt(window.location.hash.replace('#page-', ''), 10) || 1//selectedPage || 1
        , limit = pageLimit || 100
        , row = (page - 1) * limit
        , count = page * limit
        , part = [];
    var bulkData = "";

    $.ajax({
        type: 'POST',
        url: getBulkDataFromStaging,
        async: false,
        data: {
            'metadataId': stagingMetadataId,
            'pageNumber': page,
            'pageSize': pageLimit || 100,
            'isBulkPricing': isBulkPricing,
            'pagination': isImport,
            'keySearch': $('#search_field').val(),
            'isErrorLoad': isErrorLoad
        },
        beforeSend: function () {

            SGloading('Loading');
        },
        success: function (response) {

            var InvalidMsg = fixJsonQuote(response.data)[0].message;
            if (InvalidMsg !== undefined) {
                if (InvalidMsg.indexOf('Invalid Cost Option') > -1) {

                    alertmsg('red', InvalidMsg, -1);
                    return;
                }
            }

            bulkData = performAutoCalculation(fixJsonQuote(response.data), fixJsonQuote(response.col)).Data;


        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (xhr, status, error) {

            alert(status + ' ' + error);
        }


    });

    return bulkData;

}

function addDynamicPaginationToBulkVendorProductGrid() {

    var totalPages = parseInt(Math.ceil(recordCount / pageLimit))

    $('#pagination').pagination({
        pages: totalPages,

        cssStyle: 'light-theme'
    });

    $('#pagination').show();



}

(function (Handsontable) {
    function CellLoadRenderer(hotInstance, td, row, column, prop, value, cellProperties) {

        //if(prop !== 'VendorPartNumber')
        Handsontable.cellTypes.text.renderer.apply(this, arguments);
        if (prop !== 'VendorPartNumber' && prop !== 'NewVendorPartNumber' && prop !== 'LotPartNumber') {
            Handsontable.renderers.NumericRenderer.apply(this, arguments);
        }

        //td.innerHTML = '<div>'+value+'</div>'
        if (ErrorbulkData !== "" && ErrorbulkData[row].ErrorColumn !== null) {

            var errorColumnList = ErrorbulkData[row].ErrorColumn.split(',');
            for (var i = 0; i < errorColumnList.length; i++) {
                if (errorColumnList[i] === prop) {
                    td.disabled = 'false';
                    td.style.backgroundColor = 'red';

                    td.title = ErrorbulkData[row].ErrorMessage;
                }

            }

            //if (ErrorbulkData[row].ErrorMessage.indexOf('Quantity') > -1) {
            //    td.style.backgroundColor = 'red';
            //    td.title = ErrorbulkData[row].ErrorMessage;
            //    //cellProperties.comment = 'Test Comment';
            //}
        }
        return td;


        // ...your custom logic of the renderer
    }
    function batchCellLoadRenderer(hotInstance, td, row, column, prop, value, cellProperties) {
        Handsontable.renderers.AutocompleteRenderer.apply(this, arguments);

        if (prop == 'BatchLotTracking') {
            if (value == '' || value == 'No' || value == 'Yes') {
                td.className = "";

            }
        }

        return td;
    }
    function fulfillmentCellLoadRenderer(hotInstance, td, row, column, prop, value, cellProperties) {
        Handsontable.renderers.AutocompleteRenderer.apply(this, arguments);

        if (prop === 'FulfillmentCenter') {
            if (value === '' || value === ' ') {
                td.className = "";
            }

        }
        return td;
    }
    function ProductCategoryRenderer(hotInstance, td, row, column, prop, value, cellProperties) {
        var selectedId;
        var optionsList = cellProperties.chosenOptions.data;

        //if (typeof optionsList === "undefined" || typeof optionsList.length === "undefined" || !optionsList.length) {
        //    Handsontable.cellTypes.text.renderer(hotInstance, td, row, column, prop, value, cellProperties);
        //    return td;
        //}

        var values = (value + "").split("|");
        value = [];
        for (var index = 0; index < optionsList.length; index++) {

            if (values.indexOf(optionsList[index].id + "") > -1) {
                selectedId = optionsList[index].id;
                value.push(optionsList[index].label);
            }
        }
        value = value.join("| ");

        Handsontable.cellTypes.text.renderer(hotInstance, td, row, column, prop, value, cellProperties);
        return td;
    }


    function PlatformQualifierRenderer(hotInstance, td, row, column, prop, value, cellProperties) {
        var selectedId;
        var optionsList = cellProperties.chosenOptions.data;

        //if (typeof optionsList === "undefined" || typeof optionsList.length === "undefined" || !optionsList.length) {
        //    Handsontable.cellTypes.text.renderer(hotInstance, td, row, column, prop, value, cellProperties);
        //    return td;
        //}

        var values = (value + "").split("|");
        value = [];
        for (var index = 0; index < optionsList.length; index++) {

            if (values.indexOf(optionsList[index].id + "") > -1) {
                selectedId = optionsList[index].id;
                value.push(optionsList[index].label);
            }
        }
        value = value.join("| ");

        Handsontable.cellTypes.text.renderer(hotInstance, td, row, column, prop, value, cellProperties);
        return td;
    }



    function BrandRenderer(hotInstance, td, row, column, prop, value, cellProperties) {
        Handsontable.renderers.AutocompleteRenderer.apply(this, arguments);

        for (var i = 0; i < disabledProducts.length; i++) {
            if (disabledProducts[i] == value) {
                td.className = "unselectable";
                td.style.backgroundColor = 'red';
            }
        }


        return td;
    }

    // Register an alias
    Handsontable.renderers.registerRenderer('afterCellLoadRenderer', CellLoadRenderer);
    Handsontable.renderers.registerRenderer('batchLotafterCellLoadRenderer', batchCellLoadRenderer);
    Handsontable.renderers.registerRenderer('fulfillmentafterCellLoadRenderer', fulfillmentCellLoadRenderer);
    Handsontable.renderers.registerRenderer('ProductCategoryRenderer', ProductCategoryRenderer);
    Handsontable.renderers.registerRenderer('PlatformQualifierRenderer', PlatformQualifierRenderer);
    Handsontable.renderers.registerRenderer('BrandRenderer', BrandRenderer);

})(Handsontable);

function calculateMinOrderQty(minOrderLine, Cost) {
    if (Cost === undefined || Cost === "0" || Cost === null)
        return 1;
    else
        return Math.ceil(minOrderLine / Cost);
}

function CostchangeAutocalculationInGrid(newSelectedCost, rowIndex) {


    // GetPricingLevels from clientSide table
    var pricingLevels = 0;
    var columnHeaders = hot.getSettings().columns;
    $.grep(columnHeaders,
        function (n, i) {
            var title = n.title;
            if (title.indexOf("Quantity") >= 0) {
                //including pricing Quantity
                // this is compentiate in loop less than operator
                pricingLevels++;
            }

        });

    newValue = parseFloat(newSelectedCost) || 0;
    var cost = newValue;
    if (cost === 0) { return; }
    if (cost !== 0 && wasSetPrimary === false) {
        //hot.getSettings().data[rowIndex]["DateLastCostUpdate"] = GetTodayDate();
        //hot.getSettings().data[rowIndex]["Cost Last Updated"] = GetTodayDate();

        hot.getSettings().data[rowIndex]["DateLastCostUpdate"] = getCurrentDateTime();
        hot.getSettings().data[rowIndex]["Cost Last Updated"] = getCurrentDateTime();

    }
    var marginPercent = hot.getDataAtProp('Margin')[rowIndex];
    marginPercent = parseFloat(marginPercent) || 0;
    var marginAmount = hot.getDataAtProp('Discount')[rowIndex];
    marginAmount = parseFloat(marginAmount) || 0;
    var sellingPriceFromGrid = hot.getDataAtProp('Price')[rowIndex];
    if (sellingPriceFromGrid === undefined) {
        //sellingPriceFromGrid = hot.getDataAtProp('Selling Price')[rowIndex];
        sellingPriceFromGrid = hot.getDataAtProp('Unit Price')[rowIndex];
        if (sellingPriceFromGrid === undefined) {
            sellingPriceFromGrid = hot.getDataAtProp('UnitPrice')[rowIndex];
        }
    }
    sellingPriceFromGrid = parseFloat(sellingPriceFromGrid) || 0;
    var marginLocked = hot.getDataAtProp('IsMarginLocked')[rowIndex];
    if (marginLocked === undefined) {
        marginLocked = hot.getDataAtProp('Margin Locked')[rowIndex];
    }
    if (marginLocked.toLowerCase() === 'yes') {
        marginLocked = 'true';
    }
    else if (marginLocked.toLowerCase() === 'no') {
        marginLocked = 'false';
    }
    if (marginLocked === 'true') {
        if (marginPercent !== 0) {
            var newSellingPrice = calculateSellingPrice(cost, marginPercent);
            hot.getSettings().data[rowIndex]["Price"] = FormatAmount(newSellingPrice);
            //hot.getSettings().data[rowIndex]["Selling Price"] = FormatAmount(newSellingPrice);//SKYG-822
            hot.getSettings().data[rowIndex]["Unit Price"] = FormatAmount(newSellingPrice);
            hot.getSettings().data[rowIndex]["UnitPrice"] = FormatAmount(newSellingPrice);
            hot.render();
            var discount = parseFloat(newSellingPrice) - parseFloat(cost);
            discount = parseFloat(discount) || 0;
            if (discount > 0) {
                hot.getSettings().data[rowIndex]["Discount"] = FormatAmount(discount);
                //hot.setDataAtRowProp(rowIndex, "Discount", FormatAmount(discount));
            }
            else {
                hot.getSettings().data[rowIndex]["Discount"] = "";
            }
        } else if (marginAmount > 0) {
            var sellingPrice = cost + marginAmount;
            hot.getSettings().data[rowIndex]["Price"] = FormatAmount(sellingPrice);
            hot.getSettings().data[rowIndex]["UnitPrice"] = FormatAmount(newSellingPrice);
            hot.getSettings().data[rowIndex]["Unit Price"] = FormatAmount(newSellingPrice);
            //hot.setDataAtRowProp(rowIndex, 'Price', sellingPrice);
            var marginPercent = convertToGrossMarginPercent(cost, marginAmount);
            marginPercent = parseFloat(marginPercent) || 0;
            if (marginPercent > 0) {
                hot.getSettings().data[rowIndex]["Margin"] = FormatAmount(marginPercent);
                //hot.setDataAtRowProp(rowIndex, "Margin", FormatAmount(marginPercent));
            }
            else {
                hot.getSettings().data[rowIndex]["Margin"] = "";
                hot.getSettings().data[rowIndex]["Discount"] = "";
            }

        }
    }

    else {
        if (sellingPriceFromGrid > 0) {
            var newMarginPercent = calculateGrossMarginPercentFromSellingPrice(cost, sellingPriceFromGrid);
            hot.getSettings().data[rowIndex]["Margin"] = FormatAmount(newMarginPercent);
            var marginAmount = sellingPriceFromGrid - cost;
            if (marginAmount > 0) {
                hot.getSettings().data[rowIndex]["Discount"] = FormatAmount(marginAmount);
            }
            else {
                hot.getSettings().data[rowIndex]["Discount"] = "";
            }

        }

    }


    //if sale price exists, calculate sale price on cost change

    if (marginLocked === 'true') {
        var salePrice = hot.getDataAtProp('SalePrice')[rowIndex];
        if (salePrice === undefined) { salePrice = hot.getDataAtProp('Sale Price')[rowIndex]; }
        if (salePrice) {
            var salePriceMarginPercent = hot.getDataAtProp('SalePriceMarginPercent')[rowIndex];
            if (salePriceMarginPercent === undefined) {
                salePriceMarginPercent = hot.getDataAtProp('Sale Price Margin')[rowIndex];
            }
            var newSalePrice = calculateSellingPrice(cost, salePriceMarginPercent);
            //ensure that new sale price meets condition to change
            if (!isNaN(parseFloat(newSalePrice)) &&
                parseFloat(salePrice) !== parseFloat(newSalePrice)) {
                hot.getSettings().data[rowIndex]["SalePrice"] = FormatAmount(newSalePrice);
                hot.getSettings().data[rowIndex]["Sale Price"] = FormatAmount(newSalePrice);
                //hot.setDataAtRowProp(rowIndex, "SalePrice", FormatAmount(newSalePrice));
            }
        }
    }
    else {
        var salePrice = hot.getDataAtProp('SalePrice')[rowIndex];
        if (salePrice === undefined) {
            salePrice = hot.getDataAtProp('Sale Price')[rowIndex];
        }
        if (salePrice) {

            var newSalePriceMarginPercent = calculateGrossMarginPercent(cost, salePrice);
            newSalePriceMarginPercent = FormatAmount(newSalePriceMarginPercent) || newSalePriceMarginPercent;
           
                hot.getSettings().data[rowIndex]["SalePriceMarginPercent"] = FormatAmount(newSalePriceMarginPercent);
                hot.getSettings().data[rowIndex]["Sale Price Margin"] = FormatAmount(newSalePriceMarginPercent);
            

        }
    }

    //hot.render();

    //calculate pricing levels on cost change
    for (var pricingLevelCounter = 1;
        pricingLevelCounter <= pricingLevels;
        pricingLevelCounter++) {

        var qtyLevel = hot.getDataAtProp('Quantity' + pricingLevelCounter)[rowIndex];
        //pricing levels
        var marginPricingLevel = hot.getDataAtProp('Margin' + pricingLevelCounter)[rowIndex];
        marginPricingLevel = parseFloat(marginPricingLevel) || 0;

        var sellingPricePricingLevel = hot.getDataAtProp('Price' + pricingLevelCounter)[rowIndex];
        sellingPricePricingLevel = parseFloat(sellingPricePricingLevel) || 0;

        var discountPricingLevel = hot.getDataAtProp('Discount' + pricingLevelCounter)[rowIndex];
        discountPricingLevel = parseFloat(discountPricingLevel) || 0;

        if (marginLocked === 'true') {

            if (marginPricingLevel > 0) {
                var sellingPriceLevel = calculateSellingPrice(cost, marginPricingLevel);
                //var discountLevel = parseFloat(sellingPriceLevel) - parseFloat(cost);
                //discountLevel = parseFloat(discountLevel) || 0;
                if (sellingPriceLevel > 0) {
                    hot
                        .getSettings()
                        .data[rowIndex]["Price" + pricingLevelCounter] =
                        FormatAmount(sellingPriceLevel);
                    //hot.setDataAtRowProp(rowIndex, "Discount" + pricingLevelCounter, FormatAmount(discountLevel));
                } else {
                    hot.getSettings().data[rowIndex]["Price" + pricingLevelCounter] = "";
                    hot.getSettings().data[rowIndex]["Margin" + pricingLevelCounter] = "";
                }
            } else {
                if (sellingPricePricingLevel != 0) {
                    // if discount selected - calculate margin
                    var calculatedMargin = calculateGrossMarginPercentFromSellingPrice(cost, sellingPricePricingLevel);
                    calculatedMargin = parseFloat(calculatedMargin) || 0;

                    if (calculatedMargin > 0) {
                        hot
                            .getSettings()
                            .data[rowIndex]["Margin" + pricingLevelCounter] =
                            FormatAmount(calculatedMargin);
                        //hot.setDataAtRowProp(rowIndex, "Margin" + pricingLevelCounter, FormatAmount(calculatedMargin));
                    } else {
                        if (qtyLevel === 0) {
                            hot.getSettings().data[rowIndex]["Margin" + pricingLevelCounter] = "";
                        }
                        hot.getSettings().data[rowIndex]["Price" + pricingLevelCounter] = "";
                    }
                }
                else {
                    if (qtyLevel === 0) {
                        hot.getSettings().data[rowIndex]["Margin" + pricingLevelCounter] = "";
                    }
                    hot.getSettings().data[rowIndex]["Price" + pricingLevelCounter] = "";

                }
            }
        }

        else {

            if (sellingPricePricingLevel > 0) {
                var calculatedMargin = calculateGrossMarginPercentFromSellingPrice(cost, sellingPricePricingLevel);
                calculatedMargin = parseFloat(calculatedMargin) || 0;
                hot
                    .getSettings()
                    .data[rowIndex]["Margin" + pricingLevelCounter] =
                    FormatAmount(calculatedMargin);
                //hot.setDataAtRowProp(rowIndex, "Margin" + pricingLevelCounter, FormatAmount(calculatedMargin));


            }

        }

        if (showPurchasingLevel === true) {
            //purchasing levels
            var marginPurchasingLevel = hot
                .getDataAtProp('VMargin' + pricingLevelCounter)[rowIndex];
            var sellingPricePurchasingLevel = hot
                .getDataAtProp('VPrice' + pricingLevelCounter)[rowIndex];
            var discountPurchasingLevel = hot
                .getDataAtProp('VDiscount' + pricingLevelCounter)[rowIndex];
            var qtyPurchasingLevel = hot
                .getDataAtProp('VQuantity' + pricingLevelCounter)[rowIndex];
            if (marginLocked === 'true') {
                if (marginPurchasingLevel) {
                    var sellingPurchaseLevel = calculateSellingPrice(cost, marginPurchasingLevel);
                    sellingPurchaseLevel = parseFloat(sellingPurchaseLevel) || 0;
                    // var discountLevel = parseFloat(sellingPriceLevel) - parseFloat(cost);
                    //discountLevel = parseFloat(discountLevel) || 0;
                    if (sellingPurchaseLevel > 0) {
                        hot
                            .getSettings()
                            .data[rowIndex]["VPrice" + pricingLevelCounter] =
                            FormatAmount(sellingPurchaseLevel);
                        //hot.setDataAtRowProp(rowIndex, "Discount" + purchasingLevelCounter, FormatAmount(discountLevel));
                    } else {
                        if (qtyPurchasingLevel === 0) {
                            hot
                                .getSettings()
                                .data[rowIndex]["VMargin" + pricingLevelCounter] = "";
                        }
                        hot
                            .getSettings()
                            .data[rowIndex]["VPrice" + pricingLevelCounter] = "";
                    }
                } else {
                    // if discount selected - calculate margin

                    if (sellingPricePurchasingLevel > 0) {
                        var calculatedMargin = calculateGrossMarginPercentFromSellingPrice(cost, sellingPricePurchasingLevel);
                        calculatedMargin = parseFloat(calculatedMargin) || 0;
                        hot
                            .getSettings()
                            .data[rowIndex]["VMargin" + pricingLevelCounter] =
                            FormatAmount(calculatedMargin);
                    }

                    else {
                        if (qtyPurchasingLevel === 0) {
                            hot
                                .getSettings()
                                .data[rowIndex]["VMargin" + pricingLevelCounter] = "";
                        }
                        hot
                            .getSettings()
                            .data[rowIndex]["VPrice" + pricingLevelCounter] = "";
                    }

                }
            }
            else {
                if (sellingPricePurchasingLevel > 0) {
                    var calculatedMargin = calculateGrossMarginPercentFromSellingPrice(cost, sellingPricePurchasingLevel);
                    calculatedMargin = parseFloat(calculatedMargin) || 0;
                    hot
                        .getSettings()
                        .data[rowIndex]["VMargin" + pricingLevelCounter] =
                        FormatAmount(calculatedMargin);
                }

                else {
                    if (qtyPurchasingLevel === 0) {
                        hot
                            .getSettings()
                            .data[rowIndex]["VMargin" + pricingLevelCounter] = "";
                    }
                    hot
                        .getSettings()
                        .data[rowIndex]["VPrice" + pricingLevelCounter] = "";
                }



            }
        }


        //column header changes on pricing level


        //pricing levels
        var marginPricingLevel1 = hot.getDataAtProp('Margin Level ' + pricingLevelCounter)[rowIndex];
        marginPricingLevel1 = parseFloat(marginPricingLevel1) || 0;

        var sellingPricePricingLevel1 = hot.getDataAtProp('Price Level ' + pricingLevelCounter)[rowIndex];
        sellingPricePricingLevel1 = parseFloat(sellingPricePricingLevel1) || 0;

        if (marginLocked === 'true') {
            if (marginPricingLevel1 != 0) {
                var sellingPriceLevel = calculateSellingPrice(cost, marginPricingLevel1);
                //var discountLevel = parseFloat(sellingPriceLevel) - parseFloat(cost);
                //discountLevel = parseFloat(discountLevel) || 0;
                if (sellingPriceLevel > 0) {
                    hot
                        .getSettings()
                        .data[rowIndex]["Price Level " + pricingLevelCounter] =
                        FormatAmount(sellingPriceLevel);
                    //hot.setDataAtRowProp(rowIndex, "Discount" + pricingLevelCounter, FormatAmount(discountLevel));
                } else {
                    hot.getSettings().data[rowIndex]["Price Level " + pricingLevelCounter] = "";
                    hot.getSettings().data[rowIndex]["Margin Level " + pricingLevelCounter] = "";
                }
            } else {
                if (sellingPricePricingLevel1 != 0) {
                    // if discount selected - calculate margin
                    var calculatedMargin = calculateGrossMarginPercentFromSellingPrice(cost, sellingPricePricingLevel1);
                    calculatedMargin = parseFloat(calculatedMargin) || 0;

                    if (calculatedMargin > 0) {
                        hot
                            .getSettings()
                            .data[rowIndex]["Margin Level " + pricingLevelCounter] =
                            FormatAmount(calculatedMargin);
                        //hot.setDataAtRowProp(rowIndex, "Margin" + pricingLevelCounter, FormatAmount(calculatedMargin));
                    } else {
                        hot.getSettings().data[rowIndex]["Margin Level " + pricingLevelCounter] = "";
                        hot.getSettings().data[rowIndex]["Price Level " + pricingLevelCounter] = "";
                    }
                }
                else {
                    hot.getSettings().data[rowIndex]["Margin Level " + pricingLevelCounter] = "";
                    hot.getSettings().data[rowIndex]["Price Level " + pricingLevelCounter] = "";

                }
            }

        }

        else {

            if (sellingPricePricingLevel1 != 0) {
                var calculatedMargin = calculateGrossMarginPercentFromSellingPrice(cost, sellingPricePricingLevel1);
                calculatedMargin = parseFloat(calculatedMargin) || 0;

                //if (calculatedMargin > 0) {
                hot
                    .getSettings()
                    .data[rowIndex]["Margin Level " + pricingLevelCounter] =
                    FormatAmount(calculatedMargin);
            }
        }

        //purchasing levels
        if (showPurchasingLevel === true) {
            var marginPurchasingLevel1 = hot
                .getDataAtProp('VMargin Level ' + pricingLevelCounter)[rowIndex];
            var sellingPricePurchasingLevel1 = hot
                .getDataAtProp('VPrice Level ' + pricingLevelCounter)[rowIndex];
            var discountPurchasingLevel1 = hot
                .getDataAtProp('VDiscount Level ' + pricingLevelCounter)[rowIndex];

            if (marginLocked === 'true') {
                if (marginPurchasingLevel1) {
                    var sellingPurchaseLevel = calculateSellingPrice(cost, marginPurchasingLevel1);
                    sellingPurchaseLevel = parseFloat(sellingPurchaseLevel) || 0;
                    // var discountLevel = parseFloat(sellingPriceLevel) - parseFloat(cost);
                    //discountLevel = parseFloat(discountLevel) || 0;
                    if (sellingPurchaseLevel > 0) {
                        hot
                            .getSettings()
                            .data[rowIndex]["VPrice Level " + pricingLevelCounter] =
                            FormatAmount(sellingPurchaseLevel);
                        //hot.setDataAtRowProp(rowIndex, "Discount" + purchasingLevelCounter, FormatAmount(discountLevel));
                    } else {

                        hot
                            .getSettings()
                            .data[rowIndex]["VMargin Level " + pricingLevelCounter] = "";
                        hot
                            .getSettings()
                            .data[rowIndex]["VPrice Level " + pricingLevelCounter] = "";
                    }
                }
                else {
                    // if discount selected - calculate margin

                    if (sellingPricePurchasingLevel1 > 0) {
                        var calculatedMargin = calculateGrossMarginPercentFromSellingPrice(cost, sellingPricePurchasingLevel1);
                        calculatedMargin = parseFloat(calculatedMargin) || 0;
                        // if (calculatedMargin > 0) {
                        hot
                            .getSettings()
                            .data[rowIndex]["VMargin Level " + pricingLevelCounter] =
                            FormatAmount(calculatedMargin);
                    }

                    else {
                        hot
                            .getSettings()
                            .data[rowIndex]["VMargin Level " + pricingLevelCounter] = "";
                        hot
                            .getSettings()
                            .data[rowIndex]["VPrice Level " + pricingLevelCounter] = "";
                    }

                }
            }

            else {
                if (sellingPricePurchasingLevel1 > 0) {
                    var calculatedMargin = calculateGrossMarginPercentFromSellingPrice(cost, sellingPricePurchasingLevel1);
                    calculatedMargin = parseFloat(calculatedMargin) || 0;
                    //  if (calculatedMargin > 0) {
                    hot
                        .getSettings()
                        .data[rowIndex]["VMargin Level " + pricingLevelCounter] =
                        FormatAmount(calculatedMargin);
                }
            }
        }

    }
    //calculate selling price and saleprice

    var finalUnitPrice = hot.getDataAtProp('Unit Price')[rowIndex];
    if (finalUnitPrice === undefined) {
        finalUnitPrice = hot.getDataAtProp('UnitPrice')[rowIndex];
    }
    var finalSalePrice = hot.getDataAtProp('Sale Price')[rowIndex];
    if (finalSalePrice === undefined) {
        finalSalePrice = hot.getDataAtProp('SalePrice')[rowIndex];
    }
    var finalCaseQty = hot.getDataAtProp('CaseQuantity')[rowIndex];

    var finalTotalSalePrice = calculateNewSellingPrice(finalSalePrice, finalCaseQty);
    hot.getSettings().data[rowIndex]["TotalSalePrice"] = finalTotalSalePrice;

    var finalSellingPrice = calculateNewSellingPrice(finalUnitPrice, finalCaseQty);
    hot.getSettings().data[rowIndex]["SellingPrice"] = finalSellingPrice;

    hot.render();
}

function CostchangeOrderByLogic(cost, rowIndex) {


    //Get orderBy
    var orderBy = hot.getDataAtProp('OrderBy')[rowIndex];
    if (orderBy === undefined) {
        orderBy = hot.getDataAtProp('Order By')[rowIndex];

    }

    //Calculate cost per lot only if it's order by lot
    if (orderBy === "L") {
        //calculate costperlot on cost change
        var qtyPerLot = hot.getDataAtProp('QuantityPerLot')[rowIndex];
        if (qtyPerLot === undefined) {
            qtyPerLot = hot.getDataAtProp('Quantity Per Lot')[rowIndex];

        }
        var costPerLot = cost * qtyPerLot;
        costPerLot = parseFloat(costPerLot) || 0;
        //check for valid value of costperlot
        if (costPerLot > 0) {
            if (changeLevelArrayPerLot[rowIndex].indexOf("CPL") === -1) {
                hot.getSettings().data[rowIndex]["CostPerLot"] = FormatAmount(costPerLot);
                hot.getSettings().data[rowIndex]["Cost Per Lot"] = FormatAmount(costPerLot);
                //hot.setDataAtRowProp(rowIndex, "CostPerLot", FormatAmount(costPerLot));
            } else {
                changeLevelArrayPerLot[rowIndex] = "";
            }
        }
    }

    var minOrderUnit = hot.getDataAtProp('MinimumOrderUnit')[rowIndex];
    var file = false;
    if (minOrderUnit === undefined) {
        minOrderUnit = hot.getDataAtProp('Minimum Order Unit')[rowIndex];
        file = true;
    }
    var minLineOrder = "";
    if (file === false) {
        minLineOrder = getVendorMinLineOrder(hot.getDataAtProp('SupplierName')[rowIndex]);
        if (minLineOrder !== '') {
            if (minOrderUnit === 'L' && orderBy === 'L') {

                hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, hot.getSettings().data[rowIndex]["CostPerLot"])

            }
            else {

                hot.getSettings().data[rowIndex]["MinimumOrderQuantity"] = calculateMinOrderQty(minLineOrder, hot.getSettings().data[rowIndex]["Cost"])

            }
        }
    }

    else {
        minLineOrder = getVendorMinLineOrder(hot.getDataAtProp('Vendor')[rowIndex]);
        OrderBy = hot.getSettings().data[rowIndex]["Order By"];
        if (minLineOrder !== '') {
            if (minOrderUnit === 'L' && OrderBy === 'L') {

                hot.getSettings().data[rowIndex]["Minimum Order Quantity"] = calculateMinOrderQty(minLineOrder, hot.getSettings().data[rowIndex]["Cost Per Lot"])

            }
            else {

                hot.getSettings().data[rowIndex]["Minimum Order Quantity"] = calculateMinOrderQty(minLineOrder, hot.getSettings().data[rowIndex]["Cost"])

            }
        }

    }

}

function initialize() {
    // popup window height
    // var ht = $('#hot-custom-field-update').handsontable('getInstance'); // click on the dropdown triangle button
    var ht = hot.getInstance();
    var sgtabHeight = $('.sg-tab-bar').height() + 0;
    var panelHeight = $('.sg-grid-panel').height() + 0;
    var headerHeight = $('.fixedbar.header').height() + 0;
    var footerHeight = $('.fixedbar.footer').height() + 0;
    var filterdiv = $('.search-holder.top').height() + 0;
    var elmHeight = sgtabHeight + panelHeight + headerHeight + footerHeight;
    var htTypeHeight = elmHeight + filterdiv;

    ht.addHook('afterOnCellMouseDown', function (event, coords, TD) { // Header 
        if (coords.row == -2) {
            // .......ToDo };
        } else if (coords.row >= 0) {
            // is a cell is not a header
            var myoffsetTop = TD.offsetTop; // offset: depend by scroll position of the cell in the grid 
            var myCellHeight = TD.clientHeight; // cell height 
            MoveHandTypeCellWindow(myoffsetTop, myCellHeight);
        }
    }); // In case of Return or F2 on the cell 
    ht.addHook('afterBeginEditing', function (row, column) {
        var plugin = ht.getPlugin('AutoRowSize'); // get first visible row e cell height 
        var firstVisRow = plugin.getFirstVisibleRow();
        var myCellHeight = ht.getRowHeight(row); // offset: 90 in my button bar, 130 the header 1 height, I add also the header2 height (+ myCellHeight) // (row - firstVisRow) * myCellHeight): height in pixel of the visible rows 
        var myoffsetTop = elmHeight + filterdiv + myCellHeight + ((row - firstVisRow) * myCellHeight)
        MoveHandTypeCellWindow(myoffsetTop, myCellHeight);

    });
    function MoveHandTypeCellWindow(myoffsetTop, myCellHeight) { // dim local 
        // var myHeight = window.innerHeight - 90 - 10; // -90 header height -10 gap to be more sure 
        var myHeight = window.innerHeight - elmHeight - filterdiv - 160;
        var myNewTop = 0; // new start position of the window popup // if the popup window is not all visible 
        if (myoffsetTop + htTypeHeight > myHeight) {
            myNewTop = -htTypeHeight - myCellHeight + 50; // negative value so the start position of the popup is above and is egual to the window height - 5 gap to be more sure. 
        }
        else {
            myNewTop = 1;
        }// take rule .handsontable .listbox from the css
        var mysheet = document.styleSheets[0];
        var myrules = mysheet.cssRules ? mysheet.cssRules : mysheet.rules; // loop on all the css rules 
        for (i = 0; i < myrules.length; i++) {
            if (myrules[i].selectorText == ".handsontable .listbox") {
                // find 
                myrules[i].style.top = myNewTop + "px"; // change the top break;
            }
        }
    }

}
