﻿var queryStringForPC = "";

$(document).ready(function () {
     queryStringForPC = getQueryStringByName("Id");
    LoadDatatableProduct(queryStringForPC);
});


function LoadDatatableProduct(selectedId) {
    $('#PIMProducts').dataTable({
        "destroy": true,
        "bJQueryUI": true,
        "bServerSide": true,
        "bFilter": true,
        "stateSave": true,
        //"scrollX": true,
       // "scrollY":"400px;",
        "bSort": false,
        "bScrollCollapse": true,
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": tableToolsPath,
            "aButtons": tableToolsOptions
        },
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlListPIMProductKit + "?selectedId=" + selectedId,
        "sServerMethod": "GET",

        // Initialize our custom filtering buttons and the container that the inputs live in
         filterOptions: { searchButton: "ProductKitCategorySearchText", clearSearchButton: "ProductKitCategoryClearSearch", searchContainer: "ProductKitCategorySearchContainer" },
        "fnPreDrawCallback": function () {
            // gather info to compose a message
            var ob = this;
            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
            //SGtableloading('fa-plane', '', this);
            

        },
        "fnDrawCallback": function (settings) {
            var divid = this.closest('.dataTables_wrapper').attr('id');
            var dataInDataTable = $('#PIMProducts  tbody tr td').length;
            if (dataInDataTable > 1) {
                //data on grid
                $('#RemoveFromThisCategory').attr("disabled", false);

            } else {
                $('#RemoveFromThisCategory').attr("disabled", true);
            }
            var table = $('#PIMProducts');

            table.find('tbody tr:eq(0) td').each(function (index) {
                var widthrow = $(this).width();
                var theader = table.find('th:eq(' + index + ')');
                theader.css({ 'width': widthrow });
            });
             $($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust();
            SGtableloadingRemove(divid);
            rebuilttooltip();
           
        }
    });
   
}
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var table = $('#PIMProducts');

    table.find('tbody tr:eq(0) td').each(function (index) {
        var widthrow = $(this).width();
        var theader = table.find('th:eq(' + index + ')');
        theader.css({ 'width': widthrow });
    });
    $($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust();
});
$('#RemoveFromThisCategory').click(function () {
    eachIds = [];
    var selector = 'input[type=checkbox][class=PIMProductKitCategorySelector]';
    $(selector)
        .each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });

    if (eachIds.length == 0) {
        alertmsg('Please select at least one Product/Kit');
        return;
    }

    $.ajax({
        url: urlDeleteProductKit,
        data: {
            'idList': eachIds, 'CategoryId': queryStringForPC },
        type: 'POST',
        beforeSend: function () {
            SGloading("Loading");
        },
        success: function (data) {
            if (data && data.Status === true) {
                udh.notify.success(data.Message);
                $('#PIMProducts').dataTable().fnReloadAjax();
                if ($('#selectAllCheckBoxPIMProducts').prop("checked") == true) {
                    $("#selectAllCheckBoxPIMProducts").prop("checked", false);
                }
            } else {
                udh.notify.error(data.Message);
            }

        },
        complete: function () {
            eachIds = [];
            SGloadingRemove();
           

        }
    });
});
$(".nav-tabs a[data-toggle=tab]").on("click", function (e) {
   
    if ($(this).parent().hasClass("disabled")) {
        e.preventDefault();
        return false;
    }
});

$('#PIMProducts tbody').on('click', 'tr', function (e) {
    if (e.target.tagName == 'TD') {
        e.stopPropagation();
    }
});
