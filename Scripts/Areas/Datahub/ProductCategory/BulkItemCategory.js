﻿var queryStringForPC = "";

$(document).ready(function () {
    queryStringForPC = getQueryStringByName("Id");
    LoadSkusForProductCategory();
});

LoadSkusForProductCategory();

function LoadSkusForProductCategory() {

    $('#BulkLoadSkuForCategory').dataTable({
        "destroy": true,
        "bJQueryUI": true,
        "bServerSide": true,
        "bFilter": true,
        scrollResize: true,
       // "scrollX": true,
        "bSort": false,
        "bScrollCollapse": true,
        "bsearch": false,
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": tableToolsPath,
            "aButtons": tableToolsOptions
        },
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        //"data": { "searchParameter": searchModel },
        "sAjaxSource": urlLoadSkuForProductCategory ,

        "sServerMethod": "GET",


        // Initialize our custom filtering buttons and the container that the inputs live in
        // filterOptions: { searchButton: "ProductKitCategorySearchText", clearSearchButton: "ProductKitCategoryClearSearch", searchContainer: "ProductKitCategorySearchContainer" },
        "fnPreDrawCallback": function () {
            // gather info to compose a message
            var ob = this;
            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
            //SGtableloading('fa-plane', '', this);


        },
        "fnDrawCallback": function (settings) {
            // in case your overlay needs to be put away automatically you can put it here
            var divid = this.closest('.dataTables_wrapper').attr('id');
            var dataInDataTable = $('#BulkLoadSkuForCategory  tbody tr td').length;
            if (dataInDataTable > 1) {
                //data on grid
                $('#btnCatalogBulkCategory').attr("disabled", false);

            } else {
                $('#btnCatalogBulkCategory').attr("disabled", true);
            }
            SGtableloadingRemove(divid);
            rebuilttooltip();
        }
    });
        

}

$('#SearchText').click(function () {

    //if ($("#SearchText").is(":disabled") === true) {
    //    return;
    //}

   // $('#SearchText').attr("disabled", "disabled");
    //$("#selectAllCheckBox").prop("checked", false);
    //$('.productCatalogSelector').prop("checked", false);
    var searchModel = getSearchParameterViewModel();


    $('#BulkLoadSkuForCategory').dataTable({
        "destroy": true,
        "bJQueryUI": true,
        "bServerSide": true,
        "bFilter": true,
        "scrollX": true,
        "bSort": false,
        "bScrollCollapse": true,
        "bsearch": false,

        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": tableToolsPath,
            "aButtons": tableToolsOptions
        },
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        //"data": { "searchParameter": searchModel },
        "sAjaxSource": urlLoadSkuForProductCategory + "?productCategoryId=" + queryStringForPC +"&MatchingCriteria=" + searchModel.MatchingCriteria + '&KeySearch=' + searchModel.KeySearch + '&Search=' + searchModel.Search,
        
        "sServerMethod": "GET",
       

        // Initialize our custom filtering buttons and the container that the inputs live in
        // filterOptions: { searchButton: "ProductKitCategorySearchText", clearSearchButton: "ProductKitCategoryClearSearch", searchContainer: "ProductKitCategorySearchContainer" },
        "fnPreDrawCallback": function () {
            // gather info to compose a message
            var ob = this;
            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
            //SGtableloading('fa-plane', '', this);


        },
        "fnDrawCallback": function (settings) {
            // in case your overlay needs to be put away automatically you can put it here
            var divid = this.closest('.dataTables_wrapper').attr('id');
            var dataInDataTable = $('#BulkLoadSkuForCategory  tbody tr td').length;
            if (dataInDataTable > 1) {
                //data on grid
                $('#btnCatalogBulkCategory').attr("disabled", false);

            } else {
                $('#btnCatalogBulkCategory').attr("disabled", true);
            }
            SGtableloadingRemove(divid);
            rebuilttooltip();
        }
    });

});

function getSearchParameterViewModel() {
    var searchParameterModel = {
        MatchingCriteria: $("#matchingCriteria").val(),
        KeySearch: $("#KeySearch").val(),
        Search: $("#fullTextSearch").val()
       
    }
    return searchParameterModel;
}

$('#btnAddSelected').click(function () {
    eachIds = [];
    var selector = 'input[type=checkbox][class=ProductKitSelector]';
    $(selector)
        .each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });



    if (eachIds.length == 0) {
        alertmsg('red','Please select at least one Product/Kit');
        return;
    }

    $.ajax({
        url: urlAddSelectedItemToCategory,
        data: { "selectedProducts": eachIds.join(","), "categoryId": queryStringForPC },
        type: "POST",
        async: false,
        beforeSend: function () {
            SGtableloading(faIcon, '', this);
        },
        success: function (data) {
            if (data.Status == true) {
                alertmsg("green", data.Message);
                SGtableloadingRemove();
                LoadDatatableProduct(queryStringForPC);
            }
            else {
                alertmsg("red", data.Message);
            }
        },
        error: function (msg) {
            alertmsg("red", msg);
        },
        complete: function () {

            SGtableloadingRemove();
            $('#SearchText').click();
        }


    });

})

$('#btnAddAll').click(function () {
    var searchModel = getSearchParameterViewModel();

    $.ajax({
        url: urlAddAllItemToCategory,
        data: { "searchParameter": searchModel, "categoryId": queryStringForPC},
        type: "POST",
        async: false,
        beforeSend: function () {
            SGtableloading(faIcon, '', this);
        },
        success: function (data) {
            if (data.Status == true) {
                alertmsg("green", data.Message);
                SGtableloadingRemove();
                LoadDatatableProduct(queryStringForPC);
            }
            else {
                alertmsg("red", data.Message);
            }
        },
        error: function (msg) {
            alertmsg("red", msg);
        },
        complete: function () {

            SGtableloadingRemove();
            $('#SearchText').click();
        }


    });

})


$(".nav-tabs a[data-toggle=tab]").on("click", function (e) {
    if ($(this).parent().hasClass("disabled")) {
        e.preventDefault();
        return false;
    }
});

$("#fullTextSearch").keydown(function (event) {
    if (event.keyCode == 13) {
        enterSearch = true;
        $("#SearchText").click();
    }
})

$('#BulkLoadSkuForCategory tbody').on('click', 'tr', function (e) {
    if (e.target.tagName == 'TD') {
        e.stopPropagation();
    }
});
