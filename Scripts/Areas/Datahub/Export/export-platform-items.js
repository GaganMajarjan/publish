﻿$(document).ready(function () {
    var platform = getQueryStringByName("Platform");
    var filter = ""; //insert, update or delete
    if ($('#filter-insert').prop('checked') === true)
        filter = "INSERT";
    else if ($('#filter-update').prop('checked') === true)
        filter = "UPDATE";
    else
        filter = "DELETE";

    var searchCriteria = "Both";
    if ($("#searchCriteria-Product").prop('checked') === true)
        searchCriteria = "Product";
    else if (("#searchCriteria-Kit").prop('checked') === true)
        searchCriteria = "Kit";

    var table = $('#platformItemDataTable').DataTable({
        "bFilter": true,
        "deferRender": true,
        "bServerSide": true,
        "bSort": false,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": urlGetPlatformItemsDataTable + "?Platform=" + platform + "&filter=" + filter + "&searchCriteria=" + searchCriteria,
        "destroy": true,
        "scrollX": true,
        "fnPreDrawCallback": function () {
            // gather info to compose a message
            var ob = this;
            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });

        },
        "fnDrawCallback": function (settings) {
            // in case your overlay needs to be put away automatically you can put it here
            var divid = this.closest('.dataTables_wrapper').attr('id');
            SGtableloadingRemove(divid);
            rebuilttooltip();
            $('#platformItemDataTable').show();
        }
    });

    var search = function () {

        platform = getQueryStringByName("Platform");
        filter = ""; //insert, update or delete
        if ($('#filter-insert').prop('checked') === true)
            filter = "INSERT";
        else if ($('#filter-update').prop('checked') === true)
            filter = "UPDATE";
        else
            filter = "DELETE";

        var searchCriteria = "Both";
        if ($("#searchCriteria-Product").prop('checked') === true)
            searchCriteria = "Product";
        else if ($("#searchCriteria-Kit").prop('checked') === true)
            searchCriteria = "Kit";

        var updatedUrl = urlGetPlatformItemsDataTable + "?Platform=" + platform + "&filter=" + filter + "&searchCriteria=" + searchCriteria;

        table.ajax.url(updatedUrl).load();

        $('#platformItemDataTable').show();

    }

    //$('#FilterText,#filter-insert,#filter-update,#filter-delete').click(function() {
    $('#FilterText').click(function () {
        search();
    });

    $('#ClearSearch').click(function () {
        $('#filter-insert').prop('checked', true);
        //$('#platformItemDataTable').hide();
        $('#searchCriteria-Product').prop('checked', true);
        search();

    });

    $('#btnApprove').click(function (e) {

        var eachIds = [];
        $('input[type=checkbox][class=platformItemselector]').each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });

        if (eachIds.length === 0) {
            alertmsg('red', 'Please select an item first');
            e.preventDefault();
            return;
        }

        $('#approve-platform-item-modal').modal('show');
    });

    $('#btnReject').click(function (e) {

        var eachIds = [];
        $('input[type=checkbox][class=platformItemselector]').each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });

        if (eachIds.length === 0) {
            alertmsg('red', 'Please select an item first');
            e.preventDefault();
            return;
        }

        $('#reject-platform-item-modal').modal('show');
    });

    $('#btn-approve-platform-item-modal-yes').click(function (e) {
        var eachIds = [];
        $('input[type=checkbox][class=platformItemselector]').each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });

        if (eachIds.length === 0) {
            alertmsg('red', 'Please select an item first');
            e.preventDefault();
        }

        var productIdList = eachIds.toString();

        var filter = ""; //insert, update or delete
        if ($('#filter-insert').prop('checked') === true)
            filter = "INSERT";
        else if ($('#filter-update').prop('checked') === true)
            filter = "UPDATE";
        else
            filter = "DELETE";

        $.ajax({
            url: urlApprovePlatformItems,
            type: 'POST',
            data: { "platform": platform, "productIdList": productIdList, "filter": filter },
            dataType: "json",
            beforeSend: function () {
                SGloading('Loading');
            },
            success: function (result) {
                if (result != null && result.Status === true) {
                    $('#approve-platform-item-modal').modal('hide');
                    alertmsg('green', result.Message);
                    search();
                } else if (result != null && result.Status === false) {
                    alertmsg('red', result.Message);
                } else {
                    alertmsg('red', errorOccured);
                }
            },
            complete: function () {
                SGloadingRemove();
            },
            error: function (err) {
                //console.log(err);
            }
        });
    });

    $('#btn-reject-platform-item-yes').click(function (e) {
        var eachIds = [];
        $('input[type=checkbox][class=platformItemselector]').each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });

        if (eachIds.length === 0) {
            alertmsg('red', 'Please select an item first');
            e.preventDefault();
        }

        var productIdList = eachIds.toString();

        var filter = ""; //insert, update or delete
        if ($('#filter-insert').prop('checked') === true)
            filter = "INSERT";
        else if ($('#filter-update').prop('checked') === true)
            filter = "UPDATE";
        else
            filter = "DELETE";

        $.ajax({
            url: urlRejectPlatformItems,
            type: 'POST',
            data: { "platform": platform, "productIdList": productIdList, "filter": filter },
            dataType: "json",
            beforeSend: function () {
                SGloading('Loading');
            },
            success: function (result) {
                if (result != null && result.Status === true) {
                    $('#reject-platform-item-modal').modal('hide');
                    alertmsg('green', result.Message);
                    search();
                } else if (result != null && result.Status === false) {
                    alertmsg('red', result.Message);
                } else {
                    alertmsg('red', errorOccured);
                }
            },
            complete: function () {
                SGloadingRemove();
            },
            error: function (err) {
                //console.log(err);
            }
        });
    });
});