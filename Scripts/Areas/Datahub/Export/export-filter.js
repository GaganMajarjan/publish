﻿
$('#builder').queryBuilder(jsonRawSearch);
//alert(ko.toJSON(jsonRawSearch));
var rawJsonFilters = jsonRawSearch.filters;

var CheckVM = function (check, colname) {
    var me = this;

    me.Check = ko.observable(check);
    me.ColName = ko.observable(colname);
    //me.ColType = 
}

//KnockoutJS MVVM
var ReportSearchVM = function () {
    var self = this;

    self.myData = ko.observableArray([]);

    self.Cols = ko.observableArray([]);

    self.ReportName = ko.observable();

    self.ReportCategoryName = ko.observable();

    self.MasterCheck = ko.observable();

    self.MasterCheck.subscribe(function (val) {
        self.Cols().forEach(function (obj) {
            obj.Check(val);
        });
    });

    self.ShowHide = ko.observable();

    self.filterOptions = {
        filterText: ko.observable(""),
        useExternalFilter: true
    };

    self.pagingOptions = {
        pageSizes: ko.observableArray([25, 50, 100]),
        pageSize: ko.observable(25),
        totalServerItems: ko.observable(0),
        currentPage: ko.observable(1)
    };

    self.setPagingData = function (data, page, pageSize) {
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        self.myData(pagedData);
        self.pagingOptions.totalServerItems(data.length);
    };

    //self.getPagedDataAsync = function (pageSize, page, searchText) {
    //    setTimeout(function () {
    //        var data;
    //        if (searchText) {
    //            var ft = searchText.toLowerCase();
    //            $.getJSON(urlLoadData, function (largeLoad) {
    //                data = largeLoad.filter(function (item) {
    //                    return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
    //                });
    //                self.setPagingData(data, page, pageSize);
    //            });
    //        } else {
    //            $.getJSON(urlLoadData, function (largeLoad) {
    //                self.setPagingData(largeLoad, page, pageSize);
    //            });
    //        }
    //    }, 100);
    //};

    //self.filterOptions.filterText.subscribe(function (data) {
    //    self.getPagedDataAsync(self.pagingOptions.pageSize(), self.pagingOptions.currentPage(), self.filterOptions.filterText());
    //});

    //self.pagingOptions.pageSizes.subscribe(function (data) {
    //    self.getPagedDataAsync(self.pagingOptions.pageSize(), self.pagingOptions.currentPage(), self.filterOptions.filterText());
    //});
    //self.pagingOptions.pageSize.subscribe(function (data) {
    //    self.getPagedDataAsync(self.pagingOptions.pageSize(), self.pagingOptions.currentPage(), self.filterOptions.filterText());
    //});
    //self.pagingOptions.totalServerItems.subscribe(function (data) {
    //    self.getPagedDataAsync(self.pagingOptions.pageSize(), self.pagingOptions.currentPage(), self.filterOptions.filterText());
    //});
    //self.pagingOptions.currentPage.subscribe(function (data) {
    //    self.getPagedDataAsync(self.pagingOptions.pageSize(), self.pagingOptions.currentPage(), self.filterOptions.filterText());
    //});

    //self.getPagedDataAsync(self.pagingOptions.pageSize(), self.pagingOptions.currentPage());

    self.gridOptions = {
        data: self.myData,
        enablePaging: true,
        pagingOptions: self.pagingOptions,
        filterOptions: self.filterOptions,
        showGroupPanel: true,
        jqueryUIDraggable: true,
        isMultiSelect: false
    };

    self.SelectedCols = ko.computed(function () {
        var returnValue = "";
        self.Cols().forEach(function (col) {
            //check if column has been checked
            if (col.Check() == true) {
                //if checked, add column name to the collection
                returnValue += col.ColName() + ",";
            }
        });

        // check if returnValue contains ,
        if (returnValue.indexOf(',') != -1) {
            //remove last occurance of ,
            returnValue = returnValue.substring(0, returnValue.length - 1);
        }
        return returnValue;
    });

    function loadDataTable(query, param) {

        query = query.replace("\n", " ");

        $('#reportResultDataTable').dataTable({
            "deferRender": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "destroy": true,
            "bSort": false,
            "sAjaxSource": urlLoadDataTable + "?query=" + query + "&parameters=" + param,
            "bProcessing": true,
            "scrollX": true

        });
    }

    function initializeTableHeaders(query, param) {
        $.ajax({
            "dataType": 'json',
            "type": "GET",
            "url": urlGetTableSchema,
            "data": { sqlString: query },
            "success": function (data) {

                data = "[" + data + "]";
                data = JSON.parse(data);
                var jsonResultData = ConvertJsonToTable(data, "reportResultDataTable", "display", null);
                $('#reportResultDiv').html(jsonResultData);
                $('#reportResultDiv tbody tr').remove();

                loadDataTable(query, param);

                //get reference to button
                var button = $("#btnLoadData");
                button.removeClass("disabled");
            },

            "error": function (err) {
                alertmsg("red", "Report Columns not found.");
            }
        });

    }

    //Load data
    self.LoadData = function () {

        //console.log('inside Load Data');
        //prepare javascript sql
        parseSql();
        //console.log('After Parse');
        //get javascript sql
        var query = $("#SQLResult").text();
        //console.log('after SQLResult');
        //console.log(query);
        //validate javascript sql
        if (query == null)
            return;
        if (query === "")
            return;
        //console.log('before GetSQL');

        //get parametrized query
        var a = $('#builder').queryBuilder('getSQL', 'numbered');
        //console.log('After Get SQL');

        //extract parameters from the query
        var parameters = a.params;
        //console.log('After a.params');

        //get reference to button
        var button = $("#btnLoadData");

        initializeTableHeaders(query, JSON.stringify(parameters));

        //console.log('End');

    };



    self.LoadTable = function () {
        //get table name
        var tableName = $("#Table").text();

        //table name validation
        if (tableName == null)
            return;
        if (tableName == "")
            return;

        //get reference to button
        var button = $("#btnLoadTable");

        //ajax request
        $.ajax({
            type: 'POST',
            url: urlLoadTable,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'tableName': tableName },
            dataType: 'json',
            beforeSend: function () {
                //disable button before send
                button.addClass("disabled");//.button('reset');
            },
            success: function (response) {
                response = '[' + response + ']';
                var a = response.toString();
                try {
                    $('#builder').queryBuilder(response);
                } catch (e) {
                    //console.log('error: ' + e);
                }
            },
            complete: function () {
                //enable button after complete
                button.removeClass("disabled");//.button('reset');
            },
            error: function (xhr, status, error) {
                //reset variables on error OR show/log appropriate message
                alert(status + ' ' + error);
            }
        });
    };

    self.SaveReport = function () {

        //prepare javascript sql
        parseSql();

        //get javascript sql
        var query = $("#SQLResult").text();

        //validate javascript sql
        if (query == null)
            return;
        if (query == "")
            return;

        //get parametrized query
        var a = $('#builder').queryBuilder('getSQL', 'numbered');
        //console.log(a);

        var paramDataTypes = "";

        //get data types for parameters
        var res = $('#builder').queryBuilder('getRules');
        if (res != null) {
            var rules = res.rules;
            $.each(rules, function(key, value) {
                var dataType = value.type;
                paramDataTypes += dataType + ",";
            });
        }
        //console.log(paramDataTypes);
        
        //extract parameters from the query
        var parameters = a.params;

        //get reference to button
        var button = $("#btnSaveReport");

        //call ajax
        $.ajax({
            type: 'POST',
            url: urlSaveReport,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'query': query, 'parameters': JSON.stringify(parameters), 'reportName': self.ReportName(), 'paramDataTypes': paramDataTypes, 'reportCategoryName':self.ReportCategoryName() },
            dataType: 'json',
            beforeSend: function () {
                //disable button before send
                button.addClass("disabled");//.button('reset');
            },
            success: function (response) {
                try {
                    if (response != null) {

                        //console.log(response);

                        if (response.ErrorMessage) {
                            alertmsg('red', response.ErrorMessage);
                            return;
                        }

                        if (response.Value == true) {
                            //If we reach here, everything is fine. Show success message
                            alertmsg('green', jsSuccess);
                            //console.log('::' + jsSuccess);
                        }
                        else {
                            alertmsg('red', jsFail);
                            //console.log('::' + jsFail);
                        }
                    }
                    else {
                        alertmsg('red', jsFail);
                    }
                }
                catch (e) {
                    //console log error
                    //console.log(e);
                    alertmsg('red', jsError);
                }
            },
            complete: function () {
                //enable button after complete
                button.removeClass("disabled");//.button('reset');

                //check array length and show/log appropriate message
                if (self.myData().length == 0) {
                    //alertmsg('red', jsNoRecordFound);
                    //console.log(jsNoRecordFound);
                }
            },
            error: function (xhr, status, error) {
                //reset variables on error OR show/log appropriate message
                //console.log('xhr: ' + xhr + ' ; status: ' + status + ' ; error: ' + error);
                alert(status + ' ' + error);
            }
        });
    };

    self.CanSaveReport = ko.computed(function () {
        return self.ReportName() && self.ReportCategoryName();
    });
}

//KnockoutJS apply bindings
var vm = new ReportSearchVM();
ko.applyBindings(vm);
$.each(rawJsonFilters, function (key, value) {
    //console.log(value.id);
    vm.Cols.push(new CheckVM(false, value.id));
});


// reset builder
$('.reset').on('click', function () {
    $('#builder').queryBuilder('reset');
    $('#result').empty().addClass('hide');
});

// get rules
$('.parse-json').on('click', function () {
    var res = $('#builder').queryBuilder('getRules');
    $('#result').removeClass('hide')
      .find('pre').html(
        JSON.stringify(res, null, 2)
      );
});

//parse sql
var parseSql = function () {
    //console.log('start of parseSQL method');
    var res = $('#builder').queryBuilder('getSQL', $(this).data('stmt'));
    //console.log(res);

    $('#result').removeClass('hide')
      .find('pre').html(
        res.sql + (res.params ? '\n\n' + JSON.stringify(res.params, null, 2) : '')
      );

    var numberedOutput = $('#builder').queryBuilder('getSQL', 'numbered');
    var simpleOutput = $('#builder').queryBuilder('getSQL', false);
    //console.log(numberedOutput);
    //get output
    var output = ko.toJSON(res);
    //check for valid sql
    if (res.sql === "") {
        //$("#SQLResult").text("");

        var tbl = $('#Table').text();

        var sqlQuery = "SELECT " + vm.SelectedCols() + " FROM " + tbl;
        $("#SQLResult").text(sqlQuery);
        $("#SQLParam").text("");
    } else {
        var tbl = $('#Table').text();
        //var sql = "SELECT UserId,Name,AddressLine1,AddressLine2,Email,City,State,Zip FROM " + tbl + " WHERE ";
        var sql = "SELECT " + vm.SelectedCols() + " FROM " + tbl + " WHERE ";



        $("#SQLResult").text(sql + numberedOutput.sql);
        //console.log(sql + numberedOutput.sql);
        $("#SQLParam").text(JSON.stringify(numberedOutput.params));
    }

//console.log('end of parseSQL method');
};

$('.parse-sql').on('click', parseSql);
