﻿



//KnockoutJS MVVM
try {

    var ReportVM = function () {
        var self = this;

        self.Table = ko.observable();
        //console.log("above");
        //console.log(self.Table());

        self.CanSearch = ko.computed(function () {

            if (self.Table() == null || self.Table() === "") {
                //console.log('false');
                return false;
            }

            //console.log('true');
            return true;
        });

        self.LoadTable = function () {
            //get table name
            var tableName = $("#Table").val();

            //console.log(tableName);

            //table name validation
            if (tableName == null)
                return;
            if (tableName == "")
                return;

            //ajax request
            $.ajax({
                type: 'POST',
                url: urlLoadTable,
                cache: false,
                //contentType: 'application/json; charset=utf-8',
                data: { 'tableName': tableName },
                dataType: 'json',
                beforeSend: function () {
                    $("#btnLoadTable").addClass("disabled");//.button('reset');
                },
                success: function (response) {
                    response = '[' + response + ']';
                    var a = response.toString();
                    //console.log(a);
                    try {
                        $('#builder').queryBuilder(response);
                    } catch (e) {
                        //console.log('Error: ' + e);
                    }
                },
                complete: function () {
                    $("#btnLoadTable").removeClass("disabled");//.button('reset');
                    //$("#btnLoadTable").button('reset');
                },
                error: function (xhr, status, error) {
                    //reset variables here
                    alert(status + ' ' + error);
                }
            });
        };

    }

    //KnockoutJS apply bindings
    ko.applyBindings(new ReportVM());

    var substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            var matches, substrRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function (i, str) {
                if (substrRegex.test(str)) {
                    // the typeahead jQuery plugin expects suggestions to a
                    // JavaScript object, refer to typeahead docs for more info
                    matches.push({ value: str });
                }
            });

            cb(matches);
        };
    };
    //type ahead
    $('#Table').typeahead(
        {
            hint: false,
            highlight: true,
            minLength: 1
        },
    {
        name: 'table',
        displayKey: 'value',
        source: substringMatcher(tablesList)

    });


} catch (e) {
    alert(e);
}