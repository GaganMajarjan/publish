﻿$(document).ready(function () {
    if (pricingQualifierMessage != null && pricingQualifierMessage != "") {
        if (pricingQualifierMessage != null) {
            alertmsg("green", pricingQualifierMessage);
        } 
    }


$('#pricingQualifierTable').DataTable({
    "destroy": true,
    "scrollX": true,
    "bServerSide": true,
    "aLengthMenu": dataTableMenuLength,
    "iDisplayLength": dataTableDisplayLength,
    "bProcessing": true,
    "sAjaxSource": urlGetPricingQualifierDataTable,
    "aoColumns": [
                   {

                       "bSearchable": false,
                       "bSortable": false,
                       "mRender": function (data, type, full) {
                           return '&nbsp;&nbsp;&nbsp;<input type="checkbox" id="selectOneCheckBox" class="listPricingQualifier" value="' + data + '">'

                       }
                   },
                   {

                       "bSortable": true,
                       "sName": "Name"
                   },
                   {

                       "bSortable": false,
                       "sName": "QuantityFrom"
                   },

                    {

                        "bSortable": false,
                        "sName": "QuantityTo"
                    },


                     {

                         "bSortable": false,
                         "sName": "EffectiveDateStart"
                     },


                      {

                          "bSortable": false,
                          "sName": "EffectiveDateEnd"
                      },

                        {

                            "bSortable": false,
                            "sName": "PlatformSdvId"
                        },

                      
                       
                        {

                            "bSortable": false,
                            "sName": "Status"
                        },
                         {

                             "bSortable": false,
                             "sName": "QualifiedForName"
                         }


    ]
});



$('#btnCreatePricingQualifier').click(function () {
    window.location.href = urlCreatePricingQualifier;
});

$('#btnUpdatePricingQualifier').click(function () {
    var pricingQualifierId;
    var count = 0;
    $('input[type=checkbox][class=listPricingQualifier]').each(function () {
        if (this.checked) {
            pricingQualifierId = this.value;
            count++;
        }
    });
    if (count != 1) {
        alertmsg('red', 'Please select one pricing qualifier');
        return;
    }

    window.location.href = urlEditPricingQualifier + "?id=" + pricingQualifierId;

});

$('#btnDeletePricingQualifier').click(function () {
    var pricingQualifierIds = "";
    var count = 0;
    $('input[type=checkbox][class=listPricingQualifier]').each(function () {
        if (this.checked) {
            pricingQualifierIds += this.value + ",";
            count++;
        }
    });
    if (count == 0) {
        alertmsg('red', 'Please select one pricing qualifier');
        return;
    }

    else {
        $("#pricingQualifierIdsforDelete").val(pricingQualifierIds);
        $('#divConfirmDelete').modal("show");
    }
});

$('#divConfirmDeleteYes').click(function () {
    var pricingQualifierIds = $("#pricingQualifierIdsforDelete").val();

    // Ajax call
    $.ajax({
        type: 'POST',
        url: urlDeletePricingQualifier,
        cache: false,
        data: { 'commaSeparatedPricingQualifierIds': pricingQualifierIds },
        dataType: 'json',
        success: function (response) {
            if (response == "OK") {
                location.reload();
                alertmsg("green", "Pricing Qualifier(s) deleted successfully");
            } else if (response == "INCOMPLETE") {

                alertmsg("red", "Not able to delete all the selected pricing qualifiers.");
            } else if(response == "FAILURE") {
                alertmsg("red", "Failure while deleting the pricing qualifiers");
            }
            else {
                alertmsg("red", response);
            }
        },
        error: function (err) {
            location.reload();
            alertmsg("red", "Error while deleting the pricing qualifiers");
        }
    });


});

$('#selectAllCheckBoxPricingQualifier').click(function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.listPricingQualifier').prop("checked", true);
    } else {
        $('.listPricingQualifier').prop("checked", false);
    }
});


});