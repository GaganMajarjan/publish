﻿var staticDataValueGrid = "";
$(document).ready(function () {
    var counter = 0;
    var id = 0;

    

    function initializestaticdatatype() {

        $('#StaticDataTypeGrid').DataTable({
            "destroy": true,
            "bProcessing": true,
            "bJQueryUI": true,
            "bServerSide": true,
            //"bFilter": false,
            "scrollX": true,
            "bSort": false,
            "bScrollCollapse": true,

            "aLengthMenu": dataTableMenuLength,
            "iDisplayLength": dataTableDisplayLength,
            "sAjaxSource": urlGetStaticDataTypeValues,
            "sServerMethod": "POST",

            "deferRender": true,
            "fnPreDrawCallback": function () {
                // gather info to compose a message
                var ob = this;
                SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                //SGtableloading('fa-plane', '', this);


            },
            "fnDrawCallback": function () {
                // in case your overlay needs to be put away automatically you can put it here
                var divid = this.closest('.dataTables_wrapper').attr('id');
                SGtableloadingRemove(divid)
            }
          
           
          
         
        });
    }

    function getSingleStaticDatatypeId() {
        counter = 0;
        $('input[type=checkbox][class=listStaticDataType]').each(function () {
            if (this.checked) {
                id = this.value;
                counter++;
            }
        });
        return id;
    }
    function initializeStaticDataValue() {



        var staticDataTypeId = $("#StaticDataTypeLoadId").val();

        if (staticDataTypeId === "" || counter !== 1) {
           // alertmsg('red', 'Please select one default value');
            return;
        }

        // Ajax call
        $.ajax({
            type: 'POST',
            url: urlGetStaticDataValue,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'staticDataTypeId': staticDataTypeId },
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {

                //response = JSON.parse(response);
                try {
                    $('#grid-static-data-value').html('');

                    try {
                        if (response.aaData[0][0] == "Error") {
                            alertmsg("red", "Unable to load");
                            return;
                        }
                    } catch (e) {
                        // do nothing, continue ahead
                    }

                    $('#grid-static-data-value').append('<table id="staticDataValueTable"><thead><tr></tr></thead></table>');
                    ////clear heading
                    //$('#testGrid thead tr').html('');
                    //add heading to table
                    $.each(response.sColumns, function (key, value) {
                        $('#staticDataValueTable thead tr').append('<th>' + value + '</th>');
                    });
                    
                    //add data to table via datatable
                    //if (staticDataTypeId == 7000) {
                    //    staticDataValueGrid = $('#staticDataValueTable').DataTable({
                    //        "sAjaxSource": urlGetStaticDataValueForDataTable + "?staticDataTypeId=" + staticDataTypeId,
                    //        "bProcessing": true,
                    //        "deferRender": true,
                    //        "destroy": true,
                    //        "bSort": false,
                    //        "bServerSide": true,
                    //        "scrollX": true,
                    //        "aLengthMenu": dataTableMenuLength,
                    //        "iDisplayLength": dataTableDisplayLength,
                    //        "aoColumns": [
                    //            {
                    //                "mRender": function (data, type, full) {
                    //                    return '<input type="checkbox" id="selectOneCheckBox" class="listStaticDataValue" value="' + data + '">';

                    //                }
                    //            },
                    //            {
                    //                "sName": "SourceUOM"
                    //            },
                    //            {


                    //                "sName": "TargetUOM"
                    //            },
                    //            {


                    //                "sName": "OrderBy"
                    //            }

                    //        ],
                    //        columnDefs: [
                    //            { width: '5%', targets: 0 }
                    //        ],
                    //        "fnPreDrawCallback": function () {
                    //            // gather info to compose a message
                    //            var ob = this;
                    //            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                    //            //SGtableloading('fa-plane', '', this);


                    //        },
                    //        "fnDrawCallback": function () {
                    //            // in case your overlay needs to be put away automatically you can put it here
                    //            var divid = this.closest('.dataTables_wrapper').attr('id');
                    //            SGtableloadingRemove(divid)
                    //        }

                    //    });
                    //}
                    //else {
                        staticDataValueGrid = $('#staticDataValueTable').DataTable({
                            "sAjaxSource": urlGetStaticDataValueForDataTable + "?staticDataTypeId=" + staticDataTypeId,
                            "bProcessing": true,
                            "deferRender": true,
                            "destroy": true,
                            "bSort": false,
                            "bServerSide": true,
                            "scrollX": true,
                            "aLengthMenu": dataTableMenuLength,
                            "iDisplayLength": dataTableDisplayLength,
                            "aoColumns": [
                                {
                                    "mRender": function (data, type, full) {
                                        return '<input type="checkbox" id="selectOneCheckBox" class="listStaticDataValue" value="' + data + '">';

                                    }
                                },
                                {
                                    "sName": "Name"
                                },
                                {


                                    "sName": "Description"
                                }],
                            columnDefs: [
                                { width: '5%', targets: 0 }
                            ],
                            "fnPreDrawCallback": function () {
                                // gather info to compose a message
                                var ob = this;
                                SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                                //SGtableloading('fa-plane', '', this);


                            },
                            "fnDrawCallback": function () {
                                // in case your overlay needs to be put away automatically you can put it here
                                var divid = this.closest('.dataTables_wrapper').attr('id');
                                SGtableloadingRemove(divid)
                            }

                        });
                    }
                //}
                catch (e) {
                    alert(e);
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });


    }


    initializestaticdatatype();

    if (staticDataMessage) {
        staticDataMessage = fixJsonQuote(staticDataMessage);
        var status = staticDataMessage.Status;
        var message = staticDataMessage.Message;

        if (status === true) {
            alertmsg("green", message);
        } else if (status === false) {
            alertmsg("red", message);
        } else {
            alertmsg("red", errorOccuredMessage);

        }
    }

    $('.expand-cat-one').click(function () {
        //$table.floatThead('reflow');
        initializestaticdatatype();
        TblpgntBtm();
        $('.expand-btn').hide();
        $('.collapse').show();
        $('.sg-catalog-one').show();
        $('.sg-catalog-one').removeClass('col-md-6').addClass('col-md-12');
        $('.sg-catalog-two').hide();

    });
    $('.expand-cat-two').click(function () {
        //$table.floatThead('reflow');
        var staticValueId = $("#StaticDataTypeLoadId").val();
        if (staticValueId === "") {
            return;
        }
        initializeStaticDataValue();
        TblpgntBtm();
        $('.expand-btn').hide();
        $('.collapse').show();
        $('.sg-catalog-two').show();
        $('.sg-catalog-two').removeClass('col-md-6').addClass('col-md-12');
        $('.sg-catalog-one').hide();
        
    });
    $('.collapse').click(function () {
        //$table.floatThead('reflow');

        $('.collapse').hide();
        $('.expand-btn').show();
        $('.sg-catalog-one, .sg-catalog-two').show();
        $('.sg-catalog-one').removeClass('col-md-12').addClass('col-md-6');
        $('.sg-catalog-two').removeClass('col-md-12').addClass('col-md-6');

        getSingleStaticDatatypeId();
        if (counter == 1) {
            initializeStaticDataValue();
        }
        initializestaticdatatype();
        TblpgntBtm();
    });


    $('body').on('click', '#StaticDataTypeGrid tr', function (e) {
      
        document.getElementById('StaticDataTypeLoadId').value = getSingleStaticDatatypeId();
        initializeStaticDataValue();
        TblpgntBtm();

    });

    $('#btnLoadStaticDataValue').click(function () {
        document.getElementById('StaticDataTypeLoadId').value = getSingleStaticDatatypeId();
        initializeStaticDataValue();
        TblpgntBtm();
    });

    $('#btnCreateStaticDataType').click(function () {
        window.location.href = urlCreateStaticDataType;
    });

    $('#btnUpdateStaticDataType').click(function () {
        var staticDataTypeId;
        var count = 0;
        $('input[type=checkbox][class=listStaticDataType]').each(function () {
            if (this.checked) {
                staticDataTypeId = this.value;
                count++;
            }
        });
        if (count != 1) {
            alertmsg('red', 'Please select one static data type');
            return;
        }

        //console.log(staticDataTypeId);
        window.location.href = urlEditStaticDataType + "?id=" + staticDataTypeId;

    });

    $('#btnDeleteStaticDataType').click(function () {
        var staticDataTypeIds = "";
        var count = 0;
        $('input[type=checkbox][class=listStaticDataType]').each(function () {
            if (this.checked) {
                staticDataTypeIds += this.value + ",";
                count++;
            }
        });
        if (count == 0) {
            alertmsg('red', 'Please select one static data type');
            return;
        }

        else {
            $("#idContainerHiddenFieldForDelete").val(staticDataTypeIds);
            $('#divConfirmDelete').modal("show");
        }
    });

    $('#divConfirmDeleteYes').click(function () {
        var staticDataTypeIds = $("#idContainerHiddenFieldForDelete").val();

        // Ajax call
        $.ajax({
            type: 'POST',
            url: urlDeleteStaticDataType,
            cache: false,
            data: { 'commaSeparatedStaticDataTypeIds': staticDataTypeIds },
            dataType: 'json',
            success: function (response) {
                if (response == "OK") {
                    initializestaticdatatype();
                    initializeStaticDataValue();
                    alertmsg("green", "Static Data Type(s) is deleted sucessfully");
                } else if (response == "INCOMPLETE") {

                    alertmsg("red", "Not able to delete all the selected types. Please delete the corresponding static data values first.");
                } else {
                    alertmsg("red", "Failure while performing the operation");
                }
            },
            error: function (err) {
                location.reload();
                alertmsg("red", "Error while performing the operation");
            }
        });


    });

    $('#divConfirmDeleteNo').click(function () {
        location.reload();
    });

    $('#selectAllCheckBoxStaticDataType').click(function () {
        var thisCheck = $(this);
        if (thisCheck.is(':checked')) {
            $('.listStaticDataType').prop("checked", true);
        } else {
            $('.listStaticDataType').prop("checked", false);
        }
    });

    //For Static Data Value

    $('#btnCreateStaticDataValue').click(function () {

        var staticDataTypeId = 0;
        var count = 0;
        $('input[type=checkbox][class=listStaticDataType]').each(function () {
            if (this.checked) {
                staticDataTypeId = this.value;
                count++;
            }
        });

        //if (count > 1) {
        //    alertmsg('red', 'Please select one static data type');
        //    return;
        //}

        window.location.href = urlCreateStaticDataValue + "?id=" + staticDataTypeId;
    });

    $('#btnUpdateStaticDataValue').click(function () {
        var staticDataValueId;
        var count = 0;
        $('input[type=checkbox][class=listStaticDataValue]').each(function () {
            if (this.checked) {
                staticDataValueId = this.value;
                count++;
            }
        });
        if (count != 1) {
            alertmsg('red', 'Please select one static data value');
            return;
        }

        window.location.href = urlEditStaticDataValue + "?staticDataValueId=" + staticDataValueId;
    });

    $('#btnDeleteStaticDataValue').click(function () {
        var staticDataValueIds = "";
        var count = 0;
        $('input[type=checkbox][class=listStaticDataValue]').each(function () {
            if (this.checked) {
                staticDataValueIds += this.value + ",";
                count++;
            }
        });
        if (count == 0) {
            alertmsg('red', 'Please select one static data value');
            return;
        }

        else {
            $("#idContainerHiddenFieldForDeleteStaticDataValues").val(staticDataValueIds);
            $('#divConfirmDeleteStaticDataValue').modal("show");
        }
    });

    $('#divConfirmDeleteStaticDataValueYes').click(function () {
        var staticDataValueIds = $("#idContainerHiddenFieldForDeleteStaticDataValues").val();

        // Ajax call
        $.ajax({
            type: 'POST',
            url: urlDeleteStaticDataValue,
            cache: false,
            data: { 'commaSeparatedStaticDataValueIds': staticDataValueIds },
            dataType: 'json',
            success: function (response) {
                if (response == "OK") {
                    initializeStaticDataValue();
                    alertmsg("green", "Static Data Value(s) is deleted sucessfully");
                } else {
                    alertmsg("red", "Failure while performing the operation");
                }
            },
            error: function (err) {
                location.reload();
                alertmsg("red", "Error while performing the operation");
            }
        });


    });

    $('#divConfirmDeleteStaticDataValueNo').click(function () {
        //location.reload();
    });

    $(document).on('click', '#selectAllStaticDataValue', function () {
        var thisCheck = $(this);
        if (thisCheck.is(':checked')) {
            $('.listStaticDataValue').prop("checked", true);
        } else {
            $('.listStaticDataValue').prop("checked", false);
        }
    });

});



