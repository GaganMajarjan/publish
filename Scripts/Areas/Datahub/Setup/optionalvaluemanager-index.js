﻿$(document).ready(function () {

    var counter = 0;
    var id = 0;
    var possibleDataTable;
    var selectedDataTable;

    function getSingleDefaultValueId() {
        counter = 0;
        $('input[type=checkbox][class=listDefaultValue]').each(function () {
            if (this.checked) {
                id = this.value;
                counter++;
            }
        });
        return id;
    }

    function getSingleDefaultValuePossibleId() {
        counter = 0;
        $('input[type=checkbox][class=defaultValuePossiblelist]').each(function () {
            if (this.checked) {
                id = this.value;
                counter++;
            }
        });
        return id;
    }

    function getSingleDefaultValueSelectedId() {
        counter = 0;
        $('input[type=checkbox][class=defaultValueSelectedlist]').each(function () {
            if (this.checked) {
                id = this.value;
                counter++;
            }
        });
        return id;
    }

    function initializeDefaultValue() {
        $('#DefaultValueGrid').DataTable({
            destroy: true,
            "scrollX": true,
            "aLengthMenu": dataTableMenuLength,
            "iDisplayLength": dataTableDisplayLength,
            "sAjaxSource": urlGetDefaultValue,
            "bServerSide": true,
            "bProcessing": true,
            "bSort": false,
            "bFilter": false,

            "aoColumns": [
                            {

                                "bSearchable": false,
                                "bSortable": false,
                                "mRender": function (data, type, full) {
                                    return '<input type="checkbox" class="listDefaultValue" value="' + data + '">'
                                }
                            },
                            {

                                "bSortable": false,
                                "sName": "Name"
                            },
                            {

                                "bSortable": false,
                                "sName": "Code"
                            },
                            {

                                "bSortable": false,
                                "sName": "Description"
                            }],
            "fnPreDrawCallback": function () {
                // gather info to compose a message
                var ob = this;
                SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                //SGtableloading('fa-plane', '', this);


            },
            "fnDrawCallback": function () {
                // in case your overlay needs to be put away automatically you can put it here
                var divid = this.closest('.dataTables_wrapper').attr('id');
                SGtableloadingRemove(divid)
            }
        });

    }

    function initializeDefaultValuePossible() {

        ///       var defaultValueId = getSingleDefaultValueId();
        var defaultValueId = $("#defaultValueLoadId").val();

        if (defaultValueId == "" || counter != 1) {
            alertmsg('red', 'Please select one default value');
            return;
        }


        // Ajax call
        $.ajax({
            type: 'POST',
            url: urlGetDefaultValuePossible,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'defaultValueId': defaultValueId },
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {

                //response = JSON.parse(response);
                try {
                    $('#grid-default-value-possible').html('');

                    try {
                        if (response.aaData[0][0] == "Error") {
                            alertmsg("red", "Unable to load");
                            return;
                        }
                    } catch (e) {
                        // do nothing, continue ahead
                    }

                    $('#grid-default-value-possible').append('<table id="defaultValuePossibleTable"><thead><tr></tr></thead></table>');
                    ////clear heading
                    //$('#testGrid thead tr').html('');
                    //add heading to table
                    $.each(response.sColumns, function (key, value) {
                        $('#defaultValuePossibleTable thead tr').append('<th>' + value + '</th>');
                    });



                    //add data to table via datatable
                    possibleDataTable = $('#defaultValuePossibleTable').DataTable({
                        "bProcessing": true,
                        "bServerSide": true,
                        "destroy": true,
                        "scrollX": false,
                        "bSort": false,
                        "bFilter": false,
                        "aLengthMenu": dataTableMenuLength,
                        "iDisplayLength": dataTableDisplayLength,
                        "sAjaxSource": urlGetDefaultValuePossible + "?defaultValueId=" + defaultValueId,
                        "sServerMethod": "POST",
                        "aoColumns": [
                        {
                            "sName": "Id",
                            "bSearchable": false,
                            "bSortable": false,
                            "width": "10",
                            "mRender": function (data, type, full) {
                                return '<input type="checkbox" class="defaultValuePossiblelist" value="' + data + '">'
                            }
                        },
                        {
                            "sName": "Value"
                        },
                        {
                            "sName": "Description"
                        }
                        ],
                        "fnPreDrawCallback": function () {
                            // gather info to compose a message
                            var ob = this;
                            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                            //SGtableloading('fa-plane', '', this);


                        },
                        "fnDrawCallback": function () {
                            // in case your overlay needs to be put away automatically you can put it here
                            var divid = this.closest('.dataTables_wrapper').attr('id');
                            SGtableloadingRemove(divid)
                        }
                    });
                }
                catch (e) {
                    alert(e);
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });
    }

    function initializeDefaultValueSelected() {
        //check-default-value-possible
        var defaultValueId = $("#defaultValueLoadId").val();

        if (defaultValueId == "" || counter != 1) {
            alertmsg('red', 'Please select one default value');
            return;
        }


        // Ajax call
        $.ajax({
            type: 'POST',
            url: urlGetDefaultValueSelected + "?defaultValueId=" + defaultValueId,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: { 'defaultValueId': defaultValueId },
            dataType: 'json',
            beforeSend: function () {
                //$("#btnsave").attr('disabled', true);
                SGloading('Loading');
            },
            success: function (response) {

                //response = JSON.parse(response);
                try {
                    $('#grid-default-value-selected').html('');

                    try {
                        if (response.aaData[0][0] == "Error") {
                            alertmsg("red", "Unable to load");
                            return;
                        }
                    } catch (e) {
                        // do nothing, continue ahead
                    }

                    $('#grid-default-value-selected').append('<table id="defaultValueSelectedTable"><thead><tr></tr></thead></table>');
                    ////clear heading
                    //$('#testGrid thead tr').html('');
                    //add heading to table
                    $.each(response.sColumns, function (key, value) {
                        $('#defaultValueSelectedTable thead tr').append('<th>' + value + '</th>');
                    });

                    //add data to table via datatable
                    selectedDataTable = $('#defaultValueSelectedTable').DataTable({
                        "sAjaxSource": urlGetDefaultValueSelected + "?defaultValueId=" + defaultValueId,
                        "bProcessing": true,
                        destroy: true,
                        "scrollX": false,
                        "bServerSide": true,
                        "bSort": false,
                        "bFilter": false,
                        "aLengthMenu": dataTableMenuLength,
                        "iDisplayLength": dataTableDisplayLength,
                        "aoColumns": [
                            {
                                "sName": "Id",
                                "width": "10",
                                "mRender": function (data, type, full) {
                                    return '<input type="checkbox" class="defaultValueSelectedlist" value="' + data + '">'
                                }
                            },

                       {
                           "sName": "Name"
                       },
                       {
                           "sName": "Value"
                       }
                        ],
                        "fnPreDrawCallback": function () {
                            // gather info to compose a message
                            var ob = this;
                            SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
                            //SGtableloading('fa-plane', '', this);


                        },
                        "fnDrawCallback": function () {
                            // in case your overlay needs to be put away automatically you can put it here
                            var divid = this.closest('.dataTables_wrapper').attr('id');
                            SGtableloadingRemove(divid)
                        }
                    });
                }
                catch (e) {
                    alert(e);
                }
            },
            complete: function () {
                //$("#btnsave").attr('disabled', false);
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                alert(status + ' ' + error);
            }
        });
    }

    initializeDefaultValue();

    $('#selectAllCheckBoxDefaultValue').click(function () {
        var thisCheck = $(this);
        if (thisCheck.is(':checked')) {
            $('.listDefaultValue').prop("checked", true);
        } else {
            $('.listDefaultValue').prop("checked", false);
        }
    });


    $('.expand-cat-one').click(function () {
        //$table.floatThead('reflow');
        $('.expand-btn').hide();
        $('.collapse').show();
        $('.sg-catalog-one').show();
        $('.sg-catalog-one').removeClass('col-md-4').addClass('col-md-12');
        $('.sg-catalog-two').hide();
        $('.sg-catalog-three').hide();

        initializeDefaultValue();
        TblpgntBtm();
        //InitializeDataTables();
    });
    $('.expand-cat-two').click(function () {
        //$table.floatThead('reflow');
        var defaultValueId = $("#defaultValueLoadId").val();
        if (defaultValueId == "") {
            return;
        }
        $('.expand-btn').hide();
        $('.collapse').show();
        $('.sg-catalog-two').show();
        $('.sg-catalog-two').removeClass('col-md-4').addClass('col-md-12');
        $('.sg-catalog-one').hide();
        $('.sg-catalog-three').hide();

        initializeDefaultValuePossible();
        TblpgntBtm();
    });
    $('.expand-cat-three').click(function () {
        //$table.floatThead('reflow');
        var defaultValueId = $("#defaultValueLoadId").val();
        if (defaultValueId == "") {
            return;
        }
        $('.expand-btn').hide();
        $('.collapse').show();
        $('.sg-catalog-three').show();
        $('.sg-catalog-three').removeClass('col-md-4').addClass('col-md-12');
        $('.sg-catalog-one').hide();
        $('.sg-catalog-two').hide();

        initializeDefaultValueSelected();
        TblpgntBtm();
    });
    $('.collapse').click(function () {
        //$table.floatThead('reflow');

        $('.collapse').hide();
        $('.expand-btn').show();
        $('.sg-catalog-one, .sg-catalog-two, .sg-catalog-three').show();
        $('.sg-catalog-one').removeClass('col-md-12').addClass('col-md-4');
        $('.sg-catalog-two').removeClass('col-md-12').addClass('col-md-4');
        $('.sg-catalog-three').removeClass('col-md-12').addClass('col-md-4');

        initializeDefaultValue();
        getSingleDefaultValueId();
        if (counter == 1) {
            initializeDefaultValuePossible();
            TblpgntBtm();
            initializeDefaultValueSelected();
            TblpgntBtm();
        }

        TblpgntBtm();
    });


    // operation default value
    $('#btnAddDefaultValue').click(function () {

        window.location.href = urlAddDefaultValue;
    });

    $('#btnDeleteDefaultValue').click(function () {
        var defaultValuePossibleId = "";
        var count = 0;
        $('input[type=checkbox][class=listDefaultValue]').each(function () {
            if (this.checked) {
                defaultValuePossibleId += this.value + ",";
                count++;
            }
        });
        if (count == 0) {
            alertmsg('red', 'Please select one default value');
            return;
        }

        else {
            $("#idContainerHiddenFieldForDeleteDefaultValue").val(defaultValuePossibleId);
            $('#divConfirmDeleteDefaultValue').modal("show");
        }
    });

    $('#divConfirmDeleteDefaultValueYes').click(function () {
        var defaultValueIds = $("#idContainerHiddenFieldForDeleteDefaultValue").val();

        //// Ajax call
        $.ajax({
            type: 'POST',
            url: urlDeleteDefaultValue,
            cache: false,
            data: { 'commaSeparatedDefaultValuesIds': defaultValueIds },
            dataType: 'json',
            success: function (response) {
                if (response == "OK") {
                    location.reload();
                    alertmsg("green", "Operation completed successfully");
                } else if (response == "INCOMPLETE") {

                    alertmsg("red", "Not able to delete all the selected types. Please delete the default value first.");
                } else {
                    alertmsg("red", "Failure while performing the operation");
                }
            },
            error: function (err) {
                location.reload();
                alertmsg("red", "Error while performing the operation");
            }
        });


    });

    $('#btnUpdateDefaultValue').click(function () {

        var defaultValueId = getSingleDefaultValueId();

        if (counter !== 1) {
            alertmsg('red', 'Please select one default value');
            counter = 0;
            return;
        }
        window.location.href = urlEditDefaultValue + "?id=" + defaultValueId;
    });

    //end operation default value




    //operation default value possible

    $('#btnLoadDefaultValuePossible').click(function () {

        document.getElementById('defaultValueLoadId').value = getSingleDefaultValueId();

        initializeDefaultValuePossible();
        TblpgntBtm();
    });

    $('#btnUpdateDefaultValuePossible').click(function () {

        var defaultValuePossibleId = getSingleDefaultValuePossibleId();

        if (counter !== 1) {
            alertmsg('red', 'Please select one default value possible');
            counter = 0;
            return;
        }
        window.location.href = urlEditDefaultValuePossible + "?id=" + defaultValuePossibleId;
    });

    $('#btnDeleteDefaultValuePossible').click(function () {
        var defaultValuePossibleId = "";
        var count = 0;
        $('input[type=checkbox][class=defaultValuePossiblelist]').each(function () {
            if (this.checked) {
                defaultValuePossibleId += this.value + ",";
                count++;
            }
        });
        if (count == 0) {
            alertmsg('red', 'Please select one default value');
            return;
        }

        else {
            $("#idContainerHiddenDeleteDefaultValuePossible").val(defaultValuePossibleId);
            $('#divConfirmDeleteDefaultValuePossible').modal("show");
        }
    });

    $('#divConfirmDeleteDefaultValuePossibleYes').click(function () {
        var defaultValueIds = $("#idContainerHiddenDeleteDefaultValuePossible").val();

        //// Ajax call
        $.ajax({
            type: 'POST',
            url: urlDeleteDefaultValuePossible,
            cache: false,
            data: { 'commaSeparatedDefaultValuesPossibleIds': defaultValueIds },
            dataType: 'json',
            success: function (response) {
                if (response == "OK") {
                    location.reload();
                    alertmsg("green", "Operation completed successfully");
                } else if (response == "INCOMPLETE") {

                    alertmsg("red", "Not able to delete all the selected types. Please delete the default value first.");
                } else {
                    alertmsg("red", "Failure while performing the operation");
                }
            },
            error: function (err) {
                location.reload();
                alertmsg("red", "Error while performing the operation");
            }
        });


    });

    $('#btnAddDefaultValuePossible').click(function () {

        var defaultValueId = getSingleDefaultValueId();
        if (counter !== 1) {
            alertmsg('red', 'Please select one default value ');
            counter = 0;
            return;
        }
        window.location.href = urlAddDefaultValuePossible + "?defaultValueId=" + defaultValueId;
    });

    // end operation default value possible





    //default value selected

    $('#btnAssignDefaultValue').click(function () {

        var defaultValuePossibleId = getSingleDefaultValuePossibleId();

        if (counter !== 1) {
            alertmsg('red', 'Please select one default value possible');
            return;
        }

        var defaultValueSelectedId = getSingleDefaultValueSelectedId();

        if (counter !== 1) {
            alertmsg('red', 'Please select one default value selected');
            return;
        }

        $.ajax({
            type: 'POST',
            url: urlAssignDefaultValue,
            cache: false,
            //contentType: 'application/json; charset=utf-8',
            data: {
                'defaultValuePossibleId': defaultValuePossibleId,
                'defaultValueSelectedId': defaultValueSelectedId
            },
            dataType: 'json',
            beforeSend: function () {

            },
            success: function (response) {

                alertmsg('green', response);
                $('#btnLoadDefaultValueSelected').click();

            },
            complete: function () {


            },
            error: function (xhr, status, error) {
                alert(status + ' ' + error);
            }
        });

    });

    $('#btnLoadDefaultValueSelected').click(function () {
        document.getElementById('defaultValueLoadId').value = getSingleDefaultValueId();
        initializeDefaultValueSelected();
        TblpgntBtm();
    });

    $('#btnDeleteDefaultValueSelected').click(function () {
        var defaultValueSelectedId = "";
        var count = 0;
        $('input[type=checkbox][class=defaultValueSelectedlist]').each(function () {
            if (this.checked) {
                defaultValueSelectedId += this.value + ",";
                count++;
            }
        });
        if (count == 0) {
            alertmsg('red', 'Please select one default value selected');
            return;
        }

        else {
            $("#idContainerHiddenDeleteDefaultValueSelected").val(defaultValueSelectedId);
            $('#divConfirmDeleteDefaultValueSelected').modal("show");
        }
    });

    $('#divConfirmDeleteDefaultValueSelectedYes').click(function () {
        var defaultValueIds = $("#idContainerHiddenDeleteDefaultValueSelected").val();

        //// Ajax call
        $.ajax({
            type: 'POST',
            url: urlDeleteDefaultValueSelected,
            cache: false,
            data: { 'commaSeparatedDefaultValuesSelectedIds': defaultValueIds },
            dataType: 'json',
            success: function (response) {
                if (response == "OK") {
                    location.reload();
                    alertmsg("green", "Operation completed successfully");
                } else if (response == "INCOMPLETE") {

                    alertmsg("red", "Not able to delete all the selected types. Please delete the default value first.");
                } else {
                    alertmsg("red", "Failure while performing the operation");
                }
            },
            error: function (err) {
                location.reload();
                alertmsg("red", "Error while performing the operation");
            }
        });


    });

    $('#btnAddDefaultValueSelected').click(function () {

        var defaultValueSelectedId = getSingleDefaultValueId();

        if (counter !== 1) {
            alertmsg('red', 'Please select one default value ');
            counter = 0;
            return;
        }
        window.location.href = urlAddDefaultValueSelected + "?defaultValueId=" + defaultValueSelectedId;
    });

    // end operation default value selected

});










