﻿$('document').ready(function() {
    $("select").select2();
    $.ajax({
        url: urlforDataTypeDropdownGetColumns,
        data: { importSourceId: importSourceId },
        type: 'POST',
        cache: false,
        traditional: true,
        //beforeSend: function () {
        //    //$("#btnsave").attr('disabled', true);
        //    SGloading('Loading');
        //},
        success: function (data) {
           
            $.each(data, function (index, value) {
                //console.log(value);
                var ddId = '#' + value.Id;
                var dropVal = $(ddId).select2("val", value.DataType);
            });
        },
        
        error: function (xhr, status, error) {
            //reset variables here
            alert(status + ' ' + error);
        }
    });
});
$('#cancleImpSrcCols').click(function () {
    window.location = urlImportSourceSetting;
});
$('#selectAllCheckBoxImpSrcCols').click(function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.listImpSrcCol').prop("checked", true);
    } else {
        $('.listImpSrcCol').prop("checked", false);
    }
});

$('#deleteselectedbutton').click(function (e) {
    var sThisVal = 0, i = 0;
    $('input[type=checkbox][class=listImpSrcCol]').each(function () {
        sThisVal = (this.checked ? i++ : 0);
    });
    if (i == 0) { alertmsg('red', 'Please select column to delete'); e.preventDefault(); }
    else {
        $("#deleteallcolmodelbtn").click(); e.preventDefault();
    }
});



$('#deleteAllcolsYes').click(function (e) {
    var impsrcId = importSourceId;
    var sThisVal = "";
    $('input[type=checkbox][class=listImpSrcCol]').each(function () {
        if (this.checked) {
            sThisVal += $(this).val() + "_";
        }
    });
    $.ajax({
        url: urlImportSourceColumnDelete,
        data: { impsrcId: impsrcId, impsrccolIds: sThisVal },
        type: 'POST',
        cache: false,
        traditional: true,
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Loading');
        },
        success: function (data) {
            window.location.href = data;
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            alert(status + ' ' + error);
        }
    });

});


$(function () {

    $("#autodatatype").autocomplete({
        source: urlAutoCompleteDataType,
    });
});

$('#getimpsrcColmns').click(function (e) {
    var i = 1;
    var impsrcColdata = [];
    var impsrcId = importSourceId;
    $('.sourceLabel').each(function () {

        var name = 'name+' + $(this).val() + '';
        var alias = 'alias+' + $(this).val() + '';
        var ddId = '#' + $(this).val() + '_dd';
        var length = 'length+' + $(this).val() + '';

        var order = i;
        var nameValue = document.getElementById(name).value;
        var aliasValue = document.getElementById(alias).value;
        var lengthValue = document.getElementById(length).value;
        var datatype = $(ddId).select2("val");
        i++;

        var data = order + "," + nameValue + "," + aliasValue + "," + lengthValue + "," + datatype;
        if (nameValue == "" || aliasValue == "" || lengthValue == "") {

            alertmsg("red", " All data fields are required");
            PreventDefault();
        }
   
        impsrcColdata.push(data);

    });
   
    $.ajax({
        url: urlCreateImportSourceColumn,
        data: { impsrcColdata: impsrcColdata, impsrcId: impsrcId, workflowId: workflowId, stepId: stepId },
        type: 'POST',
        cache: false,
        traditional: true,
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Loading');
        },
        success: function (data) {

            window.location.href = data;
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            //reset variables here
            alert(status + ' ' + error);
        }
    });

});
function ucFirstAllWords(str) {
    var pieces = str.split(" ");
    for (var i = 0; i < pieces.length; i++) {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
}
$('#generateimpsrcColmTable').click(function () {
    var col = $("#impSrcClmns").val();

    if (fieldTerminator == null) {
        fieldTerminator = ',';
    }

    var allTerminators = '';
    var foundTerminator = false;
    var alternateTerminator;
    $.each(availableFieldTerminators, function (i, val) {
        allTerminators += ((i > 0) ? ' or ' : '') + val.Description + ' (' + val.Name + ')';
        if (col.indexOf(val.Description) > 0) {
            foundTerminator = true;
            alternateTerminator = val.Description;
        }
    });

    //if (col.indexOf(fieldTerminator) < 0) {
    //    alertmsg('red', 'Fields should be separated by ' + fieldTerminator + ' (' + fieldTerminatorName + ')');
    //    $('#impSrcClmns').focus();
    //    return;
    //}

    
    if (foundTerminator === false && parseInt(NumberOfImportSourceColumns) > 1) {
        alertmsg('red', 'Fields should be separated by ' + allTerminators);
        $('#impSrcClmns').focus();
        return;
    }

    //var eachCol = col.split(fieldTerminator);
    var eachCol = col.split(alternateTerminator);

    var i = 1;

    $.each(eachCol, function (index, value) {
        document.getElementById('name+' + i + '').value = value;
        var name = value.replace(/\_/g, ' ');
        var cap = ucFirstAllWords(name);
        document.getElementById('alias+' + i + '').value = cap;
        document.getElementById('length+' + i + '').value = 100;
        i++;
    });
});

$(function () {
    $('#impSrcClmns').focus();
    $('#impSrcClmns').attr('placeholder', 'Please enter column name seperated by ' + fieldTerminator + ' (' + fieldTerminatorName + ')');
});
$(document).on('change', ".sourceDrop", function () {
    
    var dataTypeId = $(this).attr('id');
    
    var numberedId = dataTypeId.substr(0, dataTypeId.indexOf('_'));

    var idForLength = "length+" + numberedId;
    if (document.getElementById(idForLength)) {

        if ($(this).val().toLowerCase() !== "varchar") {
            
            document.getElementById(idForLength).disabled = true;

        } else {
            document.getElementById(idForLength).value = "100";
            document.getElementById(idForLength).disabled = false;
        }
    }

});


$(document).on('click', ".removeBtn", function() {
  
    var dataTypeId = $(this).attr('id');
    var numberedId = dataTypeId.substr(dataTypeId.indexOf('_') + 1, dataTypeId.length);

    $("#tr_" + numberedId).remove();

});

$('#addMoreRow').click(function () {

    var labelId = $('#sourceColumnTable tr:last td input').attr('id');
    var numberedId = "1";
    if (labelId != null)
        numberedId = parseInt(labelId.substr(labelId.indexOf('+') + 1, labelId.length)) + 1;
    
    $('#sourceColumnTable tbody').append('<tr id="tr_'+numberedId +'">' +
        
        '<td><input id="name+' + numberedId + '" type="text" class="sg-tooltip form-control" data-toggle="tooltip" data-placement="right" title="Click to edit" /> </td>' +
        '<td style="display:none;"><input type="hidden" class="sourceLabel" name="label[]" value="' + numberedId + '" /></td>' +
        '<td><input id="alias+' + numberedId + '" type="text" class="sg-tooltip form-control" data-toggle="tooltip" data-placement="right" title="Click to edit" /></td>'+
        '<td><select id="' + numberedId + '_dd" class="sourceDrop  form-control" name="Value"></select></td> '+

        '<td><div class="input-group inline"><input id="length+' + numberedId +'" type="text" class="sg-tooltip form-control" data-toggle="tooltip" data-placement="left" title="Click to edit" value="100" />' +
        '<div class="input-group-btn"><a id="remove_' + numberedId + '" class="btn btn-danger removeBtn"  data-toggle="tooltip" data-placement="top" title="remove the row"><i class="fa fa-close"></i></a></div>' +
        '</div></td>' +


        '</tr>');
    
    $.each(dataTypeCollection, function(index, value) {
        $("#" + numberedId + "_dd").append(new Option(value.Text, value.Value));
    });

    $("#" + numberedId + "_dd").select2();
});
