﻿// ========================================================================================================
// 👉 Definition of related custom fields
// ========================================================================================================
// *  ItemPoints                 <-->    dimensions       *
// *  CommodityCode              <-->    schedule_b       *
// *  ReorderPointUpdatedOn      <-->    ReorderPoint     *
// *  ReorderQuantityUpdatedOn   <-->    ReorderQuantity  *
// *  product_url                <-->    YahooId          *
// ========================================================================================================
// Note: Not considered related custom fields: Name,Localsku and Manufacturer <--> YahooID
// ========================================================================================================

/* -- NOTE: Below comment (relatedCustomFields_Sample) is the sample of JSON returned from database. 
            Please Do not delete it.

var relatedCustomFields_Sample = {
    //Setup of fields as described above
    setup: [
        {
            target: {
                customFieldName: "ItemPoints",
                customFieldAlias: "ItemPoints",
                customFieldId: 299

            },
            source: {
                customFieldName: "dimensions",
                customFieldAlias: "dimensions",
                customFieldId: 11
            }
        },
        {
            target: {
                customFieldName: "CommodityCode",
                customFieldAlias: "CommodityCode",
                customFieldId: 212
            },
            source: {
                customFieldName: "schedule_b",
                customFieldAlias: "schedule-b",
                customFieldId: 72
            }
        },
        {
            target: {
                customFieldName: "ReorderPointUpdatedOn",
                customFieldAlias: "Target Quantity Last Updated Date",
                customFieldId: 332
            },
            source: {
                customFieldName: "ReorderPoint",
                customFieldAlias: "Target Quantity",
                customFieldId: 186
            }
        },
        {
            target: {
                customFieldName: "ReorderQuantityUpdatedOn",
                customFieldAlias: "Reorder Point Last Updated Date",
                customFieldId: 333
            },
            source: {
                customFieldName: "ReorderQuantity",
                customFieldAlias: "Reorder Point",
                customFieldId: 206
            }
        },
        {
            target: {
                customFieldName: "product_url",
                customFieldAlias: "product-url",
                customFieldId: 69

            },
            source: {
                customFieldName: "YahooId",
                customFieldAlias: "YahooId",
                customFieldId: 208

            }
        }
    ],

    //Get matched custom field by customFieldId
    match: function (customFieldId) {
        return $.grep(this.setup,
            function(value, index) {
                return (
                    value.source.customFieldId == customFieldId);
            });
    }
};

*/

var relatedCf = {
    /*
    Setup of fields as described above
    */
    setup: [],

    /*
    Get matched custom field by customFieldId
    */
    match: function(customFieldId) {
        return $.grep(this.setup,
            function(value, index) {
                return (
                    value.source.customFieldId == customFieldId || value.target.customFieldId == customFieldId);
            });
    },

    /*
   Get matched custom field by customFieldAlias
   */
    matchByAlias: function(customFieldAlias) {
        return $.grep(this.setup,
            function(value, index) {
                return (
                    value.source.customFieldAlias == customFieldAlias ||
                        value.target.customFieldAlias == customFieldAlias);
            });
    },

    matchByAliasAndGetOther: function(customFieldAlias) {
        var x = $.grep(this.setup,
            function(value, index) {
                if (value.source.customFieldAlias == customFieldAlias)
                    return value.target;
                else if (value.target.customFieldAlias == customFieldAlias)
                    return value.source;
            });
        if (x[0]) {
            if (x[0].source.customFieldAlias == customFieldAlias)
                return x[0].target;
            else
                return x[0].source;
        };
        return null;
    }
};

relatedCustomFields = relatedCustomFields || [];

relatedCustomFields = Encoder.htmlDecode(relatedCustomFields);
relatedCustomFields = "[" + relatedCustomFields + "]";
relatedCustomFields = JSON.parse(relatedCustomFields);

relatedCf.setup = relatedCustomFields;

// ========================================================================================================

//todo: CUSTOM FIELD EDIT - RELATED FIELDS NAME, ALIAS CHANGE DENY


$("#btnOkRelatedCustomFieldModal")
    .click(function () {
        //get related fields
        var hiddenAndRelatedCustomFields = $("#hiddenRelatedCustomFields").val();
        //decode html
        hiddenAndRelatedCustomFields = Encoder.htmlDecode(hiddenAndRelatedCustomFields);
        //replace "{ with { and }" with }
        hiddenAndRelatedCustomFields = hiddenAndRelatedCustomFields.replace("\"{", "{").replace("}\"", "}");
        //parse to json object
        hiddenAndRelatedCustomFields = JSON.parse(hiddenAndRelatedCustomFields);

        //add to grid
        addCustomFieldToGrid(hiddenAndRelatedCustomFields);

        //$("#RelatedCustomFieldModal").modal("hide");
		var message = "Following related fields have been added: '" + hiddenAndRelatedCustomFields.customFieldAlias + "'";
		
		udh.notify.warn(message);
    });

$(function () {
    $("#tblRelatedCustomFieldInfoModal tbody").html("");
    $("#tblRelatedCustomFieldInfoModal tbody").append("");
    $.each(relatedCf.setup,
        function (i, v) {
            $("#tblRelatedCustomFieldInfoModal tbody").append("<tr><td>{0}</td><td>{1}</td></tr>".format(v.source.customFieldAlias, v.target.customFieldAlias, v.source.rule));
        });
    $("#tblRelatedCustomFieldInfoModal tbody").append("<tr><td>{0}</td><td>{1}</td></tr>".format("","",""));
});