﻿var callBackgroundProcess = true;
var loadingCount = 0;
var dataTableLoaded = false;
var interval = 2000;
var totalTime = 0;
var clearIntervalId = "";

var searchType = 1;
var searchParameter = "";
var tokenName = 'SearchToken ' + getCurrentDateTime();
var dynamicClassAdded = false;
var queryStringVendorId = "";
var queryStringProductCategoryId = "";



//var ttt;
try {

    var x;
    var tableToolsOptions = [
        "copy",
        //"csv",
        {
            "sExtends": "xls",
            "sButtonText": "Export all to CSV",
            "fnClick": function (nButton, oConfig, oFlash) {
                var searchModel = getSearchParameterViewModel();

                //construct download all url with querystring
                var urlToDownload = urlDownloadAllToCsv +
                    "?SearchType=" + (searchModel.SearchType ? searchModel.SearchType : '') +
                    "&MatchingCriteria=" + (searchModel.MatchingCriteria ? searchModel.MatchingCriteria : '') +
                    "&KeySearch=" + (searchModel.KeySearch ? searchModel.KeySearch : '') +
                    "&VendorName=" + (searchModel.VendorName ? searchModel.VendorName : '') +
                    "&FromDate=" + (searchModel.FromDate ? searchModel.FromDate : '') +
                    "&ToDate=" + (searchModel.ToDate ? searchModel.ToDate : '') +
                    "&CostFrom=" + (searchModel.CostFrom ? searchModel.CostFrom : '') +
                    "&CostTo=" + (searchModel.CostTo ? searchModel.CostTo : '') +
                    "&PriceFrom=" + (searchModel.PriceFrom ? searchModel.PriceFrom : '') +
                    "&PriceTo=" + (searchModel.PriceTo ? searchModel.PriceTo : '') +
                    "&User=" + (searchModel.User ? searchModel.User : '') +
                    "&Search=" + (searchModel.Search ? searchModel.Search : '') +
                    "&ForPrimaryVendorProductOnly=" + (searchModel.ForPrimaryVendorProductOnly ? searchModel.ForPrimaryVendorProductOnly : '') +
                    "&StoreId=" + (searchModel.StoreId ? searchModel.StoreId : '') +
                    "&ProductCategory=" + (searchModel.ProductCategory ? searchModel.ProductCategory : '') +
                    "&CompleteFrom=" + (searchModel.CompleteFrom ? searchModel.CompleteFrom : '') +
                    "&ProductCategory=" + (searchModel.ProductCategory ? searchModel.ProductCategory : '') +
                    "&CompleteTo=" + (searchModel.CompleteTo ? searchModel.CompleteTo : '') + "'";

                //redirect to the url to download all csv
                window.location = urlToDownload;

            }
        }
    ];

    function clear_form_elements(class_name) {
        $("." + class_name).find(':input').each(function () {
            switch (this.type) {
                case 'password':
                case 'text':
                case 'textarea':
                case 'file':
                    jQuery(this).val('');
                    break;
                case 'select-one':
                case 'select-multiple':
                    //clear current values
                    jQuery(this).select2().val('');
                    jQuery(this).select2().val('');

                    ////handle remote configuration for server side dropdown
                    var id = "#" + jQuery(this).attr('id');
                    if (id === '#vendorName')
                        $(id).select2(remoteDataConfig(urlGetVendorListForSelect2, 3, 250, pleaseSelectMessage));
                    if (id === '#ProductCategory')
                        $(id).select2(remoteDataConfig(urlGetProductCategoryListForSelect2, 3, 250, pleaseSelectMessage));
                    else if (id === '#userName')
                        $('#userName').select2(remoteDataConfig(urlGetUserListForSelect2, 2, 150, pleaseSelectMessage));

                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
            }
        });
        $("#productCategory").select2({
            placeholder: '--Choose--',
            allowClear: true
        });
    }
    try {
        $(document).ready(function () {
            catalogManagerSearch = false;
            enterSearch === false;
            $('#SearchTokenName').val(tokenName);
            $('#SessionUser').val(userNameforBg);
            //$("userName").select2();
            var queryStringForSearch = getQueryStringByName("searchId");
            if (queryStringForSearch === "") {
                $("#keySearch").val("LocalSku");
            }
            //$("#keySearch").val("LocalSku");
            $("#ClearSearch").click(function (e) {
                window.location.reload();
                //$('select').each(function () {
                //    $(this).select2('val', '');

                //});
                //e.preventDefault();
            });

            //var test = $(this).val();
            //$(".prices").hide();
            //$(".prices[data-period='" + test + "']").show();

            $(".stopPropagation").click(function () {
                if (event.target.tagName == 'SPAN') {
                    var test = "#" + event.target.getAttribute('data-filter').split('.')[1].split('-')[1];
                    $(test).prop("checked", true);
                }
            });


            $('input:radio[name="SearchType"]').change(function () {
                if ($(this).val() == '3') {
                    $('#ForPrimaryVendorProductOnly').attr("disabled", true);
                } else {
                    $('#ForPrimaryVendorProductOnly').attr("disabled", false);
                }
                $("#productCategory").select2({
                    placeholder: '--Choose--',
                    allowClear: true
                });
            });
            //$('select').select2();

            $("#SearchText").click(function () {
                if ($("#SearchText").is(":disabled") === true) {
                    return;
                }

                $('#SearchText').attr("disabled", "disabled");
                $("#selectAllCheckBox").prop("checked", false);
                $('.productCatalogSelector').prop("checked", false);
                var searchModel = getSearchParameterViewModel();
                callBackgroundProcess = true;
                dataTableLoaded = false;
                totalTime = 0;
                globalSearchId = "";
                var searchId = 0;


                $.ajax({
                    url: urlGetUniqueSearchId,
                    data: { "searchParameter": searchModel },
                    type: "POST",
                    async: false,
                    beforeSend: function () {
                        SGtableloading(faIcon, '', this);
                    },
                    success: function (data) {
                        searchId = data;
                        $('#SearchId').val(data);

                    },
                    error: function () { }


                });
                clearIntervalId = setInterval(callBackgroundProcessOption.bind(null, searchId, interval, searchModel), interval);
                //doCatalogSearch
                // $("#FilterText").click();


            });

            $("#FilterText").mousedown(function () {

                $("#selectAllCheckBox").prop("checked", false);
                $('.productCatalogSelector').prop("checked", false);
            });

            if (searchId != 0) {
                $("#SearchId").val(searchId);
                $.ajax({
                    url: urlGetCatalogSearchParam,
                    data: {
                        searchId: searchId
                    },
                    type: 'GET',
                    async: false,
                    success: function (response) {
                        //debugger;
                        result = response.Data;
                        vendorNameFromVb = response.Message;
                        if (vendorNameFromVb != null) {
                            debugger;
                            categoryNameFromVb = response.Message;
                        }
                        if (result != null) {
                            if (result.SearchId == null) { $('#SearchId').val(0); }
                            searchType = result.SearchType;
                            searchParameter = result;
                        }
                    }
                });
            }

            $('.mix-container').each(function () {
                //debugger;
                $(this).mixItUp({
                    animation: {
                        enable: false,
                        duration: 300
                    },
                    layout: {
                        display: 'block'
                    },
                    load: {
                        filter: '.category-' + searchType
                    },
                    callbacks: {

                        onMixEnd: function (e) {
                            clear_form_elements('mix-container');
                        }
                    }
                });
            });


            if (searchParameter.SearchId !== '' && searchParameter.SearchId !== null && searchParameter.SearchId !== undefined) {
                $("input[name=SearchType][value=" + searchParameter.SearchType + "]").attr('checked', 'checked');
                $('#fullTextSearch').val(searchParameter.OriginalSearchValue);
                $('#matchingCriteria').val(searchParameter.MatchingCriteria);
                $('#select2-vendorName-container').text(searchParameter.VendorName);
                if (searchParameter.ForPrimaryVendorProductOnly === false) {
                    $('#ForPrimaryVendorProductOnly').prop('checked', true);
                }
                if (searchParameter.KeySearch === '' || searchParameter.KeySearch === 'Anything') {
                    //$("#keySearch").val('Anything').trigger('change');
                    $('#Keysearch option[value=""]').attr('selected', 'selected');

                }
                else {
                    $("#keySearch").val(searchParameter.KeySearch);

                }
                if (searchParameter.DisplayFromDate !== '1/1/1900 12:00:00 AM')
                    $('#FromDate').val(searchParameter.DisplayFromDate.replace(" 12:00:00 AM", ''));
                if (searchParameter.DisplayToDate !== '1/1/1900 12:00:00 AM')
                    $('#ToDate').val(searchParameter.DisplayToDate.replace(" 12:00:00 AM", ''));
                if (searchParameter.CostFrom !== 0)
                    $('#CostFrom').val(searchParameter.CostFrom);
                if (searchParameter.CostTo !== 0)
                    $('#CostTo').val(result.CostTo);
                if (searchParameter.PriceFrom !== 0)
                    $('#PriceFrom').val(searchParameter.PriceFrom);
                if (searchParameter.PriceTo !== 0)
                    $('#PriceTo').val(searchParameter.PriceTo);
                $('#productCategory').val(searchParameter.ProductCategory).trigger('change');
                // $('#completeFrom').val(searchParameter.CompleteFrom);
                if (searchParameter.CompleteFrom !== 0)
                    $("#completeFrom").val(searchParameter.CompleteFrom).trigger('change');
                if (searchParameter.CompleteTo !== 0)
                    $('#completeTo').val(searchParameter.CompleteTo).trigger('change');
                if (searchParameter.VendorName !== '') {
                    $("#vendorName").empty().append('<option value="' + searchParameter.VendorName + '">' + vendorNameFromVb + '</option>').val(searchParameter.VendorName).trigger('change');
                }
                //if (searchParameter.ProductCategory !== '') {
                //    $("#productCategory").empty().append('<option value="' + searchParameter.ProductCategory + '">' + categoryNameFromVb + '</option>').val(searchParameter.ProductCategory).trigger('change');
                //}
            }

            queryStringVendorId = getQueryStringByName("vendorId");
            var queryStringProductCategoryId = getQueryStringByName("ProductCategoryId");
            var queryStringBrandId = getQueryStringByName("BrandId");
            var queryStringIsBigCommerceProduct = getQueryStringByName("BigCommerceProduct");
            if (queryStringIsBigCommerceProduct) {
                //$("#productCategory").empty().append('<option value="' + queryStringProductCategoryId + '">' + categoryNameFromVb + '</option>').val(queryStringProductCategoryId).trigger('change');
                $('#BigCommerceProduct').val(queryStringIsBigCommerceProduct).trigger('change');
                //$("#ForPrimaryVendorProductOnly").prop("checked", true).trigger('change');
                globalSearchId = 1;
            }
            if (queryStringProductCategoryId) {
                //$("#productCategory").empty().append('<option value="' + queryStringProductCategoryId + '">' + categoryNameFromVb + '</option>').val(queryStringProductCategoryId).trigger('change');
                $('#productCategory').val(queryStringProductCategoryId).trigger('change');
                //$("#ForPrimaryVendorProductOnly").prop("checked", true).trigger('change');
                globalSearchId = 1;
            }
            if (queryStringVendorId) {

                //console.log(queryStringVendorId);
                //console.log(vendorName12);
                $("#vendorName").empty().append('<option value="' + queryStringVendorId + '">' + vendorNameFromVb + '</option>').val(queryStringVendorId).trigger('change');
                var primaryVendorProductId = getQueryStringByName("primaryVendorProductOnly");
                if (isEmptyOrSpaces(primaryVendorProductId) || primaryVendorProductId.toLowerCase() == "false") {
                    $("#ForPrimaryVendorProductOnly").prop("checked", true).trigger('change');
                }
                globalSearchId = 1;

            }

            if (queryStringBrandId) {
                //$("#productCategory").empty().append('<option value="' + queryStringProductCategoryId + '">' + categoryNameFromVb + '</option>').val(queryStringProductCategoryId).trigger('change');
                $('#pimBrand').val(queryStringBrandId).trigger('change');
                //$("#ForPrimaryVendorProductOnly").prop("checked", true).trigger('change');
                globalSearchId = 1;
            }
            if (queryStringProductCategoryId > 0 || queryStringVendorId > 0 || queryStringBrandId > 0 || queryStringIsBigCommerceProduct == true) {
                setTimeout(function () {
                    $("#FilterText").click();
                }, 2000);
                $("#SearchText").click();
            }

            $("#FromDate").datepicker();
            $("#ToDate").datepicker();
            $("#PCModelLabelAdd").hide();
            $("#VPCModelLabelAdd").hide();



            //var colvis = new $.fn.catalogViewerTable.ColVis(catalogViewerTable, {
            //    buttonText: '<img src=&quot;images/down.gif" >',
            //    activate: 'mouseover',
            //    exclude: [0]
            //});
            //$(colvis.button()).prependTo('th:nth-child(1)');



            $("#toggleFilter").click(function () {
                //   initializeCatalogViewer();
                $("#selectAllCheckBox").prop("checked", false);
                $('.productCatalogSelector').prop("checked", false);
                $("#FilterText").click();
            });
            $('#Search').change(function () {
                var localStorageValues = localStorage.getItem('catalog_viewer');

                if (localStorageValues != null) {
                    localStorageValues = $.parseJSON(localStorageValues);
                    var currentSearchValue = $("#Search").val();
                    $.each(localStorageValues, function (index, val) {
                        //console.log(val);
                        if (val.name === "Search" && val.value === currentSearchValue) {
                            $("#selectAllCheckBox").prop("checked", false);
                            $('.productCatalogSelector').prop("checked", false);
                            $("#FilterText").click();
                        }
                    });
                }
            });

            setTimeout(customJqueryUIpara('.sg-datatable-wrap'), 500);

            $("#btnAddProductcatalog").click(function () {
                window.location = urlCreateProductCatalog;
            });

            $("#btnAddVendorProductcatalog").click(function () {

                var autoIncrementId;
                var count = 0;
                $('input[type=checkbox][class=productCatalogSelector]').each(function () {
                    if (this.checked) {
                        autoIncrementId = this.value;
                        count++;
                    }
                });
                if (count !== 1) {
                    alertmsg('red', 'Please select one product catalog to add vendor product information.');
                    return;
                }
                if ($("#product_" + autoIncrementId).length) {

                    var productId = $("#product_" + autoIncrementId).data("value");
                    if ($.isNumeric(productId) === true) {
                        window.location = urlCreateVendorProductCatalog + "&productId=" + productId;
                    }
                }
            });

            $("#btnEditProductcatalog").click(function () {

                var autoIncrementId;
                var count = 0;
                $('input[type=checkbox][class=productCatalogSelector]').each(function () {
                    if (this.checked) {
                        autoIncrementId = this.value;
                        count++;
                    }
                });
                if (count !== 1) {
                    alertmsg('red', 'Please select one product catalog to edit.');
                    return;
                }
                if ($("#product_" + autoIncrementId).length) {

                    var productId = $("#product_" + autoIncrementId).data("value");

                    if (productId != null && productId !== 0) {
                        window.location = urlEditProductCatalog + "?id=" + productId + "&returnUrl=" + returnUrl;
                    } else {
                        alertmsg("red", "There is no any product catalog associated with the selected one.");
                    }
                } else {
                    alertmsg("red", "There is no any product catalog associated with the selected one.");

                }
            });

            $("#btnEditVendorProductcatalog").click(function () {
                var autoIncrementId;
                var count = 0;
                $('input[type=checkbox][class=productCatalogSelector]').each(function () {
                    if (this.checked) {
                        autoIncrementId = this.value;
                        count++;
                    }
                });
                if (count !== 1) {
                    alertmsg('red', 'Please select one vendor product catalog to edit.');
                    return;
                }
                if ($("#vendorProduct_" + autoIncrementId).length) {

                    var vendorProductId = $("#vendorProduct_" + autoIncrementId).data("value");

                    if (vendorProductId != null && vendorProductId !== 0 && vendorProductId !== "" && $.isNumeric(vendorProductId) === true) {
                        window.location = urlEditVendorProductCatalog + "?id=" + vendorProductId + "&returnUrl=" + returnUrl;
                    } else {
                        alertmsg("red", "There is no any vendor product catalog associated with the selected one.");
                    }
                } else {
                    alertmsg("red", "There is no any vendor product catalog associated with the selected one.");

                }
            });

            $('#btnSetPrimary').click(function (e) {
                var i = 0;
                var autoIncrementId = 0;
                $('input[type=checkbox][class=productCatalogSelector]').each(function () {

                    if (this.checked) {
                        i++;
                        autoIncrementId = this.value;
                    }
                });

                if (i === 0) {
                    alertmsg('red', Alert_VendorProductSetPrimary);
                    e.preventDefault();
                } else if (i > 1) {
                    alertmsg('red', Alert_singleVendorProductSetPrimary);
                    e.preventDefault();
                } else {
                    if ($("#vendorProduct_" + autoIncrementId).length) {

                        var vendorProductId = $("#vendorProduct_" + autoIncrementId).data("value");
                        var productId = $("#product_" + autoIncrementId).data("value");

                        $.ajax({
                            url: urlVendorProductSetPrimary,
                            data: {
                                vendorProductId: vendorProductId,
                                productId: productId
                            },
                            type: 'POST',
                            cache: false,
                            success: function (data) {
                                alertmsg('green', Alert_VendorProductSetPrimarySuccess);

                                catalogViewerTable.fnReloadAjax();
                            }
                        });
                    }
                }
            });

            $('#btnShowPrimary').click(function (e) {
                var i = 0;
                var autoIncrementId = 0;
                $('input[type=checkbox][class=productCatalogSelector]').each(function () {

                    if (this.checked) {
                        i++;
                        autoIncrementId = this.value;
                    }
                });

                if (i === 0) {
                    alertmsg('red', Alert_productCatalogForPrimaryVendorProduct);
                    e.preventDefault();
                } else if (i > 1) {
                    alertmsg('red', Alert_singleProductCatalogForPrimaryVendorProduct);
                    e.preventDefault();
                } else {

                    var productId = $("#product_" + autoIncrementId).data("value");


                    $.ajax({
                        url: urlVendorProductGetPrimaryId,
                        data: {
                            'productId': productId
                        },
                        type: 'POST',
                        cache: false,
                        success: function (data) {
                            if (data === "") {
                                alertmsg('yellow', Alert_NoPrimaryVendorProduct);
                            } else {

                                var searchParam = $("#Search").val();
                                $("#Search").val(data);
                                $('#SearchText').click();
                                $("#Search").val(searchParam);
                            }
                        }
                    });
                }
            });

            var validateProductForKitCreation = function (e) {
                var totalSelectedItemsLength = 0;
                var eachIds = [];
                var allComplete = true;
                var isProductDisabled = false;
                //$('#SearchDataTable tr .progress .progress-bar').html().indexOf(100)
                $('input[type=checkbox][class=productCatalogSelector]').each(function () {
                    if (this.checked) {
                        var indexOf100 = $(this).parent().parent().find('.progress .progress-bar-100')
                            .html();
                        var indexOf75 = $(this).parent().parent().find('.progress .progress-bar-75')
                            .html();

                        //console.log("index of 100 and 75");
                        //console.log(indexOf100);
                        //console.log(indexOf75);
                        //console.log(indexOf100);
                        //console.log(indexOf75);
                        //console.log(typeof indexOf100 === 'undefined' && typeof indexOf75 === 'undefined');
                        //console.log(typeof indexOf100 === 'undefined');
                        //console.log(typeof indexOf75 === 'undefined');
                        //e.preventDefault();

                        if (typeof indexOf100 === 'undefined' && typeof indexOf75 === 'undefined') {
                            allComplete = false;
                        }

                        var productDisabled = $(this).parent().parent().find('.progress .progress-bar-disabled')
                            .html();
                        console.log("product disabled");
                        console.log(productDisabled);
                        if (productDisabled) {
                            isProductDisabled = true;
                        }
                    }
                });
                return;
                if (allComplete === false) {
                    //console.log("data");
                    alertmsg('red', 'Please select product(s) with pricing information.');
                    e.preventDefault();
                }

                if (isProductDisabled === true) {
                    alertmsg('red', 'Please select enabled product(s) only.');
                    e.preventDefault();
                }

                $('input[type=checkbox][class=productCatalogSelector]').each(function () {
                    if (this.checked) {
                        totalSelectedItemsLength++;
                        if ($(this).parent('td').find('i')[0]) {
                            eachIds.push(this.value);
                        }
                    }
                });

                if (eachIds.length === 0 || eachIds.length !== totalSelectedItemsLength) {
                    alertmsg('red', 'Please select a primary product first');
                    e.preventDefault();
                }

            }

            var validateProductForCustomFieldUpdate = function (e) {
                validateProductForKitCreation(e);
            }

            $('#btnViewProductKit').click(function () {
                var eachIds = [];

                $('input[type=checkbox][class=productCatalogSelector]').each(function () {
                    if (this.checked) {
                        eachIds.push(this.value);
                    }
                });

                if (eachIds.length === 0) {
                    alertmsg('red', 'Please select a product first');
                    return;
                }

                //if (eachIds.length === 1) {
                //    alertmsg('red', 'Please select multiple products for Kit');
                //    return;
                //}


                var isValidForKitPricing = true;
                var invalidProductNameList = '';
                var productIdList = '';
                $.each(eachIds, function (index, val) {
                    var productId = $("#product_" + val).data("value");
                    var productName = $("#product_" + val).text();
                    var vendorProductId = $("#vendorProduct_" + val).data("value");
                    if ($.trim(vendorProductId).length === 0) {
                        invalidProductNameList += '"' + productName + '", ';
                        isValidForKitPricing = false;
                    }
                    productIdList += productId + ',' + vendorProductId + ',';
                });
                invalidProductNameList = invalidProductNameList.substr(0, invalidProductNameList.length - 2);
                if (!isValidForKitPricing) {
                    //console.log(invalidProductNameList);
                    //console.log(invalidProductNameList.split(', '));
                    //console.log(invalidProductNameList.split(', ').length);
                    //alertmsg('red', 'Vendor is required for Kit Pricing of product' + (invalidProductNameList.split(',').length > 1 ? '(s) ' : ' ') + invalidProductNameList);
                    alertmsg('red', 'Vendor is required for kit pricing for the selected product(s)');
                    return false;
                }

                //ensure that cost is not zero
                $('#SearchDataTable>tbody>tr').each(function () {

                    var self = $(this);
                    //if(self.class == 'selected')
                    //console.log(self);
                    if (self[0].className.indexOf('selected') > -1) {
                        //console.log(self[0]);
                        //console.log(self[0].td);
                        ttt = self[0];
                    }
                    //price - 3
                    //cost - 8
                });
                //ensure that price is not zero

                var productIdList = '';
                $.each(eachIds, function (index, val) {
                    var productId = $("#product_" + val).data("value");
                    var productName = $("#product_" + val).text();
                    var vendorProductId = $("#vendorProduct_" + val).data("value");

                    productIdList += productId + ',';
                });

                productIdList = productIdList.substr(0, productIdList.length - 1);

                window.location = urlKitView + '?productIdList=' + productIdList;

            });

            $('#btnCreateProductKit').click(function () {
                var eachIds = [];
                var productIdList = '';
                var totalSelectedItemsLength = 0;
                var totalvalidItems = 0;
                var eachKitIds = [];
                var kitFlag = 0;
                var disabledProduct = 0;
                //validateProductForKitCreation();

                $('input[type=checkbox][class=productCatalogSelector]').each(function () {

                    if (this.checked) {
                        totalSelectedItemsLength++;
                        //for disabled product check
                        if ($(this).parent('td').next('td').find('div').find('.fa-ban')[0]) {
                            disabledProduct = 1;
                        }

                        if ($(this).parent('td').next('td').find('div').find('.progress-bar').text().indexOf('K') > 0) {
                            // for kit
                            kitFlag = 1;
                            eachKitIds.push(this.value);

                        } else if ($(this).parent('td').find('i')[0]) { // for primary product
                            eachIds.push(this.value);
                            totalvalidItems++;
                        }

                    }
                });
                if (disabledProduct == 1) {
                    alertmsg('red', 'Can\'t create Kit using disabled product!');
                    return;
                }
                if (kitFlag == 1) {
                    alertmsg('red', 'Can\'t create Kit using another Kit!');
                    return;
                }
                if (totalvalidItems === 0 || totalvalidItems !== totalSelectedItemsLength) {
                    alertmsg('red', 'Please select primary products for kit creation');
                    return;
                }


                //$('input[type=checkbox][class=productCatalogSelector]').each(function () {
                //    if (this.checked) {

                //        if ($(this).parent('td').find('i')[0]) {
                //            eachIds.push(this.value);
                //        }
                //    }
                //});

                var isValidForKitPricing = true;
                var invalidProductNameList = '';
                var productIdList = '';
                var productIdForPricingValidation = '';
                $.each(eachIds, function (index, val) {
                    var productId = $("#product_" + val).data("value");
                    var productName = $("#product_" + val).text();
                    var vendorProductId = $("#vendorProduct_" + val).data("value");
                    if ($.trim(vendorProductId).length === 0) {
                        invalidProductNameList += '"' + productName + '", ';
                        isValidForKitPricing = false;
                    }
                    productIdList += productId + ',' + vendorProductId + ',';
                    productIdForPricingValidation += productId + ',';
                });

                //To create Kit product must have pricing information
                productIdForPricingValidation = productIdForPricingValidation.substr(0, productIdForPricingValidation.length - 1);

                $.ajax({
                    type: 'POST',
                    url: urlKitCreatePricingValidation,
                    data: {
                        'productIdList': productIdForPricingValidation
                    },
                    beforeSend: function () {
                        SGloading('Loading');
                    },
                    success: function (response) {

                        if (response.Status) {

                            invalidProductNameList = invalidProductNameList.substr(0, invalidProductNameList.length - 2);
                            if (!isValidForKitPricing) {
                                alertmsg('red', 'Vendor is required for kit pricing for the selected product(s)');
                                return false;
                            }

                            //ensure that cost is not zero
                            $('#SearchDataTable>tbody>tr').each(function () {

                                var self = $(this);
                                //if(self.class == 'selected')
                                //console.log(self);
                                if (self[0].className.indexOf('selected') > -1) {
                                    //console.log(self[0]);
                                    //console.log(self[0].td);
                                    ttt = self[0];
                                }
                                //price - 3
                                //cost - 8
                            });
                            //ensure that price is not zero

                            var productIdList = '';
                            $.each(eachIds, function (index, val) {
                                var productId = $("#product_" + val).data("value");
                                var productName = $("#product_" + val).text();
                                var vendorProductId = $("#vendorProduct_" + val).data("value");

                                productIdList += productId + ',';
                            });

                            productIdList = productIdList.substr(0, productIdList.length - 1);

                            window.location = urlKitCreate + '?productIdList=' + productIdList;



                        } else {
                            alertmsg("red", response.Message);

                        }


                    },
                    complete: function () {
                        SGloadingRemove();
                    },
                    error: function (xhr, status, error) {
                        alert(status + ' ' + error);
                    }
                });

            });

            $('#btnCustomFieldUpdate').click(function () {
                var productIdList = '';
                //var isForKit = $("#searchDomain_Kits").prop('checked');
                var eachIds = [];
                var totalSelectedItemsLength = 0;
                var totalvalidItems = 0;
                var eachKitIds = [];
                $('input[type=checkbox][class=productCatalogSelector]').each(function () {
                    if (this.checked) {
                        //for disabled product check
                        //if ($(this).parent('td').next('td').find('div').find('.fa-ban')[0]) {
                        //    alertmsg('red', 'Please select enable primary products for kit creation');
                        //    return;
                        //}
                        totalSelectedItemsLength++;
                        if ($(this).parent('td').next('td').find('div').find('.progress-bar').text().indexOf('K') > 0) {
                            // for kit
                            totalvalidItems++;
                            eachKitIds.push(this.value);

                        } else {
                            if ($(this).parent('td').find('i')[0]) { // for primary product
                                eachIds.push(this.value);
                                totalvalidItems++;
                            }
                        }
                    }
                });

                if (totalvalidItems === 0 || totalvalidItems !== totalSelectedItemsLength) {
                    alertmsg('red', 'Please select a primary product or kit for bulk item');
                    return;
                }
                $.each(eachKitIds, function (index, val) {
                    var productId = $("#product_" + val).data("value");
                    productIdList += productId + '_' + "0" + '_' + '1' + ',';
                });

                $.each(eachIds, function (index, val) {
                    var productId = $("#product_" + val).data("value");
                    var productName = $("#product_" + val).text();
                    var vendorProductId = $("#vendorProduct_" + val).data("value");
                    if ($.trim(vendorProductId).length === 0) {
                        invalidProductNameList += '"' + productName + '", ';
                        isValidForBulkPricing = false;
                    }
                    productIdList += productId + '_' + vendorProductId + '_' + '0' + ',';
                });


                //console.log(productIdList);
                productIdList = productIdList.substr(0, productIdList.length - 1);
                window.location = urlCustomFieldUpdate + '?productIdList=' + productIdList;
                return;

                var eachIds = [];
                var totalSelectedItemsLength = 0;

                //var isForKit = $("#searchDomain_Kits").prop('checked');

                //if (isForKit) {
                //    var kitIdList = '';
                //    $('input[type=checkbox][class=productCatalogSelector]').each(function () {
                //        if (this.checked) {
                //            eachIds.push(this.value);

                //        }
                //    });
                //    $.each(eachIds, function(index, val) {
                //        var kitId = $("#product_" + val).data("value");
                //        kitIdList += kitId + ',' + 0 + ',';
                //    });
                //    console.log(kitIdList);
                //}

                $('input[type=checkbox][class=productCatalogSelector]').each(function () {
                    if (this.checked) {
                        totalSelectedItemsLength++;

                        //if star primary get ids
                        if ($(this).parent('td').find('i')[0]) {
                            eachIds.push(this.value);
                        }
                    }
                });

                if (eachIds.length === 0 || eachIds.length !== totalSelectedItemsLength) {
                    alertmsg('red', 'Please select a primary product first');
                    return;
                }

                //if (eachIds.length === 1) {
                //    alertmsg('red', 'Please select multiple products for bulk pricing');
                //    return;
                //}

                var isValidForBulkPricing = true;
                var invalidProductNameList = '';
                var productIdList = '';
                $.each(eachIds, function (index, val) {
                    var productId = $("#product_" + val).data("value");
                    var productName = $("#product_" + val).text();
                    var vendorProductId = $("#vendorProduct_" + val).data("value");
                    if ($.trim(vendorProductId).length === 0) {
                        invalidProductNameList += '"' + productName + '", ';
                        isValidForBulkPricing = false;
                    }
                    productIdList += productId + ',' + vendorProductId + ',';
                });
                invalidProductNameList = invalidProductNameList.substr(0, invalidProductNameList.length - 2);
                if (!isValidForBulkPricing) {
                    //console.log(invalidProductNameList);
                    //console.log(invalidProductNameList.split(', '));
                    //console.log(invalidProductNameList.split(', ').length);
                    //alertmsg('red', 'Vendor is required for Bulk Pricing of product' + (invalidProductNameList.split(',').length > 1 ? '(s) ' : ' ') + invalidProductNameList);
                    alertmsg('red', 'Vendor is required for custom field update for the selected product(s)');
                    return false;
                }

                //validateProductForCustomFieldUpdate();

                //remove last ,
                //console.log(productIdList);
                productIdList = productIdList.substr(0, productIdList.length - 1);
                //console.log(productIdList);

                window.location = urlCustomFieldUpdate + '?productIdList=' + productIdList;

            });

            $('#btnBulkPricing').click(function () {
                var productIdList = '';
                //var isForKit = $("#searchDomain_Kits").prop('checked');
                var eachIds = [];
                var totalSelectedItemsLength = 0;
                var totalvalidItems = 0;
                var eachKitIds = [];
                var disabledFlag = false;
                var pricingFlag = false;
                $('input[type=checkbox][class=productCatalogSelector]').each(function () {
                    if (this.checked) {
                        totalSelectedItemsLength++;

                        //for disabled product check
                        //if ($(this).parent('td').next('td').find('div').find('.fa-ban')[0]) {
                        //    disabledFlag = true;
                        //    return;

                        //}

                        // contain only pricing information
                        if ($(this).parent('td').next('td').find('div').find('.progress-bar').text().indexOf('P') > 0) {

                            var dataDetail = $($(this).parent('td').next('td').find('div').last()).attr("data-detail");

                            if (dataDetail.indexOf("11") != 0) {
                                pricingFlag = true;
                                return;
                            }
                        }

                        if ($(this).parent('td').next('td').find('div').find('.progress-bar').text().indexOf('K') > 0) {
                            // for kit
                            totalvalidItems++;
                            eachKitIds.push(this.value);

                        } else {
                            if ($(this).parent('td').find('i')[0]) {
                                // for primary product
                                eachIds.push(this.value);
                                totalvalidItems++;
                            }
                        }
                    }
                });
                //if (disabledFlag) {
                //    alertmsg('red', 'Please select enable primary products for bulk pricing');
                //    return;
                //}
                if (pricingFlag) {
                    alertmsg('red', 'Please select products with vendor information');
                    return;
                }
                $.each(eachKitIds, function (index, val) {
                    var productId = $("#product_" + val).data("value");
                    productIdList += productId + '_' + "0" + '_' + '1' + ',';
                });

                $.each(eachIds, function (index, val) {
                    var productId = $("#product_" + val).data("value");
                    var productName = $("#product_" + val).text();
                    var vendorProductId = $("#vendorProduct_" + val).data("value");
                    if ($.trim(vendorProductId).length === 0) {
                        invalidProductNameList += '"' + productName + '", ';
                        isValidForBulkPricing = false;
                    }
                    productIdList += productId + '_' + vendorProductId + '_' + '0' + ',';
                });

                if (totalvalidItems === 0 || totalvalidItems !== totalSelectedItemsLength) {
                    alertmsg('red', 'Please select a primary product or kit first');
                    return;
                }
                //console.log(productIdList);
                productIdList = productIdList.substr(0, productIdList.length - 1);
                window.location = urlBulkPricing + '?productIdList=' + productIdList;
                return;
                $('input[type=checkbox][class=productCatalogSelector]').each(function () {
                    if (this.checked) {
                        totalSelectedItemsLength++;
                        if (isForKit) {
                            eachIds.push(this.value);

                        } else {
                            if ($(this).parent('td').find('i')[0]) {
                                eachIds.push(this.value);
                            }
                        }
                    }
                });
                if (!isForKit) {

                    if (eachIds.length === 0 || eachIds.length !== totalSelectedItemsLength) {
                        alertmsg('red', 'Please select a primary product first');
                        return;
                    }
                }

                //if (eachIds.length === 1) {
                //    alertmsg('red', 'Please select multiple products for bulk pricing');
                //    return;
                //}
                var isValidForBulkPricing = true;
                var invalidProductNameList = '';
                var kitIdList = '';
                var productIdList = '';
                $.each(eachIds, function (index, val) {
                    var productId = $("#product_" + val).data("value");
                    var productName = $("#product_" + val).text();
                    var vendorProductId = $("#vendorProduct_" + val).data("value");
                    if ($.trim(vendorProductId).length === 0) {
                        invalidProductNameList += '"' + productName + '", ';
                        isValidForBulkPricing = false;
                    }
                    kitIdList += productId + ",";
                    productIdList += productId + ',' + vendorProductId + ',';
                });
                invalidProductNameList = invalidProductNameList.substr(0, invalidProductNameList.length - 2);
                if (!isValidForBulkPricing && !isForKit) {
                    //console.log(invalidProductNameList);
                    //console.log(invalidProductNameList.split(', '));
                    //console.log(invalidProductNameList.split(', ').length);
                    //alertmsg('red', 'Vendor is required for Bulk Pricing of product' + (invalidProductNameList.split(',').length > 1 ? '(s) ' : ' ') + invalidProductNameList);
                    alertmsg('red', 'Vendor is required for bulk pricing for the selected product(s)');
                    return false;
                }
                //remove last ,
                //console.log(productIdList);
                kitIdList = kitIdList.substr(0, kitIdList.length - 1);
                productIdList = productIdList.substr(0, productIdList.length - 1);

                if (isForKit)
                    window.location = urlKitBulkPricing + "?kitItemIdList=" + kitIdList;
                else
                    window.location = urlBulkPricing + '?productIdList=' + productIdList;

                //jQuery.ajaxSettings.traditional = true;
                //$.ajax({
                //    url: urlBulkPricing,
                //    data: {
                //        'productIdList': productIdList
                //    },
                //    type: 'POST',
                //    success: function(data) {
                //        //console.log(data);
                //    }
                //});
            });

            //TODO: Assign Products to Kit
            $("#btn-assign-kit-items").click(function () {
                var selectedProductIds = [];
                validateProductForKitCreation();

                $('input[type=checkbox][class=productCatalogSelector]').each(function () {
                    if (this.checked) {

                        if ($(this).parent('td').find('i')[0]) {
                            selectedProductIds.push(this.value);
                        }
                    }
                });

                var isValidForKitPricing = true;
                var invalidProductNameList = '';

                $.each(selectedProductIds, function (index, val) {
                    var productName = $("#product_" + val).text();
                    var vendorProductId = $("#vendorProduct_" + val).data("value");
                    if ($.trim(vendorProductId).length === 0) {
                        invalidProductNameList += '"' + productName + '", ';
                        isValidForKitPricing = false;
                    }

                });

                invalidProductNameList = invalidProductNameList.substr(0, invalidProductNameList.length - 2);
                if (!isValidForKitPricing) {
                    alertmsg('red', 'Vendor is required for kit pricing for the selected product(s)');
                    return false;
                }

                var productIdList = '';
                $.each(selectedProductIds, function (index, val) {
                    var productId = $("#product_" + val).data("value");
                    productIdList += productId + ',';
                });

                productIdList = productIdList.substr(0, productIdList.length - 1);
                var kitId = getQueryStringByName("kitId");

                $.ajax({
                    url: urlAssignProductsToKit,
                    type: 'POST',
                    data: { "kitId": kitId, "productIdsCommaSeparated": productIdList },
                    dataType: "json",
                    beforeSend: function () {
                        SGloading('Loading');
                    },
                    success: function (response) {


                    },
                    complete: function () {
                        SGloadingRemove();
                    },
                    error: function (err) {

                    }
                });

            });
            $("#productCategory").select2({
                placeholder: '--Choose--',
                allowClear: true
            });
            var catalogViewerTable;

            try {
                function initializeCatalogViewer() {

                    // if (queryStringVendorId === undefined || queryStringVendorId === '') {
                    catalogViewerTable = $('#SearchDataTable').dataTableWithFilter({

                        "destroy": true,
                        "bServerSide": true,
                        "bFilter": false,
                        "bSort": false,
                        "stateSave": true,
                        "scrollX": true,
                        "bJQueryUI": true,
                        "aLengthMenu": dataTableMenuLength,
                        "iDisplayLength": dataTableDisplayLength,
                        "dom": 'Blfrtip',
                        "buttons": [
                            {
                                extend: 'colvis', text: 'Select Columns', postfixButtons: ['colvisRestore'], columns: ':gt(0)', collectionLayout: 'fixed four-column'

                            },

                            'copy',

                            //{
                            //    text: 'CSV',
                            //    action: function (e, dt, node, config) {
                            //        var searchModel = getSearchParameterViewModel();
                            //        var catalogStorage = localStorage.getItem("DataTables_SearchDataTable_/Datahub/Catalog");
                            //        var start = -1;
                            //        if (catalogStorage !== undefined) {
                            //            start = JSON.parse(catalogStorage).start;
                            //        }
                            //        //construct download all url with querystring
                            //        var urlToDownload = urlDownloadToCsv +
                            //            "?SearchType=" + (searchModel.SearchType ? searchModel.SearchType : '') +
                            //            "&MatchingCriteria=" + (searchModel.MatchingCriteria ? searchModel.MatchingCriteria : '') +
                            //            "&KeySearch=" + (searchModel.KeySearch ? searchModel.KeySearch : '') +
                            //            "&VendorName=" + (searchModel.VendorName ? searchModel.VendorName : '') +
                            //            "&FromDate=" + (searchModel.FromDate ? searchModel.FromDate : '') +
                            //            "&ToDate=" + (searchModel.ToDate ? searchModel.ToDate : '') +
                            //            "&CostFrom=" + (searchModel.CostFrom ? searchModel.CostFrom : '') +
                            //            "&CostTo=" + (searchModel.CostTo ? searchModel.CostTo : '') +
                            //            "&PriceFrom=" + (searchModel.PriceFrom ? searchModel.PriceFrom : '') +
                            //            "&PriceTo=" + (searchModel.PriceTo ? searchModel.PriceTo : '') +
                            //            "&User=" + (searchModel.User ? searchModel.User : '') +
                            //            "&Search=" + (searchModel.Search ? searchModel.Search : '') +
                            //            "&ForPrimaryVendorProductOnly=" + (searchModel.ForPrimaryVendorProductOnly ? searchModel.ForPrimaryVendorProductOnly : '') +
                            //            "&StoreId=" + (searchModel.StoreId ? searchModel.StoreId : '') +
                            //            "&ProductCategory=" + (searchModel.ProductCategory ? searchModel.ProductCategory : '') +
                            //            "&CompleteFrom=" + (searchModel.CompleteFrom ? searchModel.CompleteFrom : '') +
                            //            "&CompleteTo=" + (searchModel.CompleteTo ? searchModel.CompleteTo : '') +
                            //            "&offsetRows=" + (start ? start : 0) +
                            //            "&fetchRows=" + (dataTableDisplayLength ? dataTableDisplayLength : 0)
                            //    }
                            //},

                            {
                                text: 'Export all to CSV',
                                action: function (e, dt, node, config) {

                                    var searchModel = getSearchParameterViewModel();
                                    if (searchModel.SearchId === '' || searchModel.SearchId === undefined || parseInt(searchModel.SearchId) === 0) {
                                        alertmsg("red", "There are no search results to export. Please search for a product.");
                                        return;
                                    }
                                    //construct download all url with querystring
                                    var urlToDownload = urlDownloadAllToCsv +
                                        "?SearchId=" + (searchModel.SearchId) +
                                        "&SearchType=" + (searchModel.SearchType ? searchModel.SearchType : '') +
                                        "&MatchingCriteria=" + (searchModel.MatchingCriteria ? searchModel.MatchingCriteria : '') +
                                        "&KeySearch=" + (searchModel.KeySearch ? searchModel.KeySearch : '') +
                                        "&VendorName=" + (searchModel.VendorName ? searchModel.VendorName : '') +
                                        "&FromDate=" + (searchModel.FromDate ? searchModel.FromDate : '') +
                                        "&ToDate=" + (searchModel.ToDate ? searchModel.ToDate : '') +
                                        "&CostFrom=" + (searchModel.CostFrom ? searchModel.CostFrom : '') +
                                        "&CostTo=" + (searchModel.CostTo ? searchModel.CostTo : '') +
                                        "&PriceFrom=" + (searchModel.PriceFrom ? searchModel.PriceFrom : '') +
                                        "&PriceTo=" + (searchModel.PriceTo ? searchModel.PriceTo : '') +
                                        "&User=" + (searchModel.User ? searchModel.User : '') +
                                        "&Search=" + (searchModel.Search ? searchModel.Search : '') +
                                        "&ForPrimaryVendorProductOnly=" + (searchModel.ForPrimaryVendorProductOnly ? searchModel.ForPrimaryVendorProductOnly : '') +
                                        "&StoreId=" + (searchModel.StoreId ? searchModel.StoreId : '') +
                                        "&ProductCategory=" + (searchModel.ProductCategory ? searchModel.ProductCategory : '') +
                                        "&CompleteFrom=" + (searchModel.CompleteFrom ? searchModel.CompleteFrom : '') +
                                        "&CompleteTo=" + (searchModel.CompleteTo ? searchModel.CompleteTo : '') + "'";

                                    //redirect to the url to download all csv
                                    window.location = urlToDownload;

                                }
                            }
                        ],

                        "sAjaxSource": urlCatalogViewerSearchFromStaging,
                        "sServerMethod": "POST",
                        "aoColumnDefs": aoColumnsDefsViewBag,
                        "aoColumns": aoColumnsViewBag,
                        // Initialize our custom filtering buttons and the container that the inputs live in
                        filterOptions: { searchButton: "FilterText", clearSearchButton: "ClearSearch", searchContainer: "SearchContainer" },
                        "fnPreDrawCallback": function () {
                            // gather info to compose a message
                            var ob = this;
                            //SGloading('Loading');
                            if (queryStringVendorId === '' || queryStringProductCategoryId === '')
                                SGtableloading({ class: faIcon, message: 'Processing...', objct: ob });
                            $("#SearchText").addClass("non-editable");
                        },
                        "fnDrawCallback": function (settings) {
                            dataTableLoaded = true;
                            if ($("#SearchText").is(":disabled") === true) {
                                $('#SearchText').removeAttr("disabled");
                            }

                            // in case your overlay needs to be put away automatically you can put it here
                            var divid = this.closest('.dataTables_wrapper').attr('id');
                            var dataInDataTable = $('#SearchDataTable tbody tr td').length;
                            if (dataInDataTable > 1) {
                                //data on grid
                                $('#btnCreateProductKit').attr("disabled", false);
                                $('#bulkItemSet').attr("disabled", false);
                                //$('#btnBulkPricing').attr("disabled", false);
                                $('#catalogPricing').attr("disabled", false);
                                $('#catalogBulkPricingNew').attr("disabled", false);
                                $('#catalogVendorProductPricing').attr("disabled", false);
                                SGloadingRemove();
                                SGtableloadingRemove(divid);

                            } else {
                                $('#btnCreateProductKit').attr("disabled", true);
                                $('#bulkItemSet').attr("disabled", true);
                                //$('#btnBulkPricing').attr("disabled", true);
                                $('#catalogPricing').attr("disabled", true);
                                $('#catalogBulkPricingNew').attr("disabled", true);
                                $('#catalogVendorProductPricing').attr("disabled", true);
                            }
                            $("#SearchText").removeClass("non-editable");

                            if (dynamicClassAdded === false) {

                                $('.dt-buttons,.dataTables_length ').wrapAll('<div class="fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr">')
                                dynamicClassAdded = true;

                            }
                            if (queryStringVendorId === '' || queryStringProductCategoryId === '')
                                SGtableloadingRemove(divid);

                            rebuilttooltip();

                        },
                        "fnServerData": function (sSource, aoData, fnCallback) {
                            $.ajax({
                                "dataType": 'json',
                                "contentType": "application/json; charset=utf-8",
                                "type": "GET",
                                "url": sSource,
                                "data": aoData,
                                "success": function (json) {
                                    fnCallback(json);
                                }//,
                                //"error": function (jqXHR, timeout, message) {
                                //    var contentType = jqXHR.getResponseHeader("REQUIRES_AUTH");
                                //    if (jqXHR.status === 200 && contentType=== '1') {
                                //        // assume that our login has expired - reload our current page
                                //        console.log('Reaced');
                                //        window.location.href = "/";
                                //    }
                                //}
                            });
                        }

                    });
                    // }
                }
                //initializeCatalogViewer();
            } catch (e) {

            }
            //debugger;
            initializeCatalogViewer();
            $("#FilterText").click();

        });
        $('#SearchDataTable tbody').on('click', 'tr', function (e) {
            if (e.target.tagName == 'TD') {
                e.stopPropagation();
            }
        });

        ////remember state
        //$(function () {
        //    $("form").rememberState({
        //        objName: "catalog_viewer"
        //    }).submit(false);
        //});

        $('#selectAllCheckBox').click(function () {
            var thisCheck = $(this);
            if (thisCheck.is(':checked')) {
                $('.productCatalogSelector').prop("checked", true);
            } else {
                $('.productCatalogSelector').prop("checked", false);
            }
        });

        function getSearchParameterViewModel() {
            var searchParameterModel = {
                //SearchType: $(".searchDomainClass").val(),
                SearchId: $("#SearchId").val(),
                SearchType: $('input[name=SearchType]:checked').val(),
                SearchTokenName: tokenName,
                MatchingCriteria: $("#matchingCriteria").val(),
                KeySearch: $("#keySearch").val(),
                VendorName: $("#vendorName").val(),
                FromDate: $("#FromDate").val(),
                ToDate: $("#ToDate").val(),
                CostFrom: $("#CostFrom").val(),
                CostTo: $("#CostTo").val(),
                PriceFrom: $("#PriceFrom").val(),
                PriceTo: $("#PriceTo").val(),
                User: $("#User").val(),
                SessionUser: $("#SessionUser").val(),
                Search: $("#fullTextSearch").val(),
                ForPrimaryVendorProductOnly: !$('#ForPrimaryVendorProductOnly').prop('checked'),
                StoreId: $("#storeId").val(),
                ProductCategory: $("#productCategory").val(),
                Brand: $("#pimBrand").val(),
                BigCommerceProduct: $('#BigCommerceProduct').val(),
                CompleteFrom: $("#completeFrom").val(),
                CompleteTo: $("#completeTo").val()
            }
            return searchParameterModel;
        }

        $('#btnBulkPricingAll').click(function (e) {
            var searchModel = getSearchParameterViewModel();
            //ajax
            $.ajax({
                url: urlLoadAllBulkPricing,
                type: 'POST',
                data: { "searchParameter": searchModel },
                dataType: "json",
                beforeSend: function () {
                    SGloading('Loading');
                },
                success: function (response) {
                    window.location.href = urlBulkPricing;

                },
                complete: function () {
                    SGloadingRemove();
                },
                error: function (err) {

                }
            });
        });

        $('#btnCustomFieldUpdateAll').click(function (e) {
            var searchModel = getSearchParameterViewModel();
            //ajax
            $.ajax({
                url: urlLoadAllBulkItem,
                type: 'POST',
                data: { "searchParameter": searchModel },
                dataType: "json",
                beforeSend: function () {
                    SGloading('Loading');
                },
                success: function (response) {
                    window.location.href = urlBulkItem;

                },
                complete: function () {
                    SGloadingRemove();
                },
                error: function (err) {

                }
            });
        });


        $("#btnExportFeed").click(function () {

            var productIds = "";
            $('input[type=checkbox][class=productCatalogSelector]').each(function () {

                if (this.checked) {
                    productIds += $("#product_" + this.value).data("value") + ",";
                }
            });

            $.ajax({
                type: 'POST',
                url: urlKeepSelectedProductsAndGetUrlForFeedGenerator,
                data: { 'productIds': productIds },
                dataType: "json",
                success: function (result) {
                    if (result != null && result !== "")
                        window.location = result;
                },
                error: function (result) {
                    alertmsg("red", errorOccuredMessage);
                }
            });

        });

        $(function () {

            //always show filters section
            //$('#toggleFilter').click();

            //console.log($("#rememberStateLink"));
            //$('#rememberStateLink').trigger('click');
            //$('#rememberStateLink')[0].click();


            //   $('select').select2();

            $('#vendorName').select2(remoteDataConfig(urlGetVendorListForSelect2, 3, 250, pleaseSelectMessage));
            $('#userName').select2(remoteDataConfig(urlGetUserListForSelect2, 2, 150, pleaseSelectMessage));

        });
    } catch (e) {

    }

    var toggleProductKitButton = function () {
        if ($("#searchDomain_Kits").is(":checked") === true) {
            $("#kitsDiv").hide();
        } else {
            $("#kitsDiv").show();
        }
    }

    $("#searchDomain_Kits").change(function () {
        toggleProductKitButton();
    });

    $("#searchDomain_Products").change(function () {
        toggleProductKitButton();
    });


    $(".filter-scroll").mCustomScrollbar({
        theme: "dark"
    });
    //$(".table-wrap").mCustomScrollbar({
    //    theme: "dark-thick"
    //});

} catch (e) {
}





$('#btnBulkPricingNew').click(function () {

    var productIdList = '';
    //var isForKit = $("#searchDomain_Kits").prop('checked');
    var eachIds = [];
    var totalSelectedItemsLength = 0;
    var totalvalidItems = 0;
    var eachKitIds = [];
    var disabledFlag = false;
    var pricingFlag = false;
    $('input[type=checkbox][class=productCatalogSelector]').each(function () {
        if (this.checked) {
            totalSelectedItemsLength++;

            //for disabled product check
            //if ($(this).parent('td').next('td').find('div').find('.fa-ban')[0]) {
            //    disabledFlag = true;
            //    return;

            //}

            // contain only pricing information
            if ($(this).parent('td').next('td').find('div').find('.progress-bar').text().indexOf('P') > 0) {

                var dataDetail = $($(this).parent('td').next('td').find('div').last()).attr("data-detail");

                if (dataDetail.indexOf("11") != 0) {
                    pricingFlag = true;
                    return;
                }
            }

            if ($(this).parent('td').next('td').find('div').find('.progress-bar').text().indexOf('K') > 0) {
                // for kit
                totalvalidItems++;
                eachKitIds.push(this.value);

            } else {
                if ($(this).parent('td').find('i')[0]) {
                    // for primary product
                    eachIds.push(this.value);
                    totalvalidItems++;
                }
            }
        }
    });
    //if (disabledFlag) {
    //    alertmsg('red', 'Please select enable primary products for bulk pricing');
    //    return;
    //}
    if (pricingFlag) {
        alertmsg('red', 'Please select products with vendor information');
        return;
    }
    $.each(eachKitIds, function (index, val) {
        var productId = $("#product_" + val).data("value");
        productIdList += productId + '_' + "0" + '_' + '1' + ',';
    });

    $.each(eachIds, function (index, val) {
        var productId = $("#product_" + val).data("value");
        var productName = $("#product_" + val).text();
        var vendorProductId = $("#vendorProduct_" + val).data("value");
        if ($.trim(vendorProductId).length === 0) {
            invalidProductNameList += '"' + productName + '", ';
            isValidForBulkPricing = false;
        }
        productIdList += productId + '_' + vendorProductId + '_' + '0' + ',';
    });

    if (totalvalidItems === 0 || totalvalidItems !== totalSelectedItemsLength) {
        alertmsg('red', 'Please select a primary product or kit first');
        return;
    }
    //console.log(productIdList);
    productIdList = productIdList.substr(0, productIdList.length - 1);
    window.location = urlBulkVendorProductPricingNew + '?productIdList=' + productIdList + '&page=Bulk Pricing';
    return;
    $('input[type=checkbox][class=productCatalogSelector]').each(function () {
        if (this.checked) {
            totalSelectedItemsLength++;
            if (isForKit) {
                eachIds.push(this.value);

            } else {
                if ($(this).parent('td').find('i')[0]) {
                    eachIds.push(this.value);
                }
            }
        }
    });
    if (!isForKit) {

        if (eachIds.length === 0 || eachIds.length !== totalSelectedItemsLength) {
            alertmsg('red', 'Please select a primary product first');
            return;
        }
    }

    //if (eachIds.length === 1) {
    //    alertmsg('red', 'Please select multiple products for bulk pricing');
    //    return;
    //}
    var isValidForBulkPricing = true;
    var invalidProductNameList = '';
    var kitIdList = '';
    var productIdList = '';
    $.each(eachIds, function (index, val) {
        var productId = $("#product_" + val).data("value");
        var productName = $("#product_" + val).text();
        var vendorProductId = $("#vendorProduct_" + val).data("value");
        if ($.trim(vendorProductId).length === 0) {
            invalidProductNameList += '"' + productName + '", ';
            isValidForBulkPricing = false;
        }
        kitIdList += productId + ",";
        productIdList += productId + ',' + vendorProductId + ',';
    });
    invalidProductNameList = invalidProductNameList.substr(0, invalidProductNameList.length - 2);
    if (!isValidForBulkPricing && !isForKit) {
        //console.log(invalidProductNameList);
        //console.log(invalidProductNameList.split(', '));
        //console.log(invalidProductNameList.split(', ').length);
        //alertmsg('red', 'Vendor is required for Bulk Pricing of product' + (invalidProductNameList.split(',').length > 1 ? '(s) ' : ' ') + invalidProductNameList);
        alertmsg('red', 'Vendor is required for bulk pricing for the selected product(s)');
        return false;
    }
    //remove last ,
    //console.log(productIdList);
    kitIdList = kitIdList.substr(0, kitIdList.length - 1);
    productIdList = productIdList.substr(0, productIdList.length - 1);

    if (isForKit)
        window.location = urlKitBulkPricing + "?kitItemIdList=" + kitIdList;
    else
        window.location = urlBulkVendorProductPricingNew + '?productIdList=' + productIdList + '&page=Bulk Pricing';

    //jQuery.ajaxSettings.traditional = true;
    //$.ajax({
    //    url: urlBulkPricing,
    //    data: {
    //        'productIdList': productIdList
    //    },
    //    type: 'POST',
    //    success: function(data) {
    //        //console.log(data);
    //    }
    //});
});


$('#btnBulkPricingNewAll').click(function () {
    var searchModel = getSearchParameterViewModel();
    $.ajax({
        url: urlLoadAllBulkPricing,
        type: 'POST',
        data: { "searchParameter": searchModel, "isBulkPricing": true },
        dataType: "json",
        beforeSend: function () {
            SGloading('Loading');
        },
        success: function (response) {
            window.location.href = urlBulkVendorProductPricingNew + '?page=Bulk Pricing';

        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (err) {

        }
    });

});

$('#btnVendorProductPricing').click(function () {

    var productIdList = '';
    //var isForKit = $("#searchDomain_Kits").prop('checked');
    var eachIds = [];
    var totalSelectedItemsLength = 0;
    var totalvalidItems = 0;
    var eachKitIds = [];
    var disabledFlag = false;
    var pricingFlag = false;
    $('input[type=checkbox][class=productCatalogSelector]').each(function () {
        if (this.checked) {
            totalSelectedItemsLength++;

            if ($(this).parent('td').next('td').find('div').find('.progress-bar').text().indexOf('P') > 0) {

                var dataDetail = $($(this).parent('td').next('td').find('div').last()).attr("data-detail");

                if (dataDetail.indexOf("11") != 0) {
                    pricingFlag = true;
                    return;
                }
            }

            if ($(this).parent('td').next('td').find('div').find('.progress-bar').text().indexOf('K') > 0) {
                // for kit
                totalvalidItems++;
                eachKitIds.push(this.value);

                //} else {
                //    if ($(this).parent('td').find('i')[0]) {
                //        // for primary product
                //        eachIds.push(this.value);
                //        totalvalidItems++;
                //    }
            } else {

                // for products
                eachIds.push(this.value);
                totalvalidItems++;
            }

        }
    });
    //if (disabledFlag) {
    //    alertmsg('red', 'Please select enable primary products for bulk pricing');
    //    return;
    //}
    if (pricingFlag) {
        alertmsg('red', 'Please select products with vendor information');
        return;
    }
    $.each(eachKitIds, function (index, val) {
        var productId = $("#product_" + val).data("value");
        productIdList += productId + '_' + "0" + '_' + '1' + ',';
    });

    $.each(eachIds, function (index, val) {
        var productId = $("#product_" + val).data("value");
        var productName = $("#product_" + val).text();
        var vendorProductId = $("#vendorProduct_" + val).data("value");
        if ($.trim(vendorProductId).length === 0) {
            invalidProductNameList += '"' + productName + '", ';
            isValidForBulkPricing = false;
        }
        productIdList += productId + '_' + vendorProductId + '_' + '0' + ',';
    });

    //if (totalvalidItems === 0 || totalvalidItems !== totalSelectedItemsLength) {
    //    alertmsg('red', 'Please select a primary product or kit first');
    //    return;
    //}
    //console.log(productIdList);
    var includeSecondaryVendor = $('#ForPrimaryVendorProductOnly').val() == undefined ? false : true;
    productIdList = productIdList.substr(0, productIdList.length - 1);
    window.location = urlBulkVendorProductPricingNew + '?productIdList=' + productIdList + '&page=Bulk Vendor Product';
    return;
    $('input[type=checkbox][class=productCatalogSelector]').each(function () {
        if (this.checked) {
            totalSelectedItemsLength++;
            if (isForKit) {
                eachIds.push(this.value);

            } else {
                if ($(this).parent('td').find('i')[0]) {
                    eachIds.push(this.value);
                }
            }
        }
    });
    if (!isForKit) {

        if (eachIds.length === 0 || eachIds.length !== totalSelectedItemsLength) {
            alertmsg('red', 'Please select a primary product first');
            return;
        }
    }

    var isValidForBulkPricing = true;
    var invalidProductNameList = '';
    var kitIdList = '';
    var productIdList = '';
    $.each(eachIds, function (index, val) {
        var productId = $("#product_" + val).data("value");
        var productName = $("#product_" + val).text();
        var vendorProductId = $("#vendorProduct_" + val).data("value");
        if ($.trim(vendorProductId).length === 0) {
            invalidProductNameList += '"' + productName + '", ';
            isValidForBulkPricing = false;
        }
        kitIdList += productId + ",";
        productIdList += productId + ',' + vendorProductId + ',';
    });
    invalidProductNameList = invalidProductNameList.substr(0, invalidProductNameList.length - 2);
    if (!isValidForBulkPricing && !isForKit) {
        //console.log(invalidProductNameList);
        //console.log(invalidProductNameList.split(', '));
        //console.log(invalidProductNameList.split(', ').length);
        //alertmsg('red', 'Vendor is required for Bulk Pricing of product' + (invalidProductNameList.split(',').length > 1 ? '(s) ' : ' ') + invalidProductNameList);
        alertmsg('red', 'Vendor is required for bulk pricing for the selected product(s)');
        return false;
    }
    //remove last ,
    //console.log(productIdList);
    kitIdList = kitIdList.substr(0, kitIdList.length - 1);
    productIdList = productIdList.substr(0, productIdList.length - 1);

    if (isForKit)
        window.location = urlKitBulkPricing + "?kitItemIdList=" + kitIdList;
    else
        window.location = urlBulkVendorProductPricingNew + '?productIdList=' + productIdList + '&page=Bulk Pricing';

});

$('#btnVendorProductPricingAll').click(function () {

    var searchModel = getSearchParameterViewModel();
    $.ajax({
        url: urlLoadAllBulkPricing,
        type: 'POST',
        data: { "searchParameter": searchModel, "isBulkPricing": false },
        dataType: "json",
        beforeSend: function () {
            btnVendorProductPricingAll
            SGloading('Loading');
        },
        success: function (response) {
            window.location.href = urlBulkVendorProductPricingNew + '?form=Bulk Vendor Product';

        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (err) {

        }
    });

});

function callBackgroundProcessOption(searchId, interval, searchModel) {
    totalTime = parseInt(totalTime + interval);

    $.ajax({
        url: urlGetSearchStatus,
        type: "GET",
        data: {
            "searchId": searchId

        },
        success: function (data) {
            if (data !== "3") {
                if (totalTime > 9000 && callBackgroundProcess === true) {
                    callBackgroundProcess = false;
                    SGtableloadingRemove();
                    // $('#BackgroundCatalogModal').modal('show');
                    $('#BackgroundCatalogModal').modal({
                        show: true,
                        keyboard: false,
                        backdrop: 'static'
                    });
                    $('#txtSearchToken').val(tokenName);
                }
            }
            else {
                globalSearchId = searchId;
                enterSearch = false;
                // alert('data loaded !!!')
                $('#BackgroundCatalogModal').modal('hide');
                clearInterval(clearIntervalId);
                SGtableloadingRemove();
                $("#FilterText").click();


                //if (dataTableLoaded === false) {
                //    initializeCatalogViewer();
                //}

                ///post data load part here.
            }
        },
        error: function () { },
        complete: function () {
            SGloadingRemove();
        }
        ,
        complete: function () {
            SGtableloadingRemove();
        }

    })

}

$('#btnYesBackgroundCatalogModalModal').click(function () {

    tokenName = $('#txtSearchToken').val();
    if (tokenName === '') { return; }
    $('#SearchTokenName').val(tokenName);

    $.ajax({
        url: setStaggingTableSearchFlagToBg,
        type: 'POST',
        data: { "searchId": $('#SearchId').val(), 'SearchTokenName': tokenName },
        dataType: "json",

        success: function (response) {

            // window.location.href = '/Datahub/Dashboard';
            window.location.href = urlRedirect;
        },
        complete: function () {
            SGloadingRemove();
        },
        error: function (err) {
            window.location.href = urlRedirect;
        }
    });

});

$('#btnNoBackgroundCatalogModalModal').click(function () {

    //SGtableloading({ class: faIcon, message: 'Processing...', objct: ob });
    if (dataTableLoaded === false) {
        SGtableloading(faIcon, '', this);
    }

});

//SKYG-417 fix: Prevent double enter key press to invoke search
$("#fullTextSearch").keydown(function (event) {
    if (event.keyCode == 13) {
        enterSearch = true;
        $("#SearchText").click();
    }
})

