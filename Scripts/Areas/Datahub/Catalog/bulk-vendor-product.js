﻿var fileData;
var columns1 = [];
var container = document.getElementById('hot-bulk-vendor-product');
var hot;
var isImport = false;
isbulkVendorProductModule = true;
var isSalePriceDisabled = false;

//variables for change tracking
var changeLevelArray = new Array();
var changeLevelArrayPerLot = new Array();
var productIdList = getQueryStringByName("productIdList");
//encoded data
var encodedDataBulkVendorProductSetting = Encoder.htmlDecode(dataBulkVendorProductSetting);
//dataBulkVendorProductSetting = dataBulkVendorProductSetting.replace("\"##[", "[").replace("##]\"", "]");
var encodedDataBulkVendorProductSetting2 = encodedDataBulkVendorProductSetting
    .replace(/\"##\[/g, "[")
    .replace(/\]##\"/g, "]");
var jsonDataBulkVendorProductSetting = $.parseJSON(encodedDataBulkVendorProductSetting2);
var search = false;
var vendorMinOrderLineArry = fixJsonQuote(vendorMinOrderLine)




var bulkVendorProductColumns = [];
var prepareBulkVendorProductColumns = function () {
    bulkVendorProductColumns = [];
    $.grep(jsonDataBulkVendorProductSetting,
        function (value, index) {
            var setting = {};
            setting.title = value.ColumnName;
            setting.data = value.FileColumnName;
            //setting.type = getHandsontableTypeFromCustomFieldDataType(value.Name);
            //if (value.AllowedValues && value.AllowedValues.length > 0) {
            //    setting.type = "dropdown";
            //    setting.source = value.AllowedValues;
            //}

            //console.log(value);

            var otherSetting =
                getHandsontableTypeFromCustomFieldDataType2(value.FieldDataType, value.AllowedValues, value.ColumnName);

            $.extend(true, setting, otherSetting);

            bulkVendorProductColumns.push(setting);
        });
}

prepareBulkVendorProductColumns();
//console.log(bulkVendorProductColumns);

dataDownloadTemplate = Encoder.htmlDecode(dataDownloadTemplate);

var vendorId = getQueryStringByName("vendorId");
if (!vendorId) {
    $("#Vendor").select2(remoteDataConfig(urlGetVendorListForSelect2, 3, 250, msgPleaseSelect));
}

var selectAllCells = function () {
    if (hot) {
        hot.selectCell(0, 0, hot.countRows() - 1, hot.countCols() - 1);
    }
}

//var replacementColumnsXml = ["VendorPartNumber", "QOH"];
var columnsXml = dataDownloadTemplate.split(',');

var bulkVendorProductErrorManager = function (option) {
    option = option || {};
    var retainControls = option.retainControls || true;
    var retainHotData = option.retainHotData || false;

    //if (retainControls) {
    //    $("#import-qoh-sheet").removeClass("hidden");
    //    $("#import-qoh-setting").removeClass("hidden");
    //} else {
    //    $("#import-qoh-sheet").addClass("hidden");
    //    $("#import-qoh-setting").addClass("hidden");
    //}

    if (!retainHotData && hot) {
        hot.updateSettings({ data: [] });
    }
};

// ReSharper disable once NativeTypePrototypeExtending
Array.prototype.selectBulkVendorProductXml = function (columns, rowNode, parentNode) {
    //get the array
    var array = this;

    //placeholder for xml (to be returned)
    var xmlData = "";
    var xmlDataPricingLevel = "";

    //loop through each data row
    $.grep(array, function (element, index) {
        var data = "";
        var dataPricingLevel = "";

        //write to both xmlData & xmlDataPricingLevel
        var commonColumns = ["LocalSku", "VendorPartNumber", "SupplierName", "SetToPrimary", "Cost", "Local Part Number", "Vendor Part Number", "Vendor"];

        //loop through each column
        for (var i = 0; i < columns.length; i++) {

            //get column name and value
            var colName = columns[i].title;
            var value = element[columns[i].data];
            if (value === 0) { value = "0"; }
            //encode them
            if (colName) {
                colName = colName.toString();
                colName = Encoder.htmlEncode(colName);
            }
            if (value) {
                value = value.toString();
                value = Encoder.htmlDecode(value);
            }
            //if (value == "")
            //    value = null;
            var pricingLevel = getPricingLevel(columns[i].data);
            if (commonColumns.indexOf(columns[i].data) >= 0 || pricingLevel != null) {
                //write to both data & dataPricingLevel (for common columns)
                if (value) {
                    //for cell having data, wrap in CDATA for the xml node
                    data += "<{0} Name='{1}'><![CDATA[{2}]]></{0}>".format("Attribute", colName, value);
                    dataPricingLevel += "<{0} Name='{1}'><![CDATA[{2}]]></{0}>".format("Attribute", colName, value);
                } else {
                    //for no data, send NULL as value
                    data += "<{0} Name='{1}'><![CDATA[{2}]]></{0}>".format("Attribute", colName, "");
                    dataPricingLevel += "<{0} Name='{1}'><![CDATA[{2}]]></{0}>".format("Attribute", colName, "");
                }
            }
            /*
            else if (pricingLevel != null) {
                //write to dataPricingLevel (for columns in pricing/purchasing level)
                if (value) {
                    dataPricingLevel += "<{0} Name='{1}'><![CDATA[{2}]]></{0}>".format("Attribute", colName, value);
                } else {
                    dataPricingLevel += "<{0} Name='{1}'><![CDATA[{2}]]></{0}>".format("Attribute", colName, "");
                }
            } 
            */
            else {
                //write to data (for other columns)
                if (value) {
                    //for cell having data, wrap in CDATA for the xml node
                    data += "<{0} Name='{1}'><![CDATA[{2}]]></{0}>".format("Attribute", colName, value);
                } else {
                    //for no data, send NULL as value
                    data += "<{0} Name='{1}'><![CDATA[{2}]]></{0}>".format("Attribute", colName, "");
                }
            }

            //if (value) {
            //    //for cell having data, wrap in CDATA for the xml node
            //    data += "<{0} Name='{1}'><![CDATA[{2}]]></{0}>".format("Attribute", colName, value);
            //} else {
            //    //for no data, send NULL as value
            //    data += "<{0} Name='{1}'><![CDATA[{2}]]></{0}>".format("Attribute", colName, "");
            //}

            //var pricingLevel = getPricingLevel(columns[i].data);
            //if (commonColumns.indexOf(columns[i].data) >= 0 || pricingLevel != null) {
            //    if (value) {
            //        dataPricingLevel += "<{0} Name='{1}'><![CDATA[{2}]]></{0}>".format("Attribute", colName, value);
            //    } else {
            //        dataPricingLevel += "<{0} Name='{1}'><![CDATA[{2}]]></{0}>".format("Attribute", colName, "");
            //    }
            //}
        }

        //wrap the row data with rowNode
        //xmlData += "<{0} id='{1}'>{2}</{0}>".format(rowNode, index + 1, data);
        xmlData += "<{0} id='{1}'>{2}</{0}>".format(rowNode, index + 1, data);
        if (dataPricingLevel)
            xmlDataPricingLevel += "<{0} id='{1}'>{2}</{0}>".format(rowNode, index + 1, dataPricingLevel);

    });

    //wrap the entire xml within parentNode
    var xmlVersion = "<?xml version='1.0' encoding='ISO-8859-1' ?>";
    xmlData = "{0}<{1}>{2}</{1}>".format("", parentNode, xmlData);
    xmlDataPricingLevel = "{0}<{1}>{2}</{1}>".format("", parentNode, xmlDataPricingLevel);

    //xml is prepared, time to return it to the caller
    return { dataOriginal: xmlData, pricingLevelData: xmlDataPricingLevel };

}

//to add for catalogallsearch
//if (!isEmptyOrSpaces(CatalogSearchAllVendorProducts)) {
//    debugger;
//    dataFrom = 1; /// product 
//    dataId = CatalogSearchAllVendorProducts;
//    //   getPaginatedGrid(dataFrom, dataId);
//    var totalNumber = CatalogSearchAllVendorProducts.split(",").length;

//    getBulkVendorProductData(dataFrom, 'test', 1, totalNumber, isBulkPricing);
//}


//if (productIdList !== "") {
//    dataFrom = 1; /// product 
//    dataId = productIdList;
//    //   getPaginatedGrid(dataFrom, dataId);
//    var totalNumber = productIdList.split(",").length;

//    getBulkVendorProductData(dataFrom, dataId, 1, totalNumber, isBulkPricing);
//}

$('#btn-import')
    .click(function () {
        //simulate input[type=file] click via button click
        $("#bulk-vendor-product-file").val("");
        $('#bulk-vendor-product-file').click();
    });

$('#btn-download-template')
    .click(function () {

        //---> Case 1: if download data exists, download template directly
        //var dataDownloadTemplateColumns = dataDownloadTemplate;
        //if (isBulkPricing == "True")
        //{
        //    var dataDownloadTemplateColumns = cleanColumnForBulkPricing(dataDownloadTemplate)
        //}
        if (dataDownloadTemplate) {
            var data = dataDownloadTemplate;
            data += "\n";

            //---prepare and download the csv start---
            var link = document.createElement("a");
            link.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(data));
            if (isBulkPricing == "True" || isBulkPricing == true) {
                if (pageTitle == 'Bulk Item Create') { link.setAttribute("download", "BulkItemCreate.csv"); }
                else { link.setAttribute("download", "BulkPricing.csv"); }
            }

            else
                link.setAttribute("download", "BulkVendorProduct.csv");

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);

            //for case 1, return from here so that case 2 won't be executed
            return;
        }

        //---> Case 2: otherwise perform an ajax call and let user download template
        $.ajax({
            type: "GET",
            url: urlDownloadTemplateBulkVendorProduct,
            cache: false,
            contentType: 'application/json; charset=utf-8',
            data: { 'isBulkPricing': isBulkPricing == false },
            //data: {'isBulkPricing': isBulkPricing == "True"? true:false},
            dataType: "json",
            beforeSend: function () {
                SGloading("Downloading");
            },
            success: function (response) {
                if (response) {
                    if (response.Status === true) {

                        //get data and append newline at the end
                        var data = response.Data;
                        //if (isBulkPricing == "True" || isBulkPricing == true)
                        //data = cleanColumnForBulkPricing(data);
                        //data += "\n";

                        //---prepare and download the csv start---
                        var link = document.createElement("a");
                        link.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(data));
                        if (isBulkPricing == "True" || isBulkPricing == true)
                            link.setAttribute("download", "BulkPricing.csv");
                        else
                            link.setAttribute("download", "BulkVendorProduct.csv");

                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                        //---prepare and download the csv end---

                        //save this for later download
                        dataDownloadTemplate = response.Data;

                        alertmsg("green", response.Message, -1);
                    } else {
                        alertmsg("red", response.Message, -1);
                    }
                } else {
                    alertmsg("red", msgErrorOccured, -1);
                }
            },
            complete: function () {
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                console.log(status + ' ' + error);
                alertmsg("red", msgErrorOccured + " while downloading template", -1);
            }
        });

    });

//get columns of file (using papa-parse)
function getFileHeaders(evt, callback) {
    var file = evt.target.files[0];
    //console.log(file);
    var columns = [];

    Papa.parse(file, {
        delimiter: "",	// auto-detect
        newline: "",	// auto-detect
        header: false,
        dynamicTyping: false,
        preview: 0,
        encoding: "",
        worker: false,
        comments: false,
        step: undefined,
        error: undefined,
        download: false,
        skipEmptyLines: false,
        chunk: undefined,
        fastMode: undefined,
        beforeFirstChunk: undefined,
        withCredentials: undefined,
        complete: callback
    });

    return columns;
}

var getColumnSetting = function (parsedColumns) {

    var addedSetting = {
        "placeholder": ""
    };

    //var addedSetting = {
    //    "placeholder": "",
    //    "type": "dropdown",
    //    "source": ["Yes", "No"]
    //};

    //if (parsedColumns.data == "VP_SetToPrimary" || parsedColumns.data == "VP_IsDisabled") {
    //    var booleanYesNo = "Yes,No".split(",");
    //    addedSetting.type = "dropdown";
    //    addedSetting.source = booleanYesNo;
    //}

    return $.extend(true, parsedColumns, addedSetting);
}

var getMaxPricingLevel = function (columnEntity) {
    var maxPricingLevel = 0;
    var maxPricingLevelIndex = 0;
    for (var i = 0; i < jsonDataBulkVendorProductSetting.length; i++) {
        var value = jsonDataBulkVendorProductSetting[i];
        if (value.Entity == columnEntity) {
            var pricingLevel = getPricingLevel(value.ColumnName);
            pricingLevel = parseInt(pricingLevel) || 0;
            if (maxPricingLevel <= pricingLevel) {
                maxPricingLevel = pricingLevel;
                maxPricingLevelIndex = i;
            }
        }
    }
    return { level: maxPricingLevel, index: maxPricingLevelIndex };
};

var contextMenuBulkVendorProduct = {
    callback: function (key, options) {
        var l = options.length - 1;
        if (key === 'col_add') {
            // if not Quantity/Margin/Discount column, disable this option
           
            var selectedColIndex = options[l].start.col;
            var selectedColName = hot.getColHeader(selectedColIndex);

            var selectedColProperty = null;
            var selectedColEntity = null;
            for (var i = 0; i < jsonDataBulkVendorProductSetting.length; i++) {
                var value = jsonDataBulkVendorProductSetting[i];
                if (value.FileColumnName == selectedColName) {
                    selectedColProperty = value.ColumnName;
                    selectedColEntity = value.Entity;
                    break;
                }
            }

            var maxPricingLevel = getMaxPricingLevel(selectedColEntity);
            //get quantity discount for maxPricingLevel (N = maxPricingLevel.level)
            //get (N-0)th item
            var n0Item = jsonDataBulkVendorProductSetting[maxPricingLevel.index - 0];
            //get (N-1)th item
            var n1Item = jsonDataBulkVendorProductSetting[maxPricingLevel.index - 1];
            //get (N-2)th item
            var n2Item = jsonDataBulkVendorProductSetting[maxPricingLevel.index - 2];

            //Increment them (we have quantity, margin and discount together)
            var n0ItemIncremented = JSON.parse(JSON.stringify(n0Item).replace(new RegExp(maxPricingLevel.level, 'g'), maxPricingLevel.level + 1));
            var n1ItemIncremented = JSON.parse(JSON.stringify(n1Item).replace(new RegExp(maxPricingLevel.level, 'g'), maxPricingLevel.level + 1));
            var n2ItemIncremented = JSON.parse(JSON.stringify(n2Item).replace(new RegExp(maxPricingLevel.level, 'g'), maxPricingLevel.level + 1));
            //add the incremented columns in their respective position
            jsonDataBulkVendorProductSetting.splice(maxPricingLevel.index + 1, 0, n0ItemIncremented);
            jsonDataBulkVendorProductSetting.splice(maxPricingLevel.index + 2, 0, n1ItemIncremented);
            jsonDataBulkVendorProductSetting.splice(maxPricingLevel.index + 3, 0, n2ItemIncremented);

            prepareBulkVendorProductColumns();
            bulkVendorProductColumns = replaceOriginalColumns(bulkVendorProductColumns);
            hot.updateSettings({
                columns: bulkVendorProductColumns
            });
            
            hot.selectCell(options[l].start.row, options[l].start.col + 3);

        }
        else if (key === 'col_remove') {
            // if not Quantity/Margin/Discount column, disable this option
            var selectedColIndex = options[l].start.col;
            var selectedColName = hot.getColHeader(selectedColIndex);

            var selectedColProperty = null;
            var selectedColEntity = null;
            for (var i = 0; i < jsonDataBulkVendorProductSetting.length; i++) {
                var value = jsonDataBulkVendorProductSetting[i];
                if (value.FileColumnName == selectedColName) {
                    selectedColProperty = value.ColumnName;
                    selectedColEntity = value.Entity;
                    break;
                }
            }

            var maxPricingLevel = getMaxPricingLevel(selectedColEntity);

            //remove the selected columns from their respective position
            jsonDataBulkVendorProductSetting.splice(maxPricingLevel.index - 0, 1);
            jsonDataBulkVendorProductSetting.splice(maxPricingLevel.index - 1, 1);
            jsonDataBulkVendorProductSetting.splice(maxPricingLevel.index - 2, 1);

            //TODO: remove the corresponding data too


            prepareBulkVendorProductColumns();
            bulkVendorProductColumns = replaceOriginalColumns(bulkVendorProductColumns);
            hot.updateSettings({
                columns: bulkVendorProductColumns
            });

            hot.selectCell(options[l].start.row, options[l].start.col - 3);
        }
    },
    items: {
        //remove_row: TODO: need to re-index
        'row_above': {},
        'row_below': {},
        'remove_row': {},
        'hsep1': "---------",
        'col_add': {
            name: 'Insert column (Quantity/Margin/Discount)',
            disabled: function () {
                ////ensure that user selects only one column
                //if (hot.getSelected()[1] != hot.getSelected()[3])
                //    return true;

                // if not Quantity/Margin/Discount column, disable this option
                var selectedColIndex = hot.getSelected()[1];
                var selectedColName = hot.getColHeader(selectedColIndex);

                var selectedColProperty = null;
                var selectedColEntity = null;

                for (var i = 0; i < jsonDataBulkVendorProductSetting.length; i++) {
                    var value = jsonDataBulkVendorProductSetting[i];
                    if (value.FileColumnName == selectedColName) {
                        selectedColProperty = value.ColumnName;
                        selectedColEntity = value.Entity;
                        break;
                    }
                }
                var maxPricingLevel = getMaxPricingLevel(selectedColEntity);
                var currentPricingLevel = getPricingLevel(selectedColProperty);

                return !(maxPricingLevel.level == currentPricingLevel);
            }
        },
        'col_remove': {
            name: 'Remove column (Quantity/Margin/Discount)',
            disabled: function () {
                ////ensure that user selects only one column
                //if (hot.getSelected()[1] != hot.getSelected()[3])
                //    return true;

                // if not Quantity/Margin/Discount column, disable this option
                var selectedColIndex = hot.getSelected()[1];
                var selectedColName = hot.getColHeader(selectedColIndex);

                var selectedColProperty = null;
                var selectedColEntity = null;
                for (var i = 0; i < jsonDataBulkVendorProductSetting.length; i++) {
                    var value = jsonDataBulkVendorProductSetting[i];
                    if (value.FileColumnName == selectedColName) {
                        selectedColProperty = value.ColumnName;
                        selectedColEntity = value.Entity;
                        break;
                    }
                }
                var maxPricingLevel = getMaxPricingLevel(selectedColEntity);
                var currentPricingLevel = getPricingLevel(selectedColProperty);

                //for first level of qty/margin/discount, do not allow to delete
                if (maxPricingLevel.level == 1)
                    return true;

                return !(maxPricingLevel.level == currentPricingLevel);
            }
        },
        'hsep2': "---------",
        'undo': {},
        'redo': {},
        'hsep3': "---------",
        //'make_read_only': {},
        'alignment': {},
        'hsep4': "---------"
        //"about": { name: 'About SkyGeek' },
        //"clear_table": {
        //    name: 'Clear everything'
        //}
    }
};

var replaceOriginalColumnHeaderInData = function (data) {

    if (!data) {
        throw new Error("data cannot be null");
    }

    //if (!colToPropCache) {
    //    throw new Error("columns cannot be null");
    //}

    data = JSON.stringify(data);

    $.each(jsonDataBulkVendorProductSetting,
        function (index, value) {
            var regex = new RegExp(value.FileColumnName, "g");
            data = data.replace(regex, value.ColumnName);
        });

    $.each(jsonDataBulkVendorProductSetting,
        function (index, value) {
            var regex = new RegExp(value.FileColumnName, "g");
            data = data.replace(regex, value.ColumnName);
        });

    data = JSON.parse(data);

    return data;
};


var replaceOriginalColumns = function (columns) {

    if (!columns) {
        throw new Error("columns cannot be null");
    }

    //if (!colToPropCache) {
    //    throw new Error("columns cannot be null");
    //}

    for (i = 0; i < columns.length; i++) {
        columns[i].data = columns[i].title.trim();
        //    if (columns[i].data === "Margin1" || columns[i].data === "Margin2" || columns[i].data === "Margin3" || columns[i].data === "Margin4")
        //    {
        //        columns[i].data = columns[i].title.trim();
        //    }
        //    if (columns[i].data === "Margin1" || columns[i].data === "Margin2" || columns[i].data === "Margin3" || columns[i].data === "Margin4") {
        //        columns[i].data = columns[i].title.trim();
    }


    //}

    $.each(jsonDataBulkVendorProductSetting,
        function (index, value) {
            var regex = new RegExp(value.FileColumnName, "g");
            if (columns) {
                for (var i = 0; i < columns.length; i++) {
                    columns[i].data = columns[i].data.replace(regex, value.ColumnName);
                }
            }
        });

    return columns;
};

//var tour = new Tour({
//    steps: [{
//        element: "#btn-save",
//        title: "Click to save",
//        //content: "Click this button to save",
//        placement: "top",
//        next: -1,
//        prev: -1
//    }]
//});

//tour.init();

$("#bulk-vendor-product-file")
    .change(function (e) {

        SGloading("Loading");

        /*
        //fix for Styles Logistics (vendorId = 0)
        vendorId = parseInt(vendorId);
        if (!isFinite(vendorId) || vendorId < 0) {
            alertmsg("red", "Please select a vendor first");
            return;
        }
        */
       // salePriceErrorArr = [];
       // disabledSalePrice = [];
        var config = buildConfig();

        //For Papa Parse v4.1.2, it does not auto-detect delimiter for file having two columns. so specify delimiter (,)
        config.delimiter = ",";

        //use web workers
        config.worker = true;

        var files = $('#bulk-vendor-product-file')[0].files;
        if (files.length > 0) {


            //For file having size > 1024kB or 1MB, don't call papa parse
            //if (files[0].size > 1024 * 1024) {

            //    //If we reach here, file size is not within acceptable limit (>1000 rows)

            //    //Show appropriate message to user
            //    sizeExceededWarning = Encoder.htmlDecode(sizeExceededWarning);
            //    udh.notify.warn(sizeExceededWarning);

            //    //Make the div "#hot-bulk-vendor-product" distinct
            //    $("#hot-bulk-vendor-product").addClass("hot-distinct-message");
            //    $("#hot-bulk-vendor-product").hide();

            //    if (hot) {
            //        hot.updateSettings({
            //            data: null,
            //            columns: null,
            //            fixedColumnsLeft: 0,
            //            startCols: 0,
            //            startRows: 0
            //        });
            //        hot.render();
            //    }

            //    SGloadingRemove();

            //    return;
            //}

            $('#bulk-vendor-product-file')
                .parse({
                    config: config,
                    before: function (file, inputElem) {
                        //console.log("Parsing file:", file);
                        SGloading("Loading");
                        columns1 = [];
                        if (hot) {
                            hot.updateSettings({ data: [] });
                        }
                    },
                    complete: function () {

                        //check for larger files

                        isImport = true;

                        //If we reach here, file size is within the acceptable limit (<=1000 rows)

                        SGloadingRemove();

                        console.log(columns1);
                        // //console.log(columns);
                        //console.log(fileData);
                        var cleansedColumns = [];
                        var cleansedColumnsWithSettings = [];
                        //console.log(columns1);
                        $.grep(columns1,
                            function (n, i) {
                                var columnString = JSON.stringify(n)
                                    .replace(/\=\\\"/g, "")
                                    .replace(/\\\"\"/g, "\"")
                                    .replace(/\\\"/g, "''")
                                    .replace(/ˏ/g, ",")
                                    .replace(/\\r/g, "")
                                    .replace(/\\n/g, "");

                                //console.log(columnString);

                                var parsedColumns = fixJsonQuote(columnString);
                                parsedColumns.title = parsedColumns.title.trim();
                                parsedColumns.data = parsedColumns.data.trim();
                                ReplaceSingle2Quote(parsedColumns);

                                cleansedColumns.push(parsedColumns);
                                cleansedColumnsWithSettings.push(getColumnSetting(parsedColumns));

                            });

                        //console.log(cleansedColumns);

                        // / * -----

                        //check if columns in file (cleansedColumns) contains columns in download template (dataDownloadTemplate)
                        var columnsInDownloadTemplate = [];
                        columnsInDownloadTemplate = dataDownloadTemplate.split(',');


                        var matchedColumn = [];
                        $.grep(cleansedColumns,
                            function (fileValue, iFile) {
                                for (var i = 0; i < columnsInDownloadTemplate.length; i++) {
                                    if (columnsInDownloadTemplate[i].toLowerCase().trim() == fileValue.data.toLowerCase().trim())
           matchedColumn.push(columnsInDownloadTemplate[i]);
                                }
                            });
                        //console.log(matchedColumn);

                        if (matchedColumn.length == 0) {
                            alertmsg("red", "File does not contain any column from Download Template", -1);
                            bulkVendorProductErrorManager({ retainControls: true, retainHotData: false });
                            return;
                        }
                        if (matchedColumn.length != columnsInDownloadTemplate.length) {
                            alertmsg("red", "File should contain the columns from Download Template. Matched '{0}' column(s) out of '{1} column(s)".format(matchedColumn.length, columnsInDownloadTemplate.length), -1);
                            bulkVendorProductErrorManager({ retainControls: true, retainHotData: false });
                            return;
                        }

                        if (columnsInDownloadTemplate.length != cleansedColumns.length) {
                            alertmsg("red", "File should contain the columns from Download Template. Matched '{0}' column(s) out of '{1} column(s)".format(matchedColumn.length, cleansedColumns.length), -1);
                            bulkVendorProductErrorManager({ retainControls: true, retainHotData: false });
                            return;
                        }


                        var dataCheck = fileData;
                        columnsXml = matchedColumn;
                        // ----- * /

                        //data to show to handsontable
                        var cleansedData = fileData;

                        ////map data to expected columnName
                            
                           //cleansedData = replaceOriginalColumnHeaderInData(cleansedData);
                            bulkVendorProductColumns = replaceOriginalColumns(bulkVendorProductColumns);

                            //perform auto calculation
                            //cleansedData = performAutoCalculation(cleansedData, bulkVendorProductColumns || cleansedColumns);

                        for (i = 0; i < cleansedColumns.length; i++) {

                            for (j = 0; j < bulkVendorProductColumns.length; j++) {
                                if (cleansedColumns[i].title.trim() === bulkVendorProductColumns[j].title.trim()) {
                                    cleansedColumns[i].type = bulkVendorProductColumns[j].type;
                                    cleansedColumns[i].format = bulkVendorProductColumns[j].format;
                                    cleansedColumns[i].source = bulkVendorProductColumns[j].source;
                                    cleansedColumns[i].dateFormat = bulkVendorProductColumns[j].dateFormat;


                                }
                            }
                        }
                        var fileObject = $("#bulk-vendor-product-file");
                        var file = ''
                        if (fileObject) {
                            file = fileObject[0].files[0];
                        }

                            var formData = new FormData();
                            formData.append("type", 1);
                            formData.append("value", '');
                            formData.append("pageNumber", 1);
                            formData.append("pageSize", 100);
                            formData.append("isBulkPricing",isBulkPricing);
                            formData.append("file", file);
                            $.ajax({
                                type: 'POST',
                                url: getStagingMetadataId,
                                cache:false,
                                data: formData,
                               // async: false,
                                contentType: false,
                                processData: false,
                                dataType:"json",
                                beforeSend: function () {
                                    SGloading('Loading');
                                },
                                success: function (response) {
                                    //var bulkVendorProductColumns = '[{"title":"P_LocalSku","data":"P_LocalSku","type":"text"},{"title":"VP_VendorPartNumber","data":"VP_VendorPartNumber","type":"text"},{"title":"V_Vendor","data":"V_Vendor","type":"text"},{"title":"VP_SetToPrimary","data":"VP_SetToPrimary","type":"dropdown","source":["Yes","No"]},{"title":"VP_IsDisabled","data":"VP_IsDisabled","type":"dropdown","source":["Yes","No"]},{"title":"VP_Name","data":"VP_Name","type":"text"},{"title":"VP_Description","data":"VP_Description","type":"text"},{"title":"VP_OrderBy","data":"VP_OrderBy","type":"dropdown","source":["L","P","Q"]},{"title":"P_Margin","data":"P_Margin","type":"numeric","format":"0,0.0000%"},{"title":"P_Discount","data":"P_Discount","type":"numeric","format":"$0,0.0000"},{"title":"P_SellingPrice","data":"P_SellingPrice","type":"numeric","format":"$0,0.0000"},{"title":"P_SalePriceMargin","data":"P_SalePriceMargin","type":"numeric","format":"0,0.0000%"},{"title":"P_SalePriceDiscount","data":"P_SalePriceDiscount","type":"numeric","format":"$0,0.0000"},{"title":"VP_Cost","data":"VP_Cost","type":"numeric","format":"$0,0.0000"},{"title":"VP_QuantityPerLot","data":"VP_QuantityPerLot","type":"text"},{"title":"VP_CostPerLot","data":"VP_CostPerLot","type":"text"},{"title":"VP_MinimumOrderUnit","data":"VP_MinimumOrderUnit","type":"text"},{"title":"VP_MinimumOrderQuantity","data":"VP_MinimumOrderQuantity","type":"text"},{"title":"VP_DropShip","data":"VP_DropShip","type":"text"},{"title":"VP_SPQ","data":"VP_SPQ","type":"text"},{"title":"VP_UnitOfMeasure","data":"VP_UnitOfMeasure","type":"text"},{"title":"VP_LotDescription","data":"VP_LotDescription","type":"text"},{"title":"VP_LotPartNumber","data":"VP_LotPartNumber","type":"text"},{"title":"P_Quantity1","data":"P_Quantity1","type":"numeric","format":"0"},{"title":"P_Margin1","data":"P_Margin1","type":"numeric","format":"0,0.0000%"},{"title":"P_Discount1","data":"P_Discount1","type":"numeric","format":"$0,0.0000"},{"title":"P_Quantity2","data":"P_Quantity2","type":"numeric","format":"0"},{"title":"P_Margin2","data":"P_Margin2","type":"numeric","format":"0,0.0000%"},{"title":"P_Discount2","data":"P_Discount2","type":"numeric","format":"$0,0.0000"},{"title":"P_Quantity3","data":"P_Quantity3","type":"numeric","format":"0"},{"title":"P_Margin3","data":"P_Margin3","type":"numeric","format":"0,0.0000%"},{"title":"P_Discount3","data":"P_Discount3","type":"numeric","format":"$0,0.0000"},{"title":"VP_LotBarcode","data":"VP_LotBarcode","type":"text"},{"title":"VP_ManufacturersBarcode","data":"VP_ManufacturersBarcode","type":"text"},{"title":"VP_DateLastCostUpdate","data":"VP_DateLastCostUpdate","type":"text"},{"title":"VP_LeadTime","data":"VP_LeadTime","type":"text"},{"title":"VP_OnOrder","data":"VP_OnOrder","type":"text"},{"title":"VP_QOH","data":"VP_QOH","type":"text"},{"title":"VP_QOHUpdatedOn","data":"VP_QOHUpdatedOn","type":"text"},{"title":"P_OrderBy","data":"P_OrderBy","type":"text"},{"title":"VP_Quantity1","data":"VP_Quantity1","type":"numeric","format":"0"},{"title":"VP_Margin1","data":"VP_Margin1","type":"numeric","format":"0,0.0000%"},{"title":"VP_Discount1","data":"VP_Discount1","type":"numeric","format":"$0,0.0000"},{"title":"VP_Quantity2","data":"VP_Quantity2","type":"numeric","format":"0"},{"title":"VP_Margin2","data":"VP_Margin2","type":"numeric","format":"0,0.0000%"},{"title":"VP_Discount2","data":"VP_Discount2","type":"numeric","format":"$0,0.0000"},{"title":"VP_Quantity3","data":"VP_Quantity3","type":"numeric","format":"0"},{"title":"VP_Margin3","data":"VP_Margin3","type":"numeric","format":"0,0.0000%"},{"title":"VP_Discount3","data":"VP_Discount3","type":"numeric","format":"$0,0.0000"}]';
                                 
                                    stagingMetadataId = response.data.MetadataId;
                                    recordCount = response.data.RecordCount;
                                    if (recordCount === 0)
                                    {
                                        alertmsg('red', 'No data to load');
                                        
                                        return;
                                    }
                                    columns1 = [];
                                   
                                    //cleansedData = performAutoCalculation(getDataForBulkVendorProduct(), cleansedColumns || bulkVendorProductColumns);
                                    cleansedData = getDataForBulkVendorProduct();
                                    var settings = {
                                            data: cleansedData,
                                            colHeaders: true,
                                            rowHeaders: true,
                                            hiddenColumns: {
                                                //columns: [0],
                                                indicators: true
                                            },
                                            //stretchH: 'all',
                                            columnSorting: false,
                                            search: true,
                                            fixedRowsTop: 0,
                                            fixedColumnsLeft: 2,//3 SKYG-819
                                        contextMenu: contextMenu,//contextMenuBulkVendorProduct,
                                        licenseKey: 'non-commercial-and-evaluation',
                                            columns: cleansedColumns || bulkVendorProductColumns,// || cleansedColumnsWithSettings, //cleansedColumnsWithSettings, //cleansedColumns,
                                        beforeRemoveRow: function (index, amount) {
                                            //console.log('beforeRemove: index: %d, amount: %d', index, amount);
                                            for (var r = index; r < index + amount; r++) {
                                                stagingIdsLstToDelete.push(hot.getSettings().data[r].StagingId)
                                            }

                                            if (stagingIdsLstToDelete.length > 0) {
                                                concatenatedStagingIds = stagingIdsLstToDelete.join();
                                                $.ajax({
                                                    type: "POST",
                                                    url: "DeleteRowsFromStaging",
                                                    data: { 'MetadataId': stagingMetadataId, "Ids": concatenatedStagingIds },
                                                    beforeSend: function () {
                                                        SGloading("Deleting");
                                                    },
                                                    success: function () {
                                                        SGloadingRemove();

                                                    }


                                                })
                                            }

                                        },
                                        minSpareRows: 0,
                                            manualColumnResize: true,
                                            //manualColumnMove: true,
                                            skipEmptyLines: true,
                                            afterChange: afterChange,
                                            afterGetColHeader: afterGetColHeader,
                                            fillHandle: true,
                                            formulas: true
                                        };

                                        hot = new Handsontable(container, settings);

                                       //var clnIndex = settings.columns.findIndex(x => x.data === "Sale Price");

                                       // disabledSalePrice.forEach(y => {
                                       //     if (y.isSalePriceDisabled) {
                                       //         hot.setCellMeta(y.rowIndex, clnIndex, 'readOnly', true);
                                       //     }
                                       //     else {
                                       //         hot.setCellMeta(y.rowIndex, clnIndex, 'readOnly', false);
                                       //     }
                                       // });


                                       // if (cleansedData.length > 1 && hot.isEmptyRow(cleansedData.length - 1)) {
                                       //     hot.alter('remove_row', parseInt(cleansedData.length - 1));
                                       // }
                                       // salePriceErrorArr.forEach(y => {
                                       //     if (y.salePriceError) {
                                       //         if (hot.getSettings().data.length > 0) {
                                       //             hot.alter('remove_row', y.rowIndex, salePriceErrorArr.length);
                                       //         }
                                       //     }
                                       // });

                                        hot.render();
                                        addDynamicPaginationToBulkVendorProductGrid();
                                    
                                  
                            },
                            complete: function () {
                                SGloadingRemove();
                                $('#search_field').removeClass('hidden');
                            }
                        })




                        //var option = orderByChangeOptions();
                        //for (var rowIndex = 0; rowIndex < hot.getSettings().data.length; rowIndex++) {
                        //    orderByChange(rowIndex, option);
                        //}

                        ////hot.validateCells();
                        //TblpgntBtm1();



                    },
                    error: function (xhr, status, error) {
                        SGloadingRemove();
                        alert(status + ' ' + error);
                    }
                });
        }



    });

var activateReport = function (metadataId) {
    var link = document.getElementById("hiddenMetadataId");
    debugger;
    if (link == null) {
        link = document.createElement("input");
        link.setAttribute("id", "hiddenMetadataId");
        link.setAttribute("type", "hidden");
        document.body.appendChild(link);
    }
    link.setAttribute("value", metadataId);
    $("#btn-report").removeClass("hidden");
    $("#btn-report").tooltip('show');
};

var activateErrorReport = function (errorMetadataId) {
    var Errorlink = document.getElementById("hiddenErrorMetadataId");
    debugger;
    if (Errorlink == null) {
        Errorlink = document.createElement("input");
        Errorlink.setAttribute("id", "hiddenErrorMetadataId");
        Errorlink.setAttribute("type", "hidden");
        document.body.appendChild(Errorlink);
    }
    Errorlink.setAttribute("value", errorMetadataId);
    $("#btn-Errreport").removeClass("hidden");
    $("#btn-Errreport").tooltip('show');
};



$("#btn-save")
    .click(function () {

        SGloading('Loading');
        var myDataOriginal = null;
        var myDataPricingLevel = null;
        var file = null;
        var boolValue = true;

        //if data is loaded to grid, get from grid
        if (hot) {
            $("#hot-bulk-vendor-product tbody tr").find("td").each(function () { //get all rows in table
                var ratingTd = $(this).attr('class');//Refers to TD element
                var value = $(this).closest('table').find('th').eq($(this).index()).text();
                if (ratingTd == "htInvalid" && $(this).text() !=='{blank}') {
                    alertmsg("red", "Invalid Value of " + value + "!!!", -1);
                    boolValue = false;
                    SGloadingRemove();
                    return false;
                }
            });
            if (boolValue) {
                //get data
                var originalData = hot.getSettings().data; //.slice(0,2);


                //get columns array (alias)
                var columnsXml = [];
                columnsXml.push({ title: "StagingId", data: "StagingId" });

                $.grep(hot.getSettings().columns,
                    function (value) {
                        columnsXml.push({ title: value.title, data: value.data });
                    });


                if ((isBulkPricing == true || isBulkPricing == "True") && isImport === false) {

                    columnsXml.push({ title: "VP_Quantity1", data: "VQuantity1" });
                    columnsXml.push({ title: "VP_Margin1", data: "VMargin1" });
                    columnsXml.push({ title: "VP_SellingPrice1", data: "VPrice1" });
                    columnsXml.push({ title: "VP_Quantity2", data: "VQuantity2" });
                    columnsXml.push({ title: "VP_Margin2", data: "VMargin2" });
                    columnsXml.push({ title: "VP_SellingPrice2", data: "VPrice2" });
                    columnsXml.push({ title: "VP_Quantity3", data: "VQuantity3" });
                    columnsXml.push({ title: "VP_Margin3", data: "VMargin3" });
                    columnsXml.push({ title: "VP_SellingPrice3", data: "VPrice3" });
                    columnsXml.push({ title: "VP_Quantity4", data: "VQuantity4" });
                    columnsXml.push({ title: "VP_Margin4", data: "VMargin4" });
                    columnsXml.push({ title: "VP_SellingPrice4", data: "VPrice4" });
                    columnsXml.push({ title: "VP_IsDisabled", data: "IsDisabled" });
                    columnsXml.push({ title: "VP_SetToPrimary", data: "SetToPrimary" });
                }
                else {
                    if (showPurchasingLevel !== true) {

                        columnsXml.push({ title: "VMargin Level 1", data: "VMargin1" });
                        columnsXml.push({ title: "VMargin Level 2", data: "VMargin2" });
                        columnsXml.push({ title: "VMargin Level 3", data: "VMargin3" });
                        columnsXml.push({ title: "VMargin Level 4", data: "VMargin4" });
                    }
                }

                //genrate xml
                var myData = originalData.selectBulkVendorProductXml(columnsXml, "Product", "UDHBulkUpdate");

                myDataOriginal = myData.dataOriginal; // JSON.stringify(myData.dataOriginal).replace(/\"/,'"');
                myDataPricingLevel = myData.pricingLevelData; //JSON.stringify(myData.pricingLevelData);

                console.log(myDataOriginal);
                var stagingId = getQueryStringByName("stagingId");
                var guid = getQueryStringByName("guid");
                var logUserUploadId = getQueryStringByName("logUserUploadId");
                var errorType = getQueryStringByName("errorType");

                $.ajax({
                    type: "POST",
                    url: urlBulkStagingUpdate,
                    data: {
                        'BulkVendorProductXml': myDataOriginal, 'MetaDataId': stagingMetadataId},
                    beforeSend: function () {
                        SGloading("Loading");
                    },
                    async: false,
                    success: function (response) {
                        return;

                    },
                    complete: function () {
                        $("#btn-save").removeClass("non-editable");
                        SGloadingRemove();
                    },
                    error: function (xhr, status, error) {
                        //reset variables here
                        $("#btn-save").removeClass("non-editable");
                        alertmsg("red", msgErrorOccured + "!!!", -1);
                    }
                })


                SGloadingRemove();

                //---------------validation start-------------------
                //check for instance of handsontable
                $.ajax({
                    type: "POST",
                    url: urlBulkUpdate,
                    cache: false,
                    //contentType: 'application/json; charset=utf-8',
                    data: {
                        "MetaDataId": stagingMetadataId, 'stagingId': stagingId,
                        'guid': guid,
                        'logUserUploadId': logUserUploadId,
                        'errorType': errorType  },
                    async: true,
                    // contentType: false,
                    // processData: false,
                    //dataType: "json",
                    beforeSend: function () {
                        //disable save button
                        $("#btn-save").addClass("non-editable");
                        //if warning still appears, hide before saving
                        $(".sg-alert-wrapper").hide();
                        //show loading
                        SGloading("Loading");
                    },
                    success: function (response) {
                        var showErrorReportBtn = false;
                        if (response) {
                            if (response.Status === true) {
                                if (response.Message.indexOf('Error Records:0') > -1) {
                                    if (response.Message.indexOf('Warning Records: 0') > -1) {
                                        alertmsg("green", response.Message, -1);
                                    }
                                    else { alertmsg("yellow", response.Message, -1); }
                                }



                                else {
                                    //showErrorReportBtn = true;
                                    alertmsg("red", response.Message, -1);
                                }

                                if (response.Data) {
                                    var errorMetadataId = response.Data;
                                    activateReport(stagingMetadataId);
                                    if (errorMetadataId !== "0" && errorMetadataId !== stagingMetadataId)
                                        activateErrorReport(errorMetadataId);
                                }
                            } else {
                                alertmsg("red", response.Message, -1);
                            }
                        } else {
                            alertmsg("red", msgErrorOccured, -1);
                        }
                    },
                    complete: function () {
                        $("#btn-save").removeClass("non-editable");
                        SGloadingRemove();
                    },
                    error: function (xhr, status, error) {
                        //reset variables here
                        $("#btn-save").removeClass("non-editable");
                        alertmsg("red", msgErrorOccured + "!!!", -1);
                        SGloadingRemove();
                    }
                });
                //console.log(myData);
            }
        }
    });

$("#btn-report")
    .click(function (e) {
        var link = document.getElementById("hiddenMetadataId");
        if (!link) {
            alertmsg("red", "No report found", -1);
            return;
        }
        if (!link.value) {
            alertmsg("red", "No report found", -1);
            return;
        }
        var url = urlBulkVendorProductReport + "/" + link.value;
        //if (e.ctrlKey) {
        window.open(url, "_blank");
        //} else {
        //    window.location = url;
        //}
        $(this).tooltip('hide');
    });


$("#btn-Errreport")
    .click(function (e) {
        var link = document.getElementById("hiddenErrorMetadataId");
        if (!link) {
            alertmsg("red", "No report found", -1);
            return;
        }
        if (!link.value) {
            alertmsg("red", "No report found", -1);
            return;
        }
        var url = urlBulkUpdate + "?ErrorMetaDataId=" + link.value;
        //if (e.ctrlKey) {
        window.open(url, "_blank");
        //} else {
        //    window.location = url;
        //}
        $(this).tooltip('hide');
    });



//---------------------For selecting all cells on clicking top-left header cell start-------------------
/**
 * @param {MouseEvent} event
 * @param {WalkontableCellCoords} coords
 * @param {Element} element
 */
var handleHotAfterOnCellMouseDown = function (event, coords, element) {

    //if (coords.row < 0) {
    //    event.stopImmediatePropagation();
    //}
    if (element && element.tagName) {
        if (element.tagName == "TH" && coords.row == 0 && coords.col == 0) {
            if (hot) {
                hot.selectCell(0, 0, hot.countRows() - 1, hot.countCols() - 1);
            }
        }
    }
};

Handsontable.hooks.add('afterOnCellMouseDown',
    handleHotAfterOnCellMouseDown, hot);
//---------------------For selecting all cells on clicking top-left header cell end-------------------


$("#exportBulkPricingAll").click(function () {
    //if (dataFrom !== 0 && dataId !== 0) {
    dataFrom = 1
    if (isBulkPricing == "True" || window.location.href.includes('isBulkPricing=true') || isBulkPricing == true)
        isBulkPricing = true;
    else
        isBulkPricing = false;
    if (hot && hot.getSettings().data.length > 0) {
        var urlexportBulkData1 = urlexportBulkStagingData + "?MetaDataId=" + stagingMetadataId + "&isBulkPricing=" + isBulkPricing
        window.location = urlexportBulkData1;
    } else {
        alertmsg("red", "No data to export", -1);
    }
    //}

});

function cleanColumnForBulkPricing(dataDownloadTemplate) {

    var dataDownloadTemplateColumns = dataDownloadTemplate.split(',');
    var columnstoRemove = 'P_Discount1,P_Discount2,P_Discount3,VP_LotBarcode,VP_ManufacturersBarcode", "VP_DateLastCostUpdate,VP_LeadTime,VP_OnOrder,VP_QOH,VP_QOHUpdatedOn,VP_Quantity1,VP_Margin1,VP_Discount1,VP_Quantity2,VP_Margin2,VP_Discount2,VP_Quantity3,VP_Margin3,VP_Discount3';
    for (i = 0; i < dataDownloadTemplateColumns.length; i++) {
        if (columnstoRemove.includes(dataDownloadTemplateColumns[i] + ',') || columnstoRemove.includes(',' + dataDownloadTemplateColumns[i])) {
            //dataDownloadTemplateColumns.replace(dataDownloadTemplateColumns[i],'');
            dataDownloadTemplateColumns[i] = '';
        }
    }
    dataDownloadTemplateColumns = dataDownloadTemplateColumns.toString();
    dataDownloadTemplateColumns = dataDownloadTemplateColumns.replace(',,', ',');
    return dataDownloadTemplateColumns;


}

function savePaginateData(reLoadData) {
    SGloading('Loading');
    var myDataOriginal = null;
    var myDataPricingLevel = null;
    var file = null;


    //if data is loaded to grid, get from grid
    if (hot) {

        //get data
        var originalData = hot.getSettings().data; //.slice(0,2);

        //get columns array (alias)
        var columnsXml = [];
        columnsXml.push({ title: "StagingId", data: "StagingId" });
        $.grep(hot.getSettings().columns,
            function (value) {
                columnsXml.push({ title: value.title, data: value.data });
            });


        if ((isBulkPricing == true || isBulkPricing == "True") && isImport === false) {

            columnsXml.push({ title: "VP_Quantity1", data: "VQuantity1" });
            columnsXml.push({ title: "VP_Margin1", data: "VMargin1" });
            columnsXml.push({ title: "VP_SellingPrice1", data: "VPrice1" });
            columnsXml.push({ title: "VP_Quantity2", data: "VQuantity2" });
            columnsXml.push({ title: "VP_Margin2", data: "VMargin2" });
            columnsXml.push({ title: "VP_SellingPrice2", data: "VPrice2" });
            columnsXml.push({ title: "VP_Quantity3", data: "VQuantity3" });
            columnsXml.push({ title: "VP_Margin3", data: "VMargin3" });
            columnsXml.push({ title: "VP_SellingPrice3", data: "VPrice3" });
            columnsXml.push({ title: "VP_Quantity4", data: "VQuantity4" });
            columnsXml.push({ title: "VP_Margin4", data: "VMargin4" });
            columnsXml.push({ title: "VP_SellingPrice4", data: "VPrice4" });
            columnsXml.push({ title: "VP_IsDisabled", data: "IsDisabled" });
            columnsXml.push({ title: "VP_SetToPrimary", data: "SetToPrimary" });
        }
        else {
            if (showPurchasingLevel !== true) {

                columnsXml.push({ title: "VMargin Level 1", data: "VMargin1" });
                columnsXml.push({ title: "VMargin Level 2", data: "VMargin2" });
                columnsXml.push({ title: "VMargin Level 3", data: "VMargin3" });
                columnsXml.push({ title: "VMargin Level 4", data: "VMargin4" });
            }
        }

        //genrate xml
        var myData = originalData.selectBulkVendorProductXml(columnsXml, "Product", "UDHBulkUpdate");

        myDataOriginal = myData.dataOriginal; // JSON.stringify(myData.dataOriginal).replace(/\"/,'"');
        myDataPricingLevel = myData.pricingLevelData; //JSON.stringify(myData.pricingLevelData);

    }
    setTimeout(function () {
        $.ajax({
            type: "POST",
            url: urlBulkStagingUpdate,
            data: { 'BulkVendorProductXml': myDataOriginal, 'MetaDataId': stagingMetadataId },
            beforeSend: function () {
                SGloading("Loading");
            },
            async: false,
            success: function (response) {
                if (reLoadData === false) { return; }
                if (response) {
                    if (response.Status === true) {
                        hot.loadData(getDataForBulkVendorProduct());
                        if ($('#search_field').val() !== '') {
                            var queryResult = hot.search.query($('#search_field').val());
                        }
                        hot.render();
                    }
                    else {
                        alertmsg("red", response.Message, -1);
                    }


                }
            },
            complete: function () {
                $("#btn-save").removeClass("non-editable");
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                //reset variables here
                $("#btn-save").removeClass("non-editable");
                alertmsg("red", msgErrorOccured + "!!!", -1);
            }
        })
    }, 1000)




}

//$('#search_field').change(function () {
//    //alert('out');
//    if (hot === undefined)
//        return;
//    hot.search.query(this.value);
//    parent.location.hash = "#page-1";
//    getTotalCountFromStaging();
//    addDynamicPaginationToBulkVendorProductGrid();
//    savePaginateData();
//})

$('#search_field').on('keyup', function (e) {
    search = true;
    if (e.keyCode == 13) {
        //alert('out');
        if (hot === undefined)
            return;
        hot.search.query(this.value);
        parent.location.hash = "#page-1";
        getTotalCountFromStaging();
        addDynamicPaginationToBulkVendorProductGrid();
        savePaginateData();
    }
})

$('#SearchText').on('focus', function () {
    if (hot === undefined)
        return;
    if (search == true) {
        hot.search.query(this.value);
        parent.location.hash = "#page-1";
        getTotalCountFromStaging();
        addDynamicPaginationToBulkVendorProductGrid();
        savePaginateData();
        search = false;
    }

})


function getTotalCountFromStaging() {
    var isErrorLoad = false;
    if (ErrorbulkData !== undefined && ErrorbulkData !== "") {
        isErrorLoad = true;
    }
    $.ajax({
        type: 'POST',
        url: getBulkDataFromStaging,
        async: false,
        data: {
            'metadataId': stagingMetadataId,
            'pageNumber': 1,
            'pageSize': pageLimit || 100,
            'isBulkPricing': 'false',
            'pagination': 'false',
            'keySearch': $('#search_field').val(),
            'countOnly': 1,
            'isErrorLoad': isErrorLoad
        },
        beforeSend: function () {

            SGloading('Loading');
        },
        success: function (response) {

            recordCount = response.count;
        },
        complete: function () {

            SGloadingRemove();
        },
        error: function (xhr, status, error) {

            alert(status + ' ' + error);
        }


    });

}

function getVendorMinLineOrder(vendorName) {

    for (var i = 0 ; i < vendorMinOrderLineArry.length; i++)
    {
        if (vendorMinOrderLineArry[i].Name === vendorName)
        {
            return vendorMinOrderLineArry[i].MinLineOrderAmount;
        }
    }
    return "";
}

$(window).bind("load", function () {
    if (!isEmptyOrSpaces(errorMetadataId)) {
        $('#search_field').removeClass('hidden');
        stagingMetadataId = errorMetadataId;
        getBulkErrorDataFromStaging(errorMetadataId);
        getTotalCountFromStaging(errorMetadataId);
        addDynamicPaginationToBulkVendorProductGrid();
    }

    if (!isEmptyOrSpaces(CatalogSearchAllVendorProducts)) {
        $('#search_field').removeClass('hidden');
        dataFrom = 1; /// product 
        dataId = CatalogSearchAllVendorProducts;
        //   getPaginatedGrid(dataFrom, dataId);
        var totalNumber = CatalogSearchAllVendorProducts.split(",").length;
        debugger;
        getBulkVendorProductData(dataFrom, 'test', 1, totalNumber, isBulkPricing);
    }
    if (productIdList !== "") {
        $('#search_field').removeClass('hidden');
        dataFrom = 1; /// product 
        dataId = productIdList;
        //   getPaginatedGrid(dataFrom, dataId);
        var totalNumber = productIdList.split(",").length;
        debugger;
        getBulkVendorProductData(dataFrom, dataId, 1, totalNumber, isBulkPricing);
    }


});

