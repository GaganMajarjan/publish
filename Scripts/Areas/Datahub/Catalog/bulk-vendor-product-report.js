﻿$(document)
    .ready(function() {

        $("#tbl-report").removeClass("hidden");
        var urlSplitBySlash = window.location.href.split("/");
        var id = urlSplitBySlash[urlSplitBySlash.length - 1];

        $('#tbl-report-server')
            .dataTable({
                "deferRender": true,
                "bServerSide": true,
                "bSort": true,
                "stateSave": false,
                "bFilter": true,
                "aLengthMenu": dataTableMenuLength, //Global Variable in common.js
                "iDisplayLength": dataTableDisplayLength, //Global Variable in common.js
                "sAjaxSource": urlBulkVendorProductReportAjax, //Get server side data from this Url
                "fnServerParams": function(aoData) {
                    aoData.push({ "name": "id", "value": id });
                },
                "bProcessing": true,
                "scrollX": true,
                "fnPreDrawCallback": function () {
                    // gather info to compose a message
                    var ob = this;
                    SGtableloading({ class: faIcon, message: 'Processing...', objct: ob });
                },
                "fnDrawCallback": function (settings) {
                    SGtableloadingRemove(divid);
                    TblpgntBtm();
                    rebuilttooltip();
                }
            });

        //$('#tbl-report-server')
        //    .DataTable({
        //        "aLengthMenu": dataTableMenuLength,
        //        "iDisplayLength": 20,
        //        "scrollX": true,
        //        "sPaginationType": "full_numbers",
        //        "processing": true,
        //        "serverSide": true,
        //        "bDestroy": true,
        //        "bJQueryUI": true,
        //        "sAjaxSource": urlBulkVendorProductReport,
        //        "fnServerParams": function (aoData) {
        //            aoData.push({ "name": "id", "value": id });
        //        },
        //        //"ajax": {
        //        //    "type": "POST",
        //        //    "url": urlBulkVendorProductReport,
        //        //    "data": function (data) {
        //        //        $.extend(data, additionalData);
        //        //    }
        //        //},
        //        "columns": {},
        //        //dom: 'Bfrtip',
        //        //buttons: [
        //        //    'copy', 'excel', 'pdf'
        //        //]
        //        "dom": 'T<"clear">lfrtip',
        //        "tableTools": {
        //            "sSwfPath": tableToolsPath,
        //            "aButtons": tableToolsOptions
        //        }
        //    });

        $("#in-progress").addClass("hidden");

        //$('#table1').DataTable().ajax.reload();

    });
