﻿
var itemPointsGroup = [];
itemPointsGroup = ["ItemPoints", "ShippingHeight", "ShippingLength", "ShippingWidth"];

var HazmatGroup = [];
HazmatGroup = ["UN Number", "UN Class", "Packing Group", "Hazmat Type"];

var yahooIdGroup = [];
yahooIdGroup = ["YahooId", "product-url", "Image Url","PlatformQualifier"];//, "Mobile Link"

var productIdList = getQueryStringByName("productIdList");
var hot;
var container = document.getElementById('hot-custom-field-update');
var continueToComplete = true;

var fileData;
var filetypeFlag = false;
var columns1 = [];
var customUpdateNotWorking1stShot = 1;
var previousIndex = [];
var disabledProducts = [];
var optionsList = [];
var platformOptionList = [];

customFieldForMapping = Encoder.htmlDecode(customFieldsJson);
customFieldForMapping = JSON.parse(customFieldForMapping);
var unitedNationsNumberId = GetCustomFieldId('UN Number');
var unitedNationsClassId = GetCustomFieldId('UN Class');
var packingGroupId = GetCustomFieldId('Packing Group');
var hazmatTypeId = GetCustomFieldId('Hazmat Type');
var ItemPointId = GetCustomFieldId('ItemPoints');
var heightId = GetCustomFieldId('ShippingHeight');
var lengthId = GetCustomFieldId('ShippingLength');
var widthId = GetCustomFieldId('ShippingWidth');
var yahooId = GetCustomFieldId('YahooId');
var product_url = GetCustomFieldId('product-url');
var isDisable = GetCustomFieldId('Disable');
//var mobileLink = GetCustomFieldId('MobileLink');
var imageUrl = GetCustomFieldId('Image URL');
var platformQualifier = GetCustomFieldId('PlatformQualifier');
//var identifier_exist = GetCustomFieldId('IdentifierExists');
var arrRequiredCustomField = [];

var stepped = 0, chunks = 0, rows = 0;
var start, end;
var parser;
var pauseChecked = false;
var printStepChecked = false;
var header = [];
var numberOfColumns = 0;
var clientsideData;

var flag = 0;
var customFieldIdLst = [];

var changeLevelArray = new Array();
var changeLevelArrayPerLot = new Array();
var udhXml = '';

var unGroup = ["UN Number", "UN Class", "Packing Group", "Hazmat Type"];
var shippingGroup = ["ItemPoints", "shippinglength", "shippingheight", "shippingwidth"];
var YahooGroup = ["Disable", "PlatformQualifier", "Image URL", "product-url", "YahooId"];

arrRequiredCustomField.push(unGroup, shippingGroup, YahooGroup);



function buildConfig() {
    return {
        delimiter: ",",
        newline: "",
        header: true,
        dynamicTyping: "",
        preview: parseInt(0),
        step: undefined,
        encoding: "",
        worker: "",
        comments: "",
        complete: completeFn,
        error: errorFn,
        download: "",
        fastMode: "",
        skipEmptyLines: true,
        chunk: undefined,
        beforeFirstChunk: undefined
    };

    function getLineEnding() {

        if ($('#newline-n').is(':checked'))
            return "\n";
        else if ($('#newline-r').is(':checked'))
            return "\r";
        else if ($('#newline-rn').is(':checked'))
            return "\r\n";
        else
            return "";
    }
}

function stepFn(results, parserHandle) {
    stepped++;
    rows += results.data.length;

    parser = parserHandle;

    if (pauseChecked) {
        //console.log(results, results.data[0]);
        parserHandle.pause();
        return;
    }

    if (printStepChecked) {
    }
    //console.log(results, results.data[0]);
}

function chunkFn(results, streamer, file) {
    if (!results)
        return;
    chunks++;
    rows += results.data.length;

    parser = streamer;

    if (printStepChecked)
        //console.log("Chunk data:", results.data.length, results);

        if (pauseChecked) {
            //console.log("Pausing; " + results.data.length + " rows in chunk; file:", file);
            streamer.pause();
            return;
        }
}

function errorFn(error, file) {
    console.log('errorFn');
    //console.log("ERROR:", error, file);
}

function completeFn() {
    end = performance.now();

    //clear file
    $('#file').val('');

    if (!continueToComplete)
        return false;

    if (!$('#stream').prop('checked') && !$('#chunk').prop('checked') && arguments[0] && arguments[0].data)
        rows = arguments[0].data.length;
    var results = arguments[0];
    if (results.errors.length > 0) {
        var errorMessage = "";
        for (var i = 0; i < results.errors.length; i++) {
            //console.log(results.errors[i]);
            errorMessage += "{0} (Row#{1})".format(results.errors[i].message, results.errors[i].row) + "; ";
        }
        alertmsg('red', errorMessage, -1);
        return;
    }

    var headerJson = arguments[0].meta.fields;
    // //console.log(headerJson);
    fileData = [];
    clientsideData = [];
    clientsideData = arguments;
    fileData = arguments[0].data;

    $.each(headerJson,
        function (key, value) {
            header.push(value);
            //columns1.push({ 'title': value, 'data': value, 'type': 'text', 'renderer': 'html' });
            columns1.push({ 'title': value, 'data': value, 'type': 'text' });
        });

        $.grep(headerJson,
            function (valueHeader, colIndex) {
                $.grep(fileData,
                    function (valueData, rowIndex) {
                        if (fileData[rowIndex][valueHeader])
                            //fileData[rowIndex][valueHeader] = decodeHtml(fileData[rowIndex][valueHeader], true);

                            fileData[rowIndex][valueHeader] = decodeHtmlEntities(fileData[rowIndex][valueHeader]).replace(/\n/g, "").replace(/\r/g, "").replace(/\t/g, "");
                    });
            });



    var contextMenu = {
        callback: function (key, options) {
            var length = options.length - 1;
            var row = options[length].start.row;
            var column = options[length].start.col;

            if (hot.getSettings().columns[column].readOnly === true)
                return;



            var textToCopy = hot.getDataAtCell(row, column);

           if (key === 'remove_col1') {

                setTimeout(function () {
                    var cols = hot.getSettings().columns;

                    var columns = column;
                    var removeIdx = customFieldIdLst.findIndex(x => x == cols[columns].id);
                    if (column == 0) {
                        alertmsg('red', 'Cannot remove column LocalSku');
                        return;
                    }

                    var boolean = false;
                    //cols.pop();

                    //hot.updateSettings({
                    //    columns: cols
                    //});
                    var index = arrRequiredCustomField.findIndex(row => row.includes(cols[columns].title));
                    if (index >= 0) {

                        var exists = cols.some(y => arrRequiredCustomField[index].includes(y.title));
                        if (!exists) {
                            boolean = true;
                        }
                    }
                    else {
                        boolean = true;
                    }
                    if (boolean) {
                        $('#CustomFields').val(null).trigger("change");
                        customFieldIdLst.splice(removeIdx, 1);
                        cols.splice(columns, 1);
                        //cols.pop();
                        hot.updateSettings({
                            columns: cols
                        });
                    }
                    hot.render();
                }, 100);
            }

            else if (key === "remove_row") {
                if (stagingIdsLstToDelete.length > 0) {
                    concatenatedStagingIds = stagingIdsLstToDelete.join();
                    $.ajax({
                        type: "POST",
                        url: "DeleteRowsFromStaging",
                        data: { 'MetadataId': stagingMetadataId, "Ids": concatenatedStagingIds },
                        beforeSend: function () {
                            SGloading("Deleting");
                        },
                        success: function () {
                            SGloadingRemove();

                        }


                    })
                }

                setTimeout(KitCostDisabled, 500);
            }
        },
        items: {
            'row_above': {},
            'row_below': {},
            'remove_row': {},
            'hsep1': "---------",
            'undo': {},
            'redo': {},
            'hsep3': "---------",
            //'make_read_only': {},
            'alignment': {},
            'remove_col1': { name: 'Remove Column' }
        }
    }

};

var getHandsontableTypeFromCustomFieldDataType = function (customFieldDataType, allowedvalues) {
    //console.log(allowedvalues);
    if (allowedvalues) {
        switch (allowedvalues.toLowerCase()) {
            case '"no","yes"':
                return "dropdown"; //'source': '["Yes","No"]'
                break;
            default:
                break;
        }
    }

    if (customFieldDataType) {
        //68310,68311,68312,68314 -- int, float, varchar(max), bit
        switch (customFieldDataType.toLowerCase()) {
            case "int":
                return "numeric"; //format 0,0
                break;
            case "float":
                return "numeric"; //format 0,0.0000
                break;
            case "varchar(max)":
                return "text";
                break;
            case "datetime":
                return "date";
                break;
            //case "bit":
            //    return "checkbox";
            //    break;
            default:
                return "text";
                break;
        }
    }

    return "text";

};

$("#file").change(function () {

    $("#pagination").hide();
    stepped = 0;
    chunks = 0;
    rows = 0;
    var txt = "";
    var files = $('#file')[0].files;
    //SG-775
    var ext = getExtension(files[0].name);
    if (ext !== 'csv') {
        alertmsg('red', 'File format not supported. Only CSV file can be accepted.', -1);
        continueToComplete = false;
        return false;
    }
    var config = buildConfig();

    if (files.length > 0) {
        $("#add-custom-field").attr("disabled", true);
        $("#CustomFields").attr("disabled", true);
        //$('#hot-custom-field-update').addClass('non-editable');

        start = performance.now();
        header = [];
        $('#file').parse({
            config: config,
            before: function (file, inputElem) {
                //console.log("Parsing file:", file);
            },

            complete: function () {

                SGloadingRemove();

                var itemPointsColumns = [];
                var commodityCodeColumns = [];
                var reorderPointUpdatedOnColumns = [];
                var targetQuantityUpdatedOnColumns = [];
                var productUrlColumns = [];
                var hazmatTypeColumns = [];
                var FulFillmentCenterColumns = [];
                var TaxCodeColumns = [];

                var cleansedColumns = [];
                $.grep(columns1, function (n, i) {
                    var columnString = JSON.stringify(n).replace(/\=\\\"/g, "")
                        .replace(/\\\"\"/g, "\"")
                        .replace(/\\\"/g, "''")
                        .replace(/ˏ/g, ",")
                        .replace(/\\r/g, "")
                        .replace(/\\n/g, "");

                    //console.log(columnString);

                    var parsedColumns = fixJsonQuote(columnString);
                    ReplaceSingle2Quote(parsedColumns);
                    cleansedColumns.push(parsedColumns);

                    if (parsedColumns.data.toLowerCase() == "itempoints" ||
                        parsedColumns.data.toLowerCase() == "dimensions") {
                        itemPointsColumns.push(parsedColumns.data);
                    }
                    if (parsedColumns.data.toLowerCase() == "commoditycode" ||
                        parsedColumns.data.toLowerCase() == "schedule-b") {
                        commodityCodeColumns.push(parsedColumns.data);
                    }
                    if (parsedColumns.data.toLowerCase() == "reorder point" ||
                        parsedColumns.data.toLowerCase() == "reorder point last updated date") {
                        reorderPointUpdatedOnColumns.push(parsedColumns.data);
                    }
                    if (parsedColumns.data.toLowerCase() == "target quantity" ||
                        parsedColumns.data.toLowerCase() == "target quantity last updated date") {
                        targetQuantityUpdatedOnColumns.push(parsedColumns.data);
                    }
                    if (parsedColumns.data.toLowerCase() == "product-url" ||
                        parsedColumns.data.toLowerCase() == "yahooid") {
                        productUrlColumns.push(parsedColumns.data);
                    }

                    if (parsedColumns.data.toLowerCase() == "un number" ||
                        parsedColumns.data.toLowerCase() == "un class" ||
                        parsedColumns.data.toLowerCase() == "packing group" ||
                        parsedColumns.data.toLowerCase() == "hazmat type") {
                        hazmatTypeColumns.push(parsedColumns.data);
                    }
                    if (parsedColumns.data.toLowerCase() == "fulfillmentcenter") {
                        FulFillmentCenterColumns.push(parsedColumns.data);
                    }
                    if (parsedColumns.data.toLowerCase() == "tax code") {
                        TaxCodeColumns.push(parsedColumns.data);
                    }
                });

                console.log(fileData);
                //fData = fileData;
                var addedCalculatedColumns = [];

                //check for related custom fields that do not occur in pair
                if (itemPointsColumns.length == 1) {
                    //var itemPointsColumnTemplate = '{"title":"{0}","data":"{0}","type":"text"}';
                    //var refColumnItemPoints = relatedCf.matchByAliasAndGetOther(itemPointsColumns[0]);

                    ////add column to grid
                    //for (var i = 0; i < fileData.length; i++) {
                    //    fileData[i][refColumnItemPoints.customFieldAlias] = "";
                    //}

                    //itemPointsColumns.push(refColumnItemPoints.customFieldAlias);
                    //cleansedColumns.push({ 'title': refColumnItemPoints.customFieldAlias, 'data': refColumnItemPoints.customFieldAlias, 'type': 'text' });
                    //addedCalculatedColumns.push(refColumnItemPoints.customFieldAlias);
                }
                if (commodityCodeColumns.length == 1) {
                    var refColumnCommodityCode = relatedCf.matchByAliasAndGetOther(commodityCodeColumns[0]);

                    //add column to grid
                    for (var i = 0; i < fileData.length; i++) {
                        fileData[i][refColumnCommodityCode.customFieldAlias] = "";
                    }

                    commodityCodeColumns.push(refColumnCommodityCode.customFieldAlias);
                    cleansedColumns.push({ 'title': refColumnCommodityCode.customFieldAlias, 'data': refColumnCommodityCode.customFieldAlias, 'type': 'text' });
                    addedCalculatedColumns.push(refColumnCommodityCode.customFieldAlias);

                }
                if (reorderPointUpdatedOnColumns.length == 1) {
                    var refColumnReorderPointsUpdatedOn = relatedCf.matchByAliasAndGetOther(reorderPointUpdatedOnColumns[0]);

                    //add column to grid
                    for (var i = 0; i < fileData.length; i++) {
                        fileData[i][refColumnReorderPointsUpdatedOn.customFieldAlias] = "";
                    }

                    reorderPointUpdatedOnColumns.push(refColumnReorderPointsUpdatedOn.customFieldAlias);
                    cleansedColumns.push({ 'title': refColumnReorderPointsUpdatedOn.customFieldAlias, 'data': refColumnReorderPointsUpdatedOn.customFieldAlias, 'type': 'text' });
                    addedCalculatedColumns.push(refColumnReorderPointsUpdatedOn.customFieldAlias);

                }
                if (targetQuantityUpdatedOnColumns.length == 1) {
                    var refColumnTargetQuantityUpdatedOn = relatedCf.matchByAliasAndGetOther(targetQuantityUpdatedOnColumns[0]);

                    //add column to grid
                    for (var i = 0; i < fileData.length; i++) {
                        fileData[i][refColumnTargetQuantityUpdatedOn.customFieldAlias] = "";
                    }

                    targetQuantityUpdatedOnColumns.push(refColumnTargetQuantityUpdatedOn.customFieldAlias);
                    cleansedColumns.push({ 'title': refColumnTargetQuantityUpdatedOn.customFieldAlias, 'data': refColumnTargetQuantityUpdatedOn.customFieldAlias, 'type': 'text' });
                    addedCalculatedColumns.push(refColumnTargetQuantityUpdatedOn.customFieldAlias);

                }
                if (productUrlColumns.length == 1) {
                    var refColumnProductUrl = relatedCf.matchByAliasAndGetOther(productUrlColumns[0]);

                    //add column to grid
                    for (var i = 0; i < fileData.length; i++) {
                        fileData[i][refColumnProductUrl.customFieldAlias] = "";
                    }

                    productUrlColumns.push(refColumnProductUrl.customFieldAlias);
                    cleansedColumns.push({ 'title': refColumnProductUrl.customFieldAlias, 'data': refColumnProductUrl.customFieldAlias, 'type': 'text' });
                    addedCalculatedColumns.push(refColumnProductUrl.customFieldAlias);

                }

                var cleansedData = fileData;

                if (flag !== 0) {
                    flag = 0;
                    hot.destroy();
                }

                //if (itemPointsColumns.length == 2) {
                //    for (var i = 0; i < cleansedData.length; i++) {
                //        if (!cleansedData[i]["ItemPoints"] &&
                //            cleansedData[i]["dimensions"]) {
                //            var splitDimensions = cleansedData[i]["dimensions"].split(' ');
                //            var calculatedItemPoints = "0";
                //            if (splitDimensions.length == 3) {
                //                calculatedItemPoints = splitDimensions[0] *
                //                    splitDimensions[1] *
                //                    splitDimensions[2];

                //                calculatedItemPoints = parseFloat(calculatedItemPoints) || "0";
                //            }
                //            cleansedData[i]["ItemPoints"] =
                //                calculatedItemPoints.toString();
                //        }
                //    }
                //}

                if (commodityCodeColumns.length == 2) {
                    for (var i = 0; i < cleansedData.length; i++) {
                        if (!cleansedData[i]["CommodityCode"] &&
                            cleansedData[i]["schedule-b"]) {
                            var calculatedCommodityCode = cleansedData[i]["schedule-b"].substring(0, 4);
                            calculatedCommodityCode = calculatedCommodityCode || "";

                            cleansedData[i]["CommodityCode"] =
                                calculatedCommodityCode.toString();
                        }
                    }
                }

                if (reorderPointUpdatedOnColumns.length == 2) {
                    for (var i = 0; i < cleansedData.length; i++) {
                        if (!cleansedData[i]["Reorder Point Last Updated Date"] &&
                            cleansedData[i]["Reorder Point"]) {
                            var calculatedReorderPointUpdatedOn = FormattedDate(new Date());
                            calculatedReorderPointUpdatedOn = calculatedReorderPointUpdatedOn || "";
                            cleansedData[i]["Reorder Point Last Updated Date"] =
                                calculatedReorderPointUpdatedOn.toString();
                        }
                    }
                }

                if (targetQuantityUpdatedOnColumns.length == 2) {
                    for (var i = 0; i < cleansedData.length; i++) {
                        if (!cleansedData[i]["Target Quantity Last Updated Date"] &&
                            cleansedData[i]["Target Quantity"]) {
                            var calculatedTargetQuantityUpdatedOn = FormattedDate(new Date());
                            calculatedTargetQuantityUpdatedOn = calculatedTargetQuantityUpdatedOn || "";
                            cleansedData[i]["Target Quantity Last Updated Date"] =
                                calculatedTargetQuantityUpdatedOn.toString();
                        }
                    }
                }

                if (productUrlColumns.length > 1) {
                    for (var i = 0; i < cleansedData.length; i++) {
                        //if (!cleansedData[i]["product-url"] &&
                        if (cleansedData[i]["YahooId"] && cleansedData[i]["YahooId"] !== "") {
                            var calculatedProductUrl = "https://www.skygeek.com/{0}.html".format(cleansedData[i]["YahooId"]);

                            cleansedData[i]["product-url"] =
                                calculatedProductUrl.toString();

                            //var mobileLinkUrlforLoad = "http://m.skygeek.com/{0}.html".format(cleansedData[i]["YahooId"]);
                            //cleansedData[i]["MobileLink"] =
                            //    mobileLinkUrlforLoad.toString();
                            if (cleansedData[i]["Image URL"] === "") {

                                //feature-SKYG-690
                                //var imageUrlForLoad = "http://feeds3.yourstorewizards.com/4481/images/full/{0}.jpg".format(cleansedData[i]["YahooId"]);
                                var imageUrlForLoad = "https://sep.yimg.com/ty/cdn/stylespilotshop/image_coming_soon.jpg";
                                cleansedData[i]["Image URL"] =
                                    imageUrlForLoad.toString();

                            }
                        }
                    }
                }

                if (hazmatTypeColumns.length == 4) {

                    for (var i = 0; i < cleansedData.length; i++) {

                        var hazmatNumber = cleansedData[i]["UN Number"];
                        var hazmatClass = cleansedData[i]["UN Class"];
                        var hazmatGroup = cleansedData[i]["Packing Group"];

                        if (!cleansedData[i]["Hazmat Type"]) {

                            var hazmatType = "";
                            if (hazmatNumber || hazmatClass || hazmatGroup) {
                                if (hazmatNumber != "") { hazmatType = hazmatType + ' ' + hazmatNumber; }
                                if (hazmatClass != "") { hazmatType = hazmatType + ' ' + hazmatClass; }
                                if (hazmatGroup != "") { hazmatType = hazmatType + ' ' + hazmatGroup; }
                                hazmatType.trimLeft();
                                hazmatType.trimRight();

                                var calculatedHazmatType = hazmatType;

                                cleansedData[i]["Hazmat Type"] = calculatedHazmatType.toString();

                            }
                        }
                    }
                }
                if (FulFillmentCenterColumns.length >= 1 ) {
                    for (var k = 0; k < cleansedData.length; k++) {
                        var fulfillmentCenter = cleansedData[k]["FulfillmentCenter"];
                       // if (fulfillmentCenter === null || fulfillmentCenter === "" || fulfillmentCenter === " " || fulfillmentCenter === "{blank}") {
                            if (fulfillmentCenter === null) {//DaniUpdate1212020

                            cleansedData[k]["FulfillmentCenter"] = ""; 
                        }
                       
                    }
                }
                if (TaxCodeColumns.length >= 1) {
                    for (var t = 0; t < cleansedData.length; t++) {
                       
                        var taxCode = cleansedData[t]["Tax Code"].trim();
                        if (taxCode === null || taxCode === "" || taxCode === " " || taxCode === "{blank}") {
                            cleansedData[t]["Tax Code"] = "";
                        }
                    }
                }

                /*
                cleansedData = $.grep(cleansedData, function (value) {
                    delete value.CostPerLot;
                    delete value.LotDescription;
                    delete value.LotPartNumber;
                    delete value.QuantityPerLot;
                    return value;
                });
                */

                if (addedCalculatedColumns.length > 0) {
                    //if calculated columns have been added, then notify user

                    //$("#tblRelatedCustomFieldInfoModal tbody");
                    $("#info-added-calculated-columns").removeClass("hidden");

                    var message = "";
                    $.each(addedCalculatedColumns, function (i, v) {
                        message += "'" + v + "',";
                    });
                    message = message.substring(0, message.length - 1);

                    message = "Following related fields have been added: " + message;

                    udh.notify.warn(message);

                    //notifyAddedCalculatedColumns(addedCalculatedColumns);
                }


                columns1 = [];
                var settings = {
                    data: cleansedData,
                    colHeaders: true,
                    rowHeaders: true,
                    //stretchH: 'all',
                    columnSorting: false,
                    search: true,
                    fixedRowsTop: 0,
                    fixedColumnsLeft: 1,
                    contextMenu: contextMenuBIU,
                    licenseKey: 'non-commercial-and-evaluation',
                    columns: cleansedColumns,
                    minSpareRows: 0,
                    manualColumnResize: true,
                    manualColumnMove: true,
                    
                    //afterChange: afterChange
                    afterChange: function(changes, source) {
                        if (changes != null) {

                            for (var i = 0; i < changes.length; i++) {
                                if (changes[i] != null) {
                                    // [ [row, prop, oldVal, newVal], ... ].
                                    var rowIndex = changes[i][0];
                                    var prop = changes[i][1];
                                    var newVal = changes[i][3];
                                    var oldVal = changes[i][2];

                                    if (prop.toLocaleLowerCase() == "fulfillmentcenter") {

                                       // if (newVal === "" || newVal === " " || newVal === "{blank}") {
                                        if (newVal === "") {//DaniUpdate1212020
                                            hot.getSettings().data[rowIndex][prop] = "";
                                            hot.render();
                                        }
                                    }
                                    if (prop.toLocaleLowerCase() == "tax code") {

                                        if (newVal === "" || newVal === " " || newVal === "{blank}") {

                                            hot.getSettings().data[rowIndex][prop] = " ";
                                            hot.render();
                                        }
                                    }
                                   
                                    if (prop.toLocaleLowerCase() == "batchlottracking") {

                                        if (newVal === "" || newVal === " ") {

                                            hot.getSettings().data[rowIndex][prop] = "No";
                                            hot.render();
                                            // $('.htInvalid').removeClass('htInvalid');
                                        }
                                        else if (newVal === "{blank}") {
                                            alertmsg("red", "You cannot choose {blank} value on drop-down, please select value of dropdown", -1);
                                            return;
                                        }

                                    }

                                    var decodedVal = decodeHtml(newVal, true);
                                    if (newVal != decodedVal) {
                                        hot.setDataAtRowProp(rowIndex, prop, decodedVal);
                                    }

                                    if (prop.toLowerCase() == "shippinglength" || prop.toLowerCase() == "shippingheight" || prop.toLowerCase() == "shippingwidth") {
                                        autoCalculateItemPoints(changes[i]);
                                    }
                                   
                                    if (prop.toLowerCase() === "disable") {
                                        newVal = (newVal != null && (newVal === 'Yes' || newVal === 'True' || newVal === '1')) ? '1' : '0';
                                        if (newVal === '1') {
                                            hot.getSettings().data[rowIndex]["PlatformQualifier"] = "";
                                        }
                                       
                                            hot.updateSettings({
                                                cells: function (row, col, prop) {
                                                    var cellProperties = {};
                                                    var Index = hot.propToCol('Disable');

                                                    if (prop === 'PlatformQualifier' && (hot.getData()[row][Index] === 'Yes' || hot.getData()[row][Index] === 'True' || hot.getData()[row][Index] === '1')) {
                                                        cellProperties.readOnly = 'true';
                                                    }
                                                    else if (prop === 'PlatformQualifier' && (hot.getData()[row][Index] === 'Yes' || hot.getData()[row][Index] === 'True' || hot.getData()[row][Index] === '1')) {
                                                        cellProperties.readOnly = 'false';
                                                    }
                                                    return cellProperties;
                                                }
                                            });
                                            hot.render();
                                        

                                    }


                                    if (prop.toLowerCase() == "schedule-b") {
                                        autocalculateCommodityCode(changes[i]);
                                    }

                                    if (prop.toLowerCase() == "reorder point") {
                                        autocalculateReorderPointUpdatedOn(changes[i]);
                                    }

                                    if (prop.toLowerCase() == "reorder point last updated date") {
                                        var reorderPoint = hot.getDataAtProp('Reorder Point')[rowIndex];
                                        var newDate = FormattedDate(new Date());
                                        if ((reorderPoint != "" || reorderPoint == "0") && newVal == "") {
                                            alertmsg("yellow",
                                                "Cannot remove while Reorder Point '{0}' exists. Value set to today '{1}'.".format(reorderPoint, newDate));
                                            hot.getSettings().data[rowIndex]["Reorder Point Last Updated Date"] = newDate;
                                            hot.render();
                                        }
                                        continue;
                                    }

                                    if (prop.toLowerCase() == "target quantity") {
                                        autocalculateTargetQuantityUpdatedOn(changes[i]);
                                    }

                                    if (prop.toLowerCase() == "target quantity last updated date") {
                                        var targetQuantity = hot.getDataAtProp('Target Quantity')[rowIndex];
                                        var newDate = FormattedDate(new Date());
                                        if ((targetQuantity != "" || targetQuantity == "0") && newVal == "") {
                                            alertmsg("yellow",
                                                "Cannot remove while Target Quantity '{0}' exists. Value set to today '{1}'".format(targetQuantity, newDate));
                                            hot.getSettings().data[rowIndex]["Target Quantity Last Updated Date"] = newDate;
                                            hot.render();
                                        }
                                        continue;
                                    }

                                    //for yahooid autocalculation
                                    if (prop.toLowerCase() == "code" || prop.toLowerCase() == "localsku" || prop.toLowerCase() == "manufacturer" || prop.toLowerCase() == "name") {
                                        autoCalculateYahooId(changes[i]);
                                    }

                                    //for product-url autocalculation
                                    if (prop.toLowerCase() == "yahooid") {
                                        autocalculateProductUrl(changes[i]);
                                       // autocalculateMobileLinkUrl(changes[i]);
                                        autocalculateImageUrl(changes[i]);
                                    }
                                    if (prop.toLowerCase() == "product_url") {
                                        continue;
                                    }

                                    if (prop.toLowerCase() == "un number" || prop.toLowerCase() == "un class" || prop.toLowerCase() == "packing group") {
                                        autoCalcualteHazmatType(changes[i]);
                                    }




                                }
                            }
                        }
                    }
                };

                hot = new Handsontable(container, settings);
                if (cleansedData.length > 1 && hot.isEmptyRow(cleansedData.length - 1)) {
                    hot.alter('remove_row', parseInt(cleansedData.length - 1));
                }
                flag = 1;

                TblpgntBtm1();

            },
            error: function (xhr, status, error) {
                alert(status + ' ' + error);
            }
        });

    }
});

//SG-775
function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

function getPaginatedGrid(productIdList, count) {
    $("#pagination").show();
    //  var pagesize = $("#pageSize").val();
    var isPaginationSuccess = false;
    $.ajax({
        type: 'POST',
        url: urlgetCustomFieldUpdatePagination,
        dataType: "json",
        data: {
            'productIdList': productIdList
        },
        beforeSend: function () {
            SGloading('Loading');
        },
        success: function (response) {
            isPaginationSuccess = true;

            getData(productIdList, 1, count);
            //$("#pagination").pagination({
            //    items: response,
            //    itemsOnPage: 101,
            //    cssStyle: 'light-theme',
            //    onPageClick: function (e) {
            //        getData(productIdList, e, 20);
            //    }
            //});

        },
        complete: function () {
            if (!isPaginationSuccess)
                SGloadingRemove();
        },
        error: function (xhr, status, error) {
            alert(status + ' ' + error);
            SGloadingRemove();
        }
    });

};

function getData(productIdList, pageNumber, pagesize) {

    var reqPageNumber = pageNumber;
    var reqPageSize = pagesize;
    var isSuccess = false;

    $.ajax({
        type: 'POST',
        url: urlgetCustomFieldUpdate,
        data: {
            'productIdList': productIdList,
            'pageNumber': reqPageNumber,
            'pageSize': reqPageSize
        },
        beforeSend: function () {

            SGloading('Loading');
        },
        success: function (response) {

            isSuccess = true;

            if (response == null) {
                alertmsg('red', msgErrorOccured, -1);
                return;
            }

            if (response.Status !== true) {
                alertmsg('red', response.Message, -1);
                return;
            }

            var responseData = response.Data;

            var data = fixJsonQuote(responseData.data);
            var columns = fixJsonQuote(responseData.columns);

            //dataSchema = getDefaultHandsonValue(columns);

            var settings = {
                data: data,
                colHeaders: true,
                rowHeaders: true,
                //stretchH: 'all',
                columnSorting: false,
                //search: true,
                fixedRowsTop: 0,
                fixedColumnsLeft: 1,
                contextMenu: contextMenuBIU,
                licenseKey: 'non-commercial-and-evaluation',
                columns: columns,
                minSpareRows: 0,
                manualColumnResize: true,
                //manualColumnMove: true,
                afterLoadData: function () {
                    SGloadingRemove();
                },
                //dataSchema: dataSchema,
                //autoColumnSize: true,
                afterChange: function (changes, source) {
                    if (changes != null) {

                        for (var i = 0; i < changes.length; i++) {
                            if (changes[i] != null) {
                                // [ [row, prop, oldVal, newVal], ... ].
                                var rowIndex = changes[i][0];
                                var prop = changes[i][1];
                                var newVal = changes[i][3];
                                var oldVal = changes[i][2];
                           
                                                            
                                var decodedVal = decodeHtml(newVal, true);
                                if (newVal != decodedVal) {
                                    hot.setDataAtRowProp(rowIndex, prop, decodedVal);
                                }

                                if (prop.toLocaleLowerCase() === "fulfillmentcenter") {

                                   // if (newVal === "" || newVal === " " || newVal === "{blank}") {
                                    if (newVal === "") {//DaniUpdate1212020
                                        hot.getSettings().data[rowIndex][prop] = " ";
                                        hot.render();
                                    }
                                }
                                if (prop.toLocaleLowerCase() == "tax code") {

                                    if (newVal === "" || newVal === " " || newVal === "{blank}") {

                                        hot.getSettings().data[rowIndex][prop] = " ";
                                        hot.render();
                                    }
                                }
                                
                                if (prop.toLocaleLowerCase() === "batchlottracking") {

                                    if (newVal === "" || newVal === " " ) {

                                        hot.getSettings().data[rowIndex][prop] = "No";
                                        hot.render();
                                       // $('.htInvalid').removeClass('htInvalid');
                                    }
                                    else if (newVal === "{blank}") {
                                        alertmsg("red", "You cannot choose {blank} value on drop-down, please select value of dropdown", -1);
                                        return;
                                    }
                                }

                                if (prop.toLowerCase() == "shippinglength" || prop.toLowerCase() == "shippingheight" || prop.toLowerCase() == "shippingwidth") {
                                    autoCalculateItemPoints(changes[i]);
                                }
                             
                                if (prop.toLowerCase() === "disable") {
                                    newVal = (newVal != null && (newVal === 'Yes' || newVal === 'True' || newVal === '1')) ? '1' : '0';
                                    if (newVal === '1' || newVal === 'Yes') {
                                        hot.getSettings().data[rowIndex]["PlatformQualifier"] = "";
                                    }
                                 
                                        hot.updateSettings({
                                            cells: function (row, col, prop) {
                                                var cellProperties = {};
                                                var Index = hot.propToCol('Disable');
                                                if (prop === 'PlatformQualifier' && (hot.getData()[row][Index] === 'Yes' || hot.getData()[row][Index] === 'True' || hot.getData()[row][Index] === '1')) {
                                                    cellProperties.readOnly = 'true';
                                                }
                                                else if (prop === 'PlatformQualifier' && (hot.getData()[row][Index] === 'Yes' || hot.getData()[row][Index] === 'True' || hot.getData()[row][Index] === '1')) {
                                                    cellProperties.readOnly = 'false';
                                                }
                                                return cellProperties;
                                            }
                                        });
                                        hot.render();

                                    
                                }
                                if (prop.toLowerCase() == "schedule-b") {
                                    autocalculateCommodityCode(changes[i]);
                                }

                                if (prop.toLowerCase() == "reorder point") {
                                    autocalculateReorderPointUpdatedOn(changes[i]);
                                }

                                if (prop.toLowerCase() == "reorder point last updated date") {
                                    var reorderPoint = hot.getDataAtProp('Reorder Point')[rowIndex];
                                    var newDate = FormattedDate(new Date());
                                    if ((reorderPoint != "" || reorderPoint == "0") && newVal == "") {
                                        alertmsg("yellow",
                                            "Cannot remove while Reorder Point '{0}' exists. Value set to today '{1}'.".format(reorderPoint, newDate));
                                        hot.getSettings().data[rowIndex]["Reorder Point Last Updated Date"] = newDate;
                                        hot.render();
                                    }
                                    continue;
                                }

                                if (prop.toLowerCase() == "target quantity") {
                                    autocalculateTargetQuantityUpdatedOn(changes[i]);
                                }

                                if (prop.toLowerCase() == "target quantity last updated date") {
                                    var targetQuantity = hot.getDataAtProp('Target Quantity')[rowIndex];
                                    var newDate = FormattedDate(new Date());
                                    if ((targetQuantity != "" || targetQuantity == "0") && newVal == "") {
                                        alertmsg("yellow",
                                            "Cannot remove while Target Quantity '{0}' exists. Value set to today '{1}'".format(targetQuantity, newDate));
                                        hot.getSettings().data[rowIndex]["Target Quantity Last Updated Date"] = newDate;
                                        hot.render();
                                    }
                                    continue;
                                }

                                //for yahooid autocalculation
                                if (prop.toLowerCase() == "code" || prop.toLowerCase() == "localsku" || prop.toLowerCase() == "manufacturer" || prop.toLowerCase() == "name") {
                                    autoCalculateYahooId(changes[i]);
                                }

                                //for product-url autocalculation
                                if (prop.toLowerCase() == "yahooid") {
                                    autocalculateProductUrl(changes[i]);
                                    //autocalculateMobileLinkUrl(changes[i]);
                                    autocalculateImageUrl(changes[i]);
                                }
                                if (prop.toLowerCase() == "product_url") {
                                    continue;
                                }

                                if (prop.toLowerCase() == "un number" || prop.toLowerCase() == "un class" || prop.toLowerCase() == "packing group") {
                                    autoCalcualteHazmatType(changes[i]);
                                }

                                var caseqty = hot.getSettings().data[rowIndex]["case-qty"];
                                var HazmatBoxFee = hot.getSettings().data[rowIndex]["HazmatBoxFee"];
                                if (prop.toLowerCase() === "case-qty" && (caseqty === "" || caseqty === null)) {
                                    hot.getSettings().data[rowIndex][prop] = 1;
                                    hot.render();
                                }
                                if (prop.toLowerCase() === "hazmatboxfee" && (HazmatBoxFee === "" || HazmatBoxFee === null)) {
                                    hot.getSettings().data[rowIndex][prop] = "0.00";
                                    hot.render();
                                }
                               

                            }
                        }
                    }
                }

            }

            if (flag !== 0) {
                flag = 0;
                hot.destroy();
            }

            hot = new Handsontable(container, settings);

            flag = 1;
        },
        complete: function () {

            if (!isSuccess) {
                //console.log('sgloading removed on ajax complete');

                SGloadingRemove();
            }
        },
        error: function (xhr, status, error) {

            alert(status + ' ' + error);
            SGloadingRemove();
        }

    });

};
var pIdx = [];
//options = {customFieldId: "1", customFieldAlias: "test"};
var addCustomFieldToGrid = function (options) {
    var count = 0;
    var cols = hot.getSettings().columns;
    
    //if (options.customFieldAlias === "" || options.customFieldId === "") {
    //    udh.notify.error('Please choose a custom field');
    //    return;
    //}

    //if (JSON.stringify(hot.getSettings().columns).indexOf(options.customFieldAlias) > -1
    //    && JSON.stringify(hot.getSettings().columns).indexOf(options.customFieldId) > -1) {
    //    udh.notify.error('Cannot insert duplicate custom field to grid');
    //    return;
    //}

    var url = urlGetCustomFieldValue;

    //if custom field is referenced, set appropriate url 
    $.grep(referencedCustomFields, function (n, i) {
        //console.log(n.CustomFieldId);
        if (n.CustomFieldId == options.customFieldId) {
            //console.log(n.CustomFieldId);
            url = urlGetReferencedCustomFieldValue;
        }
    });

    $.ajax({
        type: 'POST',
        url: url,
        data: {
            'productIdList': productIdList,
            'customFieldId': options.customFieldId
        },
        beforeSend: function () {
            SGloading('Loading values');
        },
        success: function (response) {
            //console.log(response);
            //try {
            if (response == null) {
                alertmsg('red', msgErrorOccured, -1);
                return;
            }

            if (response.Status !== true) {
                alertmsg('red', response.Message, -1);
                return;
            }

            var responseData = response.Data;

            var cfValCustomFieldType;
            var cfValAllowedValues;

            var isAjaxRequest = false;

            var ajaxRequestIndex = 0;

            //var data = hot.getSettings().data;
            var data = hot.getSettings().data;
            var dataLength = data.length;
            var filledProductIds = [];
           
            responseData = decodeHtml(responseData);
            bishwo = responseData;
            //if you reach here, you're really in trouble. contact me !!! :) lol...
            responseData = $.parseJSON(responseData);

            //responseData = responseData.replace('"[{', '[{').replace('}]"', '}]');
            //fill the custom field data
            var valCustomFieldType;
            var valAllowedValues;

           
              
            for (var i = 0; i < data.length; i++)
            {
                var productId = data[i].ProductId;
                var customFieldSdvId = 67280;
                if (data[i].IsKit == 1)
                    customFieldSdvId = 67282;

                /* --Sample responseData
                [{
                    "CustomFieldId" : 36,
                    "AssociationId" : 1431,
                    "CustomFieldAssociationTypeSdvId" : 67280,
                    "CustomFieldValueId" : 2443005,
                    "CustomFieldName" : "is_hazmat_product",
                    "CustomFieldAlias" : "is-hazmat-product",
                    "AllowedValuesSdtId" : 13002,
                    "AllowedValues" : "BooleanYesNo",
                    "CustomFieldTypeSdvId" : 68312,
                    "CustomFieldType" : "VARCHAR(MAX)",
                    "FieldValue" : "No"
                }]
                */

                for (var k = 0; k < responseData.length; k++) {

                    var element = responseData[k];

                    if (element.AssociationId == productId && (element.CustomFieldAssociationTypeSdvId == customFieldSdvId || element.CustomFieldAssociationTypeSdvId == null)) {

                        filledProductIds.push(productId);

                        var valCustomFieldName = element.CustomFieldName;
                        valAllowedValues = element.AllowedValues;
                        var valAllowedValuesSdtId = element.AllowedValuesSdtId;
                        valCustomFieldType = element.CustomFieldType;
                        var valCustomFieldValue = element.FieldValue;
                        var valCustomFieldIsReadOnly = element.ReadOnly;

                        //add column only once for all data
                        if (count == 0)
                        {
                            count = 1;
                            var colType = getHandsontableTypeFromCustomFieldDataType(valCustomFieldType, valAllowedValues);

                            var valCfFormat = "";
                            if (valCustomFieldType) {
                                valCfFormat = valCustomFieldType.toLowerCase() === "float" ? "0,0.0000" : "";
                            }

                            //decode custom field value before deploying (server sends encoded custom field value)
                            valCustomFieldValue = decodeHtml(valCustomFieldValue);

                            var valAddCol;

                            //special handling for product category
                            //console.log(valCustomFieldName);
                            if (valAllowedValuesSdtId || valCustomFieldName === "udh_product_category_reserved_key" || valCustomFieldName.toLowerCase() === "category" || valCustomFieldName ==="udh_platform_qualifier_reserved_key") 
                             {
                                var valListValAllowedValues = [];

                                var requestUrl = urlGetStaticDataValues;
                                var requestData = { "staticDataTypeId": valAllowedValuesSdtId };

                                if (valCustomFieldName === "udh_product_category_reserved_key"
                                    || valCustomFieldName.toLowerCase() === "category") {
                                    requestUrl = urlGetProductCategory;
                                    requestData = null;
                                }

                                if (valCustomFieldName === "udh_platform_qualifier_reserved_key"
                                    || valCustomFieldName.toLowerCase() === "PlatformQualifier") {
                                    requestUrl = urlGetProductCategory;
                                    requestData = null;
                                }

                                //state that ajax request has been sent
                                isAjaxRequest = true;

                                //alert('get sdv');

                                //ajax start
                                $.ajax({
                                    url: requestUrl,
                                    type: 'POST',
                                    data: requestData,
                                    ajax: false,
                                    dataType: "json",
                                    beforeSend: function () {
                                        SGloading('Loading');
                                    },
                                    success: function (result) {

                                        if (result === "NO_DATA") {
                                            alertmsg("red", "No options exists for given static data type, please add static values first.", -1);
                                            return;
                                        }
                                        var jsonResult = $.parseJSON(result);

                                        //Create a new select element and then append all options. Once loop is finished, append it to actual DOM object.
                                        //This way we'll modify DOM for only one time!
                                        if (jsonResult.length > 0) {
                                            //add a blank value by default to top
                                            //valListValAllowedValues.push('');
                                        }
                                        if (valAllowedValuesSdtId !== 7000) {
                                            $.each(jsonResult, function (index, val) {
                                                if (val.Name === " ") { val.Name = "" }
                                                //if (val.IsDisabled === true) {
                                                //    disabledProducts.push(val.Name);
                                                //}
                                                valListValAllowedValues.push(val.Name);
                                             });
                                        } else {
                                            $.each(jsonResult, function (index, val) {

                                                valListValAllowedValues.push(val.TargetUOM);
                                            });
                                        }


                                        //optionsList = valListValAllowedValues;
                                        //if (options.customFieldAlias === 'PlatformQualifier') {
                                        //    optionsList = optionsList.splice(0, 2);
                                        //    var lowercaseOptionList = [];
                                        //    for (var i = 0; i < optionsList.length; i++) {
                                        //        lowercaseOptionList.push(optionsList[i].toLowerCase());
                                        //    }

                                        //    //lowercaseOptionList = lowercaseOptionList;


                                        //    optionsList = lowercaseOptionList;
                                        //    platformOptionList = optionsList;
                                        //    valAddCol = {
                                        //        name: options.customFieldAlias,
                                        //        data: options.customFieldAlias,
                                        //        placeholder: "Double click to choose platform",
                                        //        editor: 'select2',
                                        //        renderer: customDropdownRenderer,
                                        //        select2Options: {
                                        //            data: optionsList,
                                        //            //dropdownAutoWidth: true,
                                        //            tags: true,
                                        //            width: 200,
                                        //            multiple: true
                                        //        },
                                        //        title: options.customFieldAlias
                                        //    }
                                        //}

                                        // else {

                                        if (options.customFieldAlias == 'BatchLotTracking') {
                                            //valListValAllowedValues.push('')
                                            valAddCol = {
                                                'title': options.customFieldAlias,
                                                'readonly': false,
                                                'data': options.customFieldAlias,
                                                'type': 'dropdown',
                                                'id': options.customFieldId,
                                                'source': valListValAllowedValues,
                                                'renderer': 'batchLotafterCellLoadRenderer'
                                                //'renderer': 'html'
                                                //'format': val_cfFormat --> format not required for dropdown
                                            };

                                        }
                                        else if (options.customFieldAlias == 'FulfillmentCenter') {
                                            valAddCol = {
                                                'title': options.customFieldAlias,
                                                'readonly': false,
                                                'data': options.customFieldAlias,
                                                'type': 'dropdown',
                                                'id': options.customFieldId,
                                                'source': valListValAllowedValues,
                                                'renderer': 'fulfillmentafterCellLoadRenderer'
                                                //'renderer': 'html'
                                                //'format': val_cfFormat --> format not required for dropdown
                                            };

                                        }
                                        //else if (options.customFieldAlias == 'Product Category') {
                                        //    valAddCol = {
                                        //        'title': options.customFieldAlias,
                                        //        'readonly': false,
                                        //        'data': options.customFieldAlias,
                                        //        'type': 'dropdown',
                                        //        'id': options.customFieldId,
                                        //        'source': valListValAllowedValues,
                                        //        'allowInvalid': false,
                                        //        'strict': true,
                                        //        'trimDropdown': false,
                                        //        //'renderer':'html'
                                        //        'renderer': 'ProductCategoryRenderer'
                                        //        //'format': val_cfFormat --> format not required for dropdown
                                        //    };

                                        //}
                                        else if (options.customFieldAlias == 'Product Category') {
                                            valAddCol = {
                                                'title': options.customFieldAlias,
                                                'readonly': false,
                                                'data': options.customFieldAlias,
                                                'editor': 'chosen',
                                                'id': options.customFieldId,
                                                'chosenOptions': {
                                                    multiple: true,
                                                    data: jsonResult
                                                },
                                                'width': 300,
                                                // 'allowInvalid': false,
                                                //'strict': true,
                                                //'trimDropdown': false,
                                                //'renderer':'html'
                                                'renderer': 'ProductCategoryRenderer'
                                                //'format': val_cfFormat --> format not required for dropdown
                                            };
                                        }


                                        else if (options.customFieldAlias == 'PlatformQualifier') {
                                            valAddCol = {
                                                'title': options.customFieldAlias,
                                                'readonly': false,
                                                'data': options.customFieldAlias,
                                                'editor': 'chosen',
                                                'id': options.customFieldId,
                                                'chosenOptions': {
                                                    multiple: true,
                                                    data: [
                                                             {
                                                                 id: "Yahoo",
                                                                 label: "Yahoo"
                                                             }, {
                                                                 id: "Google",
                                                                 label: "Google"
                                                             }, {
                                                                 id: "Bing",
                                                                 label: "Bing"
                                                             }, {
                                                                 id: "BigCommerce",
                                                                 label: "BigCommerce"
                                                             }
                                                           ]
                                                },
                                                'width': 300,
                                                // 'allowInvalid': false,
                                                //'strict': true,
                                                //'trimDropdown': false,
                                                //'renderer':'html'
                                                'renderer': 'PlatformQualifierRenderer'
                                                //'format': val_cfFormat --> format not required for dropdown
                                            };
                                        }



                                        else if (options.customFieldAlias == 'BC Brand') {
                                            valAddCol = {
                                                'title': options.customFieldAlias,
                                                'readonly': false,
                                                'data': options.customFieldAlias,
                                                'type': 'dropdown',
                                                'id': options.customFieldId,
                                                'source': valListValAllowedValues,
                                                'allowInvalid': false,
                                                'trimDropdown': false,
                                                //'renderer':'html'
                                                'renderer': 'BrandRenderer'
                                                //'format': val_cfFormat --> format not required for dropdown
                                            };

                                        }

                                        else {


                                            valAddCol = {
                                                'title': options.customFieldAlias,
                                                'readonly': valCustomFieldIsReadOnly,
                                                'data': options.customFieldAlias,
                                                'type': 'dropdown',
                                                'id': options.customFieldId,
                                                'source': valListValAllowedValues,
                                                'trimDropdown': false
                                                //'renderer': 'html'
                                                //'format': val_cfFormat --> format not required for dropdown
                                            };
                                        }
                                        

                                        // }

                                        cols.push(valAddCol);
                                        
                                        var i = -1;
                                        
                                        
                                        hot.updateSettings({
                                            columns: cols
                                        });

                                    },
                                    complete: function () {
                                        SGloadingRemove();
                                    },
                                    error: function (err) {
                                        //console.log(err);
                                    }
                                });
                                //ajax end

                            }
                            else if (colType === "date") {
                                valAddCol = {
                                    'title': options.customFieldAlias,
                                    'readonly': valCustomFieldIsReadOnly,
                                    'data': options.customFieldAlias,
                                    'type': colType,
                                    'dateFormat': 'MM/DD/YYYY',
                                    'correctFormat': true,
                                    'id': options.customFieldId,
                                    'format': valCfFormat
                                    //'renderer': 'html'
                                };
                            }
                            else {
                                //console.log('else');

                                valAddCol = {
                                    'title': options.customFieldAlias,
                                    'readOnly': valCustomFieldIsReadOnly,
                                    'data': options.customFieldAlias,
                                    'type': colType,
                                    'id': options.customFieldId,
                                    'format': valCfFormat
                                    //'renderer': 'html'
                                };
                            }

                            if (isAjaxRequest === false) {
                                if (valAddCol.data === "Hazmat Type") { valAddCol.readOnly = true; }
                                if (valAddCol.data === "ItemPoints") { valAddCol.readOnly = true; }
                                var duplicateColumn = false;
                                $.each(cols, function (i, element) {
                                    if (element.data == valAddCol.data) {
                                        duplicateColumn = true;
                                    }
                                })
                                if (duplicateColumn === false) {
                                    cols.push(valAddCol);
                                    hot.updateSettings({

                                        columns: cols,

                                    });
                                }
                            }
                        }

                        //column has been set up. now set data
                        //if data has newline, show it to handsontable
                        //valCustomFieldValue = decodeHtml(valCustomFieldValue);
                        //decode html, but do not replace whitespace characters
                        //console.log(valCustomFieldValue);
                        valCustomFieldValue = decodeHtml(valCustomFieldValue);
                        valCustomFieldValue = decodeHtml(valCustomFieldValue);
                        valCustomFieldValue = valCustomFieldValue.replace(/\n/g, "\\\\n");

                        //
                       
                        //console.log(valCustomFieldValue);
                        //console.log("---");
                        //valCustomFieldValue = fixNewLineOnly(valCustomFieldValue);

                        data[i][options.customFieldAlias] = valCustomFieldValue;
                        data[i][options.customFieldAlias + "_name"] = valCustomFieldName;
                        data[i][options.customFieldAlias + "_id"] = options.customFieldId;
                        var imageUrlCellValue = hot.getDataAtRowProp(i, "Image URL");
                        var IsDisabled = hot.getDataAtRowProp(i, "Disable");
                        var fulfillmentCenter = hot.getDataAtProp(i, "FulfillmentCenter");
                        var BatchLotTracking = hot.getDataAtProp(i, "BatchLotTracking");
                        var taxcode = hot.getDataAtProp(i, "Tax Code");
                        

                       // if (fulfillmentCenter === "" || fulfillmentCenter === " " || fulfillmentCenter === "{blank}") {
                        if (fulfillmentCenter === "") {//DaniUpdate1212020
                            hot.setDataAtRowProp(rowIndex, "FulfillmentCenter", "");
                            hot.render();
                           
                        }
                        if (taxcode === "" || taxcode === " " || taxcode === "{blank}") {
                            hot.setDataAtRowProp(rowIndex, "tax code", "");
                            hot.render();

                        }
                       
                        
                        if (BatchLotTracking === "" || BatchLotTracking === " ") {
                            hot.setDataAtRowProp(i, "BatchLotTracking", "No");
                            hot.render();
                            

                        }
                        
                       else if(BatchLotTracking === "{blank}") {
                            alertmsg("red", "You cannot choose {blank} value on drop-down, please select value of dropdown", -1);
                           return;
                       }
                        if (imageUrlCellValue === "") {
                            hot.setDataAtRowProp(i, "Image URL", "https://sep.yimg.com/ty/cdn/stylespilotshop/image_coming_soon.jpg");
                        }
                        IsDisabled = (IsDisabled != null && (IsDisabled === 'Yes' || IsDisabled === 'True' || IsDisabled === '1')) ? '1' : '0';
                        if (IsDisabled === '1' && (options.customFieldAlias === 'YahooId' || options.customFieldAlias === 'Disable')) {
                            hot.getSettings().data[i]["PlatformQualifier"] = "";                         
                            hot.updateSettings({
                                cells: function (row, col, prop) {
                                    var cellProperties = {};
                                    var Index = hot.propToCol('Disable');
                                    if (prop === 'PlatformQualifier' && (hot.getData()[row][Index] === 'Yes' || hot.getData()[row][Index] === 'True' || hot.getData()[row][Index] === '1')) {
                                        cellProperties.readOnly = 'true';
                                    }
                                    else if (prop === 'PlatformQualifier' && (hot.getData()[row][Index] === 'Yes' || hot.getData()[row][Index] === 'True' || hot.getData()[row][Index] === '1')) {
                                        cellProperties.readOnly = 'false';
                                    }
                                    return cellProperties;
                                }
                            });
                            hot.render();
                        }
                        if (udh.settings.customfields.autocalculateItemPointsIfEmpty) {
                            //auto calculate itempoints on add
                            if (valCustomFieldName.toLowerCase() == "itempoints" ||
                                valCustomFieldName.toLowerCase() == "shipping_length" ||
                                valCustomFieldName.toLowerCase() == "shipping_height" ||
                                valCustomFieldName.toLowerCase() == "shipping_width") {
                                //console.log(valCustomFieldName);
                                autoCalculateItemPoints([i, valCustomFieldName, "", valCustomFieldValue]);
                            }
                        }
                        if (udh.settings.customfields.autocalculateCommodityCodeIfEmpty) {
                            //auto calculate commoditycode on add
                            if (valCustomFieldName.toLowerCase() == "schedule_b" ||
                                valCustomFieldName.toLowerCase() == "commoditycode") {
                                //console.log(valCustomFieldName);
                                autocalculateCommodityCode([i, valCustomFieldName, "", valCustomFieldValue]);
                            }
                        }
                        if (udh.settings.customfields.autocalculateReorderPointUpdatedOnIfEmpty) {
                            //auto calculate commoditycode on add
                            if (valCustomFieldName.toLowerCase() == "reorder point" ||
                                valCustomFieldName.toLowerCase() == "reorder point last updated date") {
                                //console.log(valCustomFieldName);
                                autocalculateReorderPointUpdatedOn([i, valCustomFieldName, "", valCustomFieldValue]);
                            }
                        }
                        if (udh.settings.customfields.autocalculateTargetQuantityUpdatedOnIfEmpty) {
                            //auto calculate commoditycode on add
                            if (valCustomFieldName.toLowerCase() == "target quantity" ||
                                valCustomFieldName.toLowerCase() == "target quantity last updated date") {
                                //console.log(valCustomFieldName);
                                autocalculateTargetQuantityUpdatedOn([i, valCustomFieldName, "", valCustomFieldValue]);
                            }
                        }

                        if (udh.settings.customfields.autocalculateYahooIdIfEmpty) {
                            //auto calculate yahooId on add 
                            if (valCustomFieldName.toLowerCase() == "code" ||
                                valCustomFieldName.toLowerCase() == "yahooid" ||
                                valCustomFieldName.toLowerCase() == "manufacturer" ||
                                valCustomFieldName.toLowerCase() == "name" ||
                                valCustomFieldName.toLowerCase() == "yahooid") {
                                //console.log(valCustomFieldName);
                                autoCalculateYahooId([i, valCustomFieldName, "", valCustomFieldValue]);
                            }
                        }
                        if (udh.settings.customfields.autocalculateProductUrlIfEmpty) {
                            //auto calculate product-url on add
                            if (valCustomFieldName.toLowerCase() == "yahooid" ||
                                valCustomFieldName.toLowerCase() == "product_url") {
                                //console.log(valCustomFieldName);
                                autocalculateProductUrl([i, valCustomFieldName, "", valCustomFieldValue]);
                            }


                        }
                       
                    }

                }
            }
           // initialize();
            //render handsontable
            hot.render();
            hot.validateCells();


        },
        complete: function () {



            if ($("#RelatedCustomFieldModal").hasClass("show-me")) {
                $("#RelatedCustomFieldModal").removeClass("show-me");

                //$("#RelatedCustomFieldModal").modal("show");

                $("#btnOkRelatedCustomFieldModal").click();
            }
            SGloadingRemove();
        },
        error: function (xhr, status, error) {
            SGloadingRemove();
            //console.log(xhr + ' ' + status + ' ' + error);
        }
    });

};


$(function () {

    referencedCustomFields = fixJsonQuote(referencedCustomFields);
    //console.log(referencedCustomFields);

    $('#CustomFields').select2();

    //for load selected, max limit is 100
    if (productIdList !== "") {
         //debugger;
        getPaginatedGrid(productIdList, 101);
    }
    //for load all, max limit is 1000
    if (CatalogSearchAllProducts !== "") {
        productIdList = CatalogSearchAllProducts;
        var trimmedProductList = [];
        var productIdListArray = productIdList.split(",");

       if (productIdListArray.length > 1000) {

            for (var i = 0; i < 1000; i++) { trimmedProductList.push(productIdListArray[i]) }
            productIdList = trimmedProductList.join();
        }
        //trim productList to 1000
        getData(productIdList, 1, 1000);
        //getPaginatedGrid(CatalogSearchAllProducts, 1000);
    }

    if (productIdList === "" && CatalogSearchAllProducts === "") {
        $("#add-custom-field").attr("disabled", true);
        $("#CustomFields").attr("disabled", true);
    }

    $('#add-custom-field')
        .click(function () {

          var options = {};

            var selectedCustomField = $('#CustomFields').select2('data')[0];
            var customFieldAlias = selectedCustomField.text;
            var customFieldId = selectedCustomField.id;


            options.customFieldId = customFieldId;
            options.customFieldAlias = customFieldAlias;

            if (options.customFieldAlias === "" || options.customFieldId === "") {
                udh.notify.error('Please choose a custom field', -1);
                return;
            }


            var flag = false;
            for (var i = 0; i < customFieldIdLst.length; i++) {

                if (customFieldIdLst[i] === customFieldId) {
                    udh.notify.error('Cannot insert duplicate custom field to grid', -1);
                    flag = true;
                    return false;
                }
            }
            if (flag === true) { return; }
            customFieldIdLst.push(customFieldId);

            //if (JSON.stringify(hot.getSettings().columns).indexOf(options.customFieldAlias) > -1
            //    && JSON.stringify(hot.getSettings().columns).indexOf(options.customFieldId) > -1) {
            //    udh.notify.error('Cannot insert duplicate custom field to grid',-1);
            //    return;
            //}


            $.each(hot.getSettings().columns, function (i, n) {
                if (n.title.indexOf(options.customFieldAlias) > -1 &&
                    n.id.indexOf(options.customFieldId) > -1) {
                    udh.notify.error('Cannot insert duplicate custom field to grid', -1);
                    flag = true;
                    return false;
                }
            })

            if (flag === true) {
                return;
            }

            if (customFieldAlias === "UN Number" || customFieldAlias === "UN Class" || customFieldAlias === "Packing Group" || customFieldAlias === "Hazmat Type") {
                var optionForUnitedNationsNumber = {};
                optionForUnitedNationsNumber.customFieldId = unitedNationsNumberId;//370;
                optionForUnitedNationsNumber.customFieldAlias = "UN Number";
                addCustomFieldToGrid(optionForUnitedNationsNumber);

                var optionForUnitedNationsClass = {};
                optionForUnitedNationsClass.customFieldId = unitedNationsClassId;//371;
                optionForUnitedNationsClass.customFieldAlias = "UN Class";
                addCustomFieldToGrid(optionForUnitedNationsClass);

                var optionForPackingGroup = {};
                optionForPackingGroup.customFieldId = packingGroupId;//372;
                optionForPackingGroup.customFieldAlias = "Packing Group";
                addCustomFieldToGrid(optionForPackingGroup);

                var optionForPackingGroup = {};
                optionForPackingGroup.customFieldId = hazmatTypeId;//25;
                optionForPackingGroup.customFieldAlias = "Hazmat Type";
                addCustomFieldToGrid(optionForPackingGroup);

                notifyAddtionOfRelatedField(HazmatGroup, customFieldAlias);


            }

            else if (customFieldAlias === "ItemPoints" || customFieldAlias.toLowerCase() === "shippinglength" || customFieldAlias.toLowerCase() === "shippingheight" || customFieldAlias.toLowerCase() === "shippingwidth") {
                var optionForItemPoints = {};
                optionForItemPoints.customFieldId = ItemPointId;
                optionForItemPoints.customFieldAlias = "ItemPoints";
                addCustomFieldToGrid(optionForItemPoints);

                var optionForlength = {};
                optionForlength.customFieldId = lengthId;
                optionForlength.customFieldAlias = "ShippingLength";
                addCustomFieldToGrid(optionForlength);

                var optionForwidth = {};
                optionForwidth.customFieldId = widthId;
                optionForwidth.customFieldAlias = "ShippingWidth";
                addCustomFieldToGrid(optionForwidth);

                var optionForheight = {};
                optionForheight.customFieldId = heightId;
                optionForheight.customFieldAlias = "ShippingHeight";
                addCustomFieldToGrid(optionForheight);
                notifyAddtionOfRelatedField(itemPointsGroup, customFieldAlias);

            }

            else if (customFieldAlias === "YahooId" || customFieldAlias === "product-url" || customFieldAlias === "Image URL" || customFieldAlias === "PlatformQualifier" || customFieldAlias === "Disable") {
                var optionForYahooId = {};
                optionForYahooId.customFieldId = yahooId;
                optionForYahooId.customFieldAlias = "YahooId";
                addCustomFieldToGrid(optionForYahooId);

                var optionForProductUrl = {};
                optionForProductUrl.customFieldId = product_url;
                optionForProductUrl.customFieldAlias = "product-url";
                addCustomFieldToGrid(optionForProductUrl);

                var optionFoPlatformQualifier = {};
                optionFoPlatformQualifier.customFieldId = platformQualifier;
                optionFoPlatformQualifier.customFieldAlias = "PlatformQualifier";
                addCustomFieldToGrid(optionFoPlatformQualifier);

                var optionForImageUrl = {};
                optionForImageUrl.customFieldId = imageUrl;
                optionForImageUrl.customFieldAlias = "Image URL";
                addCustomFieldToGrid(optionForImageUrl);

                var optionFordisable = {};
                optionFordisable.customFieldId = isDisable;
                optionFordisable.customFieldAlias = "Disable";
                addCustomFieldToGrid(optionFordisable);

                notifyAddtionOfRelatedField(yahooIdGroup, customFieldAlias);
                return;


            }
            else { addCustomFieldToGrid(options); }
           
            //check if custom field is related
            var relatedCustomFieldsList = relatedCf.match(options.customFieldId);

            //if related custom fields are found, proceed to add them to grid
            if (relatedCustomFieldsList && relatedCustomFieldsList.length > 0) {
                var me = null;
                var other = null;

                //get existing custom fields in the grid view
                var existingCustomFieldsInGrid = hot.getSettings().columns;

                //var duplicate = $.grep(existingCustomFieldsInGrid,
                //    function(v, i) {
                //        $.grep(relatedCustomFieldsList,
                //            function(vv, ii) {
                //                return v.title != vv.target.fieldAlias || v.title != vv.source.fieldAlias;
                //            });
                //    });

                //console.log("duplicate", duplicate);

                if (relatedCustomFieldsList[0].source.customFieldId == customFieldId) {
                    me = relatedCustomFieldsList[0].source;
                    other = relatedCustomFieldsList[0].target;
                } else {
                    me = relatedCustomFieldsList[0].target;
                    other = relatedCustomFieldsList[0].source;
                }

                $("#labelRelatedCustomFields")
                    .text("The custom field '{0}' is related to '{1}'"
                        .format(me.customFieldAlias,
                            other.customFieldAlias));
                $("#labelRelatedCustomFieldsRule").text("RULE: " + me.rule);

                //console.log(relatedCustomFieldsList[0].target);
                var sss = JSON.stringify(other);
                sss = Encoder.htmlEncode(sss);
                //console.log(sss);

                $("#hiddenRelatedCustomFields").val(JSON.stringify(sss));
                $("#RelatedCustomFieldModal").addClass("show-me");

                
            }
           

        });


    //$('#add-custom-field').click(function () {

    //    var selectedCustomField = $('#CustomFields').select2('data')[0];
    //    var customFieldAlias = selectedCustomField.text;
    //    var customFieldId = selectedCustomField.id;

    //    var cols = hot.getSettings().columns;

    //    var newColumn;

    //    if (customFieldAlias === "" || customFieldId === "") {
    //        alertmsg('red', 'Please choose a custom field');
    //        return;
    //    }

    //    if (JSON.stringify(hot.getSettings().columns).indexOf(customFieldAlias) > -1
    //        && JSON.stringify(hot.getSettings().columns).indexOf(customFieldId) > -1) {
    //        alertmsg('red', 'Cannot insert duplicate custom field to grid');
    //        return;
    //    }

    //    var url = urlGetCustomFieldValue;

    //    //if custom field is referenced, set appropriate url 
    //    $.grep(referencedCustomFields, function (n, i) {
    //        //console.log(n.CustomFieldId);
    //        if (n.CustomFieldId == customFieldId) {
    //            //console.log(n.CustomFieldId);
    //            url = urlGetReferencedCustomFieldValue;
    //        }
    //    });

    //    $.ajax({
    //        type: 'POST',
    //        url: url,
    //        data: {
    //            'productIdList': productIdList,
    //            'customFieldId': customFieldId
    //        },
    //        beforeSend: function () {
    //            SGloading('Loading values');
    //        },
    //        success: function (response) {
    //            //console.log(response);
    //            try {
    //                if (response == null) {
    //                    alertmsg('red', msgErrorOccured);
    //                    return;
    //                }

    //                if (response.Status !== true) {
    //                    alertmsg('red', response.Message);
    //                    return;
    //                }

    //                var responseData = response.Data;

    //                var cfValCustomFieldType;
    //                var cfValAllowedValues;

    //                var isAjaxRequest = false;

    //                var ajaxRequestIndex = 0;

    //                //var data = hot.getSettings().data;
    //                var data = hot.getSettings().data;
    //                var dataLength = data.length;
    //                var filledProductIds = [];

    //                responseData = decodeHtml(responseData);
    //                bishwo = responseData;
    //                //if you reach here, you're really in trouble. contact me !!! :) lol...
    //                responseData = $.parseJSON(responseData);

    //                //responseData = responseData.replace('"[{', '[{').replace('}]"', '}]');
    //                //fill the custom field data
    //                var valCustomFieldType;
    //                var valAllowedValues;
    //                for (var i = 0; i < data.length; i++) {
    //                    var productId = data[i].ProductId;

    //                    /* --Sample responseData
    //                    [{
    //                        "CustomFieldId" : 36,
    //                        "AssociationId" : 1431,
    //                        "CustomFieldAssociationTypeSdvId" : 67280,
    //                        "CustomFieldValueId" : 2443005,
    //                        "CustomFieldName" : "is_hazmat_product",
    //                        "CustomFieldAlias" : "is-hazmat-product",
    //                        "AllowedValuesSdtId" : 13002,
    //                        "AllowedValues" : "BooleanYesNo",
    //                        "CustomFieldTypeSdvId" : 68312,
    //                        "CustomFieldType" : "VARCHAR(MAX)",
    //                        "FieldValue" : "No"
    //                    }]
    //                    */

    //                    for (var k = 0; k < responseData.length; k++) {

    //                        var element = responseData[k];

    //                        if (element.AssociationId == productId) {

    //                            filledProductIds.push(productId);

    //                            var valCustomFieldName = element.CustomFieldName;
    //                            valAllowedValues = element.AllowedValues;
    //                            var valAllowedValuesSdtId = element.AllowedValuesSdtId;
    //                            valCustomFieldType = element.CustomFieldType;
    //                            var valCustomFieldValue = element.FieldValue;

    //                            //add column only once for all data
    //                            if (k == 0) {
    //                                var colType = getHandsontableTypeFromCustomFieldDataType(valCustomFieldType, valAllowedValues);

    //                                var valCfFormat = "";
    //                                if (valCustomFieldType) {
    //                                    valCfFormat = valCustomFieldType.toLowerCase() === "float" ? "0,0.0000" : "";
    //                                }

    //                                //decode custom field value before deploying (server sends encoded custom field value)
    //                                valCustomFieldValue = decodeHtml(valCustomFieldValue);

    //                                var valAddCol;

    //                                //special handling for product category
    //                                //console.log(valCustomFieldName);
    //                                if (valAllowedValuesSdtId || valCustomFieldName === "udh_product_category_reserved_key" || valCustomFieldName.toLowerCase() === "category") {
    //                                    var valListValAllowedValues = [];

    //                                    var requestUrl = urlGetStaticDataValues;
    //                                    var requestData = { "staticDataTypeId": valAllowedValuesSdtId };

    //                                    if (valCustomFieldName === "udh_product_category_reserved_key"
    //                                        || valCustomFieldName.toLowerCase() === "category") {
    //                                        requestUrl = urlGetProductCategory;
    //                                        requestData = null;
    //                                    }

    //                                    //state that ajax request has been sent
    //                                    isAjaxRequest = true;

    //                                    //alert('get sdv');

    //                                    //ajax start
    //                                    $.ajax({
    //                                        url: requestUrl,
    //                                        type: 'POST',
    //                                        data: requestData,
    //                                        dataType: "json",
    //                                        beforeSend: function () {
    //                                            SGloading('Loading');
    //                                        },
    //                                        success: function (result) {

    //                                            if (result === "NO_DATA") {
    //                                                alertmsg("red", "No options exists for given static data type, please add static values first.");
    //                                                return;
    //                                            }
    //                                            var jsonResult = $.parseJSON(result);

    //                                            //Create a new select element and then append all options. Once loop is finished, append it to actual DOM object.
    //                                            //This way we'll modify DOM for only one time!
    //                                            if (jsonResult.length > 0) {
    //                                                //add a blank value by default to top
    //                                                valListValAllowedValues.push('');
    //                                            }
    //                                            $.each(jsonResult, function (index, val) {
    //                                                valListValAllowedValues.push(val.Name);
    //                                            });

    //                                            valAddCol = {
    //                                                'title': customFieldAlias,
    //                                                'readonly': false,
    //                                                'data': customFieldAlias,
    //                                                'type': 'dropdown',
    //                                                'id': customFieldId,
    //                                                'source': valListValAllowedValues
    //                                                //'renderer': 'html'
    //                                                //'format': val_cfFormat --> format not required for dropdown
    //                                            };

    //                                            cols.push(valAddCol);

    //                                            hot.updateSettings({
    //                                                columns: cols
    //                                            });

    //                                        },
    //                                        complete: function () {
    //                                            SGloadingRemove();
    //                                        },
    //                                        error: function (err) {
    //                                            //console.log(err);
    //                                        }
    //                                    });
    //                                    //ajax end

    //                                }
    //                                else if (colType === "date") {
    //                                    valAddCol = {
    //                                        'title': customFieldAlias,
    //                                        'readonly': false,
    //                                        'data': customFieldAlias,
    //                                        'type': colType,
    //                                        'dateFormat': 'MM/DD/YYYY',
    //                                        'correctFormat': true,
    //                                        'id': customFieldId,
    //                                        'format': valCfFormat
    //                                        //'renderer': 'html'
    //                                    };
    //                                } else {
    //                                    //console.log('else');

    //                                    valAddCol = {
    //                                        'title': customFieldAlias,
    //                                        'readonly': false,
    //                                        'data': customFieldAlias,
    //                                        'type': colType,
    //                                        'id': customFieldId,
    //                                        'format': valCfFormat
    //                                        //'renderer': 'html'
    //                                    };
    //                                }

    //                                if (isAjaxRequest === false) {
    //                                    cols.push(valAddCol);

    //                                    hot.updateSettings({
    //                                        columns: cols
    //                                    });
    //                                }
    //                            }

    //                            //column has been set up. now set data
    //                            //if data has newline, show it to handsontable
    //                            //valCustomFieldValue = decodeHtml(valCustomFieldValue);


    //                            //decode html, but do not replace whitespace characters
    //                            //console.log(valCustomFieldValue);
    //                            valCustomFieldValue = decodeHtml(valCustomFieldValue);
    //                            valCustomFieldValue = decodeHtml(valCustomFieldValue);
    //                            valCustomFieldValue = valCustomFieldValue.replace(/\n/g, "\\\\n");
    //                            //console.log(valCustomFieldValue);
    //                            //console.log("---");
    //                            //valCustomFieldValue = fixNewLineOnly(valCustomFieldValue);

    //                            data[i][customFieldAlias] = valCustomFieldValue;
    //                            data[i][customFieldAlias + "_name"] = valCustomFieldName;
    //                            data[i][customFieldAlias + "_id"] = customFieldId;

    //                            if (udh.settings.customfields.autocalculateItemPointsIfEmpty) {
    //                                //auto calculate itempoints on add
    //                                if (valCustomFieldName.toLowerCase() == "itempoints" ||
    //                                    valCustomFieldName.toLowerCase() == "dimensions") {
    //                                    //console.log(valCustomFieldName);
    //                                    autoCalculateItemPoints([i, valCustomFieldName, "", valCustomFieldValue]);
    //                                }
    //                            }
    //                            if (udh.settings.customfields.autocalculateCommodityCodeIfEmpty) {
    //                                //auto calculate commoditycode on add
    //                                if (valCustomFieldName.toLowerCase() == "schedule_b" ||
    //                                    valCustomFieldName.toLowerCase() == "commoditycode") {
    //                                    //console.log(valCustomFieldName);
    //                                    autocalculateCommodityCode([i, valCustomFieldName, "", valCustomFieldValue]);
    //                                }
    //                            }
    //                            if (udh.settings.customfields.autocalculateReorderPointUpdatedOnIfEmpty) {
    //                                //auto calculate commoditycode on add
    //                                if (valCustomFieldName.toLowerCase() == "reorder point" ||
    //                                    valCustomFieldName.toLowerCase() == "reorder point last updated date") {
    //                                    //console.log(valCustomFieldName);
    //                                    autocalculateReorderPointUpdatedOn([i, valCustomFieldName, "", valCustomFieldValue]);
    //                                }
    //                            }
    //                            if (udh.settings.customfields.autocalculateTargetQuantityUpdatedOnIfEmpty) {
    //                                //auto calculate commoditycode on add
    //                                if (valCustomFieldName.toLowerCase() == "target quantity" ||
    //                                    valCustomFieldName.toLowerCase() == "target quantity last updated date") {
    //                                    //console.log(valCustomFieldName);
    //                                    autocalculateTargetQuantityUpdatedOn([i, valCustomFieldName, "", valCustomFieldValue]);
    //                                }
    //                            }

    //                            if (udh.settings.customfields.autocalculateYahooIdIfEmpty) {
    //                                //auto calculate yahooId on add 
    //                                if (valCustomFieldName.toLowerCase() == "code" ||
    //                                    valCustomFieldName.toLowerCase() == "yahooid" ||
    //                                    valCustomFieldName.toLowerCase() == "manufacturer" ||
    //                                    valCustomFieldName.toLowerCase() == "name" ||
    //                                    valCustomFieldName.toLowerCase() == "yahooid") {
    //                                    //console.log(valCustomFieldName);
    //                                    autoCalculateYahooId([i, valCustomFieldName, "", valCustomFieldValue]);
    //                                }
    //                            }
    //                            if (udh.settings.customfields.autocalculateProductUrlIfEmpty) {
    //                                //auto calculate product-url on add
    //                                if (valCustomFieldName.toLowerCase() == "yahooid" ||
    //                                    valCustomFieldName.toLowerCase() == "product_url") {
    //                                    //console.log(valCustomFieldName);
    //                                    autocalculateProductUrl([i, valCustomFieldName, "", valCustomFieldValue]);
    //                                }
    //                            }
    //                        }

    //                    }

    //                }

    //                //render handsontable
    //                hot.render();
    //                hot.validateCells();

    //                //}
    //            } catch (e) {
    //                console.log(e);
    //            }
    //        },
    //        complete: function () {

    //            SGloadingRemove();

    //        },
    //        error: function (xhr, status, error) {
    //            //console.log(xhr + ' ' + status + ' ' + error);
    //        }
    //    });

    //});

    /**
     * Export to CSV button
     */
    $("#export-custom-field-update").click(function () {
        //console.log(hot);
       // handsontable2ExportFormat.download(hot, "custom-field-bulk-update.csv", "csv", true);
        var exportPlugin1 = hot.getPlugin('exportFile');
        exportPlugin1.downloadFile('csv', {
            bom: true,
            columnDelimiter: ',',
            columnHeaders: true,
            exportHiddenColumns: false,
            exportHiddenRows: false,
            fileExtension: 'csv',
            filename: 'custom-field-bulk-update.csv',
            mimeType: 'text/csv',
            rowDelimiter: '\r\n',
            rowHeaders: false
        });
    });

    //---------------------------------------------
    //Functions for XML start
    //---------------------------------------------
    var rowData = "";

    function emptyRow(row) {
        var values = 0;
        for (var i = 1; i < row.length; i++) {
            if (row[i] !== '') {
                values++; break;
            }
        }
        return (values == 0);
    }

    function addRow(row) {
        //console.log(row);
        var tmp = "<row>";
        for (var n = 0; n < row.length; n++) {
            //tmp += "<column><![CDATA[" + Row[n] + "]]></column>";
            tmp += "<column><" + row[n] + "></column>";
        }
        tmp += "</row>";
        rowData += tmp;
    }

    function buildXml() {
        var xml = "<?xml version=\"1.0\"?><worksheet>" + rowData + "</worksheet>";
        return xml;
    }

    //---------------------------------------------
    //Functions for XML end
    //---------------------------------------------

    //Save
    //$("#btn-save").click(function () {
    //    rowData = "";
    //    //container.handsontable('deselectCell');
    //    var data = hot.getSettings().data;
    //    for (var i = 0; i < data.length; i++) {
    //        addRow(data[i]);
    //        //if (!emptyRow(data[i])) {
    //        //    addRow(data[i]);
    //        //}
    //    }
    //    var xml = buildXml();

    //    //console.log(xml);
    //});

    var hasItemPointsColumns = function (columns) {
        var itemPointsColumns = [];
        //$.each(columns,
        //    function (index, value) {
        //        if (value.data.toLowerCase() == "itempoints" ||
        //            value.data.toLowerCase() == "dimensions") {
        //            itemPointsColumns.push(value.data);
        //        }
        //    });

        //return itemPointsColumns.length == 2;

        $.each(columns,
            function (index, value) {
                if (value.data.toLowerCase() == "itempoints" ||
                    value.data.toLowerCase() == "height" ||
                    value.data.toLowerCase() == "width" ||
                    value.data.toLowerCase() == "length") {
                    itemPointsColumns.push(value.data);
                }
            });
        return itemPointsColumns.length == 4;
    }

    var hasCommodityCodeColumns = function (columns) {
        var commodityCodeColumns = [];
        $.each(columns,
            function (index, value) {
                if (value.data.toLowerCase() == "schedule-b" ||
                    value.data.toLowerCase() == "commoditycode") {
                    commodityCodeColumns.push(value.data);
                }
            });

        return commodityCodeColumns.length == 2;
    }

    var hasReorderPointUpdatedOnColumns = function (columns) {
        var reorderPointUpdatedOnColumns = [];
        $.each(columns,
            function (index, value) {
                if (value.data.toLowerCase() == "reorder point" ||
                    value.data.toLowerCase() == "reorder point last updated date") {
                    reorderPointUpdatedOnColumns.push(value.data);
                }
            });

        return reorderPointUpdatedOnColumns.length == 2;
    }

    var hasTargetQuantityUpdatedOnColumns = function (columns) {
        var targetQuantityUpdatedOnColumns = [];
        $.each(columns,
            function (index, value) {
                if (value.data.toLowerCase() == "target quantity" ||
                    value.data.toLowerCase() == "target quantity last updated date") {
                    targetQuantityUpdatedOnColumns.push(value.data);
                }
            });

        return targetQuantityUpdatedOnColumns.length == 2;
    }

    var validateItemPoints = function (data) {
        //for (var i = 0; i < data.length; i++) {
        //    var itemPoints = data[i]["ItemPoints"];
        //    var dimensions = data[i]["dimensions"];
        //    var splitDimensions = [];
        //    var calculatedItemPoints;
        //    if (dimensions) {
        //        splitDimensions = dimensions.split(' ');
        //        calculatedItemPoints = parseFloat(splitDimensions[0]) *
        //            parseFloat(splitDimensions[1]) *
        //            parseFloat(splitDimensions[2]);
        //    }
        //    if (itemPoints && dimensions && splitDimensions.length == 3 && itemPoints != calculatedItemPoints) {
        //        alertmsg("red", "{0}. ItemPoints '{1}' is invalid. Expected value is '{2}'.".format(i + 1, itemPoints, calculatedItemPoints), -1);
        //        SGloadingRemove();
        //        return false;
        //    }
        //}
        return true;
    }

    var validateCommodityCode = function (data) {
        for (var i = 0; i < data.length; i++) {
            var scheduleB = data[i]["schedule-b"];
            var commodityCode = data[i]["CommodityCode"];

            if (scheduleB && commodityCode && scheduleB.substring(0, 4) != commodityCode) {
                alertmsg("red", "CommodityCode '{0}' is invalid. Expected value is '{1} (calculated from schedule-b)'.".format(commodityCode, scheduleB.substring(0, 4)), -1);
                return false;
            }
        }
        return true;
    }

    var validateReorderPointUpdatedOn = function (data) {
        //for (var i = 0; i < data.length; i++) {
        //    var reorderPoint = data[i]["Reorder Point"];
        //    var reorderPointUpdatedOn = data[i]["Reorder Point Last Updated Date"];

        //    if (reorderPoint && !reorderPointUpdatedOn) {
        //        alertmsg("red", "Reorder Point Last Updated Date cannot be blank when Reorder Point exists.".format(reorderPointUpdatedOn, reorderPoint));
        //        return false;
        //    }
        //}
        return true;
    }

    var validateTargetQuantityUpdatedOn = function (data) {
        //for (var i = 0; i < data.length; i++) {
        //    var targetQuantity = data[i]["Target Quantity"];
        //    var targetQuantityUpdatedOn = data[i]["Target Quantity Last Updated Date"];

        //    if (targetQuantity && !targetQuantityUpdatedOn) {
        //        alertmsg("red", "Target Quantity Last Updated Date cannot be blank when Target Quantity exists.".format(targetQuantityUpdatedOn, targetQuantity));
        //        return false;
        //    }
        //}
        return true;
    }

    $('#btn-save').click(function () {

        //SGloading("Validating");
        //var data = hot.getSettings().data;
        var data = hot.getSettings().data;
        udhXml='<UDHProductUpdate>';
      

        //isValid = validateYahooIdBulk(data);
        //if (!isValid)
        //    return false;

        var productUrlRowValue = hot.getDataAtRowProp(0, "product-url");
        var yahooIdRowValue = hot.getDataAtRowProp(0, "YahooId");
        var code = hot.getDataAtRowProp(0, "code");
    
        var caseQty = hot.getDataAtRowProp(0, "case-qty");
        var name = hot.getDataAtRowProp(0, "Name");
       
            for (i = 0; i < data.length; i++) {
                var platformCellValue = (hot.getDataAtRowProp(i, "PlatformQualifier") !== "" && hot.getDataAtRowProp(i, "PlatformQualifier") !== null && hot.getDataAtRowProp(i, "PlatformQualifier") !== undefined) ? hot.getDataAtRowProp(i, "PlatformQualifier").toLowerCase() : null;
                var yahooIdCellValue = hot.getDataAtRowProp(i, "YahooId") !== null ? hot.getDataAtRowProp(i, "YahooId"):"";
                isValid = validateYahooIdBulk(yahooIdCellValue, platformCellValue);
                if (!isValid)
                    return false;
            }
        
        if (caseQty !== null)
        {
            for (i = 0; i < data.length; i++) {
                var cQ = hot.getDataAtRowProp(i, "case-qty");
                if (cQ <= 0) {
                    alertmsg('red', i + 1 + 'Case Quantity should be greater than or equals to 1', -1);
                    return;
                }
            }
        }
        var codeArray = [];

        if (code != null) {
            var dataCopy = data;
            for (i = 0; i < data.length; i++) {
                var codeCellValue = hot.getDataAtRowProp(i, "code");
                if (codeCellValue == "") {
                    alertmsg('red', i + 1 + '. The field \'code\' is required', -1);
                    return;
                }
                if (validateCode(codeCellValue) == false) {
                    alertmsg('red', i + 1 + '. Invalid code', -1);
                    return;
                }

                codeArray.push(codeCellValue);
            }
            var z = 0;
            while (codeArray.length > 1) {

                var persistedCode = codeArray[0];
                codeArray.splice(0, 1);
                if (codeArray.length > 0 && codeArray.includes(persistedCode)) {
                    alertmsg('red', z + 1 + '. Code ' + persistedCode + ' must be unique.', -1);
                    return;
                }
                z++;
            }

        }


        var nameArray = [];
        if (name != null) {

            var dataCopy = data;
            for (i = 0; i < data.length; i++) {
                var nameCellValue = hot.getDataAtRowProp(i, "Name");
                if (nameCellValue == "") {
                    alertmsg('red', i + 1 + '. The field \'Name\' is required', -1);
                    return;
                }
                

                nameArray.push(nameCellValue);
            }
            var z = 0;
            while (nameArray.length > 1) {

                var persistedName = nameArray[0];
                nameArray.splice(0, 1);
                if (nameArray.length > 0 && nameArray.includes(persistedName)) {
                    var sku = hot.getDataAtRowProp(z, "LocalSku");;
                    alertmsg('red', z + 1 + '. Name  must be unique for '+sku , -1);
                    return;
                }
                z++;
            }
        }

       


        if (productUrlRowValue != null && yahooIdRowValue != null) {
            for (i = 0; i < data.length; i++) {
                var productUrlCellValue = hot.getDataAtRowProp(i, "product-url").toLowerCase();
                var yahooIdCellValue = hot.getDataAtRowProp(i, "YahooId");
               // var MobileLinkCellValue = hot.getDataAtRowProp(i, "MobileLink").toLowerCase();
                var imageUrlCellValue = hot.getDataAtRowProp(i, "Image URL");
              
                if (imageUrlCellValue === "") {
                    hot.setDataAtRowProp(i, "Image URL", "https://sep.yimg.com/ty/cdn/stylespilotshop/image_coming_soon.jpg");
                }
                if (yahooIdCellValue !== "") {
                    var ExpectedProductValue = "https://www.skygeek.com/{0}.html".format(yahooIdCellValue).toLowerCase();

                    if (productUrlCellValue != ExpectedProductValue) {

                        flag = true;
                        alertmsg('red', i + 1 + '. Product-url \'' + productUrlCellValue + '\' is invalid. Expected value is \'' + ExpectedProductValue + '\' (calculated from YahooId)', -1);
                        return;
                    }
                    var platformCellValue = (hot.getDataAtRowProp(i, "PlatformQualifier") !== "" && hot.getDataAtRowProp(i, "PlatformQualifier") !== null && hot.getDataAtRowProp(i, "PlatformQualifier") !== undefined) ? hot.getDataAtRowProp(i, "PlatformQualifier").toLowerCase() : null;
                    isValid = validateYahooIdBulk(yahooIdCellValue, platformCellValue);
                    if (!isValid)
                            return false;
                }

                //if (yahooIdCellValue !== "") {
                //    var ExpectedMobileUrlValue = "http://m.skygeek.com/{0}.html".format(yahooIdCellValue).toLowerCase();

                //    if (ExpectedMobileUrlValue != MobileLinkCellValue) {

                //        flag = true;
                //        alertmsg('red', i + 1 + '. Mobile Link \'' + MobileLinkCellValue + '\' is invalid. Expected value is \'' + ExpectedMobileUrlValue + '\' (calculated from YahooId)', -1);
                //        return;
                //    }
                //}
               

            }
        }

        data = $.grep(data, function (value) {
            delete value.CostPerLot;
            delete value.LotDescription;
            delete value.LotPartNumber;
            delete value.QuantityPerLot;
            
            $.each(value, function (key, value1) {
                if (value1 !== " " && value1 !==null && value1 !== "" && key !== "FulfillmentCenter") {
                    value[key] = removeLastComma(value1.toString().replace(/\n/g, " ").replace(/\r/g, " ").replace(/\t/g, " ").replace(/  +/g, ' '));
                }
                return value;

            });

            return value;
        });

        //if (hasItemPointsColumns(hot.getSettings().columns)) {
        //    var isValid = validateItemPoints(data);
        //    if (!isValid)
        //        return;
        //}

        if (hasCommodityCodeColumns(hot.getSettings().columns)) {
            var isValid = validateCommodityCode(data);
            if (!isValid)
                return;
        }

        if (hasReorderPointUpdatedOnColumns(hot.getSettings().columns)) {
            var isValid = validateReorderPointUpdatedOn(data);
            if (!isValid)
                return;
        }

        if (hasTargetQuantityUpdatedOnColumns(hot.getSettings().columns)) {
            var isValid = validateTargetQuantityUpdatedOn(data);
            if (!isValid)
                return;
        }

        //console.log(data);
        debugger;

     
        var stringData = JSON.stringify(data);
        
     /*   var stringData = '[';
        for (var i = 0; i < data.length; i++) {

            stringData = stringData + stringifyJSON(data[i]) + ',';
        }

        stringData = stringData.slice(0, -1);
        stringData = stringData + ']';

*/
       
        if (stringData.indexOf("ProductId") < 0) {

            for (var x in data) {
                var propertyNameValue = [];
                if (typeof data[x] == 'object') {
                    var dataObject = data[x];
                    for (var eachPropertyName in dataObject) {
                        if (eachPropertyName.toLowerCase() !== "localsku" && eachPropertyName.toLowerCase() !== "local part number") {

                            //var dataValue = decodeHtml(dataObject[eachPropertyName].toString());
                            var dataValue = "";
                            if (dataObject[eachPropertyName]) {
                                dataValue = decodeHtml(dataObject[eachPropertyName].toString());
                            }

                            propertyNameValue.push([
                                //eachPropertyName.replace(/\r/g, ""), dataObject[eachPropertyName].toString().replace(/\r/g, "")
                                eachPropertyName.replace(/\r/g, ""), dataValue
                            ]);

                            // remove property from object
                            delete dataObject[eachPropertyName];
                        }
                    }
                    dataObject["ProductId"] = '';
                    dataObject["IsKit"] = '';
                    for (var i = 0; i < propertyNameValue.length; i++) {
                        dataObject[propertyNameValue[i][0]] = propertyNameValue[i][1];
                        dataObject[propertyNameValue[i][0] + "_name"] = propertyNameValue[i][0];
                        dataObject[propertyNameValue[i][0] + "_id"] = '';
                    }
                }
            }

        }


        // var xml = '<UDHProductUpdate>';
        for (var i = 0; i < data.length; i++) {
            udhXml = ConvertToXML(data[i], i + 1);
        }

        udhXml += '</UDHProductUpdate>'

        //make sure grid has allowed custom fields only
        var allCustomFieldAlias = ",";
        $('#CustomFields option').each(function (i, v) {
            allCustomFieldAlias += "," + Encoder.htmlEncode(v.innerHTML) + ",";
        });
        if (allCustomFieldAlias.indexOf("," + 'Hazmat Type' + ",") < 0) {
            allCustomFieldAlias += "," + 'Hazmat Type' + ",";
        }

        var allCustomFieldAliasToLowerCase = allCustomFieldAlias.toLowerCase();
        var columns = hot.getSettings().columns;
        var gridColumnsList = ",";
        for (var i = 0; i < columns.length; i++) {
            var thisTitle = columns[i].title;
            thisTitle = Encoder.htmlEncode(thisTitle);
            gridColumnsList += "," + thisTitle + ",";
            if (thisTitle !== "LocalSku" && thisTitle !== "Local Part Number" && allCustomFieldAliasToLowerCase.indexOf("," + thisTitle.toLowerCase().toString() + ",") < 0) {
                alertmsg("red", "Custom field '{0}' is not in the list of allowed custom fields for update".format(thisTitle), -1);
                return;
            }
        }


        //make sure localsku is there
        if (gridColumnsList.indexOf(',LocalSku,') < 0 && gridColumnsList.indexOf(',Local Part Number,') < 0) {
            alertmsg('red', 'Local Part Number should be compulsory for Bulk Item update', -1);
            return;
        }

        //stringData = JSON.stringify(data);


        //var dataxml = '<UDHProductUpdate><Product LocalSKU="TESTINGGGG" ProductId="193922" IsKit="0" SNo="1"><CustomField name="inset" alias="inset" id="29"><![CDATA[]]></CustomField><CustomField name="inset_1" alias="inset-1" id="30"><![CDATA[2]]></CustomField><CustomField name="inset_2" alias="inset-2" id="31"><![CDATA[3]]></CustomField><CustomField name="inset_3" alias="inset-3" id="32"><![CDATA[4]]></CustomField><CustomField name="inset_4" alias="inset-4" id="33"><![CDATA[5]]></CustomField><CustomField name="inset_5" alias="inset-5" id="34"><![CDATA[]]></CustomField></Product></UDHProductUpdate>'

        //console.log(stringData);
        console.log(udhXml);
        $.ajax({
            type: 'POST',
            url: urlsetCustomFieldUpdate,
            dataType: "json",
            data: {
                'jsonData': udhXml
            },
            beforeSend: function () {
                SGloading('Loading');
            },
            success: function (response) {

                if (response == null) {
                    alertmsg('red', msgErrorOccured, -1);
                    return;
                }

                if (response.Status !== true) {
                    alertmsg('red', response.Message, -1);
                    return;
                }

                if (response.Status === true) {
                    alertmsg('green', response.Message, -1);
                }
            },
            complete: function () {
                SGloadingRemove();
            },
            error: function (xhr, status, error) {
                alert(status + ' ' + error);
            }
        });

    });

});

function validateYahooIdBulk(yahooId,platformQualifier) {
   // var yahooId = hot.getDataAtRowProp(0, "YahooId");
  //  var platformQualifier = (hot.getDataAtRowProp(0, "PlatformQualifier") !== "" && hot.getDataAtRowProp(0, "PlatformQualifier") !== null && hot.getDataAtRowProp(0, "PlatformQualifier") !== undefined) ? hot.getDataAtRowProp(0, "PlatformQualifier").toLowerCase() : null;


    //var yahooId = $('input[data-name="YahooId"],textarea[data-name="YahooId"]').val();
    //var platformQualifier = $('#PlatformQualifier').val();
    if (typeof platformQualifier !== 'undefined' && platformQualifier !== null && platformQualifier.indexOf('yahoo') >= 0
        && typeof yahooId !== 'undefined' && yahooId !== null && yahooId === "") {
        var fieldAlias = "YahooId";//$('input[data-name="YahooId"],textarea[data-name="YahooId"]').parent().find('label').text();
        alertmsg("red", "{0} cannot be blank when {1} is a PlatformQualifier".format(fieldAlias, "Yahoo"));
        return false;
    }

    var regexp = /^[a-zA-Z0-9-]+$/;
    var check = yahooId;
    if (check != null && check != '' && check.search(regexp) == -1) {
        var fieldAliasRegex = "YahooId";//$('input[data-name="YahooId"],textarea[data-name="YahooId"]').parent().find('label').text();
        alertmsg("red", "{0} contains invalid characters".format(fieldAliasRegex, "Yahoo"));
        return false;
    }

    return true;
}
function validateCode(code) {
    var reg = /^[a-zA-Z0-9_-]*$/;
    return reg.test(code);
}

function removeLastComma(val) {
    if (val) {
        val = val.toString().replace(/[, ]+\s*$/, "");
        return val;
    }
    return val;
}

function GetCustomFieldId(name) {
    var tempValue;
    $.each(customFieldForMapping, function (i, e) {
        if (e.Text.toLowerCase() === name.toLocaleLowerCase()) {
            tempValue = e.Value;
            return false;
        }
    });

    return tempValue;

}

function notifyAddtionOfRelatedField(addedCustomField, customFieldAlias) {
    for (var i = 0; i < addedCustomField.length; i++) {
        if (addedCustomField[i] == customFieldAlias) {
            addedCustomField.splice(i, 1);
        }
    }

    var message1 = "";
    $.each(addedCustomField, function (i, v) {
        message1 += "'" + v + "',";
    });
    message1 = message1.substring(0, message1.length - 1);

    message1 = "Following related fields have been added: " + message1;

    udh.notify.warn(message1);
}

function stringify(obj) {
    function flatten(obj) {
        if (_.isObject(obj)) {
            return _.sortBy(_.map(
                _.pairs(obj),
                function (p) { return [p[0], flatten(p[1])]; }
            ),
                function (p) { return p[0]; }
            );
        }
        return obj;
    }
    return JSON.stringify(flatten(obj));
}


function customDropdownRenderer(instance, td, row, col, prop, value, cellProperties) {
    if (platformOptionList.length > 0) {
        optionsList = platformOptionList;
    }
    value = value.toLowerCase();
    var concatenatedvalue = '';
    var lstofValue = value.split(',');
    if (lstofValue.length > 1) {
        for (var i = 0; i < lstofValue.length; i++) {
            if (optionsList.includes(lstofValue[i].toLowerCase())) {
                if (concatenatedvalue.indexOf(lstofValue[i]) === -1) {

                    if (concatenatedvalue === '') {
                        concatenatedvalue = lstofValue[i];
                    }
                    else {
                        concatenatedvalue = concatenatedvalue + ',' + lstofValue[i];
                    }
                }
            }
        }
        value = concatenatedvalue.toString().trim();
    }

    else {
        if (!optionsList.includes(value.toLowerCase())) { value = ""; }
    }
    Handsontable.cellTypes.text.renderer.apply(this, arguments);
}



function stringifyJSON(obj) {
    keys = [];
    fixedKeys = [];
    i = 0;
    if (obj) {
        for (var key in obj) {
            if (i < 3) {
                fixedKeys.push(key);
                i++;
            }
            else
                keys.push(key);
        }
    }
    keys.sort();

    for (var l = 0; l < keys.length; l++) {

        fixedKeys.push(keys[l]);
    }

    var tObj = {};
    var key;
    for (var index in fixedKeys) {
        key = fixedKeys[index];
        tObj[key] = obj[key];
    }
    return JSON.stringify(tObj);
}

function ConvertToXML(obj, loopCount,xml) {

    dynamicKeys = [];
    fixedKeys = [];
    i = 0;
    if (obj) {
        for (var key in obj) {
            if (i < 3) {
                fixedKeys.push(key);
                i++;
            }
            else
                dynamicKeys.push(key);
        }
    }

    udhXml += '<Product LocalSKU="{1}" ProductId="{2}" IsKit="{3}" SNo="{0}">'.format(loopCount, obj[fixedKeys[0]], obj[fixedKeys[1]], obj[fixedKeys[2]]);

    for (var i = 0; i < dynamicKeys.length; i++) {
        if (dynamicKeys[i].indexOf('_name') < 0 && dynamicKeys[i].indexOf('_id') < 0) {
            var customFieldValue = encodeHTML(obj[dynamicKeys[i]]);
            var customFieldName = encodeHTML(obj[dynamicKeys[i] + '_name']);
            var customFieldId = obj[dynamicKeys[i] + '_id'];

            if (customFieldValue == undefined) { customFieldValue = ''; }
            if (customFieldName == undefined) { customFieldName = ''; }
            if (customFieldId == undefined) { customFieldId = ''; }

            udhXml += '<CustomField name="{0}" alias="{1}" id="{2}"><![CDATA[{3}]]></CustomField>'.format(customFieldName, encodeHTML(dynamicKeys[i]), customFieldId, customFieldValue);
        }
    }


    udhXml += '</Product>';
    return udhXml;

   

}

//function encodeHTML(str) {
//    return str.replace(/[\u00A0-\u9999<>&](?!#)/gim, function (i) {
//        return '&#' + i.charCodeAt(0) + ';';
//    });
//}


var escapeChars = {
    '¢': 'cent',
    '£': 'pound',
    '¥': 'yen',
    '€': 'euro',
    '©': 'copy',
    '®': 'reg',
    '<': 'lt',
    '>': 'gt',
    '"': 'quot',
    '&': 'amp',
    '\'': '#39'
};

var regexString = '[';
for (var key in escapeChars) {
    regexString += key;
}
regexString += ']';

var regex = new RegExp(regexString, 'g');

function encodeHTML(str) {
    if (str) {
        return str.replace(regex, function (m) {
            return '&' + escapeChars[m] + ';';
        });
    }
    else
        return str;
};

function initialize() {
  // popup window height
   // var ht = $('#hot-custom-field-update').handsontable('getInstance'); // click on the dropdown triangle button
    var ht = hot.getInstance();
    var sgtabHeight = $('.sg-tab-bar').height() + 0;
    var panelHeight = $('.sg-grid-panel').height() + 0;
    var headerHeight = $('.fixedbar.header').height() + 0;
    var footerHeight = $('.fixedbar.footer').height() + 0;
    var filterdiv = $('.search-holder.top').height() + 0;
    var elmHeight = sgtabHeight + panelHeight + headerHeight + footerHeight;
    var htTypeHeight = elmHeight + filterdiv; 

    ht.addHook('afterOnCellMouseDown', function (event, coords, TD)
    { // Header 
        if (coords.row == -2)
        {
            // .......ToDo };
        } else if (coords.row >= 0) {
           // is a cell is not a header
            var myoffsetTop = TD.offsetTop; // offset: depend by scroll position of the cell in the grid 
            var myCellHeight = TD.clientHeight; // cell height 
            MoveHandTypeCellWindow(myoffsetTop, myCellHeight);
        }
    }); // In case of Return or F2 on the cell 
    ht.addHook('afterBeginEditing', function (row, column) {
        var plugin = ht.getPlugin('AutoRowSize'); // get first visible row e cell height 
        var firstVisRow = plugin.getFirstVisibleRow();
        var myCellHeight = ht.getRowHeight(row); // offset: 90 in my button bar, 130 the header 1 height, I add also the header2 height (+ myCellHeight) // (row - firstVisRow) * myCellHeight): height in pixel of the visible rows 
        var myoffsetTop = elmHeight + filterdiv + myCellHeight + ((row - firstVisRow) * myCellHeight)
        MoveHandTypeCellWindow(myoffsetTop, myCellHeight);

    });
    function MoveHandTypeCellWindow(myoffsetTop, myCellHeight)
    { // dim local 
        // var myHeight = window.innerHeight - 90 - 10; // -90 header height -10 gap to be more sure 
        var myHeight = window.innerHeight - elmHeight - filterdiv - 160;
        var myNewTop = 0; // new start position of the window popup // if the popup window is not all visible 
        if (myoffsetTop + htTypeHeight > myHeight) {
            myNewTop = -htTypeHeight - myCellHeight - 105; // negative value so the start position of the popup is above and is egual to the window height - 5 gap to be more sure. 
        }
        else {
            myNewTop = 1;
        }// take rule .handsontable .listbox from the css
        var mysheet = document.styleSheets[0];
        var myrules = mysheet.cssRules ? mysheet.cssRules : mysheet.rules; // loop on all the css rules 
        for (i = 0; i < myrules.length; i++) {
            if (myrules[i].selectorText == ".handsontable .listbox") {
                // find 
                myrules[i].style.top = myNewTop + "px"; // change the top break;
            }
        }
    }

}







