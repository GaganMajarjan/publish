﻿$('#btnCreateAclUser').click(function () {
    window.location = urlCreate;
});

$('#selectAllCheckBox').click(function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.selectOne').prop("checked", true);
    } else {
        $('.selectOne').prop("checked", false);
    }
});

$('#btnEditAclUser').click(function () {
    var i = 0;
    var checkboxIds = "";
    $('input[type=checkbox][class=selectOne]').each(function () {

        if (this.checked) {
            i++;
            checkboxIds = checkboxIds + this.value;
        }
    });

    if (i == 0)
        alertmsg("red", "Please select one to edit.");
    else if (i > 1)
        alertmsg("red", "Please select only one to edit.");
    else {
        window.location = urlEdit + "/" + checkboxIds;
    }
});

$('#btnDeleteAclUser').click(function () {
    var i = 0;
    var checkboxIds = "";
    $('input[type=checkbox][class=selectOne]').each(function () {

        if (this.checked) {
            i++;
            checkboxIds = checkboxIds + this.value + ",";
        }
    });

    if (i == 0)
        alertmsg("red", "Please select one to delete.");

    else {
        $("#idContainerHiddenFieldForDelete").val(checkboxIds);
        $('#divConfirmDelete').modal("show");
    }
});

$('#divConfirmDeleteYes').click(function () {
    var IdsCommaSeparated = $("#idContainerHiddenFieldForDelete").val();

    $.ajax({
        url: urlDelete,
        data: {
            deleteIdsCommaSeparated: IdsCommaSeparated
        },
        type: 'POST',
        cache: false,
        success: function (data) {
            if (data == "OK") {
                location.reload();
                alertmsg("green", "Access Control User deleted successfully.");
            }
            else {

                alertmsg("red", "An Error occured while deleting. Please delete user-module mappings first.");
            }
        }
    });

});

$('#aclUserDataTable').dataTable({
    "deferRender": true,
    "bServerSide": true,
    "aLengthMenu": dataTableMenuLength,
    "iDisplayLength": dataTableDisplayLength,
    "destroy": true,
    "bSort": false,
    "scrollX": true,
    "sAjaxSource": urlGetDataTable,
    "aoColumns": [
                    {

                        "bSearchable": false,
                        "bSortable": false,
                        "mRender": function (data, type, full) {
                            return '&nbsp;&nbsp;&nbsp;<input type="checkbox" id="selectOneCheckBox" class="selectOne" value="' + data + '">'

                        }
                    },
                    {

                        "bSortable": false,
                        "sName": "FullName"
                    },
                    {
                        "bSortable": false,
                        "sName": "UserName"
                    },
                    {

                        "bSortable": false,
                        "sName": "Name"
                    },
                    {
                        "bSortable": false,
                        "sName": "Status"
                    }


    ],
    "fnPreDrawCallback": function () {
        // gather info to compose a message
        var ob = this;
        SGtableloading({ class: 'fa-plane', message: 'Processing...', objct: ob });
        //SGtableloading('fa-plane', '', this);


    },
    "fnDrawCallback": function () {
        // in case your overlay needs to be put away automatically you can put it here
        var divid = this.closest('.dataTables_wrapper').attr('id');
        SGtableloadingRemove(divid)
    }
});