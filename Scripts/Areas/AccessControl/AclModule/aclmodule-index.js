﻿$('#btnCreateAclModule').click(function () {
    window.location = urlCreate;
});

$('#selectAllCheckBox').click(function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.selectOne').prop("checked", true);
    } else {
        $('.selectOne').prop("checked", false);
    }
});

$('#btnEditAclModule').click(function() {
    var i = 0;
    var checkboxIds = "";
    $('input[type=checkbox][class=selectOne]').each(function () {

        if (this.checked) {
            i++;
            checkboxIds = checkboxIds + this.value;
        }
    });

    if (i == 0)
        alertmsg("red", "Please select one to edit.");
    else if(i > 1)
        alertmsg("red", "Please select only one to edit.");
    else {
        window.location = urlEdit + "/" + checkboxIds;
    }
});

$('#btnDeleteAclModule').click(function() {
    var i = 0;
    var checkboxIds = "";
    $('input[type=checkbox][class=selectOne]').each(function() {

        if (this.checked) {
            i++;
            checkboxIds = checkboxIds + this.value + ",";
        }
    });

    if (i == 0)
        alertmsg("red", "Please select one to delete.");

    else {
        $("#idContainerHiddenFieldForDelete").val(checkboxIds);
        $('#divConfirmDelete').modal("show");
    }
});

$('#divConfirmDeleteYes').click(function() {
    var IdsCommaSeparated = $("#idContainerHiddenFieldForDelete").val();

    $.ajax({
        url: urlDelete,
        data: {
            deleteIdsCommaSeparated: IdsCommaSeparated
        },
        type: 'POST',
        cache: false,
        success: function (data) {
            if (data == "OK") {
                location.reload();
                alertmsg("green", "Access Control Module deleted successfully.");
            }
            else {
                
                alertmsg("red", "An Error occured while deleting. Please delete module mappings first.");
            }
        }
    });

});

$('#aclModulesDataTable').dataTable({
    "deferRender": true,
    "bServerSide": true,
    "aLengthMenu": dataTableMenuLength,
    "iDisplayLength": dataTableDisplayLength,
    "destroy": true,
    "sAjaxSource": urlGetDataTableForModules,
    "bProcessing": true,
    "aoColumns": [
                    {

                        "bSearchable": false,
                        "bSortable": false,
                        "mRender": function (data, type, full) {
                            return '&nbsp;&nbsp;&nbsp;<input type="checkbox" id="selectOneCheckBox" class="selectOne" value="' + data + '">'
                            
                        }
                    },
                    {

                        "bSortable": false,
                        "sName": "ModuleName"
                    },
                    {

                        "bSortable": false,
                        "sName": "AreaName"
                    },
                    {

                        "bSortable": false,
                        "sName": "ControllerName"
                    },
                    {

                        "bSortable": false,
                        "sName": "ActionName"
                    },
                    {

                        "bSortable": false,
                        "sName": "ModuleAccessCode"
                    }
                    

    ]
});

