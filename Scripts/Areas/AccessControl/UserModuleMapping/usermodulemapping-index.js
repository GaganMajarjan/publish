﻿// Search Click
$('#GBDUserSearchButton').click(function () {

    var selectClient = document.getElementById("userCollection");
    var client = selectClient.options[selectClient.selectedIndex];

    var e = document.getElementById("userCollection");
    var userID = e.options[e.selectedIndex].value;


    $.ajax({
        url: urlGetUserModuleMappingData,
        type: 'GET',
        data: { userID: userID },
        dataType: "json",
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Loading');
        },
        success: function (result) {
            if (result == "NO_DATA") {
                alertmsg("red", "No mapping exists. Please assign features for the roles first.");
                //$('#btnSubmit').show();
                return;
            }
            
            result.forEach(function (module) {

                module.checked = module.Checked;

                if (module.children != null) {
                    module.children.forEach(function(area) {
                        area.checked = area.Checked;

                        if (area.children != null) {
                            area.children.forEach(function(controller) {

                                controller.checked = controller.Checked;
                                if (controller.children != null) {
                                    controller.children.forEach(function(action) {

                                        action.checked = action.Checked;
                                        delete action.Checked;
                                    });
                                    delete controller.Checked;
                                }
                            });
                            delete area.Checked;
                        }
                    });
                }

                delete module.Checked;
            });

            //console.log(result);

            $('#userModuleMappingTree').tree({
                data: result,
                checkbox: true,
                onlyLeafCheck: false,
                lines: true,
                animate: true
            });

            $('#btnSubmit').show();
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (err) {
            alert('Error');
        }
    });
});


// Save Click
$('#GBDUserSaveButton').click(function () {
    getCheckedAndIndeterminate();
});

function getCheckedAndIndeterminate() {

    var e = document.getElementById("userCollection");
    var userID = e.options[e.selectedIndex].value;


    var nodes = $('#userModuleMappingTree').tree('getChecked', 'checked');
    var moduleIdCheckedCommaSeparated = '';
    for (var i = 0; i < nodes.length; i++) {
        if (moduleIdCheckedCommaSeparated != '') moduleIdCheckedCommaSeparated += ',';
        moduleIdCheckedCommaSeparated += nodes[i].attributes.Id;
    }

    var indeterminateNodes = $('#userModuleMappingTree').tree('getChecked', 'indeterminate');
    var moduleIdIndeterminateCommaSeparated = '';
    for (var j = 0; j < indeterminateNodes.length; j++) {
        if (moduleIdIndeterminateCommaSeparated != '') moduleIdIndeterminateCommaSeparated += ',';
        moduleIdIndeterminateCommaSeparated += indeterminateNodes[j].attributes.Id;
    }

    
    $.ajax({
        url: urlSaveCheckedUserModuleMappingData,
        type: 'POST',
        cache: false,
        data: { moduleIdCheckedCommaSeparated: moduleIdCheckedCommaSeparated, moduleIdIndeterminateCommaSeparated: moduleIdIndeterminateCommaSeparated,userIdSpecified:userID },

        dataType: "json",
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Saving');
        },
        success: function (result) {
            var statusMessage = result;

            if (statusMessage == "PARAM_NULL")
                alertmsg("red", "No module selected for saving. Please selected at least one. ");

            else if (statusMessage == "OK") {
                alertmsg("green", "Successfully saved mappings for the user.");
            } else {
                alertmsg("red", "Error occured while saving mappings for the user.");
            }
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (err) {
            alertmsg("red", "Server error occured while saving mappings for the user.");
        }
    });


}

$('#gbdRevokeUserMappings').click(function() {
    
    var userId = $('#userCollection').val();
    

    $.ajax({
        url: urlRevokeUserMappings,
        type: 'POST',
        cache: false,
        data: { userId: userId},

        dataType: "json",
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Deleting');
        },
        success: function (response) {

            if (response != null && response.Status === true) {
                location.reload();
                var message = response.Message;
                if (message.indexOf('{0}') > -1) {

                    var selectedUser = $("#userCollection option:selected").text();
                    message = message.format(selectedUser);
                }

                alertmsg("green", message);
            }
            else if (response != null && response.Status === false) {
                alertmsg("red", response.Message);
            } else {
                alertmsg("red", errorOccurredMessage);

            }
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (err) {
            location.reload();
            alertmsg("red", "Server error occured while revoking mappings for the user.");
        }
    });
});
