﻿$('#btnCreateAclClient').click(function () {
    window.location = urlCreate;
});

$('#selectAllCheckBox').click(function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('.selectOne').prop("checked", true);
    } else {
        $('.selectOne').prop("checked", false);
    }
});

$('#btnEditAclClient').click(function () {
    var i = 0;
    var checkboxIds = "";
    $('input[type=checkbox][class=selectOne]').each(function () {

        if (this.checked) {
            i++;
            checkboxIds = checkboxIds + this.value;
        }
    });

    if (i == 0)
        alertmsg("red", "Please select one to edit.");
    else if (i > 1)
        alertmsg("red", "Please select only one to edit.");
    else {
        window.location = urlEdit + "/" + checkboxIds;
    }
});

$('#btnDeleteAclClient').click(function () {
    var i = 0;
    var checkboxIds = "";
    $('input[type=checkbox][class=selectOne]').each(function () {

        if (this.checked) {
            i++;
            checkboxIds = checkboxIds + this.value + ",";
        }
    });

    if (i == 0)
        alertmsg("red", "Please select one to delete.");

    else {
        $("#idContainerHiddenFieldForDelete").val(checkboxIds);
        $('#divConfirmDelete').modal("show");
    }
});

$('#divConfirmDeleteYes').click(function (e) {
    var IdsCommaSeparated = $("#idContainerHiddenFieldForDelete").val();
    //console.log(IdsCommaSeparated);
    //console.log(urlDelete);
    $.ajax({
        url: urlDelete,
        data: {deleteIdsCommaSeparated: IdsCommaSeparated},
        type: 'POST',
        cache: false,
        dataType: "json",
        success: function (data) {
            if (data == "OK") {
                location.reload();
                alertmsg("green", "Access Control Client deleted successfully.");
            }
            else {
                //console.log(data);
                alertmsg("red", "An Error occured while deleting. Please delete users and client-module mappings first.");
            }
        }
    });

});
$('#aclClientDataTable').dataTable({
    "deferRender": true,
    "bServerSide": true,
    "aLengthMenu": dataTableMenuLength,
    "iDisplayLength": dataTableDisplayLength,
    "destroy": true,
    "scrollX": true,
    "sAjaxSource": urlGetDataTable,
    "bProcessing": true,
    "aoColumns": [
                    {

                        "bSearchable": false,
                        "bSortable": false,
                        "mRender": function (data, type, full) {
                            return '&nbsp;&nbsp;&nbsp;<input type="checkbox" id="selectOneCheckBox" class="selectOne" value="' + data + '">'

                        }
                    },
                    {

                        "bSortable": false,
                        "sName": "Name"
                    },
                    {

                        "bSortable": false,
                        "sName": "Description"
                    }


    ]
});
