﻿//  Search Click


$('#GBDSearchButton').click(function () {
            
    var selectClient = document.getElementById("ClientCollection");
    var client = selectClient.options[selectClient.selectedIndex];

    var e = document.getElementById("ClientCollection");
    var clientID = e.options[e.selectedIndex].value;


    $.ajax({
        url: urlGetClientModuleMappingData,
        type: 'GET',
        data: {clientID:clientID},
        dataType: "json",
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Loading');
        },
        success: function(result) {

            if (result == "NO_DATA") {
                alertmsg("red", "No module exists in the application, please add modules first.");
                return;
            }
            result.forEach(function (module) {

                module.checked = module.Checked;
                module.children.forEach(function (area)
                {
                    area.checked = area.Checked;
                    area.children.forEach(function (controller) {

                        controller.checked = controller.Checked;
                        controller.children.forEach(function (action) {

                            action.checked = action.Checked;
                            delete action.Checked;
                        });
                        delete controller.Checked;
                    });
                    delete area.Checked;
                });

                delete module.Checked;
            });

           // console.log(JSON.stringify(result));

            $('#clientModuleMappingTree').tree({
                data: result,
                checkbox: true,
                onlyLeafCheck: false,
                lines: true,
                animate: true
                
            });

            $('#btnSubmit').show();
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function(err) {
            alert('Error');
        }
    });
});


//  Save Click

$('#GBDSaveButton').click(function() {
    saveCheckedAndIndeterminate();
});

function saveCheckedAndIndeterminate() {
    var e = document.getElementById("ClientCollection");
    var clientID = e.options[e.selectedIndex].value;

    var nodes = $('#clientModuleMappingTree').tree('getChecked', 'checked');
    var moduleIdCheckedCommaSeparated = '';
    for (var i = 0; i < nodes.length; i++) {
        if (moduleIdCheckedCommaSeparated != '') moduleIdCheckedCommaSeparated += ',';
        moduleIdCheckedCommaSeparated += nodes[i].attributes.Id;
    }
    
    var indeterminateNodes = $('#clientModuleMappingTree').tree('getChecked', 'indeterminate');
    var moduleIdIndeterminateCommaSeparated = '';
    for (var j = 0; j < indeterminateNodes.length; j++) {
        if (moduleIdIndeterminateCommaSeparated != '') moduleIdIndeterminateCommaSeparated += ',';
        moduleIdIndeterminateCommaSeparated += indeterminateNodes[j].attributes.Id;
    }

    var uncheckedNodes = $('#clientModuleMappingTree').tree('getChecked', 'unchecked');
    var moduleIdUncheckedCommaSeparated = '';
    for (var j = 0; j < uncheckedNodes.length; j++) {
        if (moduleIdUncheckedCommaSeparated != '') moduleIdUncheckedCommaSeparated += ',';
        moduleIdUncheckedCommaSeparated += uncheckedNodes[j].attributes.Id;
    }


    $.ajax({
        url: urlSaveCheckedClientModuleMappingData,
        type: 'POST',
        cache: false,
        data: { moduleIdCheckedCommaSeparated: moduleIdCheckedCommaSeparated, moduleIdIndeterminateCommaSeparated: moduleIdIndeterminateCommaSeparated,moduleIdUncheckedCommaSeparated:moduleIdUncheckedCommaSeparated, clientIdSpecified: clientID },

        dataType: "json",
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Saving');
        },
        success: function(result) {
            var statusMessage = result;
            //console.log(status);
            if (statusMessage == "PARAM_NULL")
                alertmsg("red", "No module selected for saving. Please selected at least one. ");

           else if (statusMessage == "OK") {
                alertmsg("green","Successfully saved mappings for the client.");
            } else {
                alertmsg("red","Error occured while saving mappings for the client.");
            }
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function(err) {
            alertmsg("red","Server error occured while saving mappings for the client.");
        }
    });


}
