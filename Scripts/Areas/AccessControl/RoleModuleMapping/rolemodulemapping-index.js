﻿//  Search Click

$('#GbdRoleSearchButton').click(function () {

    var selectedRole = document.getElementById("roleCollection");
    var role = selectedRole.options[selectedRole.selectedIndex];

    var e = document.getElementById("roleCollection");
    var roleId = e.options[e.selectedIndex].value;


    $.ajax({
        url: urlGetRoleModuleMappingData,
        type: 'GET',
        data: { roleIdSpecified: roleId },
        dataType: "json",
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Loading');
        },
        success: function (result) {

            if (result == "NO_DATA") {
                alertmsg("red", "No module assigned for the client, please assign module for client first.");
                return;
            }
            //console.log(result);
            result.forEach(function (module) {

                module.checked = module.Checked;

                if (module.children != null) {
                    module.children.forEach(function (area) {
                        area.checked = area.Checked;

                        if (area.children != null) {
                            area.children.forEach(function (controller) {

                                controller.checked = controller.Checked;
                                if (controller.children != null) {
                                    controller.children.forEach(function (action) {

                                        action.checked = action.Checked;
                                        delete action.Checked;
                                    });
                                    delete controller.Checked;
                                }
                            });
                            delete area.Checked;
                        }
                    });
                }

                delete module.Checked;
            });

            //console.log(result);

            $('#roleMappingTree').tree({
                data: result,
                checkbox: true,
                onlyLeafCheck: false,
                lines: true,
                animate: true
            });

            $('#btnSubmit').show();
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (err) {
            alert('Error');
        }
    });
});


//  Save Click

$('#GBDSaveButton').click(function () {
    saveCheckedAndIndeterminate();
});

function saveCheckedAndIndeterminate() {

    var e = document.getElementById("roleCollection");
    var roleId = e.options[e.selectedIndex].value;

    var nodes = $('#roleMappingTree').tree('getChecked', 'checked');
    var moduleIdCheckedCommaSeparated = '';
    for (var i = 0; i < nodes.length; i++) {
        if (moduleIdCheckedCommaSeparated != '') moduleIdCheckedCommaSeparated += ',';
        moduleIdCheckedCommaSeparated += nodes[i].attributes.Id;
    }

    var indeterminateNodes = $('#roleMappingTree').tree('getChecked', 'indeterminate');
    var moduleIdIndeterminateCommaSeparated = '';
    for (var j = 0; j < indeterminateNodes.length; j++) {
        if (moduleIdIndeterminateCommaSeparated != '') moduleIdIndeterminateCommaSeparated += ',';
        moduleIdIndeterminateCommaSeparated += indeterminateNodes[j].attributes.Id;
    }

    var uncheckedNodes = $('#roleMappingTree').tree('getChecked', 'unchecked');
    var moduleIdUncheckedCommaSeparated = '';
    for (var j = 0; j < uncheckedNodes.length; j++) {
        if (moduleIdUncheckedCommaSeparated != '') moduleIdUncheckedCommaSeparated += ',';
        moduleIdUncheckedCommaSeparated += uncheckedNodes[j].attributes.Id;
    }
    
    $.ajax({
        url: urlSaveCheckedRoleModuleMappingData,
        type: 'POST',
        cache: false,
        data: { moduleIdCheckedCommaSeparated: moduleIdCheckedCommaSeparated, moduleIdIndeterminateCommaSeparated: moduleIdIndeterminateCommaSeparated, moduleIdUncheckedCommaSeparated: moduleIdUncheckedCommaSeparated, roleId: roleId },

        dataType: "json",
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Saving');
        },
        success: function (result) {
            var statusMessage = result;
            
            if (statusMessage == "PARAM_NULL")
                alertmsg("red", "No module selected for saving. Please selected at least one. ");

           else if (statusMessage == "OK") {
                alertmsg("green","Successfully saved mappings for the role.");
            } else {
                alertmsg("red","Error occured while saving mappings for the role.");
            }
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (err) {
            alertmsg("red","Server error occured while saving mappings for the role.");
        }
    });


}


$('#gbdRevokeRoleMappings').click(function () {

    var roleId = $("#roleCollection").val();


    $.ajax({
        url: urleRevokeRoleMappings,
        type: 'POST',
        cache: false,
        data: { roleId: roleId },

        dataType: "json",
        beforeSend: function () {
            //$("#btnsave").attr('disabled', true);
            SGloading('Deleting');
        },
        success: function (response) {

            if (response != null && response.Status === true) {
                location.reload();
                alertmsg("green", response.Message);
            }
            else if (response != null && response.Status === false) {
                alertmsg("red", response.Message);
            } else {
                alertmsg("red", errorOccurredMessage);

            }
            
        },
        complete: function () {
            //$("#btnsave").attr('disabled', false);
            SGloadingRemove();
        },
        error: function (err) {
            location.reload();
            alertmsg("red", "Server error occured while revoking mappings for the role.");
        }
    });
});