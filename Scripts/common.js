//the udh object
var udh = udh || {};
var globalSearchId = "";
var catalogManagerSearch = false;
var enterSearch = false;
var test = "";

//----------------------------------------------------------------------------------------------------------------------------------
//udh datatable
//----------------------------------------------------------------------------------------------------------------------------------
udh.datatable = udh.datatable || {};
udh.datatable.dataTableMenuLength = [[10, 20, 50, 100], [10, 20, 50, 100]];

udh.datatable.dataTableDisplayLength = 100;

//datable configuration
var dataTableMenuLength = [[10, 20, 50, 100], [10, 20, 50, 100]];

//var dataTableDisplayLength = 20;
var dataTableDisplayLength = 100;
var tableToolsPath = tableToolsSwfUrl;
var tableToolsOptions = ["copy", "csv"];
// end of datatable configuration

//----------------------------------------------------------------------------------------------------------------------------------
//udh constants
//----------------------------------------------------------------------------------------------------------------------------------

udh.constants = udh.constants || {};
udh.constants.mode = {
    /**
     * Values calculation in form
     */
    FORM: 1, //default

    /**
     * Values calculation in handsontable (hot)
     */
    HOT: 2,

    /**
     * Values calculation in KnockoutJs
     */
    KO: 3,

    /**
     * Value calculation in Pure javascript object
     */
    JSObj: 4
};


//----------------------------------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------------------------
//udh utilities
//----------------------------------------------------------------------------------------------------------------------------------
udh.utilities = udh.utilities || {};

function isEmptyOrSpaces(str) {
    return str === null || str === '' || typeof str === "undefined" || str.toString().match(/^ *$/) !== null;
}

udh.utilities.isEmptyOrSpaces = isEmptyOrSpaces;

udh.utilities.setValue = function (mode, target, value, additionalData) {
    switch (mode) {
        case udh.constants.mode.HOT:
            if (typeof additionalData === "undefined") {
                console.error("additionalData needs to be defined for hot.");
                return;
            }
            //e.g. target = "ColumnName";
            //e.g. additionalData = hot.getSettings.data()[rowIndex];
            //usage: hot.getSettings.data()[rowIndex]["ColumnName"] = value; --> additionalData[target] = value;
            additionalData[target] = value;
            break;

        case udh.constants.mode.KO:
            //e.g. target = self.ColumnName(); [where self.ColumnName = ko.observable();]
            //usage: self.ColumnName(value);
            target(value);
            break;

        case udh.constants.mode.JSObj:
            //e.g. target = document.getElementById("ColumnName");
            //usage: document.getElementById("ColumnName").value = value;
            target(value);
            break;

        case udh.constants.mode.FORM:
            //e.g. target = $("#ColumnName");
            //usage: $("#ColumnName").val(value);
            target.val(value);
            break;

        default:
            console.error("Invalid mode", mode);
            break;
    }
}

udh.utilities.getValue = function (mode, target, additionalData) {
    var value = "";
    switch (mode) {
        case udh.constants.mode.HOT:
            if (typeof additionalData === "undefined") {
                console.error("additionalData needs to be defined for hot.");
                return "";
            }
            //e.g. target = "ColumnName";
            //e.g. additionalData = hot.getSettings.data()[rowIndex];
            //usage: value = hot.getSettings.data()[rowIndex]["ColumnName"]; --> value = additionalData[target];
            value = additionalData[target];
            break;

        case udh.constants.mode.KO:
            //e.g. target = self.ColumnName(); [where self.ColumnName = ko.observable();]
            //usage: value = self.ColumnName();
            value = target();
            break;

        case udh.constants.mode.JSObj:
            //e.g. target = document.getElementById("ColumnName");
            //usage: value = document.getElementById("ColumnName").value;
            value = target.value;
            break;

        case udh.constants.mode.FORM:
            //e.g. target = $("#ColumnName");
            //usage: value = $("#ColumnName").val();
            value = target.val();
            break;

        default:
            console.error("Invalid mode", mode);
            break;
    }
    return value;
}


//----------------------------------------------------------------------------------------------------------------------------------
//udh calculation
//----------------------------------------------------------------------------------------------------------------------------------
udh.calculation = udh.calculation || {};

udh.calculation.FormatAmount = function (yourNumber, decimalPlaces) {
    if ($.isNumeric(yourNumber) === false)
        return 0;
    decimalPlaces = parseInt(decimalPlaces) || 4;;
    var num = parseFloat(yourNumber);
    var factor = Math.pow(10, decimalPlaces);
    yourNumber = (Math.round((num * factor).toPrecision(12)) / factor).toFixed(decimalPlaces);
    return yourNumber;
}

udh.calculation.RoundUp = function (yourNumber, decimalPlaces) {
    if ($.isNumeric(yourNumber) === false)
        return 0;
    decimalPlaces = parseInt(decimalPlaces) || 2;
    var num = parseFloat(yourNumber);
    var factor = Math.pow(10, decimalPlaces);
    yourNumber = (Math.ceil((num * factor).toPrecision(12)) / factor).toFixed(decimalPlaces);
    return yourNumber;
}

//----------------------------------------------------------------------------------------------------------------------------------
//udh settings
//----------------------------------------------------------------------------------------------------------------------------------
udh.settings = udh.settings || {};

var isSaved = true;
//var setFormSubmitting = function () { formSubmitting = true; };

//format number to 4 decimal places
var decimal_places = 4;

/* loading icon class */
var faIcon = "fa-plane";

udh.settings.isSaved = isSaved;
udh.settings.decimal_places = decimal_places;
udh.settings.faIcon = faIcon;

//udh settings for custom fields
udh.settings.customfields = udh.settings.customfields || {};
udh.settings.customfields.autocalculateItemPointsIfEmpty = true;
udh.settings.customfields.autocalculateCommodityCodeIfEmpty = true;
udh.settings.customfields.autocalculateYahooIdIfEmpty = true;
udh.settings.customfields.autocalculateProductUrlIfEmpty = true;
//udh.settings.customfields.autocalculateMobileLinkUrlIfEmpty = true;
udh.settings.customfields.replaceExistingYahooIdWithAutoCalculatedValue = false;
udh.settings.customfields.autocalculateReorderPointUpdatedOnIfEmpty = true;
udh.settings.customfields.autocalculateTargetQuantityUpdatedOnIfEmpty = true;
udh.settings.customfields.autocalculateIdentifier = true;
udh.settings.customfields.autocalculateImageUrlIfEmpty = true;


//----------------------------------------------------------------------------------------------------------------------------------
//udh utilities for custom fields 
//----------------------------------------------------------------------------------------------------------------------------------

udh.utilities.getYahooId = function (partNumber, manufacturerPartNumber, shortDescription) {
    //if (partNumber !== "" && manufacturerPartNumber !== "" && shortDescription !== "") {
    //    var yahooId = "{0}-{1}-{2}".format(manufacturerPartNumber, partNumber, shortDescription);

    //    // Yahoo ID can only be numbers, letters, and spaces.
    //    var regex = /([^A-Za-z0-9\- ]+)/g;
    //    yahooId = yahooId.replace(regex, ""); //filter out unwanted characters other than regex

    //    // Also, the generated Aabaco ID should be 70 characters or less.
    //    var allowedLengthForYahooId = 70;
    //    yahooId = yahooId.substring(0, Math.min(allowedLengthForYahooId, yahooId.length));

    //    return yahooId.toLowerCase();
    //}
    //return null;

    var yahooId = "";
    if (manufacturerPartNumber !== null && manufacturerPartNumber !== "") {
        yahooId += manufacturerPartNumber.trim() + "-";
    }
    if (partNumber !== null && partNumber !== "") {
        yahooId += partNumber.trim() + "-";
    }
    if (shortDescription !== null && shortDescription !== "") {
        yahooId += shortDescription.trim();
    }

    if (yahooId.length > 0) {

        // Yahoo ID can only be numbers, letters, and spaces. Added dash in whitelist character.
        var regex = /([^A-Za-z0-9\- ]+)/g;
        yahooId = yahooId.replace(regex, ""); //filter out unwanted characters other than regex

        //first replace multiple whitespaces with a single whitespace, then replace a space with a dash, then replace multiple dash with single dash
        yahooId = yahooId.replace(/\s+/g, " ").replace(/ /g, "-").replace(/\-+/g, "-");

        //if first character of yahooId is - then trim it off
        if (yahooId.indexOf('-') === 0) {
            yahooId = yahooId.substring(1, yahooId.length);
        }

        // Also, the generated Aabaco ID should be 70 characters or less.
        var allowedLengthForYahooId = 70;
        yahooId = yahooId.substring(0, Math.min(allowedLengthForYahooId, yahooId.length));

        //if last character of yahooId is - then trim it off
        if (yahooId.lastIndexOf('-') === yahooId.length - 1) {
            yahooId = yahooId.substring(0, yahooId.length - 1);
        }

        //finally change to lowercase
        yahooId = yahooId.toLowerCase();
    }

    return yahooId;

}

//----------------------------------------------------------------------------------------------------------------------------------
//udh.filters
//----------------------------------------------------------------------------------------------------------------------------------
udh.filters = udh.filters || {};

//function to get distinct array values
//e.g. var distinctList = arrayList.filter(udh.filters.distinctArrayValues);
udh.filters.distinctArrayValues = function (value, index, self) {
    return self.indexOf(value) === index;
}

//----------------------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------------------
//udh messaging/notification to ui
//----------------------------------------------------------------------------------------------------------------------------------
udh.notify = udh.notify || {};

function alertmsg(x, msg, time) {
    $('.sg-alert-wrapper .alert').removeClass('alert-danger alert-success alert-warning');
    $('.sg-alert-wrapper').show();
    $('.sg-alert-wrapper .error-msg').html(msg);
    if (x == 'red') {
        //  ////console.log('red');
        $('.sg-alert-wrapper .alert').addClass('alert-danger');
    }
    else if (x == 'green') {
        //    ////console.log('green');
        $('.sg-alert-wrapper .alert').addClass('alert-success');
    }
    else if (x == 'yellow') {
        //    ////console.log('green');
        $('.sg-alert-wrapper .alert').addClass('alert-warning');
    }
    else {
        $('.sg-alert-wrapper .alert').addClass('alert-warning');
    }
    if (time !== "undefined" && time < 1) {

    } else if (time !== "undefined" && time >= 1) {
        setTimeout(function () {
            $('.sg-alert-wrapper').fadeOut(500);
        }, time);

    } else {
        setTimeout(function () {
            $('.sg-alert-wrapper').fadeOut(500);
        }, 8000);
    }

    $('.sg-alert-wrapper .close').click(function () {
        $('.sg-alert-wrapper').fadeOut(500);
    });

};

udh.notify.defaultsetting = udh.notify.defaultsetting || {};
udh.notify.behavior = {
    STAY: 0,
    FADE: 8000
};
udh.notify.defaultsetting.behavior = udh.notify.behavior.FADE;

udh.notify.warn = function (message) {
    alertmsg("yellow", message, udh.notify.defaultsetting.behavior);
}
udh.notify.error = function (message, udhnotifytime) {

    if (udhnotifytime == undefined) {
        alertmsg("red", message, udh.notify.defaultsetting.behavior);
    }
    else { alertmsg("red", message, udhnotifytime); }
}
udh.notify.success = function (message) {
    alertmsg("green", message, udh.notify.defaultsetting.behavior);
}
udh.notify.startLoading = function (message) {
    message = message || "Loading";
    SGloading(message);
}
udh.notify.stopLoading = function () {
    SGloadingRemove();
}


//-------------------------------------------------------------------------------------------------------
//-------------------------------------- Common Index Function ------------------------------------------
//-------------------------------------------------------------------------------------------------------

udh.common = udh.common || {};

udh.common.crud = function (options) {

    //datatable
    $("#" + options.dataTableId).dataTable({
        "destroy": options.destroy != null ? options.destroy:true,
        "deferRender": true,
        "bServerSide": true,
        "bSort": true,
        "stateSave": options.stateSave != null ? options.stateSave:false,
        "bFilter": true,
        "aLengthMenu": dataTableMenuLength,
        "iDisplayLength": dataTableDisplayLength,
        "sAjaxSource": options.listUrl,
        "bProcessing": true,
        "scrollX": true,
        "order": options.order || [],
        language: {
            searchPlaceholder: options.searchPlaceholder || "Search records"
        }
    });

    //select all click handler
    $('#' + options.selectAllId).click(function () {
        var thisCheck = $(this);
        if (thisCheck.is(':checked')) {
            $('.' + options.selectorClass).prop("checked", true);
        } else {
            $('.' + options.selectorClass).prop("checked", false);
        }
    });

    // add button click handler
    $('#' + options.addButtonId).click(function () {
        window.location.href = options.addUrl;
    });

    // update button click handler
    $('#' + options.updateButtonId).click(function (e) {
        var i = 0;
        var selectedId = 0;
        var selector = 'input[type=checkbox][class=' + options.selectorClass + ']';
        $(selector).each(function () {
            if (this.checked) {
                i++;
                selectedId = this.value;
            }
        });
        if (i === 0) {
            udh.notify.error('Please select ' + options.tableDisplayName + ' to edit');
            e.preventDefault();
        } else if (i > 1) {
            udh.notify.error('Please select single ' + options.tableDisplayName + ' to edit');
            e.preventDefault();
        } else {
            window.location.href = options.editUrl + "?Id=" + selectedId;
        }
    });

    //delete button click
    $("#" + options.deleteButtonId).click(function (e) {
        var i = 0;
        var selector = 'input[type=checkbox][class=' + options.selectorClass + ']';
        $(selector).each(function () {
            if (this.checked) {
                i++;
            }
        });
        if (i === 0) {
            alertmsg('red', 'Please select ' + options.tableDisplayName + '(s) to delete');
            e.preventDefault();
        } else {
            $("#" + options.deletePopupModalId).modal("show");
        }
    });

    //delete button "yes" click handler
    $("#" + options.deleteYesButtonId).on("click", function (e) {
        var eachIds = [];
        var selector = 'input[type=checkbox][class=' + options.selectorClass + ']';
        $(selector).each(function () {
            if (this.checked) {
                eachIds.push(this.value);
            }
        });
        options.deleteData[0] = eachIds;

        $.ajaxSettings.traditional = true;
        $.ajax({
            url: options.deleteUrl,
            data: { 'idList': options.deleteData[0] },
            beforeSend: function () {
                SGloading("Loading");
            },
            type: 'POST',
            success: function (data) {
                //console.log(data);
                if (data && data.Status == true) {
                    udh.notify.success(data.Message);
                } else {
                    udh.notify.error(data.Message);
                }
            },
            complete: function () {
                SGloadingRemove();
                $("#" + options.deleteNoButtonId).click();
                $("#" + options.dataTableId).dataTable().fnReloadAjax();
            }
        });
    });

    //disable button click
    $("#" + options.disableButtonId).click(function (e) {
        var i = 0;
        var selector = 'input[type=checkbox][class=' + options.selectorClass + ']';
        $(selector).each(function () {
            if (this.checked) {
                i++;
            }
        });
        if (i === 0) {
            alertmsg('red', 'Please select ' + options.tableDisplayName + '(s) to disable');
            e.preventDefault();
        } else {
            $("#" + options.disablePopupModalId).modal("show");
        }
    });

    // disable button "Yes" click handler
    $("#" + options.disableYesButtonId)
        .on("click",
            function (e) {
                var eachIds = [];
                var selector = 'input[type=checkbox][class=' + options.selectorClass + ']';
                $(selector)
                    .each(function () {
                        if (this.checked) {
                            eachIds.push(this.value);
                        }
                    });
                options.disableData[0] = eachIds;

                $.ajaxSettings.traditional = true;
                $.ajax({
                    url: options.disableUrl,
                    data: { 'idList': options.disableData[0] },
                    type: 'POST',
                    beforeSend: function () {
                        SGloading("Loading");
                    },
                    success: function (data) {
                        if (data && data.Status == true) {
                            udh.notify.success(data.Message);
                        } else {
                            udh.notify.error(data.Message);
                        }

                    },
                    complete: function () {

                        SGloadingRemove();
                        $("#" + options.disableNobtn).click();
                        $("#" + options.dataTableId).dataTable().fnReloadAjax();
                    }
                });
            });

    //enable button click
    $("#" + options.enableButtonId).click(function (e) {
        var i = 0;
        var selector = 'input[type=checkbox][class=' + options.selectorClass + ']';
        $(selector).each(function () {
            if (this.checked) {
                i++;
            }
        });
        if (i === 0) {
            alertmsg('red', 'Please select ' + options.tableDisplayName + '(s) to enable');
            e.preventDefault();
        } else {
            $("#" + options.enablePopupModalId).modal("show");
        }
    });

    // enable button "Yes" click handler
    $("#" + options.enableYesButtonId)
        .on("click",
            function (e) {
                var eachIds = [];
                var selector = 'input[type=checkbox][class=' + options.selectorClass + ']';
                $(selector)
                    .each(function () {
                        if (this.checked) {
                            eachIds.push(this.value);
                        }
                    });
                options.enableData[0] = eachIds;

                $.ajaxSettings.traditional = true;
                $.ajax({
                    url: options.enableUrl,
                    data: { 'idList': options.enableData[0] },
                    type: 'POST',
                    beforeSend: function () {
                        SGloading("Loading");
                    },
                    success: function (data) {
                        if (data && data.Status == true) {
                            udh.notify.success(data.Message);
                        } else {
                            udh.notify.error(data.Message);
                        }

                    },
                    complete: function () {

                        SGloadingRemove();
                        $("#" + options.enableNobtn).click();
                        $("#" + options.dataTableId).dataTable().fnReloadAjax();
                    }
                });
            });
}

//-------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------------------------
//udh events
//----------------------------------------------------------------------------------------------------------------------------------
$("input,textarea,select,:radio,:checkbox").change(function () { //trigers change in all input fields including text type
    isSaved = false;
});

$(":button, input[type='submit'], a .btn-next").click(function () {
    isSaved = true;
});

//if menu back button is clicked, navigate to last page
$('#menuBackButton').click(function () {


    var workflowId = getQueryStringByName("workflowId");
    var stepId = getQueryStringByName("stepId");
    if ((workflowId && stepId) || parseInt(workflowId) === 0 || parseInt(stepId) === 0)
        return false;

    //var currentUrl = window.location.href;
    //if (currentUrl.indexOf("/Datahub/Dashboard") > -1)
    //    return false;

    history.back();
    return false;
});


//----------------------------------------------------------------------------------------------------------------------------------
//udh body on load function
//----------------------------------------------------------------------------------------------------------------------------------
$(function () {
    var workflowId = getQueryStringByName("workflowId");

    if (window.location.href.indexOf('Datahub/Dashboard') > 1 && window.location.href.indexOf('Datahub/Dashboard/') === -1)
        $('#menuBackButton').parent('li').hide();

    else {
        if (workflowId) {
            $('#menuBackButton').parent('li').hide();

            //history.pushState(null, null, 'pagename');
            //window.addEventListener('popstate', function (event) {
            //    history.pushState(null, null, 'pagename');
            //});

        } else
            $('#menuBackButton').parent('li').show();
    }
});


//----------------------------------------------------------------------------------------------------------------------------------
/* Jquery Equal height extension */
//----------------------------------------------------------------------------------------------------------------------------------
(function ($) {

    $.fn.equalHeights = function (options) {
        var maxHeight = 0,
            $this = $(this),
            equalHeightsFn = function () {
                var height = $(this).innerHeight();

                if (height > maxHeight) { maxHeight = height; }
            };
        options = options || {};

        $this.each(equalHeightsFn);

        if (options.wait) {
            var loop = setInterval(function () {
                if (maxHeight > 0) {
                    clearInterval(loop);
                    return $this.css('height', maxHeight);
                }
                $this.each(equalHeightsFn);
            }, 100);
        } else {
            return $this.css('height', maxHeight);
        }
    };

    // auto-initialize plugin
    $('[data-equal]').each(function () {
        var $this = $(this),
            target = $this.data('equal');
        $this.find(target).equalHeights();
    });

})(jQuery);

/* equal height ends here */

function getConfirmExit() {

    $('#confirm-exit').modal({
        show: true,
        backdrop: false,
        keyboard: false
    });

    $('#btn-exit-no').click(function () {
        $('#confirm-exit').modal('hide');
        return false;

    });
    $('#btn-exit-close').click(function () {
        $('#confirm-exit').modal('hide');
        return false;
    });

    $('#btn-exit-yes').click(function () {
        $('#confirm-exit').modal('hide');
        return true;
    });
}

//window.onload = function() {
//    window.addEventListener("beforeunload", getConfirmExit());
//};

/* catalog filter widget */
function dtblFilter(selector) {
    $('.' + selector).prop('id');
    $('.' + selector).click(function () {
        $('i', this).toggleClass('fa-rotate-180');
        //$(".sg-more-option").slideToggle("slow");
        $('div.filter-controls').toggleClass('slideLeft');
        $('.has-filter').toggleClass('filter-mg-left');
        $('.search-holder.add').toggleClass('filter-show');

    });
    TblpgntBtm();
};

function stretchTextarea() {
    var textBox = $('.custom-rule-container');
    var rulesBox = $('.custom-rule-container').parent().next('.rules-group-container').height();

    // ReSharper disable once InvalidValue
    $('.custom-rule-container').css('height', 'rulesBox');
}

function tabCollapse() {
    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    })
};

$(function () {
    /*stretchTextarea();*/
    TblpgntBtm();
    $('[data-toggle="tooltip"]').tooltip({ html: true });
    slickTable2("radio");

    tabCollapse();

    $('.accordion-toggle').click();
    dtblFilter('search-slide');

    //for popover
    $('[data-toggle="popover"]').popover();


});
$("body").removeClass("uni");

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel, filename) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    //Generate a file name
    var fileName = filename;
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g, "_");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
function JSON2CSV(objArray) {
    var array = objArray;

    var str = '';
    var line = '';

    var head = array[0];

    //  for (var index in array[0]) {
    //     line += index + ',';
    //  }
    for (var index in array[0]) {
        var value = index + "";
        line += '"' + value.replace(/"/g, '""') + '",';
    }


    line = line.slice(0, -1);
    str += line + '\r\n';


    for (var i = 0; i < array.length; i++) {
        var line = '';


        for (var index in array[i]) {
            var value = array[i][index] + "";
            line += '"' + value.replace(/"/g, '""') + '",';
        }


        line = line.slice(0, -1);
        str += line + '\r\n';
    }
    return str;

}

function smallAlertmsg(id, x, msg, time) {
    $('.sg-alert-wrapper-modal').remove();
    var btnid = "#" + id;
    var content = '<div class="sg-alert-wrapper-modal" style="display: none;">';
    content += '<div class="alert ' + x + '">';
    content += '<a href="#" class="close">&times;</a>';
    content += '<div class="error-msg">' + msg + '</div>';
    content += ' </div></div>';

    $(btnid).parents('.modal-content').find('.modal-body').prepend(content);
    //id.parents('.modal-content').children('.modal-body').prepend(content);
    $('.sg-alert-wrapper-modal').show();
    if (typeof time !== "undefined" && time < 1) {

    } else if (typeof time !== "undefined" && time >= 1) {
        setTimeout(function () {
            $('.sg-alert-wrapper-modal').fadeOut(500);
        }, time);

    } else {
        setTimeout(function () {
            $('.sg-alert-wrapper-modal').fadeOut(500);
        }, 8000);
    }
    $('.sg-alert-wrapper-modal .close').click(function () {
        $('.sg-alert-wrapper-modal').remove();
    });

};

function clearValidation(formElement) {
    //Internal $.validator is exposed through $(form).validate()
    var validator = $(formElement).validate();
    //Iterate through named elements inside of the form, and mark them as error free
    $('[name]', formElement).each(function () {
        validator.successList.push(this);//mark as error free
        validator.showErrors();//remove error messages if present
    });
    validator.resetForm();//remove error class on name elements and clear history
    validator.reset();//remove all error and success data
}

function getPositionForTooltip(ele) {
    var x = ele.getBoundingClientRect();
    var yoffset = x.top;
    if (yoffset < 500) {
        return 'bottom';
    }
    else { return 'top'; }
}

/*tooltip rebuilt*/
function rebuilttooltip() {
    $('.sg-tooltip').mouseenter(function () {
        var that = $(this);

        //if (window.location.pathname.indexOf('/Datahub/Catalog') > -1) {
        //    //that.tooltip({ placement: getPositionForTooltip(that[0]) });
        //}

        that.tooltip({ container: 'body' });

        that.tooltip('show', {
            animation: true,
            delay: { "show": 500, "hide": 100 }
        });



        // 2 second default timeout of tooltip disabled
        //setTimeout(function () { 
        //    that.tooltip('hide');
        //}, 2000);
    });

    $('.sg-tooltip').mouseleave(function () {
        $(this).tooltip('hide');
    });
};

/*datamapping select color*/
function SGmappeddata() {
    $('.sg-select-map .sourceDrop  ').each(function () {
        var str = "";
        str = $('option:selected', this).text();

        if (str === "--Choose") {
            $(this).parent('.sg-select-map').removeClass("selected");
        }
        else {
            $(this).parent('.sg-select-map').addClass("selected");
        }
    });

    $(".sourceDrop").change(function () {
        var str = "";
        str = $('option:selected', this).text();
        if (str === "--Choose") {
            $(this).parent('.sg-select-map').removeClass("selected");
        }
        else {
            $(this).parent('.sg-select-map').addClass("selected");
        }
    });



    $('.sourceDrop option:contains("sgzB")').addClass('sgzB');
    $('.sourceDrop option:contains("sgzC")').addClass('sgzC');
    $('.sourceDrop option:contains("sgzD")').addClass('sgzD');
    $('.sourceDrop option:contains("sgzDt")').addClass('sgzDt');
    $('.sourceDrop option:contains("sgzF")').addClass('sgzF');
    $('.sourceDrop option:contains("sgzI")').addClass('sgzI');
    $('.sourceDrop option:contains("sgzM")').addClass('sgzM');
    $('.sourceDrop option:contains("sgzV")').addClass('sgzV');
    $('.sourceDrop option:contains("sgzO")').addClass('sgzO');

};

function SGAfterdata() {
    $('.sourceDrop option').each(function () {
        var content = $(this).html();
        if (content != '--Choose') {
            /*$(this).html(content.substring(0, (content.length - 4)));*/
            var datatype = content.substring((content.length - 1), content.length);
            if (datatype == 't') {
                var option = '[D' + datatype + '] ' + content.substring(0, (content.length - 4));
            }
            else {
                var option = '[' + datatype + '] ' + content.substring(0, (content.length - 4));
            }
            $(this).html(option);
        }
    });
}

/*sg loading*/
function SGloading(content) {
    $('body').append("<div class='overlay'><div class='sg-loading'> <div class='row'> <div class='col-md-4 loading-logo'> " + "<i class='fa fa-spin " + " " + faIcon + " '> </i> </div> <div class='col-md-8 loading-content'>" + content + " </div> </div></div>");
    ////console.log(content);
    //$('.sg-loading').wrap("<div class='overlay'></div>");
    centerit('overlay');
    $(window).resize(function () {
        centerit('overlay');
    });

    function centerit(ovrlay) {
        $('.' + ovrlay).height($(window).height());
        var winw = $(window).width() / 2;
        var ajxw = $('.' + ovrlay + ' > div').width() / 2;
        var wt = winw - ajxw;

        var winh = $(window).height() / 2;
        var ajxh = $('.' + ovrlay + ' > div').height() / 2;
        var ht = winh - ajxh;

        $('.sg-loading').css({ 'left': wt, 'top': '50%' });
        $('.' + ovrlay).css({ 'visibility': 'visible' });
    }
}
function SGloadingRemove() {
    $('.overlay').remove();
}

//function SGtableloading(IconClass, MsgTxt, ob) {
function SGtableloading(obj) {
    Icon = (typeof obj.class != 'undefined') ? obj.class : 'fa-plane';
    msg = (typeof obj.message != 'undefined') ? obj.message : 'Processing...';
    divid = (typeof obj.objct != 'undefined') ? (obj.objct).closest('.dataTables_wrapper').attr('id') : '';

    /*var divid = $(ob).closest('.dataTables_wrapper').attr('id');*/

    /* var Icon =(IconClass)? IconClass : 'fa-spin';
     var msg  = (MsgTxt)? MsgTxt : 'Loading'; */
    if (divid !== '')
        canvas = '#' + divid;
    else
        canvas = 'body';

    $(canvas).append("<div class='overlay'><div class='sg-loading'> <div class='row'> <div class='col-md-4 loading-logo'> " + "<i class='fa fa-spin " + " " + Icon + " '> </i> </div> <div class='col-md-8 loading-content'>" + msg + " </div> </div></div>");
    centerit('overlay');
    $(window).resize(function () {
        centerit('overlay');
    });

    function centerit(ovrlay) {
        $('.' + ovrlay).css({ 'height': '100%', 'display': 'block', 'width': '100%', 'position': 'absolute', 'left': '0', 'top': '0' });
        var winw = $(canvas).width() / 2;
        var ajxw = $('.' + ovrlay + ' > div').width() / 2;
        var wt = winw - ajxw;

        var winh = $(canvas).height() / 2;
        var ajxh = $('.' + ovrlay + ' > div').height() / 2;
        var ht = winh - ajxh;

        $('.sg-loading').css({ 'left': wt, 'top': '50%' });
        $('.' + ovrlay).css({ 'visibility': 'visible' });
    }
}

function SGtableloadingRemove(divid) {
    $(canvas).find('.overlay').remove();
}
/*sg loading ends*/

var initialHeight;
/*setting up height*/
function setHeight() {
    var winheight = $(window).height()
    var fhoffset = $('.sg-grid-wrapper > div:nth-child(1)').offset();
    $('.sg-grid-wrapper > div:nth-child(1)').height(Math.floor(winheight - fhoffset.top - 45));
    initialHeight = Math.floor(winheight - fhoffset.top - 20);
    $('body').css('overflow', 'hidden');
};

/*call this whenever panel is added*/
function fixHeight() {
    $('.sg-grid-wrapper > div:nth-child(1)').height(initialHeight - 40);
};

/*setting height for sg-table-sleek-wrapper*/
function setHeightWrap() {
    var winheight = $(window).height()
    var fhoffset = $('.height-wrap').offset();
    $('.height-wrap').height(Math.floor(winheight - fhoffset.top - 1));
};
function json2xml(o, tab) {
    var toXml = function (v, name, ind) {
        var xml = "";
        if (v instanceof Array) {
            for (var i = 0, n = v.length; i < n; i++)
                xml += ind + toXml(v[i], name, ind + "\t") + "\n";
        }
        else if (typeof (v) == "object") {
            var hasChild = false;
            xml += ind + "<" + name;
            for (var m in v) {
                if (m.charAt(0) == "@")
                    xml += " " + m.substr(1) + "=\"" + v[m].toString() + "\"";
                else
                    hasChild = true;
            }
            xml += hasChild ? ">" : "/>";
            if (hasChild) {
                for (var m in v) {
                    if (m == "#text")
                        xml += v[m];
                    else if (m == "#cdata")
                        xml += "<![CDATA[" + v[m] + "]]>";
                    else if (m.charAt(0) != "@")
                        xml += toXml(v[m], m, ind + "\t");
                }
                xml += (xml.charAt(xml.length - 1) == "\n" ? ind : "") + "</" + name + ">";
            }
        }
        else {
            xml += ind + "<" + name + ">" + v.toString() + "</" + name + ">";
        }
        return xml;
    }, xml = "";
    for (var m in o)
        xml += toXml(o[m], m, "");
    return tab ? xml.replace(/\t/g, tab) : xml.replace(/\t|\n/g, "");
}

/*for dataTable checkbox selection */
function slickTable2(type) {
    if (type == "radio") {
        ////console.log(type);
        $('body').on('click', '.dataTable  tr', function (e) {
            var tableid = $(this).parents('.dataTable').attr('id');
            var detectKey = e.shiftKey || e.ctrlKey;
            ////console.log(detectKey);

            var allRow = $('#' + tableid + ' tr td:first-child input[type="checkbox"]');

            if (e.target.tagName.toLowerCase() !== 'input') {
                var checkedRow = $(this).find('td:first-child input[type="checkbox"]');
                if (checkedRow.is(':checked')) {
                    $(this).removeClass('selected');
                    checkedRow.prop('checked', false);
                }
                else {

                    $('#' + tableid + ' tr').removeClass('selected');
                    allRow.prop('checked', false);
                    $(this).addClass('selected');
                    checkedRow.prop('checked', true);
                }


            } else if (e.ctrlKey) {

                if (e.target.tagName.toLowerCase() !== 'input') {
                    var checkedRow = $(this).find('td:first-child input[type="checkbox"]');
                    if (checkedRow.is(':checked')) {
                        $(this).removeClass('selected');
                        checkedRow.prop('checked', false);
                    } else {
                        $(this).addClass('selected');
                        checkedRow.prop('checked', true);
                    }
                } else {
                    if ($(e.target).is(':checked')) {
                        $(this).addClass('selected');
                    } else {
                        $(this).removeClass('selected');
                    }
                }
                if (window.getSelection) {
                    if (window.getSelection().empty) {  // Chrome
                        window.getSelection().empty();
                    } else if (window.getSelection().removeAllRanges) {  // Firefox
                        window.getSelection().removeAllRanges();
                    }
                } else if (document.selection) {  // IE?
                    document.selection.empty();
                }

            } else {
                if ($(e.target).is(':checked')) {
                    $('#' + tableid + ' tr').removeClass('selected');
                    allRow.prop('checked', false);
                    $(e.target).prop('checked', true);
                    $(this).addClass('selected');
                }
                else {
                    $(this).removeClass('selected');
                }
            }
        });

    } else {
        $('body').on('click', '.dataTable tr', function (e) {
            if (e.target.tagName.toLowerCase() !== 'input') {
                var checkedRow = $(this).find('td:first-child input[type="checkbox"]');
                if (checkedRow.is(':checked')) {
                    $(this).removeClass('selected');
                    checkedRow.prop('checked', false);
                }
                else {
                    $(this).addClass('selected');
                    checkedRow.prop('checked', true);
                }
            } else {
                if ($(e.target).is(':checked')) {
                    $(this).addClass('selected');
                }
                else {
                    $(this).removeClass('selected');
                }
            }
        });


        /*  $('.selectAll').change(function () {
              if ($(this).is(':checked')) {
                  $('.dataTable  tr').addClass('selected');
              }
              else {
                  $('.dataTable  tr').removeClass('selected');
              }
          });*/

    }
};
function slickTable(type) {
    var selectionPivot;
    if (type == "radio") {
        ////console.log(type);
        $('body').on('click', '.dataTable > tbody > tr', function (e) {

            var detectKey = e.shiftKey || e.ctrlKey;
            if (detectKey == false) {
                selectionPivot = $(this);
                var tableid = $(this).parents('.dataTable').attr('id');

                ////console.log(detectKey);

                var allRow = $('#' + tableid + ' tr td:first-child input[type="checkbox"]');

                // allRow.closest("tr").removeClass('selected');

                if (e.target.tagName.toLowerCase() !== 'input') {
                    allRow.prop('checked', false);
                    var checkedRow = $(this).find('td:first-child input[type="checkbox"]');
                    if (checkedRow.is(':checked')) {
                        $(this).removeClass('selected');
                        checkedRow.prop('checked', false);
                    } else {
                        allRow.prop('checked', false);
                        $('#' + tableid + ' tr').removeClass('selected');
                        allRow.prop('checked', false);
                        $(this).closest('tr').addClass('selected');
                        checkedRow.prop('checked', true);
                    }


                } else {
                    if ($(e.target).is(':checked')) {
                        $('#' + tableid + ' tr').removeClass('selected');
                        allRow.prop('checked', false);
                        $(e.target).prop('checked', true);
                        $(this).closest('tr').addClass('selected');
                    } else {
                        $('#' + tableid + ' tr').removeClass('selected');
                        allRow.prop('checked', false);
                        $(e.target).prop('checked', true);
                        $(this).closest('tr').addClass('selected');
                    }
                }
            } else if (e.ctrlKey) {

                if (e.target.tagName.toLowerCase() !== 'input') {
                    var checkedRow = $(this).find('td:first-child input[type="checkbox"]');
                    if (checkedRow.is(':checked')) {
                        $(this).removeClass('selected');
                        checkedRow.prop('checked', false);
                    } else {
                        $(this).addClass('selected');
                        checkedRow.prop('checked', true);
                    }
                } else {
                    if ($(e.target).is(':checked')) {
                        $(this).addClass('selected');
                    } else {
                        $(this).removeClass('selected');
                    }
                }
                if (window.getSelection) {
                    if (window.getSelection().empty) {  // Chrome
                        window.getSelection().empty();
                    } else if (window.getSelection().removeAllRanges) {  // Firefox
                        window.getSelection().removeAllRanges();
                    }
                } else if (document.selection) {  // IE?
                    document.selection.empty();
                }

            } else if (e.shiftKey) {
                ////console.log($(this)[0].rowIndex);
                var tbodyElement = $(this).parents('tbody');
                var trElements = tbodyElement.find('tr');
                var bot = Math.min(selectionPivot[0].rowIndex, $(this)[0].rowIndex);
                var top = Math.max(selectionPivot[0].rowIndex, $(this)[0].rowIndex);

                trElements.removeClass("selected");
                trElements.find('td:first-child input[type=checkbox]').prop('checked', false);
                //checkedRow.attr("checked", false);
                //checkdedRow.attr(':checked');
                for (var i = bot; i <= top; i++) {
                    var currentElement = trElements[i - 1];
                    currentElement.className += " selected";
                    ////console.log($(currentElement).find('td:first-child input[type="checkbox"]'));
                    $(currentElement).find('td:first-child input[type="checkbox"]').prop("checked", true);
                    //trElements[i - 1].find('td:first-child input[type="checkbox"]').

                }
                if (window.getSelection) {
                    if (window.getSelection().empty) {  // Chrome
                        window.getSelection().empty();
                    } else if (window.getSelection().removeAllRanges) {  // Firefox
                        window.getSelection().removeAllRanges();
                    }
                } else if (document.selection) {  // IE?
                    document.selection.empty();
                }
            }
        });

    } else {
        $('body').on('click', '.dataTable >  tbody >  tr', function (e) {
            if (e.target.tagName.toLowerCase() !== 'input') {
                var checkedRow = $(this).find('td:first-child input[type="checkbox"]');
                if (checkedRow.is(':checked')) {
                    $(this).removeClass('selected');
                    checkedRow.prop('checked', false);
                } else {
                    //$(this).addClass('selected');
                    checkedRow.prop('checked', true);
                }
            } else {
                if ($(e.target).is(':checked')) {
                    //$(this).addClass('selected');
                } else {
                    //$(this).removeClass('selected');
                }
            }
        });

    }

};
$('.selectAll').change(function () {
    if ($(this).is(':checked')) {
        $(this).parents('.dataTables_wrapper').find('table.dataTable tbody  tr').addClass('selected');
        $(this).parents('.dataTables_wrapper').find('table.dataTable tbody  tr td:first-child input[type=checkbox]').prop('checked', true);
    }
    else {
        $(this).parents('.dataTables_wrapper').find('table.dataTable tbody  tr').removeClass('selected');
        $(this).parents('.dataTables_wrapper').find('table.dataTable tbody  tr td:first-child input[type=checkbox]').prop('checked', false);
    }
});
/*for expand function for catalog*/
function expandview2() {
    //var $table = $('table.sg-table-sleek');
    $('.expand-cat-one').click(function () {
        //$table.floatThead('reflow');
        $('.expand-btn').hide();
        $('.collapse').show();
        $('.sg-catalog-one').show();
        $('.sg-catalog-one').removeClass('col-md-6').addClass('col-md-12');
        $('.sg-catalog-two').hide();

    });
    $('.expand-cat-two').click(function () {
        //$table.floatThead('reflow');

        $('.expand-btn').hide();
        $('.collapse').show();
        $('.sg-catalog-two').show();
        $('.sg-catalog-two').removeClass('col-md-6').addClass('col-md-12');
        $('.sg-catalog-one').hide();
    });
    $('.collapse').click(function () {
        //$table.floatThead('reflow');

        $('.collapse').hide();
        $('.expand-btn').show();
        $('.sg-catalog-one, .sg-catalog-two').show();
        $('.sg-catalog-one').removeClass('col-md-12').addClass('col-md-6');
        $('.sg-catalog-two').removeClass('col-md-12').addClass('col-md-6');
    });
};


function expandview3() {
    //var $table = $('table.sg-table-sleek');
    $('.expand-cat-one').click(function () {
        //$table.floatThead('reflow');
        $('.expand-btn').hide();
        $('.collapse').show();
        $('.sg-catalog-one').show();
        $('.sg-catalog-one').removeClass('col-md-4').addClass('col-md-12');
        $('.sg-catalog-two').hide();
        $('.sg-catalog-three').hide();
    });
    $('.expand-cat-two').click(function () {
        //$table.floatThead('reflow');

        $('.expand-btn').hide();
        $('.collapse').show();
        $('.sg-catalog-two').show();
        $('.sg-catalog-two').removeClass('col-md-4').addClass('col-md-12');
        $('.sg-catalog-one').hide();
        $('.sg-catalog-three').hide();
    });
    $('.expand-cat-three').click(function () {
        //$table.floatThead('reflow');

        $('.expand-btn').hide();
        $('.collapse').show();
        $('.ssg-catalog-three').show();
        $('.sg-catalog-three').removeClass('col-md-4').addClass('col-md-12');
        $('.sg-catalog-one').hide();
        $('.sg-catalog-two').hide();
    });
    $('.collapse').click(function () {
        //$table.floatThead('reflow');

        $('.collapse').hide();
        $('.expand-btn').show();
        $('.sg-catalog-one, .sg-catalog-two, .sg-catalog-three').show();
        $('.sg-catalog-one').removeClass('col-md-12').addClass('col-md-4');
        $('.sg-catalog-two').removeClass('col-md-12').addClass('col-md-4');
        $('.sg-catalog-three').removeClass('col-md-12').addClass('col-md-4');
    });
};


/* for pagination in bottom  in datatable */
function TblpgntBtm() {
    // var allHeight = $('.sg-datatable-wrap').prevAll();
    // var elmHeight = allHeight.height();
    var sgtabHeight = $('.sg-tab-bar').height() + 0;
    var panelHeight = $('.sg-grid-panel').height() + 0;
    var headerHeight = $('.fixedbar.header').height() + 0;
    var footerHeight = $('.fixedbar.footer').height() + 0;
    var filterdiv = $('.search-holder.top').height() + 0;
    var elmHeight = sgtabHeight + panelHeight + headerHeight + footerHeight;
    var sgWrapper = $('.sg-wrapper').height() + 0;
    var sgaassobtn = $('.btn-asso').height() + 0;
    windowHeight = function windowHeight() {
        winHei = $(window).height()//window height without pixels
        $('.sg-datatable-wrap').css('height', winHei - elmHeight - filterdiv - sgaassobtn + 'px');//dataTables container//130 is for my page design consider your own
        //var sgDatatable = $('.sg-datatable-wrap').height()

        $('div.sg-datatable-wrap  div.dataTables_scrollBody').css('height', winHei - filterdiv - elmHeight - sgaassobtn - 106 + 'px');//this is dataTable scroll body
        // hansonetable height calucalation
        $('.handsontable-wrapper').css('height', winHei - elmHeight - filterdiv - sgaassobtn - 148 + 'px')



    };
    setTimeout(function () {
        windowHeight();
    }, 200);


    setTimeout(function () {
        if (typeof hot !== "undefined") {
            hot.render();
        }
    }, 200);


    $(window).resize(function () {
        windowHeight();
    });
};
function TblpgntBtm1() {
    // var allHeight = $('.sg-datatable-wrap').prevAll();
    // var elmHeight = allHeight.height();
    var sgtabHeight = $('.sg-tab-bar').height() + 0;
    var panelHeight = $('.sg-grid-panel').height() + 0;
    var headerHeight = $('.fixedbar.header').height() + 0;
    var footerHeight = $('.fixedbar.footer').height() + 0;
    var filterdiv = $('.search-holder.top').height() + 0;
    var elmHeight = sgtabHeight + panelHeight + headerHeight + footerHeight;
    var sgWrapper = $('.sg-wrapper').height() + 0;

    windowHeight = function windowHeight() {
        winHei = $(window).height()//window height without pixels
        $('.sg-datatable-wrap').css('height', winHei - elmHeight - filterdiv + 'px');//dataTables container//130 is for my page design consider your own
        //var sgDatatable = $('.sg-datatable-wrap').height()

        $('div.sg-datatable-wrap  div.dataTables_scrollBody').css('height', winHei - filterdiv - elmHeight - 140 + 'px');//this is dataTable scroll body
        // hansonetable height calucalation
        $('.handsontable-wrapper').css('height', winHei - elmHeight - filterdiv - 160 + 'px')

    };
    setTimeout(function () {
        windowHeight();
    }, 500);

    $(window).resize(function () {
        windowHeight();
    });
};

//Enter key focus to next control
$(function () {
    $("input,select,:radio,:checkbox").not($(":button,:submit,h1,h2,h3,h4,h5,h6,textarea")).keypress(function (evt) {
        if (evt.keyCode === 13) {
            iname = $(this).val();
            if (iname !== 'Submit') {
                var fields = $(this).parents('form:eq(0),body').find('button,input,textarea,select,:radio,:checkbox').not(":hidden,:disabled");
                var index = fields.index(this);
                if (index > -1 && (index + 1) < fields.length) {

                    fields.eq(index + 1).focus();
                    fields.eq(index + 1).select();

                }
                evt.preventDefault();
            }
        }
    });
});

// Get querystring value
function getQueryStringByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function JSONDateWithTime(dateStr) {
    jsonDate = dateStr;
    var d = new Date(parseInt(jsonDate.substr(6)));
    var m, day;
    m = d.getMonth() + 1;
    if (m < 10)
        m = '0' + m
    if (d.getDate() < 10)
        day = '0' + d.getDate()
    else
        day = d.getDate();
    var formattedDate = m + "/" + day + "/" + d.getFullYear();
    var hours = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
    var minutes = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
    var formattedTime = hours + ":" + minutes + ":" + d.getSeconds() + "." + d.getMilliseconds();
    formattedDate = formattedDate + " " + formattedTime;
    return formattedDate;
}

function FormattedDate(dateStr) {
    jsonDate = dateStr;
    var d = new Date(jsonDate);
    var m, day;
    m = d.getMonth() + 1;
    if (m < 10)
        m = '0' + m
    if (d.getDate() < 10)
        day = '0' + d.getDate()
    else
        day = d.getDate();
    var formattedDate = m + "/" + day + "/" + d.getFullYear();
    return formattedDate;
}

truncateDecimals = function (number, digits) {
    var multiplier = Math.pow(10, digits),
        adjustedNum = number * multiplier,
        truncatedNum = Math[adjustedNum < 0 ? 'ceil' : 'floor'](adjustedNum);

    return truncatedNum / multiplier;
};

// Format amount
function FormatAmount(yourNumber, decimalPlaces) {
    if ($.isNumeric(yourNumber) === false)
        return 0;
    decimalPlaces = decimalPlaces != null ? decimalPlaces : 4;
    var num = parseFloat(yourNumber);
    yourNumber = (Math.round(num * Math.pow(10, decimalPlaces)) / Math.pow(10, decimalPlaces)).toFixed(decimalPlaces);
    //yourNumber = parseFloat(yourNumber).toFixed(decimalPlaces);
    return yourNumber;
    ////Seperates the components of the number
    //var n = yourNumber.toString().split(".");
    ////Comma-fies the first part
    //n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    ////Combines the two sections
    //return n.join(".");
}

function getTimeZone() {
    var timezone = /\((.*)\)/.exec(new Date().toString())[1];
    timezone = timezone.match(/\b(\w)/g);              // 
    timezone = timezone.join('');
    return timezone;
}

function customJqueryUI() {
    $("div.DTTT_container, div.dataTables_length, div.dataTables_filter, div.clear").wrapAll("<div class='fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr' />");
    $("div.dataTables_info, div.dataTables_paginate").wrapAll("<div class='fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br'/>");
}

function customJqueryUIpara(parentClass) {
    $("div.DTTT_container, div.dataTables_length, div.dataTables_filter, div.clear,div.ColVis", parentClass).wrapAll("<div class='fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr' />");
    $("div.dataTables_info, div.dataTables_paginate", parentClass).wrapAll("<div class='fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br'/>");
}

//Global error display
$(function () {
    var validationErrorHtml = $('#validation-summary-wrapper-div ul li').html();

    if (validationErrorHtml != "") {
        $('#validation-summary-wrapper-div').show();
        setTimeout(function () {
            $('#validation-summary-wrapper-div').hide();
        }, 8000);
    } else {
        $('#validation-summary-wrapper-div').hide();
    }

    $('#validation-summary-close').click(function () {
        $('#validation-summary-wrapper-div').hide();
    });
});

//Javascript equivalent of String.Format
String.prototype.format = String.prototype.f = function () {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
        // $' replace issue since $' is a jquery selector.
        //if (s.indexOf("]]></{0}>]]></{0}>") > -1) {
        //    s = s.replace("]]></{0}>]]></{0}>", '');
        //    s = s + "$']]></{0}>";
        //}
        var t = s.split(']]></{0}>');
        var z = '';
        var r = '';
        var splitted = false;
        if (t.length > 2) {
            for (var k = 0; k < t.length - 2; k++) {
                z = z + t[k] + "$'";
            }
            z = z + t[t.length - 2] + t[t.length - 1] + "]]></{0}>";
            s = z;
            splitted = true;
        }
        if (splitted === false) {
            var m = s.split('</{0}>');
            if (m.length > 2) {
                for (var n = 0; n < m.length - 2; n++) {
                    r = r + m[n] + "$'";
                }
                r = r + m[m.length - 2] + m[m.length - 1] + '</{0}>';
                s = r;
                splitted = true;
            }
        }

        if (splitted === false) {
            var m = s.split('</{1}>');
            if (m.length > 2) {
                for (var n = 0; n < m.length - 2; n++) {
                    r = r + m[n] + "$'";
                }
                r = r + m[m.length - 2] + m[m.length - 1] + '</{1}>';
                s = r;
                splitted = true;
            }
        }


    }
    return s;
};


//Add tooltip
var addTooltip = function (selector, message, placement) {
    if (placement == null) placement = 'top';
    $(selector).attr('title', message);
    $(selector).addClass('sg-tooltip');
    $(selector).attr('data-placement', placement);
    $(selector).attr('data-toggle', 'tooltip');
};

//Remove tooltip
var removeTooltip = function (selector) {
    $(selector).removeAttr('title');
    $(selector).removeClass('sg-tooltip');
    $(selector).removeAttr('data-placement');
    $(selector).removeAttr('data-toggle');
};

var fixJsonQuote = function (jsonData) {
    return JSON.parse(jsonData.replace(/\r?\n|\r/g, "").replace(/\t+/g, "").replace(/&quot;/g, '"'));
}

var fixSQLRule = function (jsonData) {
    return JSON.parse(jsonData.replace(/&quot;/g, '"').replace(/&#39;/g, '\'').replace(/&gt;/g, '>').replace(/&lt;/g, '<'));
}

var fixJsonSingleQuote = function (jsonData) {
    jsonData = jsonData.replace(/\r?\n|\r/g, "").replace(/&singlequot;/g, "'");
    return JSON.parse(jsonData);
}
var fixNewLineOnly = function (jsonData) {
    jsonData = jsonData.replace(/\r/g, "\\r").replace(/\n/g, "\\n").replace(/\t/g, "\\t").replace(/\f/g, "\\f");
    return JSON.parse(jsonData);
}

var fixSingleQuoteOnly = function (jsonData) {
    jsonData = jsonData.replace(/&singlequot;/g, "'");
    return JSON.parse(jsonData);
}
var fixJsonSingleQuoteWithNewline = function (jsonData) {
    jsonData = jsonData.replace(/\r/g, "\\r").replace(/\n/g, "\\n").replace(/&singlequot;/g, "'");
    return JSON.parse(jsonData);
}

var custom_options = {
    byPassKeys: [8, 9, 37, 38, 39, 40],
    translation: {
        '0': { pattern: /\d/ },
        '9': { pattern: /[\d.]/, recursive: true },
        '#': { pattern: /\d/, recursive: true },
        'A': { pattern: /[a-zA-Z0-9]/ },
        'S': { pattern: /[a-zA-Z]/ },
        'Z': { pattern: /(([a-zA-Z0-9])+)(([a-zA-Z0-9()\s])+)?/, recursive: true },
        'w': { pattern: /[a-zA-Z0-9_ ]/, recursive: true }
    }
};

var custom_Name_options = {
    translation: {
        'S': { pattern: /[a-zA-Z]/, recursive: true },
        'N': { pattern: /[a-zA-Z0-9.,\\\\\/-_() ]/, recursive: true }
    }
    //onKeyPress: function (cep, e, field, options) {
    //var mask = (cep.length > 1) ? "N" : "S";
    //    if (!isNaN(parseInt(cep))) {
    //        mask = "S";
    //    }
    //    $('.nameMask').mask(mask, custom_Name_options);
    //}
};

var custom_Sku_options = {
    translation: {
        'L': { pattern: /[a-zA-Z0-9\-]/, recursive: true }
    }/*,
    onKeyPress: function (value, event) {
        //event.target.value = value.toUpperCase();
        event.target.value = event.target.value.toUpperCase();
    }*/
};

var custom_Code_options = {
    translation: {
        "A": { pattern: /[a-zA-Z0-9]/, recursive: true },
        'E': { pattern: /[a-zA-Z0-9()-_ ]/, recursive: true }
    }
    //onKeyPress: function (cep, e, field, options) {
    //    var mask = (cep.length > 1) ? "E" : "A";

    //    $('.codeMask').mask(mask, custom_Code_options);
    //}
};
var custom_Address_options = {
    translation: {
        "A": { pattern: /[a-zA-Z0-9]/, recursive: true },
        'E': { pattern: /[a-zA-Z0-9(),.:\\\\\-/_;& ]/, recursive: true }
    }
    //onKeyPress: function (cep, e, field, options) {
    //    var mask = (cep.length > 1) ? "E" : "A";

    //    $('.addressMask').mask(mask, custom_Address_options);
    //}
};
$('.intDataType').mask("0#");
$('.floatDataType').mask("9", custom_options);
$('.datetimeDataType').mask('00/00/0000', { 'translation': { 0: { pattern: /[0-9]/ } } });
$('.moneyDataType').mask("9", custom_options);
$('.windowSupportName').mask("w", custom_options);
$('.codeMask').mask("E", custom_Code_options);
$('.nameMask').mask("N", custom_Name_options);
$('.localSkuTypeMask').mask("L", custom_Sku_options);
$('.addressMask').mask("E", custom_Address_options);
$('.datetimeTypeHHMM').mask('00/00/0000 00:00', { 'translation': { 0: { pattern: /[0-9]/ } } });
$('.HH').mask('00', { 'translation': { 0: { pattern: /[0-9]/ } } });
$('.HHMM').mask('00:00', { 'translation': { 0: { pattern: /[0-9]/ } } });


$(".datetimeDataType").datepicker({

    showButtonPanel: true
});

$.datepicker._gotoToday = function (id) {
    $(id).datepicker('setDate', new Date()).datepicker('hide').blur();
};


/* same height */

// align blocks height
function initSameHeight() {
    setSameHeight({
        holder: '.columns-holder',
        elements: '.same-height'
    });
}

// set same height for blocks
function setSameHeight(opt) {
    // default options
    var options = {
        holder: null,
        skipClass: 'same-height-ignore',
        leftEdgeClass: 'same-height-left',
        rightEdgeClass: 'same-height-right',
        elements: '>*',
        flexible: false,
        multiLine: false,
        useMinHeight: false,
        biggestHeight: false
    };
    for (var p in opt) {
        if (opt.hasOwnProperty(p)) {
            options[p] = opt[p];
        }
    }

    // init script
    if (options.holder) {
        var holders = lib.queryElementsBySelector(options.holder);
        lib.each(holders, function (ind, curHolder) {
            var curElements = [], resizeTimer, postResizeTimer;
            var tmpElements = lib.queryElementsBySelector(options.elements, curHolder);

            // get resize elements
            for (var i = 0; i < tmpElements.length; i++) {
                if (!lib.hasClass(tmpElements[i], options.skipClass)) {
                    curElements.push(tmpElements[i]);
                }
            }
            if (!curElements.length) return;

            // resize handler
            function doResize() {
                for (var i = 0; i < curElements.length; i++) {
                    curElements[i].style[options.useMinHeight && SameHeight.supportMinHeight ? 'minHeight' : 'height'] = '';
                }

                if (options.multiLine) {
                    // resize elements row by row
                    SameHeight.resizeElementsByRows(curElements, options);
                } else {
                    // resize elements by holder
                    SameHeight.setSize(curElements, curHolder, options);
                }
            }
            doResize();

            // handle flexible layout / font resize
            function flexibleResizeHandler() {
                clearTimeout(resizeTimer);
                resizeTimer = setTimeout(function () {
                    doResize();
                    clearTimeout(postResizeTimer);
                    postResizeTimer = setTimeout(doResize, 100);
                }, 1);
            }
            if (options.flexible) {
                addEvent(window, 'resize', flexibleResizeHandler);
                addEvent(window, 'orientationchange', flexibleResizeHandler);
                FontResizeEvent.onChange(flexibleResizeHandler);
            }
            // handle complete page load including images and fonts
            addEvent(window, 'load', flexibleResizeHandler);
        });
    }

    // event handler helper functions
    function addEvent(object, event, handler) {
        if (object.addEventListener) object.addEventListener(event, handler, false);
        else if (object.attachEvent) object.attachEvent('on' + event, handler);
    }
}

/*
 * SameHeight helper module
 */
SameHeight = {
    supportMinHeight: typeof document.documentElement.style.maxHeight !== 'undefined', // detect css min-height support
    setSize: function (boxes, parent, options) {
        var calcHeight, holderHeight = typeof parent === 'number' ? parent : this.getHeight(parent);

        for (var i = 0; i < boxes.length; i++) {
            var box = boxes[i];
            var depthDiffHeight = 0;
            var isBorderBox = this.isBorderBox(box);
            lib.removeClass(box, options.leftEdgeClass);
            lib.removeClass(box, options.rightEdgeClass);

            if (typeof parent != 'number') {
                var tmpParent = box.parentNode;
                while (tmpParent != parent) {
                    depthDiffHeight += this.getOuterHeight(tmpParent) - this.getHeight(tmpParent);
                    tmpParent = tmpParent.parentNode;
                }
            }
            calcHeight = holderHeight - depthDiffHeight;
            calcHeight -= isBorderBox ? 0 : this.getOuterHeight(box) - this.getHeight(box);
            if (calcHeight > 0) {
                box.style[options.useMinHeight && this.supportMinHeight ? 'minHeight' : 'height'] = calcHeight + 'px';
            }
        }

        lib.addClass(boxes[0], options.leftEdgeClass);
        lib.addClass(boxes[boxes.length - 1], options.rightEdgeClass);
        return calcHeight;
    },
    getOffset: function (obj) {
        if (obj.getBoundingClientRect) {
            var scrollLeft = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;
            var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
            var clientLeft = document.documentElement.clientLeft || document.body.clientLeft || 0;
            var clientTop = document.documentElement.clientTop || document.body.clientTop || 0;
            return {
                top: Math.round(obj.getBoundingClientRect().top + scrollTop - clientTop),
                left: Math.round(obj.getBoundingClientRect().left + scrollLeft - clientLeft)
            };
        } else {
            var posLeft = 0, posTop = 0;
            while (obj.offsetParent) { posLeft += obj.offsetLeft; posTop += obj.offsetTop; obj = obj.offsetParent; }
            return { top: posTop, left: posLeft };
        }
    },
    getStyle: function (el, prop) {
        if (document.defaultView && document.defaultView.getComputedStyle) {
            return document.defaultView.getComputedStyle(el, null)[prop];
        } else if (el.currentStyle) {
            return el.currentStyle[prop];
        } else {
            return el.style[prop];
        }
    },
    getStylesTotal: function (obj) {
        var sum = 0;
        for (var i = 1; i < arguments.length; i++) {
            var val = parseFloat(this.getStyle(obj, arguments[i]));
            if (!isNaN(val)) {
                sum += val;
            }
        }
        return sum;
    },
    getHeight: function (obj) {
        return obj.offsetHeight - this.getStylesTotal(obj, 'borderTopWidth', 'borderBottomWidth', 'paddingTop', 'paddingBottom');
    },
    getOuterHeight: function (obj) {
        return obj.offsetHeight;
    },
    isBorderBox: function (obj) {
        var f = this.getStyle, styleValue = f(obj, 'boxSizing') || f(obj, 'WebkitBoxSizing') || f(obj, 'MozBoxSizing');
        return styleValue === 'border-box';
    },
    resizeElementsByRows: function (boxes, options) {
        var currentRow = [], maxHeight, maxCalcHeight = 0, firstOffset = this.getOffset(boxes[0]).top;
        for (var i = 0; i < boxes.length; i++) {
            if (this.getOffset(boxes[i]).top === firstOffset) {
                currentRow.push(boxes[i]);
            } else {
                maxHeight = this.getMaxHeight(currentRow);
                maxCalcHeight = Math.max(maxCalcHeight, this.setSize(currentRow, maxHeight, options));
                firstOffset = this.getOffset(boxes[i]).top;
                currentRow = [boxes[i]];
            }
        }
        if (currentRow.length) {
            maxHeight = this.getMaxHeight(currentRow);
            maxCalcHeight = Math.max(maxCalcHeight, this.setSize(currentRow, maxHeight, options));
        }
        if (options.biggestHeight) {
            for (i = 0; i < boxes.length; i++) {
                boxes[i].style[options.useMinHeight && this.supportMinHeight ? 'minHeight' : 'height'] = maxCalcHeight + 'px';
            }
        }
    },
    getMaxHeight: function (boxes) {
        var maxHeight = 0;
        for (var i = 0; i < boxes.length; i++) {
            maxHeight = Math.max(maxHeight, this.getOuterHeight(boxes[i]));
        }
        return maxHeight;
    }
};

/*
 * FontResize Event
 */
FontResizeEvent = (function (window, document) {
    var randomID = 'font-resize-frame-' + Math.floor(Math.random() * 1000);
    var resizeFrame = document.createElement('iframe');
    resizeFrame.id = randomID; resizeFrame.className = 'font-resize-helper';
    resizeFrame.style.cssText = 'position:absolute;width:100em;height:10px;top:-9999px;left:-9999px;border-width:0';

    // wait for page load
    function onPageReady() {
        document.body.appendChild(resizeFrame);

        // use native IE resize event if possible
        if (/MSIE (6|7|8)/.test(navigator.userAgent)) {
            resizeFrame.onresize = function () {
                window.FontResizeEvent.trigger(resizeFrame.offsetWidth / 100);
            };
        }
        // use script inside the iframe to detect resize for other browsers
        else {
            var doc = resizeFrame.contentWindow.document;
            doc.open();
            doc.write('<scri' + 'pt>window.onload = function(){var em = parent.document.getElementById("' + randomID + '");window.onresize = function(){if(parent.FontResizeEvent){parent.FontResizeEvent.trigger(em.offsetWidth / 100);}}};</scri' + 'pt>');
            doc.close();
        }
    }
    if (window.addEventListener) window.addEventListener('load', onPageReady, false);
    else if (window.attachEvent) window.attachEvent('onload', onPageReady);

    // public interface
    var callbacks = [];
    return {
        onChange: function (f) {
            if (typeof f === 'function') {
                callbacks.push(f);
            }
        },
        trigger: function (em) {
            for (var i = 0; i < callbacks.length; i++) {
                callbacks[i](em);
            }
        }
    };
}(this, document));

/*
 * Utility module
 */
lib = {
    hasClass: function (el, cls) {
        return el && el.className ? el.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)')) : false;
    },
    addClass: function (el, cls) {
        if (el && !this.hasClass(el, cls)) el.className += " " + cls;
    },
    removeClass: function (el, cls) {
        if (el && this.hasClass(el, cls)) { el.className = el.className.replace(new RegExp('(\\s|^)' + cls + '(\\s|$)'), ' '); }
    },
    extend: function (obj) {
        for (var i = 1; i < arguments.length; i++) {
            for (var p in arguments[i]) {
                if (arguments[i].hasOwnProperty(p)) {
                    obj[p] = arguments[i][p];
                }
            }
        }
        return obj;
    },
    each: function (obj, callback) {
        var property, len;
        if (typeof obj.length === 'number') {
            for (property = 0, len = obj.length; property < len; property++) {
                if (callback.call(obj[property], property, obj[property]) === false) {
                    break;
                }
            }
        } else {
            for (property in obj) {
                if (obj.hasOwnProperty(property)) {
                    if (callback.call(obj[property], property, obj[property]) === false) {
                        break;
                    }
                }
            }
        }
    },
    event: (function () {
        var fixEvent = function (e) {
            e = e || window.event;
            if (e.isFixed) return e; else e.isFixed = true;
            if (!e.target) e.target = e.srcElement;
            e.preventDefault = e.preventDefault || function () { this.returnValue = false; };
            e.stopPropagation = e.stopPropagation || function () { this.cancelBubble = true; };
            return e;
        };
        return {
            add: function (elem, event, handler) {
                if (!elem.events) {
                    elem.events = {};
                    elem.handle = function (e) {
                        var ret, handlers = elem.events[e.type];
                        e = fixEvent(e);
                        for (var i = 0, len = handlers.length; i < len; i++) {
                            if (handlers[i]) {
                                ret = handlers[i].call(elem, e);
                                if (ret === false) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                }
                            }
                        }
                    };
                }
                if (!elem.events[event]) {
                    elem.events[event] = [];
                    if (elem.addEventListener) elem.addEventListener(event, elem.handle, false);
                    else if (elem.attachEvent) elem.attachEvent('on' + event, elem.handle);
                }
                elem.events[event].push(handler);
            },
            remove: function (elem, event, handler) {
                var handlers = elem.events[event];
                for (var i = handlers.length - 1; i >= 0; i--) {
                    if (handlers[i] === handler) {
                        handlers.splice(i, 1);
                    }
                }
                if (!handlers.length) {
                    delete elem.events[event];
                    if (elem.removeEventListener) elem.removeEventListener(event, elem.handle, false);
                    else if (elem.detachEvent) elem.detachEvent('on' + event, elem.handle);
                }
            }
        };
    }()),
    queryElementsBySelector: function (selector, scope) {
        scope = scope || document;
        if (!selector) return [];
        if (selector === '>*') return scope.children;
        if (typeof document.querySelectorAll === 'function') {
            return scope.querySelectorAll(selector);
        }
        var selectors = selector.split(',');
        var resultList = [];
        for (var s = 0; s < selectors.length; s++) {
            var currentContext = [scope || document];
            var tokens = selectors[s].replace(/^\s+/, '').replace(/\s+$/, '').split(' ');
            for (var i = 0; i < tokens.length; i++) {
                token = tokens[i].replace(/^\s+/, '').replace(/\s+$/, '');
                if (token.indexOf('#') > -1) {
                    var bits = token.split('#'), tagName = bits[0], id = bits[1];
                    var element = document.getElementById(id);
                    if (element && tagName && element.nodeName.toLowerCase() != tagName) {
                        return [];
                    }
                    currentContext = element ? [element] : [];
                    continue;
                }
                if (token.indexOf('.') > -1) {
                    var bits = token.split('.'), tagName = bits[0] || '*', className = bits[1], found = [], foundCount = 0;
                    for (var h = 0; h < currentContext.length; h++) {
                        var elements;
                        if (tagName == '*') {
                            elements = currentContext[h].getElementsByTagName('*');
                        } else {
                            elements = currentContext[h].getElementsByTagName(tagName);
                        }
                        for (var j = 0; j < elements.length; j++) {
                            found[foundCount++] = elements[j];
                        }
                    }
                    currentContext = [];
                    var currentContextIndex = 0;
                    for (var k = 0; k < found.length; k++) {
                        if (found[k].className && found[k].className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))) {
                            currentContext[currentContextIndex++] = found[k];
                        }
                    }
                    continue;
                }
                if (token.match(/^(\w*)\[(\w+)([=~\|\^\$\*]?)=?"?([^\]"]*)"?\]$/)) {
                    var tagName = RegExp.$1 || '*', attrName = RegExp.$2, attrOperator = RegExp.$3, attrValue = RegExp.$4;
                    if (attrName.toLowerCase() == 'for' && this.browser.msie && this.browser.version < 8) {
                        attrName = 'htmlFor';
                    }
                    var found = [], foundCount = 0;
                    for (var h = 0; h < currentContext.length; h++) {
                        var elements;
                        if (tagName == '*') {
                            elements = currentContext[h].getElementsByTagName('*');
                        } else {
                            elements = currentContext[h].getElementsByTagName(tagName);
                        }
                        for (var j = 0; elements[j]; j++) {
                            found[foundCount++] = elements[j];
                        }
                    }
                    currentContext = [];
                    var currentContextIndex = 0, checkFunction;
                    switch (attrOperator) {
                        case '=': checkFunction = function (e) { return (e.getAttribute(attrName) == attrValue) }; break;
                        case '~': checkFunction = function (e) { return (e.getAttribute(attrName).match(new RegExp('(\\s|^)' + attrValue + '(\\s|$)'))) }; break;
                        case '|': checkFunction = function (e) { return (e.getAttribute(attrName).match(new RegExp('^' + attrValue + '-?'))) }; break;
                        case '^': checkFunction = function (e) { return (e.getAttribute(attrName).indexOf(attrValue) == 0) }; break;
                        case '$': checkFunction = function (e) { return (e.getAttribute(attrName).lastIndexOf(attrValue) == e.getAttribute(attrName).length - attrValue.length) }; break;
                        case '*': checkFunction = function (e) { return (e.getAttribute(attrName).indexOf(attrValue) > -1) }; break;
                        default: checkFunction = function (e) { return e.getAttribute(attrName) };
                    }
                    currentContext = [];
                    var currentContextIndex = 0;
                    for (var k = 0; k < found.length; k++) {
                        if (checkFunction(found[k])) {
                            currentContext[currentContextIndex++] = found[k];
                        }
                    }
                    continue;
                }
                tagName = token;
                var found = [], foundCount = 0;
                for (var h = 0; h < currentContext.length; h++) {
                    var elements = currentContext[h].getElementsByTagName(tagName);
                    for (var j = 0; j < elements.length; j++) {
                        found[foundCount++] = elements[j];
                    }
                }
                currentContext = found;
            }
            resultList = [].concat(resultList, currentContext);
        }
        return resultList;
    },
    trim: function (str) {
        return str.replace(/^\s+/, '').replace(/\s+$/, '');
    },
    bind: function (f, scope, forceArgs) {
        return function () { return f.apply(scope, typeof forceArgs !== 'undefined' ? [forceArgs] : arguments); };
    }
};

// DOM ready handler
function bindReady(handler) {
    var called = false;
    var ready = function () {
        if (called) return;
        called = true;
        handler();
    };
    if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', ready, false);
    } else if (document.attachEvent) {
        if (document.documentElement.doScroll && window == window.top) {
            var tryScroll = function () {
                if (called) return;
                if (!document.body) return;
                try {
                    document.documentElement.doScroll('left');
                    ready();
                } catch (e) {
                    setTimeout(tryScroll, 0);
                }
            };
            tryScroll();
        }
        document.attachEvent('onreadystatechange', function () {
            if (document.readyState === 'complete') {
                ready();
            }
        });
    }
    if (window.addEventListener) window.addEventListener('load', ready, false);
    else if (window.attachEvent) window.attachEvent('onload', ready);
}
/* same height */
function ReplaceSingle2QuoteSubObject(object) {
    for (var x in object) {
        if (typeof object[x] == 'object') {
            ReplaceSingle2QuoteSubObject(object[x]);
        }
        if (object[x] !== null) {
            if (Object.prototype.toString.call(object[x]) === '[object String]') {
                object[x] = object[x].replace(/\''/g, "\"");
            }
        }
    }
}

function ReplaceSingle2Quote(object) {
    for (var x in object) {
        if (typeof object[x] == 'object') {
            ReplaceSingle2QuoteSubObject(object[x]);
        }
        if (object[x] !== null) {
            if (Object.prototype.toString.call(object[x]) === '[object String]') {
                object[x] = object[x].replace(/\''/g, "\"");
                //object[x] = object[x].replace("''", "\"");
            }
        }
    }

}


/* Match Height */
; (function (factory) { // eslint-disable-line no-extra-semi
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery'], factory);
    } else if (typeof module !== 'undefined' && module.exports) {
        // CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Global
        factory(jQuery);
    }
})(function ($) {
    /*
    *  internal
    */

    var _previousResizeWidth = -1,
        _updateTimeout = -1;

    /*
    *  _parse
    *  value parse utility function
    */

    var _parse = function (value) {
        // parse value and convert NaN to 0
        return parseFloat(value) || 0;
    };

    /*
    *  _rows
    *  utility function returns array of jQuery selections representing each row
    *  (as displayed after float wrapping applied by browser)
    */

    var _rows = function (elements) {
        var tolerance = 1,
            $elements = $(elements),
            lastTop = null,
            rows = [];

        // group elements by their top position
        $elements.each(function () {
            var $that = $(this),
                top = $that.offset().top - _parse($that.css('margin-top')),
                lastRow = rows.length > 0 ? rows[rows.length - 1] : null;

            if (lastRow === null) {
                // first item on the row, so just push it
                rows.push($that);
            } else {
                // if the row top is the same, add to the row group
                if (Math.floor(Math.abs(lastTop - top)) <= tolerance) {
                    rows[rows.length - 1] = lastRow.add($that);
                } else {
                    // otherwise start a new row group
                    rows.push($that);
                }
            }

            // keep track of the last row top
            lastTop = top;
        });

        return rows;
    };

    /*
    *  _parseOptions
    *  handle plugin options
    */

    var _parseOptions = function (options) {
        var opts = {
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        };

        if (typeof options === 'object') {
            return $.extend(opts, options);
        }

        if (typeof options === 'boolean') {
            opts.byRow = options;
        } else if (options === 'remove') {
            opts.remove = true;
        }

        return opts;
    };

    /*
    *  matchHeight
    *  plugin definition
    */

    var matchHeight = $.fn.matchHeight = function (options) {
        var opts = _parseOptions(options);

        // handle remove
        if (opts.remove) {
            var that = this;

            // remove fixed height from all selected elements
            this.css(opts.property, '');

            // remove selected elements from all groups
            $.each(matchHeight._groups, function (key, group) {
                group.elements = group.elements.not(that);
            });

            // TODO: cleanup empty groups

            return this;
        }

        if (this.length <= 1 && !opts.target) {
            return this;
        }

        // keep track of this group so we can re-apply later on load and resize events
        matchHeight._groups.push({
            elements: this,
            options: opts
        });

        // match each element's height to the tallest element in the selection
        matchHeight._apply(this, opts);

        return this;
    };

    /*
    *  plugin global options
    */

    matchHeight.version = 'master';
    matchHeight._groups = [];
    matchHeight._throttle = 80;
    matchHeight._maintainScroll = false;
    matchHeight._beforeUpdate = null;
    matchHeight._afterUpdate = null;
    matchHeight._rows = _rows;
    matchHeight._parse = _parse;
    matchHeight._parseOptions = _parseOptions;

    /*
    *  matchHeight._apply
    *  apply matchHeight to given elements
    */

    matchHeight._apply = function (elements, options) {
        var opts = _parseOptions(options),
            $elements = $(elements),
            rows = [$elements];

        // take note of scroll position
        var scrollTop = $(window).scrollTop(),
            htmlHeight = $('html').outerHeight(true);

        // get hidden parents
        var $hiddenParents = $elements.parents().filter(':hidden');

        // cache the original inline style
        $hiddenParents.each(function () {
            var $that = $(this);
            $that.data('style-cache', $that.attr('style'));
        });

        // temporarily must force hidden parents visible
        $hiddenParents.css('display', 'block');

        // get rows if using byRow, otherwise assume one row
        if (opts.byRow && !opts.target) {

            // must first force an arbitrary equal height so floating elements break evenly
            $elements.each(function () {
                var $that = $(this),
                    display = $that.css('display');

                // temporarily force a usable display value
                if (display !== 'inline-block' && display !== 'flex' && display !== 'inline-flex') {
                    display = 'block';
                }

                // cache the original inline style
                $that.data('style-cache', $that.attr('style'));

                $that.css({
                    'display': display,
                    'padding-top': '0',
                    'padding-bottom': '0',
                    'margin-top': '0',
                    'margin-bottom': '0',
                    'border-top-width': '0',
                    'border-bottom-width': '0',
                    'height': '100px',
                    'overflow': 'hidden'
                });
            });

            // get the array of rows (based on element top position)
            rows = _rows($elements);

            // revert original inline styles
            $elements.each(function () {
                var $that = $(this);
                $that.attr('style', $that.data('style-cache') || '');
            });
        }

        $.each(rows, function (key, row) {
            var $row = $(row),
                targetHeight = 0;

            if (!opts.target) {
                // skip apply to rows with only one item
                if (opts.byRow && $row.length <= 1) {
                    $row.css(opts.property, '');
                    return;
                }

                // iterate the row and find the max height
                $row.each(function () {
                    var $that = $(this),
                        display = $that.css('display');

                    // temporarily force a usable display value
                    if (display !== 'inline-block' && display !== 'flex' && display !== 'inline-flex') {
                        display = 'block';
                    }

                    // ensure we get the correct actual height (and not a previously set height value)
                    var css = { 'display': display };
                    css[opts.property] = '';
                    $that.css(css);

                    // find the max height (including padding, but not margin)
                    if ($that.outerHeight(false) > targetHeight) {
                        targetHeight = $that.outerHeight(false);
                    }

                    // revert display block
                    $that.css('display', '');
                });
            } else {
                // if target set, use the height of the target element
                targetHeight = opts.target.outerHeight(false);
            }

            // iterate the row and apply the height to all elements
            $row.each(function () {
                var $that = $(this),
                    verticalPadding = 0;

                // don't apply to a target
                if (opts.target && $that.is(opts.target)) {
                    return;
                }

                // handle padding and border correctly (required when not using border-box)
                if ($that.css('box-sizing') !== 'border-box') {
                    verticalPadding += _parse($that.css('border-top-width')) + _parse($that.css('border-bottom-width'));
                    verticalPadding += _parse($that.css('padding-top')) + _parse($that.css('padding-bottom'));
                }

                // set the height (accounting for padding and border)
                $that.css(opts.property, (targetHeight - verticalPadding) + 'px');
            });
        });

        // revert hidden parents
        $hiddenParents.each(function () {
            var $that = $(this);
            $that.attr('style', $that.data('style-cache') || null);
        });

        // restore scroll position if enabled
        if (matchHeight._maintainScroll) {
            $(window).scrollTop((scrollTop / htmlHeight) * $('html').outerHeight(true));
        }

        return this;
    };

    /*
    *  matchHeight._applyDataApi
    *  applies matchHeight to all elements with a data-match-height attribute
    */

    matchHeight._applyDataApi = function () {
        var groups = {};

        // generate groups by their groupId set by elements using data-match-height
        $('[data-match-height], [data-mh]').each(function () {
            var $this = $(this),
                groupId = $this.attr('data-mh') || $this.attr('data-match-height');

            if (groupId in groups) {
                groups[groupId] = groups[groupId].add($this);
            } else {
                groups[groupId] = $this;
            }
        });

        // apply matchHeight to each group
        $.each(groups, function () {
            this.matchHeight(true);
        });
    };

    /*
    *  matchHeight._update
    *  updates matchHeight on all current groups with their correct options
    */

    var _update = function (event) {
        if (matchHeight._beforeUpdate) {
            matchHeight._beforeUpdate(event, matchHeight._groups);
        }

        $.each(matchHeight._groups, function () {
            matchHeight._apply(this.elements, this.options);
        });

        if (matchHeight._afterUpdate) {
            matchHeight._afterUpdate(event, matchHeight._groups);
        }
    };

    matchHeight._update = function (throttle, event) {
        // prevent update if fired from a resize event
        // where the viewport width hasn't actually changed
        // fixes an event looping bug in IE8
        if (event && event.type === 'resize') {
            var windowWidth = $(window).width();
            if (windowWidth === _previousResizeWidth) {
                return;
            }
            _previousResizeWidth = windowWidth;
        }

        // throttle updates
        if (!throttle) {
            _update(event);
        } else if (_updateTimeout === -1) {
            _updateTimeout = setTimeout(function () {
                _update(event);
                _updateTimeout = -1;
            }, matchHeight._throttle);
        }
    };

    /*
    *  bind events
    */

    // apply on DOM ready event
    $(matchHeight._applyDataApi);

    // update heights on load and resize events
    $(window).bind('load', function (event) {
        matchHeight._update(false, event);
    });

    // throttled update heights on resize events
    $(window).bind('resize orientationchange', function (event) {
        matchHeight._update(true, event);
    });

    $('.non-editable').attr('disabled', 'disabled');
});

//function to activate bootstrap tab by tabId
var activateBootStrapTab = function (tabId) {
    if (!tabId)
        return;

    if (tabId.toString().indexOf('#') === -1)
        tabId = '#' + tabId;

    $('.nav-tabs a[href=' + tabId + ']').tab('show');

}

var getActiveHash = function () {
    var activeHash = "";
    $('.nav-tabs .active a').each(function () {
        activeHash += $(this).attr('href');
    });
    return activeHash;
}

function decodeHtmlEntities(sData) {
    if (sData) {
        if (typeof (sData) == "number")
            sData = sData.toString();

        //if no '&' is found, no need to decode return original string
        if (sData.indexOf('&') === -1 ) {
            return sData;
        }

        //replace all occurence of meta-characters
        return sData.replace(/&([^\s]*?);/g, function (match, match2) {
            if (match.substr(1, 1) === '#') {
                if (match2.indexOf('&quot') > -1) {
                    return String.fromCharCode(Number(match2.substr(1).split('&')[0])) + '"';
                }
                else {
                    return String.fromCharCode(Number(match2.substr(1)));
                }
            }
            else {
                //div for decoder
                var n = document.createElement('div');

                //write data to div and return decoded html
                n.innerHTML = match;
                return n.childNodes[0].nodeValue;
            }
        });
    }
}

/**
    * Decode HTML entities
    *  @method  decodeHtml
    *  @param   {String} sData encoded string
    *  @param   {String} donotReplaceWhitespaces if true do not replace whitespaces
    *  @returns {String} decoded string
    *  @public 
*/
function decodeHtml(sData, donotReplaceWhitespaces) {
    //decode and handle whitespace characters for enter and tab
    if (sData) {
        if (donotReplaceWhitespaces)
            return decodeHtmlEntities(sData);

        return decodeHtmlEntities(sData)
            .replace(/\n/g, "\\n")
            .replace(/\r/g, "\\r")
            .replace(/\t/g, "\\t")
            ;
    }
    else return sData;

}

//For those fields that are marked as allow-html, if any html encoded characters are found then decode them to their symbols
$(".allow-html").on("change", function () {
    console.log($(this).val());
    var value = $(this).val();
    if (!value.includes('&amp;')) {
        $(this).val(decodeHtml($(this).val(), true));
    }
   
});

/**
    * Select only given columns from the multi-dimensional array
    *  @method  select
    *  @param   {Object} columns [array of columns to be selected]
    *  @param   {Object} replacementColumns [array of columns to be replaced with header], if null gets value from columns
    *  @returns {Object} [filtered array with specified columns]
    *  @public 
*/
Array.prototype.select = function (columns, replacementColumns) {
    var array = this;
    var myData = [];
    replacementColumns = replacementColumns || columns;
    $.grep(array, function (element, index) {
        var data = {};
        for (var i = 0; i < columns.length; i++) {
            data[replacementColumns[i]] = element[columns[i]];
        }
        myData.push(data);
    });
    return myData;
}

/**
    * Select only given columns from the multi-dimensional array
    *  @method  select
    *  @param   {Object} columns [array of columns to be selected]
    *  @param   {Object} replacementColumns [array of columns to be replaced with header], if null gets value from columns
    *  @param   {String} parentNode [parent node]
    *  @returns {Object} [filtered array with specified columns]
    *  @public 
*/
Array.prototype.selectXml = function (columns, replacementColumns, parentNode) {
    var array = this;
    var xmlData = "";
    replacementColumns = replacementColumns || columns;
    $.grep(array, function (element, index) {
        var data = "";
        for (var i = 0; i < columns.length; i++) {
            data += "<{0}><![CDATA[{1}]]></{0}>".format(replacementColumns[i], element[columns[i]]);
        }
        xmlData += "<{0}>{1}</{0}>".format(parentNode, data);
    });
    return xmlData;
}




//collapsible menu

$(document).ready(function () {

    $('#NotificationCount').hide();
    getNotification();
    setInterval(function () {
        getNotification();
    }, 13000); //3 seconds

    $('.expandcollapse').click(function () {

        var newstate = $(this).attr('state') ^ 1,
            icon = newstate ? "minus" : "plus",
            text = newstate ? "Collapse" : "Expand";

        // if state=0, show all the accordion divs within the same block (in this case, within the same section)
        if ($(this).attr('state') === "0") {
            console.log('1');
            $(this).siblings('div').find('div.panel-body:not(.in)').collapse('show');
        }
        // otherwise, collapse all the divs
        else {
            console.log('2');
            $(this).siblings('div').find('div.panel-body.in').collapse('hide');
        }

        $(this).html("<i class=\"fa fa-" + icon + "-square\"></i> " + text + " All");

        $(this).attr('state', newstate)

    });

    $('a[data-toggle="tab"]').on('shown', function (e) {

        var myState = $(this).attr('state'),
            state = $('.expandcollapse').attr('state');

        if (myState != state) {
            toggleTab($(this).prop('hash'));
            $(this).attr('state', state);
        }

    })

    function toggleTab(id) {

        $(id).find('.collapse').each(function () {
            $(this).collapse('toggle');
        });

    }
});


function GetTodayDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = mm + '/' + dd + '/' + yyyy;
    return today;

}



function getCurrentDateTime() {
    var CurrentDate = new Date().toLocaleDateString();
    var CurrentTime = (new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds() + "." + new Date().getMilliseconds());
    return CurrentDate + ' ' + CurrentTime;
}

function randomWholeNum() {

    // Only change code below this line.

    return Math.floor(Math.random() * 20);
}

