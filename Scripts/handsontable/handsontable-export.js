﻿var handsontable2ExportFormat = {

    string: function (instance, exportFormat, showRowHeader) {

        var headers = instance.getColHeader();
        var separator = ",";
        if (exportFormat === "tsv")
            separator = '\t';
        else if (exportFormat === 'csv')
            separator = ',';
        else if(exportFormat !== '') {
            separator = exportFormat;
        }

        var csv = "";
        if (showRowHeader == true) {
            var accumulatedHeaders = "";
            $.each(headers, function (key, value) {
                accumulatedHeaders += "\"" + value + "\"" + ",";

            });
            accumulatedHeaders = accumulatedHeaders.slice(0, -1);
            //     console.log(accumulatedHeaders);
            csv += accumulatedHeaders + "\n";


        }
      //  csv += headers.join(separator) + "\n";

        for (var i = 0; i < instance.countRows() ; i++) {
            var row = [];
            for (var h in headers) {
                var prop = instance.colToProp(h);
                var value = instance.getDataAtRowProp(i, prop);
              
                if (typeof value !== "object" && value !== "<undefined></undefined>") {
                    if (value !== null && value !== 'undefined' && typeof value !== 'undefined') {
                        value = value.toString();
                        // row.push("\"" + value + "\""); 
                        var addedDoubleQuote = false;
                        if (value.indexOf('"') !== -1)
                        {
                            value = value.replaceAll('"', "\"\"");
                            //value = value.replace('"', "\"\"");
                            value = "\"" + value + "\"";
                            addedDoubleQuote = true;
                        }
                        if (value.indexOf(',') !== -1 && addedDoubleQuote === false) {
                            value = "\"" + value + "\"";
                        }
                        if (headers[h] === "Local Part Number" || headers[h] === "Vendor Part Number" || headers[h] === "New Vendor Part Number") {
                            value = ("\"" + "\t" + value + "\"");
                        }
                        row.push(value);
                    }
                    else {
                        row.push("\"\"");
                    }
                }
            }

            csv += row.join(separator);
            csv += "\n";
        }

        return csv;
    },

    download: function (instance, filenameWithExtension, exportFormat, showRowHeader) {
        ///added BOM to solve encoding issue for special character in csv.
        var csv ="\ufeff";
        csv = csv + handsontable2ExportFormat.string(instance, exportFormat, showRowHeader);

        var link = document.createElement("a");
          //link.setAttribute("href", "data:text/plain;charset=utf-8," + csv);
        link.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(csv));
        link.setAttribute("download", filenameWithExtension);

        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
}

var handsontable2EqualsToFormat = {

    string: function (instance, exportFormat, showRowHeader) {

        var headers = instance.getColHeader();
        var separator = ",";
        if (exportFormat === "tsv")
            separator = '\t';
        else if (exportFormat === 'csv')
            separator = ',';
        else if (exportFormat !== '') {
            separator = exportFormat;
        }

        var csv = "";
        if (showRowHeader === true) {
            var accumulatedHeaders = "";
            $.each(headers, function (key, value) {
                accumulatedHeaders += "=\"" + value + "\"" + ",";
               
            });
            accumulatedHeaders = accumulatedHeaders.slice(0, -1);
       //     console.log(accumulatedHeaders);
            csv += accumulatedHeaders + "\n";
        }
      

        for (var i = 0; i < instance.countRows() ; i++) {
            var row = [];
            for (var h in headers) {
                var prop = instance.colToProp(h);
                var value = instance.getDataAtRowProp(i, prop);

                if (value !== null && value !== "undefined" && typeof value !== 'undefined') {

                    if (jQuery.type(value) === "string") {
                        value = value.replace(/,/g, "ˏ");
                    }
                }
                if (value === null || typeof value === 'undefined') {
                    value = "";
                }


                row.push("=\"" + value + "\"");
            }

            csv += row.join(separator);
            csv += "\n";
        }

        return csv;
    },

    download: function (instance, filenameWithExtension, exportFormat, showRowHeader) {


        var csv = handsontable2EqualsToFormat.string(instance, exportFormat, showRowHeader);

        var link = document.createElement("a");
        link.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(csv));
        link.setAttribute("download", filenameWithExtension);

        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
}


String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};