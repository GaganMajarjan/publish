﻿/*  
    Line no. 22843 on jquery.handsontable.full.js
    Need to update the function and remove 100 multiplier if new version of handsontable is used
*/

function formatPercentage(n, format, roundingFunction) {
    var space = '',
      output,
      //value = n._value * 100; <----- changed: removed * 100
      value = n._value;

    // check for space before %
    if (format.indexOf(' %') > -1) {
        space = ' ';
        format = format.replace(' %', '');
    } else {
        format = format.replace('%', '');
    }

    output = formatNumber(value, format, roundingFunction);

    if (output.indexOf(')') > -1) {
        output = output.split('');
        output.splice(-1, 0, space + '%');
        output = output.join('');
    } else {
        output = output + space + '%';
    }

    return output;
}