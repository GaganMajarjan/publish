﻿/*
    UDH implementation of papaparse
    http://papaparse.com/

    Dependent on: udh-handsontable.js (if you want to display result to handsontable)

*/

var UdhPapaParse = {
    /**
     * @method   getErrorFn
     * @param    null
     * @returns  {Function} error function
     */
    getErrorFn: function() {

        return function errorFn(error, file) {

            alert("getErrorFn");

            //console.log("PapaParse errorFn");
            //console.log(error);
            //console.log(file);
            alertmsg("red", "Sorry! Error occurred");
            return;
            ////console.log("ERROR:", error, file);
        }
    },

    /**
     * @method  getCompleteFn
     * @param    null
     * @returns {Function}  complete function
     */
    getCompleteFn: function (results) {

        return function() {

            //var results = arguments[0];

            console.log('results');
            console.log(results);

            if (results.errors.length > 0) {
                var errorMessage = "";
                for (var i = 0; i < results.errors.length; i++) {
                    console.log(results.errors[i]);
                    errorMessage += "{0} (Row#{1})".format(results.errors[i].message, results.errors[i].row) + "; ";
                }
                alertmsg("red", errorMessage);
                return;
            }

            var headerJson = results.meta.fields;
            //console.log(headerJson);
            fileData = results.data || [];
            //console.log(fileData);

            callback(fileData);

            //to callback
            $.each(headerJson,
                function(key, value) {
                    header.push(value);
                    columns.push({ "title": value, "data": value, "type": "text" });
                });
           
        }
    },

    /**
     * @method  getConfiguration
     * @param   {Object} options
     * @returns {Object} papa-parse configuration 
     */
    getConfiguration: function(options) {

        options = options || {};
        var defaultOptions = {
            delimiter: "", //auto-detect
            newline: "", //auto-detect
            header: true,
            dynamicTyping: "",
            preview: parseInt(0),
            step: undefined,
            encoding: "",
            worker: "",
            comments: "",
            //complete: this.getCompleteFn(),
            //error: this.getErrorFn(),
            download: "",
            fastMode: "",
            skipEmptyLines: true,
            chunk: undefined,
            beforeFirstChunk: undefined
        };

        $.extend(true, defaultOptions, options);

        console.log(defaultOptions);
        //trigger file change option for papaparse

        defaultOptions.fileSelector.change(function() {

            var file = defaultOptions.fileSelector[0].files;

            if (file.length > 0) {
            
                /*
                defaultOptions.fileSelector
                    .parse({
                        config: defaultOptions
                    });*/

                /*
                Papa.parse(file,
                {
                    config: defaultOptions
                });*/

                alert("going to parse");

                defaultOptions.fileSelector.parse({
                    config: defaultOptions,
                    before: function (file, inputElem) {
                        // executed before parsing each file begins;
                        // what you return here controls the flow
                    },
                    //complete: defaultOptions.complete(results),
                    //error: defaultOptions.error,
                    error: function (err, file, inputElem, reason) {
                        // executed if an error occurs while loading the file,
                        // or if before callback aborted for some reason
                    },
                    complete: function () {
                        alert("Complete");
                        //console.log(file, results);
                        // executed after all files are complete
                    }
                });

                alert("parsed");

            }


        });

        return defaultOptions;
    }

    //-------------testing---------------

    /*

    ,test: function() {

        var fileSelector = $("#file");
        var fileData = [];
        var container = document.getElementById("hot-bulk-update");
        var columnsHeader = [];
        var columns = [];
        var hot;

        //callback, show data to handsontable
        var renderHandsontable = function (container, data) {

            var handsontableSettings = UdhHandsonTable.getSettings();

            hot = new Handsontable(container, handsontableSettings);

            if (data.length > 1 && hot.isEmptyRow(data.length - 1)) {
                //remove last row if last row is empty row (handsontable)
                hot.alter('remove_row', parseInt(data.length - 1));
            }

            hot.render();
        }

        var papaParseOptions = {
            fileSelector: fileSelector,
            fileData: fileData,
            header: columnsHeader,
            columns: columns,
            callback: renderHandsontable(container, fileData)
        };

        UdhPapaParse.getConfiguration(papaParseOptions);


    }

    */

}
