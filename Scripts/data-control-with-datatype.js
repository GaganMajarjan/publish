﻿//construct control elements as per data type
function ConstructControlForDataType(currentIndex, dataType, cssClass, hasEmptyValue, allowedValueSdtId) {

    if (dataType === "DATETIME" || dataType === "DATE") {

        $("#itemTextbox_" + currentIndex)
            .show()
            .attr("class", "form-control " + cssClass)
            .datepicker();
        $("#itemCheckbox_" + currentIndex).hide();
        if ($("#drop_" + currentIndex).data("select2")) {
            $("#drop_" + currentIndex).select2("destroy");
            $("#drop_" + currentIndex).hide();

        } else {
            $("#drop_" + currentIndex).hide();

        }



        if (hasEmptyValue === "true")
            $("#itemTextbox_" + currentIndex)
                .val("");

    }
    else if (dataType === "BIT") {
        $("#itemCheckbox_" + currentIndex).show();
        $("#itemTextbox_" + currentIndex).hide();
        if ($("#drop_" + currentIndex).data("select2")) {
            $("#drop_" + currentIndex).select2("destroy");
            $("#drop_" + currentIndex).hide();

        } else {
            $("#drop_" + currentIndex).hide();

        }


    } else {

        if (allowedValueSdtId != null && allowedValueSdtId !== 0) {

            $("#itemCheckbox_" + currentIndex).hide();
            $("#itemTextbox_" + currentIndex).hide();
            $("#drop_" + currentIndex)
                .show();


            $.ajax({
                url: urlGetStaticDataValues,
                type: 'POST',
                data: { "staticDataTypeId": allowedValueSdtId },
                dataType: "json",
                beforeSend: function () {
                    SGloading('Loading');
                },
                success: function (result) {

                    if (result === "NO_DATA") {
                        alertmsg("red", "No options exists for given static data type, please add statice values first.");
                        return;
                    }

                    var jsonResult = $.parseJSON(result);

                    //Create a new select element and then append all options. Once loop is finished, append it to actual DOM object.
                    //This way we'll modify DOM for only one time!

                    var selectOptions = $('<select>');
                    selectOptions.append(
                        $('<option></option>').val("").html(pleaseSelectText));

                    $.each(jsonResult, function (index, val) {

                        selectOptions.append(
                                $('<option></option>').val(val.Id).html(val.Name)
                            );
                    });

                    $("#drop_" + currentIndex).append(selectOptions.html());


                    if (kitsVm.CustomTemplates[currentIndex] != null) {
                        var dropSelectedValue = kitsVm.CustomTemplates[currentIndex].FieldValue;
                        ////console.log(dropSelectedValue);
                        if (dropSelectedValue != null && dropSelectedValue !== "" && dropSelectedValue !== 0) {

                            viewModel.CustomTemplates()[currentIndex].AllowedValuesSdtId(dropSelectedValue);
                        }

                    }

                    $("#drop_" + currentIndex).select2({ width: '100%' });

                },
                complete: function () {
                    SGloadingRemove();
                },
                error: function (err) {

                }
            });
        } else {


            $("#itemTextbox_" + currentIndex)
                .datepicker("destroy")
                .show()
                .attr("class", "form-control " + cssClass);
            $("#itemCheckbox_" + currentIndex).hide();
            if ($("#drop_" + currentIndex).data("select2")) {
                $("#drop_" + currentIndex).select2("destroy");
                $("#drop_" + currentIndex).hide();

            } else {
                $("#drop_" + currentIndex).hide();

            }


            if (hasEmptyValue === "true")
                $("#itemTextbox_" + currentIndex)
                    .val("");

        }
    }

}

//get data type of elements
function RenderElementWithDataType(customFieldId, isForAll) {

    if (isForAll === "true") {

        for (var i = 0; i < viewModel.CustomTemplates().length; i++) {

            customFieldId = viewModel.CustomTemplates()[i].CustomField();
            var returnedData = $.grep(customFieldsAdditionalViewBag, function (element, index) {
                return element.Id === customFieldId;
            });

            if (returnedData.length > 0) {
                var selectedDataType = returnedData[0].DataType;
                var selectedAllowedValueSdtId = returnedData[0].AllowedValuesSdtId;

                if (selectedDataType == null)
                    selectedDataType = "VARCHAR(MAX)";
                ConstructControlForDataType(i, selectedDataType, selectedDataType.replace(/ *\([^)]*\) */g, "").toLowerCase() + "DataType", "false", selectedAllowedValueSdtId);
            }

        }

    } else {
        var currentIndex = 0;
        for (var i = 0; i < viewModel.CustomTemplates().length; i++) {
            if (viewModel.CustomTemplates()[i].CustomField() === customFieldId) {
                currentIndex = i;
                break;
            }
        }


        //Search for DataType 
        var returnedData = $.grep(customFieldsAdditionalViewBag, function (element, index) {
            return element.Id === customFieldId;
        });

        ////////console.log(returnedData.length);

        if (returnedData.length > 0) {
            var selectedDataType = returnedData[0].DataType;
            var selectedAllowedValueSdtId = returnedData[0].AllowedValuesSdtId;
            ConstructControlForDataType(currentIndex, selectedDataType, selectedDataType.replace(/ *\([^)]*\) */g, "").toLowerCase() + "DataType", "true", selectedAllowedValueSdtId);
        }
    }

}
