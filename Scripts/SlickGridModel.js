﻿function SlickGridModel(divId, dataList, columnList, AllSortable_IMP, CheckBox, ColumnSelector, Paging, filterHeader, forceFit) {
	if (Paging) {
		$("#pager_" + divId).remove();
		$("#" + divId).after(" <div id='pager_" + divId + "'" + "style='width:100%;height:30px;'></div>");
	}
	var data = [];
	var columns = [];
	var columnFilters = {};
	if (CheckBox) {
		var checkboxSelector = new Slick.CheckboxSelectColumn({
				cssClass : "slick-cell-checkboxsel"
			});
		columns.push(checkboxSelector.getColumnDefinition());
	}
	$.each(columnList, function (index, value) {
		columns.push({
			id : value,
			name : value,
			field : value,
			sortable : AllSortable_IMP,
			editor : Slick.Editors.Text
		});
	});
	function dayFormatter(row, cell, value, columnDef, dataContext) {
		return value + ' days';
	}
	function dateFormatter(row, cell, value, columnDef, dataContext) {
		return value.getMonth() + '/' + value.getDate() + '/' + value.getFullYear();
	}
	var options = {
		editable : filterHeader,
		enableColumnReorder : false,
		multiColumnSort : AllSortable_IMP,
		enableCellNavigation : true,
		forceFitColumns : forceFit,
		showHeaderRow : filterHeader,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	$(function () {
		data = dataList;
		dataList = "";
		columnList = "";
		var dataView = new Slick.Data.DataView();
		eval("var grid" + divId + "= new Slick.Grid('#' + divId, dataView, columns, options)");
		if (Paging) {
			var pager = new Slick.Controls.Pager(dataView, eval("grid" + divId), $("#pager_" + divId));
		}
		if (ColumnSelector) {
			var columnpicker = new Slick.Controls.ColumnPicker(columns, eval("grid" + divId), options);
		}
		eval("grid" + divId).setSelectionModel(new Slick.RowSelectionModel());
		if (CheckBox) {
			eval("grid" + divId).registerPlugin(checkboxSelector);
		}

		function activaTab(tab) {
			$('.nav-tabs a[href="#' + tab + '"]').tab('show');
		};

		
		$('#btnUpdateDestTbl').click(function () {
		    var DestTable = "";
		    var multiDestTable = "";
		    if (divId == "DestinationGrid") {
		        var selectedrow = eval("grid" + divId).getSelectedRows();
		        for (var i = 0; i <= selectedrow.length; i++) {
		            var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
		            if (d != null && d != "undefined") {
		                DestTable = d.Name;
		                multiDestTable += d.Name + ",";
		            }
		        }
		        if (multiDestTable == "") {
		            alertmsg("red", "Please select destination table to update.");
		        }
		        else if (multiDestTable.match(/,/g).length > 1) {
		            alertmsg("red", "Please select single destination table.");
		        }
		        else if (DestTable != "") {
		            window.location = updateDestTableurl + "?DestTbl="+ DestTable + "";
		        }
		    }
		});

        $('#btnColumnsImportSource').click(function (e) {
			var importSourceId = "";
			if (divId == "ImportSourceGrid") {
				var selectedrow = eval("grid" + divId).getSelectedRows();
				for (var i = 0; i <= selectedrow.length; i++) {
					var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
					if (d != null && d != "undefined") {
					    importSourceId = d.id;
					    if (d.NoOfColumns == 0) {
					        alertmsg("red", "Please update number of columns.");
					    }
					    else {
					        window.location = ImpSrcColUrl + "?ImpSrcId=" + importSourceId + "";
					    }
					}
					else if (d == null)
					{
					    alertmsg("red", "Please select import source to configure columns.");
					    e.preventDefault();
					}
					break;
				}
				
			}
        });

		$('#btnUpdateImportSource').click(function () {
			var ImportSource = "";
			if (divId == "ImportSourceGrid") {
				var selectedrow = eval("grid" + divId).getSelectedRows();
				for (var i = 0; i <= selectedrow.length; i++) {
				    var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
				    if (d == null)
				    {
				        alertmsg("red", "Please select import source to update.");
				    }
					else if (d != null && d != "undefined") {
						document.getElementById('ErrorFilePathUpdate').value = d.ErrorFilePath;
						document.getElementById('FileNameFormatUpdate').value = d.FileNameFormat;
						document.getElementById('FilePathUpdate').value = d.FilePath;
						document.getElementById('NoOfVariableUpdate').value = d.NoOfColumns;
						document.getElementById('sorcenameUpdate').value = d.SourceName;
						$("label.vendorNameUpdate").replaceWith("<label class='vendorNameUpdate'>" + d.Vendor + "</label>");
						$('#delimiterUpdate').val(d.DelimiterId);
						$('#terminatorUpdate').val(d.Terminator);
						$('#startrowUpdate').val(d.StartRow);
						$("#UpdateImportSource").click();
						document.getElementById('importSourceID').value = d.id;
						document.getElementById('vendorUpdatetextBox').value = d.Vendor;
						document.getElementById('StartrowUpdatetextBox').value = d.StartRow;
						document.getElementById('terminatorUpdatetextBox').value = d.Terminator;
						document.getElementById('delimiterUpdatetextBox').value = d.DelimiterId;
						break;
					}
				}
			}
		});
		$('#butnDeleteImpSource').click(function () {
			var ImpSourceIds = "";
			if (divId == "ImportSourceGrid") {
				var selectedrow = eval("grid" + divId).getSelectedRows();
				for (var i = 0; i <= selectedrow.length; i++) {
					var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
					if (d != null && d != "undefined")
						ImpSourceIds += d.id + "_";
				}
				if (ImpSourceIds == "") {
				    alertmsg("red", "Please select import source to delete.");
				}
				else {
				    document.getElementById('ImpSrcIdsforDelete').value = ImpSourceIds;
				    $("#ModelDeleteImpSrcHdnBtn").click();
				}
				
				ImpSourceIds = "";
			}
		});
        $('#butnDeleteDestinationTable').click(function () {
		    var destinationTableId = "";
		    if (divId == "DestinationGrid") {
		        var selectedrow = eval("grid" + divId).getSelectedRows();
		        for (var i = 0; i <= selectedrow.length; i++) {
		            var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
		            if (d != null && d != "undefined") {
		                destinationTableId += d.Name + ",";
		               // console.log(destinationTableId);
		            }
		        }
		        if (destinationTableId == "") {
		            alertmsg("red", "Please select destination table to delete.");
		        }
		        else {
		            document.getElementById('DestinationTableNamesforDelete').value = destinationTableId;
		            $("#ModelDeleteDestTableHdnBtn").click();
		        
		        }
		        destinationTableId = "";
		    }
        	});

        //user role mapping buttons
        $('#addroletoUserdd').click(function () {
        	    var username = "";
        	    var multiUser = "";
        	    if (divId == "myUserRoleGrid") {
        	        var selectedrow = eval("grid" + divId).getSelectedRows();
        	        for (var i = 0; i <= selectedrow.length; i++) {
        	            var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
        	            if (d != null && d != "undefined") {
        	                multiUser += d.UserName + "_";
        	                username = d.UserName;
        	            }
        	        }
        	        if (multiUser == "") {
        	            alertmsg("red", "Please select user to assign role.");
        	        }
        	        else if (multiUser.match(/_/g).length > 1) {
        	            alertmsg("red", "Please select single user to assign role.");
        	        }
        	        else {
        	            document.getElementById('assignedroletxtboxusername').value = username;
        	            $("label.selectedusername").replaceWith("<label class='selectedusername'>" + username + "</label>");
        	            $("#ModelAssignRoleHdnBtn").click();
        	        }

        	        username = "";

        	    }
        	});
		$('#RemUserFromRole').click(function () {
			var removeRole = "";
			if (divId == "myUserRoleGrid") {
				var selectedrow = eval("grid" + divId).getSelectedRows();
				for (var i = 0; i <= selectedrow.length; i++) {
					var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
					if (d != null && d != "undefined") {
					    removeRole += d.UserName + "_";
					}
				}
				if (removeRole == "") {
				    alertmsg("red", "Please select user to remove role.");
				}
				else {
				    document.getElementById('userIdsforRemoveRoles').value = removeRole;
				    $("#ModelRemoveRoleHdnBtn").click();
				}

				removeRole = "";
				
			}
		});
		$('#btnsaverole').click(function () {
			var RoleName = $("#textRolename").val();
			if (divId == "myRolesGrid") {
				$.ajax({
				    url: createRoleUrl,
					data : {
						rolename : RoleName
					},
					type : 'POST',
					cache : false,
					success : function (data) {
						if (data = "ok") {
							location.reload();
							activaTab(RolesManage);
						}
					}
				});
			}
		});
		$('#btnUpdateRole').click(function () {
		    var role = "";
		    var multiRole = "";
			if (divId == "myRolesGrid") {
				var selectedrow = eval("grid" + divId).getSelectedRows();
				for (var i = 0; i <= selectedrow.length; i++) {
					var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
					if (d != null && d != "undefined") {
					    role = d.Name;
					    multiRole += d.Name + "_";
					}
				}
				if (multiRole == "") {
				    alertmsg("red", "Please select role to update.");
				}
				else if (multiRole.match(/_/g).length > 1) {
				    alertmsg("red", "Please select single role.");
				}
				else if (role != "") {
				//	$("label.RoletoUpdate").replaceWith("<label class='RoletoUpdate'>" + role + "</label>");
				    document.getElementById('OldRolename').value = role;
				    document.getElementById('newRoleName').value = role;
				    
					$("#ModelupdateRole").click();
				}
			}
		});
		$('#btnUpdateRoleModel').click(function () {
			var newRoleName = $("#newRoleName").val();
			var oldRoleName = $("#OldRolename").val();
			if (divId == "myRolesGrid") {
			    $.ajax({
			        url: updateRoleUrl + "",
			        data: {
			            OldroleName: oldRoleName,
			            NewroleName: newRoleName
			        },
			        type: 'POST',
			        cache: false,
			        success: function (data) {
			            if (data = "ok") {
			                location.reload();
			            }
			        }
			    });
			}
		});
        $('#btnDeleteRoles').click(function () {
			var roleId = "";
			if (divId == "myRolesGrid") {
				var selectedrow = eval("grid" + divId).getSelectedRows();
				for (var i = 0; i <= selectedrow.length; i++) {
					var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
					if (d != null && d != "undefined")
						roleId += d.id + "_";
				}
				if (roleId == "") {
				    alertmsg("red", "Please select role to delete.");
				}
				else {
				    document.getElementById('roleIdsforDelete').value = roleId;
				      $("#ModelDeleteRoleHdnBtn").click();
				}
			
				roleId = "";
			}
        });


       
        $('#btnDeleteUser').click(function () {
			var userId = "";
			if (divId == "myGrid") {
				var selectedrow = eval("grid" + divId).getSelectedRows();
				for (var i = 0; i <= selectedrow.length; i++) {
					var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
					if (d != null && d != "undefined")
						userId += d.id + "_";
				}
				if (userId == "") {
				    alertmsg("red", "Please select user to delete.");
				}
				else {
				    document.getElementById('userIdsforDelete').value = userId;
				    $("#ModelDeleteUserHdnBtn").click();
				}
				
				userId = "";
			}
		});
        $('#btnUpdateUser').click(function () {
		    var username = "";
		    var singleUser = "";
			if (divId == "myGrid") {
				var selectedrow = eval("grid" + divId).getSelectedRows();
				for (var i = 0; i <= selectedrow.length; i++) {
				    var d = eval("grid" + divId).getData().getItem(selectedrow[i]);
				    if (d != null && d != "undefined"){
				        username += d.LoginName + "_";
				        singleUser = d.LoginName;
                    }
                }
               if (username == "") {
				    alertmsg("red", "Please select user to update.");
				}
				else if (username.match(/_/g).length > 1) {
				    alertmsg("red", "Please select single user.");
				}
				else if (singleUser != "") {
				  //  $("label.usernametoUpdate").replaceWith("<label class='usernametoUpdate'>" + singleUser + "</label>");
				    document.getElementById('OldName').value = singleUser;
				    document.getElementById('newUserName').value = singleUser;
				    $.getJSON(getUserDetailUpdateUrl + "?LoginName=" + singleUser, function (dataList) {
				       
				        if (dataList != "NoData") {
				            document.getElementById('FirstNameUpdate').value = dataList.FirstName;
				            document.getElementById('MiddleNameUpdate').value = dataList.MiddleName;
				            document.getElementById('LastNameUpdate').value = dataList.LastName;
				            document.getElementById('TitleUpdate').value = dataList.Title;
				            document.getElementById('EmailUpdate').value = dataList.Email;
				            document.getElementById('MobileUpdate').value = dataList.MobileTel;
				            document.getElementById('Address1Update').value = dataList.Address1;
				            document.getElementById('CityUpdate').value = dataList.City;
				            document.getElementById('StateUpdate').value = dataList.State;
				            document.getElementById('ZipUpdate').value = dataList.Zipcode;
				            document.getElementById('FaxUpdate').value = dataList.Fax;
				        }
				    });

					$("#ModelupdateUser").click();
				}
			}
		});

		if (Paging || AllSortable_IMP) {
			dataView.onRowCountChanged.subscribe(function (e, args) {
				eval("grid" + divId).updateRowCount();
				eval("grid" + divId).render();
			});
			dataView.onRowsChanged.subscribe(function (e, args) {
				eval("grid" + divId).invalidateRows(args.rows);
				eval("grid" + divId).render();
			});
		}
		if (filterHeader) {
			function filter(item) {
				for (var columnId in columnFilters) {
					if (columnId !== undefined && columnFilters[columnId] !== "") {
						var c = eval("grid" + divId).getColumns()[eval("grid" + divId).getColumnIndex(columnId)];
						if (item[c.field] != columnFilters[columnId]) {
							return false;
						}
					}
				}
				return true;
			}
			$(eval("grid" + divId).getHeaderRow()).delegate(":input", "change keyup", function (e) {
				var columnId = $(this).data("columnId");
				if (columnId != null) {
					columnFilters[columnId] = $.trim($(this).val());
					dataView.refresh();
				}
			});
			eval("grid" + divId).onHeaderRowCellRendered.subscribe(function (e, args) {
				$(args.node).empty();
				$("<input type='text'>")
				.data("columnId", args.column.id)
				.val(columnFilters[args.column.id])
				.appendTo(args.node);
			});
		}
		if (AllSortable_IMP) {
			eval("grid" + divId).onSort.subscribe(function (e, args) {
				var cols = args.sortCols;
				data.sort(function (dataRow1, dataRow2) {
					for (var i = 0, l = cols.length; i < l; i++) {
						var field = cols[i].sortCol.field;
						var sign = cols[i].sortAsc ? 1 : -1;
						var value1 = dataRow1[field],
						value2 = dataRow2[field];
						var result = (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
						if (result != 0) {
							return result;
						}
					}
					return 0;
				});
				eval("grid" + divId).init();
				dataView.beginUpdate();
				dataView.setItems(data);
				if (filterHeader) {
					dataView.setFilter(filter);
				}
				dataView.endUpdate();
			});
		}
		eval("grid" + divId).init();
		dataView.beginUpdate();
		dataView.setItems(data);
		if (filterHeader) {
			dataView.setFilter(filter);
			dataView.setFilterArgs(0);
		}
		dataView.endUpdate();
	});

    //$('#' + divId).height($('#' + divId).height() + 25);
}