﻿
var calculateCost = function (sellingPrice, grossMarginPercent) {
    if (sellingPrice === 0.0 || grossMarginPercent === 100.0)
        return 0.0;
    var cost = sellingPrice * (1 - grossMarginPercent / 100);

    if (isNaN(cost))
        return 0.0;
    if (!isFinite(cost))
        return 0.0;

    return cost;
}

var calculateSellingPrice = function (cost, grossMarginPercent) {
    if (cost === 0.0) {
        return 0.0;
    }
    if (grossMarginPercent === 100.0) {
        return 0.0;
    }

    if (grossMarginPercent === null || grossMarginPercent === "") {
        return 0.0;
    }

    if (grossMarginPercent === 0)
        return cost;

    var sellingPrice = cost / (1 - (grossMarginPercent / 100));

    if (isNaN(sellingPrice))
        return 0.0;
    if (!isFinite(sellingPrice))
        return 0.0;

    return sellingPrice;
}

var calculateGrossMarginPercentFromSellingPrice = function (cost, sellingPrice) {
    //return margin 0 when selling price is 0
    if (sellingPrice === 0.0)
        return 0;
    if (cost === 0.0 || sellingPrice === 0.0) {
        return 100.0;
    }

    if (cost === sellingPrice)
        return 0;

    var grossMarginPercent = ((sellingPrice - cost) / sellingPrice) * 100;

    if (isNaN(grossMarginPercent))
        return 0.0;
    if (!isFinite(grossMarginPercent))
        return 0.0;

    return grossMarginPercent;
}
var calculateGrossMarginPercent = function (cost, sellingPrice) {
    if (cost === 0.0 || sellingPrice === 0.0) {
        return 100.0;
    }

    if (cost === sellingPrice)
        return 0;

    var grossMarginPercent = ((sellingPrice - cost) / sellingPrice) * 100;

    if (isNaN(grossMarginPercent))
        return 0.0;
    if (!isFinite(grossMarginPercent))
        return 0.0;

    return grossMarginPercent;
}

var convertToGrossMarginPercent = function (cost, grossMarginAmount) {
    if (cost === 0.0 || grossMarginAmount === 0.0)
        return 0.0;

    var sellingPrice = parseFloat(cost) + parseFloat(grossMarginAmount);
    var grossMarginPercent = ((sellingPrice - cost) / sellingPrice * 100);

    if (isNaN(grossMarginPercent))
        return 0.0;
    if (!isFinite(grossMarginPercent))
        return 0.0;

    return grossMarginPercent;
}

var convertToGrossMarginAmount = function (cost, grossMarginPercent) {
    if (cost === 0.0) //|| grossMarginPercent == 0.0
        return 0.0;

    if (grossMarginPercent === 0)
        return 0;

    var sellingPrice = (cost / (1 - grossMarginPercent / 100));
    var grossMarginAmount = sellingPrice - cost;

    if (isNaN(grossMarginAmount))
        return 0.0;
    if (!isFinite(grossMarginAmount))
        return 0.0;

    return grossMarginAmount;
}

var calculateNewSellingPrice = function (price, caseQuantity) {
   
    if ((caseQuantity !== '' || caseQuantity !== null || caseQuantity !== undefined) && caseQuantity > 1) {
       // return up((caseQuantity * price),2);
        return up(price, 2) * caseQuantity;
    }
    else if (caseQuantity <= 1 || (caseQuantity === '' || caseQuantity === null || caseQuantity === undefined)) {
        return up(price,2);
    }
    
}

var CalculatepriceOnSPChange = function (sellingPrice, caseQuantity) {
    if (caseQuantity > 0) {
        //  return up((sellingPrice / caseQuantity), 2);
        return up(sellingPrice, 2) / caseQuantity;
    }
    else {
        return up(sellingPrice, 2);
    }

   // return up((sellingPrice / caseQuantity), 2);
}
//var calculateTotalSalePrice = function (salePrice, caseQuantity) {
//    if (caseQuantity > 0) {
//        return (salePrice * caseQuantity);
//    } else {
//        return salePrice;
//    }
//}

function up(v, n) {
    //return Math.ceil(v * Math.pow(10, n).toFixed(2)) / Math.pow(10, n);
    return Math.ceil((v * Math.pow(10, n)).toFixed(2)) / Math.pow(10, n);
}
